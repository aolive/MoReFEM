

<!-- toc -->

- [Overview on CI for MoReFEM](#overview-on-ci-for-morefem)
- [SSH access](#ssh-access)
- [The VMs](#the-vms)
  * [Doxygen](#doxygen)
  * [macOS](#macos)
  * [Ubuntu](#ubuntu)
  * [Fedora](#fedora)
- [Administrating the CI](#administrating-the-ci)
  * [Preparing VM](#preparing-vm)
  * [Set up Jenkins nodes](#set-up-jenkins-nodes)
  * [Set up Jenkins items](#set-up-jenkins-items)
    + [Regular build](#regular-build)
    + [Nightly build](#nightly-build)
    + [User build](#user-build)

<!-- tocstop -->

# Overview on CI for MoReFEM

Continuous integration is now deployed on the [CI platform](https://ci.inria.fr) provided by Inria.

The purpose is to ensure a new development is fine and does not trigger any warning or error on an handful of configurations before requiring a merge request, hence easing the task of the release manager.

There are currently three kind of builds on a given VM:

- Regular one, which is triggered whenever a push is performed toward the _develop_ branch of the main repository.
- Nightly ones, which are triggered each night only if the repository has been modified since last build. These builds are more thorough (for instance a clean is called systematically) but therefore longer (CI VMs aren't fast at all to begin with due to their limited RAM so cleaning is not done in regular builds).
- User-specific builds, which work as regular builds on a developer repository (not the main one contrary to previous builds). Any user might configure it as he/she sees fit; I have for instance chosen to follow any branch I push on my repository.

# SSH access

If you want to interact with the VMs (rather than just analysing the outputs on the CI platform), you will need to register yourself to get proper access. The procedure is documented [here](Administration/ssh.md).


# The VMs

Here are all the VMs currently deployed; the link of each of them is expected to display the current settings of each VM.

## Doxygen

- [Doxygen 1.8.11](VM/Doxygen.md)

## macOS

- [Apple clang 9.1.0, debug mode, MoReFEM as a static library](VM/macOSClangDebug.md)
- [Apple clang 9.1.0, debug mode, MoReFEM as several shared libraries](VM/macOSClangDebugSharedSeveralLibs.md)
- [Apple clang 9.1.0, release mode, MoReFEM as several static libraries](VM/macOSClangReleaseSeveralLibs.md)

## Ubuntu

- [gcc 7.3, debug mode, MoReFEM as a static library](VM/UbuntuGccDebug.md)
- [gcc 8.0.1, debug mode, MoReFEM as several shared libraries](VM/UbuntuGccDebugSharedSeveralLibs.md)
- [gcc 7.3, release mode, MoReFEM as a static library](VM/UbuntuGccRelease.md)

## Fedora

- [clang 6.0, release mode, MoReFEM as several shared libraries](VM/FedoraClangReleaseSharedSeveralLibs.md)
- [gcc 8.1, debug mode, MoReFEM as a static library](VM/FedoraGccDebug.md)



# Administrating the CI

CI is administrated through a [SYMPA](https://sympa.inria.fr/sympa) diffusion list, as recommended by this [tutorial](https://iww.inria.fr/sed-sophia/fr/gitlab-inria-frci-inria-fr-integration/).


## Preparing VM

The steps to prepare a new VM are described for [Ubuntu](Administration/PreparingVM/Ubuntu.md), [Fedora](Administration/PreparingVM/Fedora.md) and [macOS](Administration/PreparingVM/macOS.md).

## Set up Jenkins nodes

The settings to choose for a given node are given [here](Administration/Jenkins/Nodes.md).


## Set up Jenkins items

### Regular build

What I call a 'regular' build is a build that is expected to run whenever a change is pushed to the __develop__ branch of the MoReFEM project.

You may find the specific Jenkins settings [here](Administration/Jenkins/RegularBuild.md).

### Nightly build

A nightly build is a build of the __develop__ branch that is run each night provided the code source changed since last run. 

You may find the specific Jenkins settings [here](Administration/Jenkins/NightlyBuild.md).

### User build

Both builds described above are builds on the __develop__ branch of the main MoReFEM repository, which is the branch on which the features that will be in next release are put.

However, one might want to be able to apply continuous integration on an experimental branch on its own repository.

The way to set such a build is described [here](Administration/Jenkins/UserBuild.md).





