

<!-- toc -->

- [User build](#user-build)
  * [On Jenkins](#on-jenkins)
    + [General settings](#general-settings)
    + [Source code management](#source-code-management)
    + [Build triggers](#build-triggers)
    + [Build](#build)
    + [Post build](#post-build)
  * [On gitlab](#on-gitlab)

<!-- tocstop -->

# User build

__IMPORTANT:__ Path are differents on macOS and Linux VMs; the workspaces are saved respectively on:
- /Volumes/SupplDrive on macOS.
- /media/suppl\_drive on Linux.

The screenshots used here might refer to one or the other; please adapt depending on the OS currently dealt with on your VM.

## On Jenkins

The convention to use for a user build is to preffix the regular build from which it's adapted with the name of the user in braces.

For instance __{sgilles}FedoraClangReleaseSharedSeveralLibs__, __{sgilles}macosClangReleaseSeveralLibs__ or __{sgilles}UbuntuGccRelease__.

To access the configuration of a build, you need to right click on it in the menu:

<img src="Images/ConfigureItemMenu.png" width="500px"/>


### General settings

<img src="Images/User/General.png" width="500px"/>

### Source code management

Make sure here to adapt to point to your own repository!

<img src="Images/User/SourceCodeManagement.png"  width="500px"/>

(don't bother about the error message).

I also choose here to track all branches; you may want to be more selective.

### Build triggers

<img src="Images/User/BuildTriggers1.png"  width="500px"/>

<img src="Images/User/BuildTriggers2.png"  width="500px"/>

### Build

<img src="Images/User/Build1.png"  width="500px"/>

<img src="Images/User/Build2.png"  width="500px"/>


### Post build

Post-build procedure should remain unchanged from the [procedure](RegularBuild.md#post-build) already described for a regular build.

I added the following step to help the disk not to be filled too much after a while (this requires a dedicated Jenkins plugin):

<img src="Images/User/PostBuildNew.png"  width="500px"/>


## On gitlab

Follow the [procedure](RegularBuild.md#on-gitlab) already described for a regular build.