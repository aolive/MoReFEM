# Set up the nodes

The nodes are what are under the "Build Executor status" in Jenkins:

<img src="Images/NodeList.png" alt="screenshot Jenkins nodes" width="300px"/>

Right click on one of them and choose _Configure_; the settings you should choose are:

<img src="Images/NodeSettings.png" alt="screenshot node settings" width="1000px"/>

by of course adapting _Name_, _Description_ and _Host_ to the node you are actually considering.