

<!-- toc -->

- [Regular build](#regular-build)
  * [On Jenkins](#on-jenkins)
    + [General settings](#general-settings)
    + [Source code management](#source-code-management)
    + [Build triggers](#build-triggers)
    + [Build](#build)
    + [Post build](#post-build)
  * [On gitlab](#on-gitlab)

<!-- tocstop -->

# Regular build

__IMPORTANT:__ Path are differents on macOS and Linux VMs; the workspaces are saved respectively on:
- /Volumes/SupplDrive on macOS.
- /media/suppl\_drive on Linux.

The screenshots used here might refer to one or the other; please adapt depending on the OS currently dealt with on your VM.

## On Jenkins

The regular builds are typically named by an amalgation of OS, compiler, mode (debug or release), _Shared_ if the library is shared (by convention not specified if the library is static) and _SeveralLibs_ if MoReFEM is compiled as several libraries (_Core_, _Geometry_, _FiniteElement_ and so on...)

For instance __FedoraClangReleaseSharedSeveralLibs__, __macosClangReleaseSeveralLibs__ or __UbuntuGccRelease__.

To access the configuration of a build, you need to right click on it in the menu:

<img src="Images/ConfigureItemMenu.png" width="500px"/>

The settings to choose are:

### General settings

<img src="Images/Regular/General.png" width="500px"/>

### Source code management

<img src="Images/Regular/SourceCodeManagement.png"  width="500px"/>

(don't bother about the error message).

### Build triggers

<img src="Images/Regular/BuildTriggers1.png"  width="500px"/>

<img src="Images/Regular/BuildTriggers2.png"  width="500px"/>

### Build

<img src="Images/Regular/Build1.png"  width="500px"/>

<img src="Images/Regular/Build2.png"  width="500px"/>


### Post build

<img src="Images/Regular/PostBuild1.png"  width="500px"/>

<img src="Images/Regular/PostBuildExcludeFile.png"  width="500px"/>

<img src="Images/Regular/PostBuild2.png"  width="500px"/>


## On gitlab

Go on the [MoReFEM main project](https://gitlab.inria.fr/MoReFEM/CoreLibrary) and choose in settings the integration menu:

<img src="Images/Regular/Gitlab1.png"  width="100px"/>

Then there enter the URL of the webhook and the token computed in Jenkins (see section build-triggers above):

<img src="Images/Regular/Gitlab2.png"  width="500px"/>

Finally, check the communication is allright in the test interface:

<img src="Images/Regular/Gitlab3.png"  width="500px"/>