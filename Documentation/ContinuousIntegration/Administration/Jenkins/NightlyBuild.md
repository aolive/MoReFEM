

<!-- toc -->

- [Nightly build](#nightly-build)
  * [On Jenkins](#on-jenkins)
    + [General settings](#general-settings)
    + [Source code management](#source-code-management)
    + [Build triggers](#build-triggers)
    + [Build](#build)
    + [Post build](#post-build)

<!-- tocstop -->

# Nightly build

Nightly builds are the most punishing and ressource intensive ones: no compilation warnings are allowed, and builds are systematically preceded by a clean.

__IMPORTANT:__ Path are differents on macOS and Linux VMs; the workspaces are saved respectively on:
- /Volumes/SupplDrive on macOS.
- /media/suppl\_drive on Linux.

The screenshots used here might refer to one or the other; please adapt depending on the OS currently dealt with on your VM.


## On Jenkins

The nightly builds typically named as their [regular counterpart](RegularBuild.md) with a __-nightly__ suffix added.

For instance __FedoraClangReleaseSharedSeveralLibs-nightly__, __macosClangReleaseSeveralLibs-nightly__ or __UbuntuGccRelease-nightly__.

To access the configuration of a build, you need to right click on it in the menu:

<img src="Images/ConfigureItemMenu.png" width="500px"/>

The settings to choose are:

### General settings

<img src="Images/Nightly/General.png" width="500px"/>

### Source code management

<img src="Images/Nightly/SourceCodeManagement.png"  width="500px"/>

(don't bother about the error message).

### Build triggers

<img src="Images/Nightly/BuildTriggers.png"  width="500px"/>


### Build

<img src="Images/Nightly/Build1.png"  width="500px"/>

<img src="Images/Nightly/Build2.png"  width="500px"/>


### Post build

<img src="Images/Nightly/PostBuild1.png"  width="500px"/>

[//]: # (Not an error in path!)
<img src="Images/Regular/PostBuildExcludeFile.png"  width="500px"/>

<img src="Images/Nightly/PostBuild2.png"  width="500px"/>

