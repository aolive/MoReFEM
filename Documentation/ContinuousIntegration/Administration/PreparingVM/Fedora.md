

<!-- toc -->

  * [Create the VM](#create-the-vm)
- [Connect yourself on the newly created machine](#connect-yourself-on-the-newly-created-machine)
- [(optional) Set-up configuration for shell and git](#optional-set-up-configuration-for-shell-and-git)
- [Set-up your Fedora instance](#set-up-your-fedora-instance)
  * [Update the system](#update-the-system)
  * [Install required applications](#install-required-applications)
    + [Sanitizer](#sanitizer)
    + [gcc / CMake / git (for Gulliver; already there for CI VM)](#gcc--cmake--git-for-gulliver-already-there-for-ci-vm)
- [Install and mount the supplementary disk](#install-and-mount-the-supplementary-disk)
- [SSH access](#ssh-access)
- [Third party libraries](#third-party-libraries)
- [MoReFEM](#morefem)

<!-- tocstop -->

## Create the VM

Create the VM on ci.inria.fr in the MoReFEM project with the following choices:

*  xLarge instance (2 Go of RAM is not enogh to compile Fortran part of a third party library and yields an ICE)
*  Fedora 25 (the most recent LTS provided at the time I created the VM)
*  Additional disk storage of 100 Go (18 Go provided by default aren't enough, at least with static library, and on a given VM we put at least two builds ('regular' when a push is done to develop branch and 'nightly' which is more of the same but with more work (clean before the build for instance).

# Connect yourself on the newly created machine

What follows assumes you have successfully registered a valid ssh key through the [proper procedure](../ssh.md).

As indicated on [CI website](https://ci.inria.fr), to connect yourself on a machine through ssh you need to type:

````
ssh ci@*machine name*.ci
````

and the password is 'ci'.

Personally I have defined aliases for each machine on my laptop, but it's completely up to you.

# (optional) Set-up configuration for shell and git

I like to get my usual configurations for shell and git, so I would install my own [.gitconfig file](Config/gitconfig) (with M3DISIM-CI identity given) and my [.zshrc](Config/zshrc).

These files may be copied through rsync (assuming you put the downloaded files on your desktop):

````
rsync -av ~/Desktop/zshrc ci@*VM name*.ci:~/.zshrc
rsync -av ~/Desktop/gitconfig ci@*VM name*.ci:~/.gitconfig
````

I need to install zsh, not present by default:

````
sudo dnf install -y zsh
````

and then I can use it through:

````
zsh
````


# Set-up your Fedora instance

## Update the system

Fedora 25 is shipped with gcc-6.1, which is not enough for some C++ 17 features such as string_view. So I need to first update the system, using guidelines from https://fedoraproject.org/wiki/DNF_system_upgrade to upgrade to Fedora 28:

````
sudo dnf update -y 
sudo dnf upgrade -y --refresh
sudo dnf install dnf-plugin-system-upgrade
sudo dnf system-upgrade download --refresh --releasever=27
sudo dnf system-upgrade reboot
sudo dnf --refresh upgrade
sudo dnf system-upgrade download --refresh --releasever=28
sudo dnf system-upgrade reboot
````

Note: I tried 28 first, and it failed... but when I put the 27 line and used the reboot, it installed the 28 directly.

## Install required applications

````
sudo dnf install -y ninja-build nano git cmake make readline-devel

sudo python3 -m pip install --upgrade pip
python3 -m pip install setuptools six
python3 -m pip install  distro --user
````
### Sanitizer

If you're running a VM with a sanitizer applied, you also need to install liblsan:

````
sudo dnf install liblsan
````

### gcc / CMake / git (for Gulliver; already there for CI VM)

````
sudo dnf install gcc gcc-c++ gcc-gfortran
````

# Install and mount the supplementary disk

(with great help from https://help.ubuntu.com/community/InstallingANewHardDrive)

````
sudo fdisk /dev/vdb
````

Choose 'n', 'p', 1, validate sectors twice, 'w'. Then type in terminal:

````
sudo mkfs -t ext4 /dev/vdb
sudo mkdir /media/suppl_drive
sudo nano -Bw /etc/fstab
````

In this file, add the line:

````
/dev/vdb /media/suppl_drive ext4 defaults 0 2
````

and then in terminal type:

````
sudo mount -a
sudo chown -R ci:ci /media/suppl_drive
````

# SSH access

We need to provide a SSH key so that the VM may access gitlab content through the git protocol. To do so, type:

````
ssh-keygen -t rsa -C "m3disim-ci@inria.fr" -b 4096
````

and register the public key (content of /builds/.ssh/id_rsa.pub) in the settings of the M3DISIM-CI gitlab account. Name it meaningfully (Ubuntu gcc debug for instance here).

Edit ~/.ssh/authorized_keys and put in it access for the master:

````
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQDROo+lBQVMUeI/5TwjLUqv7zSwwCC2cRwN+lrPQK31+/fjMCLlH+TeSniv5QyV2zqvwOObgyZdiY4dC0stBzGkIAlBJCPmWfW3F1HFbPwbohKs/xjQIN2LJ4ResDeYWugsYdVpVmpdr1mezdF7GoB0wxlALHgW8LsA3JGIqgQVSw== morefem
````


# Third party libraries

**WARNING:** Petsc 3.9 compilation is very needy in RAM and doesn't work with the 4 Go of RAM provided. You therefore need to enable swap space to compile it:

````
sudo dd if=/dev/zero of=swapfile bs=1M count=4K
sudo chmod 600 swapfile
sudo mkswap swapfile
sudo swapon swapfile
````

The swap file is not permanent (see [here](https://www.digitalocean.com/community/tutorials/how-to-add-swap-space-on-ubuntu-16-04) if you ever need it permanently) as we only need this for compilation of third party library.

````
mkdir -p /media/suppl_drive/opt
git clone git@gitlab.inria.fr:MoReFEM/ThirdPartyCompilationFactory.git
cd ThirdPartyCompilationFactory
````

Edit _morefem_linux_gcc.py_ and choose the settings you like (_target_directory_ in the very least needs to be _/media/suppl_drive/opt_).

````
python3 morefem_linux_gcc.py
````

# MoReFEM

The VM is now ready for use by Jenkins; Jenkins is in charge of cloning MoReREM and compiling it.

However, I find it useful to prepare a CMake PreCache file on the VM that details the settings that will be used for all the builds running for this VM (usually take the closest to your sought configuration in the [dedicated directory](../../../../cmake/PreCache) and adapt it to your needs); I usually put it in the root directory with the name _morefem\_settings.cmake_.






