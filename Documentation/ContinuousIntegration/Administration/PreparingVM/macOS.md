

<!-- toc -->

- [Create the VM](#create-the-vm)
- [Connect yourself on the newly created machine](#connect-yourself-on-the-newly-created-machine)
- [Set-up your macOS instance](#set-up-your-macos-instance)
  * [Update the system](#update-the-system)
  * [Set-up the additional disk](#set-up-the-additional-disk)
  * [(optional) Set-up configuration for shell and git](#optional-set-up-configuration-for-shell-and-git)
  * [Install required applications](#install-required-applications)
    + [Ninja](#ninja)
    + [Install CMake](#install-cmake)
    + [gfortran](#gfortran)
    + [pip / distro module](#pip--distro-module)
- [SSH access](#ssh-access)
- [Third party libraries](#third-party-libraries)

<!-- tocstop -->

# Create the VM

Create the VM on ci.inria.fr in the MoReFEM project with the following choices:

*  xLarge instance (2 Go of RAM is not enough to compile Fortran part of a third party library and yields an ICE)
* OSX 10.9!!! We will upgrade to 10.13 with AppStore...
*  Additional disk storage of 100 Go


# Connect yourself on the newly created machine

What follows assumes you have successfully registered a valid ssh key through the [proper procedure](../ssh.md).

As indicated on [CI website](https://ci.inria.fr), to connect yourself on a machine through ssh you need to type:

````
ssh -i ~/.ssh/m3disim-ci/id_rsa -L 5901:morefem-macos-clang-debug:5900 mci001@ci-ssh.inria.fr 
````

(the site gives 5900 as first port but it doesn't work for me).

Then press Cmd + K while on desktop and choose:

````
vnc://localhost:5901
````

as server address.

Login/password is ci.


# Set-up your macOS instance

## Update the system

Go in _Updates_ tab in AppStore and choose to update (you have to use to do so an AppleId; you may create one for free).

Once system is upgraded, install XCode.

## Set-up the additional disk

Format it in APFS format; for the following we will assume it has been named **SupplDrive**


## (optional) Set-up configuration for shell and git

I like to get my usual configurations for shell and git, so I would install my own [.gitconfig file](Config/gitconfig) (with M3DISIM-CI identity given) and my [.zshrc](Config/zshrc-mac).

These files may be copied through rsync (assuming you put the downloaded files on your desktop):

````
rsync -av ~/Desktop/zshrc-mac ci@*VM name*.ci:~/.zshrc
rsync -av ~/Desktop/gitconfig ci@*VM name*.ci:~/.gitconfig
````

To use by default zsh, go in *Terminal > Preferences* and type zsh in *Shell opens with Command *.


## Install required applications

### Ninja

````
git clone git://github.com/ninja-build/ninja.git && cd ninja
./configure.py --bootstrap
mkdir -p /Volumes/SupplDrive/opt/bin
cp ninja /Volumes/SupplDrive/opt/bin
cd ..
rm -rf ninja
````

### Install CMake

Download CMake on https://cmake.org/download/ and install the dmg.

Remove an older version not recent enough:

````
rm -rf /usr/local/bin/cmake  
````

````
cmake --version 
````

should work if you use the zsh environment installed earlier (PATH has been set to find the newly installed cmake)

### gfortran

Go fetch latest gfortran on http://hpc.sourceforge.net and then type (adapt if your browser unzips the target on the fly; personally I don't like this behaviour) :

````
cd /
sudo mv ~/Downloads/gfortran-8.1-bin.tar.gz .  
sudo tar xzf gfortran-8.1-bin.tar.gz
sudo rm -f gfortran-8.1-bin.tar.gz
````

Don't bother about tar error: it seems to work nonetheless.


### pip / distro module

````
sudo python -m ensurepip 
python -m pip install distro --user   
````

# SSH access

We need to provide a SSH key so that the VM may access gitlab content through the git protocol. To do so, type:

````
ssh-keygen -t rsa -C "m3disim-ci@inria.fr" -b 4096
````

and register the public key (content of /builds/.ssh/id_rsa.pub) in the settings of the M3DISIM-CI gitlab account. Name it meaningfully (macos gcc debug for instance).

Edit ~/.ssh/authorized_keys and put in it access for the master (might already be done):

````
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQDROo+lBQVMUeI/5TwjLUqv7zSwwCC2cRwN+lrPQK31+/fjMCLlH+TeSniv5QyV2zqvwOObgyZdiY4dC0stBzGkIAlBJCPmWfW3F1HFbPwbohKs/xjQIN2LJ4ResDeYWugsYdVpVmpdr1mezdF7GoB0wxlALHgW8LsA3JGIqgQVSw== morefem
````


# Third party libraries


````
mkdir -p /Volumes/SupplDrive/opt
git clone git@gitlab.inria.fr:MoReFEM/ThirdPartyCompilationFactory.git
cd ThirdPartyCompilationFactory
````

Edit morefem_macos_clang.py and replace target_directory by */Volumes/SupplDrive/opt/Library*

````
python morefem_macos_clang.py
````

Then your VM should be ready for use for Jenkins, in which you should set-up build of the library and call to CTest (upcoming note on that soon).






