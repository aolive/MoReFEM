

<!-- toc -->

- [Create the VM](#create-the-vm)
- [Connect yourself on the newly created machine](#connect-yourself-on-the-newly-created-machine)
- [(optional) Set-up configuration for shell and git](#optional-set-up-configuration-for-shell-and-git)
- [Set-up your Ubuntu instance](#set-up-your-ubuntu-instance)
  * [Update the system](#update-the-system)
  * [Add in Ubuntu sources the ppa to install a recent gcc](#add-in-ubuntu-sources-the-ppa-to-install-a-recent-gcc)
  * [Install gcc...](#install-gcc)
  * [... or clang / gfortran](#-or-clang--gfortran)
  * [Install required applications](#install-required-applications)
    + [Python](#python)
    + [Miscellaneous (through apt)](#miscellaneous-through-apt)
    + [Install CMake](#install-cmake)
    + [Set up compilers](#set-up-compilers)
      - [If you're using gcc:](#if-youre-using-gcc)
      - [If you're using clang:](#if-youre-using-clang)
- [Install and mount the supplementary disk](#install-and-mount-the-supplementary-disk)
- [SSH access](#ssh-access)
- [Third party libraries](#third-party-libraries)
- [MoReFEM](#morefem)

<!-- tocstop -->

# Create the VM

Create the VM on ci.inria.fr in the MoReFEM project with the following choices:

*  xLarge instance (2 Go of RAM is not enough to compile Fortran part of a third party library and yields an ICE)
*  Ubuntu 16.04 (the most recent LTS provided at the time I created the VM); Vincent Rouvreau (SED) is working on 18.04 and try-outs on Gulliver showed no major issue.
*  Additional disk storage of 100 Go (18 Go provided by default aren't enough, at least with static library, and on a given VM we put at least two builds ('regular' when a push is done to develop branch and 'nightly' which is more of the same but with more work (clean before the build for instance).

# Connect yourself on the newly created machine

What follows assumes you have successfully registered a valid ssh key through the [proper procedure](../ssh.md).

As indicated on [CI website](https://ci.inria.fr), to connect yourself on a machine through ssh you need to type:

````
ssh ci@*machine name*.ci
````

and the password is 'ci'.

Personally I have defined aliases for each machine on my laptop, but it's completely up to you.


# (optional) Set-up configuration for shell and git

I like to get my usual configurations for shell and git, so I would install my own [.gitconfig file](Config/gitconfig) (with M3DISIM-CI identity given) and my [.zshrc](Config/zshrc).

These files may be copied through rsync (assuming you put the downloaded files on your desktop):

````
rsync -av ~/Desktop/zshrc ci@*VM name*.ci:~/.zshrc
rsync -av ~/Desktop/gitconfig ci@*VM name*.ci:~/.gitconfig
````

I need to install zsh, not present by default:

````
sudo apt install -y zsh
````

and then I can use it through:

````
zsh
````

# Set-up your Ubuntu instance

## Update the system

````
sudo apt-get -y update
sudo apt-get -y upgrade
````

In the update command, for grub say you want to keep the local version installed and continue after _Writing GRUB to boot device failed_ error.
 
## Add in Ubuntu sources the ppa to install a recent gcc 

````
sudo apt-get install -y software-properties-common 
sudo add-apt-repository ppa:ubuntu-toolchain-r/test 
sudo apt-get update
````

Even if you're preparing a clang VM make sure to do this step: some third party libraries need gfortran.


## Install gcc...

````
export GCC_VERSION=8
sudo apt-get install -y gcc-${GCC_VERSION}  g++-${GCC_VERSION} gfortran-${GCC_VERSION}
````

On some VMs gcc 7 is installed so set accordingly GCC_VERSION.

## ... or clang / gfortran

**WARNING** For the time being do not use clang on Ubuntu: there seems to be issues with the standard library to use (libc++ or libstdc++) and for instance Boost installation is gory. Fedora is more appropriate to test clang on an Ubuntu distribution.

For Ubuntu 16.04 only; repository for 18.04 would need to be adapted.

````
wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | sudo apt-key add -
sudo apt-add-repository "deb http://apt.llvm.org/xenial/ llvm-toolchain-xenial-6.0 main"
sudo apt-get update
sudo apt-get install -y clang-6.0 libclang-6.0-dev libc++-dev

export GCC_VERSION=8
sudo apt-get install -y gfortran-${GCC_VERSION}
````


## Install required applications

### Python

````
sudo apt-get install -y python python3 python3-pip 

python3 -m pip install --upgrade pip
python3 -m pip install setuptools six
python3 -m pip install distro --user
````


### Miscellaneous (through apt)

````
sudo apt-get install -y git libreadline-dev libncurses5-dev ca-certificates curl ninja-build
````


### Install CMake

Current CMake in Ubuntu 16.04 sources is 3.5, which is not good enough to build MoReFEM which requires 3.8. On Ubuntu 18.04 cmake 3.10.2 is provided so you might use apt to install it.


````
curl -O https://cmake.org/files/v3.10/cmake-3.10.2-Linux-x86_64.sh
sudo sh cmake-3.10.2-Linux-x86_64.sh --prefix=/usr/local --exclude-subdir
rm cmake-3.10.2-Linux-x86_64.sh
````

### Set up compilers

Now that all applications are installed, make sure the correct version of the compilers are used. 

If set up earlier it might have been reversed by _apt install_ commands.

#### If you're using gcc:

````
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-${GCC_VERSION} 100 --slave /usr/bin/g++ g++ /usr/bin/g++-${GCC_VERSION} --slave /usr/bin/gfortran gfortran /usr/bin/gfortran-${GCC_VERSION}
````


#### If you're using clang:

````
sudo update-alternatives --install /usr/bin/gfortran gfortran /usr/bin/gfortran-${GCC_VERSION} 100
sudo update-alternatives --install /usr/bin/clang clang /usr/bin/clang-6.0 100 --slave /usr/bin/clang++ clang++ /usr/bin/clang++-6.0
````



# Install and mount the supplementary disk

(with great help from https://help.ubuntu.com/community/InstallingANewHardDrive)

````
sudo fdisk /dev/vda
````

Choose 'n', 'p', 1, validate sectors twice, 'w'. Then type in terminal:

````
sudo mkfs -t ext4 /dev/vda
sudo mkdir /media/suppl_drive
sudo nano -Bw /etc/fstab
````

In this file, add the line:

````
/dev/vda /media/suppl_drive ext4 defaults 0 2
````

and then in terminal type:

````
sudo mount -a
sudo chown -R ci:ci /media/suppl_drive
````

# SSH access

We need to provide a SSH key so that the VM may access gitlab content through the git protocol. To do so, type:

````
ssh-keygen -t rsa -C "m3disim-ci@inria.fr" -b 4096
````

and register the public key (content of /builds/.ssh/id\_rsa.pub) in the settings of the M3DISIM-CI gitlab account. Name it meaningfully (Ubuntu gcc debug for instance).

Edit _~/.ssh/authorized\_keys_ and put in it access for the master:

````
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQDROo+lBQVMUeI/5TwjLUqv7zSwwCC2cRwN+lrPQK31+/fjMCLlH+TeSniv5QyV2zqvwOObgyZdiY4dC0stBzGkIAlBJCPmWfW3F1HFbPwbohKs/xjQIN2LJ4ResDeYWugsYdVpVmpdr1mezdF7GoB0wxlALHgW8LsA3JGIqgQVSw== morefem
````


# Third party libraries

**WARNING:** Petsc 3.9 compilation is very needy in RAM and doesn't work with the 4 Go of RAM provided. You therefore need to enable swap space to compile it:

````
sudo dd if=/dev/zero of=swapfile bs=1M count=4K
sudo chmod 600 swapfile
sudo mkswap swapfile
sudo swapon swapfile
````

The swap file is not permanent (see [here](https://www.digitalocean.com/community/tutorials/how-to-add-swap-space-on-ubuntu-16-04) if you ever need it permanently) as we only need this for compilation of third party library.

````
git clone git@gitlab.inria.fr:MoReFEM/ThirdPartyCompilationFactory.git
cd ThirdPartyCompilationFactory
````

Edit _morefem\_linux\_gcc.py_ and choose the settings you like (_target\_directory_ in the very least needs to be */media/suppl\_drive/opt*).

Then run it:

````
python3 morefem_linux_gcc.py
````


# MoReFEM

The VM is now ready for use by Jenkins; Jenkins is in charge of cloning MoReREM and compiling it.

However, I find it useful to prepare a CMake PreCache file on the VM that details the settings that will be used for all the builds running for this VM (usually take the closest to your sought configuration in the [dedicated directory](../../../../cmake/PreCache) and adapt it to your needs); I usually put it in the root directory with the name _morefem\_settings.cmake_.



