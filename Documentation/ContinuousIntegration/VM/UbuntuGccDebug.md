# OS : Ubuntu 16.04

The VM proposed by CI support team; Ubuntu 16.04 does not support yet upgrade to 18.04.

<!-- toc -->

- [Compilers](#compilers)
  * [gcc 7](#gcc-7)
- [ThirdParty libraries](#thirdparty-libraries)
- [MoReFEM](#morefem)

<!-- tocstop -->

# Compilers

Obtained from the ppa:ubuntu-toolchain-r/test:

## gcc 7

Using built-in specs.  
COLLECT_GCC=gcc.  
COLLECT_LTO_WRAPPER=/usr/lib/gcc/x86_64-linux-gnu/7/lto-wrapper.  
OFFLOAD_TARGET_NAMES=nvptx-none.  
OFFLOAD_TARGET_DEFAULT=1.  
Target: x86_64-linux-gnu.  
Configured with: ../src/configure -v --with-pkgversion='Ubuntu 7.3.0-16ubuntu3~16.04.1' --with-bugurl=file:///usr/share/doc/gcc-7/README.Bugs --enable-languages=c,ada,c++,go,brig,d,fortran,objc,obj-c++ --prefix=/usr --with-gcc-major-version-only --with-as=/usr/bin/x86_64-linux-gnu-as --with-ld=/usr/bin/x86_64-linux-gnu-ld --program-suffix=-7 --program-prefix=x86_64-linux-gnu- --enable-shared --enable-linker-build-id --libexecdir=/usr/lib --without-included-gettext --enable-threads=posix --libdir=/usr/lib --enable-nls --with-sysroot=/ --enable-clocale=gnu --enable-libstdcxx-debug --enable-libstdcxx-time=yes --with-default-libstdcxx-abi=new --enable-gnu-unique-object --disable-vtable-verify --enable-libmpx --enable-plugin --with-system-zlib --with-target-system-zlib --enable-objc-gc=auto --enable-multiarch --disable-werror --with-arch-32=i686 --with-abi=m64 --with-multilib-list=m32,m64,mx32 --enable-multilib --with-tune=generic --enable-offload-targets=nvptx-none --without-cuda-driver --enable-checking=release --build=x86_64-linux-gnu --host=x86_64-linux-gnu --target=x86_64-linux-gnu  
Thread model: posix.  
gcc version 7.3.0 (Ubuntu 7.3.0-16ubuntu3~16.04.1).  


# ThirdParty libraries

Third party libraries have been installed through the [third-party installation facility](https://gitlab.inria.fr/MoReFEM/ThirdPartyCompilationFactory), using the tag [v18.21](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/tags/v18.21).

Following versions of the libraries are used:
- Boost v.167
- Lua v5.3.4
- Mumps v5.1.2
- OpenBLAS v0.2.20
- OpenMPI v3.1.0
- Parmetis v4.0.3
- Petsc v 3.9.2
- ScaLAPACK v2.0.2
- SuperLU dist v5.3.0



# MoReFEM

MoReFEM is compiled a static library in debug mode with the following [morefem_settings.cmake](cmake/UbuntuGccDebug.cmake) settings file.

