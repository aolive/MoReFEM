

<!-- toc -->

- [OS : Fedora 28](#os--fedora-28)
- [Compilers](#compilers)
  * [Clang](#clang)
  * [gfortran](#gfortran)
- [ThirdParty libraries](#thirdparty-libraries)
- [MoReFEM](#morefem)

<!-- tocstop -->

# OS : Fedora 28

Upgraded from the Fedora 25 provided by CI support.

# Compilers

Obtained from the dnf package manager:

## Clang

clang version 6.0.0 (tags/RELEASE_600/final)  
Target: x86_64-unknown-linux-gnu  
Thread model: posix  
InstalledDir: /bin  
Found candidate GCC installation: /bin/../lib/gcc/x86_64-redhat-linux/8  
Found candidate GCC installation: /usr/lib/gcc/x86_64-redhat-linux/8  
Selected GCC installation: /bin/../lib/gcc/x86_64-redhat-linux/8  
Candidate multilib: .;@m64  
Candidate multilib: 32;@m32  
Selected multilib: .;@m64  

## gfortran

Using built-in specs.  
COLLECT_GCC=gfortran  
COLLECT_LTO_WRAPPER=/usr/libexec/gcc/x86_64-redhat-linux/8/lto-wrapper  
OFFLOAD_TARGET_NAMES=nvptx-none  
OFFLOAD_TARGET_DEFAULT=1  
Target: x86_64-redhat-linux  
Configured with: ../configure --enable-bootstrap --enable-languages=c,c++,fortran,objc,obj-c++,ada,go,lto --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-bugurl=http://bugzilla.redhat.com/bugzilla --enable-shared --enable-threads=posix --enable-checking=release --enable-multilib --with-system-zlib --enable-__cxa_atexit --disable-libunwind-exceptions --enable-gnu-unique-object --enable-linker-build-id --with-gcc-major-version-only --with-linker-hash-style=gnu --enable-plugin --enable-initfini-array --with-isl --enable-libmpx --enable-offload-targets=nvptx-none --without-cuda-driver --enable-gnu-indirect-function --enable-cet --with-tune=generic --with-arch_32=i686 --build=x86_64-redhat-linux  
Thread model: posix  
gcc version 8.1.1 20180502 (Red Hat 8.1.1-1) (GCC)


# ThirdParty libraries

Third party libraries have been installed through the [third-party installation facility](https://gitlab.inria.fr/MoReFEM/ThirdPartyCompilationFactory), using the tag [v18.21](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/tags/v18.21).

Following versions of the libraries are used:
- Boost v.167
- Lua v5.3.4
- Mumps v5.1.2
- OpenBLAS v0.2.20
- OpenMPI v3.1.0
- Parmetis v4.0.3
- Petsc v 3.9.2
- ScaLAPACK v2.0.2
- SuperLU dist v5.3.0



# MoReFEM

MoReFEM is compiled as several shared libraries in release mode with the following [morefem_settings.cmake](cmake/FedoraClangReleaseSharedSeveralLibs.cmake) settings file.

