

<!-- toc -->

- [OS : Ubuntu 16.04](#os--ubuntu-1604)
- [Doxygen](#doxygen)

<!-- tocstop -->

# OS : Ubuntu 16.04

The VM proposed by CI support team; Ubuntu 16.04 does not support yet upgrade to 18.04.

# Doxygen

Obtained from the apt package; this is the version 1.8.11.

More recent versions are anyway problematic, as changes of policy in Doxygen make thousands of warnings:

````
//! Accessor to the \a Foo value.
int GetFoo() const;
````

for instance warns about the return type not documented in more recent versions.