# OS : macOS 10.13.5

Upgraded from the OS X 10.9 provided by the CI support team.

<!-- toc -->

- [Compilers](#compilers)
  * [clang](#clang)
  * [gfortran](#gfortran)
- [ThirdParty libraries](#thirdparty-libraries)
- [MoReFEM](#morefem)

<!-- tocstop -->

# Compilers

clang is obtained through the AppStore, along wih XCode / command line tools.
gfortran is obtained from [HPC Mac OS X](http://hpc.sourceforge.net/).

## clang

Apple LLVM version 9.1.0 (clang-902.0.39.2)  
Target: x86_64-apple-darwin17.6.0   
Thread model: posix  
InstalledDir: /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin


## gfortran

Using built-in specs.  
COLLECT_GCC=gfortran  
COLLECT_LTO_WRAPPER=/usr/local/libexec/gcc/x86_64-apple-darwin17.5.0/8.1.0/lto-wrapper  
Target: x86_64-apple-darwin17.5.0  
Configured with: ../gcc-8.1.0/configure --enable-languages=c++,fortran  
Thread model: posix  
gcc version 8.1.0 (GCC)  


# ThirdParty libraries

Third party libraries have been installed through the [third-party installation facility](https://gitlab.inria.fr/MoReFEM/ThirdPartyCompilationFactory), using the tag [v18.21](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/tags/v18.21).

Following versions of the libraries are used:
- Boost v.167
- Lua v5.3.4
- Mumps v5.1.2
- OpenBLAS v0.2.20
- OpenMPI v3.1.0
- Parmetis v4.0.3
- Petsc v 3.9.2
- ScaLAPACK v2.0.2
- SuperLU dist v5.3.0



# MoReFEM

MoReFEM is compiled as a single static library in debug mode with the following [morefem_settings.cmake](cmake/macOSClangDebug.cmake) settings file.

