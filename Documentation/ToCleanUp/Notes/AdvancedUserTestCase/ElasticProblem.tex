%
%  untitled
%
%  Created by Sebastien Gilles on 2013-05-16.
%  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
%
\documentclass[]{article}

% Use utf-8 encoding for foreign characters
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

% Setup for fullpage use
\usepackage{fullpage}

% Uncomment some of the following if you use the features
%
% Running Headers and footers
%\usepackage{fancyhdr}

% Multipart figures
%\usepackage{subfigure}

% More symbols
\usepackage{amsmath}
\usepackage{amssymb}
%\usepackage{latexsym}
\usepackage{amsfonts}

% Surround parts of graphics with box
\usepackage{boxedminipage}



% Package for biblio
\usepackage[authoryear,round,comma]{natbib}



% ========================================================================
% Code
% ========================================================================
% Package for including code in the document
\usepackage{color}
\usepackage{../new_commands}



 \usepackage{listings}
  \usepackage{courier}
 \lstset{
		language = C++, 
		deletestring=[b]{'},
		morekeywords={macro,func,solve,problem,fespace,dx,dy,dz,int3d,int2d,'},
		morecomment=[l][\color{mygreen}]{//},
		morecomment=[l][\color{myblue}\bfseries]{//EOM},
        basicstyle=\footnotesize\ttfamily,
        %numbers=left,             
        numberstyle=\tiny,         
        %stepnumber=2,             
        numbersep=5pt,             
        tabsize=2,                 
        extendedchars=true,        
        breaklines=true,           
        keywordstyle=\color{myred}\bfseries,
   		%frame=b,         
        stringstyle=\color{myblue}\bfseries, 
        showspaces=false,          
        showtabs=false,            
        xleftmargin=17pt,          
        framexleftmargin=17pt,
        framexrightmargin=5pt,
        framexbottommargin=4pt,
        %backgroundcolor=\color{lightgray},
        showstringspaces=false,     
		frame=shadowbox,
		rulesepcolor=\color{lightgray}
 }
 %\lstloadlanguages{C++}


% If you want to generate a toc for each chapter (use with book)
\usepackage{minitoc}

% This is now the recommended way for checking for PDFLaTeX:
\usepackage{ifpdf}

%\newif\ifpdf
%\ifx\pdfoutput\undefined
%\pdffalse % we are not running PDFLaTeX
%\else
%\pdfoutput=1 % we are running PDFLaTeX
%\pdftrue
%\fi

\ifpdf
\usepackage[pdftex]{graphicx}
\else
\usepackage{graphicx}
\fi



\title{A test case for what an advanced user would have to do: the elastic problem}
\author{Sébastien Gilles}

%\date{2013-05-23}

\begin{document}
	
\newcommand{\refchapter}[1]{chapter \ref{#1}}
\newcommand{\refsection}[1]{section \ref{#1}}
\newcommand{\refcode}[1]{code excerpt \ref{#1}}
\renewcommand{\lstlistingname}{Code excerpt}
\renewcommand{\lstlistlistingname}{Table of code excerpts}


\newcommand{\subsubsubsection}[1]
{
\bigskip
\textbf{#1}
\bigskip
}

\ifpdf
\DeclareGraphicsExtensions{.pdf, .jpg, .tif}
\else
\DeclareGraphicsExtensions{.eps, .jpg}
\fi

\maketitle


\tableofcontents
\newpage
\lstlistoflistings
\newpage


\section*{Introduction}

The purpose here is to demonstrate what is expected from an advanced user, i.e. someone who wants to define its own problem assuming all the global operators he needs are already correctly defined\footnote{Definition of those global operators is the prerogative of a developer - the required steps will be described in an upcoming document}.
\medskip

I chose here the elastic problem with Newmark time scheme because it's relatively straightforward; hyperelastic problem is for instance much richer but the code would be cluttered by subtleties that aren't our topic here (for instance policies are used to choose which time scheme approximation or hyperelastic law is used).
\medskip

It should be noticed that albeit the current HappyHeart is working well for the problems currently implemented, some functionalities still have to be generalized. There is for instance the underlying assumption that only one mesh is involved in a model, which is obviously too limited. 

\medskip
This document will include many portions of code (almost all of elastic problem in fact); to ease the reading I will however skip some steps such as the include guards or the HappyHeart namespace. Likewise, such requirements of the coding style such as the two blank lines after a namespace will be ignored here.

\medskip
As there are many parts that would be redundant from one problem to another, XCode templates are used to speed up the creation of a new problem.


\section{Main program}

The main program is pretty short, with mostly a call to the appropriate \lstinline{Model} class\footnote{There are actually few additional lines but the core of the main boils down to what is shown here.}:

\begin{lstlisting}[caption=Main program.]
#include "ThirdParty/Wrappers/Petsc/Petsc.hpp"
#include "Problems/Elasticity/InputParameterList.hpp"
#include "Problems/Elasticity/ElasticityModel.hpp"    

int main(int argc, char ** argv)
{    
    Wrappers::Mpi::InitEnvironment(argc, argv);
    Wrappers::Mpi::shared_ptr mpi_world_ptr =
        std::make_shared<Wrappers::Mpi>(0, Wrappers::MpiNS::Comm::World); // must be called before Petsc RAII
    Wrappers::Petsc::Petsc raii_over_petsc(__FILE__, __LINE__);

    std::string input_parameter_file =
        Utilities::CommandLineOptions::InputParameterFileFromCommandLine(argc, argv, __FILE__, __LINE__);

    ElasticityNS::InputParameterList
        input_parameter_data(input_parameter_file, mpi_world_ptr);

    ElasticityNS::ElasticityModel model(mpi_world_ptr, input_parameter_data);
    model.Run(input_parameter_data);

    return 0;
}
    
\end{lstlisting}

The three first lines in the main initialize correctly both Petsc and Mpi; they use the RAII idiom so that there is no need to call functions like \lstinline{Finalize()} at the end of the program.

\medskip

The next line reads the data from the input file and stores them in memory; \refsection{SectionInputParameterList} will describe this step.

\medskip

Finally, the elastic model is built and its method \lstinline{Run()} is called. This method is a shortcut for the usual loop required by Verdandi \lstinline{Model}:

\begin{lstlisting}[label=CodeVerdandiLoop,caption=Typical time loop in Verdandi models.]
Initialize();
while (!HasFinished())
{
    InitializeStep();
    Forward();
    FinalizeStep();
}
Finalize();
    
\end{lstlisting}

\section{Define the input parameter list}\label{SectionInputParameterList}

One of the first task is to define the list of the parameters that are to be read from the input file. To do so, a file named \lstinline{InputParameterList} has been created with the following content:

\begin{lstlisting}[caption=List of input parameters.]
# include "Core/InputParameterList.hpp"

namespace ElasticityNS
{
    typedef std::tuple
    <
        InputParameter::Transient::TimeStep,
        InputParameter::Transient::Time,
        InputParameter::Transient::TimeMax,
        InputParameter::Variable::Variable,
        InputParameter::Variable::Nature,
        InputParameter::Variable::DegreeOfExactness,
        InputParameter::Variable::TypeOfFiniteElement,
        InputParameter::BoundaryCondition::EssentialBoundaryConditionsMethod,
        InputParameter::BoundaryCondition::Component,
        InputParameter::BoundaryCondition::Variable,
        InputParameter::BoundaryCondition::BCValue,
        InputParameter::BoundaryCondition::BoundaryConditionTypeValue,
        InputParameter::BoundaryCondition::NumLabel,
        InputParameter::BoundaryCondition::Label,
        InputParameter::Miscellaneous::Verbose,
        InputParameter::Mesh::InputMesh,
        InputParameter::Mesh::MeshDir,
        InputParameter::Petsc::AbsoluteTolerance,
        InputParameter::Petsc::GmresRestart,
        InputParameter::Petsc::MaxIteration,
        InputParameter::Petsc::Preconditioner,
        InputParameter::Petsc::RelativeTolerance,
        InputParameter::Petsc::SetPreconditionerOption,
        InputParameter::Petsc::Solver,
        InputParameter::Solid::PlaneStressStrain,
        InputParameter::Solid::VolumicOrSurfacicForce,
        InputParameter::Solid::Poisson,
        InputParameter::Solid::Young,
        InputParameter::Solid::VolumicMass,
        InputParameter::Force::Volumic,
        InputParameter::Result::OutputDirectory,
        InputParameter::Result::MeditWriteSolution,
        InputParameter::Result::EnsightWriteSolution
    > InputParameterTuple;

    typedef InputParameterList<InputParameterTuple> InputParameterList;

} // namespace ElasticityNS
    
\end{lstlisting}

Each entry in this tuple is a class defined in \lstinline{Core} that specifies the type of the variable, the conditions it is expected to meet or the eventual default value to apply when a new default parameter file is created. A dedicated document explains more thoroughly the purpose of such a class; what we need to know here is that we can generate from this file the skeleton of the new model input parameter file\footnote{Through a program named \lstinline{CreateDefaultInputFile}; this program is quite crude and requires to edit its main to generate the required output.}:

\begin{lstlisting}[caption=Example of generated input file.]
-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.

-- transient
transient = {
	-- Time step between two iterations, in seconds.
	-- Expected format: VALUE
	-- Constraint: v > 0.
	timeStep = 0.1,

	-- Physical time.
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	time = 0.,

	-- Maximum time.
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	timeMax = No default value was provided!
}
...
\end{lstlisting}

\textbf{Warning:} Most parameters inside the input file still stem directly from Felisce code; a refactoring is to be performed here, for instance to make the FiniteElementSpace concept directly appear as a block in the input file.

\textbf{TODO:} Some of these parameters that are common to all problems (for instance the path to the output directory or the solver to be used) should be implicitly built in, so that the tuple there would only include parameters specific to the problem at hand.

\section{Model}\label{SectionModel}

\lstinline{ElasticityModel} must be defined as a child of the base \lstinline{Model} class; inheritance relies here on the \textit{Curiously recurrent template pattern} idiom.

\begin{lstlisting}[caption=Model inheritance.]
namespace ElasticityNS
{
    class ElasticityModel : public Model<ElasticityModel>
    { ... };    
} // namespace ElasticityNS
\end{lstlisting}

\subsection{Data attributes}\label{SectionModelDataAttributes}

This class holds only a data attribute specific to the problem considered: a pointer to the \lstinline{VariationalFormulationElasticity} object which will be described in \refsection{SectionVariationalFormulationElasticity}.

\subsection{Constructor}\label{SectionModelConstructor}

\begin{lstlisting}[caption=ElasticityModel constructor.]
ElasticityModel::ElasticityModel(Wrappers::Mpi::shared_ptr mpi_ptr,
                                 const InputParameterList& input_parameter_data)
: Model<ElasticityModel>(mpi_ptr, input_parameter_data),
variational_formulation_(nullptr)
{
    variational_formulation_ = std::make_shared<VariationalFormulationElasticity>(mpi_ptr, GetTransientParameters(), GetGeometricMeshRegionPtr());
    
    variational_formulation_->Init(GetUnknownManager(),
                                   input_parameter_data,
                                   DoComputeLocal2GlobalProcessorWise::no);
}
    
\end{lstlisting}

Constructor takes two arguments: the object in charge of parallelism and the object that holds informations about the parameter from the input file. These arguments are used mostly by the base class \lstinline{Model}.\\

The inherited constructor just adds a proper initialization of the object mentioned in \refsection{SectionModelDataAttributes}. 
\medskip

All other constructors (copy, move) are disabled.

\subsection{The mandatory methods}

The following methods must by construction be defined and implemented in the inherited class, even if they do nothing:

\begin{itemize}
    \item \lstinline{Initialize()}
    \item \lstinline{DerivedAdditionalHasFinishedConditions()}
    \item \lstinline{DerivedAdditionalInitializeStep()}
    \item \lstinline{Forward()}
    \item \lstinline{FinalizeStep()}
    \item \lstinline{Finalize()}
\end{itemize}

All of them are closely related to the loop inspired by Verdandi already mentioned in \refcode{CodeVerdandiLoop}.

I will describe each of them in the following:

\subsubsection{Initialize()}

\begin{lstlisting}[caption=ElasticityModel::Initialize().]
void ElasticityModel::Initialize()
{
    auto& formulation = this->GetNonCstVariationalFormulation();
    
 ... (lines for standard output) ...

    formulation.RunStaticCase();
    formulation.PrepareDynamicRuns();
}
\end{lstlisting}

This method calls the variational formulation to run the static case and prepare the intermediate quantities required by the dynamic ones (in elastic problem no matrix needs to be assembled past this step).

     
\subsubsection{HasFinished()} \label{SectionHasFinished}

This method is used to define when the time loop must be stopped. The usual condition is when the maximum time specified in the input file is reached; as this condition is common to all problems it is already implemented in the base \lstinline{Model} class.
\medskip

Any additional condition specific to one problem must be given through a method which returns \lstinline{true} when one of those additional stop conditions has been met. In elastic problem there are none, hence:

\begin{lstlisting}[caption=ElasticityModel::DerivedAdditionalHasFinishedConditions().]
bool ElasticityModel::DerivedAdditionalHasFinishedConditions() const
{
    return false;
}   
\end{lstlisting}
 
 
\subsubsection{InitializeStep()} 

As for \lstinline{HasFinished()}, there is a common behavior described in the base class: update the time and print it on screen.
\medskip

This base class requires that the derived class defines a method named \lstinline{DerivedAdditionalInitializeStep()}; for elastic model the body is left empty:

\begin{lstlisting}[caption=ElasticityModel::DerivedAdditionalInitializeStep().]
void ElasticityModel::DerivedAdditionalInitializeStep()
{
    // No additional operation.
}    
\end{lstlisting}

\subsubsection{Forward()}

This method calls the bulk of the time step calculation; for elastic model its content is:

\begin{lstlisting}[caption=ElasticityModel::Forward()]
void ElasticityModel::Forward()
{
    auto& formulation = this->GetNonCstVariationalFormulation();            
    
    // Only Rhs is modified at each time iteration; compute it and solve the system.
    formulation.ComputeDynamicSystemRhs();
    formulation.ApplyEssentialBoundaryCondition();
    formulation.SolveLinear();
}
\end{lstlisting}

It's quite straightforward: the RHS of the system is computed, boundary conditions are applied on top of it and the linear solver is called. 
\medskip

The last two methods are generic and therefore already defined in the base class; the one about RHS computation is addressed in \refsection{SectionVariationalFormulationElasticity}.

\subsubsection{FinalizeStep()}

The place in which the quantities for the next time iteration are updated. 


\begin{lstlisting}[caption=ElasticityModel::FinalizeStep()]
void ElasticityModel::FinalizeStep()
{
    // Update quantities for next iteration.
    auto& formulation = this->GetNonCstVariationalFormulation();
    formulation.WriteSolution();  
    formulation.UpdateDisplacementAndVelocity();
}
\end{lstlisting}

\textbf{Note:} I'm aware it means there is a useless update at the very last time iteration, but performing this in \lstinline{InitializeStep()} came with its own lot of issues\footnote{Namely a condition was required at each time iteration to distinguish the very first dynamic iteration from the following ones.}.


\subsubsection{Finalize()}

For elastic model, no such operation (but it might change in the future if for instance solutions are written only at the end).

\begin{lstlisting}[caption=ElasticityModel::Finalize()]
void ElasticityModel::Finalize()
{ }
\end{lstlisting}

\section{Variational formulation}\label{SectionVariationalFormulationElasticity}

As for \lstinline{Model}, \lstinline{VariationalFormulation} relies on the CRTP-idiom:

\begin{lstlisting}[caption=VariationalFormulation inheritance.]
namespace ElasticityNS
{
    class VariationalFormulationElasticity
    : public VariationalFormulation<VariationalFormulationElasticity>
    { ... }
} // namespace ElasticityNS


\end{lstlisting}
 

 \subsection{Constructor}  
 
Constructor is defined this way:

\begin{lstlisting}[caption=VariationalFormulationElasticity constructor.]
VariationalFormulationElasticity
::VariationalFormulationElasticity(Wrappers::Mpi::shared_ptr mpi,
                                   const TransientParameters& transient_parameters,
                                   GeometricMeshRegion::shared_ptr mesh)
: Parent(mpi,
         transient_parameters,
         mesh),
stiffness_operator_(nullptr),
mass_operator_(nullptr),
volumic_force_operator_(nullptr),
volumic_mass_(std::numeric_limits<double>::min())
{ }
\end{lstlisting}

So it doesn't do much: constructor of parent class is called and few additional data attributes (addressed in \refsection{SectionVFDataAttributes}) are initialized to default values.
\medskip

However, as shown in \refsection{SectionModelConstructor}, \lstinline{VariationalFormulation} classes requires a call to a \lstinline{Init} method to be fully initialized\footnote{This is due to a limitation of C++: in a constructor you can't successfully call a method defined in a derived class, neither by dynamic polymorphism (virtual methods) or static polymorphism (CRTP - used here). Compiler would probably accept it without any warning but the behavior could not be what you expect - or it would but wouldn't be on another machine or compiler.}.
\medskip

\textbf{Warning:} this constructor dependency upon a mesh will disappear shortly when \lstinline{FiniteElementSpace} are fully implemented.
\medskip

It should be noted that this is the sole constructor of the class: both copy and move constructors have been disabled.

\subsection{Data attributes}\label{SectionVFDataAttributes}

There are many data attributes in this class, as it is there most of the data specific of the calculation are actually stored.

\subsubsection{GlobalVariationalOperators}

First of all, global variational operators used are stored there:

\begin{lstlisting}[caption=Global variational operators.]
    GlobalVariationalOperatorNS::GradOnGradientBasedElasticityTensor::Operator::unique_ptr stiffness_operator_;

    GlobalVariationalOperatorNS::Mass::Operator::unique_ptr mass_operator_;

    GlobalVariationalOperatorNS::VolumicForce::Operator::unique_ptr volumic_force_operator_;
\end{lstlisting}

The classes that define these operators are expected to be already defined in HappyHeart (the way to define one of them will be addressed in an upcoming note).

\subsubsection{Global matrices and vectors}

Then we also need to define several global matrices and vectors, into which the operators will be assembled. There could have been less here: I have clearly made the choice of speed and safety over storage:

\begin{lstlisting}[caption=Global vectors and matrices.]
    GlobalVector vector_current_displacement_;
    GlobalVector vector_current_velocity_;

    GlobalMatrix matrix_current_displacement_;
    GlobalMatrix matrix_current_velocity_;
    GlobalMatrix mass_per_square_time_;
    GlobalMatrix matrix_current_stiffness_;
\end{lstlisting}

\lstinline{GlobalMatrix} and \lstinline{GlobalVector} are typedefs over a home-made class which encapsulates a Petsc \lstinline{Mat} or \lstinline{Vec} object.
    
\subsubsection{Isolated data attributes}
    
Finally, the volumic mass read in the input file is also stored within the class:
    
\begin{lstlisting}[caption=Isolated data attribute.]
    //! Volumic mass read from the input parameter data.
    double volumic_mass_;
\end{lstlisting}


\subsubsection{Accessors}

I won't describe each of them here as they are pretty straightforward, but I have defined accessors for each of these data attributes. They may come in two flavors: 

\begin{itemize}
    \item \lstinline{const Type& GetXXX() const;} which provides a const access.
    \item \lstinline{Type& GetXXX();} when the underlying object is to be modified.
\end{itemize}

References are always used; even global variational operators, which are stored inside unique pointers, are returned that way:

\begin{lstlisting}[caption=Global variational operator accessor.]
GlobalVariationalOperatorNS::VolumicForce::Operator& GetNonCstVolumicForceOperator();
\end{lstlisting}

All these accessors are private; the reason they are defined nonetheless rather than using directly the data attribute is that doing so allows to add checks in the process (for instance for the accessors to global variational operators there is a check about the validity of the pointer).

\subsection{The mandatory methods}

As for \lstinline{Model}, some methods are required in the derived class so that the code compiles correctly:

\begin{itemize}
    \item \lstinline{DerivedInit()}
    \item \lstinline{DerivedDefineOperators()}
    \item \lstinline{DerivedAllocateMatricesAndVectors()}
\end{itemize}

Here are the implementation of each of them:

\subsubsection{DerivedInit()}

\lstinline{DerivedInit()} purpose is to initialize properly the data attributes that are neither global variational operators nor global matrices or vectors. Only \lstinline{volumic_mass_} fits the bill for our problem; it is initialized by reading its value in the structure that holds the data from the input parameter file\footnote{See \refsection{SectionInputParameterList}.}.

\begin{lstlisting}[caption=VariationalFormulationElasticity::DerivedInit().]
template<class InputParameterDataT>
void VariationalFormulationElasticity::DerivedInit(const InputParameterDataT& input_parameter_data)
{
    namespace IPL = Utilities::InputParameterListNS;
    namespace Solid = InputParameter::Solid;
    SetVolumicMass(IPL::ExtractValue<Solid::VolumicMass>::Value(input_parameter_data));
}
\end{lstlisting}

\subsubsection{DerivedDefineOperators()}

This method role is very similar to \lstinline{DerivedInit()} but limited only to global variational operators.

\begin{lstlisting}[caption=VariationalFormulationElasticity::DerivedDefineOperators().]
void VariationalFormulationElasticity::DerivedDefineOperators(const InputParameterList& input_parameter_data)
{
    namespace GVO = GlobalVariationalOperatorNS;
    
    const auto& finite_element_space = this->GetFiniteElementSpace();
    const auto& unknown_list = finite_element_space.GetUnknownManager().GetUnknownList(); 
    
    volumic_force_operator_ 
        = std::move(Utilities::make_unique<GVO::VolumicForce>(input_parameter_data,
                                                              finite_element_space,
                                                              unknown_list));
    
    stiffness_operator_ = 
        std::move(Utilities::make_unique<GVO::GradOnGradientBasedElasticityTensor>
                                                (input_parameter_data,
                                                finite_element_space,
                                                unknown_list));
    
    mass_operator_ = std::move(Utilities::make_unique<GVO::Mass>(input_parameter_data,
                                                                 finite_element_space,
                                                                 unknown_list));        
}
\end{lstlisting}

The global variational operators defined here are very similar: they all act upon the same domain and all the unknowns in the \lstinline{FiniteElementSpace} are involved for all of them\footnote{Which is not a difficulty here, given there is only one: the displacement\dots}.
\medskip

A technical note to finish: \lstinline{std::move} is absolutely required here due to the property of \lstinline{std::unique_ptr}\footnote{Namely there can't be two \lstinline{std::unique_ptr} pointing to the same object.}.

\subsubsection{DerivedAllocateMatricesAndVectors()}

There is in the base class a method called \lstinline{AllocateMatricesAndVectors()} which allocates the system matrix, the system RHS and the solution.
\medskip

The other global matrices and vectors defined here can share the same structure:

\begin{lstlisting}[caption=VariationalFormulationElasticity::DerivedAllocateMatricesAndVectors().]
void VariationalFormulationElasticity::DerivedAllocateMatricesAndVectors()
{
    const auto& system_matrix = GetSystemMatrix();
    const auto& system_rhs = GetSystemRhs();
    
    GetNonCstMassPerSquareTime().CompleteCopy(system_matrix, __FILE__, __LINE__);
    
    GetNonCstVectorCurrentVelocity().CompleteCopy(system_rhs, __FILE__, __LINE__);
    GetNonCstVectorCurrentDisplacement().CompleteCopy(system_rhs, __FILE__, __LINE__);
    GetNonCstVolumicForce().CompleteCopy(system_rhs, __FILE__, __LINE__);
    
    GetNonCstMatrixCurrentStiffness().CompleteCopy(system_matrix, __FILE__, __LINE__);
    
    GetNonCstMatrixCurrentVelocity().CompleteCopy(system_matrix, __FILE__, __LINE__);
    GetNonCstMatrixCurrentDisplacement().CompleteCopy(system_matrix, __FILE__, __LINE__);
}
\end{lstlisting}

\subsection{RunStaticCase()}

Now the mandatory methods have been implemented, we need to implement the ones we need to perform the calculation (and which are called in \lstinline{Model} - see \refsection{SectionModel}). We'll consider them in the same order as they are called in \lstinline{Model}:

Its implementation is:

\begin{lstlisting}[caption=VariationalFormulationElasticity::RunStaticCase().]
void VariationalFormulationElasticity::RunStaticCase()
{
    AssembleStaticCase();
    ApplyEssentialBoundaryCondition();
    SolveLinear();
    DebugPrintSolutionElasticWithOneUnknown(10);
}
\end{lstlisting}

The first method in this function is defined in the derived class as well; the latter three are on the other hand functions from the base class\footnote{The complicated name of the last one is because it's really a dev function to produce an output on screen that would work only for the specific case of elastic and hyperelastic problems.}.

\begin{lstlisting}[caption=VariationalFormulationElasticity::AssembleStaticCase().]
void VariationalFormulationElasticity::AssembleStaticCase()
{
    GetNonCstVolumicForceOperator().Assemble(1., GetNonCstSystemRhs());    
    GetNonCstStiffnessOperator().Assemble(1., GetNonCstSystemMatrix());
}
\end{lstlisting}

Assembling is very simple there: both occur on the same domain, which is the whole mesh, but there could have been a \lstinline{Domain} argument to restrict the part upon which assembling happens.

\subsection{PrepareDynamicRuns()}

The other function called in \lstinline{Model::Initialize()} is one that prepare the matrices and vectors involved in the dynamic run. No assembling occur past this step: the remaining of the calculation will consist of matrix and vector manipulation.

\begin{lstlisting}[caption=VariationalFormulationElasticity::PrepareDynamicRuns().]
void VariationalFormulationElasticity::PrepareDynamicRuns()
{
    AssembleDynamicCase();
    ComputeDynamicMatrices();
    UpdateDisplacement();
}
\end{lstlisting}

The three methods involved there are all private methods of \lstinline{VariationalFormulationElasticity}.
\medskip

\subsubsection{AssembleDynamicCase()}

The first one assemble the mass matrix, which always appears in the equations with a $\frac{2 \cdot \rho}{\Delta t} $ or $\frac{2 \cdot \rho}{(\Delta t)^2} $ factor. That's the reason for which the matrix is actually stored with the latter as coefficient.

\begin{lstlisting}[caption=VariationalFormulationElasticity::AssembleDynamicCase().]
void VariationalFormulationElasticity::AssembleDynamicCase()
{
    const double mass_coefficient = 2. * GetVolumicMass() / Utilities::Square(GetTransientParameters().GetTimeStep());
        
    GetNonCstMassPerSquareTimeStepOperator().Assemble(mass_coefficient,
                                                      GetNonCstMassPerSquareTime());
}
\end{lstlisting}

\subsubsection{ComputeDynamicMatrices()}

The matrices that actually appear in the equations can hereafter be computed once and for all.
\medskip

The first one is the stiffness matrix, which has already been calculated for the static case. However in the static case it has been stored directly as the system matrix (i.e. the one actually given to the solver); in dynamic there is no longer the direct matching between both.
\medskip

The $0.5$ coefficient is very handy as we shall see in the following.

\begin{lstlisting}[caption=VariationalFormulationElasticity::ComputeDynamicMatrices() - part 1.]
void VariationalFormulationElasticity::ComputeDynamicMatrices()
{
    auto& system_matrix = this->GetNonCstSystemMatrix();
    auto& mass_per_square_time = GetNonCstMassPerSquareTime();

    {
        // System matrix currently holds the stiffness matrix assembled with a coefficient of 1, from the static
        // case.
        system_matrix.Scale(0.5, __FILE__, __LINE__);
        GetNonCstMatrixCurrentStiffness().Copy(system_matrix, __FILE__, __LINE__);
    }
\end{lstlisting}

The new system matrix is actually given by:

\[
    \mathbb{A} = \frac{\rho \cdot \mathbb{M}}{(\Delta t)^2} + \frac{\mathbb{K}}{2}
\]

hence the code:

\begin{lstlisting}[caption=VariationalFormulationElasticity::ComputeDynamicMatrices() - part 2]
    const auto& stiffness_matrix = GetMatrixCurrentStiffness();

    {
        Wrappers::Petsc::AXPY(1.,
                              mass_per_square_time,
                              system_matrix,
                              __FILE__, __LINE__);
    }
\end{lstlisting}

At each iteration, the displacement vector is involved in a product with matrix:

\[
    \mathbb{D} = \frac{\rho \cdot \mathbb{M}}{(\Delta t)^2} - \frac{\mathbb{K}}{2}
\]


\begin{lstlisting}[caption=VariationalFormulationElasticity::ComputeDynamicMatrices() - part 3]
    {
        auto& current_displacement_matrix = GetNonCstMatrixCurrentDisplacement();
        current_displacement_matrix.Copy(mass_per_square_time, __FILE__, __LINE__);
        Wrappers::Petsc::AXPY(-1.,
                              stiffness_matrix,
                              current_displacement_matrix,
                              __FILE__, __LINE__);
    
    }
\end{lstlisting}

And the velocity vector with matrix:

\[
    \mathbb{V} = \frac{\rho \cdot \mathbb{M}}{\Delta t}
\]

\begin{lstlisting}[caption=VariationalFormulationElasticity::ComputeDynamicMatrices() - part 4]
    {
        auto& current_velocity_matrix = GetNonCstMatrixCurrentVelocity();
        current_velocity_matrix.Copy(mass_per_square_time, __FILE__, __LINE__);
        current_velocity_matrix.Scale(GetTransientParameters().GetTimeStep(), __FILE__, __LINE__);
    }

    
}
\end{lstlisting}


\subsubsection{UpdateDisplacement()}\label{SectionUpdateDisplacement}

Finally, the last step to be ready to enter the dynamic loop is to update the current displacement from the resulting values of the static case:

\begin{lstlisting}[caption=VariationalFormulationElasticity::UpdateDisplacement().]
void VariationalFormulationElasticity::UpdateDisplacement()
{
    GetNonCstVectorCurrentDisplacement().Copy(GetSystemSolution(), __FILE__, __LINE__);
}
    
\end{lstlisting}

\subsection{ComputeDynamicSystemRhs()}

At each time iteration, the only quantity in the system that is modified is the RHS; it is obtained through:

\[
    \vec{R} = \mathbb{D} \cdot \vec{y} + \mathbb{V} \cdot \vec{v}
\]

\begin{lstlisting}[caption=VariationalFormulationElasticity::ComputeDynamicSystemRhs()]

void VariationalFormulationElasticity::ComputeDynamicSystemRhs()
{
    auto& rhs = this->GetNonCstSystemRhs();
   
    const auto& current_displacement_matrix = GetMatrixCurrentDisplacement();
    const auto& current_velocity_matrix = GetMatrixCurrentVelocity();
   
    const auto& current_displacement_vector = GetVectorCurrentDisplacement();
    const auto& current_velocity_vector = GetVectorCurrentVelocity();
   
    Wrappers::Petsc::MatMult(current_displacement_matrix, current_displacement_vector, rhs, __FILE__, __LINE__);
    Wrappers::Petsc::MatMultAdd(current_velocity_matrix, current_velocity_vector, rhs, rhs, __FILE__, __LINE__);
}
   
\end{lstlisting}


\subsection{UpdateDisplacementAndVelocity()}

Finally, the very last thing required to make the problem complete is to handle the updates of values between the time iterations. This is handled by:

\begin{lstlisting}[caption=VariationalFormulationElasticity::UpdateDisplacementAndVelocity()]
void VariationalFormulationElasticity::UpdateDisplacementAndVelocity()
{
    UpdateVelocity();
    UpdateDisplacement();
}
\end{lstlisting}

which calls the method already defined in \refsection{SectionUpdateDisplacement} and its counterpart for the velocity. The call order is crucial here: velocity is computed with the displacement of previous iteration.
\medskip

Velocity is computed by:

\[
    \vec{v_{i + 1}} = \frac{2}{\Delta t} \cdot (\vec{y_{i + 1}} - \vec{y_{i}}) - \vec{v_{i}}
\]

which implementation is:

\begin{lstlisting}[caption=VariationalFormulationElasticity::UpdateVelocity()]
void VariationalFormulationElasticity::UpdateVelocity()
{
    const auto& current_displacement_vector = GetVectorCurrentDisplacement();
    const auto& system_solution = GetSystemSolution();
    auto& current_velocity_vector = GetNonCstVectorCurrentVelocity();
    
    assert(GetTransientParameters().GetIteration() > 0 &&
           "Should not be called before the first dynamic iteration is done.");
    {
        Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> solution(system_solution, __FILE__, __LINE__);
        Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> displacement_prev(current_displacement_vector, __FILE__, __LINE__);
        Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> velocity(current_velocity_vector, __FILE__, __LINE__);
        
        const unsigned int size = velocity.GetSize(__FILE__, __LINE__);
        assert(size == solution.GetSize(__FILE__, __LINE__));
        assert(size == displacement_prev.GetSize(__FILE__, __LINE__));
        
        const double factor = 2. / this->GetTransientParameters().GetTimeStep();
        
        for (unsigned int i = 0u; i < size; ++i)
        {
            velocity[i] *= -1.;
            velocity[i] += factor * (solution.GetValue(i) - displacement_prev.GetValue(i));
        }
    }
}
\end{lstlisting}




\end{document}