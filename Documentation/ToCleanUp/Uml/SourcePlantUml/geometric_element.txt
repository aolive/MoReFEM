@startuml

abstract class GeomRefElement {
    # GeomRefElement()
    + {abstract} GetIdentifier()
    + {abstract} Nnode()
    + {abstract} GetDimension()
    + {abstract} GetName()
    + {abstract} NnodeOnReferenceInterface<InterfaceNatureT>()
    + {abstract} ShapeFunction(index_node, local_coords)
    + {abstract} FirstDerivateShapeFunction(index_node, index_coor, local_coords)
    + {abstract} SecondDerivateShapeFunction(index_node, index_coor1, index_coor2, local_coords)  
}

note left of GeomRefElement : This class should not hold any members: all data are deemed to be static.\nHowever, the class is intended to be used polymorphically.

namespace Private {

class TGeomRefElement <TraitsGeomRefElementT> {
    # ~TGeomRefElement()
    .. Final definition of all virtual methods of GeomRefElement,\all of which from TraitsGeomRefElementT. ..
}

class TGeometricElement <TraitsGeomRefElementT> {
    # ~TGeomRefElement()
    .. Final definition of all virtual methods of GeomRefElement,\all of which from TraitsGeomRefElementT. ..
}

}

note "TraitsGeomRefElementT is a Traits class in which all data related to \ngeometric reference element are described through static functions." as notetraits

Private.TGeomRefElement .. notetraits
notetraits .. Private.TGeometricElement

namespace GeoRef {
class Triangle6 {
    + Triangle6() = default
}

class Quadrangle4 {
    + Quadrangle4() = default
}


}

note "Example of final class. There is truly nothing more than a default constructor defined there:\nall has been defined in the step above. The complete machinery is actually quite complex\n(Traits and polymorphic classes are deeply intertwined)." as noteGeomRefElement

noteGeomRefElement -- GeoRef.Triangle6
noteGeomRefElement -- GeoRef.Quadrangle4

abstract class GeometricElement {
    + Several constructors
    + {abstract} GetIdentifier()
    + {abstract} GetName()    
    + {abstract} Nnode() (geometric ones)
    + {abstract} Nvertex()
    + {abstract} Nedge()
    + {abstract} Nface()
    + {abstract} GetDimension()
    + {abstract} ShapeFunction(index_node, local_coords)
    + {abstract} FirstDerivateShapeFunction(index_node, index_coor, local_coords)
    + {abstract} SecondDerivateShapeFunction(index_node, index_coor1, index_coor2, local_coords)
    - {abstract} BuildVertexList(existing_list)
    - {abstract} BuildEdgeList(existing_list)
    - {abstract} BuildFaceList(existing_list)
    - {abstract} BuildVolumeList(existing_list)
    - index_
    - mesh_label_
    - coords_list_
    - vertex_list_
    - edge_list_
    - face_list_
    - volume_
    - higher_interface_type_built_ 
    .. Throws an exception if Ensight not supported ..
    + {abstract} GetEnsightName()
    + {abstract} WriteEnsightFormat()
    .. Throws an exception if Medit not supported ..
    + {abstract} GetMeditIdentifier()
    + {abstract} WriteMeditFormat(...)
    + {abstract} ReadMeditFormat(...)    


}

class Triangle6 {
    + Triangle6() = default
    + Triangle6(mesh, istream)
    + Triangle6(mesh, coords)
}


class Quadrangle4 {
    + Quadrangle4() = default
    + Quadrangle4(mesh, istream)
    + Quadrangle4(mesh, coords)
}


note "Example of final class of GeometricElement. This class is registered in the GeometricElementFactory." as noteGeometricElement


Triangle6 -- noteGeometricElement
Quadrangle4 -- noteGeometricElement

namespace Private {
class GeometricElementFactory <<(S,#FF7700) Singleton>> {    
    + RegisterGeometricElement<GeomRefElementT>(no_arg_create, ensight_create)
    + CreateFromEnsightName(...)
    + CreateFromIdentifier(identifier)
    + GetGeomRefElement(identifier)
    - callbacks associative container
    
}
}



'Inheritance relationships.'
GeomRefElement <|-- Private.TGeomRefElement
GeometricElement <|-- Private.TGeometricElement
Private.TGeomRefElement <|-- GeoRef.Triangle6
Private.TGeometricElement <|-- Triangle6
Private.TGeomRefElement <|-- GeoRef.Quadrangle4
Private.TGeometricElement <|-- Quadrangle4

'Factory'
Private.GeometricElementFactory *-- Triangle6 : registered into as a type of GeometricElement
Private.GeometricElementFactory *-- GeoRef.Triangle6 : registered into as a type of GeomRefElement
Private.GeometricElementFactory *-- Quadrangle4 : registered into as a type of GeometricElement
Private.GeometricElementFactory *-- GeoRef.Quadrangle4 : registered into as a type of GeomRefElement
@enduml
