@startuml

class FiniteEltSpace {
    + FiniteEltSpace(mpi, unknown_manager)  
    .. Accessors ..
    + NnodeBearer()
    + NprocessorWiseDof()
    + NprogramWiseDof()
    + GetProcessorWiseDofList()
    + GetGhostedDofList()
    + GetDirichletBoundaryConditionList()
    + GetUnknownManager()
    + GetFiniteEltStorage()        
    + IsGhosted(node_bearer)
    + IsGhosted(dof)
    - GetNodeBearer(index_)  
    - GetNodeBearerList()    
    .. Initialization methods ..
    + Init(input_parameter_data, do_compute_local_2_global_processor_wise, mesh)
    + DetermineDofProgramWiseIndexList()
    - Partition(cumulative_dof_index, mesh)
    - ReadBoundaryConditionList(input_parameter_data)
    - CreateNodeList()
    - ComputeProcessorWiseAndGhostDofIndex()
    - InitHelper()
    - SetFiniteEltList(mesh)    
    .. Data attributes ..
    - unknown_manager_
    - node_bearer_list_
    - ghost_node_bearer_list_
    - Nprogram_wise_dof_
    - processor_wise_dof_list_
    - ghosted_dof_list_
    - boundary_condition_list_
    - finite_elt_storage_
}



class FiniteEltTypeGroup {
    + FiniteEltTypeGroup(ref_geom_elt, unknown_list, degree_of_exactness)
    + GetRefGeomElt()
    + GetQuadratureRule()
    + GetFiniteEltType(unknown)
    + GetFiniteEltTypeList()
    + Nnode()
    + Ndof()   
    .. Data attributes ..
    - ref_geom_elt_
    - finite_elt_type_list_
    - quadrature_rule_
    .. Cached data for quicker access ..
    - Nnode_
    - Ndof_
}


class FiniteEltType {
    + FiniteEltType(reference_elt, unknown)
    + GetRefFiniteElt()
    + GetUnknown()
    + Nnode()
    + Ndof()
    - &reference_elt_
    - unknown_
}


class FiniteEltGroup {
    + FiniteEltGroup(geometric_elt)
    + GetGeometricElt()
    + GetFiniteElt(unknown)
    + GetFiniteEltList()
    + GetNodeBearerList()
    .. Initialization methods ..
    + SetNodeBearerList(node_bearer_list)
    + AddFiniteElt(finite_elt)
    .. Data attributes ..
    - finite_elt_list_
    - geometric_elt_
    - node_bearer_list_
}


class FiniteElt {
    + FiniteElt(unknown)
    + GetLocal2Global<MpiScaleT>()
    + GetUnknown()
    + AddNode(node)
    + ComputeLocal2Global(do_compute_local_2_global_processor_wise) 
    - GetNodeList()
    - unknown_
    - local_2_global_program_wise_
    - local_2_global_processor_wise_
    - node_list_ (only during initialization phase)
}

class FiniteEltTypeInLocalVariationalOperator {
    + FiniteEltTypeInLocalVariationalOperator(finite_elt_type, various internal indexes)
    + GetRefFiniteElt()
    + GetUnknown()
    + GetLocalNodeIndexList()
    + GetLocalDofIndexList()
    + GetLocalDofIndexList(component_index)
    + Nnode()
    + Ndof()
    - &finite_elt_type_
    - Various internal indexes to navigate through elementary matrix
}

'Extract the attribute finite_elt_storage_ from FiniteEltSpace'
class finite_elt_storage_ << (A,orchid) >>
class FiniteEltSpace o.. finite_elt_storage_

'  Link it to LocalVariationalOperator.'
finite_elt_storage_ "1" *-- "one per RefGeomElt" class FiniteEltTypeGroup : key

'  Link it to FiniteElements.'
finite_elt_storage_ "1" *-- "one per GeomElt" class FiniteEltGroup : list of values

'Extract the attribute finite_elt_type_list_ from FiniteEltTypeGroup'
class finite_elt_type_list_ << (A,orchid) >>
class FiniteEltTypeGroup o.. finite_elt_type_list_

'  Link it to FiniteEltType.'
finite_elt_type_list_ "1" *-- "one per Unknown" class FiniteEltType

'Extract the attribute finite_elt_list_ from FiniteEltGroup'
class finite_elt_list_ << (A,orchid) >>
class FiniteEltGroup o.. finite_elt_list_

'  Link it to FiniteElt.'
finite_elt_list_ "1" *-- "one per Unknown" class FiniteElt

'Extract the attribute finite_elt_type_ from FiniteEltTypeInLocalVariationalOperator'
class finite_elt_type_ << (A,orchid) >>
class FiniteEltTypeInLocalVariationalOperator o.. finite_elt_type_

'  Link it to FiniteEltType.'
finite_elt_type_ -- class FiniteEltType : is

note top of FiniteEltTypeInLocalVariationalOperator {
    This class is intended to be used in a LocalVariationalOperator context.
    It is really a reference to a FiniteEltType with several few indexes
    stored to ease navigation within the local matrices and vectors.
    An advanced user doesn't have to consider it: it is used only when 
    a new local operator is built.
    
}

note right of FiniteEltType : This class is intended to be used in the FiniteEltSpace context.

note right of FiniteEltSpace : The FiniteEltSpace currently featured in HappyHeart, before inclusion of the GodOfDof.

@enduml
