@startuml

class GeometricMeshRegion {
    + GeometricMeshRegion(mpi)
    + Init(mesh_file, format)
    + ShrinkToProcessorWise(processor_wise_geo_element)
    + Write(mesh_file)
    - BuildInterface()
    .. A Coords is a point read in input file, it might refer to a vertex, an edge, etc ..
    - coords_list_
    - geometric_element_list_
    - dimension_
    - label_list_
    .. Format used in input; it doesn't mean another can't be used in output ..
    - format_
}



class Coords {
    + Several constructors
    - point_
    - index_
    - mesh_label_
    - interface_nature_
}

class "coords_list_" as gmr_coords_list << (A,orchid) >>


namespace Private {
class GeometricElementList {
    + GeometricElementList() = default
    + Init(unsort_geometric_element_list)
    + GetLocationInElementList(geometric_reference_element)
    + GetLocationInElementList(geometric_reference_element, mesh_label_index)
    - data_
    - geometric_element_lookup_helper_
}
}

note "Stores all the geometric elements sort so that different required lookup are all efficient." as noteGeometricElementList
noteGeometricElementList .. Private.GeometricElementList

class geometric_element_list_ << (A,orchid) >>

'Because Interface seems to be a keywork of PlantUML'
abstract class "Interface" as Interface2 {
    + Interface(reference_interface, coords_list)
    + {abstract} GetNature()
    - coords_list_
    - coords_of_node_
    - id_
}

class "coords_list_" as interface_coords_list << (A,orchid) >>


class Vertex {
    + Vertex(...)
}

class Edge {
    + Edge(...)
}

class Face {
    + Face(...)
}

class Volume {
    + Volume(...)    
}


class GeometricElement {
    .. Described more completely on its dedicated UML ..
    - vertex_list_
    - edge_list_
    - face_list_
    - volume_
}

class vertex_list_ << (A,orchid) >>
class edge_list_ << (A,orchid) >>
class face_list_ << (A,orchid) >>
class volume_ << (A,orchid) >>


GeometricMeshRegion o.. gmr_coords_list
gmr_coords_list "1" *-- "many" Coords 
Interface2 o.. interface_coords_list
interface_coords_list "1" *-- "few" Coords 

GeometricMeshRegion o.. geometric_element_list_
geometric_element_list_ -- Private.GeometricElementList : is

Interface2 <|-- Vertex
Interface2 <|-- Edge
Interface2 <|-- Face
Interface2 <|-- Volume


GeometricElement o.. vertex_list_
GeometricElement o.. edge_list_
GeometricElement o.. face_list_
GeometricElement o.. volume_

vertex_list_ "1" *-- "few" Vertex 
edge_list_ "1" *-- "few" Edge 
face_list_ "1" *-- "few" Face 
volume_ "1" *-- "1" Volume 

@enduml
