'''
Update the table of contents of all markdown files

'''

import os
import subprocess

def UpdateMarkdownTOC(directory, markdown_toc = "markdown-toc"):
    '''Go through all the files in 'directory' and apply the markdown-toc script (from https://github.com/jonschlinkert/markdown-toc) to it.
    
    This script will add or update the table of contents whenever <!-- toc --> is met.

    \param[in] directory All markdown files in this directory will be processed.
    \param[in] markdown_toc Path to markdown-toc executable.
    
    This assumes markdown-toc is installed on your system; to make it work on my mac I defined an alias to the cli.js file installed following the npm command they give on github.
    '''
    for root, dirs, files in os.walk(directory):        
        for myfile in files:
            if not myfile.endswith(".md"):
                continue
                
            markdown_file = os.path.join(root, myfile)
                
            subprocess.Popen("{} -i {}".format(markdown_toc, markdown_file), shell = True).communicate()
            print("{} processed".format(markdown_file))
            
    



if __name__ == "__main__":    
    UpdateMarkdownTOC('{}/Codes/MoReFEM/CoreLibrary'.format(os.path.expanduser('~')))


