import os


def SubstituteEnvVariable(directory, begin_separator = "${", end_separator = "}"):
    """Replace environment variables by their value (which must be properly defined).

    Syntax to provide is:
        ${ENVIRONMENT_VARIABLE}
    where {ENVIRONMENT_VARIABLE} is the name of the environment variable (e.g. ${HOME}).

    Syntax may actually be customized with begin_separator and end_separator parameters.

    There is an assumption here it is a path we are figuring out (os.path.normpath is used).

    \return Name of the directory with the value of the environment variable substituted.
    """
    begin_separator = "${"
    end_separator = "}"

    while begin_separator in directory:
        splitted = directory.split(begin_separator)
    
        separated_list = []
    
        for item in splitted:
            if item:
                assert(end_separator in item)           
                splitted_2 = item.split(end_separator)           
                assert(len(splitted_2) == 2)
       
                separated_list.append(os.environ[splitted_2[0]])
                separated_list.append(splitted_2[1])
    
        directory = os.path.normpath("/".join(separated_list))
    
    return directory