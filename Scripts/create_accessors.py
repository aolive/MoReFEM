
def FromDataAttrToMethodName(data_attr):
    """Convert a data attribute to a method name.

    Following the coding style, we get for instance:

    mass_matrix -> MassMatrix.
    """
    assert(len(data_attr) > 0)

    is_prev_char_underscore = True # for conveniency
    char_list = []

    for char in data_attr:
        if is_prev_char_underscore:
            assert(char is not '_'), "We assume neither double underscores nor underscore beginning in data attributes."
            char_list.append(char.upper())
            is_prev_char_underscore = False
        else:
            if char is '_':
                is_prev_char_underscore = True
            else:
                char_list.append(char)
            
    return ''.join(char_list)
    
    
def IsTemplate(template):
    
    if template:
        print("template{template}".format(template = template))


class CreateAccessors:
    
    def __init__(self, template_args, class_name, typename, attribute_name, dox_comment, is_constant, Nchar_max = 100):
        
        
        self._typename = typename
        
        if not attribute_name.endswith('_'):
            attribute_name += "_"
        self._attribute_name = attribute_name
        
        self._Nchar_max = Nchar_max
        self._is_constant = is_constant
        self._dox_comment = dox_comment
        self._method_name = FromDataAttrToMethodName(attribute_name)
        self._accessor_dox = "{0}{1}".format(dox_comment[0].lower(), dox_comment[1:])

        
        self.__InitTemplateArgs(template_args)
        self.__InitClassName(class_name)
            
        # Call the methods that print the relevant results on screen.
        self._DataAttribute()

        print("=========================\n")
        self._ConstantAccessorDeclaration()
        
        if not self._is_constant:
            self._NonConstantAccessorDeclaration()
        
        print("=========================\n")
        
        self._ConstantAccessorDefinition()
        
        if not self._is_constant:
            self._NonConstantAccessorDefinition()
                                                                        
        print("=========================\n")
       
       
    def __InitClassName(self, class_name):
        """Init gthe class name as it will appear in accessors definition.
        
        It is the raw class name for non template classes and the class name plus template arguments for template classes."""
        if not self._IsTemplateClass():
            self._class_name = class_name
        else:
            
            args = []
            
            for item in self._template_args:
                item = item.strip()
                splitted = item.split(' ')
                assert(len(splitted) > 1)
                # assert(len(splitted) == 2) Might be more for template template parameters!
                args.append(splitted[-1])
            
            self._class_name = "{raw}<{args}>".format(raw = class_name, args = ", ".join(args))

        

    def __InitTemplateArgs(self, template_args):
        """Interpret template arguments if some were given."""
        
        self._template_args = False
        if template_args:
            
            
            template_args = template_args.strip()
            assert(template_args[0] is '<'), "Template args argument should be a string delimited by <>."
            assert(template_args[-1] is '>'), "Template args argument should be a string delimited by <>."
            
            # Strip the delimiting characters.
            template_args = template_args[1:-1]
            
            # Split the different template arguments.
            self._template_args = template_args.split(",")
            
            self._template_args = [x.strip() for x in self._template_args]
            
    
    def _IsTemplateClass(self):
        if not self._template_args:
            return False
            
        return True
        
        
    def _PrintComment(self, dox_comment, shift_first_line = 7,  with_delimiters = True):
        """Prepare Doxygen comment, which might be splitted on several lines if it is too long.
        
        \param[in] with_delimiters Whether C/C++ comment delimiters should be added or not.
        \param[in] shift_first_line To make relatively even lines, this shift might be applied
        to firect line, to take into accoutn for instance the width of \\brief or \param[in].        
        """
        size_max = self._Nchar_max
        
        if len(dox_comment) < size_max:
            if with_delimiters:
                return "//! {0}".format(dox_comment)
            else:
                return dox_comment
            
        lines = []
        
        current_size_max = size_max - shift_first_line
        
        while len(dox_comment) >= current_size_max:

            pos_eol = current_size_max
            current_size_max = size_max
        
            while dox_comment[pos_eol] != ' ':
                assert pos_eol > 0
                pos_eol -= 1
            
            lines.append(dox_comment[:pos_eol])
            dox_comment = dox_comment[pos_eol+1:]
            
        if dox_comment:
            lines.append(dox_comment)
        
        
        if with_delimiters:
            ret = "\n/*!\n * \\brief " + "\n * ".join(lines) + "\n */"
        else:
            ret = "\n * ".join(lines) + "\n"
        
        return ret
        
        
        
        
        
        
    def _TemplateHeader(self):
        """Generates the template header in definition of the accessor."""
        assert(self._IsTemplateClass()), "Should not be called for non template classes."
        
        if (len(self._template_args) == 1):
            print("template<{0}>".format(self._template_args[0]))
        else:
            print("template\n<")
            for arg in self._template_args[:-1]:
                print("\t{0},".format(arg))
            print("\t{0}\n>".format(self._template_args[-1]))
        
        
    
    def _ConstantAccessorDeclaration(self):
        """Write the constant accessor declaration."""
        print(self._PrintComment("Constant accessor to the {dox}".format(dox = self._accessor_dox)))
        print("const {type}& Get{method}() const noexcept;\n".format(type = self._typename, method = self._method_name))
        
    
    def _ConstantAccessorDefinition(self):
        """Write the constant accessor definition.
        
        IMPORTANT: It is assumed that derived class correctly define _ConstantAccessorDefinitionContent().
        """
        if self._IsTemplateClass():
            self._TemplateHeader()
        print("inline const {type}& {class_name}::Get{method}() const noexcept".format(type = self._typename, \
                                                                                   method = self._method_name, \
                                                                                   class_name = self._class_name)
                                                                                   )
                                                                                   
        self._ConstantAccessorDefinitionContent()                                                                                   
                
    def _NonConstantAccessorDeclaration(self):
        """Write the non constant accessor declaration."""
        print(self._PrintComment("Non constant accessor to the {dox}".format(dox = self._accessor_dox)))
        print("{type}& GetNonCst{method}() noexcept;\n".format(type = self._typename, method = self._method_name))                                                        
                                                                                   
    def _NonConstantAccessorDefinition(self):
        """Write the definition of the non-constant accessor.
        """
        if self._IsTemplateClass():
            self._TemplateHeader()

        print("inline {type}& {class_name}::GetNonCst{method}() noexcept".format(type = self._typename, \
                                                                                 method = self._method_name, \
                                                                                 class_name = self._class_name))

        print("{{\n\treturn const_cast<{type}&>(Get{method}());\n}}\n".format(method = self._method_name, \
                                                                            type = self._typename))
                                                                            
    def _DataAttribute(self):
        """Print the data attribute and its Doxygen comment.
        
        IMPORTANT: It is assumed that derived class correctly define _DataAttributeStorage().
        """
        print(self._PrintComment(self._dox_comment))
        self._DataAttributeStorage()
        
        
    

class SmartPtr(CreateAccessors):
    """Create quickly the boilerplate code related to data attribute and its accessors when data attribute is a smart pointer.
    
    
    """
    def __init__(self, template_args, class_name, typename, smartptr, attribute_name, dox_comment, Nchar_max = 100):
        
        self._smartptr = smartptr
        is_constant = smartptr.startswith('const')
                
        CreateAccessors.__init__(self, template_args, class_name, typename, attribute_name, dox_comment, Nchar_max = Nchar_max, is_constant = is_constant)
        
        local = self._attribute_name[:-1]
        print(" * \param[in] {local} {dox}\n".format(local = local, dox = self._PrintComment(dox_comment, with_delimiters = False, shift_first_line = len(local) + 7)))

        print("{attr} = std::make_unique<{type}>();".format(attr = self._attribute_name, \
                                                              type = typename))
                                                              
    def _ConstantAccessorDefinitionContent(self):
        
        print("{{\n\tassert(!(!{attr}));".format(attr = self._attribute_name))    
        print("\treturn *{attr};".format(attr = self._attribute_name))                                                                    
        print("}\n\n")                                                                  
        
    
    def _DataAttributeStorage(self):
        print("{type}::{ptr} {attr} = nullptr;\n".format(type = self._typename, ptr = self._smartptr, attr = self._attribute_name))



class Reference(CreateAccessors):
    """Create quickly the boilerplate code related to data attribute and its accessors when data attribute is a reference.
    
    
    """
    def __init__(self, template_args, class_name, typename, attribute_name, dox_comment, is_const, Nchar_max = 100):
        
        self._is_constant = is_const
        self._const_preffix = self._is_constant and "const " or ''
        CreateAccessors.__init__(self, template_args, class_name, typename, attribute_name, dox_comment, is_const, Nchar_max)
    
        local = self._attribute_name[:-1]
        print("* \param[in] {local} {dox}\n".format(local = local, dox =  self._PrintComment(dox_comment, with_delimiters = False, shift_first_line = len(local))))
        print("{const}{type}& {local},\n".format(const = self._const_preffix, type = typename, local = local))
        print("{attr}({local}),".format(local = local, attr = self._attribute_name))
    

    def _DataAttributeStorage(self):
        print("{const}{type}& {attr};\n".format(const = self._const_preffix, type = self._typename, attr = self._attribute_name))
        
        
    def _ConstantAccessorDefinitionContent(self):
        
        print("{{\n\treturn {attr};".format(attr = self._attribute_name))                                                                   
        print("}\n\n")


if __name__ == "__main__":
   
    
    # Please do not use the current script directly; rather use another program which calls the above function.
    # The idea is to avoid pointless git changes/conflicts due to the use of the helper file...

    # Rather create a file named my_create_accessors.py in same directory with the following content:
    #
    # import create_accessors
    # class_name = "ExplicitStepVariationalFormulation"
    # typename = "GlobalMatrix"
    # smartptr = "unique_ptr"
    # attribute_name = "mass_matrix"
    # dox_comment = "Matrix into which the mass operator is assembled."
    #
    # create_accessors.SmartPtr(class_name, typename, smartptr, attribute_name, dox_comment)
    # or    
    # create_accessors.Reference(class_name = class_name,  \
    #                       typename = typename, 
    #                       attribute_name = attribute_name, 
    #                       dox_comment = dox_comment, 
    #                       is_const = False)
    exit()


