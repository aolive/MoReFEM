import math
import numpy as np

class MatrixContent:
    
    
    def __init__(self, Nrow, Ncol):
        self.__Nrow = Nrow
        self.__Ncol = Ncol        
        self.__storage = np.zeros([self.__Nrow + 1, self.__Ncol + 1])
        
        
    def Add(self, row_index, col_index, value):
        assert(row_index <= self.__Nrow)
        assert(col_index <= self.__Ncol)        
        self.__storage[row_index, col_index] = value
        
    def Get(self, row_index, col_index):
        assert(row_index <= self.__Nrow)
        assert(col_index <= self.__Ncol)
        
        return self.__storage[row_index, col_index] 
        
    def GetSize(self):
        return (self.__Nrow, self.__Ncol)


def LoopOverCSRContent(FILE, matrix_content, ignore_content_list):
    """Common part of reading Freefem and Matlab formats: read the lines that must be (iCSR, jCSR, value) and fill a MatrixContent.
    
    \param[in] ignore_content_list In this list, put all strings that indicates the line must be skipped.
    For instance, for MoReFEM skip line which contains 'spconvert(zzz)' by indicating  ignore_content_list = ('spconvert(zzz)', )
    """
    
    
    for line in FILE:
        line = line.strip(" \n];['")
        
        if not line:
            continue
            
        skip = False
            
        for ignored_content in ignore_content_list:
            if ignored_content in line:
                skip = True
                
        if  skip:
            continue
        
        splitted = line.split(' ')
        splitted = [item for item in splitted if item]
        
        if not (len(splitted) == 3):
            print line, splitted
        
        matrix_content.Add(int(splitted[0]), float(splitted[1]), float(splitted[2]))


def ReadRenumberedMoReFEMMatrix(file):
    """
    Returns the content of the matrix in form of a numpy array.
    """
    FILE = open(file)

    FILE.readline()
    FILE.readline()

    size_line = FILE.readline()
    splitted = size_line.split("Size =")
    assert(len(splitted) == 2)
    
    size_pair = splitted[1].strip(" \n").split(' ')
    assert(len(size_pair) == 2)
    
    (Nrow, Ncol) = (int(size_pair[0]), int(size_pair[1]))
    
    matrix_content = MatrixContent(Nrow, Ncol)
    
    FILE.readline()
    FILE.readline()
    FILE.readline()
    
    LoopOverCSRContent(FILE, matrix_content, ignore_content_list = ('spconvert(zzz)', ))
        
    return matrix_content
    
    
def ReadFreefemMatrix(file):
    """
    Returns the content of the matrix in form of a numpy array.
    """
    FILE = open(file)
    
    FILE.readline()
    FILE.readline()
    FILE.readline()
    
    size_line = FILE.readline()
    
    splitted = size_line.split(' ')
    assert(len(splitted) > 3)
    (Nrow, Ncol) = (int(splitted[0]), int(splitted[1]))
    
    matrix_content = MatrixContent(Nrow, Ncol)
    
    LoopOverCSRContent(FILE, matrix_content, ignore_content_list = [])
        
    return matrix_content


def AreAlmostEqual(x, y, precision = 1.e-6):
    
    if not x:
        if not y:
            return True
        else:
            return False
            
    if math.fabs(x - y) < precision:
        return True
        
    return False
    


def ValidateRenumbering(renumbered_morefem_matrix, freefem_matrix, precision):
    """The point of this function is to check PostProcessing MatrixConversion did its job fine.
    
    It opens up both files and check the value are in agreement; the only discrepancies expected are:
    - There is a shift of 1 in indexes: Freefem starts at 1, and Petsc at 0.
    - There might be precision error as Petsc tends to shorten its numerical values...
    
    \param[in] renumbered_morefem_matrix MoReFEM matrix after renumbering, written in Matlab format 
    \param[in] freefem_matrix Freefem matrix.
    """
    hh_matrix = ReadRenumberedMoReFEMMatrix(renumbered_morefem_matrix)
    ff_matrix = ReadFreefemMatrix(freefem_matrix)
    
    if hh_matrix.GetSize() != ff_matrix.GetSize():
        raise Exception("Shapes are not compatible: {0} for MoReFEM and {1} for Freefem.".format(hh_matrix.GetSize(), ff_matrix.GetSize()))
        
    (Nrow, Ncol) = hh_matrix.GetSize()
    
    is_valid = True

    for i in range(1, Nrow + 1):
        for j in range(1, Ncol + 1):
            if not AreAlmostEqual(hh_matrix.Get(i, j), ff_matrix.Get(i, j), precision):
                is_valid = False
                print "Discrepancy ({0}, {1})-> {2} {3}".format(i, j, hh_matrix.Get(i, j), ff_matrix.Get(i, j))
            elif ff_matrix.Get(i, j) != 0:
                print "Non zero agreement ({0}, {1})".format(i, j)                
            # else:
            #     print "Agreement ({0}, {1})".format(i, j)

    if is_valid:
        print "Both matrices are equal (with a tolerance of {precision})".format(precision=precision)
        
    
    
if __name__ == "__main__":
    ValidateRenumbering('/Volumes/Data/sebastien/Sandbox/Poromechanics/matrix_in_freefem_numbering.m', \
#                        freefem_matrix = '/Users/sebastien/Desktop/Poro/freefem_t33_call_1_time_0_it_1_internal_it_1.txt',                                      
                        freefem_matrix = '/Volumes/Data/sebastien/Sandbox/Poromechanics/freefem_full_matrix_call_1_time_0_it_1_internal_it_1.txt',
                        precision = 1.e-6)
                        
   