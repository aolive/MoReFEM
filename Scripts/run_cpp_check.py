import os
import subprocess


if __name__ == "__main__":

    cmd = \
        [
            "/Users/Shared/Software/Cppcheck/cppcheck",
            "--std=c++11",
            "--enable=all",
            "-f",
            "-q",
            "{0}/workspace/Doxygen/Sources".format(os.environ["HOME"]),
            "-i",
            "{0}/workspace/Doxygen/Sources/ThirdParty/Source".format(os.environ["HOME"]),
            "-I{0}/workspace/Doxygen/Sources".format(os.environ["HOME"]),
            "-DF77_NO_UNDER_SCORE",
            "-DTRANSMESH"
            ]
        

    FILE=open('cpp_check_report.txt', 'w')

    subprocess.Popen(cmd, shell=False, stdout=FILE, stderr=FILE).communicate()

    print("Output written in cpp_check_report.txt")