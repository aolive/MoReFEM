import os
import shutil

folder = "XCodeTemplates"

for root, dirs, files in os.walk(folder):
    for myfile in files:
    
        if myfile.endswith(".hpp") or myfile.endswith(".hxx") or myfile.endswith(".cpp"):
    
            filename = os.path.join(root, myfile)
    
            FILE_in = open(filename)
            tmp = "{0}.tmp".format(filename)
            FILE_out = open(tmp, 'w')
    
            FILE_out.write("//! \\file \n//\n")
    
            for line in FILE_in:
                FILE_out.write(line)
            
            FILE_in.close()
            FILE_out.close()
        
            os.remove(filename)
            shutil.copyfile(tmp, filename)
    


