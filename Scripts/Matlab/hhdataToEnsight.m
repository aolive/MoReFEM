function [vector_out, vector_in] = hhdataToEnsight(path_hh, mesh_number, numbering_subset_number, vector_in_name_file, dimension)

vector_in = load(vector_in_name_file);

[interfaces_indices, dof_infos_indices] = LoadInterfacesAndDofsInfosMoReFEM(path_hh, mesh_number, numbering_subset_number);

indices_out = CreateIndicesOutMoReFEMToMatlab(interfaces_indices, dof_infos_indices, dimension);

vector_out = VectorRenumbering(vector_in, indices_out);

end