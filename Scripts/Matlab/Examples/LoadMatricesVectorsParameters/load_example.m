%% Script
clear all; clc;

addpath('~/Codes/MoReFEM/Scripts/Matlab/');

% Path of the output_directory in lua file. 
path_hh_results = '~/Codes/MoReFEM/Scripts/Matlab/Examples/LoadMatricesVectorsParameters/Results/';
% Path of where the matrices, vectors or parameters have been printed ie
% 'PATH' in Instructions.
path_hh = '~/Codes/MoReFEM/Scripts/Matlab/Examples/LoadMatricesVectorsParameters/';

% MoReFEM files. 
matrix_name_file_hh = 'matrix.m';
vector_name_file_hh = 'vector.hhdata';
parameter_name_file_hh = 'scalar_parameter_at_quadrature_point_parameter.dat';

% Load once and for all indices_out for renumbering MoReFEM matrices and vectors.
mesh_number = 1;
numbering_subset_number = 1;
dimension = 3;
[interfaces_indices, dof_infos_indices] = LoadInterfacesAndDofsInfosMoReFEM(path_hh_results, mesh_number, numbering_subset_number);
indices_out = CreateIndicesOutMoReFEMToMatlab(interfaces_indices, dof_infos_indices, dimension);

% Compute actifs_dofs for HH to reduce the matrices.
dofs_bc_name_file = [path_hh 'dofs_bc.dat'];
[dofs_BC_indices_renumbering_in_matlab, indices_BC_hh] = MoReFEMIndicesToMatlabIndices(path_hh_results, mesh_number, numbering_subset_number, dofs_bc_name_file, dimension);

indices_dofs_actif_hh = RemoveInactiveDofs(indices_out, dofs_BC_indices_renumbering_in_matlab);

% Load HH Matrix
Mat_in = LoadMatrixIn([path_hh matrix_name_file_hh]);
% Renumbering the matrix with the same numbering as the original mesh.
Mat_out = MatrixRenumbering(Mat_in, indices_out);
        
% Load HH Vector.
vector_in = load([path_hh vector_name_file_hh]);
% Renumbering the vector with the same numbering as the original mesh.
vector_out = VectorRenumbering(vector_in, indices_out);

% Load HH Scalar Parameter At Quadrature Point.
parameter_hh = LoadParameterMoReFEM(path_hh, parameter_name_file_hh, 'tetrahedron');

% Load Energy
[energy_hh, time] = LoadEnergyMoReFEM(path_hh, 'Energy');

% Above interface files and dofs_infos are loaded once and for all, hence
% doing a loop over a big number of matrices is possible with just a simple
% numbering of the files name. If you want to load only one matrix or
% vector you can do it by just calling the functions below, but this time
% the files previously mentionned will be loaded each time, it is just all
% the scripts to load a matrix put in one.
% If you want to
[Mat_out_2, indices_out_2, Mat_in_2] = MoReFEMMatrixRenumbering(path_hh_results, mesh_number, numbering_subset_number, [path_hh matrix_name_file_hh], dimension);

[vector_out_2, vector_in_2] = hhdataToEnsight(path_hh_results, mesh_number, numbering_subset_number, [path_hh vector_name_file_hh], dimension);


