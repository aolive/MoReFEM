//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#ifndef _____PROJECTNAMEASIDENTIFIER_______FILEBASENAMEASIDENTIFIER_____HPP
# define _____PROJECTNAMEASIDENTIFIER_______FILEBASENAMEASIDENTIFIER_____HPP

# include "Core/InputParameter/Crtp/Section.hpp"

# include "Core/InputParameter/Parameter/Impl/ParameterUsualDescription.hpp"


namespace MoReFEM
{
    
    
    namespace InputParameter
    {
        
        
        //! \copydoc doxygen_hide_core_input_parameter_list_section
        struct ___FILEBASENAMEASIDENTIFIER___ : public Crtp::Section<___FILEBASENAMEASIDENTIFIER___, NoEnclosingSection>
        {
            
            
            
            //! Convenient alias.
            using self = ___FILEBASENAMEASIDENTIFIER___;
            
            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;
            friend parent;
            
            
            /*!
             * \brief Return the name of the section in the input parameter.
             *
             */
            static const std::string& GetName();
            
            
            
            /*!
             * \brief Choose how is described the viscosity (through a scalar, a function, etc...)
             */
            struct Nature : public Crtp::InputParameter<Nature, self, Impl::Nature::storage_type>,
            public Impl::Nature
            { };
            
            
            
            /*!
             * \brief Scalar value. Irrelevant if nature is not scalar.
             */
            struct Scalar : public Crtp::InputParameter<Scalar, self, Impl::Scalar::storage_type>,
            public Impl::Scalar
            { };
            
            
            /*!
             * \brief Function that determines viscosity value. Irrelevant if nature is not lua_function.
             */
            struct LuaFunction : public Crtp::InputParameter<LuaFunction, self, Impl::LuaFunction::storage_type>,
            public Impl::LuaFunction
            { };
            
            
            
            /*!
             * \brief Piecewise Constant domain index.
             */
            struct PiecewiseConstantByDomainId : public Crtp::InputParameter<PiecewiseConstantByDomainId, self, Impl::PiecewiseConstantByDomainId::storage_type>,
            public Impl::PiecewiseConstantByDomainId
            { };
            
            
            /*!
             * \brief Piecewise Constant value by domain.
             */
            struct PiecewiseConstantByDomainValue : public Crtp::InputParameter<PiecewiseConstantByDomainValue, self, Impl::PiecewiseConstantByDomainValue::storage_type>,
            public Impl::PiecewiseConstantByDomainValue
            { };
            
            
            //! Alias to the tuple of structs.
            using section_content_type = std::tuple
            <
                Nature,
                Scalar,
                LuaFunction,
                PiecewiseConstantByDomainId,
                PiecewiseConstantByDomainValue
            >;
            
            
        private:
            
            //! Content of the section.
            section_content_type section_content_;
            
            
        }; // struct ___FILEBASENAMEASIDENTIFIER___
        
        
    } // namespace InputParameter
    
    
} // namespace MoReFEM


# include "Core/InputParameter/Parameter/___FILEBASENAME___.hxx"


#endif /* defined(_____PROJECTNAMEASIDENTIFIER_______FILEBASENAMEASIDENTIFIER_____HPP) */
