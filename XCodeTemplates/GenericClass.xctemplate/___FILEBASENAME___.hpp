//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#ifndef _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP
# define _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP

# include <memory>
# include <vector>


namespace MoReFEM
{


    class ___FILEBASENAMEASIDENTIFIER___
    {
        
    public:
        
        //! \copydoc doxygen_hide_alias_self
        // \TODO This might seem a bit dumb but is actually very convenient for template classes.
        using self = ___FILEBASENAMEASIDENTIFIER___;
    
        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;
    
        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;
    
    public:
    
        /// \name Special members.
        ///@{
    
        //! Constructor.
        explicit ___FILEBASENAMEASIDENTIFIER___() = default;
    
        //! Destructor.
        ~___FILEBASENAMEASIDENTIFIER___() = default;
    
        //! Copy constructor.
        ___FILEBASENAMEASIDENTIFIER___(const ___FILEBASENAMEASIDENTIFIER___&) = delete;
    
        //! Move constructor.
        ___FILEBASENAMEASIDENTIFIER___(___FILEBASENAMEASIDENTIFIER___&&) = delete;
    
        //! Copy affectation.
        ___FILEBASENAMEASIDENTIFIER___& operator=(const ___FILEBASENAMEASIDENTIFIER___&) = delete;
        
        //! Move affectation.
        ___FILEBASENAMEASIDENTIFIER___& operator=(___FILEBASENAMEASIDENTIFIER___&&) = delete;
    
        ///@}
    
    private:
    
    
    
    };


} // namespace MoReFEM


# include "___VARIABLE_relativePath___/___FILEBASENAME___.hxx"


#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP) */
