//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "ModelInstances/___VARIABLE_groupName:identifier___/Model.hpp"
#include "ModelInstances/___VARIABLE_groupName:identifier___/InputParameterList.hpp"


using namespace MoReFEM;


int main(int argc, char** argv)
{
    
    //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = ___VARIABLE_problemName:identifier___NS::InputParameterList;
    
    try
    {
        MoReFEMData<InputParameterList> morefem_data(argc, argv);
                
        const auto& input_parameter_data = morefem_data.GetInputParameterList();
        const auto& mpi = morefem_data.GetMpi();
        
        try
        {
            ___VARIABLE_problemName:identifier___NS::Model model(morefem_data);
            model.Run();
            
            input_parameter_data.PrintUnused(std::cout);
        }
        catch(const std::exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());
        }
        catch(Seldon::Error& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.What());
        }
    }
    catch(const std::exception& e)
    {
        std::ostringstream oconv;
        oconv << "Exception caught from MoReFEMData<InputParameterList>: " << e.what() << std::endl;
        
        std::cout << oconv.str();
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}

