//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#ifndef _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HXX
#define _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HXX


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {
        
        
        // \TODO Complete if needed by supplementary arguments (the whole point of not using directly parent
        // is to define clearly what is the expected signature for the operator; parent gets template variadic argument.
        template<class LinearAlgebraTupleT>
        inline void ___FILEBASENAMEASIDENTIFIER___
        ::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple, const Domain& domain) const
        {
            return parent::template AssembleImpl<>(std::move(linear_algebra_tuple), domain);
        }
        
        
        
    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HXX) */
