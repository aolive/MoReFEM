//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#include "ModelInstances/___VARIABLE_relativePath___/___FILEBASENAME___.hpp"


namespace MoReFEM
{
    
    
    namespace ___VARIABLE_problemName:identifier___NS
    {


        VariationalFormulation::VariationalFormulation(const morefem_data_type& morefem_data,
                                                       const NumberingSubset& numbering_subset1,
                                                       const NumberingSubset& numbering_subset2,
                                                       const TimeManager& time_manager,
                                                       const GodOfDof& god_of_dof,
                                                       DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list)
        : parent(mpi,
                 time_manager,
                 god_of_dof,
                 std::move(boundary_condition_list)),
        numbering_subset1_(numbering_subset1), // TODO just to illustrate here: it's up to you to create attributes of type
        numbering_subset2_(numbering_subset2), // const NumberingSubset& in the place indicated in the hpp file.
        { }
      

        
        void VariationalFormulation::AllocateMatricesAndVectors()
        {
            // TODO Set here the pattern of all global matrices and vectors.
            // Typically it should be something like:
            //
            // \code
            // const auto& numbering_subset1 = GetNumberingSubset1(); // accessor you have to defined in new class; name it as appropriate!
            // const auto& numbering_subset2 = GetNumberingSubset2();
            //
            // // Allocate the relevant block matrix. Below an example for Stokes model, in which only upper blocks
            // // in the matrix need to be allocated.
            // parent::AllocateSystemMatrix(numbering_subset1, numbering_subset1);
            // parent::AllocateSystemMatrix(numbering_subset1, numbering_subset2);
            //
            // // Same for vector; system_rhs and system_solution are allocated here.
            // parent::AllocateSystemVector(numbering_subset);
            //
            // Access to the newly allocated matrix and vectors.
            // const auto& system_matrix = GetSystemMatrix(numbering_subset1, numbering_subset1);
            // const auto& system_rhs = GetSystemRhs(numbering_subset1);
            //
            // // Allocate your problem specific matrices and vectors, by coping structure of the ones allocated above.
            // GetNonCstVectorCurrentVolumicSource().CompleteCopy(system_rhs, __FILE__, __LINE__);
            // ...
            //
            // \endcode
        }


    } // namespace ___VARIABLE_problemName:identifier___NS


} // namespace MoReFEM
