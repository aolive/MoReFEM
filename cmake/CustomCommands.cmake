include(TestCXXAcceptsFlag)


# add a compiler flag only if it is accepted
macro(add_cxx_compiler_flag _flag)
  string(REPLACE "-" "_" _flag_var ${_flag})
  check_cxx_compiler_flag("${_flag}" CXX_COMPILER_${_flag_var}_OK)

  if(CXX_COMPILER_${_flag_var}_OK)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${_flag}")
  endif()
endmacro()


macro(add_c_compiler_flag _flag)
  string(REPLACE "-" "_" _flag_var ${_flag})
  check_c_compiler_flag("${_flag}" C_COMPILER_${_flag_var}_OK)
  if(C_COMPILER_${_flag_var}_OK)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${_flag}")
  endif()
endmacro()


##########################################################################################
#
# Find a library and store it in _macro; if not a message error is issued.
    
# Example: morefem_find_library(LIB_OPEN_MPI "mpi" ${OPEN_MPI_LIB_DIR} "Openmpi")    
# _macro Name in which the library is accessible within CMake.
# _libnames Name or names under which the libraries might be found in the paths.
# _dir Directory into which the library should be found. When this macro is called, it is usually with a path set in Paths.cmake.
# _name Name of the library, for the error message. 
#
##########################################################################################
function(morefem_find_library _macro _libnames _dir _name)
    
    find_library(${_macro} ${_libnames} PATHS ${_dir} NO_DEFAULT_PATH)

    if (NOT ${_macro})
         message(FATAL_ERROR "${_name} library not found in path ${_dir}")
    endif()
endfunction()    


# ##########################################################################################
#
# Set the alias related to a module depending on the value of BUILD_MOREFEM_UNIQUE_LIBRARY:
# - If True, the alias is set to morefem, the unique library into which all the code is built.
# - If False, a new (empty at the moment) library is built and the alias points to this library.
# For instance morefem_library_module(MOREFEM_UTILITIES morefem_utilities) will create morefem_utilities and set MOREFEM_UTILITIES to point to this library.
# \param[out] _alias Alias to the library, e.g. MOREFEM_UTILITIES.
# \param[in] module Module considered; if BUILD_MOREFEM_UNIQUE_LIBRARY is False the module library will use that name.
# If True it is not used.
    #
# ##########################################################################################
function(morefem_library_module _alias _module)
    if (BUILD_MOREFEM_UNIQUE_LIBRARY)
        set(${_alias} MoReFEM PARENT_SCOPE)
    else()
        add_library(${_module} ${LIBRARY_TYPE} "")
        set(${_alias} ${_module} PARENT_SCOPE)
    endif()
endfunction()


# ##########################################################################################
# A wrapper over install() command which performs the following additional tasks:
# - If the target is an executable, call set_target_properties() to properly define RPATH for the target. This is r
# required at least for shared libraries on macOS.
# - Install the targets in the MoReFEM installation directories.  
# ##########################################################################################
include(GenerateExportHeader)

function(morefem_install)
    foreach(target ${ARGN})
        
        get_target_property(target_type ${target} TYPE)
        if (target_type STREQUAL "EXECUTABLE")
            if (APPLE)
                # Additional step to make shared library work on macOS; see https://gist.github.com/robertmaynard/5750737
                set_target_properties(${target} PROPERTIES INSTALL_RPATH "@loader_path/../lib")
            endif()
        elseif()
            generate_export_header(${target})
        endif ()
        
        install(TARGETS ${target}
                EXPORT "${target}TARGETS"
                RUNTIME DESTINATION ${MOREFEM_INSTALL_DIR_EXE}
                LIBRARY DESTINATION ${MOREFEM_INSTALL_DIR_LIB}
                ARCHIVE DESTINATION ${MOREFEM_INSTALL_DIR_LIB} 
                INCLUDES DESTINATION ${MOREFEM_INSTALL_DIR_INCL}) 
                
        
    endforeach()
endfunction()



# ##########################################################################################
#
# Set the alias related to a module depending on the value of BUILD_MOREFEM_UNIQUE_LIBRARY:
# - If True, the alias is set to morefem, the unique library into which all the code is built.
# - If False, a new (empty at the moment) library is built and the alias points to this library.
# For instance morefem_library_module(MOREFEM_UTILITIES morefem_utilities) will create morefem_utilities and set MOREFEM_UTILITIES to point to this library.
# \param[out] _alias Alias to the library, e.g. MOREFEM_UTILITIES.
# \param[in] module Module considered; if BUILD_MOREFEM_UNIQUE_LIBRARY is False the module library will use that name.
# If True it is not used.
    #
# ##########################################################################################
function(morefem_library_module _alias _module)
    if (BUILD_MOREFEM_UNIQUE_LIBRARY)
        set(${_alias} MoReFEM PARENT_SCOPE)
    else()
        add_library(${_module} ${LIBRARY_TYPE} "")
        set(${_alias} ${_module} PARENT_SCOPE)
    endif()
endfunction()


# ##########################################################################################
# There seems to be a way to get all the source files related to a given target, but what I 
# want when preparing installation part is just the list of header files, which extension is
# in my case .h, .hpp or .hxx.
# This function fulfills this need.
# \param[in] _target The target for which header files are sought.
# \param[out] _list The list of header files found.
# ##########################################################################################
function(extract_header_files _target _list _include_dir)
    get_property(sources TARGET ${_target} PROPERTY SOURCES)
    
    list(APPEND ret "")
    

    
    foreach(source in ${sources})
        string(REGEX MATCH .h$ h ${source})
        string(REGEX MATCH .hpp$ hpp ${source})
        string(REGEX MATCH .hxx$ hxx ${source})  

        if(h OR hpp OR hxx)
            
            string(REPLACE ${_include_dir} "\${MOREFEM_INSTALL_DIR_INCL}" shorter_path ${source})
            list(APPEND ret ${shorter_path})
        endif()
        
    endforeach()
    
    set(${_list} ${ret} PARENT_SCOPE)
    
endfunction() 



