set(CMAKE_INSTALL_PREFIX /placeholder_value CACHE PATH "Installation directory for executables and libraries. A MoReFEM folder will be created there when install is invoked. This value should be overridden in command line!")

set(MOREFEM_THIRD_PARTY_LIBRARIES_DIR /Volumes/Data/opt)

set(CMAKE_C_COMPILER ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Openmpi/bin/mpicc CACHE FILEPATH "C compiler. Prefer to use an openmpi wrapper.")
set(CMAKE_CXX_COMPILER ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Openmpi/bin/mpic++ CACHE FILEPATH "C++ compiler. Prefer to use an openmpi wrapper.")

set(MOREFEM_DO_SANITIZE False CACHE BOOL "If true, sanitizer options -fsanitize=address, -fsanitize=memory and -fno-omit-frame-pointer are added.")

set(CMAKE_CXX_STANDARD 17 CACHE STRING "C++ standard; at least 17 is expected.")
set(CMAKE_CXX_STANDARD_REQUIRED ON CACHE STRING "Leave this one active.")
set(CMAKE_CXX_EXTENSIONS OFF CACHE STRING "If ON you might be using gnu++17; with OFF you'll use c++17.")

set(LIBRARY_TYPE STATIC CACHE BOOL "Choose either STATIC or SHARED.")

set(BUILD_MOREFEM_UNIQUE_LIBRARY True CACHE BOOL "Whether a unique library is built for MoReFEM core libraries or on the contrary if it is splitted in modules.")

set(MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE False CACHE BOOL "If true, add a (costly) method that gives an hint whether an UpdateGhost() call was relevant or not.")

set(MOREFEM_EXTENDED_TIME_KEEP False CACHE BOOL "If true, TimeKeep gains the ability to track times between each call of PrintTimeElapsed(). If not, PrintTimeElapsed() is flatly ignored. False is the best choice in production!")

set(MOREFEM_CHECK_NAN_AND_INF False CACHE BOOL "If true, there are additional checks that no nan and inf appears in the code. Even if False, solver always check for the validity of its solution (if a nan or an inf is present the SolveLinear() or SolveNonLinear() operation throws with a dedicated Petsc error). Advised in debug mode and up to you in release mode.")

set(OPEN_MPI_INCL_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Openmpi/include CACHE PATH "Include directory of Openmpi library.")
set(OPEN_MPI_LIB_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Openmpi/lib CACHE PATH "Lib directory of Openmpi library." )

set(BLAS_CUSTOM_LINKER True CACHE BOOL "If BLAS_CUSTOM_LINKER is true, BLAS_LIB field must give the command use to link with Blas. For instance on macOS it is usually \"-framework Accelerate\" (Beware: Without the quotes CMake will mute this into -framework -Accelerate). If False, FindLibrary is used to find the Blas library to be used, as for the other libraries in this file. The difference is that the name of the .a, .so or .dylib is not known, so it must be given in BLAS_LIB_NAME field. For instance openblas to find libopenblas.a in BLAS_LIB_DIR.")
set(BLAS_LIB_DIR None CACHE STRING "None or path to the lib directory of Blas (see BLAS_CUSTOM_LINKER).")
set(BLAS_LIB "-framework Accelerate" CACHE STRING "Name of the Blas lib (e.g. openblas) or command to pass if custom linker is used; see BLAS_CUSTOM_LINKER." )


set(PETSC_GENERAL_INCL_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Petsc/include CACHE PATH "Include directory of Petsc library common to any build.")
set(PETSC_DEBUG_INCL_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Petsc/debug/include CACHE PATH "Include directory of Petsc library specific to debug configuration.")
set(PETSC_RELEASE_INCL_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Petsc/release/include  CACHE PATH "Include directory of Petsc library specific to release configuration.")

set(PETSC_DEBUG_LIB_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Petsc/debug/lib  CACHE PATH "Library directory of Petsc in debug mode.")
set(PETSC_RELEASE_LIB_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Petsc/release/lib  CACHE PATH "Library directory of Petsc in release mode.")


set(PARMETIS_INCL_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Parmetis/include  CACHE PATH "Include directory of Parmetis library.")
set(PARMETIS_LIB_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Parmetis/lib  CACHE PATH "Lib directory of Parmetis library.")

# Lua library.
set(LUA_INCL_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Lua/include  CACHE PATH "Include directory of Lua library.")
set(LUA_LIB_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Lua/lib  CACHE PATH "Lib directory of Lua library.")

set(BOOST_INCL_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Boost/include  CACHE PATH "Include directory of Boost library.")
set(BOOST_LIB_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Boost/lib  CACHE PATH "Lib directory of Boost library.")

set(PHILLIPS_DIR False CACHE BOOL "If you want to couple Morefem with Phillips library. False in most of the cases! Beware: it is not put in MOREFEM_COMMON_DEP; if you need it you must add it in your add_executable command.")


