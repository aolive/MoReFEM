///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 12 Nov 2014 16:08:20 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_HPP_

# include <memory>
# include <vector>

# include "Utilities/UniqueId/UniqueId.hpp"

# include "Core/InputParameterData/Advanced/SetFromInputParameterData.hpp"
# include "Core/InputParameter/Result.hpp"

# include "Geometry/Mesh/Mesh.hpp"

# include "FiniteElement/Unknown/UnknownManager.hpp"
# include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
# include "FiniteElement/FiniteElementSpace/Internal/NdofHolder.hpp"
# include "FiniteElement/FiniteElementSpace/Internal/MatrixPattern.hpp"
# include "FiniteElement/FiniteElementSpace/Internal/ComputeMatrixPattern.hpp"
# include "FiniteElement/FiniteElementSpace/Internal/DofProgramWiseIndexListPerVertexCoordIndexListManager.hpp"
# include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GodOfDofManager;
    class GlobalMatrix;
    class GlobalVector;
    class GlobalDiagonalMatrix;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================



    //! Whether output directory must be created or not.
    enum class create_output_dir { yes, no };


    /// \addtogroup FiniteElementGroup
    ///@{


    /*!
     * \brief Object in charge of all the dofs related to a given \a Mesh.
     *
     * There is exactly one \a GodOfDof per \a Mesh; both objects share the same unique id. For instance
     * if in the input parameter file there is
     * \verbatim
        Mesh2 = { ... }
     \endverbatim
     * a \a Mesh AND a \a GodOfDof with an identifier of 2 are created.
     *
     * \a GodOfDof is also the place where all finite element spaces related to a given mesh are stored.
     */
    class GodOfDof final : public std::enable_shared_from_this<GodOfDof>,
                           public Crtp::CrtpMpi<GodOfDof>,
                           public Crtp::UniqueId<GodOfDof, UniqueIdNS::AssignationMode::manual>
    {

    public:

        //! Alias to shared pointer. It is a shared_pointer and not unique_ptr here as weak_pointers will be used in FEltSpace.
        using shared_ptr = std::shared_ptr<GodOfDof>;

        //! Alias to shared pointer. It is a shared_pointer and not unique_ptr here as weak_pointers will be used in FEltSpace.
        using const_shared_ptr = std::shared_ptr<const GodOfDof>;

        //! Convenient alias to one of the parent.
        using unique_id_parent = Crtp::UniqueId<GodOfDof, UniqueIdNS::AssignationMode::manual>;

        //! Name of the class.
        static const std::string& ClassName();

        //! Friendship.
        friend class GodOfDofManager;


        /// \name Special members.
        ///@{

    private:

        /*!
         * \brief Constructor.
         *
         * \internal <b><tt>[internal]</tt></b> It is private as construction should be driven by friend GodOfDofManager.
         *
         * \copydetails doxygen_hide_mpi_param
         * \param[in] mesh Mesh upon which the GodOfDof is built.
         */
        explicit GodOfDof(const Wrappers::Mpi& mpi,
                          Mesh& mesh);

    public:

        //! Destructor.
        ~GodOfDof() = default;

        //! Copy constructor.
        GodOfDof(const GodOfDof&) = delete;

        //! Move constructor.
        GodOfDof(GodOfDof&&) = default;

        //! Copy affectation.
        GodOfDof& operator=(const GodOfDof&) = delete;

        //! Move affectation.
        GodOfDof& operator=(GodOfDof&&) = delete;


        ///@}


    public:

        /*!
         * \brief Fully init the GodOfDof.
         *
         * This means among others:
         * - Partition between processors the problem if code is run in parallel.
         * - Set the list of finite elements in each finite element space.
         * - Set the list of nodes here.
         *
         * \copydoc doxygen_hide_input_parameter_data_arg
         * \param[in] felt_space_list List of all \a FEltSpace to consider in the \a GodOfDof.
         * \copydetails doxygen_hide_do_consider_processor_wise_local_2_global
         * \param[in] output_directory Output directory for data specific to the mesh covered by the \a GodOfDof.
         */
        template<class InputParameterDataT>
        void Init(const InputParameterDataT& input_parameter_data,
                  FEltSpace::vector_unique_ptr&& felt_space_list,
                  DoConsiderProcessorWiseLocal2Global do_consider_processor_wise_local_2_global,
                  std::string&& output_directory);


        //! Get the mesh object.
        const Mesh& GetMesh() const noexcept;

        /*!
         * \brief Get access to the \a unique_id -th finite element space.
         *
         * It assumes the finite element space does exist in the GodOfDof; this can be checked beforehand with
         * IsFEltSpace(index).
         * \param[in] unique_id Unique identifier of the request \a FEltSpace.
         *
         * \return Returns the \a unique_id -th finite element space.
         */
        const FEltSpace& GetFEltSpace(unsigned int unique_id) const;

        //! Whether the finite element space \a felt_space_index is in the GodOfDof.
        bool IsFEltSpace(unsigned int felt_space_index) const;

        //! Get the list of FEltSpace.
        const FEltSpace::vector_unique_ptr& GetFEltSpaceList() const noexcept;

        //! Returns the list of node bearers present on local processor.
        const NodeBearer::vector_shared_ptr& GetProcessorWiseNodeBearerList() const noexcept;

        //! Returns the list of ghost node bearers.
        const NodeBearer::vector_shared_ptr& GetGhostNodeBearerList() const noexcept;

        //! Returns the number of processor-wise dofs (excluding ghosts).
        unsigned int NprocessorWiseDof() const noexcept;

        //! Returns the number of processor-wise dofs (excluding ghosts) within a given numbering subset.
        unsigned int NprocessorWiseDof(const NumberingSubset& numbering_subset) const;

        //! Get the number of program-wise dofs.
        unsigned int NprogramWiseDof() const noexcept;

        //! Returns the number of program-wise dofs within a given numbering subset.
        unsigned int NprogramWiseDof(const NumberingSubset& numbering_subset) const;

        /*!
         * \brief Get the list of all processor-wise dofs.
         */
        const Dof::vector_shared_ptr& GetProcessorWiseDofList() const noexcept;

        /*!
         * \brief Get the list of all ghosted dofs.
         */
        const Dof::vector_shared_ptr& GetGhostedDofList() const noexcept;


        /*!
         * \brief Tells whether a given dof is ghosted or not.
         *
         * By design the numbering handles first all the processor-wise dofs, and then numbers the ghosted ones.
         * So the test here relies on that property: a dof is ghosted if its index is greater than the number
         * of processor-wise dofs.
         *
         * \param[in] dof Dof which status is tested.
         *
         * \return True if \a dof is a ghost on current processor, false if it is directly managed by the local processor.
         */
        bool IsGhosted(const Dof& dof) const;

        //! List of all numbering subsets, computed from the finite element spaces.
        const NumberingSubset::vector_const_shared_ptr& GetNumberingSubsetList() const noexcept;


        //! Fetch the matrix pattern which rows are numbered by \a row_numbering_subset, and columns by \a column_numbering_subset.
        const Wrappers::Petsc::MatrixPattern& GetMatrixPattern(const NumberingSubset& row_numbering_subset,
                                                               const NumberingSubset& column_numbering_subset) const;


        //! Shorthand function for a matrix described only by one \a numbering_subset (square matrix).
        const Wrappers::Petsc::MatrixPattern& GetMatrixPattern(const NumberingSubset& numbering_subset) const;


        /*!
         * \brief Return the list of the dofs involved in an essential boundary condition.
         *
         * The dofs may be processor-wise or ghosts; as usual ghost are packed together at the end of the container.
         *
         * \attention There is a return-by-value as it is currently used only in State constructor; store that
         * somewhere if it is needed by something else!
         *
         * \param[in] numbering_subset Only dofs that belongs to this \a NumberingSubset are requested.
         *
         * \return List of dofs encompassed by the boundary conditions relared to \a numbering_subset.
         */
        Dof::vector_shared_ptr GetBoundaryConditionDofList(const NumberingSubset& numbering_subset) const;

        /*!
         * \brief Yield a shared_ptr to the current object.
         *
         * This requires that at least one shared_ptr to this object exists (see behaviour of std::enable_shared_from_this
         * for more details).
         *
         * \return Return shared_from_this().
         */
        GodOfDof::shared_ptr GetSharedPtr();


        # ifndef NDEBUG
        /*!
         * \brief Whether Init() has already been called or not.
         *
         * Finite element spaces are fully initialized with the call to Init(), which among other things fill them
         * and partition the mesh. They must not been defined after this call; current debug attribute is there
         * to ensure it doesn't happen.
         *
         * \return Whether Init() has already been called or not.
         */
        bool HasInitBeenCalled() const;
        # endif // NDEBUG


        //! Print to compare the numbering to the one generated by Ondomatic.
        void OndomaticLikePrint(const NumberingSubset& numbering_subset,
                                const std::string& output_directory) const;

        //! Print the informations for each dof.
        void PrintDofInformation(const NumberingSubset& numbering_subset,
                                 std::ostream& out) const;

        //! Get the numbering subset which id is \a unique_id.
        const NumberingSubset& GetNumberingSubset(unsigned int unique_id) const;

        //! Get the numbering subset which id is \a unique_id as a smart pointer.
        const NumberingSubset::const_shared_ptr& GetNumberingSubsetPtr(unsigned int unique_id) const;

        //! Get the output directory.
        const std::string& GetOutputDirectory() const noexcept;


        /*!
         * \brief Returns the name of the subfolder related to a given numbering subset.
         *
         * For instance
         *
         * \code
         * *path_to_output_directory* /NumberingSubset*i*
         * \endcode
         *
         * where i is the unique id of the numbering subset.
         *
         * \param[in] numbering_subset Numbering subset for which output folder is sought.
         *
         * \return Name of the subfolder related to a given numbering subset.
         */
        std::string GetOutputDirectoryForNumberingSubset(const NumberingSubset& numbering_subset) const;


        //! Get the path of the time iteration file.
        const std::string& GetTimeIterationFile() const noexcept;

        //! Clear the temporary data used to build properly the Internal::FEltNS::Local2GlobalStorage objects.
        void ClearTemporaryData() const noexcept;

        //! Apply the boundary condition method given in template argument.
        template<BoundaryConditionMethod BoundaryConditionMethodT>
        void ApplyBoundaryCondition(const DirichletBoundaryCondition& boundary_condition, GlobalMatrix& matrix) const;

        //! Apply the boundary condition method given in template argument.
        template<BoundaryConditionMethod BoundaryConditionMethodT>
        void ApplyBoundaryCondition(const DirichletBoundaryCondition& boundary_condition, GlobalVector& vector) const;


        /*!
         * \brief Apply one boundary condition by pseudo-elimination on a matrix.
         *
         * Relevant numbering subset is read from the matrix object.
         *
         * \param[in] boundary_condition Boundary condition to be applied.
         * \param[in,out] matrix Matrix upon which the condition is applied.
         */
        void ApplyPseudoElimination(const DirichletBoundaryCondition& boundary_condition, GlobalMatrix& matrix) const;

        /*!
         * \brief Apply one boundary condition by pseudo-elimination on a vector.
         *
         * Relevant numbering subset is read from the vector object.
         *
         * \param[in] boundary_condition Boundary condition to be applied.
         * \param[in,out] vector Vector upon which the condition is applied.
         */
        void ApplyPseudoElimination(const DirichletBoundaryCondition& boundary_condition, GlobalVector& vector) const;

        /*!
         * \brief Apply one boundary condition by penalization on a matrix.
         *
         * Relevant numbering subset is read from the matrix object.
         *
         * \param[in] boundary_condition Boundary condition to be applied.
         * \param[in,out] matrix Matrix upon which the condition is applied.
         */
        void ApplyPenalization(const DirichletBoundaryCondition& boundary_condition, GlobalMatrix& matrix) const;

        /*!
         * \brief Apply one boundary condition by penalization on a vector.
         *
         * Relevant numbering subset is read from the vector object.
         *
         * \param[in] boundary_condition Boundary condition to be applied.
         * \param[in,out] vector Vector upon which the condition is applied.
         */
        void ApplyPenalization(const DirichletBoundaryCondition& boundary_condition, GlobalVector& vector) const;


        /*!
         * \brief Non constant access to the mesh object.
         *
         * \attention Access is public solely for the needs of FEltSpace::Movemesh(). You shouldn't call this accessor
         * at all unless you are modifying the core of MoReFEM; during the development of a model you shouldn't
         * use this one at all and prefer the constant accessor.
         *
         * \return \a Mesh on top of which the GodOfDof has been built.
         */
        Mesh& GetNonCstMesh() noexcept;



    private:

        //! Non constant access to the list of node bearers.
        NodeBearer::vector_shared_ptr& GetNonCstProcessorWiseNodeBearerList() noexcept;

        //! Non constant access to the list of ghost node bearers.
        NodeBearer::vector_shared_ptr& GetNonCstGhostNodeBearerList() noexcept;


        /*!
         * \brief Return the list of all boundaries conditions.
         *
         * \return List of all boundaries conditions inside the GodOfDof.
         */
        DirichletBoundaryCondition::vector_shared_ptr& GetNonCstDirichletBoundaryConditionList();


        /*!
         * \brief Iterator to the finite element space which index is \a unique_id.
         *
         * Iterator might be the end of the list; this method is only for internal purposes.
         *
         * \param[in] unique_id Unique identifier of the \a FEltSpace (noticeably used in the input parameter file to
         * tag the finite element space).
         *
         * \return Iterator to the finite element space which index is \a unique_id.
         */
        auto GetIteratorFEltSpace(unsigned int unique_id) const;


    private:

        //! Init the nodes.
        void CreateNodeList();

        /*!
         * \brief Create all the dofs for each node bearer.
         */
        void ComputeProcessorWiseAndGhostDofIndex();


        /*!
         * \brief Reduce to processor-wise the finite elements in each finite element space, the mesh and computes
         * the ghost node bearers.
         *
         *
         * The only quantity not yet reduced is the dofs involved in the boundary conditions.
         */
        void ReduceToProcessorWise();

        //! Returns the number of node bearers.
        unsigned int NprocessorWiseNodeBearer() const noexcept;

        /*!
         * \brief Set for each concerned dof the associated value from a Dirichlet boundary condition.
         *
         * \internal <b><tt>[internal]</tt></b> Boundary conditions have not been discussed deeply yet and so essential ones are still
         * handled à la Felisce (which is not necessarily a bad thing here).
         * However, we depart much for the handling of natural ones.
         */
        void SetBoundaryCondition();

        //! Get the list of all processor-wise dofs (ghost excluded).
        Dof::vector_shared_ptr& GetNonCstProcessorWiseDofList() noexcept;

        //! Get the list of all ghosted dofs.
        Dof::vector_shared_ptr& GetNonCstGhostedDofList() noexcept;


        /*!
         * \brief Iterate through all finite element spaces and compute the list of all numbering subsets.
         */
        void ComputeNumberingSubsetList();


    private:


        /*!
         * \brief Prepare the subfolders for each numbering_subset.
         */
        void PrepareOutput();

        //! Accessor to NdofHolder.
        const Internal::FEltSpaceNS::NdofHolder& GetNdofHolder() const noexcept;

        /*!
         * \class doxygen_hide_dof_list_per_felt_space_arg
         *
         * dof_list_per_felt_space For each finite element space (represented by its unique id), determine
         * the full list of dofs (also represented by their unique ids). This list will be used to reconstitute
         * later the list of processor- and ghost-wise dofs in each finite element space.
         * It is intentional here that indexes are used rather than shared pointers: reduction to processor-wise
         * rely on the automatic deletion of unused shared pointers and storing more here would harm the process.
         */

        /*!
         * \brief First part of the Init() method, that begins to initialize stuff but has not yet begun the reduction
         * to processor-wise.
         *
         * It is hence decoupled because this part may be compiled, contrary to the second one.
         *
         * \param[in] felt_space_list List of all \a FEltSpace to consider in the \a GodOfDof.
         * \param[out] \copydetails doxygen_hide_dof_list_per_felt_space_arg
         *
         *
         */
        void Init1(FEltSpace::vector_unique_ptr&& felt_space_list,
                   std::map<unsigned int, std::vector<unsigned int>>& dof_list_per_felt_space);


        /*!
         * \brief Second part of the Init() method.
         *
         * This is where the model-specific objects that must somehow access fully data before data reduction to
         * processor-wise are performed. The typical example here is data required by non conform interpolator, which
         * involves several GodOfDofs and can't therefore be handled solely by the clever Petsc mpi communications.
         *
         * \copydoc doxygen_hide_input_parameter_data_arg
         */
        template<class InputParameterDataT>
        void Init2(const InputParameterDataT& input_parameter_data);


        /*!
         * \brief Third part of the Init() method, which mostly settles the data reduction to processor-sie.
         *
         * \internal <b><tt>[internal]</tt></b> This part is compiled.
         *
         * \copydetails doxygen_hide_do_consider_processor_wise_local_2_global
         * \param[in] \copydetails doxygen_hide_dof_list_per_felt_space_arg
         */
        void Init3(DoConsiderProcessorWiseLocal2Global do_consider_processor_wise_local_2_global,
                   const std::map<unsigned int, std::vector<unsigned int>>& dof_list_per_felt_space);




    public:

        /*!
         * \brief Whether local2global arrays for processor-wise indexes might be required or not.
         *
         * yes doesn't mean they are already computed, but is a mandatory prerequisite should they be required.
         *
         * \return yes if local2global arrays for processor-wise indexes are to be considered.
         */
        DoConsiderProcessorWiseLocal2Global GetDoConsiderProcessorWiseLocal2Global() const;

      private:

        //! List of all finite element spaces related to the mesh covered by GodOfDof.
        FEltSpace::vector_unique_ptr felt_space_list_;

        //! List of all numbering subsets, computed from the finite element spaces.
        NumberingSubset::vector_const_shared_ptr numbering_subset_list_;

        //! Mesh covered by the current object.
        Mesh& mesh_;

        //! List of all node bearers. Each one is present only once here; ghost aren't present in this one.
        NodeBearer::vector_shared_ptr processor_wise_node_bearer_list_;

        //! List of all ghosted node bearers, that is all nodes required for calculation but not hosted by local processor.
        NodeBearer::vector_shared_ptr ghost_node_bearer_list_;

        //! Objects that counts the number of dofs in several configurations.
        Internal::FEltSpaceNS::NdofHolder::const_unique_ptr Ndof_holder_ = nullptr;

        /*!
         * \brief List of processor-wise dofs.
         *
         * Ghosted are excluded and handled in ghosted_dof_list_.
         *
         * \internal <b><tt>[internal]</tt></b> This information is redundant with the one from node_per_felt_,
         * but it allows to use a slicker interface (direct loop instead of three imbricated ones). // \todo #258
         */
        Dof::vector_shared_ptr processor_wise_dof_list_;

        /*!
         * \brief List of processor-wise dofs.
         *
         * \internal <b><tt>[internal]</tt></b> This information is redundant with the one from node_per_felt_,
         * but it allows to use a slicker interface (direct loop instead of three imbricated ones).
         */
        Dof::vector_shared_ptr ghosted_dof_list_;

        //! CSR Pattern of the matrix.
        Internal::FEltSpaceNS::MatrixPattern::vector_const_unique_ptr matrix_pattern_per_numbering_subset_;

        //! Path to the output directory into which god of dof data are written.
        std::string output_directory_;

        //! Path of the file listing the time iterations and the related files.
        std::string time_iteration_file_;

        # ifndef NDEBUG
        /*!
         * \brief Whether Init() has already been called or not.
         *
         * Finite element spaces are fully initialized with the call to Init(), which among other things fill them
         * and partition the mesh. They must not been defined after this call; current debug attribute is there
         * to ensure it doesn't happen.
         */
        bool has_init_been_called_ = false;

        /*!
         * \brief Whether local2global arrays for processor-wise indexes might be required or not.
         *
         * yes doesn't mean they are already computed, but is a mandatory prerequisite should they be required.
         */
        DoConsiderProcessorWiseLocal2Global do_consider_proc_wise_local_2_global_ = DoConsiderProcessorWiseLocal2Global::no;

        # endif // NDEBUG

    };


    /*!
     * \brief Print the ids of node bearers on the local processor.
     *
     * \param[in] node_bearer_list List of node bearers to print.
     * \param[in] rank Rank of the current processor.
     * \param[in,out] out Stream to which the informations is written.
     */
    void PrintNodeBearerList(const NodeBearer::vector_shared_ptr& node_bearer_list, unsigned int rank,
                             std::ostream& out = std::cout);


    /*!
     * \brief Compute the list of processor-wise indexes (ghost excluded!) in a \a GodOfDof matching a pair
     * \a Unknown / \a NumberingSubset.
     *
     * \param[in] god_of_dof \a GodOfDof from which list of dofs is extracted.
     * \param[in] numbering_subset \a NumberingSubset of the sought \a Dof.
     * \param[in] unknown \a Unknown of the sought \a Dof.
     *
     * \return List of processor-wise dof indexes matching all criteria.
     */
     std::vector<unsigned int> ComputeProcessorWiseIndexList(const GodOfDof& god_of_dof,
                                                             const NumberingSubset& numbering_subset,
                                                             const Unknown& unknown);


    /*!
     * \brief Allocate the underlying (parallel) pattern of a matrix.
     *
     * GlobalMatrix constructor only defines the related numbering subsets, but the Petsc matrix isn't yet allocated.
     *
     * Purpose of the current function is to finish the work and allocate the pattern of the matrix, possibly over
     * several processors in parallelism is involved.
     *
     * \param[in] god_of_dof God of dof upon which both numbering subsets of \a matrix are defined.
     * \param[in,out] matrix Matrix being allocated. On input, respective numbering subsets for rows and columns
     * are already defined; \a god_of_dof will use this information to properly allocate the underlying Petsc matrix.
     */
    void AllocateGlobalMatrix(const GodOfDof& god_of_dof,
                              GlobalMatrix& matrix);


    /*!
     *
     * \copydoc MoReFEM::AllocateGlobalMatrix()
     */
    void AllocateGlobalMatrix(const GodOfDof& god_of_dof,
                              GlobalDiagonalMatrix& matrix);


    /*!
     * \brief Allocate the underlying (parallel) pattern of a vector.
     *
     * GlobalVector constructor only defines the related numbering subset, but the Petsc vectors isn't yet allocated.
     *
     * Purpose of the current function is to finish the work and allocate the pattern of the vector, possibly over
     * several processors in parallelism is involved.
     *
     * \param[in] god_of_dof God of dof upon which numbering subset of \a vector are defined.
     * \param[in,out] vector Vector being allocated. On input, numbering subset is already defined; \a god_of_dof will
     * use this information to properly allocate the underlying Petsc vector.
     */
    void AllocateGlobalVector(const GodOfDof& god_of_dof,
                              GlobalVector& vector);


    # ifndef NDEBUG
    /*!
     * \copydoc doxygen_hide_assert_matrix_respect_pattern_function
     *
     * \param[in] god_of_dof GodOfDof onto which the matrix is defined.
     */
    void AssertMatrixRespectPattern(const GodOfDof& god_of_dof,
                                    const GlobalMatrix& matrix,
                                    const char* invoking_file, int invoking_line);
    # endif // NDEBUG



    ///@} // \addtogroup


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/FiniteElementSpace/GodOfDof.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_HPP_
