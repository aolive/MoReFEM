///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 12 Nov 2014 16:08:20 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_HXX_


namespace MoReFEM
{



    template<class InputParameterDataT>
    void GodOfDof::Init(const InputParameterDataT& input_parameter_data,
                        FEltSpace::vector_unique_ptr&& a_felt_space_list,
                        DoConsiderProcessorWiseLocal2Global do_consider_proc_wise_local_2_global,
                        std::string&& output_directory)
    {
        output_directory_ = std::move(output_directory);

        std::map<unsigned int, std::vector<unsigned int>> dof_list_per_felt_space;

        Init1(std::move(a_felt_space_list), dof_list_per_felt_space);
        Init2(input_parameter_data);
        Init3(do_consider_proc_wise_local_2_global, dof_list_per_felt_space);
    }


    template<class InputParameterDataT>
    void GodOfDof::Init2(const InputParameterDataT& input_parameter_data)
    {
        auto& manager = Internal::FEltSpaceNS::DofProgramWiseIndexListPerVertexCoordIndexListManager
        ::CreateOrGetInstance(__FILE__, __LINE__);
        Advanced::SetFromInputParameterData<>(input_parameter_data, manager, *this);

    }


    inline const Mesh& GodOfDof::GetMesh() const noexcept
    {
        return mesh_;
    }


    inline Mesh& GodOfDof::GetNonCstMesh() noexcept
    {
        return mesh_;
    }


    inline const FEltSpace::vector_unique_ptr& GodOfDof::GetFEltSpaceList() const noexcept
    {
        return felt_space_list_;
    }


    inline const NodeBearer::vector_shared_ptr& GodOfDof::GetProcessorWiseNodeBearerList() const noexcept
    {
        return processor_wise_node_bearer_list_;
    }


    inline NodeBearer::vector_shared_ptr& GodOfDof::GetNonCstProcessorWiseNodeBearerList() noexcept
    {
        return processor_wise_node_bearer_list_;
    }


    inline NodeBearer::vector_shared_ptr& GodOfDof::GetNonCstGhostNodeBearerList() noexcept
    {
        return ghost_node_bearer_list_;
    }


    inline const NodeBearer::vector_shared_ptr& GodOfDof::GetGhostNodeBearerList() const noexcept
    {
        return ghost_node_bearer_list_;
    }



    inline unsigned int GodOfDof::NprogramWiseDof() const noexcept
    {
        return GetNdofHolder().NprogramWiseDof();
    }


    inline const Dof::vector_shared_ptr& GodOfDof::GetProcessorWiseDofList() const noexcept
    {
        return processor_wise_dof_list_;
    }


    inline const Dof::vector_shared_ptr& GodOfDof::GetGhostedDofList() const noexcept
    {
        return ghosted_dof_list_;
    }


    inline unsigned int GodOfDof::NprocessorWiseDof() const noexcept
    {
        assert(GetNdofHolder().NprocessorWiseDof() == static_cast<unsigned int>(processor_wise_dof_list_.size()));
        return GetNdofHolder().NprocessorWiseDof();
    }


    inline unsigned int GodOfDof::NprocessorWiseDof(const NumberingSubset& numbering_subset) const
    {
        return GetNdofHolder().NprocessorWiseDof(numbering_subset);
    }


    inline unsigned int GodOfDof::NprogramWiseDof(const NumberingSubset& numbering_subset) const
    {
        return GetNdofHolder().NprogramWiseDof(numbering_subset);
    }


    inline unsigned int GodOfDof::NprocessorWiseNodeBearer() const noexcept
    {
        return static_cast<unsigned int>(processor_wise_node_bearer_list_.size());
    }


    inline GodOfDof::shared_ptr GodOfDof::GetSharedPtr()
    {
        return shared_from_this();
    }


    # ifndef NDEBUG
    inline bool GodOfDof::HasInitBeenCalled() const
    {
        return has_init_been_called_;
    }
    # endif // NDEBUG


    inline Dof::vector_shared_ptr& GodOfDof::GetNonCstProcessorWiseDofList() noexcept
    {
        return processor_wise_dof_list_;
    }


    inline Dof::vector_shared_ptr& GodOfDof::GetNonCstGhostedDofList() noexcept
    {
        return ghosted_dof_list_;
    }


    inline const NumberingSubset::vector_const_shared_ptr& GodOfDof::GetNumberingSubsetList() const noexcept
    {
        assert(!numbering_subset_list_.empty());
        assert(std::is_sorted(numbering_subset_list_.cbegin(),
                              numbering_subset_list_.cend(),
                              Utilities::PointerComparison::Less<NumberingSubset::const_shared_ptr>()));

        return numbering_subset_list_;
    }


    inline const std::string& GodOfDof::GetOutputDirectory() const noexcept
    {
        assert(!(!FilesystemNS::Folder::DoExist(output_directory_)));
        return output_directory_;
    }


    inline const std::string& GodOfDof::GetTimeIterationFile() const noexcept
    {
        return time_iteration_file_;
    }


    inline const Internal::FEltSpaceNS::NdofHolder& GodOfDof::GetNdofHolder() const noexcept
    {
        assert(!(!Ndof_holder_));
        return *Ndof_holder_;
    }


    inline const Wrappers::Petsc::MatrixPattern& GodOfDof
    ::GetMatrixPattern(const NumberingSubset& numbering_subset) const
    {
        return GetMatrixPattern(numbering_subset, numbering_subset);
    }


    inline const NumberingSubset& GodOfDof::GetNumberingSubset(unsigned int unique_id) const
    {
        return *(GetNumberingSubsetPtr(unique_id));
    }



    # ifndef NDEBUG
    inline DoConsiderProcessorWiseLocal2Global GodOfDof::GetDoConsiderProcessorWiseLocal2Global() const
    {
        return do_consider_proc_wise_local_2_global_;
    }

    # endif // NDEBUG


    template<BoundaryConditionMethod BoundaryConditionMethodT>
    void GodOfDof
    ::ApplyBoundaryCondition(const DirichletBoundaryCondition& boundary_condition, GlobalMatrix& matrix) const
    {
        switch(BoundaryConditionMethodT)
        {
            case BoundaryConditionMethod::pseudo_elimination:
                ApplyPseudoElimination(boundary_condition, matrix);
                break;
            case BoundaryConditionMethod::penalization:
                ApplyPenalization(boundary_condition, matrix);
                break;
        }
    }


    template<BoundaryConditionMethod BoundaryConditionMethodT>
    void GodOfDof
    ::ApplyBoundaryCondition(const DirichletBoundaryCondition& boundary_condition, GlobalVector& vector) const
    {
        switch(BoundaryConditionMethodT)
        {
            case BoundaryConditionMethod::pseudo_elimination:
                ApplyPseudoElimination(boundary_condition, vector);
                break;
            case BoundaryConditionMethod::penalization:
                ApplyPenalization(boundary_condition, vector);
                break;
        }
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_HXX_
