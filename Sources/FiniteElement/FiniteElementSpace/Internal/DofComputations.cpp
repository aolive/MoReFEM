///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 7 Apr 2015 09:39:03 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "FiniteElement/FiniteElementSpace/Internal/DofComputations.hpp"

namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace FEltSpaceNS
        {
             
        
            void ComputeProcessorWiseDofIndexes(const NodeBearer::vector_shared_ptr& node_bearer_list,
                                                DofNumberingScheme dof_numbering_scheme,
                                                unsigned int& current_dof_index,
                                                Dof::vector_shared_ptr& complete_dof_list)
            {
                Impl::ComputeDofIndexesHelper
                ::Perform<TypeDofIndex::processor_wise, Impl::DoProduceDofList::yes>(node_bearer_list,
                                                                                     dof_numbering_scheme,
                                                                                     nullptr,
                                                                                     current_dof_index,
                                                                                     complete_dof_list);
            }
                    
            
        } // namespace FEltSpaceNS
            
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
