///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 23 Dec 2013 11:27:31 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_CREATE_NODE_LIST_HELPER_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_CREATE_NODE_LIST_HELPER_HXX_



namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {



            template<class OrientedInterfaceT>
            void CreateNodeListHelper
            ::AddInterfaceNodeList(const GeometricElt& geom_elt,
                                   const Internal::RefFEltNS::RefFEltInFEltSpace& ref_felt,
                                   NodeBearer::vector_shared_ptr& node_bearer_list_for_current_local_felt_space,
                                   std::map<unsigned int, Node::shared_ptr>& node_for_current_finite_element,
                                   std::unordered_map<unsigned int, NodeBearer::shared_ptr>& interface_node_bearer_list)
            {
                using HelperType = Impl::InterfaceSpecialization<OrientedInterfaceT>;

                const unsigned int Ninterface = HelperType::Ninterface(geom_elt);
                const auto& oriented_interface_list = HelperType::GetInterfaceList(geom_elt);
                assert(static_cast<unsigned int>(oriented_interface_list.size()) == Ninterface);

                const auto& basic_ref_felt = ref_felt.GetBasicRefFElt();

                std::map<unsigned int, NodeBearer::shared_ptr> new_node_bearer_list; // key is the index of the Interface object.

                const auto& extended_unknown = ref_felt.GetExtendedUnknown();
                const auto& unknown = extended_unknown.GetUnknown();
                decltype(auto) shape_function_label = extended_unknown.GetShapeFunctionLabel();
                const auto& numbering_subset_ptr = extended_unknown.GetNumberingSubsetPtr();

                const unsigned int Ncomponent = ref_felt.Ncomponent();

                auto nature = OrientedInterfaceT::StaticNature();

                // Iterate through all interfaces, and create or retrieve the associated node.
                for (unsigned int i = 0u; i < Ninterface; ++i)
                {
                    auto oriented_interface_ptr = oriented_interface_list[i];
                    assert(!(!oriented_interface_ptr));

                    OrientedInterfaceT& oriented_interface = *oriented_interface_ptr;

                    typename HelperType::NodeListType local_node_on_interface_list =
                        HelperType::GetNodeList(basic_ref_felt, oriented_interface, i);

                    assert(!local_node_on_interface_list.empty());

                    const unsigned int interface_index = oriented_interface.GetIndex();

                    // Does the node on interface already exist? (might have been created by another finite elt).
                    auto it = interface_node_bearer_list.find(interface_index);

                    NodeBearer::shared_ptr node_bearer_ptr(nullptr);

                    const bool is_new_node_bearer = (it == interface_node_bearer_list.cend());

                    if (is_new_node_bearer)
                    {
                        node_bearer_ptr = HelperType::CreateNodeBearer(oriented_interface_ptr);
                        auto pair = std::make_pair(interface_index, node_bearer_ptr);
                        interface_node_bearer_list.insert(pair);
                        new_node_bearer_list.insert(pair);

                        CreateNodeList(local_node_on_interface_list,
                                       extended_unknown,
                                       Ncomponent,
                                       *node_bearer_ptr,
                                       node_for_current_finite_element);
                    }
                    else
                    {
                        node_bearer_ptr = it->second;

                        // Check whether the node list exists within the node bearer, and if not create it.
                        auto&& node_list = node_bearer_ptr->GetNodeList(unknown, shape_function_label);

                        if (node_list.empty())
                        {
                            CreateNodeList(local_node_on_interface_list,
                                           extended_unknown,
                                           Ncomponent,
                                           *node_bearer_ptr,
                                           node_for_current_finite_element);
                        }
                        else
                        {
                            // Case in which the Node already exist; just report properly its instances
                            // to the current finite element.
                            const std::size_t Nnode = node_list.size();

                            assert(node_list.size() == local_node_on_interface_list.size());

                            for (std::size_t j = 0ul; j < Nnode; ++j)
                            {
                                const auto& local_node_ptr = local_node_on_interface_list[j];
                                assert(!(!local_node_ptr));
                                auto& node_ptr = node_list[j];
                                assert(!(!node_ptr));
                                node_ptr->RegisterNumberingSubset(numbering_subset_ptr);

                                node_for_current_finite_element.insert({local_node_ptr->GetIndex(), node_ptr});
                            }
                        }
                    }

                    // Add the node bearer to the list of nodes bearers only if it isn't already there (from an unknown previously
                    // dealt with for instance).
                    // Node bearer index is not yet assigned; so we use to check whether the NodeBearer is already in the
                    // current finite element the interface (nature/index couple should be unique).
                    if (std::find_if(node_bearer_list_for_current_local_felt_space.cbegin(),
                                     node_bearer_list_for_current_local_felt_space.cend(),
                                     [interface_index, nature](const NodeBearer::shared_ptr& current_node_bearer_ptr)
                                     {
                                         assert(!(!current_node_bearer_ptr));

                                         auto current_nature = current_node_bearer_ptr->GetNature();

                                         return current_nature == nature
                                         && current_node_bearer_ptr->GetInterface().GetIndex() == interface_index;

                                     }) == node_bearer_list_for_current_local_felt_space.cend())
                    {
                        node_bearer_list_for_current_local_felt_space.push_back(node_bearer_ptr);
                    }
                }

                // Update node_bearer_list_ and set a temporary NodeBearer index (it is changed after partitioning).
                for (const auto& pair : new_node_bearer_list)
                {
                    auto& node_bearer_ptr = pair.second;
                    node_bearer_ptr->SetIndex(node_bearer_list_.size());
                    node_bearer_list_.push_back(node_bearer_ptr);
                }


            }


            template<class LocalNodeOnInterfaceT>
            void CreateNodeListHelper
            ::CreateNodeList(const LocalNodeOnInterfaceT& local_node_on_interface_list,
                             const ExtendedUnknown& extended_unknown,
                             const unsigned int Ncomponent,
                             NodeBearer& node_bearer,
                             std::map<unsigned int, Node::shared_ptr>& node_for_current_finite_element)
            {
                for (const auto& local_node_ptr : local_node_on_interface_list)
                {
                    auto&& node_ptr = node_bearer.AddNode(extended_unknown,
                                                          Ncomponent);

                    node_for_current_finite_element.insert({local_node_ptr->GetIndex(), std::move(node_ptr)});
                }
            }


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_CREATE_NODE_LIST_HELPER_HXX_
