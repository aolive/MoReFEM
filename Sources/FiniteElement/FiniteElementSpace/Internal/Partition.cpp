///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 3 Apr 2015 17:09:56 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "ThirdParty/Wrappers/Parmetis/Parmetis.hpp"

#include "Utilities/TimeKeep/TimeKeep.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/Partition.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Connectivity.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace FEltSpaceNS
        {
        
        
            namespace // anonymous
            {
                
                    
                
                /*!
                 * \brief This function provides a dumb partition to be fed to Parmetis library to generate a semsible one.
                 *
                 * In this dumb partition, node bearers are split equally between the processors, with the last one
                 * getting the rest of the euclidian division. So for instance if there are 10 nodes bearers and 3
                 * processors:
                 * - Processor 1 gets 0, 1, 2
                 * - Processor 2 gets 3, 4, 5
                 * - Processor 3 gets 6, 7, 8, 9
                 *
                 * \param[in] Nnode_bearer Number of node bearer program-wise.
                 * \param[in] Nprocessor Number of processors involved in mpi parallelism.
                 */
                std::vector<unsigned int> EuclidianPartition(const unsigned int Nnode_bearer,
                                                             const unsigned int Nprocessor);
                
                
                
                /*!
                 * \brief  Attribute each node to a processor.
                 *
                 * This is done crudely to pave the way to Parmetis call: an euclidian partition is actually used.
                 * That is not an entirely stupid guess: nodes are sort in a first place according to geometric
                 * element. However that is not enough: a node shared by several geometric elements is represented only
                 * in the first. That is there that Parmetis will provide insight to reduce bandwidth.
                 *
                 * Contrary to actual partitioning, all data are kept: it is really a transitory step required by Parmetis!
                 *
                 * \param[in] euclidian_partition Partition given by EuclidianPartition().
                 * \param[in] node_bearer_list List of node bearers before reduction to processor-wise. Each node bearer
                 * is assigned a processor value through SetProcessor().
                 * \param[in] Nprocessor Number of processors involved in mpi parallelism.
                 */
                void CrudePartitioning(const std::vector<unsigned int>& euclidian_partition,
                                       const NodeBearer::vector_shared_ptr& node_bearer_list,
                                       const unsigned int Nprocessor);

                
                
                
                /*!
                 * \brief Call Parmetis to provide a partition that limits as much as possible the number of ghosts.
                 *
                 * \param[in] euclidian_partition Partition given by EuclidianPartition().
                 * \param[in] node_bearer_csr_pattern CSR pattern of the node bearers (NOT of the dofs!)
                 * obtained from CrudePartitioning().
                 * \copydetails doxygen_hide_mpi_param
                 * \param[in,out] node_bearer_list In input, list of all node bearers program-wise. In output, list
                 * of the same length with the node bearers sort differently: each one has been attributed to a
                 * processor and all node bearers on a same processor are grouped together.
                 *
                 */
                void ParmetisPartitioning(const std::vector<unsigned int>& euclidian_partition,
                                          const ::MoReFEM::Wrappers::Mpi& mpi,
                                          Utilities::CSRPattern<parmetis_int>&& node_bearer_csr_pattern,
                                          NodeBearer::vector_shared_ptr& node_bearer_list);
                
                
                /*!
                 * \brief Prepare CSR format for the node bearers.
                 *
                 * This is used to feed Parmetis, so dofs aren't yet taken into account.
                 *
                 * \param[in] connectivity Connectivity of each node bearer.
                 * \param[in] node_bearer_list Node bearer list before reduction to processor-wise.
                 *
                 * \return CSR pattern.
                 */
                Utilities::CSRPattern<parmetis_int> PrepareNodeBearerCSRFormat(const NodeBearer::vector_shared_ptr& node_bearer_list,
                                                                              const connectivity_type& connectivity);
                
                
                
            } // namespace anonymous
            
            
            
            void PreparePartition(const ::MoReFEM::Wrappers::Mpi& mpi,
                                  const FEltSpace::vector_unique_ptr& felt_space_list,
                                  NodeBearer::vector_shared_ptr& node_bearer_list)
            {
                const unsigned int mpi_rank = mpi.GetRank<unsigned int>();
                
                const unsigned int Nprocessor = mpi.Nprocessor<unsigned int>();
                assert(Nprocessor > 1u);
                
                auto euclidian_partition = EuclidianPartition(static_cast<unsigned int>(node_bearer_list.size()),
                                                              Nprocessor);
                
                assert(euclidian_partition.size() == static_cast<unsigned int>(Nprocessor));
                
                
                // We separate the node_bearers following a simple euclidian division, without even looking the connectivity.
                // Data aren't reduced there of course!
                CrudePartitioning(euclidian_partition,
                                  node_bearer_list,
                                  Nprocessor);
                
                // Call Parmetis to partition the node_bearers more properly.
                auto&& connectivity = ComputeNodeBearerConnectivity(felt_space_list,
                                                                    static_cast<unsigned int>(mpi_rank),
                                                                    euclidian_partition[mpi_rank],
                                                                    KeepSelfConnexion::no);
                
                assert(!connectivity.empty() && "If it is, it's likely your finite element spaces (especially their domain) "
                       "are ill-defined. It might also be you're using too much processors for a too tiny model.");
                
                assert("Number of node_bearers must match what was determined by Euclidian partition"
                       && connectivity.size() == euclidian_partition[mpi_rank]);
                
                auto csr_node_bearer_pattern_for_parmetis = PrepareNodeBearerCSRFormat(node_bearer_list,
                                                                                       connectivity);
                
                ParmetisPartitioning(euclidian_partition,
                                     mpi,
                                     std::move(csr_node_bearer_pattern_for_parmetis),
                                     node_bearer_list);
            }
            
            
            
            
            void ReduceNodeBearerList(const unsigned int mpi_rank,
                                      NodeBearer::vector_shared_ptr& node_bearer_list)
            {
                NodeBearer::vector_shared_ptr processor_wise_node_list(node_bearer_list.size());
                
                // Create a new list that keep tabs only on processor-wise nodes.
                auto logical_end = std::copy_if(node_bearer_list.cbegin(), node_bearer_list.cend(),
                                                processor_wise_node_list.begin(),
                                                [mpi_rank](const NodeBearer::shared_ptr& node_ptr)
                                                {
                                                    return node_ptr->GetProcessor() == mpi_rank;
                                                });
                
                processor_wise_node_list.erase(logical_end, processor_wise_node_list.end());
                node_bearer_list.swap(processor_wise_node_list);
            }
            
            

            namespace // anonymous
            {
                
                
                std::vector<unsigned int> EuclidianPartition(const unsigned int Nnode_bearer,
                                                             const unsigned int Nprocessor)
                {
                    const unsigned int estimated_Ndof_per_proc = Nnode_bearer / Nprocessor;
                    std::vector<unsigned int> Nnode_bearer_per_proc(Nprocessor, estimated_Ndof_per_proc);
                    
                    // Add to last processor all dof lost due to euclidian division.
                    Nnode_bearer_per_proc.back() += Nnode_bearer % Nprocessor;
                    
                    return Nnode_bearer_per_proc;
                }
                
                
                void CrudePartitioning(const std::vector<unsigned int>& euclidian_partition,
                                       const NodeBearer::vector_shared_ptr& node_bearer_list,
                                       const unsigned int Nprocessor)
                {
                    unsigned int global_node_bearer_index = 0;
                    
                    for (unsigned int processor = 0; processor < Nprocessor; ++processor)
                    {
                        const unsigned int Nnode_bearer_on_processor = euclidian_partition[processor];
                        
                        for (unsigned int i = 0; i < Nnode_bearer_on_processor; ++i)
                        {
                            assert(global_node_bearer_index < node_bearer_list.size());
                            auto node_bearer_ptr = node_bearer_list[global_node_bearer_index++];
                            node_bearer_ptr->SetProcessor(processor);
                        }
                    }
                    
                    assert(global_node_bearer_index == node_bearer_list.size());
                }
                
                
                
                void ParmetisPartitioning(const std::vector<unsigned int>& euclidian_partition,
                                          const ::MoReFEM::Wrappers::Mpi& mpi,
                                          Utilities::CSRPattern<parmetis_int>&& csr_node_bearer_pattern_for_parmetis,
                                          NodeBearer::vector_shared_ptr& node_bearer_list)
                {                    
                    // Call to Parmetis.
                    auto&& parmetis_numbering =
                    ::MoReFEM::Wrappers::Parmetis::CreateNewPartitioning(euclidian_partition,
                                                                            csr_node_bearer_pattern_for_parmetis.iCSR(),
                                                                            csr_node_bearer_pattern_for_parmetis.jCSR(),
                                                                            mpi);
                    
                    // Report for each node_bearer the processor in charge.
                    const std::size_t Nnode_bearer = node_bearer_list.size();
                    assert(parmetis_numbering.size() == Nnode_bearer);
                    
                    
                    for (std::size_t i = 0ul; i < Nnode_bearer; ++i)
                    {
                        auto node_bearer_ptr = node_bearer_list[i];
                        assert(!(!node_bearer_ptr));
                        node_bearer_ptr->SetProcessor(parmetis_numbering[i]);
                    }
                    
                    // Now order the list of node_bearers so that all node_bearers on a given processor are contiguous.
                    std::stable_sort(node_bearer_list.begin(),
                                     node_bearer_list.end(),
                                     [](const NodeBearer::shared_ptr& node_bearer1,
                                        const NodeBearer::shared_ptr& node_bearer2)
                                     {
                                         assert(!(!node_bearer1));
                                         assert(!(!node_bearer2));
                                         return node_bearer1->GetProcessor() < node_bearer2->GetProcessor();
                                     });
                    
                    // Determine the definite index for each node_bearer.
                    for (std::size_t i = 0ul; i < Nnode_bearer; ++i)
                    {
                        auto node_bearer_ptr = node_bearer_list[i];
                        node_bearer_ptr->SetIndex(i);
                    }
                }
                
                
                            
                Utilities::CSRPattern<parmetis_int> PrepareNodeBearerCSRFormat(const NodeBearer::vector_shared_ptr& node_bearer_list,
                                                                               const connectivity_type& connectivity)
                {
                    std::vector<parmetis_int> iCSR { 0 };
                    std::vector<parmetis_int> jCSR;
                    
                    parmetis_int current_iCSR = 0;
                    
                    # ifndef NDEBUG
                    unsigned int Nnon_zero = 0;
                    
                    for (auto pair : connectivity)
                        Nnon_zero += static_cast<unsigned int>(pair.second.size());
                    # endif // NDEBUG
                    
                    
                    // We want here to fill row by row; this is why we can't iterate directly on the unordered_map connectivity.
                    
                    for (const auto& node_bearer_on_local_proc_ptr : node_bearer_list)
                    {
                        assert(!(!node_bearer_on_local_proc_ptr));
                        
                        auto it = connectivity.find(node_bearer_on_local_proc_ptr);
                        
                        // At this stage, node_bearer_list is much more filled than processor-wise data (it still includes
                        // data attributed to other processors).
                        // Ignore the non processor-wise data!
                        if (it == connectivity.cend())
                            continue;
                        
                        const auto& connected_node_bearer_list = it->second;
                        
                        const unsigned int Nconnected_node_bearer = static_cast<unsigned int>(connected_node_bearer_list.size());
                        
                        current_iCSR += static_cast<parmetis_int>(Nconnected_node_bearer);
                        iCSR.push_back(current_iCSR);
                        
                        std::vector<parmetis_int> jCSR_contribution;
                        
                        jCSR_contribution.reserve(Nconnected_node_bearer);
                        
                        for (const auto& connected_node_bearer_ptr : connected_node_bearer_list)
                        {
                            assert(!(!connected_node_bearer_ptr));
                            jCSR_contribution.push_back(static_cast<parmetis_int>(connected_node_bearer_ptr->GetIndex()));
                        }
                        
                        assert(jCSR_contribution.size() == Nconnected_node_bearer);
                        std::sort(jCSR_contribution.begin(), jCSR_contribution.end());
                        
                        
                        for (auto new_jCSR : jCSR_contribution)
                            jCSR.push_back(new_jCSR);
                    }
                    
                    assert(iCSR.size() == connectivity.size() + 1);
                    assert(jCSR.size() == Nnon_zero);
                    
                    return Utilities::CSRPattern<parmetis_int>(std::move(iCSR), std::move(jCSR));
                }
                
                
            } // namespace anonymous

            
        } // namespace FEltSpaceNS
        
        
    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
