///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Nov 2014 17:24:39 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_REDUCE_TO_PROCESSOR_WISE_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_REDUCE_TO_PROCESSOR_WISE_HPP_

# include "FiniteElement/FiniteElementSpace/Internal/FEltSpaceStorage.hpp"
# include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================

    class Mesh;
    class FEltSpace;


    namespace Wrappers
    {


        class Mpi;


    } // namespace Wrappers


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            /*!
             * \brief Reduce to processor-wise most data (finite elements in each finite element space, geometric
             * mesh region) and computes the list og ghost node bearers.
             *
             * \attention the list of processor-wise node bearers is not computed here because it already was
             * when the pattern of the global matrix was computed.
             *
             * \internal <b><tt>[internal]</tt></b> The only reason there is a class that encompass the two static methods is privilege access:
             * I wanted to forbid the public call of FEltStorage::GetNonCstLocalFEltSpacePerRefLocalFEltSpace(). However
             * such a data was required; so FEltStorage holds a friendship to present class to circumvent that.
             */
             struct ReduceToProcessorWise
            {


                /*!
                 * \brief Static method that performs the actual work.
                 *
                 * The enclosing class is there just to provide a simple object to befriend!
                 *
                 * \copydetails doxygen_hide_mpi_param
                 * \param[in,out] felt_space_list The list of finite elt spaces to reduce to processor-wise data.
                 * \param[in,out] mesh In input the mesh as read initially. In output, the reduced mesh
                 * that includes only GeometricElts on the local processor.
                 * \param[in] processor_wise_node_bearer_list The list of processor-wise node bearers, computed previously.
                 * \param[in,out] ghost_node_bearer_list The list of ghost node bearers, i.e. the ones that were in
                 * processor-wise finite elements but not in processor-wise node bearer list. It can also be an input
                 * parameter: if several finite element spaces are involved the same list us kept for all of them.
                 */

                static void Perform(const ::MoReFEM::Wrappers::Mpi& mpi,
                                    const FEltSpace::vector_unique_ptr& felt_space_list,
                                    const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                                    NodeBearer::vector_shared_ptr& ghost_node_bearer_list,
                                    Mesh& mesh);


            };


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup



#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_REDUCE_TO_PROCESSOR_WISE_HPP_
