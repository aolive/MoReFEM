///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 18 Dec 2015 15:36:26 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_DOF_PROGRAM_WISE_INDEX_LIST_PER_VERTEX_COORD_INDEX_LIST_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_DOF_PROGRAM_WISE_INDEX_LIST_PER_VERTEX_COORD_INDEX_LIST_HPP_

# include <memory>
# include <vector>
# include <map>
# include <unordered_map>

# include "Utilities/Containers/UnorderedMap.hpp"
# include "Utilities/UniqueId/UniqueId.hpp"


namespace MoReFEM
{



    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class FEltSpace;
    class NumberingSubset;
    class Interface;
    class Dof;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            // ============================
            // Forward declarations.
            // ============================


            class DofProgramWiseIndexListPerVertexCoordIndexListManager;


            // ============================
            // End of forward declarations.
            // ============================






            /*!
             * \brief An internal class used to init correctly a VertexMatching interpolator.
             *
             * Non conform interpolators require in their implementation data that are available BEFORE the dataset is
             * reduced to processor-wise data. Obtaining them by processor communication is both tricky to implement and
             * expensive, so the idea here is to store the data before reduction in a form that does not interfere with
             * the data reduction process.
             *
             * That's the reason current class - intended to be used solely at low level - does something I usually despise:
             * it relies upon integer indexes rather than objects. The reason for this is the reduction process rely on
             * the fact a shared_pointer is no longer useful, so I can't use it at this moment.
             *
             * A VertexMatching interpolator actually requires two such class: one for source and the other for target.
             *
             * \attention This class is intended at a very specific point of GodOfDof initialization (namely in GodOfDof::Init2(),
             * after some data have been initialized but before any processor-wise reduction occurred).
             */
            class DofProgramWiseIndexListPerVertexCoordIndexList
            : public ::MoReFEM::Crtp::UniqueId<DofProgramWiseIndexListPerVertexCoordIndexList, UniqueIdNS::AssignationMode::manual>
            {

            public:

                //! \copydoc doxygen_hide_alias_self
                using self = DofProgramWiseIndexListPerVertexCoordIndexList;

                //! Alias to unique pointer.
                using const_unique_ptr = std::unique_ptr<const self>;

                //! Friendship to manager.
                friend DofProgramWiseIndexListPerVertexCoordIndexListManager;

                //! Convenient alias to parent.
                using unique_id_parent = ::MoReFEM::Crtp::UniqueId<DofProgramWiseIndexListPerVertexCoordIndexList, UniqueIdNS::AssignationMode::manual>;

                //! Returns the name of the class.
                static const std::string& ClassName();

                /*!
                 * \brief Convenient alias for storage.
                 *
                 * Key is the list of all the \a Coords indexes considered in the interface (one for a vertex, two for
                 * an edge and more for faces and vertices).
                 * Value is the list of program-wise dofs associated to the \a Coords, represented by their index.
                 */
                using storage_type =
                    std::unordered_map
                    <
                        std::vector<unsigned int>,
                        std::vector<unsigned int>,
                        Utilities::ContainerHash
                    >;


            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.*
                 *
                 * \param[in] unique_id Unique identifier of the \a InputParameter::InitVertexMatchingInterpolator considered.
                 * \param[in] felt_space \a FEltSpace upon which list is computed.
                 * \param[in] numbering_subset \a NumberingSubset upon which list is computed.
                 */
                explicit DofProgramWiseIndexListPerVertexCoordIndexList(unsigned int unique_id,
                                                                    const FEltSpace& felt_space,
                                                                    const NumberingSubset& numbering_subset);

                //! Destructor.
                ~DofProgramWiseIndexListPerVertexCoordIndexList() = default;

                //! Copy constructor.
                DofProgramWiseIndexListPerVertexCoordIndexList(const DofProgramWiseIndexListPerVertexCoordIndexList&) = delete;

                //! Move constructor.
                DofProgramWiseIndexListPerVertexCoordIndexList(DofProgramWiseIndexListPerVertexCoordIndexList&&) = delete;

                //! Copy affectation.
                DofProgramWiseIndexListPerVertexCoordIndexList& operator=(const DofProgramWiseIndexListPerVertexCoordIndexList&) = delete;

                //! Move affectation.
                DofProgramWiseIndexListPerVertexCoordIndexList& operator=(DofProgramWiseIndexListPerVertexCoordIndexList&&) = delete;

                ///@}

            public:

                //! Finite element space considered.
                const FEltSpace& GetFEltSpace() const noexcept;

                //! Numbering subset considered.
                const NumberingSubset& GetNumberingSubset() const noexcept;

                //! Number of (program-wise) dofs.
                unsigned int NprogramWisedof() const noexcept;

                /*!
                 * \brief Compute the number of processor-wise dofs.
                 *
                 * \attention This method must be called once all GodOfDofs are fully initialized (so NOT in the constructor
                 * for instance).
                 *
                 * \return Number of processor-wise dofs.
                 */
                unsigned int ComputeNprocessorWisedof() const;

                /*!
                 * \brief Constant access to the actual storage.
                 *
                 * Key is the index of the Coords (which must be a vertex)
                 * and values are the program-wise indexes of the dofs.
                 *
                 * \return Actual storage.
                 */
                const storage_type& GetDofProgramWiseIndexPerCoordIndexList() const noexcept;


            private:

                /*!
                 * \brief Non constant access to the actual storage: key is the index of the Coords (which must be a vertex)
                 * and values are the program-wise indexes of the dofs.
                 */
                storage_type& GetNonCstDofProgramWiseIndexPerCoordIndexList() noexcept;

            private:

                //! Finite element space considered.
                const FEltSpace& felt_space_;

                //! Numbering subset considered.
                const NumberingSubset& numbering_subset_;

                /*!
                 * \brief Actual storage: key is the index of the Coords (which must be a vertex) and values are the
                 * program-wise indexes of the dofs.
                 */
                storage_type dof_program_wise_index_per_coord_index_list_;

                /*!
                 * \brief Number of (program-wise) dofs considered.
                 *
                 * \internal <b><tt>[internal]</tt></b> It should be noticed that when the object is created the number of processor-wise dofs
                 * is not known.
                 */
                unsigned int Nprogram_wise_dof_ = 0u;

            };


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/FiniteElementSpace/Internal/DofProgramWiseIndexListPerVertexCoordIndexList.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_DOF_PROGRAM_WISE_INDEX_LIST_PER_VERTEX_COORD_INDEX_LIST_HPP_
