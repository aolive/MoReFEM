///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 24 Mar 2015 14:02:10 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_BREAK_CIRCULAR_DEPENDANCY_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_BREAK_CIRCULAR_DEPENDANCY_HPP_

# include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

# include "Geometry/Mesh/Mesh.hpp"



namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GodOfDof;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            /*!
             * \class doxygen_hide_break_circular_dependancy_text
             *
             * This merely calls the \a GodOfDof namesake method, but this allows to do so without breaking
             * the cycling dependancy between \a GodOfDof and \a FEltSpace.
             *
             * \param[in] god_of_dof God of dof which member quantity is required.
             */


            /*!
             * \brief Return the id of \a god_of_dof.
             *
             * \copydoc doxygen_hide_break_circular_dependancy_text
             *
             * \return Unique identifier of the \a god_of_dof.
             */
            unsigned int GetUniqueId(const GodOfDof& god_of_dof);


            /*!
             * \brief Return the mesh of a god of dof.
             *
             * \copydoc doxygen_hide_break_circular_dependancy_text
             *
             * \return Mesh related to the \a god_of_dof.
             */
            const Mesh& GetMesh(const GodOfDof& god_of_dof);


            /*!
             * \brief Return the mpi object of a god of dof.
             *
             * \copydoc doxygen_hide_break_circular_dependancy_text
             *
             * \return Wrappers::Mpi object associated to the \a god_of_dof.
             */
            const ::MoReFEM::Wrappers::Mpi& GetMpi(const GodOfDof& god_of_dof);


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup



#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_BREAK_CIRCULAR_DEPENDANCY_HPP_
