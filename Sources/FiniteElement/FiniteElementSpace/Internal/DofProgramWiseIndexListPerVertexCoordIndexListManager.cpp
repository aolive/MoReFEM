///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 18 Dec 2015 15:36:26 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/DofProgramWiseIndexListPerVertexCoordIndexListManager.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace FEltSpaceNS
        {
        
        
            const std::string& DofProgramWiseIndexListPerVertexCoordIndexListManager::ClassName()
            {
                static std::string ret("DofProgramWiseIndexListPerVertexCoordIndexListManager");
                return ret;
            }
            
            
            void DofProgramWiseIndexListPerVertexCoordIndexListManager::Create(const unsigned int unique_id,
                                                                           const GodOfDof& god_of_dof,
                                                                           const unsigned int felt_space_index,
                                                                           const unsigned int numbering_subset_index)
            {
                // This method will be called from all GodOfDofs, but only the once hat encompass felt_space
                // is allowed to proceed.
                if (!god_of_dof.IsFEltSpace(felt_space_index))
                    return;

                decltype(auto) felt_space = god_of_dof.GetFEltSpace(felt_space_index);
                
                #ifndef NDEBUG
                {
                    const auto god_of_dof_from_felt_space_ptr = felt_space.GetGodOfDofFromWeakPtr();
                
                    if (god_of_dof.GetUniqueId() != god_of_dof_from_felt_space_ptr->GetUniqueId())
                        return;
                }
                #endif // NDEBUG
                
                decltype(auto) numbering_subset = god_of_dof.GetNumberingSubset(numbering_subset_index);
                
                // make_unique is not accepted here: it makes the code yell about private status of the constructor
                // with both clang and gcc.
                DofProgramWiseIndexListPerVertexCoordIndexList* buf
                    = new DofProgramWiseIndexListPerVertexCoordIndexList(unique_id, felt_space, numbering_subset);
                                                                 
                auto&& ptr = DofProgramWiseIndexListPerVertexCoordIndexList::const_unique_ptr(buf);
                
                assert(ptr->GetUniqueId() == unique_id);
                
                auto&& pair = std::make_pair(unique_id, std::move(ptr));
                
                auto insert_return_value = list_.insert(std::move(pair));
                
                if (!insert_return_value.second)
                    throw Exception("Two InitVertexMatching objects can't share the same unique identifier! (namely "
                                    + std::to_string(unique_id) + ").", __FILE__, __LINE__);
            }
            
            
            const DofProgramWiseIndexListPerVertexCoordIndexList& DofProgramWiseIndexListPerVertexCoordIndexListManager
            ::GetDofProgramWiseIndexListPerVertexCoordIndexList(unsigned int unique_id) const
            {
                const auto& list = GetStorage();
                
                auto it = list.find(unique_id);
                assert(it != list.cend());
                
                assert(!(!(it->second)));
                
                return *(it->second);
            }
            
            
        } // namespace FEltSpaceNS
            
        
    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
