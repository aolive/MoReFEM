///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 7 May 2014 13:48:55 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_IMPL_x_COMPUTE_DOF_INDEXES_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_IMPL_x_COMPUTE_DOF_INDEXES_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            namespace Impl
            {


                template<TypeDofIndex TypeDofIndexT, DoProduceDofList DoProduceDofListT>
                void ComputeDofIndexesHelper
                ::Perform(const NodeBearer::vector_shared_ptr& node_bearer_list,
                          DofNumberingScheme dof_numbering_scheme,
                          const NumberingSubset::const_shared_ptr& numbering_subset_ptr,
                          unsigned int& current_dof_index,
                          Dof::vector_shared_ptr& complete_dof_list)
                {
                    assert(dof_numbering_scheme == DofNumberingScheme::contiguous_per_node
                           && "Other schemes not yet implemented (need to be discussed in code meeting.");

                    for (const auto& node_bearer_ptr : node_bearer_list)
                    {
                        assert(!(!node_bearer_ptr));
                        const auto& node_bearer = *node_bearer_ptr;

                        const auto& node_list = node_bearer.GetNodeList();

                        for (const auto& node_ptr : node_list)
                        {
                            assert(!(!node_ptr));
                            const auto& node = *node_ptr;

                            // Filter out nodes not encompassed by numbering subset if this quantity is
                            // relevant (otherwise it is nullptr...)
                            if (!(!numbering_subset_ptr))
                            {
                                if (!node.DoBelongToNumberingSubset(*numbering_subset_ptr))
                                    continue;
                            }

                            const auto& dof_list = node.GetDofList();

                            # ifndef NDEBUG
                            std::vector<unsigned int> debug_numbering_subset_index_list;
                            {
                                // Prepare the list of numbering subsets to give to each dof in \a dof_list.
                                // This is only a dev/debug tool.
                                const auto& ns_list = node.GetNumberingSubsetList();

                                for (const auto& ns : ns_list)
                                    debug_numbering_subset_index_list.push_back(ns->GetUniqueId());
                            }
                            # endif // NDEBUG


                            for (auto& dof_ptr : dof_list)
                            {
                                assert(!(!dof_ptr));

                                auto& dof = *dof_ptr;

                                # ifndef NDEBUG
                                dof.numbering_subset_index_list_ = debug_numbering_subset_index_list;
                                # endif //  NDEBUG

                                switch(TypeDofIndexT)
                                {
                                    case TypeDofIndex::program_wise_per_numbering_subset:
                                        assert(!(!numbering_subset_ptr));
                                        dof.SetProgramWiseIndex(*numbering_subset_ptr, current_dof_index);
                                        break;
                                    case TypeDofIndex::processor_wise_per_numbering_subset:
                                        assert(!(!numbering_subset_ptr));
                                        dof.SetProcessorWiseOrGhostIndex(*numbering_subset_ptr, current_dof_index);
                                        break;
                                    case TypeDofIndex::processor_wise:
                                        assert(!numbering_subset_ptr && "No numbering subset are expected for this case!");
                                        dof.SetInternalProcessorWiseOrGhostIndex(current_dof_index);
                                        break;

                                }

                                switch(dof_numbering_scheme)
                                {
                                    case DofNumberingScheme::contiguous_per_node:
                                        current_dof_index++;
                                        break;
                                }

                                if (DoProduceDofListT == DoProduceDofList::yes)
                                    complete_dof_list.push_back(dof_ptr);
                            }
                        }
                    }
                }


            } // namespace Impl


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_IMPL_x_COMPUTE_DOF_INDEXES_HXX_
