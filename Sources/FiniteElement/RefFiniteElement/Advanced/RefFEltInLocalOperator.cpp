///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 6 Apr 2016 14:47:48 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "ThirdParty/Wrappers/Seldon/SeldonFunctions.hpp"
#include "ThirdParty/Wrappers/Seldon/MatrixOperations.hpp"

#include "FiniteElement/RefFiniteElement/Advanced/RefFEltInLocalOperator.hpp"


namespace MoReFEM
{
    
    
    namespace Advanced
    {
    
        
        RefFEltInLocalOperator::RefFEltInLocalOperator(const Internal::RefFEltNS::RefFEltInFEltSpace& ref_felt,
                                                       const unsigned int index_first_node_in_elementary_data,
                                                       const unsigned int index_first_dof_in_elementary_data)
        : ref_felt_(ref_felt),
        index_first_dof_(index_first_dof_in_elementary_data),
        Ndof_per_component_(ref_felt.Ndof() / ref_felt.Ncomponent())
        {
            const unsigned int Ndof = ref_felt.Ndof();
            const unsigned int Ncomponent = ref_felt.Ncomponent();
            
            assert(Ndof % Ncomponent == 0u);
            const auto Ndof_per_component = NdofPerComponent();
            
            const auto Nnode = ref_felt.Nnode();
            
            local_node_indexes_ = Wrappers::Seldon::ContiguousSequence(index_first_node_in_elementary_data,
                                                                       Nnode);
            
            local_dof_indexes_ = Wrappers::Seldon::ContiguousSequence(index_first_dof_in_elementary_data,
                                                                      Ndof);
            
            for (unsigned int i = 0; i < Ncomponent; ++i)
                local_dof_indexes_per_component_.push_back(Wrappers::Seldon
                                                           ::ContiguousSequence(index_first_dof_in_elementary_data + i * Ndof_per_component,
                                                                                Ndof_per_component));
            
            const auto& ref_felt_space_dim = ref_felt.GetFEltSpaceDimension();
            
            InitLocalMatrixStorage(
                                   {
                                       {
                                           { Nnode, ref_felt_space_dim }
                                       }
                                   });
            
            InitLocalVectorStorage(
                                   {
                                       {
                                           Nnode
                                       }
                                   });
        }
        
        
        
        const LocalMatrix& ExtractSubMatrix(const LocalMatrix& full_matrix,
                                            const RefFEltInLocalOperator& ref_felt)
        {
            auto& ret = ref_felt.GetLocalMatrix<0>(); // mutable...
            
            assert(full_matrix.GetM() >= ret.GetM());
            assert(full_matrix.GetN() >= ret.GetN());

            const auto& row_range = ref_felt.GetLocalNodeIndexList();
            
            const auto ref_felt_space_dim = static_cast<int>(ref_felt.GetFEltSpaceDimension());
            
            #ifndef NDEBUG
            Wrappers::Seldon::AssertBlockRowValidity(full_matrix, row_range);
            #endif // NDEBUG
            
            assert(static_cast<int>(ref_felt.Nnode()) == row_range.GetM());
            assert(ref_felt_space_dim == ret.GetN());
            
            const auto Nrow = row_range.GetM();
            
            for (int i = 0; i < Nrow; ++i)
            {
                auto row_index = row_range(i);
                
                for (int component = 0; component < ref_felt_space_dim; ++component)
                    ret(i, component) = full_matrix(row_index, component);
            }
            
            return ret;
       }
        
        
        const LocalVector& ExtractSubVector(const LocalVector& full_vector,
                                            const RefFEltInLocalOperator& ref_felt)
        {
            auto& ret = ref_felt.GetLocalVector<0>(); // mutable...
            
            assert(full_vector.GetM() >= ret.GetM());
            
            const auto& row_range = ref_felt.GetLocalNodeIndexList();
            
            #ifndef NDEBUG
            Wrappers::Seldon::AssertBlockValidity(full_vector, row_range);
            #endif // NDEBUG

            const auto Nrow = row_range.GetM();
            
            for (int i = 0; i < Nrow; ++i)
            {
                auto row_index = row_range(i);
                ret(i) = full_vector(row_index);
            }
            
            return ret;
        }

    
    } // namespace Advanced
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
