///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 5 Nov 2014 14:25:32 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_GEOMETRY_BASED_BASIC_REF_F_ELT_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_GEOMETRY_BASED_BASIC_REF_F_ELT_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace RefFEltNS
        {


            namespace Impl
            {


                template<class TopologyT>
                [[noreturn]] LocalCoords ComputeCenterOfGravity(const std::vector<LocalCoords>& ,
                                                                          std::false_type)
                {
                    assert(false && "Should never be called in runtime; just there to prevent compilation failure!");
                    exit(-1);
                }


                template<class TopologyT, class InterfaceContentT>
                LocalCoords ComputeCenterOfGravity(const std::vector<LocalCoords>& vertex_list,
                                                             const InterfaceContentT& vertex_on_interface_index_list)
                {
                    # ifndef NDEBUG
                    const unsigned int Nvertex = TopologyT::Nvertex;

                    assert(vertex_list.size() == Nvertex);
                    assert(std::all_of(vertex_on_interface_index_list.cbegin(),
                                       vertex_on_interface_index_list.cend(),
                                       [](unsigned int index)
                                       {
                                           return index < Nvertex;
                                       }));
                    # endif // NDEBUG

                    std::vector<LocalCoords> vertex_on_interface;
                    vertex_on_interface.reserve(vertex_on_interface_index_list.size());

                    for (unsigned int vertex_in_interface_index : vertex_on_interface_index_list)
                        vertex_on_interface.push_back(vertex_list[vertex_in_interface_index]);

                    return ComputeCenterOfGravity(vertex_on_interface);
                }




            } // namespace Impl



            template
            <
                class TopologyT,
                class ShapeFunctionT,
                InterfaceNS::Nature HigherInterfaceConnectedT,
                unsigned int NdiscontinuousLocalNodeT
            >
            GeometryBasedBasicRefFElt<TopologyT, ShapeFunctionT, HigherInterfaceConnectedT, NdiscontinuousLocalNodeT>
            ::GeometryBasedBasicRefFElt()
            {
                static_assert(HigherInterfaceConnectedT != InterfaceNS::Nature::undefined,
                              "HigherInterfaceConnectedT must be vertex, edge, face or volume.");

                assert(static_cast<int>(TopologyT::GetInteriorInterface()) > static_cast<int>(HigherInterfaceConnectedT)
                       && "Interior must not be connected to neighbors!");

                Init<TopologyT>();
            }


            template
            <
                class TopologyT,
                class ShapeFunctionT,
                InterfaceNS::Nature HigherInterfaceConnectedT,
                unsigned int NdiscontinuousLocalNodeT
            >
            LocalNode::vector_const_shared_ptr
            GeometryBasedBasicRefFElt<TopologyT, ShapeFunctionT, HigherInterfaceConnectedT, NdiscontinuousLocalNodeT>
            ::ComputeLocalNodeList()
            {
                LocalNode::vector_const_shared_ptr local_node_list;

                const auto& local_vertex_coords = TopologyT::GetVertexLocalCoordsList();
                assert(local_vertex_coords.size() == TopologyT::Nvertex);
                const unsigned int Nvertex = TopologyT::Nvertex;

                unsigned int current_local_node_index = 0u;

                using Topology = RefGeomEltNS::TopologyNS::LocalData<TopologyT>;

                if (static_cast<int>(HigherInterfaceConnectedT) >= static_cast<int>(InterfaceNS::Nature::vertex))
                {

                    for (unsigned int i = 0u; i < Nvertex; ++i)
                    {
                        auto&& local_interface = Topology::ComputeLocalVertexInterface(i);

                        auto&& local_node_ptr = std::make_shared<LocalNode>(std::move(local_interface),
                                                                            current_local_node_index,
                                                                            local_vertex_coords[i]);

                        ++current_local_node_index;

                        local_node_list.emplace_back(std::move(local_node_ptr));
                    }

                    assert(current_local_node_index == Nvertex);


                    if (static_cast<int>(HigherInterfaceConnectedT) >= static_cast<int>(InterfaceNS::Nature::edge))
                    {
                        const unsigned int Nedge = TopologyT::Nedge;

                        for (unsigned int i = 0u; i < Nedge; ++i)
                        {
                            // Compute here the coordinates of the middle of an edge.
                            const auto& vertex_on_edge_index_list = Topology::GetEdge(i);
                            assert(Topology::NverticeInEdge(i) == 2ul);

                            auto center_of_gravity =
                                Impl::ComputeCenterOfGravity<TopologyT>(local_vertex_coords,
                                                                        vertex_on_edge_index_list);

                            auto&& local_interface = Topology::ComputeLocalEdgeInterface(i);

                            auto&& local_node_ptr = std::make_shared<LocalNode>(std::move(local_interface),
                                                                                current_local_node_index++,
                                                                                center_of_gravity);

                            local_node_list.emplace_back(std::move(local_node_ptr));
                        }


                        assert(current_local_node_index == Nvertex + Nedge);

                        if (static_cast<int>(HigherInterfaceConnectedT) >= static_cast<int>(InterfaceNS::Nature::face))
                        {
                            const unsigned int Nface = TopologyT::Nface;

                            for (unsigned int i = 0u; i < Nface; ++i)
                            {
                                // Compute here the coordinates of the center of gravity of a face.
                                const auto& vertex_on_face_index_list = Topology::GetFace(i);

                                auto center_of_gravity =
                                    Impl::ComputeCenterOfGravity<TopologyT>(local_vertex_coords,
                                                                            vertex_on_face_index_list);

                                auto&& local_interface = Topology::ComputeLocalFaceInterface(i);


                                auto&& local_node_ptr = std::make_shared<LocalNode>(std::move(local_interface),
                                                                                    current_local_node_index++,
                                                                                    center_of_gravity);

                                local_node_list.emplace_back(std::move(local_node_ptr));
                            }

                            assert(current_local_node_index == Nvertex + Nedge + Nface);
                        }
                    }
                }


                // Now add the discontinuous local nodes (if any).
                {
                    if (NdiscontinuousLocalNodeT > 0)
                    {

                        const auto center_of_gravity = ComputeCenterOfGravity(local_vertex_coords);

                        for (unsigned int i = 0; i < NdiscontinuousLocalNodeT; ++i)
                        {
                            auto&& local_interface = Topology::ComputeLocalInteriorInterface();

                            auto&& local_node_ptr = std::make_shared<LocalNode>(std::move(local_interface),
                                                                                current_local_node_index++,
                                                                                center_of_gravity);

                            local_node_list.emplace_back(std::move(local_node_ptr));
                        }
                    }
                }



                return local_node_list;
            }


            template
            <
                class TopologyT,
                class ShapeFunctionT,
                InterfaceNS::Nature HigherInterfaceConnectedT,
                unsigned int NdiscontinuousLocalNodeT
            >
            inline double
            GeometryBasedBasicRefFElt<TopologyT, ShapeFunctionT, HigherInterfaceConnectedT, NdiscontinuousLocalNodeT>
            ::ShapeFunction(unsigned int local_node_index,
                            const LocalCoords& local_coords) const
            {
                return ShapeFunctionT::ShapeFunction(local_node_index, local_coords);
            }


            template
            <
                class TopologyT,
                class ShapeFunctionT,
                InterfaceNS::Nature HigherInterfaceConnectedT,
                unsigned int NdiscontinuousLocalNodeT
            >
            inline double
            GeometryBasedBasicRefFElt<TopologyT, ShapeFunctionT, HigherInterfaceConnectedT, NdiscontinuousLocalNodeT>
            ::FirstDerivateShapeFunction(unsigned int local_node_index,
                                         unsigned int component,
                                         const LocalCoords& local_coords) const
            {
                return ShapeFunctionT::FirstDerivateShapeFunction(local_node_index, component, local_coords);
            }



            template
            <
                class TopologyT,
                class ShapeFunctionT,
                InterfaceNS::Nature HigherInterfaceConnectedT,
                unsigned int NdiscontinuousLocalNodeT
            >
            inline unsigned int
            GeometryBasedBasicRefFElt<TopologyT, ShapeFunctionT, HigherInterfaceConnectedT, NdiscontinuousLocalNodeT>
            ::GetOrder() const noexcept
            {
                return ShapeFunctionT::Order;
            }


        } // namespace RefFEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_GEOMETRY_BASED_BASIC_REF_F_ELT_HXX_
