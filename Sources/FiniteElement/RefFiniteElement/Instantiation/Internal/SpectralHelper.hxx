///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 29 Sep 2014 12:13:46 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_SPECTRAL_HELPER_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_SPECTRAL_HELPER_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace RefFEltNS
        {


            template<unsigned int NI, unsigned int NJ, unsigned int NK>
            std::array<unsigned int, 3u> ComputeIntegerCoordinates(unsigned int local_node_index)
            {
                assert(local_node_index < (NI + 1u) * (NJ + 1u) * (NK + 1u));
                const unsigned int m = local_node_index % ((NI + 1u) * (NJ + 1u));

                std::array<unsigned int, 3u> ret
                {
                    {
                        m % (NI + 1u),
                        m / (NJ + 1u),
                        local_node_index / ((NI + 1u) * (NJ + 1u))
                    }
                };

                return ret;
            }




            template <class TopologyT, unsigned int NI, unsigned int NJ, unsigned int NK>
            std::string GenerateShapeFunctionLabel()
            {
                std::ostringstream oconv;
                const char letter =
                    std::is_same<TopologyT, ::MoReFEM::RefGeomEltNS::TopologyNS::Segment>() ? 'P' : 'Q';

                constexpr unsigned int dimension = TopologyT::dimension;

                oconv << letter << NI;

                switch (dimension)
                {
                    case 1u:
                        // Nothing to add.
                        break;
                    case 2u:
                        // Check for anisotropy.
                        if (NI != NJ)
                            oconv << '-' << letter << NJ;
                        break;
                    case 3u:
                        // Check for anisotropy.
                        if (NI != NJ || NI != NK)
                            oconv << '-' << letter << NJ << '-' << letter << NK;
                        break;
                    default:
                        assert(false && "Should not happen!");
                        exit(EXIT_FAILURE);
                }

                const std::string ret(oconv.str());
                return ret;
            }



        } // namespace RefFEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_SPECTRAL_HELPER_HXX_
