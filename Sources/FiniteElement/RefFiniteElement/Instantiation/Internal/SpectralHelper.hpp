///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 29 Sep 2014 12:13:46 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_SPECTRAL_HELPER_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_SPECTRAL_HELPER_HPP_

# include <cassert>
# include <vector>
# include <array>
# include <string>
# include <sstream>

# include "Geometry/RefGeometricElt/Instances/Segment/Topology/Segment.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace RefFEltNS
        {

            /*!
             *
             * \class doxygen_hide_spectral_helper_args
             *
             * \param[in] pos The lagrangian basis are built upon the points \a pos. This list gets the
             * same size as the number of local nodes.
             * \param[in] local_node_index Local node for which interpolation is computed.
             * \param[in] p Position for which the interpolation is computed.
             *
             * \internal <b><tt>[internal]</tt></b> Implementation is directly taken from Ondomatic, with
             * just a bit of reformatting.
             */


            /*!
             *
             * \class doxygen_hide_spectral_helper_NI_NJ_NK
             *
             * \tparam NI number of local nodes to consider along x axis.
             * \tparam NJ number of local nodes to consider along y axis.
             * \tparam NK number of local nodes to consider along z axis.
             */



            /*!
             * \brief Generates the I, J and K position of a node given its local node index.
             *
             * \copydoc doxygen_hide_spectral_helper_NI_NJ_NK
             *
             * \param[in] local_node_index Index of the local node for which computation occurs.
             *
             * \return (I, J, K) position of a node given its \a local_node_index.
             */
            template
            <
                unsigned int NI,
                unsigned int NJ,
                unsigned int NK
            >
            std::array<unsigned int, 3u> ComputeIntegerCoordinates(unsigned int local_node_index);


            /*!
             * \brief 1D Interpolation at \a p of the shape function \a phi.
             *
             * \copydoc doxygen_hide_spectral_helper_args
             *
             * \return Interpolated value.
             */
            double Interpolation(const std::vector<double>& pos,
                                 double p,
                                 unsigned int local_node_index);


            /*!
             * \brief 1D Interpolation at 'p' of the derivative of a shape function 'phi'.
             *
             * \copydoc doxygen_hide_spectral_helper_args
             *
             * \return Interpolated value.
             */
            double DerivativeInterpolation(const std::vector<double>& pos,
                                           double p,
                                           size_t local_node_index);


            /*!
             * \brief Generate the most adequate shape function label.
             *
             * \tparam TopologyT Topology considered.
             * \copydoc doxygen_hide_spectral_helper_NI_NJ_NK
             *
             * \return Shape function label.
             *
             * The convention is:
             * - A 'P' or a 'Q'
             * - Followed by one figure if isotropy, e.g. 'P4' or 'Q6'.
             * - Followed by figures separated by a '-'if anisotropy, e.g. 'P2-3' or 'Q5-4-7'.
             */
            template
            <
                class TopologyT,
                unsigned int NI,
                unsigned int NJ,
                unsigned int NK
            >
            std::string GenerateShapeFunctionLabel();


        } // namespace RefFEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/RefFiniteElement/Instantiation/Internal/SpectralHelper.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_SPECTRAL_HELPER_HPP_
