///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Sep 2014 14:25:09 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_SPECTRAL_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_SPECTRAL_HXX_


namespace MoReFEM
{


    namespace RefFEltNS
    {



        template <class TopologyT, unsigned int NI, unsigned int NJ, unsigned int NK>
        const std::string& Spectral<TopologyT, NI, NJ, NK>::ShapeFunctionLabel()
        {
            static std::string ret = Internal::RefFEltNS::GenerateShapeFunctionLabel<TopologyT, NI, NJ, NK>();
            return ret;
        }


        template <class TopologyT, unsigned int NI, unsigned int NJ, unsigned int NK>
        Spectral<TopologyT, NI, NJ, NK>::Spectral()
        {
            this->template Init<TopologyT>();
        }


        template <class TopologyT, unsigned int NI, unsigned int NJ, unsigned int NK>
        double Spectral<TopologyT, NI, NJ, NK>::ShapeFunction(unsigned int local_node_index,
                                                              const LocalCoords& local_coords) const
        {
            const std::array<unsigned int, 3u> shape_function_index_list =
                Internal::RefFEltNS::ComputeIntegerCoordinates<NI, NJ, NK>(local_node_index);

            const unsigned int Ncomponent = local_coords.GetDimension();

            double ret = 1.;

            for (unsigned int index = 0u; index < Ncomponent; ++index)
                ret *= Internal::RefFEltNS::Interpolation(points_per_ijk_[index],
                                                          local_coords[index],
                                                          shape_function_index_list[index]);


            return ret;
        }


        template <class TopologyT, unsigned int NI, unsigned int NJ, unsigned int NK>
        double Spectral<TopologyT, NI, NJ, NK>::FirstDerivateShapeFunction(unsigned int local_node_index,
                                                                           unsigned int icoor,
                                                                           const LocalCoords& local_coords) const
        {
            const std::array<unsigned int, 3u> shape_function_index_list =
                Internal::RefFEltNS::ComputeIntegerCoordinates<NI, NJ, NK>(local_node_index);

            double ret = 1.;

            const unsigned int Ncomponent = local_coords.GetDimension();

            for (unsigned int index = 0u; index < Ncomponent; ++index)
            {
                if (icoor == index)
                    ret *= Internal::RefFEltNS::DerivativeInterpolation(points_per_ijk_[index],
                                                            local_coords[index],
                                                            shape_function_index_list[index]);
                else
                    ret *= Internal::RefFEltNS::Interpolation(points_per_ijk_[index],
                                                  local_coords[index],
                                                  shape_function_index_list[index]);
            }

            return ret;
        }



        /////////////////////
        // PRIVATE METHODS //
        /////////////////////

        template<class TopologyT, unsigned int NI, unsigned int NJ, unsigned int NK>
        LocalNode::vector_const_shared_ptr Spectral<TopologyT, NI, NJ, NK>
        ::ComputeLocalNodeList()
        {
//            static_assert(std::is_same<TopologyT, RefGeomEltNS::TopologyNS::Hexahedron>() ||
//                          std::is_same<TopologyT, RefGeomEltNS::TopologyNS::Quadrangle>() ||
//                          std::is_same<TopologyT, RefGeomEltNS::TopologyNS::Segment>(),
//                          "Spectral finite element doesn't make sense for other topologies.");
//
//            static_assert(!(std::is_same<TopologyT, RefGeomEltNS::TopologyNS::Segment>() && (NJ != 0u || NK != 0u)),
//                          "Spectral upon segment can't be defined with NJ or NK > 0!");
//
//            static_assert(!(std::is_same<TopologyT, RefGeomEltNS::TopologyNS::Quadrangle>() && NK != 0u),
//                          "Spectral upon quadrangle can't be defined with NK > 0!");
//

            LocalNode::vector_const_shared_ptr local_node_list;

            unsigned int Ncomponent = 0;

            assert(std::all_of(points_per_ijk_.cbegin(), points_per_ijk_.cend(),
                               [](const std::vector<double>& vector)
                               {
                                   return vector.empty();
                               })
                   && "This container is initialized in the present method which should be called only once!");

            // The local coords of the local dofs are computed with the Gauss-Lobatto formula.
            std::array<unsigned int, 3> Ngauss { { NI, NJ, NK } };
            std::array<std::vector<double>, 3> weights; // I don't care about weight in present function;
            // only there for Gauss-Lobatto function prototype.


            for (std::size_t i = 0ul; i < 3ul; ++i)
            {
                if (Ngauss[i] > 0)
                {
                    QuadratureNS::ComputeGaussFormulas<QuadratureNS::GaussFormula::Gauss_Lobatto>(Ngauss[i] + 1,
                                                                                                  points_per_ijk_[i],
                                                                                                  weights[i]);
                    Ncomponent = static_cast<unsigned int>(i) + 1u;
                }
                else
                    points_per_ijk_[i] = { 0. };
            }


            // Create here all the local nodes.
            for (unsigned int k = 0u; k <= NK; ++k)
            {
                const unsigned int is_k_border = (k == 0u || k == NK ? 1u : 0u);

                for (unsigned int j = 0u; j <= NJ; ++j)
                {
                    const unsigned int is_j_border = (j == 0u || j == NJ ? 1u : 0u);

                    for (unsigned int i = 0u; i <= NI; ++i)
                    {
                        const unsigned int index = k * (NJ + 1u) * (NI + 1u) + j * (NI + 1u) + i;
                        const unsigned int Nborder = is_k_border + is_j_border + (i == 0u || i == NI ? 1u : 0u);

                        InterfaceNS::Nature nature = InterfaceNS::Nature::none;

                        LocalCoords::unique_ptr local_coords_ptr(nullptr);

                        switch (Ncomponent)
                        {
                            case 1u:
                            {
                                std::vector<double> buf {points_per_ijk_[0][i]};
                                local_coords_ptr = std::make_unique<LocalCoords>(buf);
                                break;
                            }
                            case 2u:
                            {
                                std::vector<double> buf {points_per_ijk_[0][i], points_per_ijk_[1][j]};
                                local_coords_ptr = std::make_unique<LocalCoords>(buf);
                                break;
                            }
                            case 3u:
                            {
                                std::vector<double> buf {
                                    points_per_ijk_[0][i],
                                    points_per_ijk_[1][j],
                                    points_per_ijk_[2][k]
                                };
                                local_coords_ptr = std::make_unique<LocalCoords>(buf);
                                break;
                            }
                            default:
                            {
                                assert(false);
                                break;
                            }

                        }

                        assert(!(!local_coords_ptr));
                        const auto& local_coords = *local_coords_ptr;


                        switch (Nborder)
                        {
                            case 0u:
                                nature = InterfaceNS::Nature::volume;
                                break;
                            case 1u:
                                nature = InterfaceNS::Nature::face;
                                break;
                            case 2u:
                                nature = InterfaceNS::Nature::edge;
                                break;
                            case 3u:
                                nature = InterfaceNS::Nature::vertex;
                                break;
                            default:
                                assert(false);
                        }

                        assert(nature != InterfaceNS::Nature::none);

                        auto&& local_interface =
                            RefGeomEltNS::TopologyNS::LocalData<TopologyT>::FindLocalInterface(nature, local_coords);


                        auto local_node_ptr = std::make_shared<const LocalNode>(std::move(local_interface),
                                                                                index,
                                                                                local_coords);

                        local_node_list.push_back(local_node_ptr);
                    }
                }
            }

            assert(local_node_list.size() == (NI + 1u) * (NJ + 1u) * (NK + 1u));

            return local_node_list;
        }


        template <class TopologyT, unsigned int NI, unsigned int NJ, unsigned int NK>
        unsigned int Spectral<TopologyT, NI, NJ, NK>::GetOrder() const noexcept
        {
            return std::max({NI, NJ, NK});
        }



    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_SPECTRAL_HXX_
