///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Sep 2014 14:25:09 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_SPECTRAL_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_SPECTRAL_HPP_

# include <memory>
# include <vector>
# include <array>
# include <unordered_map>

# include "Utilities/Miscellaneous.hpp"
# include "Utilities/Containers/UnorderedMap.hpp"

# include "Geometry/RefGeometricElt/Internal/Topology/LocalData.hpp"

# include "FiniteElement/QuadratureRules/GaussQuadratureFormula.hpp"
# include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"
# include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"
# include "FiniteElement/RefFiniteElement/Instantiation/Internal/SpectralHelper.hpp"
# include "FiniteElement/Nodes_and_dofs/LocalNode.hpp"



namespace MoReFEM
{


    namespace RefFEltNS
    {


        /*!
         * \brief Defines a Spectral finite element.
         *
         * \tparam TopologyT One of the following class defined in RefGeomEltNS::TopologyNS namespace:
         * Segment, Quadrangle, Hexahedron.
         * \tparam NI Finite element order in the first direction,
         * \tparam NJ Finite element order in the second direction. Might differ from \a NI if for instance
         * considering Q3-Q2 finite elements.
         * \tparam NK Same as NJ for third direction.
         *
         * Numbering convention: local nodes are numbered by increasing (x,y,z); hence local nodes of different natures
         * are completely intertwined. For instance for a Quadrangle Q3:
         *
         *   12 13 14 15
         *    8  9 10 11
         *    4  5  6  7
         *    0  1  2  3
         *
         * So here vertices are numbered 0, 3, 12, 15, edges 1, 2, 4, 8, 7, 11, 13, 14 and faces 5, 6, 9, 10.
         */
        template <class TopologyT, unsigned int NI, unsigned int NJ, unsigned int NK>
        class Spectral : public Internal::RefFEltNS::BasicRefFElt
        {


        public:

            //! Name of the shape function used.
            static const std::string& ShapeFunctionLabel();


        public:

            //! Alias over Topology Traits class.
            using topology = TopologyT;


            /// \name Special members.
            ///@{

            /*!
             * \brief Private constructor.
             *
             * Privacy is required to ensure proper singleton behaviour.
             */
            explicit Spectral();

        public:

            //! Destructor.
            virtual ~Spectral() override = default;

            //! Copy constructor.
            Spectral(const Spectral&) = delete;

            //! Move constructor.
            Spectral(Spectral&&) = delete;

            //! Affectation.
            Spectral& operator=(const Spectral&) = delete;

            //! Affectation.
            Spectral& operator=(Spectral&&) = delete;


            ///@}


            /*!
             *
             *  \copydoc doxygen_hide_shape_function
             */
            virtual double ShapeFunction(unsigned int local_node_index,
                                         const LocalCoords& local_coords) const override final;


            /*!
             *
             *  \copydoc doxygen_hide_first_derivate_shape_function
             */
            virtual double FirstDerivateShapeFunction(unsigned int local_node_index,
                                                      unsigned int component,
                                                      const LocalCoords& local_coords) const override final;

            //! \copydoc doxygen_hide_shape_function_order_method
            virtual unsigned int GetOrder() const noexcept override final;


        private:

            /*!
             * \brief Compute the list of all local nodes to feed to Init() method.
             *
             * \internal <b><tt>[internal]</tt></b> This method should not be used outside of constructor; it is not
             * const as it sets some work data attributes.
             *
             * \return List of local nodes.
             */
            virtual LocalNode::vector_const_shared_ptr ComputeLocalNodeList() override final;


        private:

            /*!
             * \brief A work variable that stores all the first component values in a first container,
             * and so forth up to third component.
             *
             */
            std::array<std::vector<double>, 3> points_per_ijk_;


        };


    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/RefFiniteElement/Instantiation/Spectral.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_SPECTRAL_HPP_
