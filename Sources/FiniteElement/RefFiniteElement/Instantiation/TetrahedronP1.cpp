///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 20 Mar 2014 09:44:10 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "FiniteElement/RefFiniteElement/Instantiation/TetrahedronP1.hpp"
#include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hpp"


namespace MoReFEM
{


    namespace RefFEltNS
    {
        
        
        namespace // anonymous
        {
            
            
            __attribute__((unused)) const bool registered =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__).Register<TetrahedronP1>();
            
            
        } // namespace // anonymous
        
        
        const std::string& TetrahedronP1::ShapeFunctionLabel()
        {
            static std::string ret("P1");
            return ret;
        }
        
        
        TetrahedronP1::~TetrahedronP1() = default;
        
        
    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
