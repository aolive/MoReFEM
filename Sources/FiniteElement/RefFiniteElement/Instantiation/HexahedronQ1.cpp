///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "FiniteElement/RefFiniteElement/Instantiation/HexahedronQ1.hpp"
#include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hpp"


namespace MoReFEM
{
    
    
    namespace RefFEltNS
    {
        
        
        namespace // anonymous
        {
            
            // #896 obsolete with the correction of the spectral elements.
            //__attribute__((unused)) const bool registered =
            //    Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__).Register<HexahedronQ1>();
            
            
        } // namespace // anonymous
        
        
        const std::string& HexahedronQ1::ShapeFunctionLabel()
        {
            static std::string ret("Q1");
            return ret;
        }
        
        
        HexahedronQ1::~HexahedronQ1() = default;
        
        
    } // namespace RefFEltNS
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
