///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_SEGMENT_P1_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_SEGMENT_P1_HPP_

# include <memory>
# include <vector>

# include "Geometry/RefGeometricElt/Instances/Segment/Topology/Segment.hpp"
# include "Geometry/RefGeometricElt/Instances/Segment/ShapeFunction/Segment2.hpp"

# include "FiniteElement/Nodes_and_dofs/LocalNode.hpp"
# include "FiniteElement/RefFiniteElement/Instantiation/Internal/GeometryBasedBasicRefFElt.hpp"



namespace MoReFEM
{


    namespace RefFEltNS
    {


        /*!
         * \brief Reference finite element for SegmentP1.
         *
         * Numbering convention: Local nodes (all on vertices) are numbered exactly as they were on the
         * RefGeomEltNS::TopologyNS::Segment class.
         *
         * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
         */
        class SegmentP1 : public Internal::RefFEltNS::GeometryBasedBasicRefFElt
        <
            RefGeomEltNS::TopologyNS::Segment,
            RefGeomEltNS::ShapeFunctionNS::Segment2,
            InterfaceNS::Nature::vertex,
            0u
        >
        {

        public:

            //! Name of the shape function used.
            static const std::string& ShapeFunctionLabel();

        public:

            /// \name Special members.
            ///@{

            //! Constructor.
            explicit SegmentP1() = default;

            //! Destructor.
            ~SegmentP1() override;

            //! Copy constructor.
            SegmentP1(const SegmentP1&) = default;

            //! Move constructor.
            SegmentP1(SegmentP1&&) = default;

            //! Affectation.
            SegmentP1& operator=(const SegmentP1&) = default;

            //! Affectation.
            SegmentP1& operator=(SegmentP1&&) = default;


            ///@}

        };



    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_SEGMENT_P1_HPP_
