///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 20 Mar 2014 12:04:07 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include <cmath>

#include "Utilities/Containers/Print.hpp" // \todo DEV TMP #907

# include "FiniteElement/QuadratureRules/GaussQuadratureFormula.hpp"
#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"
#include "FiniteElement/QuadratureRules/Instantiation/Hexahedron.hpp"


namespace MoReFEM
{
    
    
    namespace QuadratureNS
    {
        
        
        namespace // anonymous
        {
            
            
            const std::array<QuadratureRule::const_shared_ptr, 3> CreateQuadratureRuleListPerDegreeOfExactness();
            
            
        } // namespace anonymous
        
        
        
        
        const std::array<QuadratureRule::const_shared_ptr, 3>& Hexahedron::GetPerDegreeOfExactnessList()
        {
            static const std::array<QuadratureRule::const_shared_ptr, 3> ret = CreateQuadratureRuleListPerDegreeOfExactness();
            return ret;
        }
        
        
        
        QuadratureRule::const_shared_ptr Hexahedron::GetShapeFunctionOrder(unsigned int order)
        {
            auto ret =
                std::make_shared<QuadratureRule>(std::string("hexahedron_") + std::to_string(order) + "_points",
                                                 RefGeomEltNS::TopologyNS::Type::hexahedron);

            {
                auto& n_points = *ret;
                
                std::vector<double> points;
                std::vector<double> weights;
                
                const unsigned int Ngauss_points = order + 1u;
                QuadratureNS::ComputeGaussFormulas<QuadratureNS::GaussFormula::Gauss>(Ngauss_points,
                                                                                      points,
                                                                                      weights);
                
                const auto Npoints = points.size();
                
                for (auto i = 0ul ; i < Npoints ; ++i)
                {
                    for (auto j = 0ul; j < Npoints ; ++j)
                    {
                        for (auto k = 0ul; k < Npoints ; ++k)
                            n_points.AddQuadraturePoint(LocalCoords({points[k], points[j], points[i]}),
                                                        weights[i] * weights[j] * weights[k]);
                    }
                }
            }
            
            return ret;
        }

        
        
        
        namespace // anonymous
        {
            
            
            const std::array<QuadratureRule::const_shared_ptr, 3> CreateQuadratureRuleListPerDegreeOfExactness()
            {
                auto one_point_ptr = std::make_shared<QuadratureRule>("hexahedron_1_point",
                                                                      RefGeomEltNS::TopologyNS::Type::hexahedron,
                                                                      1);
                
                {
                    one_point_ptr->AddQuadraturePoint(LocalCoords({0., 0., 0.}), 8.);
                }
                
                auto eight_points_ptr = std::make_shared<QuadratureRule>("hexahedron_8_points",
                                                                         RefGeomEltNS::TopologyNS::Type::hexahedron,
                                                                         3);
                {
                    auto& eight_points = *eight_points_ptr;
                    
                    const double q2ptx1 = - sqrt(1./3.);
                    const double q2ptx2 = -q2ptx1;
                    
                    eight_points.AddQuadraturePoint(LocalCoords({q2ptx1, q2ptx1, q2ptx1}), 1.);
                    eight_points.AddQuadraturePoint(LocalCoords({q2ptx2, q2ptx1, q2ptx1}), 1.);
                    eight_points.AddQuadraturePoint(LocalCoords({q2ptx2, q2ptx2, q2ptx1}), 1.);
                    eight_points.AddQuadraturePoint(LocalCoords({q2ptx1, q2ptx2, q2ptx1}), 1.);
                    eight_points.AddQuadraturePoint(LocalCoords({q2ptx1, q2ptx1, q2ptx2}), 1.);
                    eight_points.AddQuadraturePoint(LocalCoords({q2ptx2, q2ptx1, q2ptx2}), 1.);
                    eight_points.AddQuadraturePoint(LocalCoords({q2ptx2, q2ptx2, q2ptx2}), 1.);
                    eight_points.AddQuadraturePoint(LocalCoords({q2ptx1, q2ptx2, q2ptx2}), 1.);
                }
                
                
                auto twenty_seven_points_ptr = std::make_shared<QuadratureRule>("hexahedron_27_points",
                                                                                RefGeomEltNS::TopologyNS::Type::hexahedron,
                                                                                5);
                
                {
                    auto& twenty_seven_points = *twenty_seven_points_ptr;
                    
                    const double q3ptx1 = 0., q3ptx2 = - sqrt(3./5.) , q3ptx3 = -q3ptx2;
                    const double one_ninth = 1. / 9.;
                    const double q3ptw1 = 8. * one_ninth;
                    const double q3ptw2 = 5. * one_ninth;
                    const double q3ptw3 = q3ptw2;
                    
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx1, q3ptx1, q3ptx1}), q3ptw1 * q3ptw1 * q3ptw1);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx2, q3ptx1, q3ptx1}), q3ptw2 * q3ptw1 * q3ptw1);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx3, q3ptx1, q3ptx1}), q3ptw3 * q3ptw1 * q3ptw1);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx1, q3ptx2, q3ptx1}), q3ptw1 * q3ptw2 * q3ptw1);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx2, q3ptx2, q3ptx1}), q3ptw2 * q3ptw2 * q3ptw1);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx3, q3ptx2, q3ptx1}), q3ptw3 * q3ptw2 * q3ptw1);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx1, q3ptx3, q3ptx1}), q3ptw1 * q3ptw3 * q3ptw1);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx2, q3ptx3, q3ptx1}), q3ptw2 * q3ptw3 * q3ptw1);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx3, q3ptx3, q3ptx1}), q3ptw3 * q3ptw3 * q3ptw1);
                    
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx1, q3ptx1, q3ptx2}), q3ptw1 * q3ptw1 * q3ptw2);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx2, q3ptx1, q3ptx2}), q3ptw2 * q3ptw1 * q3ptw2);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx3, q3ptx1, q3ptx2}), q3ptw3 * q3ptw1 * q3ptw2);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx1, q3ptx2, q3ptx2}), q3ptw1 * q3ptw2 * q3ptw2);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx2, q3ptx2, q3ptx2}), q3ptw2 * q3ptw2 * q3ptw2);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx3, q3ptx2, q3ptx2}), q3ptw3 * q3ptw2 * q3ptw2);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx1, q3ptx3, q3ptx2}), q3ptw1 * q3ptw3 * q3ptw2);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx2, q3ptx3, q3ptx2}), q3ptw2 * q3ptw3 * q3ptw2);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx3, q3ptx3, q3ptx2}), q3ptw3 * q3ptw3 * q3ptw2);
                    
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx1, q3ptx1, q3ptx3}), q3ptw1 * q3ptw1 * q3ptw3);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx2, q3ptx1, q3ptx3}), q3ptw2 * q3ptw1 * q3ptw3);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx3, q3ptx1, q3ptx3}), q3ptw3 * q3ptw1 * q3ptw3);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx1, q3ptx2, q3ptx3}), q3ptw1 * q3ptw2 * q3ptw3);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx2, q3ptx2, q3ptx3}), q3ptw2 * q3ptw2 * q3ptw3);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx3, q3ptx2, q3ptx3}), q3ptw3 * q3ptw2 * q3ptw3);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx1, q3ptx3, q3ptx3}), q3ptw1 * q3ptw3 * q3ptw3);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx2, q3ptx3, q3ptx3}), q3ptw2 * q3ptw3 * q3ptw3);
                    twenty_seven_points.AddQuadraturePoint(LocalCoords({q3ptx3, q3ptx3, q3ptx3}), q3ptw3 * q3ptw3 * q3ptw3);
                    
                }
                
                assert(one_point_ptr->NquadraturePoint() == 1u);
                assert(eight_points_ptr->NquadraturePoint() == 8u);
                assert(twenty_seven_points_ptr->NquadraturePoint() == 27u);
                
                
                return
                {
                    {
                        one_point_ptr,
                        eight_points_ptr,
                        twenty_seven_points_ptr
                    }
                };
            }
            
            
            
            
        } // namespace anonymous

        
        
    } //  namespace QuadratureNS
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
