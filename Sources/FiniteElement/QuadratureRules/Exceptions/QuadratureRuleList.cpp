///
////// \file
///
///
/// Created by Sebastien Gilles <srpgilles@gmail.com> on the Tue, 5 Feb 2013 11:59:16 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include <sstream>
#include "FiniteElement/QuadratureRules/Exceptions/QuadratureRuleList.hpp"



namespace // anonymous
{
    // Forward declarations here; definitions are at the end of the file
    
    std::string InvalidDegreeMsg(unsigned int degree, unsigned int max_degree);
    
    
} // namespace anonymous


namespace MoReFEM
{
    
    
    namespace ExceptionNS
    {
        
        
        namespace QuadratureRuleListNS
        {
            
            
            Exception::~Exception() = default;
            
            
            Exception::Exception(const std::string& msg, const char* invoking_file, int invoking_line)
            : MoReFEM::Exception(msg, invoking_file, invoking_line)
            { }
            
            
            InvalidDegree::~InvalidDegree() = default;
            
            
            InvalidDegree::InvalidDegree(unsigned int degree, unsigned int max_degree, const char* invoking_file, int invoking_line)
            : Exception(InvalidDegreeMsg(degree, max_degree), invoking_file, invoking_line)
            { }
            
            
        } // namespace QuadratureRuleListNS
        
        
    } // namespace ExceptionNS
    
    
} // namespace MoReFEM



namespace // anonymous
{
    
    
    // Definitions of functions defined at the beginning of the file
    std::string InvalidDegreeMsg(unsigned int degree, unsigned int max_degree)
    {
        std::ostringstream oconv;
        oconv << "A quadrature rule for degree " << degree << " has been required but the maximum degree of exactness "
        "available for an element of this type is " << max_degree << '.';
        
        return oconv.str();
    }
    
    
} // namespace anonymous


/// @} // addtogroup FiniteElementGroup
