///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 29 Sep 2014 10:40:17 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_ENUM_GAUSS_QUADRATURE_FORMULA_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_ENUM_GAUSS_QUADRATURE_FORMULA_HPP_


namespace MoReFEM
{


    namespace QuadratureNS
    {


        /// \addtogroup FiniteElementGroup
        ///@{


        /*!
         * \brief Variants of Gauss quadrature formula available.
         */
        enum class GaussFormula
        {
            Gauss,
            Gauss_Lobatto
        };


    } // namespace QuadratureNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_ENUM_GAUSS_QUADRATURE_FORMULA_HPP_
