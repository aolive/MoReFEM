///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 6 Apr 2016 13:24:21 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_INTERNAL_x_GAUSS_QUADRATURE_FORMULA_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_INTERNAL_x_GAUSS_QUADRATURE_FORMULA_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace GaussQuadratureNS
        {





        } // namespace GaussQuadratureNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_INTERNAL_x_GAUSS_QUADRATURE_FORMULA_HXX_
