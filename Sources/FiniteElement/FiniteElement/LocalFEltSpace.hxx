///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 24 Apr 2014 15:48:01 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_x_LOCAL_F_ELT_SPACE_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_x_LOCAL_F_ELT_SPACE_HXX_


namespace MoReFEM
{



    inline const GeometricElt& LocalFEltSpace::GetGeometricElt() const noexcept
    {
        assert(!(!geometric_elt_));
        return *geometric_elt_;
    }


    inline GeometricElt::shared_ptr LocalFEltSpace::GetGeometricEltPtr() const noexcept
    {
        assert(!(!geometric_elt_));
        return geometric_elt_;
    }


    inline const FElt& LocalFEltSpace::GetFElt(const ExtendedUnknown& extended_unknown) const
    {
        auto it = std::find_if(felt_list_.cbegin(),
                               felt_list_.cend(),
                               [&extended_unknown](const auto& felt_ptr)
                               {
                                   assert(!(!felt_ptr));
                                   return felt_ptr->GetExtendedUnknown() == extended_unknown;
                               });
        assert(it != felt_list_.cend());
        return *(*it);

    }


    inline const FElt& LocalFEltSpace::GetFElt(const Unknown& unknown) const
    {
        auto it = std::find_if(felt_list_.cbegin(),
                               felt_list_.cend(),
                               [&unknown](const auto& felt_ptr)
                               {
                                   assert(!(!felt_ptr));
                                   return felt_ptr->GetExtendedUnknown().GetUnknown() == unknown;
                               });
        assert(it != felt_list_.cend());
        return *(*it);
    }


    inline FElt& LocalFEltSpace::GetNonCstFElt(const ExtendedUnknown& extended_unknown)
    {
        return const_cast<FElt&>(GetFElt(extended_unknown));
    }


    inline const FElt::vector_shared_ptr& LocalFEltSpace::GetFEltList() const noexcept
    {
        return felt_list_;
    }


    inline FElt::vector_shared_ptr& LocalFEltSpace::GetNonCstFEltList() noexcept
    {
        return const_cast<FElt::vector_shared_ptr&>(GetFEltList());
    }


    inline const NodeBearer::vector_shared_ptr& LocalFEltSpace::GetNodeBearerList() const noexcept
    {
        assert(!node_bearer_list_.empty());
        return node_bearer_list_;
    }


    inline const Internal::RefFEltNS::RefLocalFEltSpace& LocalFEltSpace::GetRefLocalFEltSpace() const noexcept
    {
        return ref_felt_space_;
    }


    template<MpiScale MpiScaleT>
    inline const Internal::FEltNS::Local2GlobalStorage& LocalFEltSpace
    ::GetLocal2GlobalStorageForNumberingSubset(const NumberingSubset& numbering_subset) const
    {
        const auto numbering_subset_id = numbering_subset.GetUniqueId();

        const auto& local2global_storage = GetLocal2GlobalStorage<MpiScaleT>();

        const auto it = std::find_if(local2global_storage.cbegin(),
                                     local2global_storage.cend(),
                                     [numbering_subset_id](const auto& pair)
                                     {
                                         return pair.first == numbering_subset_id;
                                     });

        assert(it != local2global_storage.cend());
        assert(!(!it->second));

        return *(it->second);
    }



    template<MpiScale MpiScaleT>
    inline Internal::FEltNS::Local2GlobalStorage&
    LocalFEltSpace
    ::GetNonCstLocal2GlobalStorageForNumberingSubset(const NumberingSubset& numbering_subset)
    {
        return const_cast<Internal::FEltNS::Local2GlobalStorage&>(GetLocal2GlobalStorageForNumberingSubset<MpiScaleT>(numbering_subset));
    }



    template<MpiScale MpiScaleT>
    const std::vector<PetscInt>& LocalFEltSpace
    ::GetLocal2Global(const ExtendedUnknown::vector_const_shared_ptr& unknown_list) const
    {
        assert(!unknown_list.empty());

        assert(std::none_of(unknown_list.cbegin(),
                            unknown_list.cend(),
                            Utilities::IsNullptr<ExtendedUnknown::const_shared_ptr>));

        const auto& numbering_subset = unknown_list.back()->GetNumberingSubset();

        assert(std::all_of(unknown_list.cbegin(),
                           unknown_list.cend(),
                           [&numbering_subset](const auto& unknown_ptr)
                           {
                               return unknown_ptr->GetNumberingSubset() == numbering_subset;
                           }) && "All unknowns in the list should belong to the same numbering subset!");


        const auto& local2global_storage = GetLocal2GlobalStorageForNumberingSubset<MpiScaleT>(numbering_subset);

        return local2global_storage.GetLocal2Global(unknown_list);
    }


    template<MpiScale MpiScaleT>
    const std::vector<PetscInt>& LocalFEltSpace
    ::GetLocal2Global(const ExtendedUnknown& unknown) const
    {
        const auto& local2global_storage = GetLocal2GlobalStorageForNumberingSubset<MpiScaleT>(unknown.GetNumberingSubset());

        return local2global_storage.GetLocal2Global(unknown);
    }


    template<MpiScale MpiScaleT>
    inline const std::vector<std::pair<unsigned int, Internal::FEltNS::Local2GlobalStorage::unique_ptr>>&
    LocalFEltSpace::GetLocal2GlobalStorage() const noexcept
    {
        switch(MpiScaleT)
        {
            case MpiScale::program_wise:
                return local_2_global_program_wise_per_numbering_subset_;
            case MpiScale::processor_wise:
                return local_2_global_processor_wise_per_numbering_subset_;
        }

        assert(false);
        return local_2_global_program_wise_per_numbering_subset_; // to avoid warnings
    }





} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_x_LOCAL_F_ELT_SPACE_HXX_
