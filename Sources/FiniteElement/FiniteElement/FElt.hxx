///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 24 Apr 2014 15:48:01 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_x_F_ELT_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_x_F_ELT_HXX_


namespace MoReFEM
{



    inline const ExtendedUnknown& FElt::GetExtendedUnknown() const noexcept
    {
        return GetRefFElt().GetExtendedUnknown();
    }


    inline const Node::vector_shared_ptr& FElt::GetNodeList() const noexcept
    {
        assert(!node_list_.empty() && "As this container is used only to compute the Local2Global, it is "
               "erased once it's done.");
        return node_list_;
    }


    inline const Internal::RefFEltNS::RefFEltInFEltSpace& FElt::GetRefFElt() const noexcept
    {
        return ref_felt_;
    }



} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_x_F_ELT_HXX_
