///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Apr 2016 13:48:56 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_DOF_STORAGE_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_DOF_STORAGE_HPP_

# include <memory>
# include <vector>
# include <cassert>

# include "Utilities/Containers/PointerComparison.hpp"
# include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"

# include "FiniteElement/Nodes_and_dofs/Dof.hpp"


namespace MoReFEM
{



    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class DirichletBoundaryCondition;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================



    namespace Internal
    {


        namespace BoundaryConditionNS
        {


            /*!
             * \brief Store there for a pair boundary condition/numbering subset the dofs involved in
             * the boundary condition and their associated values.
             */
            class DofStorage
            {

            public:

                //! \copydoc doxygen_hide_alias_self
                using self = DofStorage;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Alias to vector of unique pointers.
                using vector_unique_ptr = std::vector<unique_ptr>;

                /*!
                 * \brief Friendship to the class that owns it.
                 */
                friend DirichletBoundaryCondition;


            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] numbering_subset Numbering subset for which current object is built.
                 * \param[in] value_per_dof Value of the boundary condition for each dof inside.
                 */
                explicit DofStorage(const NumberingSubset& numbering_subset,
                                                     Utilities::PointerComparison::Map<Dof::shared_ptr, PetscScalar>&& value_per_dof);

                //! Default constructor, for cases in which numbering subset is relevant but no dofs are on current processor.
                DofStorage() = default;

                //! Destructor.
                ~DofStorage() = default;

                //! Copy constructor.
                DofStorage(const DofStorage&) = delete;

                //! Move constructor.
                DofStorage(DofStorage&&) = default;

                //! Copy affectation.
                DofStorage& operator=(const DofStorage&) = delete;

                //! Move affectation.
                DofStorage& operator=(DofStorage&&) = delete;

                ///@}

                //! Get the list of all the dofs relevant for boundary condition and numbering subset.
                const Dof::vector_shared_ptr& GetDofList() const noexcept;

                //! Get the list of all dof program-wise index (within the numbering subset for which current class exists).
                const std::vector<PetscInt>& GetProgramWiseDofIndexList() const noexcept;

                //! Get the list of all dof processor-wise index (within the numbering subset for which current class exists).
                const std::vector<unsigned int>& GetProcessorWiseDofIndexList() const noexcept;


                //! Get the list of all dof values.
                const std::vector<PetscScalar>& GetDofValueList() const noexcept;

                /*!
                 * \brief Update the values to apply to the dofs in the boundary condition.
                 *
                 * \param[in] new_values New values to apply. This vector should be the sme size as \a dof_value_list_.
                 * The legwork to create \a new_values is performed by DirichletBoundaryCondition class (the values are
                 * extracted from a GlobalVector).
                 */
                void UpdateValueList(std::vector<PetscScalar>&& new_values);

            private:

                //! Get the list of all dof values.
                std::vector<PetscScalar>& GetNonCstDofValueList() noexcept;


            private:

                /*!
                 * \brief List of all dofs in the combination boundary condition/numbering subset.
                 *
                 */
                Dof::vector_shared_ptr dof_list_;

                //! List of all dof program-wise index (within the numbering subset for which current class exists).
                std::vector<PetscInt> program_wise_dof_index_list_;

                //! List of all dof processor-wise index (within the numbering subset for which current class exists).
                std::vector<unsigned int> processor_wise_dof_index_list_;

                //! List of all dof values.
                std::vector<PetscScalar> dof_value_list_;

            };



        } // namespace BoundaryConditionNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/BoundaryConditions/Internal/DofStorage.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_DOF_STORAGE_HPP_
