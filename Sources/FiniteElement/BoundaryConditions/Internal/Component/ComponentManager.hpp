///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 4 Apr 2016 23:42:27 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPONENT_x_COMPONENT_MANAGER_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPONENT_x_COMPONENT_MANAGER_HPP_


# include <string>
# include <bitset>
# include <cassert>

# include <memory>


namespace MoReFEM
{


    namespace Internal
    {


        namespace BoundaryConditionNS
        {


            /*!
             * \brief Helper class to handle which components are to be used in an essential boundary condition.
             *
             * This allows in particular to for instance consider only the second component of a vectorial unknown.
             *
             * \internal <b><tt>[internal]</tt></b> Management of components should probably be overhauled completely
             * at some point; current class is still heavily inspired by Felisce interface. However so far it worked
             * for all the cases we considered and so it's quite low in our current todo list.
             */
            class ComponentManager
            {
            public:


                //! Shared smart pointer.
                using const_shared_ptr = std::shared_ptr<const ComponentManager>;

                //! Constructor.
                explicit ComponentManager(const std::string& name, const std::bitset<3>& is_activated);

                //! Destructor
                ~ComponentManager() = default;

                //! Copy constructor.
                ComponentManager(const ComponentManager&) = delete;

                //! Move constructor.
                ComponentManager(ComponentManager&&) = delete;

                //! Copy affectation.
                ComponentManager& operator=(const ComponentManager&) = delete;

                //! Move affectation.
                ComponentManager& operator=(ComponentManager&&) = delete;


                //! Returns the name of the component.
                const std::string& Name() const;

                //! Returns whether the \a i -th component is active.
                bool IsActive(unsigned int i) const;

                //! Returns the number of active components.
                unsigned int Nactive() const;

                //! Return as a string the components available, separated by a comma (e.g. "x, y").
                std::string AsString() const;

                /*!
                 * \brief Return the position of the \a i -th active component in the bitset (in C numbering).
                 *
                 *
                 * For instance, if bitset = 101 (and remember bitsets are read from the right to the left!):
                 * \code
                 * ActiveComponent(0); // yields 0
                 * ActiveComponent(1); // yields 2
                 * ActiveComponent(2); // ERROR!
                 * \endcode
                 *
                 * \param[in] i Index of the active component to consider; should be in [0, Nactive_component[.
                 *
                 * \return Which is the \a i -th active component.
                 */
                unsigned int ActiveComponent(unsigned int i) const;



            private:

                //! Name of the component, e.g. "Comp12".
                std::string name_;

                /*!
                 * \brief Which component are activated.
                 *
                 * BEWARE: a bitset is read from the right to the left... So "100" means z component is active, not x one.
                 */
                std::bitset<3> is_activated_;

            };


        } // namespace BoundaryConditionNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/BoundaryConditions/Internal/Component/ComponentManager.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPONENT_x_COMPONENT_MANAGER_HPP_
