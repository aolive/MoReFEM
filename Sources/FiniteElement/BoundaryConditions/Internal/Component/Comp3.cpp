///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 4 Apr 2016 23:36:01 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "FiniteElement/BoundaryConditions/Internal/Component/Comp3.hpp"
#include "FiniteElement/BoundaryConditions/Internal/Component/ComponentFactory.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace BoundaryConditionNS
        {
            
            
            namespace // anonymous
            {
                
                
                ComponentManager::const_shared_ptr Create()
                {
                    return std::make_unique<Comp3>();
                }
                
                
                
                // Register the geometric element in the 'ComponentFactory' singleton.
                // The return value is mandatory: we can't simply call a void function outside function boundaries
                // See "Modern C++ Design", Chapter 8, P205.
                __attribute__((unused)) const bool registered =
                    Internal::BoundaryConditionNS::ComponentFactory::CreateOrGetInstance(__FILE__, __LINE__).Register<Comp3>(Create);
                
                
            } // anonymous namespace
            
            
            
            Comp3::Comp3() = default;
            
            
            
        } // namespace BoundaryConditionNS
        
        
    } // namespace Internal
    
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
