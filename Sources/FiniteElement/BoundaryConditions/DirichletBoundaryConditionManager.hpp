///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 24 Sep 2015 14:23:26 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_DIRICHLET_BOUNDARY_CONDITION_MANAGER_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_DIRICHLET_BOUNDARY_CONDITION_MANAGER_HPP_


# include <memory>
# include <vector>
# include <map>

# include "Utilities/Singleton/Singleton.hpp"

# include "Core/InputParameterData/InputParameterList.hpp"
# include "Core/InputParameter/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"

# include "Geometry/Domain/DomainManager.hpp"

# include "FiniteElement/Unknown/UnknownManager.hpp"
# include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"



namespace MoReFEM
{


        /// \addtogroup FiniteElementGroup
        ///@{


        /*!
         * \brief Singleton class which is aware of all essential boundary conditions that might be considered within
         * a model.
         *
         * These conditions are defined in the input parameter file.
         */
        class DirichletBoundaryConditionManager final
        : public Utilities::Singleton<DirichletBoundaryConditionManager>
        {

        public:

            //! Return the name of the class.
            static const std::string& ClassName();

            //! Base type of DirichletBoundaryCondition as input parameter (requested to identify domains in the input parameter data).
            using input_parameter_type = InputParameter::BaseNS::DirichletBoundaryCondition;


        public:

            /// \name Special members
            ///@{

            //! Destructor.
            ~DirichletBoundaryConditionManager() = default;

            //! Copy constructor.
            DirichletBoundaryConditionManager(const DirichletBoundaryConditionManager&) = delete;

            //! Move constructor.
            DirichletBoundaryConditionManager(DirichletBoundaryConditionManager&&) = default;

            //! Affectation.
            DirichletBoundaryConditionManager& operator=(const DirichletBoundaryConditionManager&) = delete;

            //! Affectation.
            DirichletBoundaryConditionManager& operator=(DirichletBoundaryConditionManager&&) = delete;

            ///@}

        public:


            /*!
             * \brief Create a new DirichletBoundaryCondition object from the data of the input parameter file.
             *
             * \param[in] section Section dedicated to a BoundaryCondition in the
             * input parameter file.
             *
             */
            template<class DirichletBoundaryConditionSectionT>
            void Create(const DirichletBoundaryConditionSectionT& section);

            //! Returns the number of boundary conditions.
            unsigned int NboundaryCondition() const noexcept;

            //! Get the boundary condition associated with \a unique_id.
            const DirichletBoundaryCondition& GetDirichletBoundaryCondition(unsigned int unique_id) const noexcept;

            //! Get the boundary condition associated with \a unique_id.
            DirichletBoundaryCondition& GetNonCstDirichletBoundaryCondition(unsigned int unique_id) noexcept;

            //! Get the boundary condition associated with \a unique_id as a smart pointer.
            DirichletBoundaryCondition::shared_ptr GetDirichletBoundaryConditionPtr(unsigned int unique_id) const;


            //! Get the boundary condition associated with \a name.
            const DirichletBoundaryCondition& GetDirichletBoundaryCondition(const std::string& name) const noexcept;

            //! Get the boundary condition associated with \a unique_id.
            DirichletBoundaryCondition& GetNonCstDirichletBoundaryCondition(const std::string& name) noexcept;

            //! Get the boundary condition associated with \a name as a smart pointer.
            DirichletBoundaryCondition::shared_ptr GetDirichletBoundaryConditionPtr(const std::string& name) const;

            //! Access to the list of boundary conditions.
            const DirichletBoundaryCondition::vector_shared_ptr& GetList() const noexcept;




        private:

            /*!
             *
             * \copydetails doxygen_hide_boundary_condition_constructor_args
             */
            void Create(unsigned int unique_id,
                        const std::string& name,
                        const Domain& domain,
                        const Unknown& unknown,
                        const std::vector<double>& value_per_component,
                        const std::string& component,
                        const bool is_mutable,
                        const bool may_overlap);

        private:

            /// \name Singleton requirements.
            ///@{

            //! Constructor.
            DirichletBoundaryConditionManager() = default;

            //! Friendship declaration to Singleton template class (to enable call to constructor).
            friend class Utilities::Singleton<DirichletBoundaryConditionManager>;
            ///@}


        private:

            //! Register properly boundary condition in use.
            void RegisterDirichletBoundaryCondition(const DirichletBoundaryCondition::shared_ptr& boundary_condition);


        private:

            //! List of boundary conditions.
            DirichletBoundaryCondition::vector_shared_ptr boundary_condition_list_;

        };

        /*!
         * \brief Clear the initial values of all essential boundary conditions.
         *
         * At the beginning of a model, boundary conditions are initialized with a value read in the input parameter
         * file. This value is retained for homogeneous boundary condition, and modified for other cases; whichever
         * the case the initial value is not needed anymore, as the storage is different in all cases. So it's possible
         * to spare (very few) memory by dropping now unused values.
         */
        void ClearAllBoundaryConditionInitialValueList();


        ///@} // \addtogroup


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_DIRICHLET_BOUNDARY_CONDITION_MANAGER_HPP_
