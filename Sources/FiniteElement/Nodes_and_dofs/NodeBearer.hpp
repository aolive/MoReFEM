///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Dec 2013 14:06:14 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_NODE_BEARER_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_NODE_BEARER_HPP_


# include <memory>
# include <map>
# include <vector>

# include "Geometry/Coords/Coords.hpp"
# include "Geometry/Interfaces/Internal/TInterface.hpp"
# include "FiniteElement/Unknown/ExtendedUnknown.hpp"
# include "FiniteElement/Nodes_and_dofs/Node.hpp"



namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GodOfDof;


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            class CreateNodeListHelper;
            class MatrixPattern;


            namespace Impl
            {


                struct ComputeDofIndexesHelper;


            } // namespace Impl


        } // namespace FEltSpaceNS


    } // namespace Internal


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    /// \addtogroup FiniteElementGroup
    ///@{

    //! Enum to list all the possible choices available to number the dofs.
    enum class DofNumberingScheme
    {

        contiguous_per_node  //! < Means that all the dofs born by a same node share contiguous indexes.
//        contiguous_per_component
    };


    /*!
     *
     * \brief Returns the way dofs are numbered.
     *
     * \return the way dofs are numbered.
     *
     * DEV TMP #256 Put that in the input parameter file!
     */
    DofNumberingScheme CurrentDofNumberingScheme();


    /*!
     * \brief A NodeBearer is created whenever some dofs are located on a given geometric Interface.
     *
     * So for instance if in a mesh there are triangles P1:
     * - If we consider P1 finite elements, dofs are all located on vertices. So NodeBearer objects will be created
     * for each Vertex object that bears a dof.
     * - If we consider P2 finite elements, there are also dofs on edges. Therefore additional NodeBearer objects are
     * created for each Edge object that bears a dof.
     *
     * Of course, the same is true for Face or Volume interfaces.
     *
     * There is only one NodeBearer for a given interface; there is further classification within NodeBearer objects
     * (Dofs are grouped per Node).
     *
     */
    class NodeBearer final : public std::enable_shared_from_this<NodeBearer>
    {
    public:

        //! Shared smart pointer.
        using shared_ptr = std::shared_ptr<NodeBearer>;

        //! Vector of shared smart pointers.
        using vector_shared_ptr = std::vector<shared_ptr>;

        //! Friendship to give access to internal storage.
        friend struct Internal::FEltSpaceNS::Impl::ComputeDofIndexesHelper;
        friend class Internal::FEltSpaceNS::MatrixPattern;
        friend class GodOfDof;
        friend class Internal::FEltSpaceNS::CreateNodeListHelper;


    public:

        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] interface Interface onto which the node is built.
         *
         * \internal <b><tt>[internal]</tt></b> index is given as a std::size_t as in practice it will be the size of the list
         * that will be passed to this constructor:
         * \code
         * node_list_.push_back(std::make_shared<Node>(interface, node_list_.size()));
         * \endcode
         *
         */
        explicit NodeBearer(const Interface::shared_ptr& interface);

        //! Destructor.
        ~NodeBearer() = default;

        //! Recopy constructor (deleted).
        NodeBearer(const NodeBearer&) = delete;

        //! Move constructor (deleted).
        NodeBearer(NodeBearer&& ) = delete;

        //! operator= (deleted).
        NodeBearer& operator=(const NodeBearer&) = delete;

        //! operator= (deleted).
        NodeBearer& operator=(NodeBearer&&) = delete;

        ///@}

        //! Get index.
        unsigned int GetIndex() const noexcept;

        /*!
         * \brief Returns the list of \a Node matching the given \a unknown and \a shape_function_label.
         *
         * \internal <b><tt>[internal]</tt></b> This method is expected to be used only in init phase, hence the brutal
         * return by value of a vector.
         * \internal No restriction upon \a NumberingSubset in purpose; that's the reason \a ExtendedUnknown is not used.
         * A same \a Node might be defined upon several \a NumberingSubset.
         *
         * \param[in] unknown \a Unknown for which the list of \a Node is sought.
         * \param[in] shape_function_label Shape function label for which the list of \a Node is sought.
         *
         * \return List of \a Node related to \a unknown.
         *
         * \internal This method is only called in the initialization phase, and it should remain that way: it involves
         * allocating and fillind a std::vector on the fly.
         */
        Node::vector_shared_ptr GetNodeList(const Unknown& unknown,
                                            const std::string& shape_function_label) const;

        //! Set the processor to which the node bearer belongs to.
        void SetProcessor(unsigned int processor);

        //! Get the processor to which the node bearer belongs to.
        unsigned int GetProcessor() const noexcept;

        //! Returns the nature of the node.
        InterfaceNS::Nature GetNature() const noexcept;

        //! Returns the number of dofs born by the node.
        unsigned int Ndof() const;

        //! Returns the number of dofs born by the node inside the \a numbering_subset.
        unsigned int Ndof(const NumberingSubset& numbering_subset) const;

        //! Whether an unknown is present or not.
        bool IsUnknown(const Unknown& unknown) const;

        //! Returns the interface on which the node is located.
        const Interface& GetInterface() const noexcept;

        //! Set the index.
        void SetIndex(std::size_t index);

        //! Number of Nodes for a couple Unknown/Shape function label.
        unsigned int Nnode(const Unknown& unknown, const std::string& shape_function_label) const;

        //! Whether there are nodes in the node bearer.
        bool IsEmpty() const noexcept;

        //! Return the list of nodes.
        const Node::vector_shared_ptr& GetNodeList() const noexcept;


    private:

        /*!
         * \brief Add a new node.
         *
         * \internal <b><tt>[internal]</tt></b> It is assumed here the node does not exist yet; such liabilities are
         * handled by the class in charge of actually calling such methods (\a CreateNodeListHelper).
         *
         * \param[in] extended_unknown Couple \a Unknown / \a Numbering subset for which the node is/are created.
         * \param[in] Ndof Number of dofs to create on the node.
         *
         * \return Node newly created.
         */
        Node::shared_ptr AddNode(const ExtendedUnknown& extended_unknown,
                                 unsigned int Ndof);


    private:

        //! An internal index that is useful during partitioning steps.
        unsigned int index_;

        /*!
         * \brief Processor that holds the node.
         *
         * Once data have been reduced to processor wise, this data might still be useful: ghost nodes
         * keep existing!
         */
        unsigned int processor_;

        //! List of nodes.
        Node::vector_shared_ptr node_list_;

        //! Interface that bears the node.
        Interface::shared_ptr interface_;
    };


    //! Overload operator<: two nodes are sort according to their respective index.
    bool operator<(const NodeBearer& node1, const NodeBearer& node2);

    //! Overload operator== two nodes are sort according to their respective index.
    bool operator==(const NodeBearer& node1, const NodeBearer& node2);


    ///@} // \addtogroup


} // namespace MoReFEM


namespace std
{


    //! Provide hash function for NodeBearer::shared_ptr.
    template<>
    struct hash<MoReFEM::NodeBearer::shared_ptr>
    {
    public:

        //! Overload.
        std::size_t operator()(const MoReFEM::NodeBearer::shared_ptr& ptr) const
        {
            assert(!(!ptr));
            return std::hash<unsigned int>()(ptr->GetIndex());
        }
    };


} // namespace std


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/Nodes_and_dofs/NodeBearer.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_NODE_BEARER_HPP_
