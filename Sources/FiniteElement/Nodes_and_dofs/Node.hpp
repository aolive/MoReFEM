///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 9 Apr 2014 15:38:05 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_NODE_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_NODE_HPP_

# include <memory>
# include <vector>
# include <cassert>

# include "FiniteElement/Unknown/Unknown.hpp"
# include "FiniteElement/Nodes_and_dofs/Dof.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class ExtendedUnknown;
    class NodeBearer;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    /// \addtogroup FiniteElementGroup
    ///@{


    /*!
     * \brief A Node is an ensemble of Dofs located at the same place and addressing the same unknown and shape function
     * label.
     *
     * \internal A node doesn't store directly an \a ExtendedUnknown: if the \a Unknown and the \a ShapeFunctionLabel
     * are obvious, a same \a Node might be registered in several \a FEltSpace for different \a NumberingSubset.
     */
    class Node final
    {
    public:

        //! Shared pointer.
        using shared_ptr = std::shared_ptr<Node>;

        //! Vector of shared pointers.
        using vector_shared_ptr = std::vector<shared_ptr>;

    public:

        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] extended_unknown Unknown addressed by all the dofs in the Node
         * (e.g. 'displacement', 'pressure'). It is here an extended unknown because when it is created it is
         * within a given numbering subset, however it's important to remember a given Node might be related to many
         * numbering subsets in the end (some may be added through \a RegisterNumberingSubset()).
         * \param[in] node_bearer_ptr Pointer to the node bearer onto which the \a Node is created.
         * \param[in] Ndof Number of dofs to create.
         */
        explicit Node(const std::shared_ptr<const NodeBearer>& node_bearer_ptr,
                      const ExtendedUnknown& extended_unknown,
                      unsigned int Ndof);

        //! Destructor.
        ~Node() = default;

        //! Deactivate copy constructor.
        Node(const Node&) = delete;

        //! Deactivate move constructor.
        Node(Node&&) = delete;

        //! Deactivate copy affectation.
        Node& operator=(const Node&) = delete;

        //! Deactivate move affectation.
        Node& operator=(Node&&) = delete;


        ///@}

        //! Get the unknown.
        const Unknown& GetUnknown() const noexcept;

        //! Get the shape function label.
        const std::string& GetShapeFunctionLabel() const noexcept;

        //! Get the list of dofs.
        const Dof::vector_shared_ptr& GetDofList() const noexcept;

        //! Returns the \a i -th dof of the list.
        const Dof& GetDof(unsigned int i) const;

        //! Returns the \a i -th dof of the list as a smart pointer.
        const Dof::shared_ptr& GetDofPtr(unsigned int i) const;

        //! Returns the number of dofs.
        unsigned int Ndof() const noexcept;

        /*!
         * \brief Register a numbering subset for the node if it does not exist yet; do nothing otherwise.
         */
        void RegisterNumberingSubset(NumberingSubset::const_shared_ptr numbering_subset);

        //! Returns whether the node belongs to the \a numbering_subset.
        bool DoBelongToNumberingSubset(const NumberingSubset& numbering_subset) const;

        //! Return the list of numbering subset.
        const NumberingSubset::vector_const_shared_ptr& GetNumberingSubsetList() const noexcept;

    private:

        //! Unknown.
        const Unknown& unknown_;

        //! Shape function.
        const std::string& shape_function_label_;

        //! List of numbering subsets to which the node belongs.
        NumberingSubset::vector_const_shared_ptr numbering_subset_list_;

        //! List of dofs.
        Dof::vector_shared_ptr dof_list_;
    };


    //! Operator<: compare unknown first and shape_function_label in case of same unknown.
    bool operator<(const Node& lhs, const Node& rhs);

    //! Operator==: compare both unknown and shape_function_label.
    bool operator==(const Node& lhs, const Node& rhs);


    ///@} // \addtogroup


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/Nodes_and_dofs/Node.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_NODE_HPP_
