///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Dec 2013 12:31:10 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_DOF_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_DOF_HXX_


namespace MoReFEM
{


    inline unsigned int Dof::GetInternalProcessorWiseOrGhostIndex() const
    {
        assert(internal_processor_wise_or_ghost_index_ != NumericNS::UninitializedIndex<unsigned int>());
        return internal_processor_wise_or_ghost_index_;
    }


    inline bool operator<(const Dof& dof1, const Dof& dof2)
    {
        return (dof1.GetInternalProcessorWiseOrGhostIndex() < dof2.GetInternalProcessorWiseOrGhostIndex());
    }


    inline bool operator==(const Dof& dof1, const Dof& dof2)
    {
        return (dof1.GetInternalProcessorWiseOrGhostIndex() == dof2.GetInternalProcessorWiseOrGhostIndex());
    }


    inline const Dof::index_per_numbering_subset_type& Dof::GetProcessorWiseOrGhostIndexPerNumberingSubset() const
    {
        return processor_wise_or_ghost_index_per_numbering_subset_;
    }


    inline const Dof::index_per_numbering_subset_type& Dof::GetProgramWiseIndexPerNumberingSubset() const
    {
        return program_wise_index_per_numbering_subset_;
    }


    inline std::shared_ptr<const NodeBearer> Dof::GetNodeBearerFromWeakPtr() const
    {
        assert(!node_bearer_.expired());
        return node_bearer_.lock();
    }



} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_DOF_HXX_
