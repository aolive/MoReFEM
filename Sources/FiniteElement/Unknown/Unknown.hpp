///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Dec 2013 12:31:10 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_HPP_


# include <memory>
# include <cassert>
# include <string>
# include <vector>

# include "Utilities/UniqueId/UniqueId.hpp"

# include "FiniteElement/Unknown/EnumUnknown.hpp"
# include "Core/NumberingSubset/NumberingSubset.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class Mesh;
    class UnknownManager;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    /*!
     * \brief Very generic informations about a given unknown.
     *
     *
     */
    class Unknown : public Crtp::UniqueId<Unknown, UniqueIdNS::AssignationMode::manual>
    {

    public:

        //! Name of the class.
        static const std::string& ClassName();

        //! Alias to shared pointer.
        using const_shared_ptr = std::shared_ptr<const Unknown>;

        //! Alias to vector of shared pointers.
        using vector_const_shared_ptr = std::vector<const_shared_ptr>;

        //! Alias to the unique id parent.
        using unique_id_parent = Crtp::UniqueId<Unknown, UniqueIdNS::AssignationMode::manual>;

        //! Friendship.
        friend class UnknownManager;


        /// \name Special members.
        ///@{

    private:

        /*!
         * \brief Constructor.
         *
         * Private as unknown should be created by UnknownManager.
         *
         * \copydetails doxygen_hide_unknown_constructor_args
         */
        explicit Unknown(const std::string& name,
                         unsigned int unique_id,
                         const UnknownNS::Nature nature);

    public:

        //! Destructor.
        ~Unknown() = default;

        //! Copy constructor.
        Unknown(const Unknown&) = delete;

        //! Move constructor.
        Unknown(Unknown&&) = delete;

        //! Copy affectation.
        Unknown& operator=(const Unknown&) = delete;

        //! Move affectation.
        Unknown& operator=(Unknown&&) = delete;


        ///@}


    public:

        //! Nature of the unknown.
        UnknownNS::Nature GetNature() const noexcept;

        //! Get the name of the variable.
        const std::string& GetName() const noexcept;

    private:

        //! Name of the variable.
        const std::string name_;

        //! Nature of the unknown.
        const UnknownNS::Nature nature_;
    };


    //! Criterion used here is the underlying unique id.
    bool operator<(const Unknown& unknown1, const Unknown& unknown2);

    //! Criterion used here is the underlying unique id.
    bool operator==(const Unknown& unknown1, const Unknown& unknown2);

    //! Criterion used here is the underlying unique id.
    bool operator!=(const Unknown& unknown1, const Unknown& unknown2);

    //! Number of components to consider for the unknown.
    unsigned int Ncomponent(const Unknown& unknown,
                            const unsigned int mesh_dimension);


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/Unknown/Unknown.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_HPP_
