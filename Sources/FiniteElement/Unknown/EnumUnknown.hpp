///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 26 Mar 2015 15:49:50 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_ENUM_UNKNOWN_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_ENUM_UNKNOWN_HPP_


namespace MoReFEM
{


    namespace UnknownNS
    {


        /*!
         * \brief Possible values for the nature of a given Unknown.
         */
        enum class Nature
        {
            scalar = 0u,
            vectorial = 1u
        };


    } // namespace UnknownNS


} // namespace MoReFEM




namespace std
{

    //! \copydoc doxygen_hide_std_stream_out_overload
    std::ostream& operator<<(std::ostream& stream, const ::MoReFEM::UnknownNS::Nature rhs);


} // namespace std


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_ENUM_UNKNOWN_HPP_
