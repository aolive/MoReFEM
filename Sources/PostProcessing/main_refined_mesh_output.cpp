/// \file
///
///
/// Created by Federica Caforio <federica.caforio@inria.fr> on the Fri, 13 May 2016 15:12:32 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Core/InputParameterData/InputParameterList.hpp"
#include "Core/InputParameter/Geometry/Mesh.hpp"
#include "Core/InputParameter/Result.hpp"
#include "Core/InputParameterData/Advanced/SetFromInputParameterData.hpp"

#include "Geometry/Mesh/Internal/MeshManager.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/MovemeshHelper.hpp"



#include "PostProcessing/PostProcessing.hpp"
#include "PostProcessing/RefineMeshQuadranglesSpectral/Model.hpp"

#include "Utilities/UniqueId/UniqueId.hpp"

//#include "ModelInstances/main_refined_mesh_output/Model.hpp"
//#include "ModelInstances/main_refined_mesh_output/InputParameterList.hpp"


using namespace MoReFEM;


int main(int argc, char** argv)
{
    
    using namespace MoReFEM;
    
    
    try
    {
        
              
        //! \copydoc doxygen_hide_input_parameter_tuple
        using InputParameterTuple = std::tuple
        <
            InputParameter::TimeManager,
        
            InputParameter::Mesh<1>,
        
            InputParameter::FEltSpace<1>,
        
            InputParameter::Domain<1>,
        
            InputParameter::Unknown<1>,
        
            InputParameter::NumberingSubset<1>,
        
            InputParameter::Result
        >;
        
        //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = InputParameterList<InputParameterTuple>;

        using morefem_data_type =
            MoReFEMData<InputParameterList, Utilities::InputParameterListNS::DoTrackUnusedFields::no>;

        morefem_data_type morefem_data(argc, argv);
        
        const auto& mpi = morefem_data.GetMpi();
        
        try
        {
            RefineMeshNS::Model<morefem_data_type> model(morefem_data);
            model.Run(create_output_dir::no);  
            std::cout << "End of Post-Processing. Refined mesh created." << std::endl;
        }
        catch(const std::exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());
        }
        catch(Seldon::Error& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.What());
        }
    }
    catch(const std::exception& e)
    {
        std::ostringstream oconv;
        oconv << "Exception caught from MoReFEMData<InputParameterList>: " << e.what() << std::endl;
        std::cout << oconv.str();
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;

}

