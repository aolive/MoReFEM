///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 17 Dec 2014 14:46:10 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup PostProcessingGroup
/// \addtogroup PostProcessingGroup
/// \{

#ifndef MOREFEM_x_POST_PROCESSING_x_FILE_x_INTERFACE_FILE_HPP_
# define MOREFEM_x_POST_PROCESSING_x_FILE_x_INTERFACE_FILE_HPP_

# include <map>
# include <vector>
# include <string>

# include "Geometry/Interfaces/EnumInterface.hpp"
# include "Geometry/Coords/Coords.hpp"

# include "PostProcessing/Data/Interface.hpp"


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        /*!
         * \brief Class which holds the informations obtained from interfaces.hhdata output file.
         *
         * There is one such file per mesh.
         *
         * This file gives for each interface (vertex, edge, face or volume)  the list of \a Coords that delimit it.
         *
         * \attention Some of the methods of this class were implemented for debug purposes, and are not necessarily
         * very efficient (noticeably accessors to interface). If they are to be used extensively in some post
         * processing informations, there are many ways to improve them tremendously.
         */
        class InterfaceFile final
        {

        public:

            //! Alias to unique_ptr.
            using const_unique_ptr = std::unique_ptr<const InterfaceFile>;


        public:

            /// \name Special members.
            ///@{

            //! Constructor.
            explicit InterfaceFile(const std::string& input_file);

            //! Destructor.
            ~InterfaceFile() = default;

            //! Copy constructor.
            InterfaceFile(const InterfaceFile&) = delete;

            //! Move constructor.
            InterfaceFile(InterfaceFile&&) = delete;

            //! Affectation.
            InterfaceFile& operator=(const InterfaceFile&) = delete;

            //! Affectation.
            InterfaceFile& operator=(InterfaceFile&&) = delete;

            ///@}


            //! Return the number of a given type of interface.
            template<InterfaceNS::Nature NatureT>
            unsigned int Ninterface() const;

            //! Return the adequate interface.
            const Data::Interface& GetInterface(InterfaceNS::Nature interface_nature,
                                                unsigned int interface_index) const;

            //! Return the vertex interface that matches a given coord_index.
            const Data::Interface& GetVertexInterfaceFromCoordIndex(unsigned int coord_index) const;

            //! Return the face interface that matches a list of coords.
            const Data::Interface& GetFaceInterfaceFromCoordsList(const Coords::vector_raw_ptr& coords_list) const;


            /*!
             * \brief Computes here all the interfaces that encompass a given Coords.
             */
            std::vector<Data::Interface*> ComputeInterfaceWithCoords(InterfaceNS::Nature interface_nature,
                                                                     unsigned int coords_index) const;

        private:

            //! Return the list of interfaces of \a nature.
            const Data::Interface::vector_unique_ptr& GetInterfaceList(InterfaceNS::Nature interface_nature) const;


        private:

            /*!
             * \brief Content of the interface file.
             *
             * Key is the nature of the interface.
             * Value is for each node the list of Coords that delimit it.
             */
            std::map<InterfaceNS::Nature, Data::Interface::vector_unique_ptr> content_;

        };


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


# include "PostProcessing/File/InterfaceFile.hxx"


#endif // MOREFEM_x_POST_PROCESSING_x_FILE_x_INTERFACE_FILE_HPP_
