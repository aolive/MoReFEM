///
////// \file
///
///
/// Created by Federica Caforio <federica.caforio@inria.fr> on the Thu, 12 May 2016 16:34:28 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup PostProcessingGroup
/// \addtogroup PostProcessingGroup
/// \{

#ifndef MOREFEM_x_POST_PROCESSING_x_REFINE_MESH_QUADRANGLES_SPECTRAL_x_MODEL_HXX_
# define MOREFEM_x_POST_PROCESSING_x_REFINE_MESH_QUADRANGLES_SPECTRAL_x_MODEL_HXX_


namespace MoReFEM
{


    namespace RefineMeshNS
    {


        template<class MoReFEMDataT>
        Model<MoReFEMDataT>::Model(const MoReFEMDataT& morefem_data)
        : parent(morefem_data)
        { }


        template<class MoReFEMDataT>
        void Model<MoReFEMDataT>::SupplInitialize()
        {
            decltype(auto) god_of_dof = GodOfDofManager::GetInstance(__FILE__, __LINE__).GetGodOfDof(1);
            decltype(auto) felt_space = god_of_dof.GetFEltSpace(1);
            decltype(auto) unknown = UnknownManager::GetInstance(__FILE__, __LINE__).GetUnknown(1);
            decltype(auto) extended_unknown_ptr = felt_space.GetExtendedUnknownPtr(unknown);

            felt_space.ComputeLocal2Global(std::move(extended_unknown_ptr),
                                           DoComputeProcessorWiseLocal2Global::yes);

            RefineMeshNS::RefineMeshSpectral(felt_space,
                                             god_of_dof.GetMesh(),
                                             parent::GetOutputDirectory());
        }


        template<class MoReFEMDataT>
        inline const std::string& Model<MoReFEMDataT>::ClassName()
        {
            static std::string name("RefineMesh");
            return name;
        }


        template<class MoReFEMDataT>
        inline bool Model<MoReFEMDataT>::SupplHasFinishedConditions() const
        {
            return true; // No time iteration!
        }


        template<class MoReFEMDataT>
        inline void Model<MoReFEMDataT>::SupplInitializeStep()
        { }


        template<class MoReFEMDataT>
        inline void Model<MoReFEMDataT>::Forward()
        { }


        template<class MoReFEMDataT>
        inline void Model<MoReFEMDataT>::SupplFinalizeStep()
        { }


        template<class MoReFEMDataT>
        inline void Model<MoReFEMDataT>::SupplFinalize()
        { }


    } // namespace RefineMeshNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


#endif // MOREFEM_x_POST_PROCESSING_x_REFINE_MESH_QUADRANGLES_SPECTRAL_x_MODEL_HXX_
