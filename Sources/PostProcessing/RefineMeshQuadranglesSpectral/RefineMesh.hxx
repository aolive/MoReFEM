///
////// \file
///
///
/// Created by Federica Caforio <federica.caforio@inria.fr> on the Mon, 6 Jun 2016 16:19:29 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup PostProcessingGroup
/// \addtogroup PostProcessingGroup
/// \{

#ifndef MOREFEM_x_POST_PROCESSING_x_REFINE_MESH_QUADRANGLES_SPECTRAL_x_REFINE_MESH_HXX_
# define MOREFEM_x_POST_PROCESSING_x_REFINE_MESH_QUADRANGLES_SPECTRAL_x_REFINE_MESH_HXX_


namespace MoReFEM
{





} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


#endif // MOREFEM_x_POST_PROCESSING_x_REFINE_MESH_QUADRANGLES_SPECTRAL_x_REFINE_MESH_HXX_
