/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 28 Jul 2015 11:51:43 +0200
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_TEST_x_PARAMETER_x_AT_DOF_x_INPUT_PARAMETER_LIST_HPP_
# define MOREFEM_x_TEST_x_PARAMETER_x_AT_DOF_x_INPUT_PARAMETER_LIST_HPP_

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/InputParameterData/InputParameterList.hpp"
# include "Core/InputParameter/Geometry/Domain.hpp"
# include "Core/InputParameter/FElt/FEltSpace.hpp"
# include "Core/InputParameter/FElt/Unknown.hpp"
# include "Core/InputParameter/FElt/NumberingSubset.hpp"
# include "Core/InputParameter/Parameter/Fiber/Fiber.hpp"


namespace MoReFEM
{


    namespace TestAtDofNS
    {


        //! \copydoc doxygen_hide_mesh_enum
        enum class MeshIndex
        {
            mesh = 1
        };


        //! \copydoc doxygen_hide_domain_enum
        enum class DomainIndex
        {
            domain = 1,
            full_mesh = 2
        };


        //! \copydoc doxygen_hide_felt_space_enum
        enum class FEltSpaceIndex
        {
            index = 1
        };


        //! \copydoc doxygen_hide_unknown_enum
        enum class UnknownIndex
        {
            scalar = 1,
            vectorial = 2
        };


        //! \copydoc doxygen_hide_solver_enum
        enum class SolverIndex

        {
            solver = 1
        };


        //! \copydoc doxygen_hide_numbering_subset_enum
        enum class NumberingSubsetIndex
        {
            scalar = 1,
            vectorial = 2
        };


        //! \copydoc doxygen_hide_fiber_enum
        enum class FiberIndex
        {
            fiber = 1,
            distance = 2
        };



        //! \copydoc doxygen_hide_input_parameter_tuple
        using InputParameterTuple = std::tuple
        <
            InputParameter::TimeManager,

            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::scalar)>,
            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::vectorial)>,

            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::scalar)>,
            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::vectorial)>,

            InputParameter::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            InputParameter::Domain<EnumUnderlyingType(DomainIndex::domain)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,

            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::index)>,

            InputParameter::Fiber<EnumUnderlyingType(FiberIndex::fiber), ParameterNS::Type::vector>,
            InputParameter::Fiber<EnumUnderlyingType(FiberIndex::distance), ParameterNS::Type::scalar>,

            InputParameter::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputParameter::Result
        >;


        //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = InputParameterList<InputParameterTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputParameterList>;


    } // namespace TestAtDofNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_PARAMETER_x_AT_DOF_x_INPUT_PARAMETER_LIST_HPP_
