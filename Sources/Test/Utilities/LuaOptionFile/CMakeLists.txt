add_executable(MoReFEMTestLuaOptionFile
                ${CMAKE_CURRENT_LIST_DIR}/main.cpp
               )
          
target_link_libraries(MoReFEMTestLuaOptionFile
                      ${MOREFEM_UTILITIES})
                      
add_test(LuaOptionFile 
         MoReFEMTestLuaOptionFile
         --root_dir ${MOREFEM_ROOT}
         --test_output_dir /tmp)
                      
morefem_install(MoReFEMTestLuaOptionFile)                      
