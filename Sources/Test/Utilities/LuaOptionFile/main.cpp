/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include "Utilities/LuaOptionFile/LuaOptionFile.hpp"
#include "Utilities/Environment/Environment.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Test/Tools/CatchMainTest.hpp"


using namespace MoReFEM;


TEST_CASE("Options are correctly read from the Lua file")
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    decltype(auto) input_file =
        environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Utilities/LuaOptionFile/demo_lua_option_file.lua");

    std::unique_ptr<LuaOptionFile> ptr;
    REQUIRE_NOTHROW(ptr.reset(new LuaOptionFile(input_file, __FILE__, __LINE__)));

    auto& lua_option_file = *(ptr.get());

    CHECK(NumericNS::AreEqual(lua_option_file.Read<double>("root_value", "", __FILE__, __LINE__), 2.44));

    CHECK(lua_option_file.Read<std::string>("section1.string_value", "", __FILE__, __LINE__) == "string");
    CHECK(NumericNS::AreEqual(lua_option_file.Read<double>("section1.double_value", "", __FILE__, __LINE__), 5.215));

    CHECK(lua_option_file.Read<unsigned int>("section1.int_value", "", __FILE__, __LINE__) == 10u);
    CHECK_THROWS(lua_option_file.Read<unsigned int>("section1.double_value", "", __FILE__, __LINE__) == 5u);

    CHECK(lua_option_file.Read<std::vector<std::string>>("section1.vector_value", "", __FILE__, __LINE__).size() == 3ul);
    CHECK(lua_option_file.Read<std::vector<std::string>>("section1.vector_value", "", __FILE__, __LINE__)[0] == "foo");
    CHECK(lua_option_file.Read<std::vector<std::string>>("section1.vector_value", "", __FILE__, __LINE__)[1] == "bar");
    CHECK(lua_option_file.Read<std::vector<std::string>>("section1.vector_value", "", __FILE__, __LINE__)[2] == "baz");
    CHECK_THROWS(lua_option_file.Read<int>("section1.vector_value", "", __FILE__, __LINE__));
    CHECK_THROWS(lua_option_file.Read<std::vector<int>>("section1.int_value", "", __FILE__, __LINE__));

    CHECK(lua_option_file.Read<std::string>("section1.string_value", "", __FILE__, __LINE__) == "string");

    CHECK(lua_option_file.Read<std::map<int, int>>("section1.map_value", "", __FILE__, __LINE__).size() == 3ul);
    CHECK(lua_option_file.Read<std::map<int, int>>("section1.map_value", "", __FILE__, __LINE__)[3] == 5);
    CHECK(lua_option_file.Read<std::map<int, int>>("section1.map_value", "", __FILE__, __LINE__)[4] == 7);
    CHECK(lua_option_file.Read<std::map<int, int>>("section1.map_value", "", __FILE__, __LINE__)[5] == 8);

    CHECK_THROWS(lua_option_file.Read<std::string>("unknown_key", "", __FILE__, __LINE__));
    CHECK_THROWS(lua_option_file.Read<double>("section1.string_value", "", __FILE__, __LINE__));

    CHECK_THROWS(lua_option_file.Read<double>("section1.invalid_value", "", __FILE__, __LINE__));

    // This test should be added when Catch2 implements it; there's pressure on devs to provide it.
    // CHECK_ABORT(lua_option_file.Read<double>("", "", __FILE__, __LINE__));
}


TEST_CASE("Constraints are correctly checked within the Lua file")
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    decltype(auto) input_file =
        environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Utilities/LuaOptionFile/demo_lua_option_file.lua");

    std::unique_ptr<LuaOptionFile> ptr;
    REQUIRE_NOTHROW(ptr.reset(new LuaOptionFile(input_file, __FILE__, __LINE__)));
    auto& lua_option_file = *(ptr.get());

    CHECK_THROWS(NumericNS::AreEqual(lua_option_file.Read<double>("root_value", "v > 5.", __FILE__, __LINE__), 2.44));
    CHECK_NOTHROW(NumericNS::AreEqual(lua_option_file.Read<double>("root_value", "v < 3.", __FILE__, __LINE__), 2.44));

    CHECK_NOTHROW(lua_option_file.Read<std::vector<std::string>>("section1.vector_value",
                                                        "value_in(v, { 'foo', 'bar', 'baz' })", __FILE__, __LINE__));

    CHECK_THROWS(lua_option_file.Read<std::vector<std::string>>("section1.vector_value",
                                                        "value_in(v, { 'bar', 'baz' })", __FILE__, __LINE__));

}


// This test should be added when Catch2 implements it; there's pressure on devs to provide it.
//TEST_CASE("Forgotten call to Open()")
//{
//    decltype(auto) input_file =
//    environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Utilities/LuaOptionFile/demo_lua_option_file.lua");
//
//    LuaOptionFile lua_option_file;
//    CHECK_THROWS(NumericNS::AreEqual(lua_option_file.Read<double>("root_value", "v > 5.", __FILE__, __LINE__), 2.44));
//
//}


TEST_CASE("Lua functions are correctly interpreted")
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    decltype(auto) input_file =
        environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Utilities/LuaOptionFile/demo_lua_option_file.lua");

    std::unique_ptr<LuaOptionFile> ptr;
    REQUIRE_NOTHROW(ptr.reset(new LuaOptionFile(input_file, __FILE__, __LINE__)));
    auto& lua_option_file = *(ptr.get());

    CHECK(NumericNS::AreEqual(lua_option_file.Apply<double>("section2.one_arg_function", __FILE__, __LINE__, 3.), -3.));
    CHECK(NumericNS::AreEqual(lua_option_file.Apply<double>("section2.several_arg_function", __FILE__, __LINE__, 3., 4., 5.), 2.));

    CHECK(lua_option_file.Apply<unsigned int>("section2.one_arg_function", __FILE__, __LINE__, 3) == -3);
}



TEST_CASE("Redundant parameters in the option file are correctly trapped")
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    decltype(auto) input_file =
        environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Utilities/LuaOptionFile/redundancy.lua");

    std::unique_ptr<LuaOptionFile> ptr = nullptr;
    REQUIRE_THROWS(ptr.reset(new LuaOptionFile(input_file, __FILE__, __LINE__)));

}

