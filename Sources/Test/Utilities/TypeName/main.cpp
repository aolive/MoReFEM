//! \file 
//
//
//  main.cpp
//  MoReFEM
//
//  Created by sebastien on 04/03/2018.
//Copyright © 2018 Inria. All rights reserved.
//

#define CATCH_CONFIG_MAIN
#include "ThirdParty/Source/Catch/catch.hpp"

#include "Utilities/Type/PrintTypeName.hpp"


using namespace MoReFEM;


TEST_CASE("Types are correctly extracted" )
{
    REQUIRE(GetTypeName<int>() == "int");
    REQUIRE(GetTypeName<double>() == "double");

    #ifdef __clang__
    constexpr auto expected = "const int &";
    # elif defined(__GNUC__)
    constexpr auto expected = "const int&";
    #endif

    REQUIRE(GetTypeName<const int&>() == expected);

    // The tests below are not complete: it's highly possible a compiler will yield a correct result. Feel free to
    // extend the test; currently it covers output by AppleClang, Clang and gcc.
    REQUIRE(( // double parenthesis on purpose!
             GetTypeName<std::vector<double>>() == "std::__1::vector<double, std::__1::allocator<double> >"
             || GetTypeName<std::vector<double>>() == "std::vector<double, std::allocator<double> >"
             || GetTypeName<std::vector<double>>() == "std::vector<double>"
             ));

    
    REQUIRE(( // double parenthesis on purpose!
             GetTypeName<std::string>() == "std::__1::basic_string<char>"
             || GetTypeName<std::string>()
             == "std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >"
             || GetTypeName<std::string>() == "std::__cxx11::basic_string<char>"
            ));
}


