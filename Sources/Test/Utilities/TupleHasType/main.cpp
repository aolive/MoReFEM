//! \file 
//
//
//  main.cpp
//  MoReFEM
//
//  Created by sebastien on 26/03/2018.
//Copyright © 2018 Inria. All rights reserved.
//

#define CATCH_CONFIG_MAIN
#include "ThirdParty/Source/Catch/catch.hpp"

#include "Utilities/Containers/Tuple.hpp"


using namespace MoReFEM;


TEST_CASE("Simple case: std::is_same" )
{
    using tuple = std::tuple<int, float, double, float>;
    REQUIRE(Utilities::Tuple::HasType<int, std::is_same, tuple>());
    REQUIRE(!Utilities::Tuple::HasType<char, std::is_same, tuple>());
}


TEST_CASE("A bit more complex: also test Decay." )
{
    using tuple = std::tuple<int&&, float[3], double&>;
    using decayed_tuple = Utilities::Tuple::Decay<tuple>::type;

    REQUIRE(Utilities::Tuple::HasType<int, std::is_same, decayed_tuple>());
    REQUIRE(Utilities::Tuple::HasType<float*, std::is_same, decayed_tuple>());
    REQUIRE(Utilities::Tuple::HasType<double, std::is_same, decayed_tuple>());
}


TEST_CASE("Check HasType with is_base_of is working." )
{
    struct Base
    { };

    struct Derived : public Base
    { };

    using tuple = std::tuple<int, Derived, double>;

    REQUIRE(!Utilities::Tuple::HasType<Base, std::is_same, tuple>());
    REQUIRE(Utilities::Tuple::HasType<Base, std::is_base_of, tuple>());
}
