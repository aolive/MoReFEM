/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/String/String.hpp"

#include "ThirdParty/IncludeWithoutWarning/Seldon/Seldon.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Utilities/InputParameterList/InputParameterList.hpp"


using namespace MoReFEM;


int main(int argc, char** argv)
{
    
    //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = TestNS::InputParameterListNS::InputParameterList;
    
    try
    {
        MoReFEMData<InputParameterList> morefem_data(argc, argv);
                
        const auto& input_parameter_data = morefem_data.GetInputParameterList();
        const auto& mpi = morefem_data.GetMpi();
        
        try
        {
            namespace ipl = Utilities::InputParameterListNS;
          
            {
                using NS = InputParameter::NumberingSubset<1>;
                decltype(auto) value = ipl::Extract<NS::Name>::Value(input_parameter_data);
                
                if (value != "Numbering subset")
                    throw Exception("Field related to numbering subset not read correctly",
                                    __FILE__, __LINE__);
            }
            
            {
                using interp_type = InputParameter::InterpolationFile;
            
                decltype(auto) value = ipl::Extract<interp_type>::Value(input_parameter_data);
                
                if (value != "${MOREFEM_ROOT}/Data/Interpolation/Poromechanics/Identity_6x6.hhdata")
                    throw Exception("Field related to interpolation file not read correctly.",
                                    __FILE__, __LINE__);
                
                
                decltype(auto) path = ipl::Extract<interp_type>::Path(input_parameter_data);

                if (!Utilities::String::EndsWith(path, "Data/Interpolation/Poromechanics/Identity_6x6.hhdata")
                    || path.find("${MOREFEM_ROOT}") != std::string::npos)
                    throw Exception("Field related to interpolation file not read correctly regarding its environment variable.",
                                    __FILE__, __LINE__);
                
                
                
            }
            
                        
        }
        catch(const std::exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());            
        }
        catch(Seldon::Error& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.What());
        }        
    }
    catch(const std::exception& e)
    {
        std::ostringstream oconv;
        oconv << "Exception caught from MoReFEMData<InputParameterList>: " << e.what() << std::endl;
        
        std::cout << oconv.str();
        return EXIT_FAILURE;
    }
    
    
    return EXIT_SUCCESS;
}

