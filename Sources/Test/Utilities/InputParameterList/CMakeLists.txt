add_executable(MoReFEMTestInputParameterList 
              ${CMAKE_CURRENT_LIST_DIR}/main.cpp
              ${CMAKE_CURRENT_LIST_DIR}/InputParameterList.hpp
              )
          
target_link_libraries(MoReFEMTestInputParameterList
    ${ALL_LOAD_BEGIN_FLAG}                    
    ${MOREFEM_MODEL}
    ${ALL_LOAD_END_FLAG})
    

#add_test(TestInputParameterList MoReFEMTestInputParameterList)
                      
morefem_install(MoReFEMTestInputParameterList)                      