/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 28 Jul 2015 11:51:43 +0200
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_TEST_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INPUT_PARAMETER_LIST_HPP_
# define MOREFEM_x_TEST_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INPUT_PARAMETER_LIST_HPP_

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/InputParameterData/InputParameterList.hpp"
# include "Core/InputParameter/TimeManager/TimeManager.hpp"
# include "Core/InputParameter/Geometry/Domain.hpp"
# include "Core/InputParameter/Geometry/Mesh.hpp"
# include "Core/InputParameter/FElt/FEltSpace.hpp"
# include "Core/InputParameter/FElt/Unknown.hpp"
# include "Core/InputParameter/FElt/NumberingSubset.hpp"
# include "Core/InputParameter/Solver/Petsc.hpp"
# include "Core/InputParameter/Geometry/InterpolationFile.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        namespace InputParameterListNS
        {


            //! \copydoc doxygen_hide_mesh_enum
            enum class MeshIndex
            {
                mesh = 1
            };


            //! \copydoc doxygen_hide_domain_enum
            enum class DomainIndex
            {
                domain = 1
            };


            //! \copydoc doxygen_hide_felt_space_enum
            enum class FEltSpaceIndex
            {
                felt_space = 1
            };


            //! \copydoc doxygen_hide_unknown_enum
            enum class UnknownIndex
            {
                unknown = 1
            };


            //! \copydoc doxygen_hide_solver_enum
            enum class SolverIndex

            {
                solver = 1
            };


            //! \copydoc doxygen_hide_numbering_subset_enum
            enum class NumberingSubsetIndex : unsigned int
            {
                numbering_subset = 1
            };



            //! \copydoc doxygen_hide_input_parameter_tuple
            using InputParameterTuple = std::tuple
            <
                InputParameter::TimeManager,

                InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::numbering_subset)>,

                InputParameter::InterpolationFile,

                InputParameter::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

                InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::unknown)>,

                InputParameter::Domain<EnumUnderlyingType(DomainIndex::domain)>,

                InputParameter::Result,

                InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::felt_space)>,

                InputParameter::Petsc<EnumUnderlyingType(SolverIndex::solver)>

            >;


            //! \copydoc doxygen_hide_model_specific_input_parameter_list
            using InputParameterList = InputParameterList<InputParameterTuple>;

            //! \copydoc doxygen_hide_morefem_data_type
            using morefem_data_type = MoReFEMData<InputParameterList>;


        } // namespace TestNS


    } // namespace InputParameterListNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INPUT_PARAMETER_LIST_HPP_
