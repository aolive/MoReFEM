#define CATCH_CONFIG_MAIN
#include "ThirdParty/Source/Catch/catch.hpp"

# include "Utilities/Environment/Environment.hpp"


using namespace MoReFEM;


TEST_CASE("Shell environment variables are properly read")
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    std::string shell_user(std::getenv("PATH")); // I assume here PATH is rather universal and should be defined in
                                                 // all Unix environments.

    SECTION("When the key is given as a std::string")
    {
        CHECK(environment.DoExist(std::string("PATH")));
        CHECK(environment.GetEnvironmentVariable(std::string("PATH"), __FILE__, __LINE__) == shell_user);
    }

    SECTION("When the key is given as a char*")
    {
        CHECK(environment.DoExist("PATH"));
        CHECK(environment.GetEnvironmentVariable("PATH", __FILE__, __LINE__) == shell_user);
    }


    SECTION("Throws when stupid environment variable name (hope you don't get one named like that...")
    {
        CHECK(environment.DoExist("FHLKJHFFLKJ") == false);
        CHECK_THROWS(environment.GetEnvironmentVariable("FHLKJHFFLKJ", __FILE__, __LINE__));
    }

}


TEST_CASE("SubstituteValues().")
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    std::string shell_user(std::getenv("USER")); // I assume here USER is rather universal and should be defined in
    // all Unix environments.

    std::string line = "/Volumes/Blah/${USER}";

    CHECK(environment.SubstituteValues(line) == "/Volumes/Blah/" + shell_user);
}


TEST_CASE("Internal storage is working as expected.")
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    CHECK(environment.DoExist("ASDFGHJK") == false);

    CHECK_NOTHROW(environment.SetEnvironmentVariable(std::make_pair("ASDFGHJK", "42 "), __FILE__, __LINE__));

    CHECK(environment.DoExist("ASDFGHJK") == true);

    CHECK(environment.GetEnvironmentVariable("ASDFGHJK", __FILE__, __LINE__) == "42 ");

    CHECK_THROWS(environment.SetEnvironmentVariable(std::make_pair("ASDFGHJK", "47 "), __FILE__, __LINE__));
}


TEST_CASE("Insertion of an already existing environment variable in the shell fails.")
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    CHECK_THROWS(environment.SetEnvironmentVariable(std::make_pair("USER", "42"), __FILE__, __LINE__));
}




