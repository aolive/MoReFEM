/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 22 Apr 2014 14:04:24 +0200
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_TEST_x_ONDOMATIC_NUMBERING_x_INPUT_PARAMETER_LIST_HPP_
# define MOREFEM_x_TEST_x_ONDOMATIC_NUMBERING_x_INPUT_PARAMETER_LIST_HPP_

# include "Core/InputParameterData/InputParameterList.hpp"
# include "Core/InputParameter/Geometry/Domain.hpp"
# include "Core/InputParameter/FElt/FEltSpace.hpp"
# include "Core/InputParameter/FElt/Unknown.hpp"


namespace MoReFEM
{


    namespace OndomaticNumberingNS
    {


        //! \copydoc doxygen_hide_input_parameter_tuple
        using InputParameterTuple = std::tuple
        <
            InputParameter::Unknown::Name<1>,
            InputParameter::Unknown::Nature<1>,

            InputParameter::Mesh::Mesh<1>,
            InputParameter::Mesh::Format<1>,
            InputParameter::Mesh::Dimension<1>,

            InputParameter::Domain::MeshIndex<1>,
            InputParameter::Domain::DimensionList<1>,
            InputParameter::Domain::MeshLabelList<1>,
            InputParameter::Domain::GeomEltTypeList<1>,

            InputParameter::FEltSpace::GodOfDofIndex<1>,
            InputParameter::FEltSpace::DomainIndex<1>,
            InputParameter::FEltSpace::UnknownList<1>,
            InputParameter::FEltSpace::ShapeFunctionList<1>,
            InputParameter::FEltSpace::NumberingSubsetList<1>,

            InputParameter::BoundaryCondition,

            InputParameter::Result::OutputDirectory
        >;


        //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = InputParameterList<InputParameterTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputParameterList>;



    }; // namespace OndomaticNumberingNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_ONDOMATIC_NUMBERING_x_INPUT_PARAMETER_LIST_HPP_
