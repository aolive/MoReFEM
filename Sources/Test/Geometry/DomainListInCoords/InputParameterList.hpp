/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 14 Apr 2017 16:21:23 +0200
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_DOMAIN_LIST_IN_COORDS_x_INPUT_PARAMETER_LIST_HPP_
# define MOREFEM_x_TEST_x_GEOMETRY_x_DOMAIN_LIST_IN_COORDS_x_INPUT_PARAMETER_LIST_HPP_

# include "Utilities/Containers/EnumClass.hpp"
# include "Core/InputParameterData/InputParameterList.hpp"

# include "Core/InputParameter/TimeManager/TimeManager.hpp"
# include "Core/InputParameter/Geometry/Domain.hpp"
# include "Core/InputParameter/Geometry/Mesh.hpp"
# include "Core/InputParameter/Solver/Petsc.hpp"
# include "Core/InputParameter/FElt/Unknown.hpp"
# include "Core/InputParameter/FElt/NumberingSubset.hpp"
# include "Core/InputParameter/FElt/FEltSpace.hpp"
#  include "Core/InputParameter/Geometry/LightweightDomainList.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        namespace DomainListInCoordsNS
        {


            //! \copydoc doxygen_hide_mesh_enum
            enum class MeshIndex
            {
                mesh = 1
            };


            //! \copydoc doxygen_hide_domain_enum
            enum class DomainIndex
            {
                volume = 1,
                exterior_surface = 2,
                interior_surface = 3,
                ring = 4
            };


            //! Default value for some input parameter that are required by a MoReFEM model but are actually unused for current test.
            constexpr auto whatever = 0;


            //! \copydoc doxygen_hide_input_parameter_tuple
            using InputParameterTuple = std::tuple
            <
                InputParameter::TimeManager,

                InputParameter::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

                InputParameter::Domain<EnumUnderlyingType(DomainIndex::volume)>,
                InputParameter::Domain<EnumUnderlyingType(DomainIndex::exterior_surface)>,
                InputParameter::Domain<EnumUnderlyingType(DomainIndex::interior_surface)>,
                InputParameter::Domain<EnumUnderlyingType(DomainIndex::ring)>,

                InputParameter::LightweightDomainList<whatever>,

                InputParameter::Unknown<whatever>,
                InputParameter::NumberingSubset<whatever>,
                InputParameter::FEltSpace<whatever>,
                InputParameter::Petsc<whatever>,

                InputParameter::Result
            >;


            //! \copydoc doxygen_hide_model_specific_input_parameter_list
            using InputParameterList = InputParameterList<InputParameterTuple>;

            //! \copydoc doxygen_hide_morefem_data_type
            using morefem_data_type = MoReFEMData<InputParameterList>;


        } // namespace DomainListInCoordsNS


    } // namespace TestNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_DOMAIN_LIST_IN_COORDS_x_INPUT_PARAMETER_LIST_HPP_
