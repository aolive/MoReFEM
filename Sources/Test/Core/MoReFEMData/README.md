This check was introduced to test the new feature of environment variable in MoReFEMData; other aspects of this class that could be
tested as well aren't yet handled.

However, it must be noticed this is a very tricky class to handle: as Mpi initialization is performed inside, we can't run different test cases, as
MPI_Init() and MPI_Finalize() can be called only once per program. So when more tests are added it might be useful to introduce others
main files for these tests.

