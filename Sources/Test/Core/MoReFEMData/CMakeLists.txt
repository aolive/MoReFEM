add_executable(MoReFEMTestMoReFEMDataCommandLineOptions
                ${CMAKE_CURRENT_LIST_DIR}/test_command_line_options.cpp
               )
          
target_link_libraries(MoReFEMTestMoReFEMDataCommandLineOptions
                      ${MOREFEM_CORE})
                      
add_test(MoReFEMDataCommandLineOptions
         MoReFEMTestMoReFEMDataCommandLineOptions
         --root_dir ${MOREFEM_ROOT}
         --test_output_dir ${MOREFEM_TEST_OUTPUT_DIR})
                      
morefem_install(MoReFEMTestMoReFEMDataCommandLineOptions)
