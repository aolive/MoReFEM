-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.

-- transient
transient = {
	-- Tells which policy is used to describe time evolution.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'constant_time_step', 'variable_time_step'}})
	time_evolution_policy = "variable_time_step",

	-- Time at the beginning of the code (in seconds).
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	init_time = 0.057,

	-- Time step between two iterations, in seconds.
	-- Expected format: VALUE
	-- Constraint: v > 0.
	timeStep = 0.1,
    
    -- Minimum time step between two iterations, in seconds.
    -- Expected format: VALUE
    -- Constraint: v > 0.
    minimum_time_step = 0.007,

	-- Maximum time, if set to zero run a static case.
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	timeMax = 5.
}



-- Result
Result = {
	-- Directory in which all the results will be written. 
	-- Expected format: "VALUE"
	output_directory = '${MOREFEM_TEST_OUTPUT_DIR}/TestVariableTimeStep',
    
    -- Enables to skip some printing in the console. Can be used to WriteSolution every n time.
    -- Expected format: VALUE
    display_value = 1
}
