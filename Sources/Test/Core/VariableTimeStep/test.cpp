/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 Sep 2017 15:59:08 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "ThirdParty/IncludeWithoutWarning/Seldon/Seldon.hpp"
#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/TimeManagerInstance.hpp"
#include "Core/TimeManager/Policy/VariableTimeStep.hpp"

#include "Test/Core/VariableTimeStep/InputParameterList.hpp"
#include "Test/Tools/CatchMainTest.hpp"
#include "Test/Tools/Fixture.hpp"

using namespace MoReFEM;


namespace // anonymous
{


    struct LuaFile
    {


        static std::string GetPath();


    };


    struct MockModel
    {

        using morefem_data_type = MoReFEMData<TestVariableTimeStepNS::InputParameterList>;

    };


    using fixture_type =
    TestNS::Fixture
    <
        MockModel,
        LuaFile
    >;


} // namespace anonymous


TEST_CASE_METHOD(fixture_type, "Check Send() and Receive() behave as expected with single values.")
{
    decltype(auto) morefem_data = fixture_type::GetMoReFEMData();

    decltype(auto) input_data = morefem_data.GetInputParameterList();
    decltype(auto) mpi = morefem_data.GetMpi();

    TimeManagerInstance<TimeManagerNS::Policy::VariableTimeStep> time_manager(input_data);

    CHECK(time_manager.GetTime() == Approx(0.057));
    time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::decreasing);
    CHECK(time_manager.GetTimeStep() == Approx(0.05));

    CHECK(time_manager.GetTime() == Approx(0.057));

    time_manager.IncrementTime();
    CHECK(time_manager.GetTime() == Approx(0.107));

    time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::decreasing);
    time_manager.IncrementTime();
    CHECK(time_manager.GetTimeStep() == Approx(0.025));
    CHECK(time_manager.GetTime() == Approx(0.132));

    // \todo #1274 Clarify expected behaviour and if correct adapt the time steps.
    // \todo #1274 Also add tests for others variable step method.
//    time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::increasing);
//    CHECK(time_manager.GetTimeStep() == Approx(0.05));
//    time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::increasing);
//    CHECK(time_manager.GetTimeStep() == Approx(0.1));
//    time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::increasing);
//    CHECK(time_manager.GetTimeStep() == Approx(0.1));
//
//    time_manager.IncrementTime();
//    CHECK(time_manager.GetTime() == Approx(0.232));

}


namespace // anonymous
{


    std::string LuaFile::GetPath()
    {
        decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

        return
        environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Core/VariableTimeStep/demo_input_data.lua");
    }


} // namespace anonymous

