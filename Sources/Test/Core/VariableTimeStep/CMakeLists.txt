# add_executable(MoReFEMTestVariableTimeStep
#               ${CMAKE_CURRENT_LIST_DIR}/main.cpp
#               ${CMAKE_CURRENT_LIST_DIR}/Model.cpp
#               ${CMAKE_CURRENT_LIST_DIR}/Model.hpp
#               ${CMAKE_CURRENT_LIST_DIR}/Model.hxx
#               ${CMAKE_CURRENT_LIST_DIR}/InputParameterList.hpp
#               )
#
# target_link_libraries(MoReFEMTestVariableTimeStep
#     ${ALL_LOAD_BEGIN_FLAG}
#     ${MOREFEM_MODEL}
# ${ALL_LOAD_END_FLAG})


add_executable(MoReFEMTestVariableTimeStep
               ${CMAKE_CURRENT_LIST_DIR}/InputParameterList.hpp
               ${CMAKE_CURRENT_LIST_DIR}/test.cpp
              )
          
target_link_libraries(MoReFEMTestVariableTimeStep
                      ${ALL_LOAD_BEGIN_FLAG}                    
                      ${MOREFEM_CORE}
                      ${ALL_LOAD_END_FLAG})
    
    
add_test(VariableTimeStep          
         MoReFEMTestVariableTimeStep
         --root_dir ${MOREFEM_ROOT}
         --test_output_dir ${MOREFEM_TEST_OUTPUT_DIR})
                      
morefem_install(MoReFEMTestVariableTimeStep)                        
                      
                     
