/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 28 Jul 2015 11:51:43 +0200
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_TEST_x_CORE_x_VARIABLE_TIME_STEP_x_INPUT_PARAMETER_LIST_HPP_
# define MOREFEM_x_TEST_x_CORE_x_VARIABLE_TIME_STEP_x_INPUT_PARAMETER_LIST_HPP_

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/InputParameterData/InputParameterList.hpp"
# include "Core/InputParameter/Geometry/Domain.hpp"
# include "Core/InputParameter/FElt/FEltSpace.hpp"
# include "Core/InputParameter/FElt/Unknown.hpp"
# include "Core/InputParameter/FElt/NumberingSubset.hpp"


namespace MoReFEM
{


    namespace TestVariableTimeStepNS
    {



        //! \copydoc doxygen_hide_input_parameter_tuple
        using InputParameterTuple = std::tuple
        <
            InputParameter::TimeManager,
            InputParameter::Result
        >;


        //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = InputParameterList<InputParameterTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputParameterList>;


    } // namespace TestVariableTimeStepNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_CORE_x_VARIABLE_TIME_STEP_x_INPUT_PARAMETER_LIST_HPP_
