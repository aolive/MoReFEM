//! \file 
//
//
//  CatchImpl.cpp
//  MoReFEM
//
//  Created by sebastien on 09/05/2018.
//Copyright © 2018 Inria. All rights reserved.
//

// Required for at least CMake shared lib build on macOS.
// This was determined empirically, as I want to encapsulate in a library some extensions of Catch, and therefore
// couldn't define CATCH_CONFIG_MAIN or CATCH_CONFIG_RUNNER.
#define CATCH_IMPL
#define CATCH_CONFIG_ALL_PARTS
#include "ThirdParty/Source/Catch/catch.hpp"

