//! \file
//
//
//  Fixture.hpp
//  MoReFEM
//
//  Created by sebastien on 16/03/2018.
//Copyright © 2018 Inria. All rights reserved.
//

#ifndef MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_HPP_
# define MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_HPP_

# include <memory>
# include <vector>

# include "Test/Tools/InitializeTestMoReFEMData.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        /*!
         * \brief An helper class to build tests with catch.
         *
         * The goal is to provide in a main different tests:
         *
         * \code
         namespace // anonymous
         {
            struct LuaFile
            {
                static std::string GetPath();
            };

            using fixture_type =
                TestNS::Fixture
                <
                    TestNS::Pk2::Model,
                    LuaFile
                >;
         } // namespace anonymous

         TEST_CASE_METHOD(fixture_type, "Unknown and test function unknown are identical")
         {
            GetModel().SameUnknown();
         }

         TEST_CASE_METHOD(fixture_type, "Unknown and test function unknown are different but share the same P1 shape function label")
         {
            GetModel().UnknownP1TestP1();
         }

         TEST_CASE_METHOD(fixture_type, "Unknown and test function unknown are different; unknown is P2 and test function P1")
         {
            GetModel().UnknownP2TestP1();
         }
         * \endcode
         *
         * The fixture here is a hack to run once and only once most of the initialization steps (MPI initialization,
         * building of the different singletons, etc...).
         *
         * Basically it nuilds and run the \a Model throughout all of the integration tests.
         *
         * \attention Contrary to the original intent of the fixture, it does NOT make tabula rasa of the state after
         * each test. So the tests should be a independant as possible from one another.
         *
         * \tparam ModelT Type of the model to build.
         * \tparam LuaFileT An ad hoc class which encapsulates in a \a GetPath() method the path to the Lua file.
         */
        template
        <
            class ModelT,
            class LuaFileT
        >
        struct Fixture
        {
        public:

            //! Alias to MoReFEMData.
            using morefem_data_type = typename ModelT::morefem_data_type;

            //! Alias to input parameter list within the model.
            using input_parameter_data_type = typename morefem_data_type::input_parameter_data_type;

            //! Alias to the proper InitializeTestMoReFEMData type.
            using initialize_type = TestNS::InitializeTestMoReFEMData<input_parameter_data_type>;

            /*!
             * \brief Static method which yields the model considered for the tests.
             *
             * \return Constant reference to the \a Model.
             */
            static const ModelT& GetModel();

            /*!
             * \brief Static method which yields the \a MoReFEMData considered for the tests.
             *
             * This static method is not really useful for a full-fledged model, which provides access to this anyway,
             * but is really handy for mock models used in some unit tests.
             *
             * \return Constant reference to the \a MoReFEMData.
             */
            static const morefem_data_type& GetMoReFEMData();

        public:

            /// \name Special members.
            ///@{

            //! Constructor.
            explicit Fixture();

            //! Destructor.
            ~Fixture() = default;

            //! Copy constructor.
            Fixture(const Fixture&) = delete;

            //! Move constructor.
            Fixture(Fixture&&) = delete;

            //! Copy affectation.
            Fixture& operator=(const Fixture&) = delete;

            //! Move affectation.
            Fixture& operator=(Fixture&&) = delete;

            ///@}

        };


    } // namespace TestNS


} // namespace MoReFEM


# include "Test/Tools/Fixture.hxx"


#endif // MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_HPP_
