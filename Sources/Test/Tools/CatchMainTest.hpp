//! \file
//
//
//  CatchMainTest.hpp
//  MoReFEM
//
//  Created by sebastien on 11/04/2018.
//Copyright © 2018 Inria. All rights reserved.
//

#ifndef MOREFEM_x_TEST_x_TOOLS_x_CATCH_MAIN_CHECK_MODEL_HPP_
# define MOREFEM_x_TEST_x_TOOLS_x_CATCH_MAIN_CHECK_MODEL_HPP_

# include "Utilities/Environment/Environment.hpp"

#define CATCH_CONFIG_RUNNER
#include "ThirdParty/Source/Catch/catch.hpp"


//! \cond IGNORE_BLOCK_IN_DOXYGEN

int main(int argc, char** argv)
{
    Catch::Session session;

    using namespace Catch::clara;

    std::string output_dir, root_dir;

    auto cli
    = session.cli() // Get Catch's composite command line parser
    | Opt( output_dir, "MOREFEM_TEST_OUTPUT_DIR" ) // bind variable to a new option, with a hint string
    ["--test_output_dir"]    // the option names it will respond to
    ("Specify here the MOREFEM_TEST_OUTPUT_DIR defined in the CMake environment")        // description string for the help output
    | Opt( root_dir, "MOREFEM_ROOT" ) // bind variable to a new option, with a hint string
    ["--root_dir"]    // the option names it will respond to
    ("Specify here the MOREFEM_ROOT defined in the CMake environment");        // description string for the help output

    // Now pass the new composite back to Catch so it uses that
    session.cli(cli);

    // Let Catch (using Clara) parse the command line
    int returnCode = session.applyCommandLine(argc, argv);

    if (returnCode != 0) // Indicates a command line error
        return returnCode;

    decltype(auto) environment = MoReFEM::Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    environment.SetEnvironmentVariable(std::make_pair("MOREFEM_TEST_OUTPUT_DIR", output_dir), __FILE__, __LINE__);
    environment.SetEnvironmentVariable(std::make_pair("MOREFEM_ROOT", root_dir), __FILE__, __LINE__);

    return session.run();
}

TEST_CASE("Environment variables are correctly read")
{
    decltype(auto) environment = MoReFEM::Utilities::Environment::GetInstance(__FILE__, __LINE__);

    REQUIRE(!environment.GetEnvironmentVariable("MOREFEM_ROOT", __FILE__, __LINE__).empty());
    REQUIRE(!environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__).empty());
}


//! \endcond IGNORE_BLOCK_IN_DOXYGEN

#endif // MOREFEM_x_TEST_x_TOOLS_x_CATCH_MAIN_CHECK_MODEL_HPP_
