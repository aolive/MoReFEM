//! \file
//
//
//  Fixture.hxx
//  MoReFEM
//
//  Created by sebastien on 16/03/2018.
//Copyright © 2018 Inria. All rights reserved.
//

#ifndef MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_HXX_
# define MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_HXX_


namespace MoReFEM
{


    namespace TestNS
    {


        template
        <
            class ModelT,
            class LuaFileT
        >
        Fixture<ModelT, LuaFileT>::Fixture()
        {
            static bool first_call = true;

            if (first_call)
            {
                const auto root_value = std::getenv("MOREFEM_RESULT_DIR");

                if (root_value != nullptr)
                {
                    std::ostringstream oconv;
                    oconv << "Please run the tests with MOREFEM_RESULT_DIR environment variables unset! "
                    "(currently it is set to " << root_value << ").";
                    throw Exception(oconv.str(), __FILE__, __LINE__);
                }
            }
        }


        template
        <
            class ModelT,
            class LuaFileT
        >
        const ModelT& Fixture<ModelT, LuaFileT>::GetModel()
        {
            static bool first_call = true;

            static ModelT model(GetMoReFEMData());

            if (first_call)
            {
                model.Run();
                first_call = false;
            }

            return model;
        }


        template
        <
            class ModelT,
            class LuaFileT
        >
        const typename Fixture<ModelT, LuaFileT>::morefem_data_type&
        Fixture<ModelT, LuaFileT>::GetMoReFEMData()
        {
            static initialize_type init(LuaFileT::GetPath());

            return init.GetMoReFEMData();
        }


    } // namespace TestNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_HXX_
