//! \file 
//
//
//  CompareEnsightFile.cpp
//  MoReFEM
//
//  Created by sebastien on 12/04/2018.
//Copyright © 2018 Inria. All rights reserved.
//

#include <cassert>
#include <sstream>

#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/Numeric/Numeric.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/Source/Catch/catch.hpp"

#include "Test/Tools/CompareEnsightFiles.hpp"


namespace MoReFEM
{
    
    
    namespace TestNS
    {


        namespace // anonymous
        {


            std::vector<double> ReadEnsightFile(const std::string& file);

            void CheckAreEquals(const std::vector<double>& ref,
                                const std::vector<double>& obtained,
                                const char* invoking_file, int invoking_line,
                                double epsilon);


        } // namespace anonymous


        void CompareEnsightFiles(const std::string& ref_dir,
                                 const std::string& obtained_dir,
                                 std::string&& filename,
                                 const char* invoking_file, int invoking_line,
                                 double epsilon)
        {
            SECTION("File " + filename  + " is almost identical to reference one." )
            {
                std::string ref_input_data = ref_dir + "/" + filename;
                REQUIRE(FilesystemNS::File::DoExist(ref_input_data));

                std::string obtained_input_data = obtained_dir + "/" + filename;
                REQUIRE(FilesystemNS::File::DoExist(obtained_input_data));

                const auto ref_values = ReadEnsightFile(ref_input_data);
                const auto obtained_values = ReadEnsightFile(obtained_input_data);

                REQUIRE(ref_values.size() == obtained_values.size());

                CHECK_NOTHROW(CheckAreEquals(ref_values, obtained_values, invoking_file, invoking_line, epsilon));
            }
        }


        namespace // anonymous
        {


            std::vector<double> ReadEnsightFile(const std::string& file)
            {
                std::ifstream in;
                FilesystemNS::File::Read(in, file, __FILE__, __LINE__);

                std::string line;
                getline(in, line); // skip first line

                std::string str_value;

                std::vector<double> ret;

                while (getline(in, line))
                {
                    const auto max = std::min(line.size(), 72ul);

                    for (auto index = 0ul; index < max; index += 12ul)
                    {
                        str_value.assign(line, index, 12ul);
                        ret.push_back(std::stod(str_value));
                    }
                }

                return ret;
            }


            void CheckAreEquals(const std::vector<double>& ref,
                                const std::vector<double>& obtained,
                                const char* invoking_file, int invoking_line,
                                double epsilon)
            {
                assert(ref.size() == obtained.size() && "REQUIRE conditions should make sure it's the case.");

                const auto end_obtained = obtained.cend();

                for (auto it_obtained = obtained.cbegin(), it_ref = ref.cbegin();
                     it_obtained != end_obtained;
                     ++it_obtained, ++it_ref)
                {
                    const auto obtained_value = *it_obtained;
                    const auto ref_value = *it_ref;

                    // Crude condition, that is ok for most of the cases!
                    if (!NumericNS::AreEqual(ref_value, obtained_value, epsilon))
                    {
                        std::ostringstream oconv;
                        oconv << it_ref - ref.cbegin() << "-th value is not identical to what was stored as a reference "
                        "(values are respectively " << ref_value << " and " << obtained_value << ").";
                        throw Exception(oconv.str(), invoking_file, invoking_line);
                    }
                }
            }
            

        } // namespace anonymous
     
        
    } // namespace TestNS


} // namespace MoReFEM
