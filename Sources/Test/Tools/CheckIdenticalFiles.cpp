//! \file 
//
//
//  CheckIdenticalFiles.cpp
//  MoReFEM
//
//  Created by sebastien on 11/04/2018.
//Copyright © 2018 Inria. All rights reserved.
//

#include "Utilities/Filesystem/File.hpp"
#include "ThirdParty/Source/Catch/catch.hpp"

#include "Test/Tools/CheckIdenticalFiles.hpp"


namespace MoReFEM
{
    
    
    namespace TestNS
    {


        void CheckIdenticalFiles(const std::string& ref_dir,
                                 const std::string& obtained_dir,
                                 std::string&& filename,
                                 const char* invoking_file, int invoking_line)
        {
            SECTION("File " + filename  + " is identical to reference one." )
            {
                std::string ref_input_data = ref_dir + "/" + filename;
                // dev #1264 std::cout << "REF = |" << ref_input_data << '|' << std::endl;
                REQUIRE(FilesystemNS::File::DoExist(ref_input_data));

                std::string obtained_input_data = obtained_dir + "/" + filename;
                // dev #1264 std::cout << "OBT = |" << obtained_input_data << '|' << std::endl;
                REQUIRE(FilesystemNS::File::DoExist(obtained_input_data));

                CHECK(FilesystemNS::File::AreEquals(ref_input_data, obtained_input_data, invoking_file, invoking_line));
            }

        }

        
    } // namespace TestNS


} // namespace MoReFEM
