//! \file
//
//
//  CompareEnsightFile.hpp
//  MoReFEM
//
//  Created by sebastien on 12/04/2018.
//Copyright © 2018 Inria. All rights reserved.
//

#ifndef MOREFEM_x_TEST_x_TOOLS_x_COMPARE_ENSIGHT_FILES_HPP_
# define MOREFEM_x_TEST_x_TOOLS_x_COMPARE_ENSIGHT_FILES_HPP_

# include <string>


namespace MoReFEM
{


    namespace TestNS
    {


        /*!
         * \brief Check a file obtained in a test about a model is mostly the same as a reference one, at expected
         * numerical precision.
         *
         * Numerical values in Ensight outputs are already truncated, so CheckIdenticalFiles() actually works pretty
         * well to check the values are conform to what is expected. However in some cases one digit may vary; present
         * function is expected to take care correctly of those cases.
         *
         * This function is assumed to be called within a Catch TEST_CASE; in case there is an issue (non existent
         * file or non consistant content) the Catch test fails.
         *
         * \param[in] ref_dir The reference directory. It is a subdirectory of ModelInstances/ *YourModel* /ExpectedResults,
         * e.g. ${MOREFEM_ROOT}/Sources/ModelInstances/Heat/ExpectedResults/1D/Mesh_1/Ensight6.
         * \param[in] obtained_dir The pendant of \a ref_dir for the outputs given by the model that is expected to have run
         * juste before the call to the result check. It is expected to be a subdirectory somwehere in
         * ${MOREFEM_TEST_OUTPUT_DIR}.
         * \param[in] filename Name of the file being compared. It is expected to be the same in \a ref_dir and in
         * \a obtained_dir and to be located directly there (not in a subdirectory).
         * \copydoc doxygen_hide_invoking_file_and_line
         * \param[in] epsilon Epsilon used for the comparison. A default value is provided; but the parameter
         * is there if you want to play with it.
         */
        void CompareEnsightFiles(const std::string& ref_dir,
                                 const std::string& obtained_dir,
                                 std::string&& filename,
                                 const char* invoking_file, int invoking_line,
                                 double epsilon = NumericNS::DefaultEpsilon<double>());


    } // namespace TestNS


} // namespace MoReFEM


# include "Test/Tools/CompareEnsightFiles.hxx"


#endif // MOREFEM_x_TEST_x_TOOLS_x_COMPARE_ENSIGHT_FILES_HPP_
