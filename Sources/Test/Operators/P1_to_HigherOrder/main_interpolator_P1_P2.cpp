/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Operators/P1_to_HigherOrder/Model.hpp"
#include "Test/Operators/P1_to_HigherOrder/InputParameterList.hpp"


using namespace MoReFEM;
using namespace MoReFEM::TestNS;


int main(int argc, char** argv)
{
    
    //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = P1_to_P_HigherOrder_NS::InputParameterList;
    
    try
    {
        MoReFEMData<InputParameterList> morefem_data(argc, argv);
                
        const auto& input_parameter_data = morefem_data.GetInputParameterList();
        const auto& mpi = morefem_data.GetMpi();
        
        try
        {
            P1_to_P_HigherOrder_NS::Model<P1_to_P_HigherOrder_NS::HigherOrder::P2> model(morefem_data);
            model.Run();
            
            input_parameter_data.PrintUnused(std::cout);
        }
        catch(const std::exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());
        }
        catch(Seldon::Error& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.What());
        }
    }
    catch(const std::exception& e)
    {
        std::ostringstream oconv;
        oconv << "Exception caught from MoReFEMData<InputParameterList>: " << e.what() << std::endl;
        
        std::cout << oconv.str();
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}

