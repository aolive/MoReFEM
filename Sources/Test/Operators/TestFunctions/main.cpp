/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#define CATCH_CONFIG_MAIN
#include "Test/Tools/InitializeTestMoReFEMData.hpp"

#include "Test/Operators/TestFunctions/Model.hpp"
#include "Test/Operators/TestFunctions/InputParameterList.hpp"





using namespace MoReFEM;


TEST_CASE("Run the model")
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    decltype(auto) input_file =
        environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Operators/TestFunctions/"
                                                   "demo_input_parameter_test_functions_3D.lua");

    using InputParameterList = TestFunctionsNS::InputParameterList;

    TestNS::InitializeTestMoReFEMData<InputParameterList> helper(std::move(input_file));

    decltype(auto) morefem_data = helper.GetMoReFEMData();

    TestFunctionsNS::Model model(morefem_data);

    model.Run();
}
