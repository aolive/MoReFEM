/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Operators/TestFunctions/Model.hpp"
#include "Test/Operators/TestFunctions/InputParameterList.hpp"


#define CATCH_CONFIG_MAIN
#include "ThirdParty/Source/Catch/catch.hpp"


using namespace MoReFEM;


TEST_CASE("Run the model")
{
    decltype(auto) input_file =
        Utilities::EnvironmentNS::SubstituteValues("${MOREFEM_ROOT}/CoreLibrary/Sources/Test/Operators/TestFunctions/demo_input_parameter_test_functions_3D.lua");

    CHECK(true);

    using InputParameterList = TestFunctionsNS::InputParameterList;

    using morefem_data_type = MoReFEMData<InputParameterList>;
    std::unique_ptr<morefem_data_type> ptr;

    char* str = const_cast<char*>(input_file.c_str());
    char* program_name= const_cast<char*>("TestFunctions");
    char* option = const_cast<char*>("-i");

    char* argv[3] = { program_name, option, str };
    REQUIRE_NOTHROW(ptr.reset(new morefem_data_type(3, argv)));
    decltype(auto) morefem_data = *ptr;

    const auto& input_parameter_data = morefem_data.GetInputParameterList();
    const auto& mpi = morefem_data.GetMpi();

    TestFunctionsNS::Model model(morefem_data);
    model.Run(morefem_data);
}
