/// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 2 Aug 2017 12:30:53 +0200
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_TEST_x_OPERATORS_x_TEST_FUNCTIONS_x_VARIATIONAL_FORMULATION_HXX_
# define MOREFEM_x_TEST_x_OPERATORS_x_TEST_FUNCTIONS_x_VARIATIONAL_FORMULATION_HXX_


namespace MoReFEM
{


    namespace TestFunctionsNS
    {


        inline Wrappers::Petsc::Snes::SNESFunction VariationalFormulation::ImplementSnesFunction() const
        {
            return nullptr;
        }


        inline Wrappers::Petsc::Snes::SNESJacobian VariationalFormulation::ImplementSnesJacobian() const
        {
            return nullptr;
        }


        inline Wrappers::Petsc::Snes::SNESViewer VariationalFormulation::ImplementSnesViewer() const
        {
            return nullptr;
        }


        inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction VariationalFormulation::ImplementSnesConvergenceTestFunction() const
        {
            return nullptr;
        }


        inline const ScalarParameter<>& VariationalFormulation::GetIntracelluarTransDiffusionTensor() const noexcept
        {
            assert(!(!intracellular_trans_diffusion_tensor_));
            return *intracellular_trans_diffusion_tensor_;
        }


        inline const ScalarParameter<>& VariationalFormulation::GetExtracelluarTransDiffusionTensor() const noexcept
        {
            assert(!(!extracellular_trans_diffusion_tensor_));
            return *extracellular_trans_diffusion_tensor_;
        }


        inline const ScalarParameter<>& VariationalFormulation::GetIntracelluarFiberDiffusionTensor() const noexcept
        {
            assert(!(!intracellular_fiber_diffusion_tensor_));
            return *intracellular_fiber_diffusion_tensor_;
        }


        inline const ScalarParameter<>& VariationalFormulation::GetExtracelluarFiberDiffusionTensor() const noexcept
        {
            assert(!(!extracellular_fiber_diffusion_tensor_));
            return *extracellular_fiber_diffusion_tensor_;
        }


        inline const GlobalMatrix& VariationalFormulation::GetMatrixBidomain() const noexcept
        {
            assert(!(!matrix_bidomain_));
            return *matrix_bidomain_;
        }


        inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixBidomain()
        {
            return const_cast<GlobalMatrix&>(GetMatrixBidomain());
        }


        inline const GlobalMatrix& VariationalFormulation::GetMatrixGradPhiTauTauGradPhi() const noexcept
        {
            assert(!(!matrix_grad_phi_tau_tau_grad_phi_));
            return *matrix_grad_phi_tau_tau_grad_phi_;
        }


        inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixGradPhiTauTauGradPhi()
        {
            return const_cast<GlobalMatrix&>(GetMatrixGradPhiTauTauGradPhi());
        }


        inline const GlobalMatrix& VariationalFormulation::GetMatrixBidomainNonSymmetric() const noexcept
        {
            assert(!(!matrix_bidomain_non_symmetric_));
            return *matrix_bidomain_non_symmetric_;
        }


        inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixBidomainNonSymmetric()
        {
            return const_cast<GlobalMatrix&>(GetMatrixBidomainNonSymmetric());
        }


        inline const GlobalMatrix& VariationalFormulation::GetMatrixGradPhiTauTauGradPhiNonSymmetric() const noexcept
        {
            assert(!(!matrix_grad_phi_tau_tau_grad_phi_non_symmetric_));
            return *matrix_grad_phi_tau_tau_grad_phi_non_symmetric_;
        }


        inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixGradPhiTauTauGradPhiNonSymmetric()
        {
            return const_cast<GlobalMatrix&>(GetMatrixGradPhiTauTauGradPhiNonSymmetric());
        }


    } // namespace TestFunctionsNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_TEST_FUNCTIONS_x_VARIATIONAL_FORMULATION_HXX_
