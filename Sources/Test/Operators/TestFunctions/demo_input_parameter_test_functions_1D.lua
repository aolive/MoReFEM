-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.

transient = {

	-- Tells which policy is used to describe time evolution.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'constant_time_step'})
	time_evolution_policy = "constant_time_step",

	-- Time at the beginning of the code (in seconds).
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	init_time = 0.,

	-- Time step between two iterations, in seconds.
	-- Expected format: VALUE
	-- Constraint: v > 0.
	timeStep = 0.1,
    
    -- Minimum time step between two iterations, in seconds.
    -- Expected format: VALUE
    -- Constraint: v > 0.
    minimum_time_step = 0.1,

	-- Maximum time, if set to zero run a static case.
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	timeMax = 0.

} -- transient

NumberingSubset1 = {
    
    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = "potential_1_potential_2",
    
    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false
    
    
} -- NumberingSubset1

NumberingSubset2 = {
    
    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = "potential_1",
    
    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false
    
    
} -- NumberingSubset2

NumberingSubset3 = {
    
    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = "potential_2",
    
    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false
    
    
} -- NumberingSubset3

NumberingSubset4 = {
    
    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = "potential_3",
    
    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false
    
    
} -- NumberingSubset4

NumberingSubset5 = {
    
    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = "displacement_potential_1",
    
    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false
    
    
} -- NumberingSubset5

NumberingSubset6 = {
    
    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = "displacement",
    
    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false
    
    
} -- NumberingSubset6

NumberingSubset7 = {
    
    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = "potential_1_potential_2_potential_4",
    
    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false
    
    
} -- NumberingSubset7

NumberingSubset8 = {
    
    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = "P1_potential_1_P2_potential_2",
    
    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false
    
} -- NumberingSubset8

NumberingSubset11 = {

    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = "potential_1_potential_2_test",

    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false


} -- NumberingSubset11

NumberingSubset12 = {

    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = "potential_1_test",

    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false


} -- NumberingSubset12

NumberingSubset13 = {

    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = "potential_2_test",

    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false


} -- NumberingSubset13

NumberingSubset14 = {

    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = "potential_3_test",

    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false


} -- NumberingSubset14

NumberingSubset15 = {

    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = "displacement_potential_1_test",

    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false


} -- NumberingSubset5

NumberingSubset16 = {

    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = "displacement_test",

    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false


} -- NumberingSubset16

NumberingSubset17 = {

    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = "potential_1_potential_2_potential_4_test",

    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false

} -- NumberingSubset17

NumberingSubset18 = {

    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = "P1_potential_1_P2_potential_2_test",

    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false

} -- NumberingSubset18

Unknown1 = {
    
    -- Name of the unknown (used for displays in output).
    -- Expected format: "VALUE"
    name = "potential_1",
    
    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'scalar', 'vectorial'})
    nature = "scalar",
    
} -- Unknown1

Unknown2 = {
    
    -- Name of the unknown (used for displays in output).
    -- Expected format: "VALUE"
    name = "potential_2",
    
    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'scalar', 'vectorial'})
    nature = "scalar",
    
} -- Unknown2

Unknown3 = {
    
    -- Name of the unknown (used for displays in output).
    -- Expected format: "VALUE"
    name = "potential_3",
    
    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'scalar', 'vectorial'})
    nature = "scalar",
    
} -- Unknown3

Unknown4 = {
    
    -- Name of the unknown (used for displays in output).
    -- Expected format: "VALUE"
    name = "potential_4",
    
    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'scalar', 'vectorial'})
    nature = "scalar",
    
} -- Unknown4

Unknown5 = {
    
    -- Name of the unknown (used for displays in output).
    -- Expected format: "VALUE"
    name = "displacement",
    
    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'scalar', 'vectorial'})
    nature = "vectorial",
    
} -- Unknown5


Unknown11 = {

    -- Name of the unknown (used for displays in output).
    -- Expected format: "VALUE"
    name = "potential_1_test",

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'scalar', 'vectorial'})
    nature = "scalar",

} -- Unknown1

Unknown12 = {

    -- Name of the unknown (used for displays in output).
    -- Expected format: "VALUE"
    name = "potential_2_test",

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'scalar', 'vectorial'})
    nature = "scalar",

} -- Unknown2

Unknown13 = {

    -- Name of the unknown (used for displays in output).
    -- Expected format: "VALUE"
    name = "potential_3_test",

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'scalar', 'vectorial'})
    nature = "scalar",

} -- Unknown3

Unknown14 = {

    -- Name of the unknown (used for displays in output).
    -- Expected format: "VALUE"
    name = "potential_4_test",

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'scalar', 'vectorial'})
    nature = "scalar",

} -- Unknown14

Unknown15 = {

    -- Name of the unknown (used for displays in output).
    -- Expected format: "VALUE"
    name = "displacement_test",

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'scalar', 'vectorial'})
    nature = "vectorial",

} -- Unknown15

Mesh1 = {
    
    -- Path of the mesh file to use.
    -- Expected format: "VALUE"
    --mesh = "${MOREFEM_ROOT}/Data/Mesh/bar_1D_L1_N3_Ensight.geo",
    mesh = "${MOREFEM_ROOT}/Data/Mesh/bar_1D_L1_N2_Ensight.geo",
    
    -- Format of the input mesh.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'Ensight', 'Medit'})
    format = "Ensight",
    
    -- Highest dimension of the input mesh. This dimension might be lower than the one effectively read in the
    -- mesh file; in which case Coords will be reduced provided all the dropped values are 0. If not, an
    -- exception is thrown.
    -- Expected format: VALUE
    -- Constraint: v <= 3 and v > 0
    dimension = 1,
    
    -- Space unit of the mesh.
    -- Expected format: VALUE
    space_unit = 1.
    
} -- Mesh1

Domain1 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: value_in(v, {0, 1, 2, 3})
    dimension_list = { 1 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at LuaOptionFile level, as some geometric element types could be added after
    -- generation of current input parameter file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }
    
} -- Domain1

Domain2 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: value_in(v, {0, 1, 2, 3})
    dimension_list = { 0 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at LuaOptionFile level, as some geometric element types could be added after
    -- generation of current input parameter file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }
    
} -- Domain2

Domain3 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: value_in(v, {0, 1, 2, 3})
    dimension_list = { },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at LuaOptionFile level, as some geometric element types could be added after
    -- generation of current input parameter file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }
    
} -- Domain3

FiniteElementSpace1 = {

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,

    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 1,

    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = { "potential_1", "potential_2", "potential_1_test", "potential_2_test" },

    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = { "P1", "P1", "P2", "P2" },

    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 1, 1, 11, 11 },

} -- FiniteElementSpace1

FiniteElementSpace2 = {

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,

    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 1,

    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = {"potential_1", "potential_1_test"},

    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = { "P1", "P2" },

    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 2, 12 },

} -- FiniteElementSpace2

FiniteElementSpace3 = {

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,

    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 1,

    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = {"potential_2", "potential_2_test"},

    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = {"P1", "P2"},

    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 3, 13 },

} -- FiniteElementSpace3

FiniteElementSpace4 = {

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,

    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 1,

    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = { "potential_1", "potential_3", "potential_1_test", "potential_3_test" },

    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = { "P1", "P2", "P2", "P1" }, -- See if relevant...

    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 2, 4, 12, 14 },

} -- FiniteElementSpace4

FiniteElementSpace5 = {

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,

    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 1,

    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = { "displacement" , "potential_1", "displacement_test" , "potential_1_test"  },

    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = { "P1", "P0", "P2", "P1" },

    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 5, 5, 15, 15 },

} -- FiniteElementSpace5

FiniteElementSpace6 = {

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,

    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 1,

    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = { "displacement", "displacement_test" },

    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = { "P1", "P2" },

    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 6, 16 },

} -- FiniteElementSpace6

FiniteElementSpace7 = {

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,

    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 1,

    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = { "potential_1", "potential_2", "potential_4", "potential_1_test", "potential_2_test", "potential_4_test" },

    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = { "P1", "P1", "P1", "P2", "P2", "P2" },

    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 7, 7, 7, 17, 17, 17 },

} -- FiniteElementSpace7

FiniteElementSpace8 = {

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,

    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 2,

    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = { "potential_1", "potential_1_test" },

    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = { "P1", "P2" },

    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 2, 12 },

} -- FiniteElementSpace8

FiniteElementSpace9 = {

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,

    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 2,

    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = { "displacement", "displacement_test" },

    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = { "P1", "P2" },

    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 6, 16 },

} -- FiniteElementSpace9

FiniteElementSpace10 = {

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,

    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 2,

    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = { "potential_1", "potential_2", "potential_1_test", "potential_2_test" },

    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = { "P1", "P2", "P2", "P1" },  -- see if pertinent

    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 8, 8, 18, 18 },

} -- FiniteElementSpace10

Petsc1 = {

	-- Absolute tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	absoluteTolerance = 1e-50,

	-- gmresStart
	-- Expected format: VALUE
	-- Constraint: v >= 0
	gmresRestart = 200,

	-- Maximum iteration
	-- Expected format: VALUE
	-- Constraint: v > 0
	maxIteration = 1000,

	-- Preconditioner to use. Must be lu for any direct solver.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'lu', 'none'})
	preconditioner = 'lu',

	-- Relative tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	relativeTolerance = 1e-9,

	-- Step size tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	stepSizeTolerance = 1e-8,

	-- Solver to use.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, { 'Mumps', 'Umfpack', 'Gmres' })
	solver = 'Mumps'

} -- Petsc1

Fiber_vector_1 = {
    
    -- Path to Ensight file.
    -- Expected format: "VALUE"
    --ensight_file = "${MOREFEM_ROOT}/Data/Fiber/fibers_bar_1D_L1_N3.vct",
    ensight_file = "${MOREFEM_ROOT}/Data/Fiber/fibers_bar_1D_L1_N2.vct",
    
    -- Index of the domain upon which parameter is defined.
    -- Expected format: VALUE
    domain_index = 1,
    
    -- Index of the finite element space upon which parameter is defined.
    -- Expected format: VALUE
    felt_space_index = 6,
    
    -- Name of the unknown used to describe the dofs. Might be a fictitious one (see documentation for more
    -- details).
    -- Expected format: "VALUE"
    unknown = 'displacement'
    
} -- Fiber1

Fiber_vector_2 = {
    
    -- Path to Ensight file.
    -- Expected format: "VALUE"
    ensight_file = "${MOREFEM_ROOT}/Data/Fiber/fibers_bar_1D_L1_N2.vct",
    
    -- Index of the domain upon which parameter is defined.
    -- Expected format: VALUE
    domain_index = 2,
    
    -- Index of the finite element space upon which parameter is defined.
    -- Expected format: VALUE
    felt_space_index = 9,
    
    -- Name of the unknown used to describe the dofs. Might be a fictitious one (see documentation for more
    -- details).
    -- Expected format: "VALUE"
    unknown = 'displacement'
    
} -- Fiber2

Fiber_scalar_3 = {
    
    -- Path to Ensight file.
    -- Expected format: "VALUE"
    --ensight_file = "${MOREFEM_ROOT}/Data/Fiber/angles_bar_1D_L1_N3.scl",
    ensight_file = "${MOREFEM_ROOT}/Data/Fiber/angles_bar_1D_L1_N2.scl",
    
    -- Index of the domain upon which parameter is defined.
    -- Expected format: VALUE
    domain_index = 1,
    
    -- Index of the finite element space upon which parameter is defined.
    -- Expected format: VALUE
    felt_space_index = 2,
    
    -- Name of the unknown used to describe the dofs. Might be a fictitious one (see documentation for more
    -- details).
    -- Expected format: "VALUE"
    unknown = 'potential_1'
    
} -- Fiber3

-- TransientSource
TransientSource1 = {
    -- How is given the transient source value (as a constant, as a Lua function, per quadrature point, etc...)
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function'})
    nature = "constant",
    
    -- Value of the transient source in the case nature is 'constant'(and also initial value if nature is 'at_quadrature_point'; irrelevant otherwise).
    -- Expected format: VALUE
    scalar_value = 1.,
    
    -- Value of the transient source for the component x in the case nature is 'lua_function'(and also initial value if nature is 'at_quadrature_point'; irrelevant otherwise).
    -- Expected format: Function in Lua language, for instance:
    -- function(arg1, arg2, arg3)
    -- return arg1 + arg2 - arg3
    -- end
    -- sin, cos and tan require a 'math.' preffix.
    lua_function = function (x, y, z)
        return 0.;
    end,
    
    -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'. The various
    -- domains given here must not intersect.
    -- Expected format: {VALUE1, VALUE2, ...}
    piecewise_constant_domain_id = { },
    
    -- Value of the parameter in the case nature is 'piecewise_constant_by_domain'.
    -- Expected format: {VALUE1, VALUE2, ...}
    piecewise_constant_domain_value = { }
    
}

Solid = {
    
    VolumicMass = {
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose
        -- "ignore" if you do not want this parameter (in this case it will stay at nullptr).
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','at_quadrature_point','piecewise_constant_by_domain'})
        nature = "constant",
        
        -- Value of the volumic mass in the case nature is 'constant'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: VALUE
        scalar_value = 1.,
        
        -- Value of the volumic mass in the case nature is 'lua_function'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: Function in Lua language, for instance:
        -- 		-- function(arg1, arg2, arg3)
        -- return arg1 + arg2 -
        -- arg3
        -- end
        -- sin, cos and tan require a 'math.'
        -- preffix.
        -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
        -- content is not interpreted by LuaOptionFile until an actual use of the underlying function.
        lua_function = none,
        
        -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'. The various
        -- domains given here must not intersect.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_id = { },
        
        -- Value of the parameter in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_value = { }
        
    }, -- VolumicMass
    
    HyperelasticBulk = {
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose
        -- "ignore" if you do not want this parameter (in this case it will stay at nullptr).
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','at_quadrature_point','piecewise_constant_by_domain'})
        nature = "constant",
        
        -- Value of the volumic mass in the case nature is 'constant'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: VALUE
        scalar_value = 1.,
        
        -- Value of the volumic mass in the case nature is 'lua_function'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: Function in Lua language, for instance:
        -- 		-- function(arg1, arg2, arg3)
        -- return arg1 + arg2 -
        -- arg3
        -- end
        -- sin, cos and tan require a 'math.'
        -- preffix.
        -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
        -- content is not interpreted by LuaOptionFile until an actual use of the underlying function.
        lua_function = none,
        
        -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'. The various
        -- domains given here must not intersect.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_id = { },
        
        -- Value of the parameter in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_value = { }
        
    }, -- HyperelasticBulk
    
    Kappa1 = {
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose
        -- "ignore" if you do not want this parameter (in this case it will stay at nullptr).
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','at_quadrature_point','piecewise_constant_by_domain'})
        nature = "constant",
        
        -- Value of the volumic mass in the case nature is 'constant'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: VALUE
        scalar_value = 1.,
        
        -- Value of the volumic mass in the case nature is 'lua_function'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: Function in Lua language, for instance:
        -- 		-- function(arg1, arg2, arg3)
        -- return arg1 + arg2 -
        -- arg3
        -- end
        -- sin, cos and tan require a 'math.'
        -- preffix.
        -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
        -- content is not interpreted by LuaOptionFile until an actual use of the underlying function.
        lua_function = none,
        
        -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'. The various
        -- domains given here must not intersect.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_id = { },
        
        -- Value of the parameter in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_value = { }
        
    }, -- Kappa1
    
    Kappa2 = {
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose
        -- "ignore" if you do not want this parameter (in this case it will stay at nullptr).
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','at_quadrature_point','piecewise_constant_by_domain'})
        nature = "constant",
        
        -- Value of the volumic mass in the case nature is 'constant'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: VALUE
        scalar_value = 1.,
        
        -- Value of the volumic mass in the case nature is 'lua_function'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: Function in Lua language, for instance:
        -- 		-- function(arg1, arg2, arg3)
        -- return arg1 + arg2 - 
        -- arg3
        -- end
        -- sin, cos and tan require a 'math.' 
        -- preffix.
        -- If you do not wish to provide one, put anything you want (e.g. 'none'): the 
        -- content is not interpreted by LuaOptionFile until an actual use of the underlying function. 
        lua_function = none,
        
        -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'. The various 
        -- domains given here must not intersect. 
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_id = { },
        
        -- Value of the parameter in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_value = { }
        
    }, -- Kappa2
    
    PoissonRatio = {
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...)
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function', 'at_quadrature_point', 'piecewise_constant_by_domain'})
        nature = "constant",
        
        -- Value of the volumic mass in the case nature is 'constant'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: VALUE
        scalar_value = 0.,
        
        -- Value of the volumic mass in the case nature is 'lua_function'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: Function in Lua language, for instance:
        -- 		-- function(arg1, arg2, arg3)
        -- return arg1 + arg2 -
        -- arg3
        -- end
        -- sin, cos and tan require a 'math.'
        -- preffix.
        -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
        -- content is not interpreted by LuaOptionFile until an actual use of the underlying function.
        lua_function = none,
        
        -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'. The various
        -- domains given here must not intersect.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_id = {},
        
        -- Value of the parameter in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_value = {},
        
    }, -- PoissonRatio
    
    YoungModulus = {
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...)
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function', 'at_quadrature_point', 'piecewise_constant_by_domain'})
        nature = 'constant',
        
        -- Value of the volumic mass in the case nature is 'constant'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: VALUE
        -- Constraint: v > 0.
        scalar_value =  1.,
        
        -- Value of the volumic mass in the case nature is 'lua_function'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: Function in Lua language, for instance:
        -- 		-- function(arg1, arg2, arg3)
        -- return arg1 + arg2 -
        -- arg3
        -- end
        -- sin, cos and tan require a 'math.'
        -- preffix.
        -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
        -- content is not interpreted by LuaOptionFile until an actual use of the underlying function.
        lua_function = none,
        
        -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'. The various
        -- domains given here must not intersect.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_id = {},
        
        -- Value of the parameter in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_value = {},
        
    }, -- YoungModulus
    
    LameLambda = {
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose
        -- "ignore" if you do not want this parameter (in this case it will stay at nullptr).
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','at_quadrature_point','piecewise_constant_by_domain'})
        nature = "ignore",
        
        -- Value of the volumic mass in the case nature is 'constant'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: VALUE
        scalar_value = 0.,
        
        -- Value of the volumic mass in the case nature is 'lua_function'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: Function in Lua language, for instance:
        -- 		-- function(arg1, arg2, arg3)
        -- return arg1 + arg2 -
        -- arg3
        -- end
        -- sin, cos and tan require a 'math.'
        -- preffix.
        -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
        -- content is not interpreted by LuaOptionFile until an actual use of the underlying function.
        lua_function = none,
        
        -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'. The various
        -- domains given here must not intersect.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_id = { },
        
        -- Value of the parameter in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_value = { }
        
    }, -- LameLambda
    
    LameMu = {
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose
        -- "ignore" if you do not want this parameter (in this case it will stay at nullptr).
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','at_quadrature_point','piecewise_constant_by_domain'})
        nature = "ignore",
        
        -- Value of the volumic mass in the case nature is 'constant'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: VALUE
        scalar_value = 0.,
        
        -- Value of the volumic mass in the case nature is 'lua_function'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: Function in Lua language, for instance:
        -- 		-- function(arg1, arg2, arg3)
        -- return arg1 + arg2 -
        -- arg3
        -- end
        -- sin, cos and tan require a 'math.'
        -- preffix.
        -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
        -- content is not interpreted by LuaOptionFile until an actual use of the underlying function.
        lua_function = none,
        
        -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'. The various
        -- domains given here must not intersect.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_id = { },
        
        -- Value of the parameter in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_value = { }
        
    }, -- LameMu
    
    Viscosity = {
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose
        -- "ignore" if you do not want this parameter (in this case it will stay at nullptr).
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','at_quadrature_point','piecewise_constant_by_domain'})
        nature = "constant",
        
        -- Value of the volumic mass in the case nature is 'constant'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: VALUE
        scalar_value = 1.,
        
        -- Value of the volumic mass in the case nature is 'lua_function'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: Function in Lua language, for instance:
        -- 		-- function(arg1, arg2, arg3)
        -- return arg1 + arg2 -
        -- arg3
        -- end
        -- sin, cos and tan require a 'math.'
        -- preffix.
        -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
        -- content is not interpreted by LuaOptionFile until an actual use of the underlying function. 
        lua_function = none,
        
        -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'. The various 
        -- domains given here must not intersect. 
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_id = { },
        
        -- Value of the parameter in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_value = { }
        
    }, -- Viscosity
    
    -- For 2D operators, which approximation to use.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'irrelevant', 'plane_strain', 'plane_stress'})
    PlaneStressStrain = "plane_strain",
    
} -- Solid

Fluid = {
    
    -- Viscosity
    Viscosity = {
        -- How is given the viscosity (as a constant, as a Lua function, per quadrature point, etc...)
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {'constant', 'lua_function', 'piecewise_constant_by_domain'})
        nature = "constant",
        
        -- Value of the viscosity in the case nature is 'constant'(and also initial value if nature is 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: VALUE
        -- Constraint: v > 0.
        scalar_value = 1.,
        
        -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_id = {},
        
        -- Value of the Young modulus in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_value = {},
        
        -- Value of the viscosity in the case nature is 'lua_function'(and also initial value if nature is 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: Function in Lua language, for instance:
        -- function(arg1, arg2, arg3)
        -- return arg1 + arg2 - arg3
        -- end
        -- sin, cos and tan require a 'math.' preffix.
        
        lua_function = function (x, y, z)
        return 1.;
        end
        
    }
    
} -- Fluid

-- ReactionMitchellSchaeffer
ReactionMitchellSchaeffer = {
    -- Value of Tau_in in MitchellSchaeffer.
    -- Expected format: VALUE
    tau_in = 0.2,
    
    -- Value of Tau_out in MitchellSchaeffer.
    -- Expected format: VALUE
    tau_out = 6,
    
    -- Value of Tau_open in MitchellSchaeffer.
    -- Expected format: VALUE
    tau_open = 120,
    
    tau_close = {
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...)
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function', 'at_quadrature_point', 'piecewise_constant_by_domain'})
        nature = "constant",
        
        -- Value of the volumic mass in the case nature is 'constant'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: VALUE
        scalar_value = 300.,
        
        -- Value of the volumic mass in the case nature is 'lua_function'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: Function in Lua language, for instance:
        -- 		-- function(arg1, arg2, arg3)
        -- return arg1 + arg2 -
        -- arg3
        -- end
        -- sin, cos and tan require a 'math.'
        -- preffix.
        -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
        -- content is not interpreted by LuaOptionFile until an actual use of the underlying function.
        lua_function = none,
        
        -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'. The various
        -- domains given here must not intersect.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_id = { },
        
        -- Value of the parameter in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_value = { }
        
    }, -- tau_close
    
    -- Value of U_gate in MitchellSchaeffer.
    -- Expected format: VALUE
    u_gate = 0.13,
    
    -- Value of U_min in MitchellSchaeffer.
    -- Expected format: VALUE
    u_min = 0.,
    
    -- Value of U_max in MitchellSchaeffer.
    -- Expected format: VALUE
    u_max = 1.
}

-- InitialConditionGate
InitialConditionGate = {
    -- Value of Initial Condition Gate in ReactionDiffusion.
    -- Expected format: VALUE
    initial_condition_gate = 1.,
    
    -- Either Write the Gate at each time step or not.
    -- Expected format: 'true' or 'false' (without the quote)
    write_gate = false
}

Result = {

	-- Directory in which all the results will be written.  
	-- Expected format: "VALUE"
	output_directory = "${MOREFEM_TEST_OUTPUT_DIR}/TestFunctions",

	-- Enables to skip some printing in the console. Can be used to WriteSolution every n time.
	-- Expected format: VALUE
	display_value = 1

} -- Result

