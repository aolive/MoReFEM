/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 14 Nov 2013 13:42:29 +0100
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_GRAD_GRAD_x_INPUT_PARAMETER_LIST_HPP_
# define MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_GRAD_GRAD_x_INPUT_PARAMETER_LIST_HPP_

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/InputParameterData/InputParameterList.hpp"
# include "Core/InputParameter/Geometry/Domain.hpp"
# include "Core/InputParameter/FElt/FEltSpace.hpp"
# include "Core/InputParameter/FElt/Unknown.hpp"
# include "Core/InputParameter/FElt/NumberingSubset.hpp"
# include "Core/InputParameter/Parameter/Fiber/Fiber.hpp"
# include "Core/InputParameter/Parameter/Source/ScalarTransientSource.hpp"
# include "Core/InputParameter/Parameter/Solid/Solid.hpp"
# include "Core/InputParameter/Parameter/Fluid/Fluid.hpp"
# include "Core/InputParameter/Reaction/MitchellSchaeffer.hpp"
# include "Core/InputParameter/InitialConditionGate.hpp"

namespace MoReFEM
{


    namespace TestNS::GradGrad
    {


        //! \copydoc doxygen_hide_mesh_enum
        enum class MeshIndex
        {
            mesh = 1
        };


        //! \copydoc doxygen_hide_domain_enum
        enum class DomainIndex
        {
            domain = 1u
        };


        //! \copydoc doxygen_hide_felt_space_enum
        enum class FEltSpaceIndex
        {
            felt_space = 1u
        };


        //! \copydoc doxygen_hide_unknown_enum
        enum class UnknownIndex
        {
            scalar_unknown_P1 = 1u,
            other_scalar_unknown_P1 = 2u,
            scalar_unknown_P2 = 3u,
            vectorial_unknown_P1 = 4u
        };


        //! \copydoc doxygen_hide_numbering_subset_enum
        enum class NumberingSubsetIndex
        {
            scalar_unknown_P1 = 1u,
            other_scalar_unknown_P1 = 2u,
            scalar_unknown_P2 = 3u,
            vectorial_unknown_P1 = 4u
        };


        //! \copydoc doxygen_hide_solver_enum
        enum class SolverIndex

        {
            solver = 1u
        };


        //! \copydoc doxygen_hide_input_parameter_tuple
        using InputParameterTuple = std::tuple
        <
            InputParameter::TimeManager,

            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::scalar_unknown_P1)>,
            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::other_scalar_unknown_P1)>,
            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::scalar_unknown_P2)>,
            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::vectorial_unknown_P1)>,

            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::scalar_unknown_P1)>,
            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::other_scalar_unknown_P1)>,
            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::scalar_unknown_P2)>,
            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::vectorial_unknown_P1)>,

            InputParameter::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            InputParameter::Domain<EnumUnderlyingType(DomainIndex::domain)>,

            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::felt_space)>,

            InputParameter::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputParameter::Result
        >;


        //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = InputParameterList<InputParameterTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputParameterList>;


    } // namespace TestNS::GradGrad


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_GRAD_GRAD_x_INPUT_PARAMETER_LIST_HPP_
