/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Tools/CatchMainTest.hpp"
#include "Test/Tools/Fixture.hpp"

#include "Test/Operators/VariationalInstances/GradGrad/Model.hpp"
#include "Test/Operators/VariationalInstances/GradGrad/InputParameterList.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    struct LuaFile
    {


        static std::string GetPath();


    };

    using fixture_type =
        TestNS::Fixture
        <
            TestNS::GradGrad::Model,
            LuaFile
        >;


} // namespace anonymous



TEST_CASE_METHOD(fixture_type, "Unknown and test function unknown are identical")
{
    SECTION("P1 case")
    {
        GetModel().SameUnknownP1(UnknownNS::Nature::scalar);
    }

    SECTION("P1 case, vectorial unknown")
    {
        GetModel().SameUnknownP1(UnknownNS::Nature::vectorial);
    }

    SECTION("P2 case")
    {
        GetModel().SameUnknownP2();
    }
        
}


TEST_CASE_METHOD(fixture_type, "Unknown and test function unknown are different but share the same P1 shape function label")
{
    GetModel().UnknownP1TestP1();
}


TEST_CASE_METHOD(fixture_type, "Unknown and test function unknown are different; unknown is P2 and test function P1")
{
    GetModel().UnknownP2TestP1();
}


namespace // anonymous
{


    std::string LuaFile::GetPath()
    {
        decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

        return
            environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Operators/"
                                                       "VariationalInstances/GradGrad/"
                                                       "demo_input_parameter_2D.lua");
    }

} // namespace anonymous

