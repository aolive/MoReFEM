//! \file 
//
//
//  SameUnknown.cpp
//  MoReFEM
//
//  Created by sebastien on 14/03/2018.
//Copyright © 2018 Inria. All rights reserved.
//

#include "ThirdParty/Source/Catch/catch.hpp"

#include "Test/Tools/TestLinearAlgebra.hpp"
#include "Test/Operators/VariationalInstances/GradGrad/Model.hpp"
#include "Test/Operators/VariationalInstances/GradGrad/ExpectedResults.hpp"


namespace MoReFEM
{
    
    
    namespace TestNS::GradGrad
    {


        void Model::UnknownP2TestP1() const
        {
            const auto& god_of_dof = parent::GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));

            decltype(auto) unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);

            const auto& felt_space =
            god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::felt_space));

            const auto& scalar_unknown_P1_ptr =
            unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::scalar_unknown_P1));

            const auto& scalar_unknown_P2_ptr =
            unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::scalar_unknown_P2));


            GlobalVariationalOperatorNS::GradPhiGradPhi variational_operator(felt_space,
                                                                             scalar_unknown_P1_ptr,
                                                                             scalar_unknown_P2_ptr);

            decltype(auto) numbering_subset_p1 =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::scalar_unknown_P1));
            decltype(auto) numbering_subset_p2 =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::scalar_unknown_P2));

            GlobalMatrix matrix(numbering_subset_p2, numbering_subset_p1);
            AllocateGlobalMatrix(god_of_dof, matrix);

            const auto& mesh = god_of_dof.GetMesh();
            const auto dimension = mesh.GetDimension();

            matrix.ZeroEntries(__FILE__, __LINE__);

            GlobalMatrixWithCoefficient matrix_with_coeff(matrix, 1.);

            variational_operator.Assemble(std::make_tuple(std::ref(matrix_with_coeff)));

            CHECK_NOTHROW(CheckMatrix(god_of_dof,
                                      matrix,
                                      GetExpectedMatrixP2P1(dimension),
                                      __FILE__, __LINE__,
                                      1.e-5));

        }
             
        
    } // namespace TestNS::GradGrad


} // namespace MoReFEM
