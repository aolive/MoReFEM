/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sat, 16 Sep 2017 22:22:57 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include "Test/Operators/VariationalInstances/GradGrad/ExpectedResults.hpp"


namespace MoReFEM
{
    
    
    namespace TestNS::GradGrad
    {


        namespace // anonymous
        {


            expected_results_type<IsMatrixOrVector::matrix> Matrix3D(UnknownNS::Nature scalar_or_vectorial);
            expected_results_type<IsMatrixOrVector::matrix> Matrix2D(UnknownNS::Nature scalar_or_vectorial);
            expected_results_type<IsMatrixOrVector::matrix> Matrix1D();


        } // namespace anonymous


        expected_results_type<IsMatrixOrVector::matrix> GetExpectedMatrixP1P1(unsigned int dimension,
                                                                              UnknownNS::Nature scalar_or_vectorial)
        {
            switch (dimension)
            {
                case 3u:
                    return Matrix3D(scalar_or_vectorial);
                case 2u:
                    return Matrix2D(scalar_or_vectorial);
                case 1u:
                    return Matrix1D();
                default:
                    assert(false && "Invalid case!");
                    exit(EXIT_FAILURE);
            }
        }


        namespace // anonymous
        {


            expected_results_type<IsMatrixOrVector::matrix> Matrix3D(UnknownNS::Nature scalar_or_vectorial)
            {
                constexpr double one_6th = 1. / 6.;

                switch(scalar_or_vectorial)
                {
                    case UnknownNS::Nature::scalar:
                        return expected_results_type<IsMatrixOrVector::matrix>
                        {
                            { .5, -one_6th, -one_6th, -one_6th },
                            { -one_6th, one_6th, 0., 0. },
                            { -one_6th, 0., one_6th, 0. },
                            { -one_6th, 0., 0., one_6th },
                        };
                    case UnknownNS::Nature::vectorial:
                        return expected_results_type<IsMatrixOrVector::matrix>
                        {
                            { .5, 0., 0., -one_6th, 0., 0., -one_6th, 0., 0., -one_6th, 0., 0. },
                            { 0., .5, 0., 0., -one_6th, 0., 0., -one_6th, 0., 0., -one_6th, 0. },
                            { 0., 0., .5, 0., 0., -one_6th, 0., 0., -one_6th, 0., 0., -one_6th },

                            { -one_6th, 0., 0., one_6th, 0., 0., 0., 0., 0., 0., 0., 0. },
                            { 0., -one_6th, 0., 0., one_6th, 0., 0., 0., 0., 0., 0., 0. },
                            { 0., 0., -one_6th, 0., 0., one_6th, 0., 0., 0., 0., 0., 0. },

                            { -one_6th, 0., 0., 0., 0., 0., one_6th, 0., 0., 0., 0., 0. },
                            { 0., -one_6th, 0., 0., 0., 0., 0., one_6th, 0., 0., 0., 0. },
                            { 0., 0., -one_6th, 0., 0., 0., 0., 0., one_6th, 0., 0., 0. },

                            { -one_6th, 0., 0., 0., 0., 0., 0., 0., 0., one_6th, 0., 0. },
                            { 0., -one_6th, 0., 0., 0., 0., 0., 0., 0., 0., one_6th, 0. },
                            { 0., 0., -one_6th, 0., 0., 0., 0., 0., 0., 0., 0., one_6th },

                        };

                } // switch

                assert(false);
                exit(EXIT_FAILURE);

            }


            expected_results_type<IsMatrixOrVector::matrix> Matrix2D(UnknownNS::Nature scalar_or_vectorial)
            {

                switch(scalar_or_vectorial)
                {
                    case UnknownNS::Nature::scalar:
                        return expected_results_type<IsMatrixOrVector::matrix>
                    {
                        { 1., -.5, -.5 },
                        { -.5, .5, 0. },
                        { -.5, 0., .5 },
                    };
                    case UnknownNS::Nature::vectorial:
                        return expected_results_type<IsMatrixOrVector::matrix>
                    {
                        {  1., 0.,  -.5, 0.,  -.5 , 0. },
                        { 0.,  1., 0.,  -.5, 0.,  -.5  },
                        {  -.5, 0.,  .5, 0.,  0. , 0. },
                        { 0.,  -.5, 0.,  .5, 0.,  0.  },
                        {  -.5, 0.,  0., 0.,  .5 , 0. },
                        { 0.,  -.5, 0.,  0., 0.,  .5  }
                    };

                } // switch

                assert(false);
                exit(EXIT_FAILURE);
            }


            expected_results_type<IsMatrixOrVector::matrix> Matrix1D()
            {
                return expected_results_type<IsMatrixOrVector::matrix>
                {
                    { 1., -1. },
                    { -1., 1. },
                };
            }


        } // namespace anonymous



    } // namespace TestNS::GradGrad


} // namespace MoReFEM


