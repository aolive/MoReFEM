/// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 2 Aug 2017 12:30:53 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include "ThirdParty/Source/Catch/catch.hpp"

#include "Test/Operators/VariationalInstances/GradGrad/Model.hpp"
#include "Test/Operators/VariationalInstances/GradGrad/ExpectedResults.hpp"


namespace MoReFEM
{
    
    
    namespace TestNS::GradGrad
    {


        Model::Model(const morefem_data_type& morefem_data)
        : parent(morefem_data, create_domain_list_for_coords::yes, print_banner::no)
        {
            decltype(auto) mpi = parent::GetMpi();

            if (mpi.Nprocessor<int>() > 1 )
            {
                throw Exception("The point of this test is to check operator assemble correctly into a matrix and/or "
                                "a vector; the expected values assume the dof numbering of the sequential case. Please "
                                "run it sequentially.",
                                __FILE__, __LINE__);
            }
        }


        void Model::SupplInitialize()
        {
            quadrature_rule_per_topology_for_operators_ = std::make_unique<const QuadratureRulePerTopology>(10,
                                                                                                            3);

            // Required to enable construction of an operator after initialization step.
            parent::SetClearGodOfDofTemporaryDataToFalse();
        }


        void Model::Forward()
        { }


        void Model::SupplFinalizeStep()
        { }
        

        void Model::SupplFinalize()
        { }


    } // namespace TestNS::GradGrad


} // namespace MoReFEM
