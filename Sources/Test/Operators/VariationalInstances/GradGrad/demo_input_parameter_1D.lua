-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.

transient = {

	-- Tells which policy is used to describe time evolution.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'constant_time_step'})
	time_evolution_policy = "constant_time_step",

	-- Time at the beginning of the code (in seconds).
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	init_time = 0.,

	-- Time step between two iterations, in seconds.
	-- Expected format: VALUE
	-- Constraint: v > 0.
	timeStep = 0.1,
    
    -- Minimum time step between two iterations, in seconds.
    -- Expected format: VALUE
    -- Constraint: v > 0.
    minimum_time_step = 0.1,

	-- Maximum time, if set to zero run a static case.
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	timeMax = 0.

} -- transient

NumberingSubset1 = {

    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = "scalar_unknown_P1",

    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- vectorial_unknown.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false


} -- NumberingSubset1

NumberingSubset2 = {

    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = "other_scalar_unknown_P1",

    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- vectorial_unknown.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false


} -- NumberingSubset2

NumberingSubset3 = {

    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = "scalar_unknown_P2",

    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- vectorial_unknown.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false


} -- NumberingSubset3

NumberingSubset4 = {

    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = "vectorial_unknown",

    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- vectorial_unknown.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false


} -- NumberingSubset4



Unknown1 = {

    -- Name of the unknown (used for displays in output).
    -- Expected format: "VALUE"
    name = "scalar_unknown_P1",

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'scalar', 'vectorial'})
    nature = "scalar",

} -- Unknown1

Unknown2 = {

    -- Name of the unknown (used for displays in output).
    -- Expected format: "VALUE"
    name = "other_scalar_unknown_P1",

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'scalar', 'vectorial'})
    nature = "scalar",

} -- Unknown2

Unknown3 = {

    -- Name of the unknown (used for displays in output).
    -- Expected format: "VALUE"
    name = "scalar_unknown_P2",

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'scalar', 'vectorial'})
    nature = "scalar",

} -- Unknown3

Unknown4 = {

    -- Name of the unknown (used for displays in output).
    -- Expected format: "VALUE"
    name = "vectorial_unknown_P1",

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'scalar', 'vectorial'})
    nature = "vectorial",

} -- Unknown4

Mesh1 = {
    
    -- Path of the mesh file to use.
    -- Expected format: "VALUE"
    --mesh = "${MOREFEM_ROOT}/Data/Mesh/bar_1D_L1_N3_Ensight.geo",
    mesh = "${MOREFEM_ROOT}/Data/Mesh/bar_1D_L1_N2_Ensight.geo",
    
    -- Format of the input mesh.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'Ensight', 'Medit'})
    format = "Ensight",
    
    -- Highest dimension of the input mesh. This dimension might be lower than the one effectively read in the
    -- mesh file; in which case Coords will be reduced provided all the dropped values are 0. If not, an
    -- exception is thrown.
    -- Expected format: VALUE
    -- Constraint: v <= 3 and v > 0
    dimension = 1,
    
    -- Space unit of the mesh.
    -- Expected format: VALUE
    space_unit = 1.
    
} -- Mesh1

Domain1 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: value_in(v, {0, 1, 2, 3})
    dimension_list = { 1 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at LuaOptionFile level, as some geometric element types could be added after
    -- generation of current input parameter file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }
    
} -- Domain1



FiniteElementSpace1 = {

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,

    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 1,

    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = { "scalar_unknown_P1", "other_scalar_unknown_P1", "scalar_unknown_P2", "vectorial_unknown_P1" },

    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = { "P1", "P1", "P2", "P1" },

    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 1, 2, 3, 4 },

} -- FiniteElementSpace1


Petsc1 = {

	-- Absolute tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	absoluteTolerance = 1e-50,

	-- gmresStart
	-- Expected format: VALUE
	-- Constraint: v >= 0
	gmresRestart = 200,

	-- Maximum iteration
	-- Expected format: VALUE
	-- Constraint: v > 0
	maxIteration = 1000,

	-- Preconditioner to use. Must be lu for any direct solver.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'lu', 'none'})
	preconditioner = 'lu',

	-- Relative tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	relativeTolerance = 1e-9,

	-- Step size tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	stepSizeTolerance = 1e-8,

	-- Solver to use.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, { 'Mumps', 'Umfpack', 'Gmres' })
	solver = 'Mumps'

} -- Petsc1



Result = {

	-- Directory in which all the results will be written.  
	-- Expected format: "VALUE"
	output_directory = "${MOREFEM_TEST_OUTPUT_DIR}/Test/Operators/GradGrad/3D",

	-- Enables to skip some printing in the console. Can be used to WriteSolution every n time.
	-- Expected format: VALUE
	display_value = 1

} -- Result

