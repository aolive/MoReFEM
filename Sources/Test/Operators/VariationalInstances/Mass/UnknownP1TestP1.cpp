//! \file 
//
//
//  SameUnknown.cpp
//  MoReFEM
//
//  Created by sebastien on 14/03/2018.
//Copyright © 2018 Inria. All rights reserved.
//

#include "ThirdParty/Source/Catch/catch.hpp"

#include "Test/Tools/TestLinearAlgebra.hpp"
#include "Test/Operators/VariationalInstances/Mass/Model.hpp"
#include "Test/Operators/VariationalInstances/Mass/ExpectedResults.hpp"


namespace MoReFEM
{
    
    
    namespace TestNS::Mass
    {


        void Model::UnknownP1TestP1() const
        {
            const auto& god_of_dof = parent::GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));

            decltype(auto) unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);

            const auto& felt_space =
            god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::felt_space));

            const auto& potential_1_ptr =
                unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::potential_P1));

            const auto& other_potential_1_ptr =
                unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::other_potential_P1));


            GlobalVariationalOperatorNS::Mass mass_op(felt_space,
                                                      potential_1_ptr,
                                                      other_potential_1_ptr);

            decltype(auto) numbering_subset_p1 =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::potential_P1));
            decltype(auto) numbering_subset_other_p1 =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::other_potential_P1));

            GlobalMatrix matrix_p1_p1(numbering_subset_other_p1, numbering_subset_p1);
            AllocateGlobalMatrix(god_of_dof, matrix_p1_p1);

            const auto& mesh = god_of_dof.GetMesh();
            const auto dimension = mesh.GetDimension();

            matrix_p1_p1.ZeroEntries(__FILE__, __LINE__);

            GlobalMatrixWithCoefficient matrix(matrix_p1_p1, 1.);

            mass_op.Assemble(std::make_tuple(std::ref(matrix)));

            CHECK_NOTHROW(CheckMatrix(god_of_dof,
                                      matrix_p1_p1,
                                      GetExpectedMatrixP1P1(dimension, UnknownNS::Nature::scalar),
                                      __FILE__, __LINE__,
                                      1.e-5));
          
        }
        
     
        
    } // namespace TestNS::Mass


} // namespace MoReFEM
