/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sat, 16 Sep 2017 22:22:57 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include "Test/Operators/VariationalInstances/NonlinearMembrane/ExpectedResults.hpp"


namespace MoReFEM
{
    
    
    namespace TestNS::NonLinearMembraneOperatorNS
    {


        expected_results_type<IsMatrixOrVector::matrix> GetExpectedMatrixP1P1()
        {
            // The values below have been computed independantly in Matlab by Dominique Chapelle.
            return expected_results_type<IsMatrixOrVector::matrix>
            {
                {
                    3.1217173e+09, -5.3254715e+08, -3.0275440e+08, -2.8516195e+09, 4.1341198e+08,
                    2.1134163e+08,  -2.7009781e+08, 1.1913516e+08, 9.1412770e+07
                },
                {
                    -5.3254715e+08, 8.0440892e+09, 2.2840439e+09, 4.3393421e+08, -7.4416177e+09,
                    -2.0490275e+09, 9.8612930e+07, -6.0247154e+08, -2.3501641e+08
                },
                {
                    -3.0275440e+08, 2.2840439e+09, 4.1100877e+09, 2.1831961e+08, -2.0269924e+09,
                    -3.7093376e+09, 8.4434793e+07, -2.5705148e+08, -4.0075010e+08
                },
                {
                    -2.8516195e+09, 4.3393421e+08, 2.1831961e+08, 3.0203942e+09, -3.1979658e+08,
                    -1.6118229e+08, -1.6877461e+08, -1.1413764e+08, -5.7137319e+07
                },
                {
                    4.1341198e+08, -7.4416177e+09, -2.0269924e+09, -3.1979658e+08, 8.0377623e+09,
                    2.0659864e+09, -9.3615406e+07, -5.9614462e+08, -3.8993961e+07
                },
                {
                    2.1134163e+08, -2.0490275e+09, -3.7093376e+09, -1.6118229e+08, 2.0659864e+09,
                    3.8470915e+09, -5.0159341e+07, -1.6958891e+07, -1.3775390e+08
                },
                {
                    -2.7009781e+08, 9.8612930e+07, 8.4434793e+07, -1.6877461e+08, -9.3615406e+07,
                    -5.0159341e+07, 4.3887242e+08, -4.9975241e+06, -3.4275451e+07
                },
                {
                    1.1913516e+08, -6.0247154e+08, -2.5705148e+08, -1.1413764e+08, -5.9614462e+08,
                    -1.6958891e+07, -4.9975241e+06, 1.1986162e+09, 2.7401037e+08
                },
                {
                    9.1412770e+07, -2.3501641e+08, -4.0075010e+08, -5.7137319e+07, -3.8993961e+07,
                    -1.3775390e+08, -3.4275451e+07, 2.7401037e+08, 5.3850401e+08
                }
            };


        }


        expected_results_type<IsMatrixOrVector::vector> GetExpectedVectorP1P1()
        {
            // The values below have been computed independantly in Matlab by Dominique Chapelle.
            return  expected_results_type<IsMatrixOrVector::vector>
            {
                3.3424261e+10,
                -3.7758597e+11,
                -1.6427512e+11,
                -2.5069387e+10,
                4.2107970e+11,
                1.7009310e+11,
                -8.3548746e+09,
                -4.3493729e+10,
                -5.8179833e+09
            };
        }


    } // namespace TestNS::NonLinearMembraneOperatorNS


} // namespace MoReFEM


