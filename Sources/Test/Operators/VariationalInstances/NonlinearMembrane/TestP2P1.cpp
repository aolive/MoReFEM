/// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 3 Feb 2017 11:26:22 +0100
/// Copyright (c) Inria. All rights reserved.
///

#include "ThirdParty/Source/Catch/catch.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/NonlinearMembrane.hpp"

#include "Test/Operators/VariationalInstances/NonlinearMembrane/Model.hpp"
#include "Test/Operators/VariationalInstances/NonlinearMembrane/ExpectedResults.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM
{
    
    
    namespace TestNS::NonLinearMembraneOperatorNS
    {


        namespace // anonymous
        {


            using ::MoReFEM::Internal::assemble_into_matrix;

            using ::MoReFEM::Internal::assemble_into_vector;


        } // namespace anonymous


        void Model::TestP2P1() const
        {
            const auto& god_of_dof = GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));

            decltype(auto) morefem_data = parent::GetMoReFEMData();

            const auto& displacement_p1_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::displacementP1));

            const auto& displacement_p2_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::displacementP2));

            const auto& felt_space_surface = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::surface));

            decltype(auto) domain_full_mesh =
            DomainManager::GetInstance(__FILE__, __LINE__).GetDomain(EnumUnderlyingType(DomainIndex::full_mesh), __FILE__, __LINE__);

            Solid solid(morefem_data.GetInputParameterList(),
                        domain_full_mesh,
                        felt_space_surface.GetQuadratureRulePerTopology());

            decltype(auto) unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);

            const auto& displacement_p1_ptr =
                unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::displacementP1));

            const auto& displacement_p2_ptr =
                unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::displacementP2));

            namespace GVO = GlobalVariationalOperatorNS;

            namespace IPL = Utilities::InputParameterListNS;

            using scalar_parameter_type = Internal::ParameterNS::ParameterInstance
            <
                ParameterNS::Type::scalar,
                ::MoReFEM::ParameterNS::Policy::Constant,
                ParameterNS::TimeDependencyNS::None
            >;

            decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);

            decltype(auto) domain_surface =
            domain_manager.GetDomain(EnumUnderlyingType(DomainIndex::surface), __FILE__, __LINE__);

            scalar_parameter_type thickness("Thickness",
                                            domain_surface,
                                            1.);

            GVO::NonlinearMembrane stiffness_operator(felt_space_surface,
                                                      displacement_p2_ptr,
                                                      displacement_p1_ptr,
                                                      solid.GetYoungModulus(),
                                                      solid.GetPoissonRatio(),
                                                      thickness);

            GlobalMatrix matrix_tangent_stiffness(displacement_p1_numbering_subset, displacement_p2_numbering_subset);
            AllocateGlobalMatrix(god_of_dof, matrix_tangent_stiffness);

            GlobalVector vector_stiffness_residual(displacement_p1_numbering_subset);
            AllocateGlobalVector(god_of_dof, vector_stiffness_residual);

            GlobalVector previous_iteration(displacement_p2_numbering_subset);
            AllocateGlobalVector(god_of_dof, previous_iteration);

            {
                Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write>
                content(previous_iteration,
                        __FILE__, __LINE__);

                content[0] = 30.;
                content[3] = -42.;
                content[6] = -17.;
                content[1] = 10.;
                content[4] = 97.;
                content[7] = 41.;
                content[2] = 5.;
                content[5] = -84.;
                content[8] = -20.5;
                content[9] = 1.84;
                content[10] = -113.98;
                content[11] = -8.97;
                content[12] = 29.43;
                content[13] = .32;
                content[14] = -49.83;
                content[15] = 2.432;
                content[16] = -231.24;
                content[17] = -70.5;
            }

            GlobalMatrixWithCoefficient mat(matrix_tangent_stiffness, 1.);
            GlobalVectorWithCoefficient vec(vector_stiffness_residual, 1.);

            stiffness_operator.Assemble(std::make_tuple(std::ref(mat), std::ref(vec)),
                                        previous_iteration);

            CHECK_NOTHROW(CheckMatrix(god_of_dof,
                                      matrix_tangent_stiffness,
                                      GetExpectedMatrixP2P1(),
                                      __FILE__, __LINE__,
                                      1.e6)); // #1259 Obviously we need more precise reference values!

            CHECK_NOTHROW(CheckVector(god_of_dof,
                                      vector_stiffness_residual,
                                      GetExpectedVectorP2P1(),
                                      __FILE__, __LINE__,
                                      1.e6)); // #1259 Obviously we need more precise reference values!

        }

        
    } // namespace TestNS::NonLinearMembraneOperatorNS


} // namespace MoReFEM
