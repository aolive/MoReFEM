/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sat, 16 Sep 2017 22:22:57 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include "Test/Operators/VariationalInstances/NonlinearMembrane/ExpectedResults.hpp"


namespace MoReFEM
{
    
    
    namespace TestNS::NonLinearMembraneOperatorNS
    {


        expected_results_type<IsMatrixOrVector::matrix> GetExpectedMatrixP2P1()
        {
            // #1259 The results below are those computed with current instance; Dominique should provide soon
            // independently obtained ones with greater precision.
            return expected_results_type<IsMatrixOrVector::matrix>
            {
                { 9.24496e+08, -5.08502e+08, 0., -5.59314e+08, 1.22953e+09, 0., -3.65182e+08, -7.21025e+08, 0., 1.17058e+09, -1.76323e+08, 0., -3.50671e+09, 4.52115e+08, 0., 2.33612e+09, -2.75792e+08, 0. },
                { -2.90145e+08, 4.20034e+09, 0., 1.89397e+08, -9.60781e+09, 0., 1.00747e+08, 5.40747e+09, 0., 1.91481e+09, 9.97412e+08, 0., -2.53626e+08, -2.34382e+09, 0., -1.66118e+09, 1.34641e+09, 0. },
                { -1.13029e+08, 2.12631e+09, 0., 6.7287e+07, -2.99312e+09, 0., 4.57416e+07, 8.66813e+08, 0., 4.02989e+08, 1.84602e+09, 0., 1.20035e+08, -4.44324e+09, 0., -5.23024e+08, 2.59721e+09, 0. },
                { -5.59314e+08, 2.42443e+08, 0., 4.08456e+08, -8.06065e+08, 0., 1.50857e+08, 5.63622e+08, 0., -4.14032e+08, 7.52451e+07, 0., 2.09165e+09, -2.69148e+08, 0., -1.67761e+09, 1.93903e+08, 0. },
                { 1.91276e+08, -1.79835e+09, 0., -1.45608e+08, 5.90083e+09, 0., -4.5668e+07, -4.10249e+09, 0., -1.23153e+09, -3.52266e+08, 0., 2.82533e+08, 1.32832e+09, 0., 9.48999e+08, -9.76054e+08, 0. },
                { 6.8948e+07, -1.01966e+09, 0., -4.84758e+07, 1.82055e+09, 0., -2.04723e+07, -8.00892e+08, 0., -2.54713e+08, -7.00645e+08, 0., -3.17787e+07, 2.59721e+09, 0., 2.86492e+08, -1.89657e+09, 0. },
                { -3.65182e+08, 2.66059e+08, 0., 1.50857e+08, -4.23462e+08, 0., 2.14325e+08, 1.57403e+08, 0., -7.56552e+08, 1.01077e+08, 0., 1.41506e+09, -1.82966e+08, 0., -6.58509e+08, 8.18891e+07, 0. },
                { 9.88684e+07, -2.40199e+09, 0., -4.3789e+07, 3.70697e+09, 0., -5.50795e+07, -1.30498e+09, 0., -6.83278e+08, -6.45146e+08, 0., -2.89073e+07, 1.0155e+09, 0., 7.12185e+08, -3.70356e+08, 0. },
                { 4.40806e+07, -1.10665e+09, 0., -1.88113e+07, 1.17257e+09, 0., -2.52693e+07, -6.59212e+07, 0., -1.48276e+08, -1.14538e+09, 0., -8.82561e+07, 1.84602e+09, 0., 2.36532e+08, -7.00645e+08, 0. }
            };
        }


        expected_results_type<IsMatrixOrVector::vector> GetExpectedVectorP2P1()
        {
            // #1259 The results below are those computed with current instance; Dominique should provide soon
            // independently obtained ones with greater precision.
            return  expected_results_type<IsMatrixOrVector::vector>
            {
                1.63454e+10,
                -8.48116e+10,
                -3.56282e+10,
                -9.66401e+09,
                4.64395e+10,
                1.77902e+10,
                -6.68136e+09,
                3.83721e+10,
                1.78379e+10
            };
        }


    } // namespace TestNS::NonLinearMembraneOperatorNS


} // namespace MoReFEM


