/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Tools/CatchMainTest.hpp"
#include "Test/Tools/Fixture.hpp"

#include "Test/Operators/VariationalInstances/NonlinearMembrane/Model.hpp"
#include "Test/Operators/VariationalInstances/NonlinearMembrane/InputParameterList.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    struct LuaFile
    {


        static std::string GetPath();


    };

    using fixture_type =
    TestNS::Fixture
    <
        TestNS::NonLinearMembraneOperatorNS::Model,
        LuaFile
    >;


    using ::MoReFEM::Internal::assemble_into_matrix;

    using ::MoReFEM::Internal::assemble_into_vector;


} // namespace anonymous



TEST_CASE_METHOD(fixture_type, "Unknown and test function unknown are identical (P1)")
{
    GetModel().TestP1P1(assemble_into_matrix::yes, assemble_into_vector::yes);
    GetModel().TestP1P1(assemble_into_matrix::yes, assemble_into_vector::no);
    GetModel().TestP1P1(assemble_into_matrix::no, assemble_into_vector::yes);
}



TEST_CASE_METHOD(fixture_type, "Unknown is P2 and test function P1")
{
    GetModel().TestP2P1();
}


namespace // anonymous
{


    std::string LuaFile::GetPath()
    {
        decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

        return
        environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Operators/"
                                                   "VariationalInstances/NonlinearMembrane/"
                                                   "demo_input_nonlinear_membrane.lua");
    }

} // namespace anonymous

