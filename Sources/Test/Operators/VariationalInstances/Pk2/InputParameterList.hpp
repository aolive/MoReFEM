/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 14 Nov 2013 13:42:29 +0100
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_PK2_x_INPUT_PARAMETER_LIST_HPP_
# define MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_PK2_x_INPUT_PARAMETER_LIST_HPP_

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/InputParameterData/InputParameterList.hpp"
# include "Core/InputParameter/Geometry/Domain.hpp"
# include "Core/InputParameter/FElt/FEltSpace.hpp"
# include "Core/InputParameter/FElt/Unknown.hpp"
# include "Core/InputParameter/FElt/NumberingSubset.hpp"
# include "Core/InputParameter/Parameter/Fiber/Fiber.hpp"
# include "Core/InputParameter/Parameter/Source/ScalarTransientSource.hpp"
# include "Core/InputParameter/Parameter/Solid/Solid.hpp"
# include "Core/InputParameter/Parameter/Fluid/Fluid.hpp"
# include "Core/InputParameter/Reaction/MitchellSchaeffer.hpp"
# include "Core/InputParameter/InitialConditionGate.hpp"

namespace MoReFEM
{


    namespace TestNS::Pk2
    {


        //! \copydoc doxygen_hide_mesh_enum
        enum class MeshIndex
        {
            mesh = 1
        };


        //! \copydoc doxygen_hide_domain_enum
        enum class DomainIndex
        {
            volume = 1
        };


        //! \copydoc doxygen_hide_felt_space_enum
        enum class FEltSpaceIndex
        {
            felt_space = 1,
        };


        //! \copydoc doxygen_hide_unknown_enum
        enum class UnknownIndex
        {
            displacement_p1 = 1,
            displacement_p2 = 2,
            other_displacement_p1 = 3
        };


        //! \copydoc doxygen_hide_numbering_subset_enum
        enum class NumberingSubsetIndex
        {
            displacement_p1 = 1,
            displacement_p2 = 2,
            other_displacement_p1 = 3
        };


        //! \copydoc doxygen_hide_solver_enum
        enum class SolverIndex

        {
            solver = 1
        };



        //! \copydoc doxygen_hide_input_parameter_tuple
        using InputParameterTuple = std::tuple
        <
            InputParameter::TimeManager,

            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement_p1 )>,
            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement_p2)>,
            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::other_displacement_p1)>,

            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::displacement_p1)>,
            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::displacement_p2)>,
            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::other_displacement_p1)>,

            InputParameter::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            InputParameter::Domain<EnumUnderlyingType(DomainIndex::volume)>,

            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::felt_space)>,

            InputParameter::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputParameter::Solid::VolumicMass,
            InputParameter::Solid::HyperelasticBulk,
            InputParameter::Solid::Kappa1,
            InputParameter::Solid::Kappa2,
            InputParameter::Solid::PoissonRatio,
            InputParameter::Solid::YoungModulus,
            InputParameter::Solid::LameLambda,
            InputParameter::Solid::LameMu,
            InputParameter::Solid::Viscosity,

            InputParameter::Result
        >;


        //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = InputParameterList<InputParameterTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputParameterList>;


    } // namespace TestNS::Pk2


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_PK2_x_INPUT_PARAMETER_LIST_HPP_
