//! \file 
//
//
//  SameUnknown.cpp
//  MoReFEM
//
//  Created by sebastien on 14/03/2018.
//Copyright © 2018 Inria. All rights reserved.
//

#include "ThirdParty/Source/Catch/catch.hpp"

#include "Test/Tools/TestLinearAlgebra.hpp"
#include "Test/Operators/VariationalInstances/Pk2/Model.hpp"
#include "Test/Operators/VariationalInstances/Pk2/ExpectedResults.hpp"


namespace MoReFEM
{
    
    
    namespace TestNS::Pk2
    {


        namespace // anonymous
        {


            using ::MoReFEM::Internal::assemble_into_matrix;

            using ::MoReFEM::Internal::assemble_into_vector;


        } // namespace anonymous


        void Model::SameUnknown(assemble_into_matrix do_assemble_into_matrix,
                                assemble_into_vector do_assemble_into_vector) const
        {
            const auto& god_of_dof = GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));

            decltype(auto) unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);

            const auto& felt_space =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::felt_space));

            const auto& displacement_p1_ptr =
            unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::displacement_p1));

            StiffnessOperatorType pk2_operator(felt_space,
                                               displacement_p1_ptr,
                                               displacement_p1_ptr,
                                               *solid_,
                                               GetTimeManager(),
                                               hyperelastic_law_parent::GetHyperelasticLawPtr(),
                                               nullptr,
                                               nullptr);

            decltype(auto) numbering_subset_p1 =
            god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::displacement_p1));

            GlobalMatrix matrix_p1_p1(numbering_subset_p1, numbering_subset_p1);
            AllocateGlobalMatrix(god_of_dof, matrix_p1_p1);

            GlobalVector vector_p1(numbering_subset_p1);
            AllocateGlobalVector(god_of_dof, vector_p1);

            GlobalVector previous_iteration_p1(vector_p1);

            const auto& mesh = god_of_dof.GetMesh();
            const auto dimension = mesh.GetDimension();

            {
                matrix_p1_p1.ZeroEntries(__FILE__, __LINE__);
                vector_p1.ZeroEntries(__FILE__, __LINE__);
                previous_iteration_p1.ZeroEntries(__FILE__, __LINE__);

                switch(dimension)
                {
                    case 3:
                        previous_iteration_p1.SetValue(3, 2., INSERT_VALUES, __FILE__, __LINE__);
                        [[fallthrough]];
                    case 2:
                        previous_iteration_p1.SetValue(2, 2., INSERT_VALUES, __FILE__, __LINE__);
                        [[fallthrough]];
                    case 1:
                        previous_iteration_p1.SetValue(0, 1., INSERT_VALUES, __FILE__, __LINE__);
                        previous_iteration_p1.SetValue(1, 2., INSERT_VALUES, __FILE__, __LINE__);
                        break;

                }

                previous_iteration_p1.Assembly(__FILE__, __LINE__);

                GlobalMatrixWithCoefficient matrix(matrix_p1_p1, 1.);
                GlobalVectorWithCoefficient vec(vector_p1, 1.);


                if (do_assemble_into_matrix == assemble_into_matrix::yes
                    && do_assemble_into_vector == assemble_into_vector::yes)
                {
                    pk2_operator.Assemble(std::make_tuple(std::ref(matrix), std::ref(vec)),
                                          previous_iteration_p1);

                    CHECK_NOTHROW(CheckMatrix(god_of_dof,
                                              matrix_p1_p1,
                                              GetExpectedMatrixP1P1(dimension),
                                              __FILE__, __LINE__,
                                              1.e-5));

                    CHECK_NOTHROW(CheckVector(god_of_dof,
                                              vector_p1,
                                              GetExpectedVectorP1P1(dimension),
                                              __FILE__, __LINE__,
                                              1.e-5));
                }
                else if (do_assemble_into_matrix == assemble_into_matrix::yes
                         && do_assemble_into_vector == assemble_into_vector::no)
                {
                    pk2_operator.Assemble(std::make_tuple(std::ref(matrix)),
                                          previous_iteration_p1);

                    CHECK_NOTHROW(CheckMatrix(god_of_dof,
                                              matrix_p1_p1,
                                              GetExpectedMatrixP1P1(dimension),
                                              __FILE__, __LINE__,
                                              1.e-5));
                }
                else if (do_assemble_into_matrix == assemble_into_matrix::no
                     && do_assemble_into_vector == assemble_into_vector::yes)
                {
                    pk2_operator.Assemble(std::make_tuple(std::ref(vec)),
                                          previous_iteration_p1);

                    CHECK_NOTHROW(CheckVector(god_of_dof,
                                              vector_p1,
                                              GetExpectedVectorP1P1(dimension),
                                              __FILE__, __LINE__,
                                              1.e-5));
                }

            }

        }
        
     
        
    } // namespace TestNS::Pk2


} // namespace MoReFEM
