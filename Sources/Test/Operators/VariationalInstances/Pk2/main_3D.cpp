/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Tools/CatchMainTest.hpp"
#include "Test/Tools/Fixture.hpp"

#include "Test/Operators/VariationalInstances/Pk2/Model.hpp"
#include "Test/Operators/VariationalInstances/Pk2/InputParameterList.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    struct LuaFile
    {


        static std::string GetPath();


    };

    using fixture_type =
        TestNS::Fixture
        <
            TestNS::Pk2::Model,
            LuaFile
        >;


    using ::MoReFEM::Internal::assemble_into_matrix;

    using ::MoReFEM::Internal::assemble_into_vector;


} // namespace anonymous



TEST_CASE_METHOD(fixture_type, "Unknown and test function unknown are identical")
{
    GetModel().SameUnknown(assemble_into_matrix::yes, assemble_into_vector::yes);
}


TEST_CASE_METHOD(fixture_type, "Partial non linear assembling (only matrix or only vector) properly implemented")
{
    GetModel().SameUnknown(assemble_into_matrix::yes, assemble_into_vector::no);
    GetModel().SameUnknown(assemble_into_matrix::no, assemble_into_vector::yes);
}


TEST_CASE_METHOD(fixture_type, "Unknown and test function unknown are different but share the same P1 shape function label")
{
    GetModel().UnknownP1TestP1();
}


TEST_CASE_METHOD(fixture_type, "Unknown and test function unknown are different; unknown is P2 and test function P1")
{
    GetModel().UnknownP2TestP1();
}


namespace // anonymous
{


    std::string LuaFile::GetPath()
    {
        decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

        return
            environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Operators/"
                                                       "VariationalInstances/Pk2/"
                                                       "demo_input_parameter_3D.lua");
    }


} // namespace anonymous

