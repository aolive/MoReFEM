/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include <cstdlib>

#include "ThirdParty/Wrappers/Tclap/StringPair.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "ThirdParty/IncludeWithoutWarning/Seldon/Seldon.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/ThirdParty/Mpi/Gather/InputParameterList.hpp"
#include "Test/Tools/CatchMainTest.hpp"
#include "Test/Tools/Fixture.hpp"

using namespace MoReFEM;


namespace // anonymous
{


    struct LuaFile
    {


        static std::string GetPath();


    };


    struct MockModel
    {

        using morefem_data_type = MoReFEMData<TestNS::InputParameterListNS::InputParameterList>;

    };


    using fixture_type =
        TestNS::Fixture
        <
            MockModel,
            LuaFile
        >;


} // namespace anonymous




TEST_CASE_METHOD(fixture_type, "Check AllGather() behave as expected.")
{
    decltype(auto) morefem_data = fixture_type::GetMoReFEMData();

    const auto& mpi = morefem_data.GetMpi();

    const auto rank = mpi.GetRank<int>();

    REQUIRE(mpi.Nprocessor<int>() == 3); // "This test has been written for three processors specifically!

    namespace ipl = Utilities::InputParameterListNS;

    std::vector<int> imbalanced_sent_data;
    std::vector<int> balanced_sent_data;

    switch(rank)
    {
        case 0:
            imbalanced_sent_data = { 1, 2, 3 };
            break;
        case 1:
            imbalanced_sent_data = { 11, 12, 13 };
            break;
        case 2:
            imbalanced_sent_data = { 21, 22, 23, 24 };
            break;
    }

    if (rank == 2)
        balanced_sent_data = { 21, 22, 23 };
    else
        balanced_sent_data = imbalanced_sent_data;

    std::vector<int> gathered_data;

    SECTION("Valid Gather(), should work")
    {
        CHECK_NOTHROW(mpi.Gather(balanced_sent_data, gathered_data));

        if (rank == 0)
        {
            std::vector<int> expected { 1, 2, 3, 11, 12, 13, 21, 22, 23 };
            CHECK(gathered_data == expected);
        }
        else
            CHECK(gathered_data.empty());
    }

    SECTION("Valid AllGather(), should work")
    {
        CHECK_NOTHROW(mpi.AllGather(balanced_sent_data, gathered_data));

        std::vector<int> expected { 1, 2, 3, 11, 12, 13, 21, 22, 23 };
        CHECK(gathered_data == expected);
    }


    // This test should be added when Catch2 implements it; there's pressure on devs to provide it.
    //    SECTION("One of the vector is not the right size; should fail")
    //    {
    //        CHECK_ABORT(mpi.Gather(imbalanced_sent_data, gathered_data));
    //    }


    SECTION("Valid Gatherv(), should work")
    {
        CHECK_NOTHROW(mpi.Gatherv(imbalanced_sent_data, gathered_data));

        if (rank == 0)
        {
            std::vector<int> expected { 1, 2, 3, 11, 12, 13, 21, 22, 23, 24 };
            CHECK(gathered_data == expected);
        }
        else
            CHECK(gathered_data.empty());
    }


    SECTION("Valid AllGatherv(), should work")
    {
        CHECK_NOTHROW(mpi.AllGatherv(imbalanced_sent_data, gathered_data));

        std::vector<int> expected { 1, 2, 3, 11, 12, 13, 21, 22, 23, 24 };
        CHECK(gathered_data == expected);
    }

}


namespace // anonymous
{


    std::string LuaFile::GetPath()
    {
        decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

        return
            environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/ThirdParty/Mpi/Gather/"
                                         "demo_input_data.lua");
    }

} // namespace anonymous

