add_executable(MoReFEMTestMpiGather
               ${CMAKE_CURRENT_LIST_DIR}/InputParameterList.hpp
               ${CMAKE_CURRENT_LIST_DIR}/main.cpp
              )
          
target_link_libraries(MoReFEMTestMpiGather
                     ${ALL_LOAD_BEGIN_FLAG}
                     ${MOREFEM_MODEL}
                     ${ALL_LOAD_END_FLAG})
    
add_test(MpiGather 
         ${OPEN_MPI_INCL_DIR}/../bin/mpirun
         --oversubscribe
         -np 3
         MoReFEMTestMpiGather
         --root_dir ${MOREFEM_ROOT}
         --test_output_dir ${MOREFEM_TEST_OUTPUT_DIR})
                      
morefem_install(MoReFEMTestMpiGather)                        
