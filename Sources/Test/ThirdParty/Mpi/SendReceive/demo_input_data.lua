Result = {

	-- Directory in which all the results will be written. This path may use the environment variable
	-- MOREFEM_TEST_OUTPUT_DIR, which is either provided in user's environment or automatically set to 
	-- '/Volumes/Data/${USER}/MoReFEM/Results' in MoReFEM initialization step. 
	-- Expected format: "VALUE"
    output_directory = '${MOREFEM_TEST_OUTPUT_DIR}/Mpi/SendReceive',

	-- Enables to skip some printing in the console. Can be used to WriteSolution every n time.
	-- Expected format: VALUE
	display_value = 1

} -- Result

