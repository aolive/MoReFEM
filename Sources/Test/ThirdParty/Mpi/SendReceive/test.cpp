/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 Sep 2017 15:59:08 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "ThirdParty/IncludeWithoutWarning/Seldon/Seldon.hpp"
#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/ThirdParty/Mpi/SendReceive/InputParameterList.hpp"
#include "Test/Tools/CatchMainTest.hpp"
#include "Test/Tools/Fixture.hpp"

using namespace MoReFEM;


namespace // anonymous
{


    struct LuaFile
    {


        static std::string GetPath();


    };


    struct MockModel
    {

        using morefem_data_type = MoReFEMData<TestNS::MpiSendReceiveNS::InputParameterList>;

    };


    using fixture_type =
        TestNS::Fixture
        <
            MockModel,
            LuaFile
        >;


} // namespace anonymous


TEST_CASE_METHOD(fixture_type, "Check Send() and Receive() behave as expected with single values.")
{
    decltype(auto) morefem_data = fixture_type::GetMoReFEMData();

    const auto& mpi = morefem_data.GetMpi();

    REQUIRE(mpi.Nprocessor<int>() == 4);

    const auto rank = mpi.GetRank<int>();
    const auto Nproc = mpi.Nprocessor<int>();

    namespace ipl = Utilities::InputParameterListNS;

    double token = std::numeric_limits<double>::lowest();

    switch(rank)
    {
        case 1:
            token = mpi.Receive<double>(static_cast<unsigned int>(rank - 1));
            CHECK(token == Approx(1.245));
            break;
        case 2:
            token = mpi.Receive<double>(static_cast<unsigned int>(rank - 1));
            CHECK(token == Approx(11.245));
            break;
        case 3:
            token = mpi.Receive<double>(static_cast<unsigned int>(rank - 1));
            CHECK(token == Approx(31.245));
            break;
        case 0:
            // Set the token's value if you are process 0
            token = 1.245;
            break;

    }

    token += static_cast<double>(10 * rank);

    mpi.Send(static_cast<unsigned int>((rank + 1) % Nproc),
             token);

    // Now process 0 can receive from the last process.
    if (rank == 0)
    {
        token = mpi.Receive<double>(static_cast<unsigned int>(Nproc - 1));

        CHECK(token == Approx(61.245));
    }
}


TEST_CASE_METHOD(fixture_type, "Check Send() and Receive() behave as expected with arrays.")
{
    decltype(auto) morefem_data = fixture_type::GetMoReFEMData();

    const auto& mpi = morefem_data.GetMpi();

    REQUIRE(mpi.Nprocessor<int>() == 4);

    const auto rank = mpi.GetRank<int>();
    const auto Nproc = mpi.Nprocessor<int>();

    namespace ipl = Utilities::InputParameterListNS;

    std::vector<int> token { NumericNS::UninitializedIndex<int>(), NumericNS::UninitializedIndex<int>() };

    constexpr auto MAX_LENGTH = 6u;

    switch(rank)
    {
        case 1:
            token = mpi.Receive<int>(static_cast<unsigned int>(rank - 1), MAX_LENGTH);
            CHECK(token == std::vector<int>{ -1, -2, 0 });
            break;
        case 2:
            token = mpi.Receive<int>(static_cast<unsigned int>(rank - 1), MAX_LENGTH);
            CHECK(token == std::vector<int>{ -1, -2, 0, 10 });
            break;
        case 3:
            token = mpi.Receive<int>(static_cast<unsigned int>(rank - 1), MAX_LENGTH);
            CHECK(token == std::vector<int>{ -1, -2, 0, 10, 20 });
            break;
        case 0:
            // Set the token's value if you are process 0
            token = { -1, -2 };
            break;

    }

    token.push_back(rank * 10);

    mpi.SendContainer(static_cast<unsigned int>((rank + 1) % Nproc),
                      token);

    // Now process 0 can receive from the last process.
    if (rank == 0)
    {
        token = mpi.Receive<int>(static_cast<unsigned int>(Nproc - 1), MAX_LENGTH);

        CHECK(token == std::vector<int>{ -1, -2, 0, 10, 20, 30 });
    }
}

        
namespace // anonymous
{


    std::string LuaFile::GetPath()
    {
        decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

        return
            environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/ThirdParty/Mpi/SendReceive/"
                                         "demo_input_data.lua");
    }


} // namespace anonymous
