#define CATCH_CONFIG_MAIN
#include "ThirdParty/Source/Catch/catch.hpp"

# include "ThirdParty/Wrappers/Tclap/StringPair.hpp"

using namespace MoReFEM;


TEST_CASE("String pair is correctly read.")
{
    TCLAP::CmdLine cmd("Command description message");

    cmd.setExceptionHandling(false);

    TCLAP::MultiArg<Wrappers::Tclap::StringPair> env_arg("e",
                                                         "env",
                                                         "environment_variable",
                                                         false,
                                                         "string=string",
                                                         cmd);

    std::vector<std::string> short_args
    {
        "program_name",
        "-e FOO=BAR",
        "-e BAZ=42 " // space is intentional!
    };

    std::vector<std::string> long_args
    {
        "program_name",
        "--env FOO=BAR",
        "--env BAZ=42 " // space is intentional!
    };


    SECTION("With shorthand notation for arguments")
    {
        cmd.parse(short_args);

        REQUIRE(env_arg.getValue().size() == 2ul);

        CHECK(env_arg.getValue()[0].GetValue().first == "FOO");
        CHECK(env_arg.getValue()[0].GetValue().second == "BAR");

        CHECK(env_arg.getValue()[1].GetValue().first == "BAZ");
        CHECK(env_arg.getValue()[1].GetValue().second == "42");
    }


    SECTION("With longer notation for arguments")
    {
        cmd.parse(long_args);

        REQUIRE(env_arg.getValue().size() == 2ul);

        CHECK(env_arg.getValue()[0].GetValue().first == "FOO");
        CHECK(env_arg.getValue()[0].GetValue().second == "BAR");

        CHECK(env_arg.getValue()[1].GetValue().first == "BAZ");
        CHECK(env_arg.getValue()[1].GetValue().second == "42");
    }

} // TEST_CASE


TEST_CASE("Invalid command line arguments are properly rejected")
{
    TCLAP::CmdLine cmd("Command description message");

    cmd.setExceptionHandling(false);

    TCLAP::MultiArg<Wrappers::Tclap::StringPair> env_arg("e",
                                                         "env",
                                                         "environment_variable",
                                                         false,
                                                         "string=string",
                                                         cmd);


    SECTION("Two equals case")
    {
        std::vector<std::string> invalid_args
        {
            "program_name",
            "--env FOO=BAR=BAZ",
        };

        CHECK_THROWS(cmd.parse(invalid_args));
    }

    SECTION("MIssing argument")
    {
       std::vector<std::string> invalid_args
        {
            "program_name",
            "-e",
        };

        CHECK_THROWS(cmd.parse(invalid_args));
    }

}


TEST_CASE("Mandatory argument is properly read")
{
    TCLAP::CmdLine cmd("Command description message");

    cmd.setExceptionHandling(false);

    TCLAP::ValueArg<std::string> input_file_arg("i",
                                                "input_file",
                                                "Input file to interpret",
                                                true,
                                                "",
                                                "string",
                                                cmd);

    std::vector<std::string> invalid_args
    {
        "program_name"
    };

    std::vector<std::string> valid_args
    {
        "program_name",
        "-i path"
    };

    std::vector<std::string> another_valid_args
    {
        "program_name",
        "--input_file path"
    };

    SECTION("Valid case: argument provided.")
    {
        REQUIRE_NOTHROW(cmd.parse(valid_args));
        CHECK(input_file_arg.getValue() == "path");
    }

    SECTION("Valid case: argument provided with long form.")
    {
        REQUIRE_NOTHROW(cmd.parse(another_valid_args));
        CHECK(input_file_arg.getValue() == "path");
    }

    SECTION("Invalid case: argument forgotten.")
    {
        CHECK_THROWS(cmd.parse(invalid_args));
    }


} // TEST_CASE




