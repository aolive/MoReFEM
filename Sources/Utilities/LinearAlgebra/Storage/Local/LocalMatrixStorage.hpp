///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 10 Mar 2015 14:57:33 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_LOCAL_x_LOCAL_MATRIX_STORAGE_HPP_
# define MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_LOCAL_x_LOCAL_MATRIX_STORAGE_HPP_

# include <array>

# include "Utilities/MatrixOrVector.hpp"


namespace MoReFEM
{


    namespace Crtp
    {



        /*!
         * \brief CRTP to give access to \a NlocalMatricesT local matrices.
         *
         * \tparam DerivedT Name of the base class for which the CRTP is deployed.
         * \tparam NlocalMatricesT Number of local matrices to add.
         *
         * It is advised to declare in \a DerivedT an enum class to tag the local matrices, e.g.:
         * \code
         * enum class LocalMatrixIndex : std::size_t
         * {
         *      tangent_matrix = 0,
         *      linear_part,
         *      dPhi_mult_gradient_based_block,
         *      ...
         * };
         * \endcode
         */
        template<class DerivedT, std::size_t NlocalMatricesT>
        class LocalMatrixStorage
        {
        public:

            //! Return the number of local matrices.
            static constexpr std::size_t N();

        public:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * Does nothing: the computation of the dimension in the constructor of DerivedT proved to be unreadable.
             * So InitLocalMatrixStorage() must absolutely be called to set appropriately the dimensions.
             */
            explicit LocalMatrixStorage() = default;


        protected:

            //! Destructor.
            ~LocalMatrixStorage() = default;

            //! Copy constructor.
            LocalMatrixStorage(const LocalMatrixStorage&) = default;

            //! Move constructor.
            LocalMatrixStorage(LocalMatrixStorage&&) = default;

            //! Copy affectation.
            LocalMatrixStorage& operator=(const LocalMatrixStorage&) = default;

            //! Move affectation.
            LocalMatrixStorage& operator=(LocalMatrixStorage&) = default;


            ///@}


        public:


            /*!
             * \brief This method must be called in the constructor of DerivedT, once the dimensions have been
             * computed.
             *
             * For safety reasons, matrix is filled with 0 to avoid undefined behaviour if no value is actually given.
             *
             * \param[in] matrices_dimension For each \a LocalMatrix stored this way, the pair is respectively the
             * number of rows and columns to consider.
             */
            void InitLocalMatrixStorage(const std::array<std::pair<unsigned int, unsigned int>, NlocalMatricesT>& matrices_dimension);

            /*!
             * \brief Access to the \a IndexT -th local matrix.
             *
             * \return Access to the \a IndexT -th local matrix.
             *
             * The idea is to use it as such:
             * \code
             * auto& local_matrix = GetLocalMatrix<0>();
             * ... (use local_matrix variable in the following)
             * \endcode
             *
             * \internal <b><tt>[internal]</tt></b> This method is const because we might want to use local matrices in const methods of
             * DerivedT; as local matrices are bound to be used within a single method they are declared as mutable.
             */
            template<std::size_t IndexT>
            LocalMatrix& GetLocalMatrix() const;


        private:

            //! Local matrices stored.
            mutable std::array<LocalMatrix, NlocalMatricesT> matrix_list_;

        };


    } // namespace Crtp


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hxx"


#endif // MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_LOCAL_x_LOCAL_MATRIX_STORAGE_HPP_
