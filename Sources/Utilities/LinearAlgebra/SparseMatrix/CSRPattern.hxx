///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 7 Oct 2013 09:17:07 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_SPARSE_MATRIX_x_C_S_R_PATTERN_HXX_
# define MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_SPARSE_MATRIX_x_C_S_R_PATTERN_HXX_


namespace MoReFEM
{


    namespace Utilities
    {


        template<typename T>
        CSRPattern<T>::CSRPattern(std::vector<T>&& i_CSR, std::vector<T>&& j_CSR)
        : iCSR_(i_CSR),
        jCSR_(j_CSR)
        {
            static_assert(std::is_integral<T>::value == true, "Pattern is expected to handle integral values!");
            CheckConsistency();
        }

        template<typename T>
        const std::vector<T>& CSRPattern<T>::iCSR() const
        {
            return iCSR_;
        }


        template<typename T>
        const std::vector<T>& CSRPattern<T>::jCSR() const
        {
            return jCSR_;
        }


        template<typename T>
        std::vector<T>& CSRPattern<T>::NonCstiCSR()
        {
            return iCSR_;
        }


        template<typename T>
        std::vector<T>& CSRPattern<T>::NonCstjCSR()
        {
            return jCSR_;
        }


        template<typename T>
        unsigned int CSRPattern<T>::NiCSR() const
        {
            return static_cast<unsigned int>(iCSR_.size());
        }


        template<typename T>
        unsigned int CSRPattern<T>::Nrow() const
        {
            assert(!iCSR_.empty());
            return iCSR_.size() - 1;
        }


        template<typename T>
        unsigned int CSRPattern<T>::NjCSR() const
        {
            return static_cast<unsigned int>(jCSR_.size());
        }


        template<typename T>
        T CSRPattern<T>::iCSR(unsigned int i) const
        {
            assert(i < iCSR_.size());
            return iCSR_[i];
        }


        template<typename T>
        T CSRPattern<T>::jCSR(unsigned int j) const
        {
            assert(j < jCSR_.size());
            return jCSR_[j];

        }


        template<typename T>
        void CSRPattern<T>::CheckConsistency() const
        {
            assert(!iCSR_.empty());
            assert(iCSR_.front() == 0);

            if (static_cast<unsigned int>(iCSR_.back()) != jCSR_.size())
                throw MoReFEM::Exception("Invalid CSR format: last iCSR element read is not the size of jCSR.",
                                            __FILE__, __LINE__);
        }


        template<typename T>
        template<typename IntT>
        std::vector<IntT> CSRPattern<T>::Nnon_zero_terms_per_row() const
        {
            const unsigned int csr_size = iCSR_.size();
            const unsigned int Nrow = csr_size - 1;
            std::vector<IntT> ret(Nrow);

            for (unsigned int i = 0; i < Nrow; ++i)
                ret[i] = iCSR_[i + 1] - iCSR_[i];

            return ret;
        }


        template<typename T>
        template<typename IntT>
        void CSRPattern<T>::Nnon_zero_terms_per_row(std::function<bool(unsigned int)> is_on_local_proc,
                                                    std::vector<IntT>& Ndiagonal_non_zero,
                                                    std::vector<IntT>& Noff_diagonal_non_zero) const
        {
            const unsigned int Nrow = this->Nrow();
            Ndiagonal_non_zero.resize(Nrow);
            Noff_diagonal_non_zero.resize(Nrow);

            for (unsigned int i = 0; i < Nrow; ++i)
            {
                const unsigned int Nnon_zero_on_row = static_cast<unsigned int>(iCSR_[i + 1] - iCSR_[i]);

                for (unsigned int j = 0; j < Nnon_zero_on_row; ++j)
                {
                    const unsigned int jCSR_index = static_cast<unsigned int>(iCSR_[i]) + j;

                    if (is_on_local_proc(static_cast<unsigned int>(jCSR_[jCSR_index])))
                        ++Ndiagonal_non_zero[i];
                    else
                        ++Noff_diagonal_non_zero[i];
                }
            }

        }




    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_SPARSE_MATRIX_x_C_S_R_PATTERN_HXX_
