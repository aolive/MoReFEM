///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 7 Oct 2013 09:17:07 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_SPARSE_MATRIX_x_C_S_R_PATTERN_HPP_
# define MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_SPARSE_MATRIX_x_C_S_R_PATTERN_HPP_

# include <cassert>
# include <vector>
# include <type_traits>
# include <functional>

# include "Utilities/Exceptions/Exception.hpp"


namespace MoReFEM
{


    namespace Utilities
    {



        /*!
         * \brief Objects that store informations about the CSR pattern of a sparse matrix.
         *
         * To put in a nutshell, iCSR tells for each line how much non zero values there are, and jCSR indicates
         * their position.
         *
         * Wikipedia explanation is very enlightning:
         * https://en.wikipedia.org/wiki/Sparse_matrix#Compressed_sparse_row_.28CSR.2C_CRS_or_Yale_format.29
         */
        template<typename T>
        class CSRPattern final
        {
        public:

            /// \name Constructors and destructor.
            ///@{

            //! Empty constructor.
            CSRPattern() = default;

            /*!
             * \brief Constructor with CSR pattern already calculated.
             *
             */
            explicit CSRPattern(std::vector<T>&& iCSR, std::vector<T>&& jCSR);

            //! Recopy constructor.
            CSRPattern(const CSRPattern& ) = default;

            //! Move constructor.
            CSRPattern(CSRPattern&& ) = default;

            //! Copy assignation.
            CSRPattern& operator=(const CSRPattern& ) = default;

            //! Move assignation.
            CSRPattern& operator=(CSRPattern&& ) = default;

            //! Destructor.
            ~CSRPattern() = default;
            ///@}


            //! Const accessor to iCSR.
            const std::vector<T>& iCSR() const;

            //! Const accessor to iCSR.
            const std::vector<T>& jCSR() const;

            /*!
             * \brief Non-const accessor to iCSR.
             *
             * \return Reference to internal storage of iCSR.
             *
             * \internal <b><tt>[internal]</tt></b> I don't like providing such a direct accessor, but Parmetis interface for instance
             * does not specify constness of arguments... Hence the name that is not iCSR(): user can't
             * provide direct accessor without his knowledge.
             */
            std::vector<T>& NonCstiCSR();

            /*!
             * \brief Non-const accessor to jCSR.
             *
             * \return Reference to internal storage of jCSR.
             *
             * \internal <b><tt>[internal]</tt></b> I don't like providing such a direct accessor, but Parmetis interface for instance
             * does not specify constness of arguments... Hence the name that is not ijSR(): user can't
             * provide direct accessor without his knowledge.

             */
            std::vector<T>& NonCstjCSR();


            //! Number of elements in iCSR.
            unsigned int NiCSR() const;

            //! Number of rows (NiCSR - 1 by construct).
            unsigned int Nrow() const;

            //! Number of elements in jCSR.
            unsigned int NjCSR() const;

            //! Returns the value of thre \a i -th element in iCSR (with C numbering beginning at 0).
            T iCSR(unsigned int i) const;

            //! Returns the value of thre \a j -th element in jCSR (with C numbering beginning at 0).
            T jCSR(unsigned int j) const;

            //! Returns the total numbers of non-zero terms per row.
            template<typename IntT>
            std::vector<IntT> Nnon_zero_terms_per_row() const;


            /*!
             * \brief Returns the numbers of non-zero terms per row, separating those that are on the
             * same processor as the row index and those that are not.
             *
             * We will use Petsc denomination for it:
             * Diagonal terms for the ones on the same processor as the row index (diagonal because the submatrix is
             * on the diagonal).
             * Off-diagonal terms if column index is not on the same processor.
             *
             * \param[in] is_on_local_proc Function that tells whether a given index is on the local processor or not.
             * \param[in] Ndiagonal_non_zero Number of non-zero diagonal terms.
             * \param[in] Noff_diagonal_non_zero Number of non-zero off-diagonal terms.
             */
            template<typename IntT>
            void Nnon_zero_terms_per_row(std::function<bool(unsigned int)> is_on_local_proc,
                                         std::vector<IntT>& Ndiagonal_non_zero,
                                         std::vector<IntT>& Noff_diagonal_non_zero) const;


        private:

            //! Check the consistency of the given iCSR_ and jCSR_.
            void CheckConsistency() const;


        private:

            //! Storage of iCSR vector.
            std::vector<T> iCSR_;

            //! Storage of jCSR vector.
            std::vector<T> jCSR_;

        };


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/LinearAlgebra/SparseMatrix/CSRPattern.hxx"


#endif // MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_SPARSE_MATRIX_x_C_S_R_PATTERN_HPP_
