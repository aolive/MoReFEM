//! \file
//
//
//  LuaOptionFile.hpp
//
//  Created by sebastien on 01/03/2018.
//Copyright © 2018 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_LUA_OPTION_FILE_x_LUA_OPTION_FILE_HPP_
# define MOREFEM_x_UTILITIES_x_LUA_OPTION_FILE_x_LUA_OPTION_FILE_HPP_

# include <cassert>
# include <memory>
# include <vector>
# include <map>
# include <sstream>
# include <iostream>
# include <tuple>

# include "ThirdParty/IncludeWithoutWarning/Lua/Lua.hpp"

# include "Utilities/String/Traits.hpp"
# include "Utilities/Exceptions/Exception.hpp"
# include "Utilities/Miscellaneous.hpp"
# include "Utilities/Type/PrintTypeName.hpp"


namespace MoReFEM
{


    /*!
     * \brief A class to load input parameter data stored in a Lua file.
     *
     * This class is very inspired from https://gitlab.com/libops, which was used for a long time in MoReFEM. The
     * reason I no longer use it directly is that I needed extensions (e.g. handling of map containers) I wasn't
     * comfortable in adding directly in the sometimes weird libops logic. Current class is not a mere extension of
     * libops:
     * - Some libops features aren't present here, such as the reading of individual values of an array. The reason is
     * that LuaOptionFile is use donly by higher-level InputParameterList which doesn't need this feature.
     * - On the other hand, (ordered) associative containers are now supported.
     * - Using a Lua function will be more efficient: libops reallocates a std::vector at each call, whereas I'm using
     * instead variadic parameters. So I do not have a limit of the number of parameters in a Lua function (but they
     * still should all be of the same type).
     *
     * It must be noted that I'm not familiar with Lua and Lua code has been mostly been left untouched.
     *
     * \attention This class requires Lua 5.3, whereas libops expected 5.1. The reason is that I wanted lua_isinteger
     * not yet in 5.1, whereas 5.2+ made compilation errors with part of libops code I didn't retain.
     */
    class LuaOptionFile
    {

    public:

        //! \copydoc doxygen_hide_alias_self
        using self = LuaOptionFile;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;


    public:

        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] filename Path of the Lua file to load.
         * \copydoc doxygen_hide_invoking_file_and_line
         */
        explicit LuaOptionFile(const std::string& filename, const char* invoking_file, int invoking_line);

        //! Destructor.
        ~LuaOptionFile();

        //! Copy constructor.
        LuaOptionFile(const LuaOptionFile&) = delete;

        //! Move constructor.
        LuaOptionFile(LuaOptionFile&&) = delete;

        //! Copy affectation.
        LuaOptionFile& operator=(const LuaOptionFile&) = delete;

        //! Move affectation.
        LuaOptionFile& operator=(LuaOptionFile&&) = delete;

        ///@}

    public:

        /*!
         * \class doxygen_hide_lua_option_file_constraint
         *
         * \param[in] constraint constraint that the entry value must satisfy. The value is named 'v' by convention; the
         * constraint might be:
         * - A direct comparison, e.g. 'v > 10', 'v <= 3'. or  'v == "foo"'
         * - Checking it is among a list of choices, e.b. 'value_in(v, { "constant", "piecewise_per_domain", "function"} )'
         * - Empty! (i.e. "")
         *
         * An exception is thrown is the constraint is not fulfilled.
         */


        /*!
         * \class doxygen_hide_lua_option_file_read
         *
         * \param[in] entry_name name of the entry.
         * \copydoc doxygen_hide_lua_option_file_constraint
         * \copydoc doxygen_hide_invoking_file_and_line
         */

        /*!
         * \brief Retrieves a value from the configuration file.
         *
         * \tparam T C++ type of the value read in the Lua file. Possible values are:
         * - Integral types.
         * - Floating point types.
         * - bool
         * - std::string
         * - std::vector<> (with the template parameter chosen among the ones mentioned above)
         * - std::map<> (with the template parameters for key and values chosen among the ones mentioned above)
         *
         * \copydoc doxygen_hide_lua_option_file_read
         *
         * \attention Please notice Read() should not be used for a Lua function, see \a Apply instead.
         *
         * \return The value of the entry.
         */
        template<class T>
        T Read(const std::string& entry_name, const std::string& constraint,
              const char* invoking_file, int invoking_line);


        /*!
         * \brief Fetch the Lua function which entry is \a entry_name and use \a in arguments to compute a value.
         *
         * \tparam T C++ type of the value read in the Lua file. Possible values are:
         * - Integral types.
         * - Floating point types.
         * - bool
         * - std::string
         *
         * \param[in] entry_name name of the entry.
         * \copydoc doxygen_hide_invoking_file_and_line
         * \param[in] in Variadic arguments given to the Lua function. A restriction is that they are assumed to be
         * of the type T.
         *
         * \attention Please notice Read() should not be used for a Lua function, see \a Apply instead.
         *
         * \return The value returned by the Lua function.
         */
        template<typename T, typename ...Args>
        T Apply(const std::string& entry_name,
                const char* invoking_file, int invoking_line,
                Args&&... in);

    public:


        /*!
         * \brief Get the list of all entries in the Lua file.
         *
         * Entries are in the format "section1.subsection.subsubsection.entry".
         *
         * \return List of all entries found.
         */
        const std::vector<std::string>& GetEntryKeyList() const noexcept;

    private:

        /*!
         * \brief Open a Lua file and load it.
         *
         * The file is examined in a first step to determine all the entry keys.
         * Then luaL_dofile is called on it.
         *
         * \param[in] filename Path of the Lua file to load.
         * \copydoc doxygen_hide_invoking_file_and_line
         */
        void Open(const std::string& filename, const char* invoking_file, int invoking_line);


    private:

        //! Get the Lua state.
        lua_State* GetNonCstLuaState() noexcept;

        /*!
         * \brief Put \a entry_name on top of Lua stack.
         *
         * If \a entry_name is a simple variable, it calls 'lua_getglobal' once. But if
         * \a entry_name is encapsulated in a table, this method iterates until it finds
         * the variable.
         * \param[in] entry_name Name of the entry to be put on top of the stack.
         */
        void PutOnStack(const std::string& entry_name);

        /*!
         * \brief Push on stack the value related to the last entry put on stack.
         *
         * This method is only used along with \a Apply, to give to Lua the values from C++ types.
         *
         * \param[in] value Value to push on Lua stack.
         *
         * \tparam T C++ type of the value. It might be:
         * - Integral types.
         * - Floating point types.
         * - bool
         * - std::string
         * - A std::tuple with one of the above type inside (as a matter of fact it is always a tuple that is called
         * in the first place; this tuple will recursively call the version on single types).
         */
        template<typename T>
        void PushOnStack(T value);

        
        /*!
         * \brief Convert the \a index_in_lua_stack -th element into a C++ type.
         *
         * \tparam T C++ type into which Lua data will be converted. Possible choices are:
         * - Integral types.
         * - Floating point types.
         * - bool
         * - std::string
         *
         * \tparam StringT The universal reference trick; read it as either const std::string& or std::string&&.
         *
         * \param[in] index_in_lua_stack Index to indicate which element in Lua stack must be converted. Usually -1 or
         * -2.
         * \param[in] entry_name Name of the entry for which the converion occurs. Only used to display meaningful
         * error message,
         * \copydoc doxygen_hide_invoking_file_and_line
         *
         * \return The value in a C++ type.
         */
        template<class T, class StringT>
        T Convert(int index_in_lua_stack, StringT&& entry_name,
                  const char* invoking_file, int invoking_line);


        //! Clears the Lua stack.
        void ClearStack();

        /*!
         * \brief Checks whether an entry satisfies a constraint.
         *
         * \tparam StringT The universal reference trick; read it as either const std::string& or std::string&&.
         *
         * \param[in] entry_name The entry for which the constraint is to be checked.
         * \copydoc doxygen_hide_lua_option_file_constraint
         * \copydoc doxygen_hide_invoking_file_and_line
         *
         * \return True if the constraint is satisfied, false otherwise.
         */
        template<class StringT>
        void CheckConstraint(StringT&& entry_name, const std::string& constraint,
                             const char* invoking_file, int invoking_line);


    private:

        /*!
         * \brief Sub-function of Read when T is a std::vector.
         *
         * \copydoc doxygen_hide_lua_option_file_read
         * \param[out] vector The resulting std::vector with all its value properly filled from Lua data.
         */
        template<class T>
        void ReadVector(const std::string& entry_name, const std::string& constraint,
                        const char* invoking_file, int invoking_line, T& vector);


        /*!
         * \brief Sub-function of Read when T is a std::map.
         *
         * \copydoc doxygen_hide_lua_option_file_read
         * \param[out] map The resulting std::map with all its value properly filled from Lua data.
         */
        template<class T>
        void ReadMap(const std::string& entry_name, const std::string& constraint,
                        const char* invoking_file, int invoking_line, T& map);


        /*!
         * \brief Return the path of the Lua file which was interpreted.
         *
         * \return Path to the Lua file.
         */
        const std::string& GetFilename() const noexcept;

        /*!
         * \brief Set the list of all entry keys.
         *
         * This method should only be called once.
         *
         * \param[in] entry_key_list List of all the entries in the Lua file to be set.
         */
        void SetEntryKeyList(std::vector<std::string>&& entry_key_list);


    private:


        //! Lua state.
        lua_State* state_ = nullptr;

        /*!
         * \brief List of all entry keys found in the Lua file.
         *
         * \internal This list was provided by manual parsing, not by Lua directly.
         */
        std::vector<std::string> entry_key_list_;

        //! Filename read.
        std::string filename_ = "";

    };


} // namespace MoReFEM


# include "Utilities/LuaOptionFile/LuaOptionFile.hxx"


#endif // MOREFEM_x_UTILITIES_x_LUA_OPTION_FILE_x_LUA_OPTION_FILE_HPP_
