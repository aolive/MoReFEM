//! \file 
//
//
//  LuaOptionFile.cpp
//
//
//  Created by sebastien on 01/03/2018.
// Copyright © 2018 Inria. All rights reserved.
//

#include <fstream>
#include <sstream>
#include <array>

#include "Utilities/Containers/Print.hpp" // FPOR DEV PURPORSES
#include "Utilities/Containers/Vector.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"
#include "Utilities/LuaOptionFile/LuaOptionFile.hpp"


namespace MoReFEM
{


    namespace // anonymous
    {


        //! Defines 'value_in' for the user. It checks whether an element is in a table.
        void AddValueIn(lua_State* lua_state);

        
        /*!
         * \brief Just concatenate the section (and eventually subsections and so forth...) and the parameter name.
         *
         * e.g. if hierarchy is { "Solid", "YoungModulus"} and param_name is "LuaFunction" this function will yield
         * "Solid.YoungModulus.LuaFunction", aka the keyword identifier in the input parameter file.
         */
        std::string GenerateSectionNameFromHierarchy(const std::vector<std::string>& hierarchy,
                                                     const std::string& param_name);


        /*!
         * \brief Parse the file to extract all the keys in it.
         *
         * \param[in] path Path to the input Lua file.
         *
         * \internal Lua is not involved at all here; it is done in pure C++ by parsing manually the file.
         *
         * \return All the keys found in the input file.
         */
        std::vector<std::string> ExtractKeysFromFile(const std::string& path);


        /*!
         * \brief Sift through the sections/subsections and so forth to put them on Lua stack.
         *
         *
         * \param[in] full_name The full name with all the layers (e.g. VolumicMass.nature)
         * \param[in] entry_name Name of an entry that is accessible from the entry
         currently on top of the stack (e.g. nature).
         */
        void WalkDown(const std::string& full_name,
                      const std::string& entry_name,
                      lua_State* state);


    } // namespace anonymous


    LuaOptionFile
    ::LuaOptionFile(const std::string& filename, const char* invoking_file, int invoking_line)
    : state_(luaL_newstate()),
    filename_(filename)
    {
        decltype(auto) state = GetNonCstLuaState();
        luaL_openlibs(state);

        AddValueIn(state);

        /*
         local f, t = function()return function()end end, {nil,
         [false]  = 'Lua 5.1',
         [true]   = 'Lua 5.2',
         [1/'-0'] = 'Lua 5.3',
         [1]      = 'LuaJIT' }
         local version = t[1] or t[1/0] or t[f()==f()]
*/

        // Clean-up if an exception is thrown.
        try
        {
            Open(filename, invoking_file, invoking_line);
        }
        catch(...)
        {
            lua_close(state_);
            state_ = nullptr;
            throw;
        }
    }


    LuaOptionFile::~LuaOptionFile()
    {
        lua_close(state_);
        state_ = nullptr;
    }


    void LuaOptionFile::Open(const std::string& filename, const char* invoking_file, int invoking_line)
    {
        SetEntryKeyList(ExtractKeysFromFile(filename));

        if (luaL_dofile(state_, filename.c_str()))
        {
            std::ostringstream oconv;
            oconv << "Open(string, bool) " <<  lua_tostring(state_, -1);
            throw Exception(oconv.str(), invoking_file, invoking_line);
        }

    }


    void LuaOptionFile::PutOnStack(const std::string& name)
    {
        assert(!name.empty()); // \todo Note: was handled in Ops but very likely not relevant for us.

        const auto end = name.find(".");
        assert(end != 0ul);

        // No section involved: the parameter was directly defined in root level.
        if (end == std::string::npos)
        {
            lua_getglobal(state_, name.c_str());
        }
        else
        {
            lua_getglobal(state_, name.substr(0, end).c_str());

            assert(end < name.size());
            assert(name[end] == '.');
            WalkDown(name, name.substr(end + 1).c_str(), state_);
        }
    }

    
    void LuaOptionFile::ClearStack()
    {
        lua_pop(state_, lua_gettop(state_));
    }


    namespace // anonymous
    {


        void AddValueIn(lua_State* lua_state)
        {
            std::string code = "function value_in(v, table)\
            for _, value in ipairs(table) do        \
            if v == value then                  \
            return true                     \
            end                                 \
            end                                     \
            return false                            \
            end";

            if (luaL_dostring(lua_state, code.c_str()))
                throw Exception(lua_tostring(lua_state, -1), __FILE__, __LINE__);
        }


        std::vector<std::string> ExtractKeysFromFile(const std::string& path)
        {
            std::ifstream stream;
            FilesystemNS::File::Read(stream, path, __FILE__, __LINE__);

            auto npos = std::string::npos;

            std::string line;
            std::string lhs;

            std::vector<std::string> section_hierarchy;

            std::vector<std::string> ret;

            // All strings that might be tagged as variable or section and begins with one
            // of these keys followed by ' ' or '(' must be rejected.
            std::array<std::string, 3> rejected_keys
            {
                {
                    "if",
                    "else",
                    "elseif"
                }
            };

            unsigned int line_count = 0;


            while (std::getline(stream, line))
            {
                ++line_count;

                // Delete the comment part of the line.
                auto pos = line.find("--");

                if (pos != npos)
                    line.erase(pos, npos);

                // If there are more closing braces than opening ones on the line, it means the current
                // section has been closed.
                auto Nopening_brace = (std::count(line.cbegin(), line.cend(), '{'));
                auto Nclosing_brace = (std::count(line.cbegin(), line.cend(), '}'));



                if (Nclosing_brace > Nopening_brace)
                {
                    if (Nclosing_brace - Nopening_brace != 1u || section_hierarchy.empty())
                        throw Exception("Invalid Lua file: issue with closing braces somewhere in the block closed "
                                        "in line " + std::to_string(line_count) + " (might be too much of them, "
                                        ", missing commas or opening brace not on the same line as its '=' sign).",
                                        __FILE__,
                                        __LINE__);

                    section_hierarchy.pop_back();
                }

                // Consider only lines with an '='.
                pos = line.find('=');

                if (pos == npos)
                    continue;

                // Extract the name of the variable (or of the section).
                {
                    lhs = line.substr(0, pos);
                    Utilities::String::Strip(lhs, "\t\n ");

                    // If there is if or else or elseif in it, it should be rejected.
                    bool reject_key = false;

                    for (auto rejected_key : rejected_keys)
                    {
                        if (Utilities::String::StartsWith(lhs, rejected_key + "(") ||
                            Utilities::String::StartsWith(lhs, rejected_key + " "))
                        {
                            reject_key = true;
                            break;
                        }
                    }

                    if (reject_key)
                        continue;
                }


                // Extract the associated value. If '{', it is a section, otherwise it is a mere variable.
                if (Nopening_brace > Nclosing_brace)
                {
                    if (Nopening_brace != Nclosing_brace + 1u)
                        throw Exception("Invalid Lua file: too much opening braces on line " + std::to_string(line_count),
                                        __FILE__,
                                        __LINE__);

                    section_hierarchy.push_back(lhs);
                }
                else
                {
                    ret.push_back(GenerateSectionNameFromHierarchy(section_hierarchy, lhs));
                }
            }

            const auto size = ret.size();

            Utilities::EliminateDuplicate(ret);

            if (ret.size() != size)
            {
                std::ostringstream oconv;
                oconv << "Invalid Lua file: the keys of arguments were not unique (" << size - ret.size()
                << "were duplicated).";
                throw Exception(oconv.str(), __FILE__, __LINE__);
            }

            return ret;
        }


        std::string GenerateSectionNameFromHierarchy(const std::vector<std::string>& hierarchy,
                                                     const std::string& param_name)
        {
            std::ostringstream oconv;

            for (const auto& item : hierarchy)
                oconv << item << ".";

            oconv << param_name;

            return oconv.str();
        }


        void WalkDown(const std::string& full_name,
                      const std::string& name,
                      lua_State* state)
        {
            assert(!name.empty());

            if (lua_isnil(state, -1))
            {
                std::ostringstream oconv;
                oconv << "Invalid Lua file: unable to find field '" << full_name << "'.";
                throw Exception(oconv.str(), __FILE__, __LINE__);
            }

            // The sub-entries are introduced with ".".
            const auto end = name.find(".");

            if (end == std::string::npos)
            {
                lua_pushstring(state, name.c_str());
                lua_gettable(state, -2);
            }
            else
            {
                assert(end < name.size());
                assert(name[end] == '.');
                // One step down.
                {
                    lua_pushstring(state, name.substr(0, end).c_str());
                    lua_gettable(state, -2);
                    WalkDown(name, name.substr(end + 1).c_str(), state);
                }
            }
        }


    } // namespace anonymous


} // namespace MoReFEM
