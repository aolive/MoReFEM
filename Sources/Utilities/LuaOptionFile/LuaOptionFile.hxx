//! \file
//
//
//  LuaOptionFile.hxx
//
//  Created by sebastien on 01/03/2018.
//Copyright © 2018 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_LUA_OPTION_FILE_x_LUA_OPTION_FILE_HXX_
# define MOREFEM_x_UTILITIES_x_LUA_OPTION_FILE_x_LUA_OPTION_FILE_HXX_


namespace MoReFEM
{


    namespace // anonymous
    {


        constexpr auto most_recent_in_stack = -1;


    } // namespace anonymous


    inline lua_State* LuaOptionFile::GetNonCstLuaState() noexcept
    {
        assert(!(!state_));
        return state_;
    }


    template<class T>
    T LuaOptionFile::Read(const std::string& entry_name, const std::string& constraint,
                  const char* invoking_file, int invoking_line)
    {
        PutOnStack(entry_name);

        if (lua_isnil(state_, -1))
        {
            decltype(auto) filename = GetFilename();
            std::ostringstream oconv;
            oconv << "Couldn't read entry '" << entry_name << "' in file '" << filename << "'.";
            throw Exception(oconv.str(), invoking_file, invoking_line);
        }

        T ret;

        if constexpr (Utilities::IsSpecializationOf<std::vector, T>())
            ReadVector(entry_name, constraint, invoking_file, invoking_line, ret);
        else if constexpr (Utilities::IsSpecializationOf<std::map, T>())
            ReadMap(entry_name, constraint, invoking_file, invoking_line, ret);
        else
        {
            ret = Convert<T>(most_recent_in_stack, entry_name, invoking_file, invoking_line);
            CheckConstraint(entry_name, constraint, invoking_file, invoking_line);
        }

        ClearStack();

        return ret ;
    }


    template<class T, class StringT>
    T LuaOptionFile::Convert(int index_in_lua_stack, StringT&& entry_name,
                     const char* invoking_file, int invoking_line)
    {
        if constexpr (std::is_same<T, bool>())
        {
            if (!lua_isboolean(state_, index_in_lua_stack))
                throw Exception("Entry " + entry_name + " is not a bool. Error is probably in the Read<>() type given in "
                                "the call.", invoking_file, invoking_line);

            return static_cast<bool>(lua_toboolean(state_, index_in_lua_stack));
        }
        else if constexpr (std::is_same<T, std::string>())
        {
            if (!lua_isstring(state_, index_in_lua_stack))
                throw Exception("Entry " + entry_name + " is not a string. Error is probably in the Read<>() type given in "
                                "the call.", invoking_file, invoking_line);

            return static_cast<std::string>(lua_tostring(state_, index_in_lua_stack));
        }
        else if constexpr (std::is_integral<T>())
        {
            if (!lua_isinteger(state_, index_in_lua_stack))
            {
                std::ostringstream oconv;
                oconv << "Entry " << entry_name << " is not an integer. Error is probably in the Read<>() type given "
                "in the call.";

                throw Exception(oconv.str(), invoking_file, invoking_line);
            }

            return static_cast<T>(lua_tointeger(state_, index_in_lua_stack));
        }
        else if constexpr (std::is_floating_point<T>())
        {
            if (!lua_isnumber(state_, index_in_lua_stack))
            {
                std::ostringstream oconv;
                oconv << "Entry " << entry_name << " is not a floating point. Error is probably in the Read<>() type "
                "given in the call.";

                throw Exception(oconv.str(), invoking_file, invoking_line);
            }

            return static_cast<T>(lua_tonumber(state_, index_in_lua_stack));
        }

        std::cerr << "Conversion into " << GetTypeName<T>() << " was not foreseen..." << std::endl;
        assert(false && "Conversion not foreseen... (or return missing in a case above).");
        exit(EXIT_FAILURE);

    }


    template<class StringT>
    void LuaOptionFile::CheckConstraint(StringT&& name, const std::string& constraint,
                                const char* invoking_file, int invoking_line)
    {
        if (constraint == "")
            return;

        std::ostringstream oconv;
        oconv << "function lua_opt_file_check_constraint(v)\nreturn " << constraint
        << "\nend\nlua_opt_file_result = lua_opt_file_check_constraint(" << name << ')';

        if (luaL_dostring(state_, oconv.str().c_str()))
        {
            oconv.str("");
            oconv << "Error in CheckConstraint while checking " << name << ":\n  " << lua_tostring(state_, -1);

            throw Exception(oconv.str(), invoking_file, invoking_line);
        }

        PutOnStack("lua_opt_file_result");

        assert(lua_isboolean(state_, -1));
        const bool result = static_cast<bool>(lua_toboolean(state_, -1));

        if (!result)
        {
            oconv.str("");
            oconv << "Constraint \"" << constraint << "\" is not fulfilled for \"" << name << "\"!";
            throw Exception(oconv.str(), invoking_file, invoking_line);
        }
    }


    template<class T>
    void LuaOptionFile::ReadVector(const std::string& entry_name, const std::string& constraint,
                           const char* invoking_file, int invoking_line, T& vector)
    {
        if (!lua_istable(state_, -1))
            throw Exception("Error in Read: entry " + entry_name + " is not a table.",
                            invoking_file, invoking_line);

        using element_type = typename T::value_type;

        // Now loops over all elements of the table.
        lua_pushnil(state_);
        while (lua_next(state_, -2) != 0)
        {
            // Duplicates the key and value so that 'lua_tostring' (applied to
            // them) should not interfere with 'lua_next'.
            lua_pushvalue(state_, -2);
            lua_pushvalue(state_, -2);

            const auto element = Convert<element_type>(-1, entry_name, invoking_file, invoking_line);
            vector.push_back(element);

            lua_pop(state_, 3);
        }

        const auto Nelem = vector.size();
        for (auto i = 0ul; i < Nelem; ++i)
            CheckConstraint(entry_name + "[" + std::to_string(i + 1) + "]", constraint,
                            invoking_file, invoking_line);
    }


    template<class T>
    void LuaOptionFile::ReadMap(const std::string& entry_name, const std::string& constraint,
                        const char* invoking_file, int invoking_line, T& map)
    {
        if (!lua_istable(state_, -1))
            throw Exception("Error in Read: entry " + entry_name + " is not a table.",
                            invoking_file, invoking_line);

        using key_type = typename T::key_type;
        using mapped_type = typename T::mapped_type;

        // Now loops over all elements of the table.
        lua_pushnil(state_);
        while (lua_next(state_, -2) != 0)
        {
            // Duplicates the key and value so that 'lua_tostring' (applied to
            // them) should not interfere with 'lua_next'.
            lua_pushvalue(state_, -2);
            lua_pushvalue(state_, -2);

            const auto key = Convert<key_type>(-2, entry_name, invoking_file, invoking_line);
            const auto element = Convert<mapped_type>(-1, entry_name, invoking_file, invoking_line);

            const auto check = map.insert(std::make_pair(key, element));

            if (!check.second)
                throw Exception("Error while reading entry \"" + entry_name + "\": same key read twice!",
                                invoking_file, invoking_line);

            lua_pop(state_, 3);

        }

        for (const auto& elem : map)
        {
            std::ostringstream oconv;

            if (std::is_same<key_type, std::string>())
                oconv << entry_name << "[\"" << elem.first << "\"]";
            else
                oconv << entry_name << "[" << elem.first << "]";

            CheckConstraint(oconv.str(), constraint,
                            invoking_file, invoking_line);
        }
    }


    inline const std::string& LuaOptionFile::GetFilename() const noexcept
    {
        assert("Probably missing call to Open()!" && !filename_.empty());
        return filename_;
    }


    template<typename T, typename ...Args>
    T LuaOptionFile::Apply(const std::string& entry_name,
                           const char* invoking_file, int invoking_line,
                           Args&&... in)
    {
        PutOnStack(entry_name);
        PushOnStack(std::forward_as_tuple(in...));

        int n = lua_gettop(state_);

        const auto size = static_cast<int>(sizeof...(in));

        if (lua_pcall(state_, size, LUA_MULTRET, 0) != 0)
        {
            std::ostringstream oconv;
            oconv << "Error while calling Apply() for entry '" << entry_name << "'.";
            throw Exception(oconv.str(), invoking_file, invoking_line);
        }

        n = lua_gettop(state_) - n + 1 + size;

        for (int i = 0; i < n - 1; i++)
            Convert<T>(i - n, "Function index", invoking_file, invoking_line);

        const auto ret = Convert<T>(-1, "Last function index", invoking_file, invoking_line);

        ClearStack();

        return ret;
    }


    template<typename T>
    void LuaOptionFile::PushOnStack(T value)
    {
        if constexpr (Utilities::IsSpecializationOf<std::tuple, T>())
        {
            #  ifdef MOREFEM_LLVM_CLANG
            PRAGMA_DIAGNOSTIC(push)
            PRAGMA_DIAGNOSTIC(ignored "-Wunused-lambda-capture")
            //< because LLVM clang issues an erroneous warning with 'this' capture below.
            #  endif // MOREFEM_LLVM_CLANG

            // See https://stackoverflow.com/questions/16387354/template-tuple-calling-a-function-on-each-element.
            std::apply([this](auto ...x)
                       { (..., PushOnStack(x)); },
                       value);

            #  ifdef MOREFEM_LLVM_CLANG
            PRAGMA_DIAGNOSTIC(pop)
            #  endif // MOREFEM_LLVM_CLANG

        }
        else
        {
            if constexpr (std::is_same<T, bool>())
                lua_pushboolean(state_, value);
            else if constexpr (std::is_same<T, std::string>())
                lua_pushstring(state_, value.c_str());
            else if constexpr (std::is_integral<T>())
                lua_pushinteger(state_, value);
            else if constexpr (std::is_floating_point<T>())
                lua_pushnumber(state_, value);
            else
            {
                std::cerr << "PushingOnStack type " << GetTypeName<T>() << " was not foreseen..." << std::endl;
                assert(false);
                exit(EXIT_FAILURE);
            }
        }
    }


    inline void LuaOptionFile::SetEntryKeyList(std::vector<std::string>&& entry_key_list)
    {
        assert(entry_key_list_.empty() && "Should only be called once.");
        entry_key_list_ = std::move(entry_key_list);
    }


    inline const std::vector<std::string>& LuaOptionFile::GetEntryKeyList() const noexcept
    {
        return entry_key_list_;
    }


} // namespace MoReFEM


#endif // MOREFEM_x_UTILITIES_x_LUA_OPTION_FILE_x_LUA_OPTION_FILE_HXX_
