///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 30 Aug 2013 11:06:17 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_LUA_FUNCTION_HPP_
# define MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_LUA_FUNCTION_HPP_

# include <cassert>
# include <string>
# include <functional>

# include "Utilities/LuaOptionFile/LuaOptionFile.hpp"


namespace MoReFEM
{


    namespace Utilities
    {


        namespace InputParameterListNS
        {


            //! \cond IGNORE_BLOCK_IN_DOXYGEN

            template<class T>
            struct LuaFunction;

            //! \endcond IGNORE_BLOCK_IN_DOXYGEN



            /*!
             * \brief High-level functor that handles the call to lua to get the result of a function
             * defined as an input parameter.
             *
             * \internal <b><tt>[internal]</tt></b> This is defined as a child of std::function because it is really one specific instance of it;
             * nevertheless the only feature inherited is result_type alias. The derivation could have
             * been skipped and \code alias ResultTypeT result_type \endcode put instead in this class.
             *
             * \tparam ReturnTypeT Return type of the function.
             * \tparam Args Types of the arguments of the function. Currently all the arguments
             * should share the same type, due to a limitation in current Ops implementation. Hopefully Ops
             * will be extended to allow use of different arguments; anyway this interface is ready to face it
             * when possible.
             */
            template<typename ReturnTypeT, typename... Args>
            struct LuaFunction<ReturnTypeT(Args...)> : public std::function<ReturnTypeT(Args...)>
            {

                /// \name Special members.
                ///@{

                /*!
                 * \brief Default constructor.
                 *
                 * Required only to be able to store an LuaFunction object in a tuple; should not be used otherwise.
                 */
                explicit LuaFunction();

                /*!
                 * \brief Effective constructor.
                 *
                 * \param[in] fullname Key used in the Ops file. A key is:
                 * - Either the Name() of the InputParameter if its Section() is left empty.
                 * - or Section() + '.' + Name() otherwise.
                 * \param[in] lua_option_file \a LuaOptionFile object to which the calculation will be passed.
                 */
                explicit LuaFunction(const std::string& fullname, LuaOptionFile* lua_option_file);


                //! Destructor.
                ~LuaFunction() = default;

                //! Copy constructor.
                LuaFunction(const LuaFunction&) = default;

                //! Move constructor.
                LuaFunction(LuaFunction&&) = default;

                //! Copy assignation.
                LuaFunction& operator=(const LuaFunction&) = default;

                //! Move assignation.
                LuaFunction& operator=(LuaFunction&&) = default;


                ///@}

                /*!
                 * \brief Ensure functor behaviour.
                 *
                 * \copydoc doxygen_hide_cplusplus_variadic_args
                 *
                 * \return Value computed by the functor.
                 */
                ReturnTypeT operator()(Args... args) const;

                //! Return the key of the funciton in the input file (required to make Ops fetch it).
                const std::string& GetKey() const;


            private:

                /*!
                 * \brief Key used in the Ops file.
                 *
                 * A key is:
                 * - Either the Name() of the InputParameter if its Section() is left empty.
                 * - or Section() + '.' + Name() otherwise.
                 */
                std::string fullname_;

                //! Reference to underlying LuaOptionFile object.
                LuaOptionFile* lua_option_file_ = nullptr;

            };


        } // namespace InputParameterListNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/InputParameterList/LuaFunction.hxx"


#endif // MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_LUA_FUNCTION_HPP_
