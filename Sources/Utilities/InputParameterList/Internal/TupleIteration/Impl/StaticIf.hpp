///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Dec 2016 17:21:02 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_STATIC_IF_HPP_
# define MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_STATIC_IF_HPP_

# include <memory>
# include <vector>


namespace MoReFEM
{


    namespace Internal
    {


        namespace InputParameterListNS
        {


            namespace Impl
            {


                /*!
                 * \brief Internal helper object to find an \a InputParameterT
                 */
                template
                <
                    bool IsFoundT,
                    class NextItemT,
                    class InputParameterT
                >
                struct Find;



                // ============================
                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                // Specializations.
                // ============================
                template
                <
                    class NextItemT,
                    class InputParameterT
                >
                struct Find<true, NextItemT, InputParameterT>
                {


                    static constexpr bool Perform();


                };



                template
                <
                    class NextItemT,
                    class InputParameterT
                >
                struct Find<false, NextItemT, InputParameterT>
                {

                    static constexpr bool Perform();

                };



                // ============================
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN
                // Specializations.
                // ============================


                //! Free function used to ensure choice is truly made statically.
                template
                <
                    bool IsFoundT,
                    class EnrichedSectionOrParameterT,
                    unsigned int IndexT,
                    class NextItemT
                >
                struct ExtractValueFromInputParameter;


                // ============================
                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                // Specializations.
                // ============================


                //! Specialization when requested value has been found.
                template
                <
                    class EnrichedSectionOrParameterT,
                    unsigned int IndexT,
                    class NextItemT
                >
                struct ExtractValueFromInputParameter<true, EnrichedSectionOrParameterT, IndexT, NextItemT>
                {


                    template<class TupleT, class InputParameterT>
                    static void Perform(const TupleT& tuple,
                                        const InputParameterT*& parameter_if_found);

                };


                //! Specialization when requested value has not been found.
                template
                <
                    class EnrichedSectionOrParameterT,
                    unsigned int IndexT,
                    class NextItemT
                >
                struct ExtractValueFromInputParameter<false, EnrichedSectionOrParameterT, IndexT, NextItemT>
                {


                    template<class TupleT, class InputParameterT>
                    static void Perform(const TupleT& tuple,
                                        const InputParameterT*& parameter_if_found);


                };


                // ============================
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN
                // Specializations.
                // ============================



                template
                <
                    bool IsParameterInCurrentSectionT,
                    class TupleIterationT
                >
                struct ExtractValueFromSection;




                // ============================
                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                // Specializations.
                // ============================
                template
                <
                    class TupleIterationT
                >
                struct ExtractValueFromSection<true, TupleIterationT>
                {

                    template<class SectionContentT, class InputParameterT>
                    static void Perform(const SectionContentT& item,
                                        const InputParameterT*& parameter_if_found);


                };



                template
                <
                    class TupleIterationT
                >
                struct ExtractValueFromSection<false, TupleIterationT>
                {

                    template<class SectionContentT, class InputParameterT>
                    static void Perform(const SectionContentT& item,
                                        const InputParameterT*& parameter_if_found);


                };
                // ============================
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN
                // Specializations.
                // ============================



            } // namespace Impl


        } // namespace InputParameterListNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/InputParameterList/Internal/TupleIteration/Impl/StaticIf.hxx"


#endif // MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_STATIC_IF_HPP_
