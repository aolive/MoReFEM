///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 26 Aug 2015 17:28:06 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#include <memory>
#include <vector>
#include <cassert>
#include <algorithm>

#include "Utilities/String/String.hpp"
#include "Utilities/InputParameterList/Internal/TupleIteration/Impl/PrepareDefaultEntry.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace InputParameterListNS
        {
            
            
            namespace Impl
            {
                    
                    
                    namespace // anonymous
                    {
                        
                        
                        struct AnalyzePath
                        {
                            
                            using unique_ptr = std::unique_ptr<AnalyzePath>;
                            
                            using vector_unique_ptr = std::vector<unique_ptr>;
                            
                            explicit AnalyzePath(const std::pair<std::string, std::string>& key_and_content);
                            
                            std::string parameter_name_;
                            
                            std::string content_in_file_;
                            
                            std::vector<std::string> section_list_;
                            
                        };
                        
                        
                        struct Section
                        {
                            
                            using unique_ptr = std::unique_ptr<Section>;
                            
                            using vector_unique_ptr = std::vector<unique_ptr>;
                            
                            explicit Section(std::string name);
                            
                            std::string name;
                            
                            Section::vector_unique_ptr section_list_;
                            
                            AnalyzePath::vector_unique_ptr parameter_list_;
                            
                            
                        };
                        
                        
                        
                        void SortPerSection(const std::vector<std::pair<std::string, std::string>>& parameter_content,
                                            Section::vector_unique_ptr& section_hierarchy,
                                            AnalyzePath::vector_unique_ptr& root_level_parameter_list);
                        
                        
                        void PrintParameterList(const AnalyzePath::vector_unique_ptr& parameter_list,
                                                unsigned int depth_level,
                                                std::ostream& out);
                        
                        /*!
                         * \brief Print the content of all subsections within a section.
                         *
                         * \internal <b><tt>[internal]</tt></b> This function calls itself recursively on purpose: we do not want to miss one
                         * parameter in the hierarchy.
                         */
                        void PrintSubsectionContent(const Section::vector_unique_ptr& subsection_list,
                                                    unsigned int& depth_level,
                                                    std::ostream& out);
                        
                        
                    } // namespace anonymous
                    
                    
                    const std::string& GetIndentPlaceholder()
                    {
                        static std::string ret("INDENT_PLACEHOLDER");
                        return ret;
                    }
                    
                    
                    
                    void PrintInFile(const std::vector<std::pair<std::string, std::string>>& parameter_block_per_identifier,
                                     std::ostream& out)
                    {
                        
                        Section::vector_unique_ptr section_hierarchy;
                        AnalyzePath::vector_unique_ptr root_level_parameter_list;
                        
                        SortPerSection(parameter_block_per_identifier,
                                       section_hierarchy,
                                       root_level_parameter_list);

                        unsigned int depth_level = 0ul;
                        
                        // Iterate through the section hierarchy.
                        for (const auto& section_ptr : section_hierarchy)
                        {
                            assert(!(!section_ptr));
                            
                            const auto& section = *section_ptr;
                            
                            out << section.name << " = {\n\n";
                            ++depth_level;
                            
                            PrintSubsectionContent(section.section_list_, depth_level, out);
                            PrintParameterList(section.parameter_list_, depth_level, out);
                            assert(depth_level > 0u);
                            --depth_level;
                            out << "} -- " << section.name << "\n\n";
                        }
                        
                        
                        // Then print the root level parameters, which were not in the hierarchy considered so far.
                        assert(depth_level == 0u);
                        PrintParameterList(root_level_parameter_list, depth_level, out);
                    
                    }
                    
                    
                    namespace // anonymous
                    {
                        
                        
                        AnalyzePath::AnalyzePath(const std::pair<std::string, std::string>& key_and_content)
                        : content_in_file_(key_and_content.second)
                        {
                            const auto& identifier = key_and_content.first;
                            auto pos = identifier.rfind('.');
                            
                            if (pos == std::string::npos)
                                parameter_name_ = identifier;
                            else
                            {
                                parameter_name_ = std::string(identifier, pos + 1, std::string::npos);
                                
                                std::string parent(identifier, 0, pos);
                                
                                pos = parent.find('.');
                                
                                while (pos != std::string::npos)
                                {
                                    section_list_.push_back(std::string(parent, 0, pos));
                                    parent.erase(0, pos + 1);
                                    
                                    pos = parent.find('.');
                                }
                                
                                section_list_.push_back(parent);
                            }
                            
                            //                        std::cout << "SECTION = ";
                            //
                            //                        for (const auto& content : section_list_)
                            //                            std::cout << content << '\t';
                            //
                            //                        std::cout << "NAME = " << parameter_name_ << std::endl;
                            
                            
                        }
                        
                        
                        Section::Section(std::string a_name)
                        : name(a_name)
                        { }
                        
                        
                        Section* Helper(AnalyzePath::unique_ptr& current_object_ptr,
                                        std::size_t depth_level,
                                        Section::vector_unique_ptr& section_list,
                                        AnalyzePath::vector_unique_ptr& end_parameter_list);
                        
                        
                        Section* Helper(AnalyzePath::unique_ptr& current_object_ptr,
                                        std::size_t depth_level,
                                        Section::vector_unique_ptr& section_list,
                                        AnalyzePath::vector_unique_ptr& end_parameter_list)
                        
                        {
                            assert(!(!current_object_ptr));
                            
                            auto& item = *current_object_ptr;
                            
                            if (depth_level < item.section_list_.size())
                            {
                                const auto& name = item.section_list_[depth_level];
                                
                                auto it = std::find_if(section_list.cbegin(),
                                                       section_list.cend(),
                                                       [&name](const auto& section)
                                                       {
                                                           return section->name == name;
                                                       });
                                
                                if (it == section_list.cend())
                                {
                                    section_list.emplace_back(std::make_unique<Section>(name));
                                    return section_list.back().get();
                                }
                                else
                                    return it->get();
                            }
                            else
                            {
                                end_parameter_list.push_back(nullptr);
                                std::swap(current_object_ptr, end_parameter_list.back());
                                return nullptr;
                            }
                        }
                        
                        
                        
                        void SortPerSection(const std::vector<std::pair<std::string, std::string>>& parameter_content,
                                            Section::vector_unique_ptr& section_hierarchy,
                                            AnalyzePath::vector_unique_ptr& root_level_parameter_list)
                        {
                            AnalyzePath::vector_unique_ptr break_into_sections;
                            
                            std::size_t maximum_depth = 0ul;
                            
                            
                            // Iterate through the keys and split content into sections.
                            for (const auto& item : parameter_content)
                            {
                                auto&& current_object = std::make_unique<AnalyzePath>(item);
                                
                                if (current_object->section_list_.size() > maximum_depth)
                                    maximum_depth = current_object->section_list_.size();
                                
                                break_into_sections.emplace_back(std::move(current_object));
                            }
                            
                            // Create all root level sections and end-parameters.
                            
                            
                            for (auto& item_ptr : break_into_sections)
                            {
                                std::size_t index = 0u;
                                auto next_section_ptr = Helper(item_ptr,
                                                               index,
                                                               section_hierarchy,
                                                               root_level_parameter_list);
                                
                                while (next_section_ptr  != nullptr)
                                {
                                    auto& next_section= *next_section_ptr;
                                    
                                    next_section_ptr = Helper(item_ptr,
                                                              ++index,
                                                              next_section.section_list_,
                                                              next_section.parameter_list_);
                                }
                            }
                    
                        }
                        
                        
                        
                        void PrintParameterList(const AnalyzePath::vector_unique_ptr& parameter_list,
                                                unsigned int depth_level,
                                                std::ostream& out)
                        {
                            
                            const auto Nparameter = parameter_list.size();
                            std::size_t index = 0ul;
                            
                            for (const auto& parameter_ptr : parameter_list)
                            {
                                ++index;
                                
                                assert(!(!parameter_ptr));
                                
                                auto& content = parameter_ptr->content_in_file_;
                                
                                // Replace placeholder by the correct indent.
                                const auto indent = Utilities::String::Repeat('\t', depth_level);
                                
                                Utilities::String::Replace(GetIndentPlaceholder(), indent, content);
                                
                                out << parameter_ptr->content_in_file_;
                                
                                if (index < Nparameter)
                                    out << ',';
                                
                                out << "\n\n";
                            }
                        }
                        
                        
                        /*!
                         * \brief Print the content of all subsections within a section.
                         *
                         * \internal <b><tt>[internal]</tt></b> This function calls itself recursively on purpose: we do not want to miss one 
                         * parameter in the hierarchy.
                         */
                        void PrintSubsectionContent(const Section::vector_unique_ptr& subsection_list,
                                                    unsigned int& depth_level,
                                                    std::ostream& out)
                        {
                            for (const auto& subsection_ptr : subsection_list)
                            {
                                assert(!(!subsection_ptr));
                                const auto& subsection = *subsection_ptr;
                                
                                const auto indent = Utilities::String::Repeat('\t', depth_level);
                                
                                out << indent << subsection.name << " = {\n\n";
                                PrintSubsectionContent(subsection.section_list_, ++depth_level, out);

                                PrintParameterList(subsection.parameter_list_, depth_level, out);
                                assert(depth_level > 0u);
                                --depth_level;
                                
                                out << indent << "}, -- " << subsection.name << "\n\n";
                            }
                        }
                        
                        
                    } // namespace anonymous
                    
                    
            } // namespace Impl
            
            
        } // namespace InputParameterListNS
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup
