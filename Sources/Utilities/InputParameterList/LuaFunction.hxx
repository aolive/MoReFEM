///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 30 Aug 2013 11:06:17 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_LUA_FUNCTION_HXX_
# define MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_LUA_FUNCTION_HXX_


namespace MoReFEM
{


    namespace Utilities
    {


        namespace InputParameterListNS
        {



            template<typename ReturnTypeT, typename ...Args>
            LuaFunction<ReturnTypeT(Args...)>::LuaFunction()
            : lua_option_file_(nullptr)
            { }


            template<typename ReturnTypeT, typename ...Args>
            LuaFunction<ReturnTypeT(Args...)>::LuaFunction(const std::string& fullname,
                                                           LuaOptionFile* lua_option_file)
            : fullname_(fullname),
            lua_option_file_(lua_option_file)
            { }


            template<typename ReturnTypeT, typename ...Args>
            inline ReturnTypeT LuaFunction<ReturnTypeT(Args...)>::operator()(Args... args) const
            {
                assert(!fullname_.empty());
                assert(!(!lua_option_file_));

                return lua_option_file_->Apply<ReturnTypeT>(fullname_, __FILE__, __LINE__, args...);
            }


            template<typename ReturnTypeT, typename ...Args>
            const std::string& LuaFunction<ReturnTypeT(Args...)>::GetKey() const
            {
                return fullname_;
            }


        } // namespace InputParameterListNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup



#endif // MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_LUA_FUNCTION_HXX_
