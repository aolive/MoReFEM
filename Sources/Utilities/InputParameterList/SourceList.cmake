target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Definitions.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Base.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Base.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Definitions.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/LuaFunction.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/LuaFunction.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/Crtp/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/SourceList.cmake)
