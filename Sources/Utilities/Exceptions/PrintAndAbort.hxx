///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 4 Mar 2015 10:51:42 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_EXCEPTIONS_x_PRINT_AND_ABORT_HXX_
# define MOREFEM_x_UTILITIES_x_EXCEPTIONS_x_PRINT_AND_ABORT_HXX_


namespace MoReFEM
{


    namespace ExceptionNS
    {



    } // namespace ExceptionNS


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_EXCEPTIONS_x_PRINT_AND_ABORT_HXX_
