target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Exception.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Factory.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/PrintAndAbort.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Exception.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Factory.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/PrintAndAbort.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/PrintAndAbort.hxx"
)

