///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Aug 2013 11:03:27 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_STRING_x_STRING_HPP_
# define MOREFEM_x_UTILITIES_x_STRING_x_STRING_HPP_


# include <string>
# include <sstream>
# include <vector>


namespace MoReFEM
{


    namespace Utilities
    {


        namespace String
        {


            /*!
             * \brief Strip from the left all characters that belongs to \a char_to_strip.
             *
             * \param[in,out] string String to be stripped.
             * \param[in] char_to_strip All the characters that should be deleted should they be found on the left of \a string.
             *
             * For instance,
             * \code
             * std::string str = "  \t\n      TEST    \t \n";
             * StripLeft(str, "\t\n "); // => yields "TEST    \t \n";
             *
             * \endcode
             */
            void StripLeft(std::string& string, const std::string& char_to_strip = "\t\n ");


            /*!
             * \brief Strip from the right all characters that belongs to \a char_to_strip.
             *
             * \param[in,out] string String to be stripped.
             * \param[in] char_to_strip All the characters that should be deleted should they be found on the right of \a string.
             *
             * For instance,
             * \code
             * std::string str = "  \t\n      TEST    \t \n";
             * StripRight(str, "\t\n "); // => yields "  \t\n      TEST";
             *
             * \endcode
             */
            void StripRight(std::string& string, const std::string& char_to_strip = "\t\n ");


            /*!
             * \brief Strip from the left and the right all characters that belongs to \a char_to_strip.
             *
             * \param[in,out] string String to be stripped.
             * \param[in] char_to_strip All the characters that should be deleted should they be found on the
             * left and the right of \a string.
             *
             * For instance,
             * \code
             * std::string str = "  \t\n      TEST    \t \n";
             * Strip(str, "\t\n "); // => yields "TEST";
             *
             * \endcode
             */
            void Strip(std::string& string, const std::string& char_to_strip = "\t\n ");


            /*!
             * \brief Tells whether \a string starts with \a sequence.
             *
             * \param[in] string String considered.
             * \param[in] sequence Sequence that is searched at the beginning of \a string.
             *
             * \return True if \a string starts with \a sequence.
             */
            bool StartsWith(const std::string& string, const std::string& sequence);


            /*!
             * \brief Tells whether \a string ends with \a sequence.
             *
             * \param[in] string String considered.
             * \param[in] sequence Sequence that is searched at the end of \a string.
             *
             * \return True if \a string ends with \a sequence.
             */
            bool EndsWith(const std::string& string, const std::string& sequence);



            /*!
             * \brief Replace all occurrences of \a to_replace in \a string_to_modify by \a replacement.
             *
             * \param[in] to_replace Sequence looked at in the string. Beware: this look-up is case-sensitive!
             * \param[in] replacement String to replace the previous one.
             * \param[in,out] string_to_modify String into which the substitution will occur.
             *
             * \return Number of occurrences found.
             */
            unsigned int Replace(const std::string& to_replace,
                                 const std::string& replacement,
                                 std::string& string_to_modify);



            /*!
             * \brief Reformat a string so that it doesn't exceed a certain length per line.
             *
             * \param[in] string String considered.
             * \param[in] preffix Preffix to add to each new line. For instance "\t--" in the case of a comment
             * in an input parameter file.
             * \param[in] max_length Maximum number of characters on a new line. Preffix size is not included in this
             * count.
             * \param[in] separator Acceptable line separator. Space by default.
             *
             * \return Reformatted string.
             */
            std::string Reformat(const std::string& string, std::size_t max_length, const std::string& preffix,
                                 const char separator = ' ');


            /*!
             * \brief Repeat the same \a string \a times.
             *
             * \code
             * Repeat('\t', 4)
             * \endcode
             *
             * yields '\\t\\t\\t\\t'.
             *
             * \param[in] string String to repeat.
             * \param[in] times Number of times the string is to be repeated.
             *
             * \return The result (i.e. \a string repeated \a times times).
             */
            template<class StringT>
            std::string Repeat(const StringT& string, unsigned int times);



            /*!
             * \brief Convert a std::vector<char> into a string.
             *
             * This is useful when using functions with a C API. in this case you should use a std::vector<char> to
             * collect char*, as seen in this example with MPI API;
             *
             * \code
             * std::vector<char> error_string_char(MPI_MAX_ERROR_STRING);
             * int length_of_error_string;
             *
             * MPI_Error_string(error_code, error_string_char.data(), &length_of_error_string);
             * \endcode
             *
             * This char array might be required as a string if another operation is asked, hence this function, which
             * takes care of the terminating null character (std::string expects '\0' as final character).
             *
             * \param[in] array C-string array to be converted.
             * \return The C++ string.
             */
            std::string ConvertCharArray(const std::vector<char>& array);


        } // namespace String


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/String/String.hxx"


#endif // MOREFEM_x_UTILITIES_x_STRING_x_STRING_HPP_
