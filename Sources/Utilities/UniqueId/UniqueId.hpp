///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 14 Nov 2014 11:12:55 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_UNIQUE_ID_x_UNIQUE_ID_HPP_
# define MOREFEM_x_UTILITIES_x_UNIQUE_ID_x_UNIQUE_ID_HPP_


# include <set>
# include <sstream>

# include "Utilities/Numeric/Numeric.hpp"
# include "Utilities/Exceptions/Exception.hpp"


namespace MoReFEM
{


    namespace UniqueIdNS
    {


        /*!
         * \brief Whether the unique id is given in constructor or generated automatically by incrementing a static counter.
         *
         * Details for each mode:
         * - \a automatic: Each time a new instance of \a DerivedT is created, it is assigned a value (which is the number
         * of already created instance). This value is accessible through the method GetUniqueId().
         * - \a manual: The ID is given by the user, and there is a check the id remains effectively unique.
         */
        enum class AssignationMode
        {
            automatic,
            manual
        };


        /*!
         * \brief Whether it is possible to build an object without assigning to it a unique id.
         *
         * \attention Relevant only for a \a UniqueId which \a AssignationMode is manual.
         */
        enum class DoAllowNoId
        {
            no,
            yes
        };



    } // namespace UniqueIdNS



    namespace Crtp
    {


        /*!
         * \brief This CRTP class defines a unique ID for each object of the DerivedT class.
         *
         * \tparam AssignationModeT See \a AssignationMode enum.
         * \tparam DoAllowNoIdT See \a DoAllowNoId enum.
         */
        template
        <
            class DerivedT,
            UniqueIdNS::AssignationMode AssignationModeT = UniqueIdNS::AssignationMode::automatic,
            UniqueIdNS::DoAllowNoId DoAllowNoIdT = UniqueIdNS::DoAllowNoId::no
        >
        class UniqueId
        {


        public:

            /// \name Special members.
            ///@{

            //! Constructor for AssignationMode::automatic.
            explicit UniqueId();

            //! Constructor for AssignationMode::manual.
            explicit UniqueId(unsigned int id);

            //! Constructor for AssignationMode::manual when no id is to be assigned (only if DoAllowNoIdT is yes).
            explicit UniqueId(std::nullptr_t);

            //! Destructor.
            ~UniqueId() = default;

            //! Copy constructor.
            UniqueId(const UniqueId&) = delete;

            //! Move constructor.
            UniqueId(UniqueId&&) = default;

            //! Copy affectation.
            UniqueId& operator=(const UniqueId&) = delete;

            //! Move affectation.
            UniqueId& operator=(UniqueId&&) = default;


            ///@}

        public:

            /*!
             * \brief Get the value of the internal unique ID.
             *
             */
            unsigned int GetUniqueId() const;


        private:

            /*!
             * \brief Static counter that increment the value of \a unique_id_ and returns it.
             */
            static unsigned int& AssignUniqueId();

        private:

            /*!
             * \brief The value of the unique id for the current \a DerivedT object.
             */
            const unsigned int unique_id_;

            /*!
             * \brief List of already known ids. This is relevant only for manual assignation.
             */
            static std::set<unsigned int>& GetNonCstUniqueIdList();

        };


    } // namespace Crtp


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#include "Utilities/UniqueId/UniqueId.hxx"


#endif // MOREFEM_x_UTILITIES_x_UNIQUE_ID_x_UNIQUE_ID_HPP_
