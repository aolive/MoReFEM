///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 3 Oct 2013 14:58:15 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#include <sstream>

#include "ThirdParty/IncludeWithoutWarning/Boost/Filesystem.hpp"

#include "Utilities/Filesystem/Folder.hpp"
#include "Utilities/Exceptions/Exception.hpp"


namespace MoReFEM
{
    
    
    namespace FilesystemNS
    {
        
        
        namespace Folder
        {
            
            
            bool DoExist(const std::string& folder)
            {
                try
                {
                    // is_directory also checks it does exist.
                    return boost::filesystem::is_directory(boost::filesystem::path(folder));
                }
                catch (const boost::filesystem::filesystem_error& e)
                {
                    std::ostringstream oconv;
                    oconv << boost::diagnostic_information(e);
                    throw Exception(oconv.str(), __FILE__, __LINE__);
                }
            }
            
            
            void Create(const std::string& folder, const char* invoking_file, int invoking_line)
            {
                if (DoExist(folder))
                    throw Exception("Folder " + folder + " already exists!", invoking_file, invoking_line);

                try
                {
                    const auto is_ok = boost::filesystem::create_directories(boost::filesystem::path(folder));
//                    assert(is_ok && "If not, it means DoExist doesn't work as expected...");
                    static_cast<void>(is_ok);

                }
                catch (const boost::filesystem::filesystem_error& e)
                {
                    std::ostringstream oconv;
                    oconv << boost::diagnostic_information(e);
                    throw Exception(oconv.str(), __FILE__, __LINE__);
                }
            }
            
            
            void Remove(const std::string& folder, const char* invoking_file, int invoking_line)
            {
                if (!DoExist(folder))
                    throw Exception("Folder " + folder + " doesn't exist!", invoking_file, invoking_line);
                
                try
                {
                    boost::filesystem::remove_all(boost::filesystem::path(folder));
                }
                catch (const boost::filesystem::filesystem_error& e)
                {
                    std::ostringstream oconv;
                    oconv << boost::diagnostic_information(e);
                    throw Exception(oconv.str(), __FILE__, __LINE__);
                }
            }
            
            
        } // namespace Folder
        
        
    } // namespace FilesystemNS
    
    
} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup
