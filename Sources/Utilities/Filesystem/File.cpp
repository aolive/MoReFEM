///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 30 Jul 2013 14:26:35 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#include <sstream>

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Exceptions/Exception.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Filesystem.hpp"


namespace MoReFEM
{
    
    
    namespace FilesystemNS
    {
        
        
        namespace // anonymous
        {
            
            
            void CheckFilenameNotEmpty(const std::string& filename, const char* invoking_file, int invoking_line)
            {
                if (filename.empty())
                    throw Exception("Trying to operate on an empty string!", invoking_file, invoking_line);
            }
            
            
        } // namespace anonymous
        
        
        namespace File
        {
            
            
            
            void Remove(const std::string& filename, const char* invoking_file, int invoking_line)
            {
                CheckFilenameNotEmpty(filename, invoking_file, invoking_line);
                
                int err = std::remove(filename.c_str());
                
                if (err)
                    throw Exception("Unable to remove " + filename, invoking_file, invoking_line);
            }
            
            
            
            void Create(std::ofstream& out, const std::string& filename, const char* invoking_file, int invoking_line)
            {
                CheckFilenameNotEmpty(filename, invoking_file, invoking_line);
                out.open(filename);
                
                if (!out)
                    throw Exception("Unable to create file " + filename, invoking_file, invoking_line);
            }
            
            
            void Read(std::ifstream& in, const std::string& filename, const char* invoking_file, int invoking_line)
            {
                CheckFilenameNotEmpty(filename, invoking_file, invoking_line);
                in.open(filename);
                
                if (!in)
                    throw Exception("Unable to read file " + filename, invoking_file, invoking_line);
            }
            
            
            void Append(std::ofstream& inout, const std::string& filename, const char* invoking_file, int invoking_line)
            {
                CheckFilenameNotEmpty(filename, invoking_file, invoking_line);
                
                inout.open(filename, std::ios::app);
                
                if (!inout)
                    throw Exception("Unable to read/modify file \"" + filename + "\".", invoking_file, invoking_line);
            }
            
            
            bool DoExist(const std::string& filename)
            {
                // Not the most efficient; 'struct stat' is probably better.
                std::ifstream in;
                
                in.open(filename);
                
                if (!in)
                    return false;
                
                return true;
            }
            
            
            
            std::string Extension(const std::string& filename)
            {
                auto pos = filename.rfind('.');
                
                if (pos == std::string::npos)
                    return "";
                
                return filename.substr(pos + 1);
            }
            
            
            void Copy(const std::string& source,
                      const std::string& target,
                      fail_if_already_exist do_fail_if_already_exist,
                      autocopy allow_autocopy,
                      const char* invoking_file, int invoking_line)
            {
                // Handle separately the case source and target are identical: I'm not positive Yuni handles it
                // properly.
                if (source == target)
                {
                    switch (allow_autocopy)
                    {
                        case autocopy::yes:
                            return;
                        case autocopy::no:
                            throw Exception("Trying to copy file \"" + source + "\" onto itself!",
                                            invoking_file, invoking_line);
                    }
                }
                
                const auto boost_option =
                    do_fail_if_already_exist == fail_if_already_exist::yes
                    ? boost::filesystem::copy_option::fail_if_exists
                    : boost::filesystem::copy_option::overwrite_if_exists;
                
                try
                {
                    boost::filesystem::path source_path(source);
                    boost::filesystem::path target_path(target);

                    boost::filesystem::copy_file(source_path, target_path, boost_option);
                }
                catch (const boost::filesystem::filesystem_error& e)
                {
                    std::ostringstream oconv;
                    oconv << boost::diagnostic_information(e);
                    throw Exception(oconv.str(), __FILE__, __LINE__);
                }
            }
            
            
            void Concatenate(const std::vector<std::string>& file_list,
                             const std::string& amalgated_file,
                             const char* invoking_file, int invoking_line)
            {
                std::ofstream out;
                
                Create(out, amalgated_file, invoking_file, invoking_line);
                
                std::ifstream in;
                std::string line;

                for (const auto& input_file : file_list)
                {
                    Read(in, input_file, invoking_file, invoking_line);
                    
                    while (getline(in, line))
                        out << line << std::endl;
                    
                    in.close();
                }
            }
            
            
            bool AreEquals(const std::string& lhs,
                           const std::string& rhs,
                           const char* invoking_file, int invoking_line)
            {
                std::ifstream lhs_stream, rhs_stream;
                
                // Load the stream, and check both exists.
                Read(lhs_stream, lhs, invoking_file, invoking_line);
                Read(rhs_stream, rhs, invoking_file, invoking_line);
                
                if (lhs == rhs) // if exact same file content is the same!
                    return true;
                
                std::ifstream::pos_type size1, size2;
                
                size1 = lhs_stream.seekg(0, std::ifstream::end).tellg();
                lhs_stream.seekg(0, std::ifstream::beg);
                
                size2 = rhs_stream.seekg(0, std::ifstream::end).tellg();
                rhs_stream.seekg(0, std::ifstream::beg);
                
                if(size1 != size2)
                    return false;
                
                const auto block_size = 4096l;
                auto remaining = static_cast<long>(size1);
                
                while(remaining)
                {
                    char buffer1[block_size], buffer2[block_size];
                    auto size = std::min(block_size, remaining);
                    
                    lhs_stream.read(buffer1, size);
                    rhs_stream.read(buffer2, size);
                    
                    if (0 != memcmp(buffer1, buffer2, static_cast<std::size_t>(size)))
                        return false;
                    
                    remaining -= size;
                }
                
                return true;
            }
            
            
            
        } // namespace File
        
        
    } // namespace FilesystemNS
    
    
} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup
