///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 8 Apr 2014 14:47:39 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_NUMERIC_x_NUMERIC_HPP_
# define MOREFEM_x_UTILITIES_x_NUMERIC_x_NUMERIC_HPP_



# include <cassert>
# include <vector>
# include <limits>
# include <cmath>
# include <type_traits>


namespace MoReFEM
{


    namespace NumericNS
    {


        /*!
         * \brief When an  index is not known at object constructor, better assign a controlled dumb value
         * than not knowing what the compiler decided.
         *
         * In most cases (typically for instance the initialization of a class attribute) it is a good idea to
         * use it along with decltype:
         *
         * \code
         * MyClass::MyClass()
         * : index_(MoReFEM::NumericNS::UninitializedIndex<decltype(index_)>())
         * { }
         *
         * \endcode
         *
         * Doing so makes the code robust to a change of type: if some day index_ becomes a unsigned int rather than
         * an int the default value will keep being the greatest value it can reach.
         *
         * \return Known value when an index is not properly initialized (currently it is the highest possible value for
         * \a T).
         */
        template<class T>
        constexpr T UninitializedIndex() noexcept;


        //! Takes the Square of the value \a value
        template<class T>
        constexpr T Square(T value) noexcept;


        //! Takes the Cube of the value \a value
        template<class T>
        constexpr T Cube(T value) noexcept;


        //! Takes the 4-th power of the value \a value
        template<class T>
        constexpr T PowerFour(T value) noexcept;


        //! Returns value if value is positive or zero otherwise.
        template<class T>
        constexpr T AbsPlus(T value) noexcept;


        /*!
         * \brief Returns the sign of a value.
         *
         * \param[in] value Value which sign is evaluated.
         *
         * \return -1 if negative, 1 if positive, 0 if null (as determined by the \a IsZero function).
         */
        template<class T>
        constexpr T Sign(T value) noexcept;


        /*!
         * \brief Returns the true sign of a value.
         *
         * \param[in] value Value which sign is evaluated.
         *
         * This is directly lifted from HeartLab code; TrueSign is like the sign except zero is counted as positive.
         *
         * \return -1 if negative, 1 if positive or zero (as determined by the \a IsZero function).
         */
        template<class T>
        constexpr T TrueSign(T value) noexcept;

        /*!
         * \brief Heaviside function.
         *
         * \param[in] value Value which sign is evaluated.
         *
         * \tparam T A floating-point type.
         *
         * \return 0 if negative, 1 if positive, 0.5 if null (as determined by the \a IsZero function).
         */
        template<class T>
        constexpr T Heaviside(T value) noexcept;


        //! Returns a floating-point value that is small enough for most purposes (currently 1.e-15 is used here).
        template<class T>
        constexpr std::enable_if_t<std::is_floating_point<T>::value, T> DefaultEpsilon() noexcept;


        /*!
         * \brief Check whether a value is close enough to zero to be able to be considered as 0.
         *
         * \tparam T Floating point type considered (float, double or long double).
         *
         * \param[in] value Value that is tested.
         * \param[in] epsilon Epsilon used for the comparison. A default value is provided; but the parameter
         * is there if you want to play with it.
         *
         * \return True if \a value might be considered as null or not.
         */
        template<class T>
        std::enable_if_t<std::is_floating_point<T>::value, bool>
        IsZero(T value, T epsilon = DefaultEpsilon<T>()) noexcept;


        /*!
         * \brief Check whether a value is close enough to another.
         *
         * \tparam T Floating point type considered (float, double or long double).
         *
         * \param[in] lhs Lhs value.
         * \param[in] rhs Rhs value.
         * \param[in] epsilon Epsilon used for the comparison. A default value is provided; but the parameter
         * is there if you want to play with it.
         *
         * \return True if \a lhs and \a rhs are deemed to be close enough to be considered equal.
         */
        template<class T>
        std::enable_if_t<std::is_floating_point<T>::value, bool>
        AreEqual(T lhs, T rhs, T epsilon = DefaultEpsilon<T>()) noexcept;


    } // namespace NumericNS


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/Numeric/Numeric.hxx"


#endif // MOREFEM_x_UTILITIES_x_NUMERIC_x_NUMERIC_HPP_
