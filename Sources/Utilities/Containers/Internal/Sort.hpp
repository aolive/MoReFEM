///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_INTERNAL_x_SORT_HPP_
# define MOREFEM_x_UTILITIES_x_CONTAINERS_x_INTERNAL_x_SORT_HPP_


# include <cassert>
# include <tuple>


namespace MoReFEM
{


    namespace Internal
    {


        namespace SortNS
        {

//            *      static int Value(const T& element)
//            *      {
//                *          return T.GetDimension();
//                *      }

            /*!
             * \brief Compare whether two objects of same type are equal regarding one unary function.
             *
             * \tparam T Type of the objects involved.
             * \tparam UnaryT Structure which is expected to define a static unary function named
             * Value which sole parameter is a \a T object.
             * Typically UnaryT will be a trick to get the results of a method of T object.
             *
             * For instance:
             * \code{.cpp}
             struct Dimension
             {
                static int Value(const T& element)
                {
                    return T.GetDimension();
                }
             };

             T foo, bar;

             bool are_equal = IsEqual<T, Dimension>(foo, bar);
             //< actually performs foo.GetDimension() == bar.GetDimension();
             \endcode
             *
             * In the above example it may seem pointlessly complicated, but the highlight
             * is that it can be used in metaprogramming context (see for instance Sort
             * implementation).
             *
             *
             * \copydoc doxygen_hide_lhs_rhs_arg
             * \return True of both lhs and rhs are equal considering \a UnaryT function.
             */
            template<class T, class UnaryT>
            bool IsEqual(const T& lhs, const T& rhs);



            /*!
             * \brief Helper function for SortingCriterion.
             *
             * See SortingCriterion documentation to understand what is the goal.
             *
             * \tparam T Type of the objects to sort. Operators < and == must be defined for this type.
             * \tparam IndexT This index is going downward from the size of the tuple minus 1 to 0 (reason
             * for this awful trick is that to end the recursion I can use 0 but not std::tuple_size<TupleT>::value).
             * So SortHelper is first called with IndexT = Size - 1, which means in fact very first element
             * of the tuple. Then with Size - 2, which means second element of tuple... and so forth!
             */
            template <class T, int IndexT, class TupleT>
            struct SortHelper
            {

                //! Actual function (wrapped into a class for template specialization).
                static bool Compare(const T& lhs, const T& rhs);


            };


            // ============================
            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            // ============================


            //! Same as above, used to end the recursion.
            template <class T, class TupleT>
            struct SortHelper<T, 0, TupleT>
            {

                //! Actual function (wrapped into a class for template specialization).
                static bool Compare(const T& lhs, const T& rhs);


            };


            // ============================
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN
            // ============================



        } // namespace SortNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/Containers/Internal/Sort.hxx"


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_INTERNAL_x_SORT_HPP_
