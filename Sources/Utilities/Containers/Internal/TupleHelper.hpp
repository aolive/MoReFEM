///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Dec 2016 22:00:00 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_INTERNAL_x_TUPLE_HELPER_HPP_
# define MOREFEM_x_UTILITIES_x_CONTAINERS_x_INTERNAL_x_TUPLE_HELPER_HPP_

#include "Utilities/Numeric/Numeric.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace Tuple
        {


            /*!
             ** \brief helper class for IndexOf, which does much of the actual work
             **
             ** This is called recursively in IndexOf until the class is found or the
             ** list has been completely checked
             **
             ** \tparam T Class looked at in the tuple
             ** \tparam TupleT std::tuple which is investigated
             ** \tparam I Index of the current element index in the typelist
             */
            template<class T, class TupleT, unsigned int I>
            struct IndexOfHelper
            {
            private:

                //! Type if the \a I -th element of the \a TupleT.
                using type = typename std::tuple_element<I, TupleT>::type;

                //! Enum used to end the recursion.
                enum { found = std::is_same<T, type>::value };

            public:

                // Static_cast to avoid gcc warning about enumeral/non enumeral
                enum { value = (found == 1 ? static_cast<unsigned int>(I)
                                : static_cast<unsigned int>(IndexOfHelper<T, TupleT, I-1>::value)) };
            };


            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            template<class T, class TupleT>
            struct IndexOfHelper<T, TupleT, 0>
            {
                using type = typename std::tuple_element<0, TupleT>::type;
                enum { value = (std::is_same<T, type>::value == 1 ? 0u : NumericNS::UninitializedIndex<unsigned int>()) };
            };
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            namespace Impl
            {


                /*!
                 * \brief Check that elements at \a I -th and \a J -th position in \a TupleT don't share the same
                 * type.
                 *
                 * \tparam TupleT Tuple being checked.
                 * \tparam I Index of the first element in the comparison.
                 * \tparam J Index of the second element in the comparison. By construct should be higher than I.
                 * \tparam TupleSizeT Size of the tuple; it is const but required nonetheless to implement the
                 * specialization that ends the recursion.
                 */
                template<class TupleT, unsigned int I, unsigned int J, unsigned int TupleSizeT>
                struct CompareRemainingToI
                {


                    //! Type of the \a I -th element in the tuple.
                    using TypeI = typename std::tuple_element<I, TupleT>::type;

                    //! Type of the \a J -th element in the tuple.
                    using TypeJ = typename std::tuple_element<J, TupleT>::type;

                    //! Static function that does the actual work.
                    static void Perform()
                    {
                        static_assert(I < J, "If not ill-defined call to this static method!");
                        static_assert(J != TupleSizeT, "Equality should be handled in a specialization.");
                        static_assert(J < TupleSizeT, "If not ill-defined call to this static method!");

                        static_assert(!std::is_same<TypeI, TypeJ>::value,
                                      "No type should be present twice in this tuple!");

                        CompareRemainingToI<TupleT, I, J + 1, TupleSizeT>::Perform();
                    }


                };


                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                template<class TupleT, unsigned int I, unsigned int TupleSizeT>
                struct CompareRemainingToI<TupleT, I, TupleSizeT, TupleSizeT>
                {


                    static void Perform()
                    {
                        // End recursion.
                    }

                };
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            } // namespace Impl


            /*!
             * \brief Check that all the types in \a TupleT after \a I are not the same type as \a I.
             *
             * \tparam TupleT Tuple being checked.
             * \tparam I Index of the tuple element against which all the following will be compared.
             * \tparam TupleSizeT Size of the tuple; it is const but required nonetheless to implement the
             * specialization that ends the recursion.
             *
             */
            template<class TupleT, unsigned int I, unsigned int TupleSizeT>
            struct AssertNoDuplicateHelper
            {
                //! Method which does the actual work.
                static void Perform()
                {
                    Impl::CompareRemainingToI<TupleT, I, I + 1, TupleSizeT>::Perform();

                    AssertNoDuplicateHelper<TupleT, I + 1, TupleSizeT>::Perform();
                }
            };


            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            template<class TupleT, unsigned int TupleSizeT>
            struct AssertNoDuplicateHelper<TupleT, TupleSizeT, TupleSizeT>
            {

                static void Perform()
                {
                    // Just end recursion.
                }

            };
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN



            /*!
             * \brief Helper class to determine whether all containers held in \a TupleT are the same size.
             *
             * \tparam TupleT Tuple investigated.
             * \tparam Index Index of the tuple element currently scrutinized.
             * \tparam LengthMinusOne Length of the tuple minus one.
             */
            template<class TupleT, unsigned int Index, unsigned int LengthMinusOne>
            struct AreEqualLengthHelper
            {

                /*!
                 * \brief The methods that performs the actual work.
                 */
                static bool Check(const TupleT& tuple)
                {
                    if (std::get<Index>(tuple).size() == std::get<Index + 1>(tuple).size())
                        return AreEqualLengthHelper<TupleT, Index + 1, LengthMinusOne>::Check(tuple);

                    return false;
                }


            };


            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            template<class TupleT, unsigned int LengthMinusOne>
            struct AreEqualLengthHelper<TupleT, LengthMinusOne, LengthMinusOne>
            {

                static bool Check(const TupleT& tuple)
                {
                    // End recursion.
                    return (std::get<LengthMinusOne>(tuple).size() == std::get<LengthMinusOne + 1>(tuple).size());
                }


            };
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN



            /*!
             * \brief Helper function for Utilities::Tuple::CallFunctionWithArgumentsFromTuple.
             *
             * See Utilities::Tuple::CallFunctionWithArgumentsFromTuple for an explanation for this
             * very complicated class, which is intended to be used only at the lowest-level of
             * MoReFEM.
             */
            template
            <
                typename ReturnTypeT,
                typename FunctionPrototypeT,
                typename TupleT,
                bool IsDoneT,
                unsigned int NvalueInTupleT,
                std::size_t... ArgumentIteratorT
            >
            struct CallFunctionWithArgumentsFromTuple
            {


                //! Static function that does the actual work.
                static ReturnTypeT Perform(FunctionPrototypeT& f, TupleT&& tuple)
                {
                    return CallFunctionWithArgumentsFromTuple
                    <
                        ReturnTypeT,
                        FunctionPrototypeT,
                        TupleT,
                        NvalueInTupleT == 1 + sizeof...(ArgumentIteratorT),
                        NvalueInTupleT,
                        ArgumentIteratorT...,
                        sizeof...(ArgumentIteratorT)
                    >::Perform(f, std::forward<TupleT>(tuple));
                }


            };


            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            template
            <
                typename ReturnTypeT,
                typename FunctionPrototypeT,
                typename TupleT,
                unsigned int NvalueInTupleT,
                std::size_t... ArgumentIteratorT
            >
            struct CallFunctionWithArgumentsFromTuple
            <
                ReturnTypeT,
                FunctionPrototypeT,
                TupleT,
                true,
                NvalueInTupleT,
                ArgumentIteratorT...
            >
            {


                //! Static function that does the actual work.
                static ReturnTypeT Perform(FunctionPrototypeT& f, TupleT&& tuple)
                {
                    return f(std::get<ArgumentIteratorT>(std::forward<TupleT>(tuple))...);
                }


            };

            // \todo #1081
            // Until C++ 17 is around: std::invoke and std::apply will reduce need for the CallXXXWithArgumentsFromTuple
            // functionalities! See https://isocpp.org/wiki/faq/pointers-to-members#typedef-for-ptr-to-memfn for
            // this CALL_MEMBER_FN!
            #define CALL_MEMBER_FN(object,ptrToMember)  ((object).*(ptrToMember))

            template
            <
                typename ReturnTypeT,
                typename FunctionPrototypeT,
                typename TupleT,
                bool IsDoneT,
                unsigned int NvalueInTupleT,
                std::size_t... ArgumentIteratorT
            >
            struct CallMethodWithArgumentsFromTuple
            {


                //! Static function that does the actual work.
                template<class ObjectT>
                static ReturnTypeT Perform(ObjectT& object, FunctionPrototypeT& f, TupleT&& tuple)
                {
                    return CallMethodWithArgumentsFromTuple
                    <
                        ReturnTypeT,
                        FunctionPrototypeT,
                        TupleT,
                        NvalueInTupleT == 1 + sizeof...(ArgumentIteratorT),
                        NvalueInTupleT,
                        ArgumentIteratorT...,
                        sizeof...(ArgumentIteratorT)
                    >::Perform(object, f, std::forward<TupleT>(tuple));
                }


            };



            template
            <
                typename ReturnTypeT,
                typename FunctionPrototypeT,
                typename TupleT,
                unsigned int NvalueInTupleT,
                std::size_t... ArgumentIteratorT
            >
            struct CallMethodWithArgumentsFromTuple
            <
                ReturnTypeT,
                FunctionPrototypeT,
                TupleT,
                true,
                NvalueInTupleT,
                ArgumentIteratorT...
            >
            {


                //! Static function that does the actual work.
                template<class ObjectT>
                static ReturnTypeT Perform(ObjectT& object, FunctionPrototypeT& f, TupleT&& tuple)
                {
                    return CALL_MEMBER_FN(object, f)(std::get<ArgumentIteratorT>(std::forward<TupleT>(tuple))...);
                }


            };
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            /*!
             * \brief Helper struct used to build the underlying type of Utilities::Tuple::Decay.
             *
             * \tparam OriginalTupleT Original tuple, for which each element type should be decayed.
             * \tparam DecayedTupleUnderConstructT Current progress of the new tuple type. Current instantiation
             * has to add the type of the \a I -th element.
             * \tparam I Index of the element type to be added.
             * \tparam TupleSizeT The result of std::tuple_size<OriginalTupleT>::value.
             */
            template
            <
                class OriginalTupleT,
                class DecayedTupleUnderConstructT,
                unsigned int I,
                unsigned int TupleSizeT
            >
            struct Decay
            {
            private:

                //! Type of I-th element of OriginalTupleT.
                using item_type = typename std::tuple_element<I, OriginalTupleT>::type;

                //! Decayed type of \a item_type (const, references and pointers are scrapped).
                using item_decayed_type = std::decay_t<item_type>;

                //! Artificial tuple that includes only one type: \a item_decayed_type.
                using tuple_new_item = std::tuple<item_decayed_type>;

                /*!
                 * \brief Static method that returns a \a tuple_new_item object.
                 *
                 * \internal <b><tt>[internal]</tt></b> This method is ONLY declarative, there is no definition
                 * provided on purpose. The use of it is exclusively in \a new_tuple_type_so_far, where it is used
                 * to derive the type of the new tuple up to the I-th element.  It is important to understand that
                 * actually no \a tuple_new_item objects are created at all at any point! See Modern C++ Design
                 * from Andrei Alexandrescu, chapter 1, if you want to see exactly the pattern used here.
                 *
                 * \return Object which only interest is its type (see explanation in the note above).
                 */
                static tuple_new_item MakeTupleCurrentDecayedItem();

                //! Similar to MakeItem().
                static DecayedTupleUnderConstructT MakeNewTuple();

                //! Tuple type that basically includes DecayedTupleUnderConstructT + decayed type of I-th element.
                using new_tuple_type_so_far = decltype(std::tuple_cat(MakeNewTuple(), MakeTupleCurrentDecayedItem()));

            public:

                //! Recursively calls the next tuple element to completely build the decayed tuple.
                using type = typename Decay<OriginalTupleT, new_tuple_type_so_far, I + 1, TupleSizeT>::type;

            };


            /*!
             * \brief Stopping point of the helper struct used to build the underlying type of Utilities::Tuple::Decay.
             *
             * \tparam OriginalTupleT Original tuple, for which each element type should be decayed.
             * \tparam DecayedTupleT When this last intantiation is called, this should actually yield the type
             * Utilities::Tuple::Decay requires.
             * \tparam TupleSizeT The result of std::tuple_size<OriginalTupleT>::value.
             */
            template
            <
                class OriginalTupleT,
                class DecayedTupleT,
                unsigned int TupleSizeT
            >
            struct Decay<OriginalTupleT, DecayedTupleT, TupleSizeT, TupleSizeT>
            {

                //! Type required by Utilities::Tuple::Decay.
                using type = DecayedTupleT;


            };


        } // namespace Tuple


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup



#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_INTERNAL_x_TUPLE_HELPER_HPP_
