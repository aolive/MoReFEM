///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 5 Nov 2015 11:00:08 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_ARRAY_HXX_
# define MOREFEM_x_UTILITIES_x_CONTAINERS_x_ARRAY_HXX_


namespace MoReFEM
{


    namespace Utilities
    {



        template<class ItemPtrT, std::size_t N>
        std::array<ItemPtrT, N> NullptrArray()
        {
            static_assert(IsSharedPtr<ItemPtrT>() || std::is_pointer<ItemPtrT>() || IsUniquePtr<ItemPtrT>(),
                          "ItemPtrT must behaves like a pointer!");

            std::array<ItemPtrT, N> ret;

            for (auto& item : ret)
                item = nullptr;

            return ret;
        }


        template<class T, std::size_t N>
        constexpr std::size_t ArraySize<std::array<T, N>>::GetValue() noexcept
        {
            return N;
        }


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_ARRAY_HXX_
