///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 18 Mar 2016 17:07:38 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_UPDATE_FIBER_DEFORMATION_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_UPDATE_FIBER_DEFORMATION_HXX_


namespace MoReFEM
{


    namespace GlobalParameterOperatorNS
    {


        inline void UpdateFiberDeformation::Update(const GlobalVector& displacement_increment) const
        {
            return parent::UpdateImpl(displacement_increment);
        }


        inline void UpdateFiberDeformation
        ::SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                      LocalParameterOperator& local_operator,
                                      const std::tuple<const GlobalVector&>& additional_arguments) const
        {
            GlobalVariationalOperatorNS::ExtractLocalDofValues(local_felt_space,
                                                               this->GetExtendedUnknown(),
                                                               std::get<0>(additional_arguments),
                                                               local_operator.GetNonCstIncrementLocalDisplacement());
        }


    } // namespace GlobalParameterOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_UPDATE_FIBER_DEFORMATION_HXX_
