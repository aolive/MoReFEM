///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 18 Mar 2016 17:07:38 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_TENSOR_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_TENSOR_HPP_

# include "Parameters/Parameter.hpp"
# include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

# include "Operators/GlobalVariationalOperator/ExtractLocalDofValues.hpp"

# include "Operators/ParameterOperator/GlobalParameterOperator/GlobalParameterOperator.hpp"
# include "OperatorInstances/ParameterOperator/Local/UpdateCauchyGreenTensor.hpp"


namespace MoReFEM
{


    namespace GlobalParameterOperatorNS
    {


        /*!
         * \brief Implementation of global \a UpdateCauchyGreenTensor operator.
         */
        class UpdateCauchyGreenTensor final
        : public GlobalParameterOperator
        <
            UpdateCauchyGreenTensor,
            LocalParameterOperatorNS::UpdateCauchyGreenTensor,
            ParameterNS::Type::vector
        >
        {

        public:

            //! Alias to unique pointer.
            using const_unique_ptr = std::unique_ptr<const UpdateCauchyGreenTensor>;

            //! Returns the name of the operator.
            static const std::string& ClassName();

            //! Convenient alias to pinpoint the GlobalParameterOperator parent.
            using parent = GlobalParameterOperator
            <
                UpdateCauchyGreenTensor,
                LocalParameterOperatorNS::UpdateCauchyGreenTensor,
                ParameterNS::Type::vector
            >;

            //! Friendship to the parent class so that the CRTP can reach private methods defined below.
            friend parent;

        public:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] felt_space Finite element space upon which the operator is defined.
             * \param[in] unknown Unknown considered for this operator (might be vector or vectorial).
             * \param[in,out] cauchy_green_tensor Cauchy Green tensor which is to be updated by present operator.
             * Size of a local vector inside must be 3 for a 2D mesh and 6 for a 3D one.
             * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
             */
            explicit UpdateCauchyGreenTensor(const FEltSpace& felt_space,
                                             const Unknown& unknown,
                                             ParameterAtQuadraturePoint<ParameterNS::Type::vector, ParameterNS::TimeDependencyNS::None>& cauchy_green_tensor,
                                             const QuadratureRulePerTopology* const quadrature_rule_per_topology);

            //! Destructor.
            ~UpdateCauchyGreenTensor() = default;

            //! Copy constructor.
            UpdateCauchyGreenTensor(const UpdateCauchyGreenTensor&) = default;

            //! Move constructor.
            UpdateCauchyGreenTensor(UpdateCauchyGreenTensor&&) = default;

            //! Copy affectation.
            UpdateCauchyGreenTensor& operator=(const UpdateCauchyGreenTensor&) = default;

            //! Move affectation.
            UpdateCauchyGreenTensor& operator=(UpdateCauchyGreenTensor&&) = default;

            ///@}


            /*!
             * \brief Assemble into one or several vectors.
             *
             * \param[in] current_solid_displacement The current solid displacement (iteration {n}) used to compute the
             * tensor.
             */
            void Update(const GlobalVector& current_solid_displacement) const;


        public:

            /*!
             * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
             * a tuple.
             *
             * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
             * variable is actually set in this call.
             * \param[in] local_felt_space List of finite elements being considered; all those related to the
             * same GeometricElt.
             * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
             * Here there is one:
             * - The current solid displacement (iteration {n}) used to compute the tensor.
             *
             */
            void
            SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                        LocalParameterOperator& local_operator,
                                        const std::tuple<const GlobalVector&>& additional_arguments) const;


        };


    } // namespace GlobalParameterOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/ParameterOperator//UpdateCauchyGreenTensor.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_TENSOR_HPP_
