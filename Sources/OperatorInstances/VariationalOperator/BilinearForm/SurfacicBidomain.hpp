///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 May 2014 15:57:53 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_SURFACIC_BIDOMAIN_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_SURFACIC_BIDOMAIN_HPP_

#include "ParameterInstances/Fiber/FiberList.hpp"
#include "ParameterInstances/Fiber/Internal/FiberListManager.hpp"

# include "Operators/GlobalVariationalOperator/GlobalVariationalOperator.hpp"

# include "OperatorInstances/VariationalOperator/BilinearForm/Local/SurfacicBidomain.hpp"


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {



        /*!
         * \brief Operator description.
         *
         * \todo #9 Describe operator!
         */
        class SurfacicBidomain final : public GlobalVariationalOperatorNS::SameForAllRefGeomElt
        <
            SurfacicBidomain,
            Advanced::OperatorNS::Nature::bilinear,
            Advanced::LocalVariationalOperatorNS::SurfacicBidomain
        >
        {

        public:

            //! \copydoc doxygen_hide_alias_self
            using self = SurfacicBidomain;

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

            //! Returns the name of the operator.
            static const std::string& ClassName();

            //! Alias to local operator.
            using local_operator_type = Advanced::LocalVariationalOperatorNS::SurfacicBidomain;

            //! Convenient alias to pinpoint the GlobalVariationalOperator parent.
            using parent = GlobalVariationalOperatorNS::SameForAllRefGeomElt
            <
                self,
                Advanced::OperatorNS::Nature::bilinear,
                local_operator_type
            >;

            //! Friendship to the parent class so that the CRTP can reach private methods defined below.
            friend parent;

            //! Unique ptr.
            using const_unique_ptr = std::unique_ptr<const SurfacicBidomain>;

            //! Alias to relevant scalar parameter type used in this operator.
            using scalar_parameter = local_operator_type::scalar_parameter;

        public:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] unknown_list Unknows list. \todo #9 Complete!
             * \copydoc doxygen_hide_test_unknown_list_param
             * \param[in] intracellular_trans_diffusion_tensor Intracellular trans diffusion tensor.
             * \param[in] extracellular_trans_diffusion_tensor Extracellular trans diffusion tensor.
             * \param[in] intracellular_fiber_diffusion_tensor Intracellular fiber diffusion tensor.
             * \param[in] extracellular_fiber_diffusion_tensor Extracellular fiber diffusion tensor.
             * \param[in] heterogeneous_conductivity_coefficient Heterogeneous conductivity coefficient.
             * \param[in] fibers Fibers.
             * \param[in] angles Angles. \todo #9 Should be completed.
             * \copydoc doxygen_hide_gvo_felt_space_arg
             * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
             */
            explicit SurfacicBidomain(const FEltSpace& felt_space,
                                      const std::array<Unknown::const_shared_ptr, 2>& unknown_list,
                                      const std::array<Unknown::const_shared_ptr, 2>& test_unknown_list,
                                      const scalar_parameter& intracellular_trans_diffusion_tensor,
                                      const scalar_parameter& extracellular_trans_diffusion_tensor,
                                      const scalar_parameter& intracellular_fiber_diffusion_tensor,
                                      const scalar_parameter& extracellular_fiber_diffusion_tensor,
                                      const scalar_parameter& heterogeneous_conductivity_coefficient,
                                      const FiberList<ParameterNS::Type::vector>& fibers,
                                      const FiberList<ParameterNS::Type::scalar>& angles,
                                      const QuadratureRulePerTopology* const quadrature_rule_per_topology = nullptr);

            //! Destructor.
            ~SurfacicBidomain() = default;

            //! Copy constructor.
            SurfacicBidomain(const SurfacicBidomain&) = delete;

            //! Move constructor.
            SurfacicBidomain(SurfacicBidomain&&) = delete;

            //! Copy affectation.
            SurfacicBidomain& operator=(const SurfacicBidomain&) = delete;

            //! Move affectation.
            SurfacicBidomain& operator=(SurfacicBidomain&&) = delete;

            ///@}


        public:

            // TODO Replace matrix by vector if relevant.
            /*!
             * \brief Assemble into one or several matrices.
             *
             * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixWithCoefficient objects.
             *
             * \param[in] global_matrix_with_coeff_list List of global matrices into which the operator is
             * assembled. These matrices are assumed to be already properly allocated.
             * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
             * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
             * element space; if current \a domain is not a subset of finite element space one, assembling will occur
             * upon the intersection of both.
             *
             */
            template<class LinearAlgebraTupleT>
            void Assemble(LinearAlgebraTupleT&& global_matrix_with_coeff_list, const Domain& domain = Domain()) const;


        };


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/VariationalOperator/BilinearForm/SurfacicBidomain.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_SURFACIC_BIDOMAIN_HPP_
