///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 May 2014 15:57:53 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_GRAD_ON_GRADIENT_BASED_ELASTICITY_TENSOR_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_GRAD_ON_GRADIENT_BASED_ELASTICITY_TENSOR_HXX_


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        template<class LinearAlgebraTupleT>
        inline void GradOnGradientBasedElasticityTensor
        ::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple, const Domain& domain) const
        {
            return parent::template AssembleImpl<>(std::move(linear_algebra_tuple), domain);
        }



    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_GRAD_ON_GRADIENT_BASED_ELASTICITY_TENSOR_HXX_
