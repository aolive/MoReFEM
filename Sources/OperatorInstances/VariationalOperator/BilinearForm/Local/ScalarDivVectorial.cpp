///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 8 Sep 2014 12:16:31 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#include "ThirdParty/Wrappers/Seldon/SeldonFunctions.hpp"

#ifdef MOREFEM_CHECK_NAN_AND_INF
# include "ThirdParty/Wrappers/Seldon/MatrixOperations.hpp"
#endif // MOREFEM_CHECK_NAN_AND_INF

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/ScalarDivVectorial.hpp"


namespace MoReFEM
{
    
    
    namespace Advanced
    {


    namespace LocalVariationalOperatorNS
    {
        
        
        
        ScalarDivVectorial::ScalarDivVectorial(const ExtendedUnknown::vector_const_shared_ptr& a_unknown_storage,
                                               const ExtendedUnknown::vector_const_shared_ptr& a_test_unknown_storage,
                                               elementary_data_type&& a_elementary_data,
                                               double alpha,
                                               double beta)
        : BilinearLocalVariationalOperator(a_unknown_storage, a_test_unknown_storage, std::move(a_elementary_data)),
        alpha_(alpha),
        beta_(beta)
        {
            #ifndef NDEBUG
            {
                const auto& unknown_storage = this->GetExtendedUnknownList();
                assert(unknown_storage.size() == 2ul && "Namesake GlobalVariationalOperator should provide the correct one.");
                assert(!(!unknown_storage.front()));
                assert("Namesake GlobalVariationalOperator should provide two unknowns, the first being vectorial"
                       && unknown_storage.front()->GetUnknown().GetNature() == UnknownNS::Nature::vectorial);
                assert(!(!unknown_storage.back()));
                assert("Namesake GlobalVariationalOperator should provide two unknowns, the second being scalar"
                       && unknown_storage.back()->GetUnknown().GetNature() == UnknownNS::Nature::scalar);
                
                const auto& test_unknown_storage = this->GetExtendedTestUnknownList();
                assert(test_unknown_storage.size() == 2ul && "Namesake GlobalVariationalOperator should provide the correct one.");
                assert(!(!test_unknown_storage.front()));
                assert("Namesake GlobalVariationalOperator should provide two test unknowns, the first being vectorial"
                       && test_unknown_storage.front()->GetUnknown().GetNature() == UnknownNS::Nature::vectorial);
                assert(!(!test_unknown_storage.back()));
                assert("Namesake GlobalVariationalOperator should provide two test unknowns, the second being scalar"
                       && test_unknown_storage.back()->GetUnknown().GetNature() == UnknownNS::Nature::scalar);
            }
            #endif // NDEBUG            
        }
        
        
        ScalarDivVectorial::~ScalarDivVectorial() = default;
        
        
        const std::string& ScalarDivVectorial::ClassName()
        {
            static std::string name("ScalarDivVectorial");
            return name;
        }
        
        
        void ScalarDivVectorial::ComputeEltArray()
        {
            auto& elementary_data = GetNonCstElementaryData();

            auto& matrix_result = elementary_data.GetNonCstMatrixResult();
                        
            const auto& scalar_ref_felt =
                elementary_data.GetRefFElt(GetNthUnknown(EnumUnderlyingType(UnknownIndex::scalar)));
            const auto& vectorial_ref_felt =
                elementary_data.GetRefFElt(GetNthUnknown(EnumUnderlyingType(UnknownIndex::vectorial)));
            
            const auto& scalar_test_ref_felt =
                elementary_data.GetTestRefFElt(GetNthTestUnknown(EnumUnderlyingType(TestUnknownIndex::scalar)));
            const auto& vectorial_test_ref_felt =
                elementary_data.GetTestRefFElt(GetNthTestUnknown(EnumUnderlyingType(TestUnknownIndex::vectorial)));
            
            const auto Nnode_velocity = static_cast<int>(vectorial_ref_felt.Nnode());
            const auto Ndof_velocity = static_cast<int>(vectorial_ref_felt.Ndof());
            const auto Nnode_pressure = static_cast<int>(scalar_ref_felt.Nnode());
            
            const auto Nnode_test_velocity = static_cast<int>(vectorial_test_ref_felt.Nnode());
            const auto Ndof_test_velocity = static_cast<int>(vectorial_test_ref_felt.Ndof());
            const auto Nnode_test_pressure = static_cast<int>(scalar_test_ref_felt.Nnode());
            
            const auto Ncomponent = static_cast<int>(vectorial_ref_felt.Ncomponent());
            
            assert(Ncomponent == static_cast<int>(vectorial_test_ref_felt.Ncomponent()));
            
            const auto& infos_at_quad_pt_list_for_unknown = elementary_data.GetInformationsAtQuadraturePointListForUnknown();
            const auto& infos_at_quad_pt_list_for_test_unknown = elementary_data.GetInformationsAtQuadraturePointListForTestUnknown();
            
            assert(infos_at_quad_pt_list_for_unknown.size() == infos_at_quad_pt_list_for_test_unknown.size());
            
            const double alpha = GetAlpha();
            const double beta = GetBeta();
            
            assert(infos_at_quad_pt_list_for_unknown.size() == infos_at_quad_pt_list_for_test_unknown.size());
            const auto end_infos_at_quad_pt = infos_at_quad_pt_list_for_unknown.cend();

            for (auto it_infos_at_quad_pt = infos_at_quad_pt_list_for_unknown.cbegin(),
                 it_infos_at_quad_pt_test = infos_at_quad_pt_list_for_test_unknown.cbegin();
                 it_infos_at_quad_pt != end_infos_at_quad_pt;
                 ++it_infos_at_quad_pt, ++it_infos_at_quad_pt_test)
            {
                const auto& infos_at_quad_pt_for_unknown = *it_infos_at_quad_pt;
                const auto& infos_at_quad_pt_for_test_unknown = *it_infos_at_quad_pt_test;
                
                const auto& felt_phi = infos_at_quad_pt_for_unknown.GetFEltPhi(); // on (v p)
                const auto& gradient_felt_phi = infos_at_quad_pt_for_unknown.GetGradientFEltPhi(); // on (v p)
                
                const auto& test_felt_phi = infos_at_quad_pt_for_test_unknown.GetFEltPhi(); // on (v* p*)
                const auto& test_gradient_felt_phi = infos_at_quad_pt_for_test_unknown.GetGradientFEltPhi(); // on (v* p*)
                
                const auto& pressure_felt_phi = ExtractSubVector(felt_phi,
                                                                 scalar_ref_felt); // only on p
                
                const auto& test_pressure_felt_phi = ExtractSubVector(test_felt_phi,
                                                                      scalar_test_ref_felt); // only on p*
                
                // Example to show an another way of getting gradient_felt_phi on only one unknown.
//                const auto& velocity_gradient = ExtractSubMatrix(gradient_felt_phi,
//                                                                 vectorial_ref_felt); // only on v
//
//                const auto& test_velocity_gradient = ExtractSubMatrix(test_gradient_felt_phi,
//                                                                      vectorial_test_ref_felt); // only on v*
                
                const double factor = infos_at_quad_pt_for_unknown.GetQuadraturePoint().GetWeight()
                                    * infos_at_quad_pt_for_unknown.GetAbsoluteValueJacobianDeterminant();
                
                assert(infos_at_quad_pt_for_unknown.GetQuadraturePoint().GetRuleName() ==
                       infos_at_quad_pt_for_test_unknown.GetQuadraturePoint().GetRuleName());
                
                // Part in (v* p)
                for (int node_test_velocity_index = 0; node_test_velocity_index < Nnode_test_velocity; ++node_test_velocity_index)
                {
                    int dof_test_velocity_index = node_test_velocity_index;
                    
                    for (int node_pressure_index = 0; node_pressure_index < Nnode_pressure; ++node_pressure_index)
                    {
                        const int& dof_pressure = node_pressure_index; // alias
         
                        // The two terms below give the same result but in a different manner. It is just here to show how to use them.
                        //const auto test_pressure_term = test_felt_phi(Nnode_test_velocity + node_test_pressure_index);
                        const auto pressure_term = pressure_felt_phi(node_pressure_index);
                        
                        for (int component = 0; component < Ncomponent; ++component, dof_test_velocity_index += Nnode_test_velocity)
                        {
                            const double product =
                                factor * pressure_term * test_gradient_felt_phi(node_test_velocity_index, component);
                            
                            matrix_result(dof_test_velocity_index, Ndof_velocity + dof_pressure) += beta * product;
                        }
                    }
                }
                
                // Part in (p* v)
                for (int node_test_pressure_index = 0; node_test_pressure_index < Nnode_test_pressure; ++node_test_pressure_index)
                {
                    const int& dof_test_pressure = node_test_pressure_index; // alias
                    
                    // The two terms below give the same result but in a different manner. It is just here to show how to use them.
                    // const auto pressure_term = felt_phi(Nnode_velocity + node_pressure_index);
                    const auto test_pressure_term = test_pressure_felt_phi(node_test_pressure_index);
                    
                    for (int node_velocity_index = 0; node_velocity_index < Nnode_velocity; ++node_velocity_index)
                    {
                        int dof_velocity_index = node_velocity_index;
                        
                        for (int component = 0; component < Ncomponent; ++component, dof_velocity_index += Nnode_velocity)
                        {
                            const double product =
                                factor * test_pressure_term * gradient_felt_phi(node_velocity_index, component);
                            
                            matrix_result(Ndof_test_velocity + dof_test_pressure, dof_velocity_index) += alpha * product;
                        }
                    }
                }
            }            
        }
        

    } // namespace LocalVariationalOperatorNS
        
        
    } // namespace Advanced
    
    
} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
