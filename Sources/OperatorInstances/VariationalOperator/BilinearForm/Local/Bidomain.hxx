///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 1 Sep 2015 14:09:59 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_BIDOMAIN_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_BIDOMAIN_HXX_


namespace MoReFEM
{


    namespace Advanced
    {


    namespace LocalVariationalOperatorNS
    {


        inline const Bidomain::scalar_parameter& Bidomain::GetIntracelluarTransDiffusionTensor() const noexcept
        {
            return intracellular_trans_diffusion_tensor_;
        }

        inline const Bidomain::scalar_parameter& Bidomain::GetExtracelluarTransDiffusionTensor() const noexcept
        {
            return extracellular_trans_diffusion_tensor_;
        }


        inline const Bidomain::scalar_parameter& Bidomain::GetIntracelluarFiberDiffusionTensor() const noexcept
        {
            return intracellular_fiber_diffusion_tensor_;
        }

        inline const Bidomain::scalar_parameter& Bidomain::GetExtracelluarFiberDiffusionTensor() const noexcept
        {
            return extracellular_fiber_diffusion_tensor_;
        }

        inline const FiberList<ParameterNS::Type::vector>& Bidomain::GetFibers() const noexcept
        {
            return fibers_;
        }


    } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_BIDOMAIN_HXX_
