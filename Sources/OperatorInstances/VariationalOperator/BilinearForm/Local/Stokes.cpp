///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 May 2015 10:21:36 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#include "ThirdParty/Wrappers/Seldon/SeldonFunctions.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/Stokes.hpp"


namespace MoReFEM
{
    
    
    namespace Advanced
    {
    
    
    namespace LocalVariationalOperatorNS
    {
        
        
        Stokes::Stokes(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                       const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                       elementary_data_type&& a_elementary_data,
                       const scalar_parameter& fluid_viscosity)
        : BilinearLocalVariationalOperator(unknown_list, test_unknown_list, std::move(a_elementary_data)),
        matrix_parent(),
        fluid_viscosity_(fluid_viscosity)
        {
            
            #ifndef NDEBUG
            {
                const auto& unknown_storage = this->GetExtendedUnknownList();
                assert(unknown_storage.size() == 2ul && "Namesake GlobalVariationalOperator should provide the correct one.");
                assert(!(!unknown_storage.front()));
                assert("Namesake GlobalVariationalOperator should provide two unknowns, the first being vectorial"
                       && unknown_storage.front()->GetUnknown().GetNature() == UnknownNS::Nature::vectorial);
                assert(!(!unknown_storage.back()));
                assert("Namesake GlobalVariationalOperator should provide two unknowns, the second being scalar"
                       && unknown_storage.back()->GetUnknown().GetNature() == UnknownNS::Nature::scalar);
                
                const auto& test_unknown_storage = this->GetExtendedTestUnknownList();
                assert(test_unknown_storage.size() == 2ul && "Namesake GlobalVariationalOperator should provide the correct one.");
                assert(!(!test_unknown_storage.front()));
                assert("Namesake GlobalVariationalOperator should provide two test unknowns, the first being vectorial"
                       && test_unknown_storage.front()->GetUnknown().GetNature() == UnknownNS::Nature::vectorial);
                assert(!(!test_unknown_storage.back()));
                assert("Namesake GlobalVariationalOperator should provide two test unknowns, the second being scalar"
                       && test_unknown_storage.back()->GetUnknown().GetNature() == UnknownNS::Nature::scalar);
            }
            #endif // NDEBUG
            
            const auto& elementary_data = GetElementaryData();
            const auto& velocity_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(EnumUnderlyingType(UnknownIndex::vectorial)));
            const int Nnode_for_velocity = static_cast<int>(velocity_ref_felt.Nnode());
            
            const auto& test_velocity_ref_felt =
                elementary_data.GetTestRefFElt(GetNthTestUnknown(EnumUnderlyingType(TestUnknownIndex::vectorial)));
            const int Nnode_for_test_velocity = static_cast<int>(test_velocity_ref_felt.Nnode());
            
            const int felt_space_dimension = static_cast<int>(velocity_ref_felt.GetFEltSpaceDimension());
            
            InitLocalMatrixStorage({
                {
                    { Nnode_for_test_velocity, Nnode_for_velocity }, // velocity_block_matrix
                    { felt_space_dimension, Nnode_for_velocity } // transposed_dphi_velocity_block
                }});
        }
        
        
        Stokes::~Stokes() = default;
        
        
        
        const std::string& Stokes::ClassName()
        {
            static std::string name("Stokes");
            return name;
        }
        
        
        void Stokes::ComputeEltArray()
        {
            auto& elementary_data = GetNonCstElementaryData();
            
            auto& matrix_result = elementary_data.GetNonCstMatrixResult();
            matrix_result.Zero();
            
            const auto& scalar_ref_felt =
                elementary_data.GetRefFElt(GetNthUnknown(EnumUnderlyingType(UnknownIndex::scalar)));
            const auto& vectorial_ref_felt =
                elementary_data.GetRefFElt(GetNthUnknown(EnumUnderlyingType(UnknownIndex::vectorial)));
            
            const auto& scalar_test_ref_felt =
                elementary_data.GetTestRefFElt(GetNthTestUnknown(EnumUnderlyingType(TestUnknownIndex::scalar)));
            const auto& vectorial_test_ref_felt =
                elementary_data.GetTestRefFElt(GetNthTestUnknown(EnumUnderlyingType(TestUnknownIndex::vectorial)));
            
            const auto Nnode_velocity = static_cast<int>(vectorial_ref_felt.Nnode());
            const auto Ndof_velocity = static_cast<int>(vectorial_ref_felt.Ndof());
            const auto Nnode_pressure = static_cast<int>(scalar_ref_felt.Nnode());
            
            const auto Nnode_test_velocity = static_cast<int>(vectorial_test_ref_felt.Nnode());
            const auto Nnode_test_pressure = static_cast<int>(scalar_test_ref_felt.Nnode());
            const auto Ndof_test_velocity = static_cast<int>(vectorial_test_ref_felt.Ndof());
            
            const auto& infos_at_quad_pt_list_for_unknown =
                elementary_data.GetInformationsAtQuadraturePointListForUnknown();
            const auto& infos_at_quad_pt_list_for_test_unknown =
                elementary_data.GetInformationsAtQuadraturePointListForTestUnknown();
            
            assert(infos_at_quad_pt_list_for_unknown.size() == infos_at_quad_pt_list_for_test_unknown.size());

            auto& velocity_block_matrix = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::velocity_block_matrix)>(); // Used to store factor * dPhi * transp(dPhi_test).
            auto& transposed_dphi_velocity = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_dphi_velocity)>(); // Helper matrix to store transp(dPhi_test).
            
            const auto& fluid_viscosity = GetFluidViscosity();
            const auto& geom_elt = elementary_data.GetCurrentGeomElt();
            
            const auto Ncomponent = static_cast<int>(vectorial_ref_felt.Ncomponent());
            
            assert(Ncomponent == static_cast<int>(vectorial_test_ref_felt.Ncomponent()));
            
            assert(infos_at_quad_pt_list_for_unknown.size() == infos_at_quad_pt_list_for_test_unknown.size());
            const auto end_infos_at_quad_pt = infos_at_quad_pt_list_for_unknown.cend();

            for (auto it_infos_at_quad_pt = infos_at_quad_pt_list_for_unknown.cbegin(),
                 it_infos_at_quad_pt_test = infos_at_quad_pt_list_for_test_unknown.cbegin();
                 it_infos_at_quad_pt != end_infos_at_quad_pt;
                 ++it_infos_at_quad_pt, ++it_infos_at_quad_pt_test)
            {
                const auto& infos_at_quad_pt_for_unknown = *it_infos_at_quad_pt;
                const auto& infos_at_quad_pt_for_test_unknown = *it_infos_at_quad_pt_test;
                
                const auto& quad_pt = infos_at_quad_pt_for_unknown.GetQuadraturePoint();
                
                const double factor = quad_pt.GetWeight()
                                    * infos_at_quad_pt_for_unknown.GetAbsoluteValueJacobianDeterminant()
                                    * fluid_viscosity.GetValue(quad_pt, geom_elt);
                
                assert(infos_at_quad_pt_for_unknown.GetQuadraturePoint().GetRuleName() ==
                       infos_at_quad_pt_for_test_unknown.GetQuadraturePoint().GetRuleName());

                // First the scalar div vectorial contribution, which should yield a result similar to operator
                // ScalarDivVectorial.
                {
                    const auto& felt_phi = infos_at_quad_pt_for_unknown.GetFEltPhi();
                    const auto& test_felt_phi = infos_at_quad_pt_for_test_unknown.GetFEltPhi();

                    const auto& gradient_felt_phi = infos_at_quad_pt_for_unknown.GetGradientFEltPhi();
                    const auto& test_gradient_felt_phi = infos_at_quad_pt_for_test_unknown.GetGradientFEltPhi();
                    
                    // Part in (v*, p)
                    for (int node_test_velocity_index = 0; node_test_velocity_index < Nnode_test_velocity; ++node_test_velocity_index)
                    {
                        int dof_test_velocity_index = node_test_velocity_index;
                        
                        for (int node_pressure_index = 0; node_pressure_index < Nnode_pressure; ++node_pressure_index)
                        {
                            const int& dof_pressure = node_pressure_index; // alias
                            const auto pressure_term = felt_phi(Nnode_velocity + node_pressure_index);
                            
                            for (int component = 0; component < Ncomponent; ++component, dof_test_velocity_index += Nnode_test_velocity)
                            {
                                const double product =
                                    factor * pressure_term * test_gradient_felt_phi(node_test_velocity_index, component);
                                
                                matrix_result(dof_test_velocity_index, Ndof_velocity + dof_pressure) -= product;
                            }
                        }
                    }
                    
                    // Part in (p*, v)
                    for (int node_test_pressure_index = 0; node_test_pressure_index < Nnode_test_pressure; ++node_test_pressure_index)
                    {
                        const int& dof_test_pressure = node_test_pressure_index; // alias
                        const auto test_pressure_term = test_felt_phi(Nnode_test_velocity + node_test_pressure_index);
                        
                        for (int node_velocity_index = 0; node_velocity_index < Nnode_velocity; ++node_velocity_index)
                        {
                            int dof_velocity_index = node_velocity_index;
                            
                            for (int component = 0; component < Ncomponent; ++component, dof_velocity_index += Nnode_velocity)
                            {
                                const double product =
                                    factor * test_pressure_term * gradient_felt_phi(node_velocity_index, component);
                                
                                matrix_result(Ndof_test_velocity + dof_test_pressure, dof_velocity_index) -= product;
                            }
                        }
                    }
                }

                // Then here we perform grad-grad operator on the velocity-velocity block.
                {
                    const auto& dphi_velocity = ExtractSubMatrix(infos_at_quad_pt_for_unknown.GetGradientFEltPhi(),
                                                                 vectorial_ref_felt);
                    
                    const auto& dphi_test_velocity = ExtractSubMatrix(infos_at_quad_pt_for_test_unknown.GetGradientFEltPhi(),
                                                                      vectorial_test_ref_felt);
                    
                    velocity_block_matrix.Zero();
                    
                    // First compute the content of the block matrix.
                    assert(dphi_velocity.GetM() == Nnode_velocity);
                    assert(dphi_velocity.GetN() == Ncomponent);
                    
                    assert(dphi_test_velocity.GetM() == Nnode_test_velocity);
                    assert(dphi_test_velocity.GetN() == Ncomponent);
                    
                    Wrappers::Seldon::Transpose(dphi_velocity, transposed_dphi_velocity);
                    
                    Seldon::Mlt(factor,
                                dphi_test_velocity,
                                transposed_dphi_velocity,
                                velocity_block_matrix);

                    // Then report it into the elementary matrix.
                    for (int m = 0; m < Nnode_test_velocity; ++m)
                    {
                        for (int n = 0; n < Nnode_velocity; ++n)
                        {
                            const double value = velocity_block_matrix(m, n);

                            for (int component = 0; component < Ncomponent; ++component)
                            {
                                const auto shift_row = Nnode_test_velocity * component;
                                const auto shift_col = Nnode_velocity * component;
                                matrix_result(m + shift_row, n + shift_col) += value;
                            }
                        }
                    }
                }
            }
        }
     
        
    } // namespace LocalVariationalOperatorNS
        
        
    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
