///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 1 Sep 2015 14:09:59 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Seldon/SeldonFunctions.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/Bidomain.hpp"


namespace MoReFEM
{
    
    
    namespace Advanced
    {
    
    
    namespace LocalVariationalOperatorNS
    {
        
        
        Bidomain::Bidomain(const ExtendedUnknown::vector_const_shared_ptr& a_unknown_storage,
                           const ExtendedUnknown::vector_const_shared_ptr& a_test_unknown_storage,
                           elementary_data_type&& a_elementary_data,
                           const scalar_parameter& intracellular_trans_diffusion_tensor,
                           const scalar_parameter& extracellular_trans_diffusion_tensor,
                           const scalar_parameter& intracellular_fiber_diffusion_tensor,
                           const scalar_parameter& extracellular_fiber_diffusion_tensor,
                           const FiberList<ParameterNS::Type::vector>& fibers)
        : BilinearLocalVariationalOperator(a_unknown_storage, a_test_unknown_storage, std::move(a_elementary_data)),
        matrix_parent(),
        intracellular_trans_diffusion_tensor_(intracellular_trans_diffusion_tensor),
        extracellular_trans_diffusion_tensor_(extracellular_trans_diffusion_tensor),
        intracellular_fiber_diffusion_tensor_(intracellular_fiber_diffusion_tensor),
        extracellular_fiber_diffusion_tensor_(extracellular_fiber_diffusion_tensor),
        fibers_(fibers)
        {
            const auto& elementary_data = GetElementaryData();
            
            const auto& unknown1_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
            const int Nnode_for_unknown1 = static_cast<int>(unknown1_ref_felt.Nnode());
            
            const auto& unknown2_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(1));
            const int Nnode_for_unknown2 = static_cast<int>(unknown2_ref_felt.Nnode());
            
            const auto& test_unknown1_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
            const int Nnode_for_test_unknown1 = static_cast<int>(test_unknown1_ref_felt.Nnode());
            
            const auto& test_unknown2_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(1));
            const int Nnode_for_test_unknown2 = static_cast<int>(test_unknown2_ref_felt.Nnode());
            
            const int felt_space_dimension = static_cast<int>(unknown1_ref_felt.GetFEltSpaceDimension());
            
            InitLocalMatrixStorage({
                {
                    { Nnode_for_test_unknown1, Nnode_for_unknown1 }, // block matrix 1 (Vm,Vm)
                    { Nnode_for_test_unknown2, Nnode_for_unknown2 }, // block matrix 2 (Ue,Ue)
                    { Nnode_for_test_unknown1, Nnode_for_unknown2 }, // block matrix 3 (Vm, Ue)
                    { Nnode_for_test_unknown2, Nnode_for_unknown1 }, // block matrix 4 (Ue, Vm)
                    { felt_space_dimension, Nnode_for_unknown1 }, // transposed dPhi
                    { felt_space_dimension, Nnode_for_unknown2 }, // transposed dPsi
                    { Nnode_for_test_unknown1, felt_space_dimension }, // dPhi_test*sigma
                    { Nnode_for_test_unknown2, felt_space_dimension }, // dPsi_test*sigma
                    { felt_space_dimension, felt_space_dimension } // tau_X_tau
                }});
        }
                                                                       
                                                                       
        Bidomain::~Bidomain() = default;
        
                                                                       
        const std::string& Bidomain::ClassName()
        {
            static std::string name("Bidomain");
            return name;
        }
        
        
        void Bidomain::ComputeEltArray()
        {
            auto& elementary_data = GetNonCstElementaryData();
            
            auto& matrix_result = elementary_data.GetNonCstMatrixResult();
            matrix_result.Zero();
            
            const auto& unknown1_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
            const auto& unknown2_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(1));
            
            const auto& test_unknown1_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
            const auto& test_unknown2_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(1));
            
            // Current operator yields in fact a diagonal per block matrix where each block is the same.
            auto& block_matrix1 = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix1)>();
            auto& block_matrix2 = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix2)>();
            auto& block_matrix3 = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix3)>();
            auto& block_matrix4 = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix4)>();
            auto& transposed_dphi = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_dphi)>();
            auto& transposed_dpsi = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_dpsi)>();
            auto& dphi_test_sigma = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dphi_test_sigma)>();
            auto& dpsi_test_sigma = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dpsi_test_sigma)>();
            auto& tau_sigma = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tau_sigma)>();
            
            const auto& infos_at_quad_pt_list_for_unknown = elementary_data.GetInformationsAtQuadraturePointListForUnknown();
            const auto& infos_at_quad_pt_list_for_test_unknown = elementary_data.GetInformationsAtQuadraturePointListForTestUnknown();
            
            assert(infos_at_quad_pt_list_for_unknown.size() == infos_at_quad_pt_list_for_test_unknown.size());
            
            const int Nnode_for_unknown1 = static_cast<int>(unknown1_ref_felt.Nnode());
            const int Nnode_for_unknown2 = static_cast<int>(unknown2_ref_felt.Nnode());
            
            assert(unknown1_ref_felt.Ncomponent() == 1u && "Bidomain operator limited to scalar unknowns.");
            assert(unknown2_ref_felt.Ncomponent() == 1u && "Bidomain operator limited to scalar unknowns.");
            
            const int Nnode_for_test_unknown1 = static_cast<int>(test_unknown1_ref_felt.Nnode());
            const int Nnode_for_test_unknown2 = static_cast<int>(test_unknown2_ref_felt.Nnode());
            
            assert(test_unknown1_ref_felt.Ncomponent() == 1u && "Bidomain operator limited to scalar unknowns.");
            assert(test_unknown2_ref_felt.Ncomponent() == 1u && "Bidomain operator limited to scalar unknowns.");
            
            const auto& intracellular_trans_diffusion_tensor = GetIntracelluarTransDiffusionTensor();
            const auto& extracellular_trans_diffusion_tensor = GetExtracelluarTransDiffusionTensor();
            const auto& intracellular_fiber_diffusion_tensor = GetIntracelluarFiberDiffusionTensor();
            const auto& extracellular_fiber_diffusion_tensor = GetExtracelluarFiberDiffusionTensor();
            
            const auto& geom_elt = elementary_data.GetCurrentGeomElt();
            
            const int felt_space_dimension = static_cast<int>(unknown1_ref_felt.GetFEltSpaceDimension());
            
            auto& fibers = GetFibers();
            
            assert(infos_at_quad_pt_list_for_unknown.size() == infos_at_quad_pt_list_for_test_unknown.size());
            const auto end_infos_at_quad_pt = infos_at_quad_pt_list_for_unknown.cend();

            for (auto it_infos_at_quad_pt = infos_at_quad_pt_list_for_unknown.cbegin(),
                 it_infos_at_quad_pt_test = infos_at_quad_pt_list_for_test_unknown.cbegin();
                 it_infos_at_quad_pt != end_infos_at_quad_pt;
                 ++it_infos_at_quad_pt, ++it_infos_at_quad_pt_test)
            {
                const auto& infos_at_quad_pt_for_unknown = *it_infos_at_quad_pt;
                const auto& infos_at_quad_pt_for_test_unknown = *it_infos_at_quad_pt_test;
                
                // First compute the content of the block matrix.
                const double factor = infos_at_quad_pt_for_unknown.GetQuadraturePoint().GetWeight()
                                    * infos_at_quad_pt_for_unknown.GetAbsoluteValueJacobianDeterminant();
                
                assert(infos_at_quad_pt_for_unknown.GetQuadraturePoint().GetRuleName() ==
                       infos_at_quad_pt_for_test_unknown.GetQuadraturePoint().GetRuleName());
                
                const auto& quad_pt = infos_at_quad_pt_for_unknown.GetQuadraturePoint();
                
                const double factor1 = factor * intracellular_trans_diffusion_tensor.GetValue(quad_pt, geom_elt);
                
                const double factor2 = factor * extracellular_trans_diffusion_tensor.GetValue(quad_pt, geom_elt);
                
                const double factor3 = factor * intracellular_fiber_diffusion_tensor.GetValue(quad_pt, geom_elt);
                
                const double factor4 = factor * extracellular_fiber_diffusion_tensor.GetValue(quad_pt, geom_elt); 
                
                const auto& grad_felt_phi_for_unknown = infos_at_quad_pt_for_unknown.GetGradientFEltPhi();
                
                const auto& dphi = ExtractSubMatrix(grad_felt_phi_for_unknown,
                                                    unknown1_ref_felt);
                
                const auto& dpsi = ExtractSubMatrix(grad_felt_phi_for_unknown,
                                                    unknown2_ref_felt);
                
                const auto& grad_felt_phi_for_test_unknown = infos_at_quad_pt_for_test_unknown.GetGradientFEltPhi();
                
                const auto& dphi_test = ExtractSubMatrix(grad_felt_phi_for_test_unknown,
                                                         test_unknown1_ref_felt);
                
                const auto& dpsi_test = ExtractSubMatrix(grad_felt_phi_for_test_unknown,
                                                         test_unknown2_ref_felt);
                
                assert(dphi.GetM() == Nnode_for_unknown1);
                assert(dpsi.GetM() == Nnode_for_unknown2);
                assert(dphi_test.GetM() == Nnode_for_test_unknown1);
                assert(dpsi_test.GetM() == Nnode_for_test_unknown2);
                
                Wrappers::Seldon::Transpose(dphi, transposed_dphi);
                Wrappers::Seldon::Transpose(dpsi, transposed_dpsi);
                
                block_matrix1.Zero();
                block_matrix2.Zero();
                block_matrix3.Zero();
                block_matrix4.Zero();
                
                Seldon::Mlt(factor1,
                            dphi_test,
                            transposed_dphi,
                            block_matrix1);
                
                Seldon::Mlt(factor1 + factor2,
                            dpsi_test,
                            transposed_dpsi,
                            block_matrix2);
                
                Seldon::Mlt(factor1,
                            dphi_test,
                            transposed_dpsi,
                            block_matrix3);
                
                Seldon::Mlt(factor1,
                            dpsi_test,
                            transposed_dphi,
                            block_matrix4);
                
                const auto& tau_interpolate = fibers.GetValue(quad_pt, geom_elt);
                
                double norm = 0.;
                for (int component = 0 ; component < felt_space_dimension ; ++component)
                    norm += NumericNS::Square(tau_interpolate(component));
                
                tau_sigma.Zero();

                if (!(NumericNS::IsZero(norm)))
                {                   
                    Wrappers::Seldon::OuterProd(tau_interpolate, tau_interpolate, tau_sigma);
                    Mlt(1. / norm, tau_sigma);
                

                    Seldon::Mlt(factor3 - factor1,
                                dphi_test,
                                tau_sigma,
                                dphi_test_sigma);
                    
                    Seldon::MltAdd(1.,
                                   dphi_test_sigma,
                                   transposed_dphi,
                                   1.,
                                   block_matrix1);
                    
                    Seldon::Mlt(factor3 - factor1,
                                dphi_test,
                                tau_sigma,
                                dphi_test_sigma);
                    
                    Seldon::MltAdd(1.,
                                   dphi_test_sigma,
                                   transposed_dpsi,
                                   1.,
                                   block_matrix3);
                    
                    Seldon::Mlt(factor3 - factor1,
                                dpsi_test,
                                tau_sigma,
                                dpsi_test_sigma);
                    
                    Seldon::MltAdd(1.,
                                   dpsi_test_sigma,
                                   transposed_dphi,
                                   1.,
                                   block_matrix4);
                    
                    Seldon::Mlt(factor3 - factor1 + factor4 - factor2,
                                dpsi_test,
                                tau_sigma,
                                dpsi_test_sigma);
                    
                    Seldon::MltAdd(1.,
                                   dpsi_test_sigma,
                                   transposed_dpsi,
                                   1.,
                                   block_matrix2);
                }

                // Then report it into the elementary matrix.
                for (int node_test_unknown1_index = 0; node_test_unknown1_index < Nnode_for_test_unknown1; ++node_test_unknown1_index)
                {
                    for (int node_unknown1_index = 0; node_unknown1_index < Nnode_for_unknown1; ++node_unknown1_index)
                    {
                        const double value1 = block_matrix1(node_test_unknown1_index, node_unknown1_index);
                        
                        matrix_result(node_test_unknown1_index, node_unknown1_index) += value1;
                    }
                }
                
                for (int node_test_unknown2_index = 0; node_test_unknown2_index < Nnode_for_test_unknown2; ++node_test_unknown2_index)
                {
                    for (int node_unknown2_index = 0; node_unknown2_index < Nnode_for_unknown2; ++node_unknown2_index)
                    {
                        const double value2 = block_matrix2(node_test_unknown2_index, node_unknown2_index);
                        
                        matrix_result(node_test_unknown2_index + Nnode_for_test_unknown1, node_unknown2_index + Nnode_for_unknown1) += value2;
                    }
                }

                for (int node_test_unknown2_index = 0; node_test_unknown2_index < Nnode_for_test_unknown2; ++node_test_unknown2_index)
                {
                    for (int node_unknown1_index = 0; node_unknown1_index < Nnode_for_unknown1; ++node_unknown1_index)
                    {
                        const double value4 = block_matrix4(node_test_unknown2_index, node_unknown1_index);
                        
                        matrix_result(node_test_unknown2_index + Nnode_for_test_unknown1, node_unknown1_index) += value4;
                    }
                }
                
                for (int node_test_unknown1_index = 0; node_test_unknown1_index < Nnode_for_test_unknown1; ++node_test_unknown1_index)
                {
                    for (int node_unknown2_index = 0; node_unknown2_index < Nnode_for_unknown2; ++node_unknown2_index)
                    {
                        const double value3 = block_matrix3(node_test_unknown1_index, node_unknown2_index);
                        
                        matrix_result(node_test_unknown1_index, node_unknown2_index + Nnode_for_unknown1) += value3;
                    }
                }
            }
        }
        
        
    } // namespace LocalVariationalOperatorNS
        
        
    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
