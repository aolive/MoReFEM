///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 May 2014 16:04:53 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#include "ThirdParty/Wrappers/Seldon/SeldonFunctions.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/GradPhiGradPhi.hpp"


namespace MoReFEM
{
    
    
    namespace Advanced
    {


    namespace LocalVariationalOperatorNS
    {
        
        
        
        GradPhiGradPhi::GradPhiGradPhi(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                                       const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                                       elementary_data_type&& a_elementary_data)
        : parent(unknown_list, test_unknown_list, std::move(a_elementary_data)),
        matrix_parent()
        {
            assert(unknown_list.size() == 1);
            assert(test_unknown_list.size() == 1);
            
            const auto& elementary_data = parent::GetElementaryData();
            
            const auto& unknown = parent::GetNthUnknown(0);
            const auto& test_unknown = parent::GetNthTestUnknown(0);
            
            const auto& ref_felt = elementary_data.GetRefFElt(unknown);
            const auto& test_ref_felt = elementary_data.GetTestRefFElt(test_unknown);
            
            const int Nnode_for_unknown = static_cast<int>(ref_felt.Nnode());
            const int Nnode_for_test_unknown = static_cast<int>(test_ref_felt.Nnode());

            #ifndef NDEBUG
            {
                const auto& infos_at_quad_pt_list_for_unknown = elementary_data.GetInformationsAtQuadraturePointListForUnknown();
                assert(!infos_at_quad_pt_list_for_unknown.empty());
                const auto& last_dphi = infos_at_quad_pt_list_for_unknown.back().GetGradientFEltPhi();
                assert(last_dphi.GetM() == Nnode_for_unknown);
            }
            #endif // NDEBUG

            const int felt_space_dimension = static_cast<int>(ref_felt.GetFEltSpaceDimension());
            
            InitLocalMatrixStorage({
                {
                    { Nnode_for_test_unknown, Nnode_for_unknown  }, // block matrix
                    { felt_space_dimension, Nnode_for_unknown } // transposed dPhi
                }});
        }
        
        
        GradPhiGradPhi::~GradPhiGradPhi() = default;
        
        
        const std::string& GradPhiGradPhi::ClassName()
        {
            static std::string name("GradPhiGradPhi");
            return name;
        }
        
        
        void GradPhiGradPhi::ComputeEltArray()
        {
            auto& elementary_data = parent::GetNonCstElementaryData();

            const auto& ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
            const auto& test_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
            
            // Current operator yields in fact a diagonal per block matrix where each block is the same.
            auto& block_matrix = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix)>();
            
            auto& matrix_result = elementary_data.GetNonCstMatrixResult();
            matrix_result.Zero();
            
            auto& transposed_dphi = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_dphi)>();
            
            const auto& infos_at_quad_pt_list_for_unknown = elementary_data.GetInformationsAtQuadraturePointListForUnknown();
            const auto& infos_at_quad_pt_list_for_test_unknown = elementary_data.GetInformationsAtQuadraturePointListForTestUnknown();
            
            assert(infos_at_quad_pt_list_for_unknown.size() == infos_at_quad_pt_list_for_test_unknown.size());
            
            const int Nnode_for_unknown = static_cast<int>(ref_felt.Nnode());
            const int Nnode_for_test_unknown = static_cast<int>(test_ref_felt.Nnode());
            
            const int Ncomponent = static_cast<int>(ref_felt.Ncomponent());
            
            assert(Ncomponent == static_cast<int>(test_ref_felt.Ncomponent()));
            
            assert(infos_at_quad_pt_list_for_unknown.size() == infos_at_quad_pt_list_for_test_unknown.size());
            const auto end_infos_at_quad_pt = infos_at_quad_pt_list_for_unknown.cend();

            for (auto it_infos_at_quad_pt = infos_at_quad_pt_list_for_unknown.cbegin(),
                 it_infos_at_quad_pt_test = infos_at_quad_pt_list_for_test_unknown.cbegin();
                 it_infos_at_quad_pt != end_infos_at_quad_pt;
                 ++it_infos_at_quad_pt, ++it_infos_at_quad_pt_test)
            {
                const auto& infos_at_quad_pt_for_unknown = *it_infos_at_quad_pt;
                const auto& infos_at_quad_pt_for_test_unknown = *it_infos_at_quad_pt_test;
                
                block_matrix.Zero();

                // First compute the content of the block matrix.
                const double factor = infos_at_quad_pt_for_unknown.GetQuadraturePoint().GetWeight()
                                    * infos_at_quad_pt_for_unknown.GetAbsoluteValueJacobianDeterminant();
                
                assert(infos_at_quad_pt_for_unknown.GetQuadraturePoint().GetRuleName() ==
                       infos_at_quad_pt_for_test_unknown.GetQuadraturePoint().GetRuleName());
                
                const auto& dphi = ExtractSubMatrix(infos_at_quad_pt_for_unknown.GetGradientFEltPhi(),
                                                    ref_felt);
                       
               const auto& dphi_test = ExtractSubMatrix(infos_at_quad_pt_for_test_unknown.GetGradientFEltPhi(),
                                                   test_ref_felt);
                
                assert(dphi.GetM() == Nnode_for_unknown);
                assert(dphi_test.GetM() == Nnode_for_test_unknown);
                
                Wrappers::Seldon::Transpose(dphi, transposed_dphi);
                
                Seldon::Mlt(factor,
                            dphi_test,
                            transposed_dphi,
                            block_matrix);
                
                // Then report it into the elementary matrix.
                for (int m = 0; m < Nnode_for_test_unknown; ++m)
                {
                    for (int n = 0; n < Nnode_for_unknown; ++n)
                    {
                        const double value = block_matrix(m, n);
                        
                        for (int deriv_component = 0; deriv_component < Ncomponent; ++deriv_component)
                        {
                            const auto shift = Nnode_for_test_unknown * deriv_component;
                            matrix_result(m + shift, n + shift) += value;
                        }
                    }
                }
            }
        }
        
        
    } // namespace LocalVariationalOperatorNS
        
        
    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
