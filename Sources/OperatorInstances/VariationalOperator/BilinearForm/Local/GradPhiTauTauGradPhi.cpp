///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 9 Aug 2017 17:28:42 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Seldon/SeldonFunctions.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/GradPhiTauTauGradPhi.hpp"


namespace MoReFEM
{
    
    
    namespace Advanced
    {
    
    
    namespace LocalVariationalOperatorNS
    {
        
        
        GradPhiTauTauGradPhi::GradPhiTauTauGradPhi(const ExtendedUnknown::vector_const_shared_ptr& a_unknown_storage,
                                                   const ExtendedUnknown::vector_const_shared_ptr& a_test_unknown_storage,
                                                   elementary_data_type&& a_elementary_data,
                                                   const scalar_parameter& transverse_diffusion_tensor,
                                                   const scalar_parameter& fiber_diffusion_tensor,
                                                   const FiberList<ParameterNS::Type::vector>& fibers)
        : BilinearLocalVariationalOperator(a_unknown_storage, a_test_unknown_storage, std::move(a_elementary_data)),
        matrix_parent(),
        transverse_diffusion_tensor_(transverse_diffusion_tensor),
        fiber_diffusion_tensor_(fiber_diffusion_tensor),
        fibers_(fibers)
        {
            const auto& elementary_data = GetElementaryData();
            
            const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
            const int Nnode_for_unknown = static_cast<int>(unknown_ref_felt.Nnode());
            
            const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
            const int Nnode_for_test_unknown = static_cast<int>(test_unknown_ref_felt.Nnode());
            
            const int felt_space_dimension = static_cast<int>(unknown_ref_felt.GetFEltSpaceDimension());
            
            InitLocalMatrixStorage({
                {
                    { Nnode_for_test_unknown, Nnode_for_unknown}, // block matrix
                    { felt_space_dimension, Nnode_for_unknown }, // transposed dPhi
                    { Nnode_for_test_unknown, felt_space_dimension }, // dPhi_test*sigma
                    { felt_space_dimension, felt_space_dimension } // tau_X_tau
                }});
        }
                                                                       
                                                                       
        GradPhiTauTauGradPhi::~GradPhiTauTauGradPhi() = default;
        
                                                                       
        const std::string& GradPhiTauTauGradPhi::ClassName()
        {
            static std::string name("GradPhiTauTauGradPhi");
            return name;
        }
        
        
        void GradPhiTauTauGradPhi::ComputeEltArray()
        {
            auto& elementary_data = GetNonCstElementaryData();
            
            auto& matrix_result = elementary_data.GetNonCstMatrixResult();
            matrix_result.Zero();
            
            const auto& ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
            const auto& test_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
            
            // Current operator yields in fact a diagonal per block matrix where each block is the same.
            auto& block_matrix = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix)>();
            auto& transposed_dphi = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_dphi)>();
            auto& dphi_test_sigma = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dphi_test_sigma)>();
            auto& tau_sigma = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tau_sigma)>();
            
            const auto& infos_at_quad_pt_list_for_unknown = elementary_data.GetInformationsAtQuadraturePointListForUnknown();
            const auto& infos_at_quad_pt_list_for_test_unknown = elementary_data.GetInformationsAtQuadraturePointListForTestUnknown();
            
            assert(infos_at_quad_pt_list_for_unknown.size() == infos_at_quad_pt_list_for_test_unknown.size());
            
            const int Nnode_for_unknown = static_cast<int>(ref_felt.Nnode());
            const int Nnode_for_test_unknown = static_cast<int>(test_ref_felt.Nnode());
            
            const int Ncomponent = static_cast<int>(ref_felt.Ncomponent());
            
            assert(Ncomponent == static_cast<int>(test_ref_felt.Ncomponent()));
            
            assert(Ncomponent == 1u && "GradPhiTauTauGradPhi operator limited to scalar unknowns.");
            
            static_cast<void>(Ncomponent);
            
            const auto& transverse_diffusion_tensor = GetTransverseDiffusionTensor();
            const auto& fiber_diffusion_tensor = GetFiberDiffusionTensor();
            
            const auto& geom_elt = elementary_data.GetCurrentGeomElt();
            
            const int felt_space_dimension = static_cast<int>(ref_felt.GetFEltSpaceDimension());
            
            auto& fibers = GetFibers();
            
            assert(infos_at_quad_pt_list_for_unknown.size() == infos_at_quad_pt_list_for_test_unknown.size());
            const auto end_infos_at_quad_pt = infos_at_quad_pt_list_for_unknown.cend();

            for (auto it_infos_at_quad_pt = infos_at_quad_pt_list_for_unknown.cbegin(),
                 it_infos_at_quad_pt_test = infos_at_quad_pt_list_for_test_unknown.cbegin();
                 it_infos_at_quad_pt != end_infos_at_quad_pt;
                 ++it_infos_at_quad_pt, ++it_infos_at_quad_pt_test)
            {
                const auto& infos_at_quad_pt_for_unknown = *it_infos_at_quad_pt;
                const auto& infos_at_quad_pt_for_test_unknown = *it_infos_at_quad_pt_test;
                
                // First compute the content of the block matrix.
                const double factor = infos_at_quad_pt_for_unknown.GetQuadraturePoint().GetWeight()
                                    * infos_at_quad_pt_for_unknown.GetAbsoluteValueJacobianDeterminant();
                
                assert(infos_at_quad_pt_for_unknown.GetQuadraturePoint().GetRuleName() ==
                       infos_at_quad_pt_for_test_unknown.GetQuadraturePoint().GetRuleName());
                
                const auto& quad_pt = infos_at_quad_pt_for_unknown.GetQuadraturePoint();
                
                const double factor1 = factor * transverse_diffusion_tensor.GetValue(quad_pt, geom_elt);
                
                const double factor2 = factor * fiber_diffusion_tensor.GetValue(quad_pt, geom_elt);
                
                const auto& dphi = ExtractSubMatrix(infos_at_quad_pt_for_unknown.GetGradientFEltPhi(),
                                                    ref_felt);
                
                const auto& dphi_test = ExtractSubMatrix(infos_at_quad_pt_for_test_unknown.GetGradientFEltPhi(),
                                                    test_ref_felt);
                
                assert(dphi.GetM() == Nnode_for_unknown);
                assert(dphi_test.GetM() == Nnode_for_test_unknown);
                
                Wrappers::Seldon::Transpose(dphi, transposed_dphi);
                
                block_matrix.Zero();
                
                Seldon::Mlt(factor1,
                            dphi_test,
                            transposed_dphi,
                            block_matrix);
                
                const auto& tau_interpolate = fibers.GetValue(quad_pt, geom_elt);
                
                double norm = 0.;
                for (int component = 0 ; component < felt_space_dimension ; ++component)
                    norm += tau_interpolate(component) * tau_interpolate(component);
                
                tau_sigma.Zero();

                if (!(NumericNS::IsZero(norm)))
                {                   
                    Wrappers::Seldon::OuterProd(tau_interpolate, tau_interpolate, tau_sigma);
                    Mlt(1. / norm, tau_sigma);
                    
                    Seldon::Mlt(factor2 - factor1,
                                dphi_test,
                                tau_sigma,
                                dphi_test_sigma);
                                        
                    Seldon::MltAdd(1.,
                                   dphi_test_sigma,
                                   transposed_dphi,
                                   1.,
                                   block_matrix);
                }

                // Then report it into the elementary matrix.
                for (int m = 0; m < Nnode_for_test_unknown; ++m)
                {
                    for (int n = 0; n < Nnode_for_unknown; ++n)
                    {
                        const double value = block_matrix(m, n);
                        
                        matrix_result(m, n) += value;
                    }
                }
            }
        }
        
        
    } // namespace LocalVariationalOperatorNS
        
        
    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
