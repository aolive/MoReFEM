///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 May 2014 15:57:53 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_STOKES_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_STOKES_HPP_

# include "Operators/GlobalVariationalOperator/GlobalVariationalOperator.hpp"

# include "OperatorInstances/VariationalOperator/BilinearForm/Local/Stokes.hpp"


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        /*!
         * \brief Operator description.
         *
         * \todo #9 Describe operator!
         */
        class Stokes final : public GlobalVariationalOperatorNS::SameForAllRefGeomElt
        <
            Stokes,
            Advanced::OperatorNS::Nature::bilinear,
            Advanced::LocalVariationalOperatorNS::Stokes
        >
        {

        public:

            //! Alias to unique pointer.
            using const_unique_ptr = std::unique_ptr<const Stokes>;

            //! Returns the name of the operator.
            static const std::string& ClassName();

            //! \copydoc doxygen_hide_alias_self
            using self = Stokes;

            //! Alias to local operator.
            using local_operator_type = Advanced::LocalVariationalOperatorNS::Stokes;

            //! Convenient alias to pinpoint the GlobalVariationalOperator parent.
            using parent = GlobalVariationalOperatorNS::SameForAllRefGeomElt
            <
                self,
                Advanced::OperatorNS::Nature::bilinear,
                local_operator_type
            >;

            //! Friendship to the parent class so that the CRTP can reach private methods defined below.
            friend parent;

            //! Alias to scalar parameter used with this operator.
            using scalar_parameter = typename local_operator_type::scalar_parameter;


        public:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] unknown_list Container with vectorial then scalar unknown.
             * \copydoc doxygen_hide_test_unknown_list_param
             * \copydoc doxygen_hide_gvo_felt_space_arg
             * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
             * \param[in] fluid_viscosity Fluid viscosity.
             */
            explicit Stokes(const FEltSpace& felt_space,
                            const std::array<Unknown::const_shared_ptr, 2>& unknown_list,
                            const std::array<Unknown::const_shared_ptr, 2>& test_unknown_list,
                            const scalar_parameter& fluid_viscosity,
                            const QuadratureRulePerTopology* const quadrature_rule_per_topology = nullptr);

            //! Destructor.
            ~Stokes() = default;

            //! Copy constructor.
            Stokes(const Stokes&) = delete;

            //! Move constructor.
            Stokes(Stokes&&) = delete;

            //! Copy affectation.
            Stokes& operator=(const Stokes&) = delete;

            //! Move affectation.
            Stokes& operator=(Stokes&&) = delete;

            ///@}


        public:

            /*!
             * \brief Assemble into one or several matrices.
             *
             * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixWithCoefficient objects.
             *
             * \param[in] global_matrix_with_coeff_list List of global matrices into which the operator is
             * assembled. These matrices are assumed to be already properly allocated.
             * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
             * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
             * element space; if current \a domain is not a subset of finite element space one, assembling will occur
             * upon the intersection of both.
             *
             */
            template<class LinearAlgebraTupleT>
            void Assemble(LinearAlgebraTupleT&& global_matrix_with_coeff_list, const Domain& domain = Domain()) const;

        };


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/VariationalOperator/BilinearForm/Stokes.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_STOKES_HPP_
