///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Mon, 19 Oct 2015 17:44:11 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#include "OperatorInstances/VariationalOperator/NonlinearForm/NonlinearMembrane.hpp"


namespace MoReFEM
{
    
    
    namespace GlobalVariationalOperatorNS
    {
        
        
        NonlinearMembrane::NonlinearMembrane(const FEltSpace& felt_space,
                                             Unknown::const_shared_ptr unknown_list,
                                             Unknown::const_shared_ptr test_unknown_list,
                                             const scalar_parameter& youngs_modulus,
                                             const scalar_parameter& poisson_ratio,
                                             const scalar_parameter& thickness,
                                             const QuadratureRulePerTopology* const quadrature_rule_per_topology)

        : parent(felt_space,
                 unknown_list,
                 test_unknown_list,
                 quadrature_rule_per_topology,
                 AllocateGradientFEltPhi::yes,
                 DoComputeProcessorWiseLocal2Global::yes,
                 youngs_modulus,
                 poisson_ratio,
                 thickness)
        { }
        
        
        const std::string& NonlinearMembrane::ClassName()
        {
            static std::string name("NonlinearMembrane");
            return name;
        }        
     
        
    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
