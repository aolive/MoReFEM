//! \file
//
//
//  QuasiIncompressibleSecondPiolaKirchhoffStressTensor.hpp
//  MoReFEM
//
//  Created by sebastien on 10/03/2018.
//Copyright © 2018 Inria. All rights reserved.
//

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_QUASI_INCOMPRESSIBLE_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_QUASI_INCOMPRESSIBLE_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HPP_

# include <memory>
# include <vector>

# include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor.hpp"
# include "OperatorInstances/VariationalOperator/NonlinearForm/MixedSolidIncompressibility.hpp"


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        /*!
         * \brief Quasi-incompressible second Piola-Kirchhoff stress tensor operator.
         *
         * This operator is a bit peculiar: it is not directly a child of one of the basic class used to define
         * a \a GlobalVariationalOperator, but merely an ad-hoc object which initializes under the hood 2 such operators
         * and also defines a \a Assemble method which in fact calls assembling for the two internal operators.
         *
         * The rationale for this is that when the incompressible behaviour is expected both operators are actually
         * assembled at the same time, so this simplifies the reading of the code in the model.
         *
         * \internal We could have done as in Stokes operator and define a full-fledged global operator with its associated
         * \a LocalVariationalOperator, but due to the complexity of \a SecondPiolaKirchhoffStressTensor the duplication
         * of the code of the local operator would be far from trivial, contrary to Stokes case.
         *
         * \internal We could also have enriched \a SecondPiolaKirchhoffStressTensor operator to deal directly with both
         * cases, but this operator is already by far the most complex operator in MoReFEM and it could have become even
         * more unwieldy.
         *
         * \internal Of course, in due time if some additional efficiency is sought writing the unique operator (probably
         * by making incompressibility a template parameter or a policy of SecondPiolaKirchhoffStressTensor) is still
         * an option.
         */
        template
        <
            class HyperelasticityPolicyT,
            class ViscoelasticityPolicyT,
            class ActiveStressPolicyT,
            class HydrostaticLawPolicyT
        >
        class QuasiIncompressibleSecondPiolaKirchhoffStressTensor final
        : public SecondPiolaKirchhoffStressTensor
        <
            HyperelasticityPolicyT,
            ViscoelasticityPolicyT,
            ActiveStressPolicyT
        >,
        public MixedSolidIncompressibility<HydrostaticLawPolicyT>
        {

        public:

            //! \copydoc doxygen_hide_alias_self
            using self = QuasiIncompressibleSecondPiolaKirchhoffStressTensor
            <
                HyperelasticityPolicyT,
                ViscoelasticityPolicyT,
                ActiveStressPolicyT,
                HydrostaticLawPolicyT
            >;

            //! Alias to unique pointer.
            using const_unique_ptr = std::unique_ptr<const self>;

            //! Alias for the stiffnes operator.
            using stiffness_operator_parent =
                SecondPiolaKirchhoffStressTensor
                <
                    HyperelasticityPolicyT,
                    ViscoelasticityPolicyT,
                    ActiveStressPolicyT
                >;

            //! Alias for the penalization operator.
            using penalization_operator_parent =
                MixedSolidIncompressibility<HydrostaticLawPolicyT>;

            //! 'Inherited' alias from stiffness operator.
            using cauchy_green_tensor_type = typename stiffness_operator_parent::cauchy_green_tensor_type;

            //! 'Inherited' alias from stiffness operator.
            using input_active_stress_policy_type =
                typename stiffness_operator_parent::input_active_stress_policy_type;

        public:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \copydoc doxygen_hide_gvo_felt_space_arg
             * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
             * \param[in] unknown_list Container with vectorial then scalar unknown.
             * \copydoc doxygen_hide_test_unknown_list_param
             * \param[in] hydrostatic_law Policy concerning hydrostatic law. Do not delete this pointer!
             * \param[in] solid Object which provides the required material parameters for the solid.
             * \param[in] time_manager \a TimeManager need for Viscoelasticity and Active Stress.
             * \param[in] input_active_stress_policy Object required only for Active Stress to compute U0 and U1 locally.
             * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
             * \param[in] hyperelastic_law Hyperelastic law considered. Do not delete this pointer!
             */
            explicit QuasiIncompressibleSecondPiolaKirchhoffStressTensor(const FEltSpace& felt_space,
                                                                         const std::array<Unknown::const_shared_ptr, 2>& unknown_list,
                                                                         const std::array<Unknown::const_shared_ptr, 2>& test_unknown_list,
                                                                         const Solid& solid,
                                                                         const TimeManager& time_manager,
                                                                         const typename HyperelasticityPolicyT::law_type* hyperelastic_law,
                                                                         const HydrostaticLawPolicyT* hydrostatic_law,
                                                                         const QuadratureRulePerTopology* const quadrature_rule_per_topology,
                                                                         input_active_stress_policy_type* input_active_stress_policy);


            //! Destructor.
            ~QuasiIncompressibleSecondPiolaKirchhoffStressTensor() = default;

            //! Copy constructor.
            QuasiIncompressibleSecondPiolaKirchhoffStressTensor(const QuasiIncompressibleSecondPiolaKirchhoffStressTensor&) = delete;

            //! Move constructor.
            QuasiIncompressibleSecondPiolaKirchhoffStressTensor(QuasiIncompressibleSecondPiolaKirchhoffStressTensor&&) = delete;

            //! Copy affectation.
            QuasiIncompressibleSecondPiolaKirchhoffStressTensor& operator=(const QuasiIncompressibleSecondPiolaKirchhoffStressTensor&) = delete;

            //! Move affectation.
            QuasiIncompressibleSecondPiolaKirchhoffStressTensor& operator=(QuasiIncompressibleSecondPiolaKirchhoffStressTensor&&) = delete;

            ///@}

        public:

            //! \copydoc doxygen_hide_second_piola_assemble_1
            template<class LinearAlgebraTupleT>
            void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                          const GlobalVector& state_previous_iteration,
                          const Domain& domain = Domain()) const;


            //! \copydoc doxygen_hide_second_piola_assemble_2
            template<class LinearAlgebraTupleT>
            void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                          const GlobalVector& state_previous_iteration,
                          const GlobalVector& velocity_previous_iteration,
                          const Domain& domain = Domain()) const;


            //! \copydoc doxygen_hide_second_piola_assemble_3
            template<class LinearAlgebraTupleT>
            void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                          const GlobalVector& state_previous_iteration,
                          const GlobalVector& velocity_previous_iteration,
                          const GlobalVector& electrical_activation_previous_time,
                          const GlobalVector& electrical_activation_at_time,
                          const bool do_update_sigma_c,
                          const Domain& domain = Domain()) const;



            //! \copydoc doxygen_hide_second_piola_assemble_4
            template<class LinearAlgebraTupleT>
            void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                          const GlobalVector& state_previous_iteration,
                          const GlobalVector& velocity_previous_iteration,
                          const GlobalVector& electrical_activation_previous_time,
                          const GlobalVector& electrical_activation_at_time,
                          const Domain& domain = Domain()) const;


            //! \copydoc doxygen_hide_second_piola_assemble_5
            template<class LinearAlgebraTupleT>
            void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                          const GlobalVector& state_previous_iteration,
                          const GlobalVector& electrical_activation_previous_time,
                          const GlobalVector& electrical_activation_at_time,
                          const bool do_update_sigma_c,
                          const Domain& domain = Domain()) const;


            //! \copydoc doxygen_hide_second_piola_assemble_6
            template<class LinearAlgebraTupleT>
            void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                          const GlobalVector& state_previous_iteration,
                          const GlobalVector& electrical_activation_previous_time,
                          const GlobalVector& electrical_activation_at_time,
                          const Domain& domain = Domain()) const;

     
        };


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


# include "OperatorInstances/VariationalOperator/NonlinearForm/QuasiIncompressibleSecondPiolaKirchhoffStressTensor.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_QUASI_INCOMPRESSIBLE_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HPP_
