///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 13 Mar 2017 22:23:07 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_x_APPLY_SET_CAUCHY_GREEN_TENSOR_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_x_APPLY_SET_CAUCHY_GREEN_TENSOR_HPP_


namespace MoReFEM
{


    namespace Internal
    {


        namespace GlobalVariationalOperatorNS
        {


            /*!
             * \brief Metaprogramming helper to call SetCauchyGreenTensor() for all \a LocalVariationalOperator hidden
             * in \a LocalOperatorTupleT.
             *
             * \tparam doxygen_hide_global_operator_local_operator_tuple_type
             * \tparam Index of the tuple item currently handled in the recursion "loop".
             * \tparam SizeTupleT Must be std::tuple_size<LocalOperatorTupleT>::value; there only to provide the stopping
             * condition of the recursion.
             */
            template
            <
                class LocalOperatorTupleT,
                std::size_t I,
                std::size_t SizeTupleT
            >
            struct ApplySetCauchyGreenTensor
            {


                static_assert(std::tuple_size<LocalOperatorTupleT>::value == SizeTupleT, "");


                /*!
                 * \brief Static method that does the actual work.
                 *
                 * \copydoc doxygen_hide_global_operator_local_operator_tuple_in
                 * \param[in] cauchy_green_tensor_raw_ptr Raw pointer to a \a CauchyGreenTensor.
                 */
                template<class CauchyGreenTypeT>
                static void Perform(const LocalOperatorTupleT& local_operator_tuple,
                                    const CauchyGreenTypeT& cauchy_green_tensor_raw_ptr);


            };



            // ============================
            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            // Stopping condition for the recursive call.
            // ============================


            template
            <
                class LocalOperatorTupleT,
                std::size_t SizeTupleT
            >
            struct ApplySetCauchyGreenTensor<LocalOperatorTupleT, SizeTupleT, SizeTupleT>
            {


                static_assert(std::tuple_size<LocalOperatorTupleT>::value == SizeTupleT, "");


                template<class CauchyGreenTypeT>
                static void Perform(const LocalOperatorTupleT& ,
                                    const CauchyGreenTypeT& );


            };


            // ============================
            // Stopping condition for the recursive call.
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN
            // ============================


        } // namespace GlobalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/Internal/ApplySetCauchyGreenTensor.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_x_APPLY_SET_CAUCHY_GREEN_TENSOR_HPP_
