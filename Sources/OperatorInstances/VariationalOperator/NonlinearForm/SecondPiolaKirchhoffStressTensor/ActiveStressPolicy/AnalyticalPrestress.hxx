///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 22 Jan 2016 12:33:42 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_ACTIVE_STRESS_POLICY_x_ANALYTICAL_PRESTRESS_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_ACTIVE_STRESS_POLICY_x_ANALYTICAL_PRESTRESS_HXX_


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        namespace SecondPiolaKirchhoffStressTensorNS
        {


            namespace ActiveStressPolicyNS
            {


                template <unsigned int FiberIndexT>
                template<class LocalOperatorStorageT>
                void AnalyticalPrestress<FiberIndexT>
                ::InitializeActiveStressPolicy(const Domain& domain,
                                               const QuadratureRulePerTopology& quadrature_rule_per_topology,
                                               const TimeManager& time_manager,
                                               const LocalOperatorStorageT& local_operator_storage,
                                               input_active_stress_policy_type* input_active_stress_policy)
                {
                    assert(!(!input_active_stress_policy));

                    input_active_stress_policy_ = input_active_stress_policy;

                    sigma_c_ = std::make_unique<ParameterAtQuadraturePoint<ParameterNS::Type::scalar>>("SigmaC",
                                                                                                       domain,
                                                                                                       quadrature_rule_per_topology,
                                                                                                       GetInputActiveStressPolicy().GetInitialValueActiveStress(),
                                                                                                       time_manager);

                    ParameterAtQuadraturePoint<ParameterNS::Type::scalar>* raw_sigma_c = sigma_c_.get();


                    ::MoReFEM::Internal::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS
                    ::SetSigmaCHelper<LocalOperatorStorageT, 0, std::tuple_size<LocalOperatorStorageT>::value>
                    ::Perform(raw_sigma_c,
                              local_operator_storage);
                }


                template <unsigned int FiberIndexT>
                inline const typename AnalyticalPrestress<FiberIndexT>::input_active_stress_policy_type&
                AnalyticalPrestress<FiberIndexT>::GetInputActiveStressPolicy() const noexcept
                {
                    assert(!(!input_active_stress_policy_));
                    return *input_active_stress_policy_;
                }


                template <unsigned int FiberIndexT>
                inline typename AnalyticalPrestress<FiberIndexT>::input_active_stress_policy_type&
                AnalyticalPrestress<FiberIndexT>::GetNonCstInputActiveStressPolicy() noexcept
                {
                    return const_cast<input_active_stress_policy_type&>(GetInputActiveStressPolicy());
                }


            } // namespace ActiveStressPolicyNS


        } // namespace SecondPiolaKirchhoffStressTensorNS


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_ACTIVE_STRESS_POLICY_x_ANALYTICAL_PRESTRESS_HXX_
