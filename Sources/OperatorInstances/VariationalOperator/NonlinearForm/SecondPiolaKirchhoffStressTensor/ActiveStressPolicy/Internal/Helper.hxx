///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 2 Mar 2016 18:13:31 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_ACTIVE_STRESS_POLICY_x_INTERNAL_x_HELPER_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_ACTIVE_STRESS_POLICY_x_INTERNAL_x_HELPER_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace GlobalVariationalOperatorNS
        {


            namespace SecondPiolaKirchhoffStressTensorNS
            {


                template <class ActiveStressPolicyT>
                template <class LocalOperatorStorageT>
                void ActiveStressHelper<ActiveStressPolicyT>
                ::InitActiveStress(const Domain& domain,
                                   const QuadratureRulePerTopology& quadrature_rule_per_topology,
                                   const TimeManager& time_manager,
                                   const LocalOperatorStorageT& local_operator_storage,
                                   typename ActiveStressPolicyT::input_active_stress_policy_type* input_active_stress_policy,
                                   ActiveStressPolicyT& active_stress)
                {
                    active_stress.template InitializeActiveStressPolicy(domain,
                                                                        quadrature_rule_per_topology,
                                                                        time_manager,
                                                                        local_operator_storage,
                                                                        input_active_stress_policy);
                }


                template <class LocalOperatorStorageT>
                void ActiveStressHelper<::MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ActiveStressPolicyNS::None>
                ::InitActiveStress(const Domain& domain,
                                   const QuadratureRulePerTopology& quadrature_rule_per_topology,
                                   const TimeManager& time_manager,
                                   const LocalOperatorStorageT& local_operator_storage,
                                   typename none_type::input_active_stress_policy_type* input_active_stress_policy,
                                   none_type& active_stress)
                {
                    static_cast<void>(domain);
                    static_cast<void>(quadrature_rule_per_topology);
                    static_cast<void>(time_manager);
                    static_cast<void>(local_operator_storage);
                    static_cast<void>(input_active_stress_policy);
                    static_cast<void>(active_stress);
                }


                template
                <
                    class LocalOperatorTupleT,
                    std::size_t I,
                    std::size_t TupleSizeT
                >
                void SetSigmaCHelper<LocalOperatorTupleT, I, TupleSizeT>
                ::Perform(ParameterAtQuadraturePoint<ParameterNS::Type::scalar>* raw_sigma_c,
                          const LocalOperatorTupleT& local_operator_tuple)
                {
                    const auto& local_operator_item = std::get<I>(local_operator_tuple);

                    if (local_operator_item.IsRelevant())
                        local_operator_item.GetNonCstLocalOperator().SetSigmaC(raw_sigma_c);

                    SetSigmaCHelper<LocalOperatorTupleT, I + 1, TupleSizeT>::Perform(raw_sigma_c, local_operator_tuple);
                }


                template
                <
                    class LocalOperatorTupleT,
                    std::size_t TupleSizeT
                >
                void SetSigmaCHelper<LocalOperatorTupleT, TupleSizeT, TupleSizeT>
                ::Perform(ParameterAtQuadraturePoint<ParameterNS::Type::scalar>* ,
                          const LocalOperatorTupleT& )
                {
                    // Do nothing.
                }


            } // namespace SecondPiolaKirchhoffStressTensorNS


        } // namespace GlobalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_ACTIVE_STRESS_POLICY_x_INTERNAL_x_HELPER_HXX_
