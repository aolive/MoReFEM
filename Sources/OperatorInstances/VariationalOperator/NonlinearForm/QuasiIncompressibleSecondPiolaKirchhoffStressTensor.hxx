//! \file
//
//
//  QuasiIncompressibleSecondPiolaKirchhoffStressTensor.hxx
//  MoReFEM
//
//  Created by sebastien on 10/03/2018.
//Copyright © 2018 Inria. All rights reserved.
//

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_QUASI_INCOMPRESSIBLE_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_QUASI_INCOMPRESSIBLE_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HXX_


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        template
        <
            class HyperelasticityPolicyT,
            class ViscoelasticityPolicyT,
            class ActiveStressPolicyT,
            class HydrostaticLawPolicyT
        >
        QuasiIncompressibleSecondPiolaKirchhoffStressTensor
        <
            HyperelasticityPolicyT,
            ViscoelasticityPolicyT,
            ActiveStressPolicyT,
            HydrostaticLawPolicyT
        >
        ::QuasiIncompressibleSecondPiolaKirchhoffStressTensor(const FEltSpace& felt_space,
                                                              const std::array<Unknown::const_shared_ptr, 2>& unknown_list,
                                                              const std::array<Unknown::const_shared_ptr, 2>& test_unknown_list,
                                                              const Solid& solid,
                                                              const TimeManager& time_manager,
                                                              const typename HyperelasticityPolicyT::law_type* hyperelastic_law,
                                                              const HydrostaticLawPolicyT* hydrostatic_law,
                                                              const QuadratureRulePerTopology* const a_quadrature_rule_per_topology,
                                                              input_active_stress_policy_type* input_active_stress_policy)
        : stiffness_operator_parent(felt_space,
                                    unknown_list[0],
                                    test_unknown_list[0],
                                    solid,
                                    time_manager,
                                    hyperelastic_law,
                                    a_quadrature_rule_per_topology,
                                    input_active_stress_policy),
        penalization_operator_parent(felt_space,
                                     unknown_list,
                                     test_unknown_list,
                                     stiffness_operator_parent::GetCauchyGreenTensor(),
                                     hydrostatic_law,
                                     a_quadrature_rule_per_topology)
        {
            assert(!(!unknown_list[0]));
            assert(!(!unknown_list[1]));
            assert(!(!test_unknown_list[0]));
            assert(!(!test_unknown_list[1]));
            assert(unknown_list[0]->GetNature() == UnknownNS::Nature::vectorial);
            assert(test_unknown_list[0]->GetNature() == UnknownNS::Nature::vectorial);
            assert(unknown_list[1]->GetNature() == UnknownNS::Nature::scalar);
            assert(test_unknown_list[1]->GetNature() == UnknownNS::Nature::scalar);
        }


        template
        <
            class HyperelasticityPolicyT,
            class ViscoelasticityPolicyT,
            class ActiveStressPolicyT,
            class HydrostaticLawPolicyT
        >
        template<class LinearAlgebraTupleT>
        void QuasiIncompressibleSecondPiolaKirchhoffStressTensor
        <
            HyperelasticityPolicyT,
            ViscoelasticityPolicyT,
            ActiveStressPolicyT,
            HydrostaticLawPolicyT
        >
        ::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      const GlobalVector& state_previous_iteration,
                      const Domain& domain) const
        {
            stiffness_operator_parent::Assemble(linear_algebra_tuple,
                                              state_previous_iteration,
                                              domain);

            penalization_operator_parent::Assemble(linear_algebra_tuple,
                                               state_previous_iteration,
                                               domain);
        }


        template
        <
            class HyperelasticityPolicyT,
            class ViscoelasticityPolicyT,
            class ActiveStressPolicyT,
            class HydrostaticLawPolicyT
        >
        template<class LinearAlgebraTupleT>
        void QuasiIncompressibleSecondPiolaKirchhoffStressTensor
        <
            HyperelasticityPolicyT,
            ViscoelasticityPolicyT,
            ActiveStressPolicyT,
            HydrostaticLawPolicyT
        >
        ::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                   const GlobalVector& state_previous_iteration,
                   const GlobalVector& velocity_previous_iteration,
                   const Domain& domain) const
        {
            stiffness_operator_parent::Assemble(linear_algebra_tuple,
                                              state_previous_iteration,
                                              velocity_previous_iteration,
                                              domain);

            penalization_operator_parent::Assemble(linear_algebra_tuple,
                                               state_previous_iteration,
                                               domain);
        }


        template
        <
            class HyperelasticityPolicyT,
            class ViscoelasticityPolicyT,
            class ActiveStressPolicyT,
            class HydrostaticLawPolicyT
        >
        template<class LinearAlgebraTupleT>
        void QuasiIncompressibleSecondPiolaKirchhoffStressTensor
        <
            HyperelasticityPolicyT,
            ViscoelasticityPolicyT,
            ActiveStressPolicyT,
            HydrostaticLawPolicyT
        >
        ::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                          const GlobalVector& state_previous_iteration,
                          const GlobalVector& velocity_previous_iteration,
                          const GlobalVector& electrical_activation_previous_time,
                          const GlobalVector& electrical_activation_at_time,
                          const bool do_update_sigma_c,
                          const Domain& domain) const
        {
            stiffness_operator_parent::Assemble(linear_algebra_tuple,
                                              state_previous_iteration,
                                              velocity_previous_iteration,
                                              electrical_activation_previous_time,
                                              electrical_activation_at_time,
                                              do_update_sigma_c,
                                              domain);

            penalization_operator_parent::Assemble(linear_algebra_tuple,
                                               state_previous_iteration,
                                               domain);
        }



        template
        <
            class HyperelasticityPolicyT,
            class ViscoelasticityPolicyT,
            class ActiveStressPolicyT,
            class HydrostaticLawPolicyT
        >
        template<class LinearAlgebraTupleT>
        void QuasiIncompressibleSecondPiolaKirchhoffStressTensor
        <
            HyperelasticityPolicyT,
            ViscoelasticityPolicyT,
            ActiveStressPolicyT,
            HydrostaticLawPolicyT
        >
        ::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      const GlobalVector& state_previous_iteration,
                      const GlobalVector& velocity_previous_iteration,
                      const GlobalVector& electrical_activation_previous_time,
                      const GlobalVector& electrical_activation_at_time,
                      const Domain& domain) const
        {
            stiffness_operator_parent::Assemble(linear_algebra_tuple,
                                              state_previous_iteration,
                                              velocity_previous_iteration,
                                              electrical_activation_previous_time,
                                              electrical_activation_at_time,
                                              domain);

            penalization_operator_parent::Assemble(linear_algebra_tuple,
                                               state_previous_iteration,
                                               domain);
        }


        template
        <
            class HyperelasticityPolicyT,
            class ViscoelasticityPolicyT,
            class ActiveStressPolicyT,
            class HydrostaticLawPolicyT
        >
        template<class LinearAlgebraTupleT>
        void QuasiIncompressibleSecondPiolaKirchhoffStressTensor
        <
            HyperelasticityPolicyT,
            ViscoelasticityPolicyT,
            ActiveStressPolicyT,
            HydrostaticLawPolicyT
        >
        ::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      const GlobalVector& state_previous_iteration,
                      const GlobalVector& electrical_activation_previous_time,
                      const GlobalVector& electrical_activation_at_time,
                      const bool do_update_sigma_c,
                      const Domain& domain) const
        {
            stiffness_operator_parent::Assemble(linear_algebra_tuple,
                                              state_previous_iteration,
                                              electrical_activation_previous_time,
                                              electrical_activation_at_time,
                                              do_update_sigma_c,
                                              domain);

            penalization_operator_parent::Assemble(linear_algebra_tuple,
                                               state_previous_iteration,
                                               domain);
        }


        template
        <
            class HyperelasticityPolicyT,
            class ViscoelasticityPolicyT,
            class ActiveStressPolicyT,
            class HydrostaticLawPolicyT
        >
        template<class LinearAlgebraTupleT>
        void QuasiIncompressibleSecondPiolaKirchhoffStressTensor
        <
            HyperelasticityPolicyT,
            ViscoelasticityPolicyT,
            ActiveStressPolicyT,
            HydrostaticLawPolicyT
        >
        ::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      const GlobalVector& state_previous_iteration,
                      const GlobalVector& electrical_activation_previous_time,
                      const GlobalVector& electrical_activation_at_time,
                      const Domain& domain) const
        {
            stiffness_operator_parent::Assemble(linear_algebra_tuple,
                                                state_previous_iteration,
                                                electrical_activation_previous_time,
                                                electrical_activation_at_time,
                                                domain);

            penalization_operator_parent::Assemble(linear_algebra_tuple,
                                                   state_previous_iteration,
                                                   domain);
        }


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_QUASI_INCOMPRESSIBLE_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HXX_
