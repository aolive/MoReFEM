///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Dec 2016 22:00:00 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_x_PARTIAL_SPECIALIZATION_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_x_PARTIAL_SPECIALIZATION_HPP_

# include <memory>
# include <vector>

# include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/HyperelasticityPolicy/None.hpp"
# include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/ActiveStressPolicy/None.hpp"
# include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/None.hpp"
# include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/ActiveStressPolicy/AnalyticalPrestress.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace LocalVariationalOperatorNS
        {


            namespace SecondPiolaKirchhoffStressTensorNS
            {


                //! \copydoc doxygen_hide_namespace_cluttering
                namespace AdvancedCounterpart =
                    ::MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS;

                //! \copydoc doxygen_hide_namespace_cluttering
                namespace HyperelasticityPolicyNS = AdvancedCounterpart::HyperelasticityPolicyNS;

                //! \copydoc doxygen_hide_namespace_cluttering
                namespace ActiveStressPolicyNS = AdvancedCounterpart::ActiveStressPolicyNS;

                //! \copydoc doxygen_hide_namespace_cluttering
                namespace ViscoelasticityPolicyNS = AdvancedCounterpart::ViscoelasticityPolicyNS;


                /*!
                 * \brief Helper struct used to call SecondPiolaKirchhoffStressTensor::ComputeWDerivates() if the
                 * HyperelasticityPolicyT is not HyperelasticityPolicyNS::None.
                 *
                 */
                template <unsigned int DimensionT, class HyperelasticityPolicyT>
                struct ComputeWDerivatesHyperelasticity
                {


                    /*!
                     * \copydoc doxygen_hide_second_piola_compute_W_deriv_hyperelasticity
                     * \param[in,out] hyperelasticity Object which contains relevant data about hyperelasticity.
                     */
                    static void Perform(const QuadraturePoint& quad_pt,
                                        const GeometricElt& geom_elt,
                                        const Advanced::RefFEltInLocalOperator& ref_felt,
                                        const LocalVector& cauchy_green_tensor_value,
                                        HyperelasticityPolicyT& hyperelasticity,
                                        LocalVector& dW,
                                        LocalMatrix& d2W);

                };


                /*!
                 * \brief Helper struct used to call SecondPiolaKirchhoffStressTensor::ComputeWDerivates() if the
                 * HyperelasticityPolicyT is HyperelasticityPolicyNS::None.
                 *
                 * \internal <b><tt>[internal]</tt></b> Using this struct allows not to define a full-fledged HyperelasticityPolicyNS::None policy
                 * in which all the methods would be asked to do nothing.
                 *
                 */
                template<unsigned int DimensionT>
                struct ComputeWDerivatesHyperelasticity<DimensionT, HyperelasticityPolicyNS::None>
                {


                    //! Do nothing: specialization for the case hyperelastic policy is None.
                    static void Perform(const QuadraturePoint& quad_pt,
                                        const GeometricElt& geom_elt,
                                        const Advanced::RefFEltInLocalOperator& ref_felt,
                                        const LocalVector& cauchy_green_tensor_value,
                                        HyperelasticityPolicyNS::None& hyperelasticity,
                                        LocalVector& dW,
                                        LocalMatrix& d2W);
                };


                /*!
                 * \brief Helper struct used to call SecondPiolaKirchhoffStressTensor::ComputeWDerivates() if the
                 * ActiveStressPolicyT is not ActiveStressPolicyT::None.
                 *
                 */
                template<unsigned int DimensionT, class ActiveStressPolicyT>
                struct ComputeWDerivatesActiveStress
                {


                    /*!
                     * \copydoc doxygen_hide_second_piola_compute_W_derivates_active_stress
                     * \param[in,out] active_stress Object which contains relevant data about active stress.
                     */
                    static void Perform(const ::MoReFEM::Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint& infos_at_quad_pt,
                                        const GeometricElt& geom_elt,
                                        const Advanced::RefFEltInLocalOperator& ref_felt,
                                        const LocalVector& cauchy_green_tensor_value,
                                        const LocalMatrix& transposed_De,
                                        ActiveStressPolicyT& active_stress,
                                        LocalVector& dW,
                                        LocalMatrix& d2W);


                };


                /*!
                 * \brief Helper struct used to call SecondPiolaKirchhoffStressTensor::ComputeWDerivates() if the
                 * ActiveStressPolicyT is ActiveStressPolicyT::None.
                 *
                 * \internal <b><tt>[internal]</tt></b> Using this struct allows not to define a full-fledged ActiveStressPolicyT::None policy
                 * in which all the methods would be asked to do nothing.
                 *
                 */
                template <unsigned int DimensionT>
                struct ComputeWDerivatesActiveStress<DimensionT, ActiveStressPolicyNS::None>
                {


                    //! Do nothing: specialization for the case active stress policy is None.
                    static void Perform(const ::MoReFEM::Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint& infos_at_quad_pt,
                                        const GeometricElt& geom_elt,
                                        const Advanced::RefFEltInLocalOperator& ref_felt,
                                        const LocalVector& cauchy_green_tensor_value,
                                        const LocalMatrix& transposed_De,
                                        ActiveStressPolicyNS::None& active_stress,
                                        LocalVector& dW,
                                        LocalMatrix& d2W);

                };


                /*!
                 * \brief Helper struct used to call SecondPiolaKirchhoffStressTensor::CorrectRHSWithActiveSchurComplement() if the
                 * ActiveStressPolicyT is not ActiveStressPolicyT::None.
                 *
                 */
                template<class ActiveStressPolicyT>
                struct CorrectRHSWithActiveSchurComplement
                {


                    /*!
                     * \brief Add active schur complement to RHS.
                     *
                     * \param[in,out] active_stress Object that manages active stress data.
                     * \param[in,out] rhs Rhs being modified.
                     */
                    static void Perform(ActiveStressPolicyT& active_stress,
                                        LocalVector& rhs);

                };


                /*!
                 * \brief Helper struct used to call SecondPiolaKirchhoffStressTensor::CorrectRHSWithActiveSchurComplement() if the
                 * ActiveStressPolicyT is ActiveStressPolicyT::AnalyticalPrestress.
                 *
                 * \internal <b><tt>[internal]</tt></b> Using this struct allows not to define the method for
                 *  ActiveStressPolicyT::AnalyticalPrestress policy.
                 *
                 */
                template <unsigned int FiberIndexT>
                struct CorrectRHSWithActiveSchurComplement<ActiveStressPolicyNS::AnalyticalPrestress<FiberIndexT>>
                {


                    /*!
                     * \brief Add active schur complement to RHS.
                     *
                     * \param[in,out] active_stress Object that manages active stress data.
                     * \param[in,out] rhs Rhs being modified.
                     */
                    static void Perform(ActiveStressPolicyNS::AnalyticalPrestress<FiberIndexT>& active_stress,
                                        LocalVector& rhs);


                };


                /*!
                 * \brief Helper struct used to call SecondPiolaKirchhoffStressTensor::ComputeWDerivates() if the
                 * ActiveStressPolicyT is ActiveStressPolicyT::None.
                 *
                 * \internal <b><tt>[internal]</tt></b> Using this struct allows not to define a full-fledged ActiveStressPolicyT::None policy
                 * in which all the methods would be asked to do nothing.
                 *
                 */
                template <>
                struct CorrectRHSWithActiveSchurComplement<ActiveStressPolicyNS::None>
                {


                    //! Do nothing: specialization for the case active stress policy is None.
                    static void Perform(ActiveStressPolicyNS::None& ,
                                        LocalVector& );


                };


                /*!
                 * \brief Helper struct used to call SecondPiolaKirchhoffStressTensor::ComputeWDerivates() if the
                 * ViscoelasticityPolicyT is not ViscoelasticityPolicyT::None.
                 *
                 */
                template<unsigned int DimensionT, class ViscoelasticityPolicyT>
                struct ComputeWDerivatesViscoelasticity
                {


                    //! \copydoc doxygen_hide_namespace_cluttering
                    using InformationsAtQuadraturePoint = ::MoReFEM::Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint;

                    /*!
                     * \copydoc doxygen_hide_second_piola_compute_W_derivates_visco_elasticity
                     * \param[in] ref_felt Reference finite element considered.
                     * \param[in,out] viscoelasticity Object which contains relevant data about visco-elasticity.
                     */
                    static void Perform(const InformationsAtQuadraturePoint& infos_at_quad_pt,
                                        const GeometricElt& geom_elt,
                                        const Advanced::RefFEltInLocalOperator& ref_felt,
                                        const LocalMatrix& De,
                                        const LocalMatrix& transposed_De,
                                        ViscoelasticityPolicyT& viscoelasticity,
                                        LocalVector& dW,
                                        LocalMatrix& d2W);

                };


                /*!
                 * \brief Helper struct used to call SecondPiolaKirchhoffStressTensor::ComputeWDerivates() if the
                 * ViscoelasticityPolicyT is ViscoelasticityPolicyT::None.
                 *
                 * \internal <b><tt>[internal]</tt></b> Using this struct allows not to define a full-fledged ViscoelasticityPolicyT::None policy
                 * in which all the methods would be asked to do nothing.
                 *
                 */
                template<unsigned int DimensionT>
                struct ComputeWDerivatesViscoelasticity<DimensionT, ViscoelasticityPolicyNS::None>
                {


                    //! \copydoc doxygen_hide_namespace_cluttering
                    using InformationsAtQuadraturePoint = ::MoReFEM::Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint;


                    //! Do nothing: specialization for the case visco-elasticity policy is None.
                    static void Perform(const InformationsAtQuadraturePoint& ,
                                        const GeometricElt& ,
                                        const Advanced::RefFEltInLocalOperator& ,
                                        const LocalMatrix& ,
                                        const LocalMatrix& ,
                                        ViscoelasticityPolicyNS::None& ,
                                        LocalVector& ,
                                        LocalMatrix& );

                };


                /*!
                 * \brief Helper struct used to call SecondPiolaKirchhoffStressTensor::AddTangentMatrixViscoelasticity()
                 * if the ViscoelasticityPolicyT is not ViscoelasticityPolicyT::None.
                 *
                 */
                template<class ViscoelasticityPolicyT>
                struct AddTangentMatrixViscoelasticity
                {


                    /*!
                     * \brief Add contribution of the visco-elasticity to the tangent matrix.
                     *
                     * \param[in,out] tangent_matrix Tangent matrix to which visco-elasticity contribution is to be added.
                     * \param[in,out] viscoelasticity Object which handles visco-elasticity computations. Its
                     * ComputeWDerivates() method will be called.
                     */
                    static void Perform(LocalMatrix& tangent_matrix,
                                        ViscoelasticityPolicyT& viscoelasticity);

                };


                /*!
                 * \brief Helper struct used to call SecondPiolaKirchhoffStressTensor::AddTangentMatrixViscoelasticity()
                 * if the ViscoelasticityPolicyT is ViscoelasticityPolicyT::None.
                 *
                 * \internal <b><tt>[internal]</tt></b> Using this struct allows not to define a full-fledged
                 * ViscoelasticityPolicyT::None policy in which all the methods would be asked to do nothing.
                 *
                 */
                template<>
                struct AddTangentMatrixViscoelasticity<ViscoelasticityPolicyNS::None>
                {


                    //! Specialization for the ViscoelasticityPolicyNS::None case; do absolutely nothing.
                    static void Perform(LocalMatrix& ,
                                        ViscoelasticityPolicyNS::None& );


                };


            } // namespace SecondPiolaKirchhoffStressTensorNS


        } // namespace LocalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/Internal/PartialSpecialization.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_x_PARTIAL_SPECIALIZATION_HPP_
