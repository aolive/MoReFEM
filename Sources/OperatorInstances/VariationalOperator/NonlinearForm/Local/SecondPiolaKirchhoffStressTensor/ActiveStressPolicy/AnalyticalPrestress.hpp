///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Jan 2015 10:49:18 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_ACTIVE_STRESS_POLICY_x_ANALYTICAL_PRESTRESS_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_ACTIVE_STRESS_POLICY_x_ANALYTICAL_PRESTRESS_HPP_

# include <memory>
# include <vector>

# include "Utilities/Containers/EnumClass.hpp"
# include "Utilities/Numeric/Numeric.hpp"
# include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp"

#include "ThirdParty/Wrappers/Seldon/SeldonFunctions.hpp"

# include "Core/TimeManager/TimeManager.hpp"

# include "ParameterInstances/Fiber/FiberList.hpp"
# include "ParameterInstances/Fiber/Internal/FiberListManager.hpp"
# include "Parameters/Parameter.hpp"
# include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
# include "Parameters/ParameterAtQuadraturePoint.hpp"

# include "Operators/LocalVariationalOperator/ElementaryData.hpp"
# include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/ActiveStressPolicy/InputAnalyticalPrestress.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    namespace GlobalVariationalOperatorNS
    {


        namespace SecondPiolaKirchhoffStressTensorNS
        {


            namespace ActiveStressPolicyNS
            {


                template<unsigned int FiberIndexT>
                class AnalyticalPrestress;


            } // namespace ActiveStressPolicyNS


        } // namespace SecondPiolaKirchhoffStressTensorNS


    } // namespace GlobalVariationalOperatorNS


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Advanced
    {


    namespace LocalVariationalOperatorNS
    {


        namespace SecondPiolaKirchhoffStressTensorNS
        {


            namespace ActiveStressPolicyNS
            {


                /*!
                 * \brief Policy to use when \a AnalyticalPrestress is involved in \a SecondPiolaKirchhoffStressTensor.
                 *
                 * \tparam FiberIndexT Index of the relevant fiber in the \a InputParameterFile.
                 *
                 * \todo #9 (Gautier) Explain difference with InputAnalyticalPrestress.
                 */
                template <unsigned int FiberIndexT>
                class AnalyticalPrestress
                : public Crtp::LocalVectorStorage<AnalyticalPrestress<FiberIndexT>, 1ul>
                {

                public:

                    //! \copydoc doxygen_hide_alias_self
                    using self = AnalyticalPrestress<FiberIndexT>;

                    //! Alias to unique pointer.
                    using unique_ptr = std::unique_ptr<self>;

                    //! Alias to vector of unique pointers.
                    using vector_unique_ptr = std::vector<unique_ptr>;

                    //! Type of the elementary vector.
                    using vector_type = LocalVector;

                    //! Alias to the parent that provides LocalVectorStorage.
                    using vector_parent = Crtp::LocalVectorStorage<AnalyticalPrestress<FiberIndexT>, 1ul>;

                    //! Friendship to the GlobalVariationalOperator.
                    friend ::MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ActiveStressPolicyNS::AnalyticalPrestress<FiberIndexT>;

                    //! Alias to a scalar parameter at quadrature point.
                    using ScalarParameterAtQuadPt = ParameterAtQuadraturePoint<ParameterNS::Type::scalar>;

                    //! Alias to the type of the input of the policy.
                    using input_active_stress_policy_type = LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ActiveStressPolicyNS::InputAnalyticalPrestress;

                    //! Friendship to the class that calls SetSigmaC().
                    template
                    <
                        class LocalOperatorTupleT,
                        std::size_t I,
                        std::size_t TupleSizeT
                    >
                    friend struct
                        Internal::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::SetSigmaCHelper;

            public:

                    /// \name Special members.
                    ///@{

                    //! Constructor.
                    explicit AnalyticalPrestress(const unsigned int mesh_dimension,
                                                 const unsigned int Nnode_unknown,
                                                 const unsigned int Nquad_point,
                                                 const TimeManager& time_manager,
                                                 input_active_stress_policy_type* input_active_stress_policy);

                    //! Destructor.
                    ~AnalyticalPrestress() = default;

                    //! Copy constructor.
                    AnalyticalPrestress(const AnalyticalPrestress&) = delete;

                    //! Move constructor.
                    AnalyticalPrestress(AnalyticalPrestress&&) = delete;

                    //! Copy affectation.
                    AnalyticalPrestress& operator=(const AnalyticalPrestress&) = delete;

                    //! Move affectation.
                    AnalyticalPrestress& operator=(AnalyticalPrestress&&) = delete;

                    ///@}

                public:

                    /*!
                     * \class doxygen_hide_second_piola_compute_W_derivates_active_stress
                     *
                     * \brief Compute the active stress and its derivatives.
                     *
                     * \copydoc doxygen_hide_dW_d2W_derivates_arg
                     * \copydoc doxygen_hide_infos_at_quad_pt_arg
                     * \param[in] ref_felt Reference finite element considered.
                     * \param[in] geom_elt \a GeometricElt for which the computation takes place.
                     * \param[in] cauchy_green_tensor_value Value oif the CauchyGreen tensor at the current
                     * \a QuadraturePoint.
                     * \param[in] transposed_De Transposed De matrix.
                     */

                    //! \copydoc doxygen_hide_second_piola_compute_W_derivates_active_stress
                    template<unsigned int DimensionT>
                    void ComputeWDerivates(const ::MoReFEM::Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint& infos_at_quad_pt,
                                           const GeometricElt& geom_elt,
                                           const Advanced::RefFEltInLocalOperator& ref_felt,
                                           const LocalVector& cauchy_green_tensor_value,
                                           const LocalMatrix& transposed_De,
                                           LocalVector& dW,
                                           LocalMatrix& d2W);

                public:

                    //! Constant accessor to the local velocity required by ComputeEltArray().
                    const std::vector<double>& GetLocalElectricalActivationPreviousTime() const noexcept;

                    //! Non constant accessor to the local velocity required by ComputeEltArray().
                    std::vector<double>& GetNonCstLocalElectricalActivationPreviousTime() noexcept;

                    //! Constant accessor to the local velocity required by ComputeEltArray().
                    const std::vector<double>& GetLocalElectricalActivationAtTime() const noexcept;

                    //! Non constant accessor to the local velocity required by ComputeEltArray().
                    std::vector<double>& GetNonCstLocalElectricalActivationAtTime() noexcept;

                    //! Indicates if sigma_c must be advanced in time or not.
                    void SetDoUpdateSigmaC(const bool do_update);

                    //! Constant accessor to sigma_c.
                    const ParameterAtQuadraturePoint<ParameterNS::Type::scalar>& GetSigmaC() const noexcept;

                private:

                    //! Constant accessor to Time Manager.
                    const TimeManager& GetTimeManager() const noexcept;

                    //! Constant accessor to the fibers.
                    const ::MoReFEM::FiberList<ParameterNS::Type::vector>& GetFibers() const noexcept;

                    //! Non constant accessor to sigma_c.
                    ParameterAtQuadraturePoint<ParameterNS::Type::scalar>& GetNonCstSigmaC() noexcept;

                    //! Give sigma_c to the local operator.
                    void SetSigmaC(ParameterAtQuadraturePoint<ParameterNS::Type::scalar>* sigma_c);

                private:

                    //! Contant accessor to the input of the policy.
                    const input_active_stress_policy_type& GetInputActiveStressPolicy() const noexcept;

                private:

                    //! Fibers parameter.
                    const ::MoReFEM::FiberList<ParameterNS::Type::vector>& fibers_;

                    //! Active stress in the fiber direction.
                    ParameterAtQuadraturePoint<ParameterNS::Type::scalar>* sigma_c_;

                private:

                    //! Time manager.
                    const TimeManager& time_manager_;

                private:

                    /*!
                     * \brief U0 obtained at previous time iteration expressed at the local level.
                     *
                     * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within ComputeEltArray.
                     */
                    std::vector<double> local_electrical_activation_previous_time_;

                    /*!
                     * \brief U0 obtained at previous time iteration expressed at the local level.
                     *
                     * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within ComputeEltArray.
                     */
                    std::vector<double> local_electrical_activation_at_time_;

                private:

                    //! Input of the policy.
                    input_active_stress_policy_type& input_active_stress_policy_;

                private:

                    //! Boolean to know if it is the first newton iteration to advance sigma_c in time.
                    bool do_update_sigma_c_;

                private:

                    /// \name Useful indexes to fetch the work matrices and vectors.
                    ///@{

                    //! Convenient enum to alias vectors.
                    enum class LocalVectorIndex : std::size_t
                    {
                        tauxtau = 0
                    };

                    ///@}


                };


            } // namespace ActiveStressPolicyNS


        } // namespace SecondPiolaKirchhoffStressTensorNS


    } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/ActiveStressPolicy/AnalyticalPrestress.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_ACTIVE_STRESS_POLICY_x_ANALYTICAL_PRESTRESS_HPP_
