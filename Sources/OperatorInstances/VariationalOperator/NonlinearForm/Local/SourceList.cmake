target_sources(${MOREFEM_OP_INSTANCES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/NonlinearMembrane.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/FollowingPressure.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/FollowingPressure.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/MixedSolidIncompressibility.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/MixedSolidIncompressibility.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/NonlinearMembrane.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/NonlinearMembrane.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/SecondPiolaKirchhoffStressTensor.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/SecondPiolaKirchhoffStressTensor.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/SecondPiolaKirchhoffStressTensor/SourceList.cmake)
