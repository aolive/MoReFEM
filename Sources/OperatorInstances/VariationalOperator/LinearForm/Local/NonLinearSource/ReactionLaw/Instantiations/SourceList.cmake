target_sources(${MOREFEM_OP_INSTANCES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/CourtemancheRamirezNattel.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/FitzHughNagumo.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/MitchellSchaeffer.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/CourtemancheRamirezNattel.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/CourtemancheRamirezNattel.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/FitzHughNagumo.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/FitzHughNagumo.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/MitchellSchaeffer.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/MitchellSchaeffer.hxx"
)

