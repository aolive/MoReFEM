///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_LOG_I3_PENALIZATION_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_LOG_I3_PENALIZATION_HXX_


namespace MoReFEM
{


    namespace HyperelasticLawNS
    {

        inline const LogI3Penalization::scalar_parameter& LogI3Penalization::GetBulk() const noexcept
        {
            return bulk_;
        }


    } // namespace HyperelasticLawNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_LOG_I3_PENALIZATION_HXX_
