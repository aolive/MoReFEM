///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_LOG_I3_PENALIZATION_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_LOG_I3_PENALIZATION_HPP_

# include <vector>
# include <array>
# include <bitset>
# include <cmath>
# include <exception>

# include "Core/InputParameterData/InputParameterList.hpp"

# include "Parameters/Parameter.hpp"

#include "Operators/LocalVariationalOperator/CauchyAndInvariant/InvariantHolder.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class QuadraturePoint;
    class Solid;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace HyperelasticLawNS
    {


        /*!
         * \brief Ciarlet-Geymonat laws, to use a a policy of class HyperElasticityLaw.
         */
        class LogI3Penalization
        {
        public:

            //! Return the name of the hyperelastic law.
            static const std::string& ClassName();

            //! \copydoc doxygen_hide_operator_alias_scalar_parameter
            using scalar_parameter = ScalarParameter<ParameterNS::TimeDependencyNS::None>;

            //! \copydoc doxygen_hide_alias_self
            using self = LogI3Penalization;

            //! \copydoc doxygen_hide_alias_const_unique_ptr
            using const_unique_ptr = std::unique_ptr<const self>;
            
            //! Alias on the type of the invariant holder for the current law. 3 is the number of invariants.
            using invariant_holder_type = InvariantHolder<3>;
            
        public:
            
            //! Number of invariants of the law.
            static constexpr unsigned int Ninvariants() { return 3; }
            
            //! If the law needs I4 or not.
            static constexpr bool DoI4Activate() { return false; }

        public:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] solid Object which provides the required material parameters for the solid.
             */
            explicit LogI3Penalization(const Solid& solid);

            //! Destructor.
            ~LogI3Penalization() = default;

            //! Copy constructor.
            LogI3Penalization(const LogI3Penalization&) = delete;

            //! Move constructor.
            LogI3Penalization(LogI3Penalization&&) = delete;

            //! Copy affectation.
            LogI3Penalization& operator=(const LogI3Penalization&) = delete;

            //! Move affectation.
            LogI3Penalization& operator=(LogI3Penalization&&) = delete;

            ///@}

        public:


            //! Function W.
            double W(const invariant_holder_type& invariant_holder,
                     const QuadraturePoint& quadrature_point,
                     const GeometricElt& geom_elt) const;

            //! Derivative of W with respect of third invariant (dWdI3)
            double FirstDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                                  const QuadraturePoint& quadrature_point,
                                                  const GeometricElt& geom_elt) const;

            //! Second derivative of W with respect of third invariant (d2WdI3dI3)
            double SecondDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                                   const QuadraturePoint& quadrature_point,
                                                   const GeometricElt& geom_elt) const;


        public:

            //! Hyperelastic bulk.
            const scalar_parameter& GetBulk() const noexcept;

        private:

            //! Bulk.
            const scalar_parameter& bulk_;

        };


    } // namespace HyperelasticLawNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/HyperelasticLaws/LogI3Penalization.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_LOG_I3_PENALIZATION_HPP_
