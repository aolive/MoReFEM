///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 6 Oct 2017 11:34:26 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

/*!
 \file ExponentialJ1J4.h
 \authors S. Gilles
 \date 25/03/2013
 \brief Class in charge of Ciarlet-Geymonat laws
 */

#include <string>

#include "Utilities/Numeric/Numeric.hpp"
#include "Utilities/Containers/EnumClass.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "Operators/LocalVariationalOperator/CauchyAndInvariant/InvariantHolder.hpp"
#include "OperatorInstances/HyperelasticLaws/ExponentialJ1J4.hpp"



namespace MoReFEM
{
    
    
    namespace HyperelasticLawNS
    {
        
        
        const std::string& ExponentialJ1J4::ClassName()
        {
            static std::string ret("ExponentialJ1J4");
            return ret;
        }
        
        
        ExponentialJ1J4::ExponentialJ1J4(const Solid& solid,
                                         const FiberList<ParameterNS::Type::vector>* fibers)
        : mu_1_(solid.GetMu1()),
        mu_2_(solid.GetMu2()),
        c_0_(solid.GetC0()),
        c_1_(solid.GetC1()),
        c_2_(solid.GetC2()),
        c_3_(solid.GetC3()),
        bulk_(solid.GetHyperelasticBulk()),
        fibers_(fibers)
        { }
        
        double ExponentialJ1J4::W(const invariant_holder_type& invariant_holder,
                                  const QuadraturePoint& quad_pt,
                                  const GeometricElt& geom_elt) const
        {
            const double I1 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I1);
            const double I2 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I2);
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            const double I4 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I4);
            const double I3_pow_minus_one_third = std::pow(I3, -1. / 3.);
            const double sqrt_I3 = std::sqrt(I3);
            const double expI1 = std::exp(GetC1().GetValue(quad_pt, geom_elt) * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));
            const double expI4 = std::exp(GetC3().GetValue(quad_pt, geom_elt) * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));
            
            return GetMu1().GetValue(quad_pt, geom_elt) * (I1 * I3_pow_minus_one_third - 3.)
            + GetMu2().GetValue(quad_pt, geom_elt) * (I2 * NumericNS::Square(I3_pow_minus_one_third) - 3.)
            + GetC0().GetValue(quad_pt, geom_elt) * expI1
            + GetC2().GetValue(quad_pt, geom_elt) * expI4
            + GetBulk().GetValue(quad_pt, geom_elt) * (sqrt_I3 - 1. - std::log(sqrt_I3));
        }
        
        double ExponentialJ1J4::FirstDerivativeWFirstInvariant(const invariant_holder_type& invariant_holder,
                                                               const QuadraturePoint& quad_pt,
                                                               const GeometricElt& geom_elt) const
        {
            const double I1 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I1);
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            const double I3_pow_minus_one_third = std::pow(I3, -1. / 3.);
            const double I3_pow_one_third = std::pow(I3, 1. / 3.);
            
            const double mu1 = GetMu1().GetValue(quad_pt, geom_elt);
            const double C0 = GetC0().GetValue(quad_pt, geom_elt);
            const double C1 = GetC1().GetValue(quad_pt, geom_elt);
            
            const double expI1 = std::exp(C1 * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));;
            
            return 2. * C0 * C1 * NumericNS::Square(I3_pow_minus_one_third) * (I1 - 3. * I3_pow_one_third) * expI1
            + mu1 * I3_pow_minus_one_third;
        }
        
        
        double ExponentialJ1J4::FirstDerivativeWSecondInvariant(const invariant_holder_type& invariant_holder,
                                                                const QuadraturePoint& quad_pt,
                                                                const GeometricElt& geom_elt) const
        {
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            const double mu2 = GetMu2().GetValue(quad_pt, geom_elt);
            
            return mu2 * std::pow(I3, -2. / 3.);
        }
        
        
        double ExponentialJ1J4::FirstDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                                               const QuadraturePoint& quad_pt,
                                                               const GeometricElt& geom_elt) const
        {
            const double I1 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I1);
            const double I2 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I2);
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            const double I4 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I4);
            
            constexpr const double minus_one_third = -1. / 3.;
            constexpr const double two_third = 2. / 3.;
            
            const double I3_pow_one_third = std::pow(I3, 1. / 3.);
            const double I3_pow_minus_one_third = std::pow(I3, -1. / 3.);
            
            const double mu1 = GetMu1().GetValue(quad_pt, geom_elt);
            const double mu2 = GetMu1().GetValue(quad_pt, geom_elt);
            const double C0 = GetC0().GetValue(quad_pt, geom_elt);
            const double C1 = GetC1().GetValue(quad_pt, geom_elt);
            const double C2 = GetC2().GetValue(quad_pt, geom_elt);
            const double C3 = GetC3().GetValue(quad_pt, geom_elt);
            const double kappa = GetBulk().GetValue(quad_pt, geom_elt);
            
            const double expI1 = std::exp(GetC1().GetValue(quad_pt, geom_elt) * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));
            const double expI4 = std::exp(GetC3().GetValue(quad_pt, geom_elt) * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));
            
            return - two_third * C0 * C1 * std::pow(I3, 5. * minus_one_third) * I1 * (I1 - 3. * I3_pow_one_third) * expI1
            + two_third * C2 * C3 * std::pow(I3, 5. * minus_one_third) * I4 * (I3_pow_one_third - I4) * expI4
            + mu1 * minus_one_third * I1 * std::pow(I3, 4. * minus_one_third)
            + 2. * mu2 * minus_one_third * I2 * std::pow(I3, 5. * minus_one_third)
            + kappa * (0.5 * std::pow(I3, -0.5) - 0.5 / I3);
        }
        
        double ExponentialJ1J4::FirstDerivativeWFourthInvariant(const invariant_holder_type& invariant_holder,
                                                                const QuadraturePoint& quad_pt,
                                                                const GeometricElt& geom_elt) const
        {
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            const double I4 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I4);
            
            const double I3_pow_one_third = std::pow(I3, 1. / 3.);
            const double I3_pow_minus_one_third = std::pow(I3, -1. / 3.);
            
            const double C2 = GetC2().GetValue(quad_pt, geom_elt);
            const double C3 = GetC3().GetValue(quad_pt, geom_elt);
            
            const double expI4 = std::exp(GetC3().GetValue(quad_pt, geom_elt) * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));
            
            return - 2. * C2 * C3 * NumericNS::Square(I3_pow_minus_one_third) * (I3_pow_one_third - I4) * expI4;
        }
        
        double ExponentialJ1J4::SecondDerivativeWFirstInvariant(const invariant_holder_type& invariant_holder,
                                                                const QuadraturePoint& quad_pt,
                                                                const GeometricElt& geom_elt) const
        {
            const double I1 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I1);
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            const double I3_pow_minus_one_third = std::pow(I3, -1. / 3.);
            const double I3_pow_one_third = std::pow(I3, 1. / 3.);
            
            const double C0 = GetC0().GetValue(quad_pt, geom_elt);
            const double C1 = GetC1().GetValue(quad_pt, geom_elt);
            
            const double expI1 = std::exp(C1 * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));
            
            return 2. * C0 * C1 * std::pow(I3, -4. / 3.) * (2. * C1 * NumericNS::Square(I1 - 3. * I3_pow_one_third) + NumericNS::Square(I3_pow_one_third)) * expI1;
        }
        
        
        double ExponentialJ1J4::SecondDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                                                const QuadraturePoint& quad_pt,
                                                                const GeometricElt& geom_elt) const
        {
            const double I1 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I1);
            const double I2 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I2);
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            const double I4 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I4);
            
            constexpr const double minus_one_third = -1. / 3.;
            constexpr const double two_ninth = 2. / 9.;
            
            const double I3_pow_minus_one_third = std::pow(I3, minus_one_third);
            const double I3_pow_one_third = std::pow(I3, 1. / 3.);
            
            const double mu1 = GetMu1().GetValue(quad_pt, geom_elt);
            const double mu2 = GetMu1().GetValue(quad_pt, geom_elt);
            const double C0 = GetC0().GetValue(quad_pt, geom_elt);
            const double C1 = GetC1().GetValue(quad_pt, geom_elt);
            const double C2 = GetC2().GetValue(quad_pt, geom_elt);
            const double C3 = GetC3().GetValue(quad_pt, geom_elt);
            
            const double expI1 = std::exp(C1 * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));
            const double expI4 = std::exp(C3 * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));
            
            return two_ninth * std::pow(I3, -10. / 3.) * (C0 * C1 * expI1 * I1 * (2. * C1 * I1 * NumericNS::Square(I1 - 3. * I3_pow_one_third) + 5. * I1 * NumericNS::Square(I3_pow_one_third) - 12. * I3)
                                                          + C2 * C3 * expI4 * I4 * (-4. * I3 + (5. + 2. * C3) * I4 * NumericNS::Square(I3_pow_one_third) - 4. * C3 * I3_pow_one_third * NumericNS::Square(I4) + 2. * C3 * NumericNS::Cube(I4)))
            + 4. / 9. * mu1 * I1 * std::pow(I3, -7. / 3.)
            + 10. / 9. * mu2 * I2 * std::pow(I3, -8. / 3.)
            + GetBulk().GetValue(quad_pt, geom_elt) * (0.5 * std::pow(I3, -2.) - 0.25 * std::pow(I3, -1.5));
        }
        
        double ExponentialJ1J4::SecondDerivativeWFourthInvariant(const invariant_holder_type& invariant_holder,
                                                                 const QuadraturePoint& quad_pt,
                                                                 const GeometricElt& geom_elt) const
        {
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            const double I4 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I4);
            
            constexpr const double minus_one_third = -1. / 3.;
            
            const double I3_pow_minus_one_third = std::pow(I3, minus_one_third);
            
            const double C2 = GetC2().GetValue(quad_pt, geom_elt);
            const double C3 = GetC3().GetValue(quad_pt, geom_elt);
            
            const double expI4 = std::exp(C3 * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));
            
            return 2. * C2 * C3 * std::pow(I3, 4 * minus_one_third)* expI4 * ((1. + 2. * C3) * std::pow(I3, 2. / 3.) - 4. * C3 * std::pow(I3, 1. / 3.) * I4 + 2. * C3 * NumericNS::Square(I4));
        }
        
        
        
        double ExponentialJ1J4::SecondDerivativeWFirstAndThirdInvariant(const invariant_holder_type& invariant_holder,
                                                                        const QuadraturePoint& quad_pt,
                                                                        const GeometricElt& geom_elt) const
        {
            const double I1 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I1);
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            
            constexpr const double minus_one_third = -1. / 3.;
            
            const double I3_pow_minus_one_third = std::pow(I3, minus_one_third);
            
            const double I3_pow_one_third = std::pow(I3, 1. / 3.);
            
            constexpr const double minus_two_third = -2. / 3.;
            
            const double mu1 = GetMu1().GetValue(quad_pt, geom_elt);
            const double C0 = GetC0().GetValue(quad_pt, geom_elt);
            const double C1 = GetC1().GetValue(quad_pt, geom_elt);
            
            const double expI1 = std::exp(C1 * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));
            
            return minus_two_third * C0 * C1 * expI1 * std::pow(I3, -7. / 3.) * (2. * C1 * I1 * NumericNS::Square(I1 - 3. * I3_pow_one_third) + 2. * I1 * NumericNS::Square(I3_pow_one_third) - 3. * I3)
            - 1. / 3. * mu1 * std::pow(I3, 2. * minus_two_third);
        }
        
        
        double ExponentialJ1J4::SecondDerivativeWSecondAndThirdInvariant(const invariant_holder_type& invariant_holder,
                                                                         const QuadraturePoint& quad_pt,
                                                                         const GeometricElt& geom_elt) const
        {
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            constexpr const double minus_one_third = -1. / 3.;
            const double mu2 = GetMu2().GetValue(quad_pt, geom_elt);
            
            return mu2 * 2. * minus_one_third * std::pow(I3, 5. * minus_one_third);
        }
        
        
        double ExponentialJ1J4::SecondDerivativeWThirdAndFourthInvariant(const invariant_holder_type& invariant_holder,
                                                                         const QuadraturePoint& quadrature_point,
                                                                         const GeometricElt& geom_elt) const
        {
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            const double I4 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I4);
            
            constexpr const double minus_one_third = -1. / 3.;
            constexpr const double minus_two_third = 2. * minus_one_third;
            
            const double I3_pow_minus_one_third = std::pow(I3, minus_one_third);
            
            const double C2 = GetC2().GetValue(quadrature_point, geom_elt);
            const double C3 = GetC3().GetValue(quadrature_point, geom_elt);
            
            const double expI4 = std::exp(C3 * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));
            
            return  minus_two_third * std::pow(I3, -7. / 3.) * C2 * C3 * expI4 * (-I3 + 2. * (1. + C3) * std::pow(I3, 2. / 3.) * I4
                                                                                  - 4. * C3 * std::pow(I3, 1. / 3.) * I4 * I4
                                                                                  + 2. * C3 * NumericNS::Cube(I4));
        }
        


    } // namespace HyperelasticLawNS
    
    
} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup

