///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 14 Dec 2015 12:18:51 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/DofProgramWiseIndexListPerVertexCoordIndexList.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/DofProgramWiseIndexListPerVertexCoordIndexListManager.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"

#include "OperatorInstances/NonConformInterpolator/FromVertexMatching/FromVertexMatching.hpp"


namespace MoReFEM
{
    
    
    namespace NonConformInterpolatorNS
    {
        
        
        void FromVertexMatching::Construct(const MeshNS::InterpolationNS::VertexMatching& vertex_matching,
                                           const unsigned int source_index,
                                           const unsigned int target_index,
                                           store_matrix_pattern do_store_matrix_pattern)
        {
            auto& init_vertex_matching_manager =
                Internal::FEltSpaceNS::DofProgramWiseIndexListPerVertexCoordIndexListManager::GetInstance(__FILE__, __LINE__);

            decltype(auto) source =
                init_vertex_matching_manager.GetDofProgramWiseIndexListPerVertexCoordIndexList(source_index);
            
            decltype(auto) target =
                init_vertex_matching_manager.GetDofProgramWiseIndexListPerVertexCoordIndexList(target_index);
            
            interpolation_matrix_ = std::make_unique<GlobalMatrix>(target.GetNumberingSubset(),
                                                                   source.GetNumberingSubset());
            
            decltype(auto) source_dof_index_list_per_coord_index = source.GetDofProgramWiseIndexPerCoordIndexList();
            decltype(auto) target_dof_list_per_proc_wise_coord_index = target.GetDofProgramWiseIndexPerCoordIndexList();
            
            const auto Nprocessor_wise_source_dof = source.ComputeNprocessorWisedof();
            const auto Nprocessor_wise_target_dof = target.ComputeNprocessorWisedof();
            
            const auto Nprogram_wise_source_dof = source.NprogramWisedof();
            const auto Nprogram_wise_target_dof = target.NprogramWisedof();
            
            assert(Nprogram_wise_source_dof == Nprogram_wise_target_dof);
            assert(!source_dof_index_list_per_coord_index.empty());
            
            std::vector<std::vector<PetscInt>> non_zero_pattern_per_line(Nprocessor_wise_target_dof);
            
            const auto dumb_value = static_cast<PetscInt>(-1);
            
            std::vector<PetscInt> target_processor_to_program_wise(Nprocessor_wise_target_dof,
                                                                   dumb_value);
            
            decltype(auto) target_numbering_subset = target.GetNumberingSubset();

            const auto complete_dof_list_target = target.GetFEltSpace().GetProcessorWiseDofList(target_numbering_subset);
            
            const auto end_dof_list_target = complete_dof_list_target.cend();
            
            for (const auto& pair : target_dof_list_per_proc_wise_coord_index)
            {
                // Use geometric data to make source and target vertices match.
                // Consider only Coords handled processor-wisely.
                decltype(auto) target_coords_index_list = pair.first;
                
                std::vector<unsigned int> source_coords_index_list;
                
                source_coords_index_list.resize(target_coords_index_list.size());
                
                const auto end_target_coords_index_list = target_coords_index_list.cend();
                
                const auto end_source_coords_index_list =
                    std::transform(target_coords_index_list.cbegin(),
                                   end_target_coords_index_list,
                                   source_coords_index_list.begin(),
                                   [&vertex_matching](const unsigned int local_target_index)
                                   {
                                       return vertex_matching.FindSourceIndex(local_target_index);
                                   });
                
                static_cast<void>(end_source_coords_index_list);
                assert(static_cast<std::size_t>(end_source_coords_index_list - source_coords_index_list.begin())
                       == target_coords_index_list.size());
                
                // Then read the associated dof data.
                const auto it_source = source_dof_index_list_per_coord_index.find(source_coords_index_list);
                assert(it_source != source_dof_index_list_per_coord_index.cend());
                    
                const auto& source_dof_list = it_source->second;
                const auto& target_dof_list = pair.second;
                
                const auto Ndof_for_vertex = source_dof_list.size();
                assert(Ndof_for_vertex == target_dof_list.size());
                
                for (auto j = 0ul; j < Ndof_for_vertex; ++j)
                {
                    // \todo #775 Ordering of the unknowns shouldn't be implicitly assumed.
                    const auto target_program_wise_index = target_dof_list[j];
                    const auto source_program_wise_index = source_dof_list[j];
                    
                    // Identify the dof objects: we need respectively processor- and program-wise indexes for row
                    // and columns.
                    const auto it_target =
                        std::find_if(complete_dof_list_target.cbegin(),
                                     end_dof_list_target,
                                     [target_program_wise_index, &target_numbering_subset](const auto& dof_ptr)
                                     {
                                         assert(!(!dof_ptr));
                                         return dof_ptr->GetProgramWiseIndex(target_numbering_subset) == target_program_wise_index;
                                     });
                    
                    // This might happen: a Coords may exist processor-wise but bear no dofs (they are in this case born
                    // by another processor).
                    if (it_target == end_dof_list_target)
                        continue;
                    
                    assert(!(!*it_target));
                    const auto& target_dof = *(*it_target);

                    const auto target_processor_wise_index = target_dof.GetProcessorWiseOrGhostIndex(target_numbering_subset);
                    
                    assert(target_processor_wise_index < Nprocessor_wise_target_dof);
                    assert(source_program_wise_index < Nprogram_wise_source_dof);
                    
                    // I reconstitute here a processor-2-program-wise array, usually handled through Dof object
                    // (can't be used here - see Internal::DofProgramWiseIndexListPerVertexCoordIndexList for more about it).
                    non_zero_pattern_per_line[static_cast<std::size_t>(target_processor_wise_index)] =
                        { static_cast<PetscInt>(source_program_wise_index) };
                    
                    target_processor_to_program_wise[static_cast<std::size_t>(target_processor_wise_index)] =
                        static_cast<PetscInt>(target_program_wise_index);
                }
            }
            
            assert(std::none_of(target_processor_to_program_wise.cbegin(),
                                target_processor_to_program_wise.cend(),
                                [dumb_value](auto value)
                                {
                                    return value == dumb_value;
                                }));
            
            
            matrix_pattern_ = std::make_unique<Wrappers::Petsc::MatrixPattern>(non_zero_pattern_per_line);
            
            decltype(auto) matrix_pattern = *matrix_pattern_;
            
            auto& interpolation_matrix = *interpolation_matrix_;
            
            const auto& mpi = source.GetFEltSpace().GetMpi();
            
            if (mpi.Nprocessor<int>() == 1)
            {
                assert(Nprocessor_wise_source_dof == Nprocessor_wise_target_dof);
                
                interpolation_matrix.InitSequentialMatrix(static_cast<unsigned int>(Nprocessor_wise_target_dof),
                                                          static_cast<unsigned int>(Nprocessor_wise_source_dof),
                                                          matrix_pattern,
                                                          mpi,
                                                          __FILE__, __LINE__);
            }
            else
            {
                assert(Nprogram_wise_source_dof == Nprogram_wise_target_dof);
                
                interpolation_matrix.InitParallelMatrix(static_cast<unsigned int>(Nprocessor_wise_target_dof),
                                                        static_cast<unsigned int>(Nprocessor_wise_source_dof),
                                                        Nprogram_wise_target_dof,
                                                        Nprogram_wise_source_dof,
                                                        matrix_pattern,
                                                        mpi,
                                                        __FILE__, __LINE__);
            }
            
            if (do_store_matrix_pattern == store_matrix_pattern::no)
                matrix_pattern_ = nullptr;
            
            // Fill the interpolation matrix.
            std::vector<PetscScalar> one { 1. };
                    
            assert(non_zero_pattern_per_line.size() == Nprocessor_wise_target_dof);
            assert(target_processor_to_program_wise.size() == Nprocessor_wise_target_dof);
            
            std::vector<PetscInt> source_index_list;
            source_index_list.reserve(Nprocessor_wise_target_dof);
            
            for (decltype(auto) item : non_zero_pattern_per_line)
            {
                assert(item.size() == 1ul);
                source_index_list.push_back(item.back());
            }
            
            std::vector<PetscScalar> one_list(Nprocessor_wise_target_dof, 1.);

            for (auto i = 0ul; i < Nprocessor_wise_target_dof; ++i)
            {
                interpolation_matrix.SetValue(target_processor_to_program_wise[i],
                                              source_index_list[i],
                                              1.,
                                              INSERT_VALUES,
                                              __FILE__, __LINE__);
            }
            
            interpolation_matrix.Assembly(__FILE__, __LINE__);
        }
        
        
    } // namespace NonConformInterpolatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
