///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Mar 2016 12:06:54 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_P1B_xTO_x_P1_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_P1B_xTO_x_P1_HPP_

# include <memory>
# include <vector>

# include "OperatorInstances/ConformInterpolator/Local/Internal/Phigher_to_P1.hpp"


namespace MoReFEM
{


    namespace ConformInterpolatorNS
    {


        namespace Local
        {


            /*!
             * \brief Local operator that defines P1 -> P1b interpolation.
             */
            class P1b_to_P1
            : public ::MoReFEM::Internal::ConformInterpolatorNS::Local::Phigher_to_P1
            {

            public:

                //! \copydoc doxygen_hide_alias_self
                using self = P1b_to_P1;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Alias to vector of unique pointers.
                using vector_unique_ptr = std::vector<unique_ptr>;

                //! Class name.
                static const std::string& ClassName();

                //! Label of the source shape function label.
                static const std::string& GetSourceShapeFunctionLabel();

                //! Parent.
                using parent = ::MoReFEM::Internal::ConformInterpolatorNS::Local::Phigher_to_P1;

            public:

                /// \name Special members.
                ///@{

                //! Constructor.
                explicit P1b_to_P1(const FEltSpace& source_felt_space,
                                   const Internal::RefFEltNS::RefLocalFEltSpace& target_ref_local_felt_space,
                                   const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data);


                //! Destructor.
                virtual ~P1b_to_P1();

                //! Copy constructor.
                P1b_to_P1(const P1b_to_P1&) = delete;

                //! Move constructor.
                P1b_to_P1(P1b_to_P1&&) = delete;

                //! Copy affectation.
                P1b_to_P1& operator=(const P1b_to_P1&) = delete;

                //! Move affectation.
                P1b_to_P1& operator=(P1b_to_P1&&) = delete;

                ///@}

            private:



            };



        } // namespace Local


    } // namespace ConformInterpolatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_P1B_xTO_x_P1_HPP_
