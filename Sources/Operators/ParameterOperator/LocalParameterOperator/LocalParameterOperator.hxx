///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 18 Mar 2016 17:07:38 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_PARAMETER_OPERATOR_x_LOCAL_PARAMETER_OPERATOR_x_LOCAL_PARAMETER_OPERATOR_HXX_
# define MOREFEM_x_OPERATORS_x_PARAMETER_OPERATOR_x_LOCAL_PARAMETER_OPERATOR_x_LOCAL_PARAMETER_OPERATOR_HXX_


namespace MoReFEM
{


    namespace Advanced
    {


        template
        <
            ParameterNS::Type TypeT,
            template<ParameterNS::Type> class TimeDependencyT
        >
        LocalParameterOperator<TypeT, TimeDependencyT>
        ::LocalParameterOperator(const ExtendedUnknown::const_shared_ptr& unknown,
                                 elementary_data_type&& elementary_data,
                                 ParameterAtQuadraturePoint<TypeT, TimeDependencyT>& parameter)
        : elementary_data_(std::move(elementary_data)),
        parameter_(parameter),
        extended_unknown_(unknown)
        { }


        template
        <
            ParameterNS::Type TypeT,
            template<ParameterNS::Type> class TimeDependencyT
        >
        inline const typename LocalParameterOperator<TypeT, TimeDependencyT>::elementary_data_type&
        LocalParameterOperator<TypeT, TimeDependencyT>::GetElementaryData() const
        {
            return elementary_data_;
        }


        template
        <
            ParameterNS::Type TypeT,
            template<ParameterNS::Type> class TimeDependencyT
        >
        inline typename LocalParameterOperator<TypeT, TimeDependencyT>::elementary_data_type&
        LocalParameterOperator<TypeT, TimeDependencyT>::GetNonCstElementaryData()
        {
            return elementary_data_;
        }


        template
        <
            ParameterNS::Type TypeT,
            template<ParameterNS::Type> class TimeDependencyT
        >
        inline const ParameterAtQuadraturePoint<TypeT, TimeDependencyT>&
        LocalParameterOperator<TypeT, TimeDependencyT>::GetParameter() const noexcept
        {
            return parameter_;
        }


        template
        <
            ParameterNS::Type TypeT,
            template<ParameterNS::Type> class TimeDependencyT
        >
        inline ParameterAtQuadraturePoint<TypeT, TimeDependencyT>&
        LocalParameterOperator<TypeT, TimeDependencyT>::GetNonCstParameter()
        {
            return parameter_;
        }


        template
        <
            ParameterNS::Type TypeT,
            template<ParameterNS::Type> class TimeDependencyT
        >
        inline void LocalParameterOperator<TypeT, TimeDependencyT>
        ::SetLocalFEltSpace(const LocalFEltSpace& local_felt_space)
        {
            auto& elementary_data = GetNonCstElementaryData();
            elementary_data.ComputeLocalFEltSpaceData(local_felt_space);
            elementary_data.Zero();
        }


        template
        <
            ParameterNS::Type TypeT,
            template<ParameterNS::Type> class TimeDependencyT
        >
        inline const ExtendedUnknown::const_shared_ptr&
        LocalParameterOperator<TypeT, TimeDependencyT>::GetExtendedUnknownPtr() const
        {
            assert(!(!extended_unknown_));
            return extended_unknown_;
        }


        template
        <
            ParameterNS::Type TypeT,
            template<ParameterNS::Type> class TimeDependencyT
        >
        inline const ExtendedUnknown&
        LocalParameterOperator<TypeT, TimeDependencyT>::GetExtendedUnknown() const
        {
            assert(!(!extended_unknown_));
            return *extended_unknown_;
        }


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_PARAMETER_OPERATOR_x_LOCAL_PARAMETER_OPERATOR_x_LOCAL_PARAMETER_OPERATOR_HXX_
