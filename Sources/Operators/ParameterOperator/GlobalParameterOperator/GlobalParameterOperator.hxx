///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 29 Apr 2014 11:27:59 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_PARAMETER_OPERATOR_x_GLOBAL_PARAMETER_OPERATOR_x_GLOBAL_PARAMETER_OPERATOR_HXX_
# define MOREFEM_x_OPERATORS_x_PARAMETER_OPERATOR_x_GLOBAL_PARAMETER_OPERATOR_x_GLOBAL_PARAMETER_OPERATOR_HXX_


namespace MoReFEM
{


    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        template<ParameterNS::Type> class TimeDependencyT
    >
    template
    <
        typename... Args
    >
    GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeDependencyT>
    ::GlobalParameterOperator(const FEltSpace& felt_space,
                              const Unknown& unknown,
                              const QuadratureRulePerTopology* const quadrature_rule_per_topology,
                              AllocateGradientFEltPhi do_allocate_gradient_felt_phi,
                              ParameterAtQuadraturePoint<TypeT, TimeDependencyT>& parameter,
                              Args&&... args)
    : felt_space_(felt_space),
    quadrature_rule_per_topology_(quadrature_rule_per_topology),
    parameter_(parameter),
    do_allocate_gradient_felt_phi_(do_allocate_gradient_felt_phi),
    extended_unknown_(felt_space.GetExtendedUnknownPtr(unknown))
    {
        local_operator_per_ref_geom_elt_.max_load_factor(Utilities::DefaultMaxLoadFactor());

        CreateLocalOperatorList(felt_space.GetMeshDimension(),
                                parameter,
                                std::forward<decltype(args)>(args)...);
    }


    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        template<ParameterNS::Type> class TimeDependencyT
    >
    inline const FEltSpace& GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeDependencyT>
    ::GetFEltSpace() const noexcept
    {
        return felt_space_;
    }


    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        template<ParameterNS::Type> class TimeDependencyT
    >
    template<typename... Args>
    void GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeDependencyT>
    ::UpdateImpl(Args&&... args) const
    {
        const auto& felt_storage = this->GetFEltSpace().GetLocalFEltSpacePerRefLocalFEltSpace();

        for (const auto& pair : felt_storage)
        {
            auto& ref_felt_space_ptr = pair.first;
            assert(!(!ref_felt_space_ptr));
            const auto& ref_felt_space = *ref_felt_space_ptr;

            const auto& ref_geom_elt = ref_felt_space.GetRefGeomElt();
            const auto ref_geom_elt_id = ref_geom_elt.GetIdentifier();

            if (!DoConsider(ref_geom_elt_id))
                continue;

            auto& local_operator = GetNonCstLocalOperator(ref_geom_elt_id);

            const auto& local_felt_space_list = pair.second;

            for (const auto& local_felt_space_pair : local_felt_space_list)
            {
                const auto& local_felt_space_ptr = local_felt_space_pair.second;
                assert(!(!local_felt_space_ptr));

                auto& local_felt_space = *local_felt_space_ptr;
                local_operator.SetLocalFEltSpace(local_felt_space);

                PerformElementaryCalculation(local_felt_space,
                                             local_operator,
                                             std::forward_as_tuple(args...));
            }
        }
    }


    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        template<ParameterNS::Type> class TimeDependencyT
    >
    LocalParameterOperatorT&
    GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeDependencyT>
    ::GetNonCstLocalOperator(Advanced::GeometricEltEnum ref_geom_elt_id) const
    {
        auto it = local_operator_per_ref_geom_elt_.find(ref_geom_elt_id);
        assert(it != local_operator_per_ref_geom_elt_.cend());
        return *(it->second);
    }


    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        template<ParameterNS::Type> class TimeDependencyT
    >
    bool GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeDependencyT>
    ::DoConsider(Advanced::GeometricEltEnum ref_geom_elt_id) const
    {
        auto it = local_operator_per_ref_geom_elt_.find(ref_geom_elt_id);
        return it != local_operator_per_ref_geom_elt_.cend();
    }



    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        template<ParameterNS::Type> class TimeDependencyT
    >
    template<typename... Args>
    void
    GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeDependencyT>
    ::PerformElementaryCalculation(const LocalFEltSpace& local_felt_space,
                                   LocalParameterOperatorT& local_operator,
                                   std::tuple<Args...>&& additional_arguments) const
    {
        static_assert(std::tuple_size<std::tuple<Args...>>::value > 0,
                      "If no supplementary arguments, the overload of current method should have been called. It is "
                      "likely it was not because last template parameter in LocalParameterOperatorT "
                      "was not set to std::false_type.");

        static_cast<const DerivedT&>(*this).SetComputeEltArrayArguments(local_felt_space,
                                                                        local_operator,
                                                                        std::move(additional_arguments));

        local_operator.ComputeEltArray();
    }


    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        template<ParameterNS::Type> class TimeDependencyT
    >
    void
    GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeDependencyT>
    ::PerformElementaryCalculation(const LocalFEltSpace& local_felt_space,
                                   LocalParameterOperatorT& local_operator,
                                   std::tuple<>&& ) const
    {
        static_cast<void>(local_felt_space);

        local_operator.ComputeEltArray();
    }


    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        template<ParameterNS::Type> class TimeDependencyT
    >
    template<typename... Args>
    void GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeDependencyT>
    ::CreateLocalOperatorList(const unsigned int mesh_dimension,
                              ParameterAtQuadraturePoint<TypeT, ParameterNS::TimeDependencyNS::None>& parameter,
                              Args&&... args)
    {
        const auto& felt_space = GetFEltSpace();
        const auto& felt_storage = felt_space.GetLocalFEltSpacePerRefLocalFEltSpace();

        if (felt_storage.empty())
            std::cout << "[WARNING] Finite element space related to operator " << DerivedT::ClassName() << " is empty! "
            "It might be due for instance to a non existing label for the dimension considered here (namely "
            << GetFEltSpace().GetDimension() << ")." << std::endl;

        using pair_type = typename decltype(local_operator_per_ref_geom_elt_)::value_type;

        const auto& unknown_storage = this->GetExtendedUnknownPtr();

        for (const auto& pair : felt_storage)
        {
            const auto& ref_felt_space_ptr = pair.first;
            assert(!(!ref_felt_space_ptr));
            const auto& ref_felt_space = *ref_felt_space_ptr;

            decltype(auto) ref_geom_elt = ref_felt_space.GetRefGeomElt();
            const auto ref_geom_elt_id = ref_geom_elt.GetIdentifier();

            assert(local_operator_per_ref_geom_elt_.find(ref_geom_elt_id)
                   == local_operator_per_ref_geom_elt_.cend());

            const auto& quadrature_rule = GetQuadratureRule(ref_geom_elt);

            typename LocalParameterOperatorT::elementary_data_type
            elementary_data(ref_felt_space,
                            quadrature_rule,
                            { unknown_storage },
                            { unknown_storage },
                            felt_space.GetDimension(),
                            mesh_dimension,
                            DoAllocateGradientFEltPhi());

            auto&& local_operator_ptr =
                std::make_unique<LocalParameterOperatorT>(unknown_storage,
                                                          std::move(elementary_data),
                                                          parameter,
                                                          std::forward<decltype(args)>(args)...);

            pair_type&& ret_pair{ref_geom_elt_id, std::move(local_operator_ptr)};

            local_operator_per_ref_geom_elt_.insert(std::move(ret_pair));
        }
    }


    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        template<ParameterNS::Type> class TimeDependencyT
    >
    inline const QuadratureRule& GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeDependencyT>
    ::GetQuadratureRule(const RefGeomElt& ref_geom_elt) const
    {
        return GetQuadratureRulePerTopology().GetRule(ref_geom_elt.GetTopologyIdentifier());
    }



    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        template<ParameterNS::Type> class TimeDependencyT
    >
    const QuadratureRulePerTopology&
    GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeDependencyT>
    ::GetQuadratureRulePerTopology() const noexcept
    {
        return (quadrature_rule_per_topology_ == nullptr
                ? GetFEltSpace().GetQuadratureRulePerTopology()
                : *quadrature_rule_per_topology_);

    }


    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        template<ParameterNS::Type> class TimeDependencyT
    >
    const Utilities::PointerComparison::Map<RefGeomElt::shared_ptr, typename LocalParameterOperatorT::unique_ptr>&
    GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeDependencyT>
    ::GetLocalOperatorPerRefGeomElt() const noexcept
    {
        return local_operator_per_ref_geom_elt_;
    }


    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        template<ParameterNS::Type> class TimeDependencyT
    >
    inline const ParameterAtQuadraturePoint<TypeT, ParameterNS::TimeDependencyNS::None>&
    GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeDependencyT>
    ::GetParameter() const noexcept
    {
        return parameter_;
    }


    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        template<ParameterNS::Type> class TimeDependencyT
    >
    inline AllocateGradientFEltPhi
    GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeDependencyT>
    ::DoAllocateGradientFEltPhi() const noexcept
    {
        return do_allocate_gradient_felt_phi_;
    }


    template
    <
    class DerivedT,
    class LocalParameterOperatorT,
    ParameterNS::Type TypeT,
    template<ParameterNS::Type> class TimeDependencyT
    >
    inline const ExtendedUnknown::const_shared_ptr&
    GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeDependencyT>::GetExtendedUnknownPtr() const
    {
        assert(!(!extended_unknown_));
        return extended_unknown_;
    }


    template
    <
    class DerivedT,
    class LocalParameterOperatorT,
    ParameterNS::Type TypeT,
    template<ParameterNS::Type> class TimeDependencyT
    >
    inline const ExtendedUnknown&
    GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeDependencyT>::GetExtendedUnknown() const
    {
        assert(!(!extended_unknown_));
        return *extended_unknown_;
    }


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_PARAMETER_OPERATOR_x_GLOBAL_PARAMETER_OPERATOR_x_GLOBAL_PARAMETER_OPERATOR_HXX_
