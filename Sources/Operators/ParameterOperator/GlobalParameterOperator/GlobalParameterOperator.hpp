///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 29 Apr 2014 11:27:59 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_PARAMETER_OPERATOR_x_GLOBAL_PARAMETER_OPERATOR_x_GLOBAL_PARAMETER_OPERATOR_HPP_
# define MOREFEM_x_OPERATORS_x_PARAMETER_OPERATOR_x_GLOBAL_PARAMETER_OPERATOR_x_GLOBAL_PARAMETER_OPERATOR_HPP_

# include <memory>
# include <vector>
# include <string>


# include "Utilities/Containers/Tuple.hpp"

# include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"
# include "ThirdParty/Wrappers/Petsc/Vector/AccessGhostContent.hpp"

# include "Utilities/MatrixOrVector.hpp"

# include "Geometry/Mesh/Mesh.hpp"
# include "Geometry/Domain/Domain.hpp"

# include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
# include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
# include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
# include "FiniteElement/QuadratureRules/QuadratureRulePerTopology.hpp"

# include "Parameters/ParameterAtQuadraturePoint.hpp"

# include "Operators/LocalVariationalOperator/ElementaryData.hpp"


namespace MoReFEM
{


    /*!
     * \brief Parent class of all GlobalParameterOperator.
     *
     * This kind of operator is used to update Parameters.
     *
     * \attention Contrary to variational operators, no local2global is automatically defined here, as only a subset
     * of such parameter might need it. If you need one that might not already be defined, call
     * \a FEltSpace::ComputeLocal2Global. Make sure to do so before the end of the initialization phase (which
     * ends at the end of Model::Initialize() call).
     *
     * \tparam LocalParameterOperatorT Class that defines the expected elementary behaviour at each quadrature point;
     * it must be derived from LocalParameterOperator.
     */
    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        template<ParameterNS::Type> class TimeDependencyT = ParameterNS::TimeDependencyNS::None
    >
    class GlobalParameterOperator
    {
    public:

        //! Alias over the class that handles the elementary calculation.
        using LocalParameterOperator = LocalParameterOperatorT;


        /// \name Special members.
        ///@{

    protected:

        /*!
         * \brief Constructor.
         *
         * \param[in] felt_space Finite element space upon which the operator is defined.
         * \param[in] unknown Unknown considered.
         *
         * \param[in] parameter Parameter at quadrature point on which the operator is defined.
         *
         * \param[in] args Variadic arguments that will be perfect-forwarded to the constructor of each
         * LocalParameterOperator.
         * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
         * \copydoc doxygen_hide_operator_do_allocate_gradient_felt_phi_arg
         */
        template
        <
            typename... Args
        >
        explicit GlobalParameterOperator(const FEltSpace& felt_space,
                                         const Unknown& unknown,
                                         const QuadratureRulePerTopology* const quadrature_rule_per_topology,
                                         AllocateGradientFEltPhi do_allocate_gradient_felt_phi,
                                         ParameterAtQuadraturePoint<TypeT, TimeDependencyT>& parameter,
                                         Args&&... args);

        //! Protected destructor: no direct instance of this class should occur!
        ~GlobalParameterOperator() = default;

        //! Copy constructor.
        GlobalParameterOperator(const GlobalParameterOperator&) = delete;

        //! Move constructor.
        GlobalParameterOperator(GlobalParameterOperator&&) = delete;

        //! Copy affectation.
        GlobalParameterOperator& operator=(const GlobalParameterOperator&) = delete;

        //! Move affectation.
        GlobalParameterOperator& operator=(GlobalParameterOperator&&) = delete;


        ///@}

        //! Constant access to the parameter.
        const ParameterAtQuadraturePoint<TypeT, ParameterNS::TimeDependencyNS::None>& GetParameter() const noexcept;

    protected:

        /*!
         * \brief This method is in charge of updating the parameter at each quadrature point.
         *
         * This method uses the new C++ 11 feature of variadic template; so that these methods can
         * handle generically all the operators, whatever argument they might require. The drawback is that
         * it isn't clear which arguments are expected for a specific global operator; that's the reason the
         * following method is NOT called Assemble() and is protected rather than public. When a developer
         * introduces a new operator, he must therefore define a public \a Assemble() that calls the present one.
         *
         * For instance for UpdateFiberDeformation operator, which requires an additional \a displacement_increment argument:
         *
         * \code
         * inline void UpdateFiberDeformation::Update(const GlobalVector& increment_displacement) const
         * {
         *      return parent::UpdateImpl(increment_displacement);
         * }
         * \endcode
         *
         * \param[in] args Variadic template arguments, specific to the operator being implemented. These arguments
         * might be global: they are to be given to DerivedT::SetComputeEltArrayArguments() which will produce
         * the local ones.
         *
         */
        template<typename... Args>
        void UpdateImpl(Args&&... args) const;

        //! Return the Unknown and its associated numbering subset.
        const ExtendedUnknown::const_shared_ptr& GetExtendedUnknownPtr() const;

        //! Return the Unknown and its associated numbering subset.
        const ExtendedUnknown& GetExtendedUnknown() const;


    protected:

        //! FEltSpace.
        const FEltSpace& GetFEltSpace() const noexcept;

        /*!
         * \brief Access to the container that contains the match between a reference geometric element and a local
         * Parameters operator.
         */
        const Utilities::PointerComparison::Map<RefGeomElt::shared_ptr, typename LocalParameterOperatorT::unique_ptr>&
        GetLocalOperatorPerRefGeomElt() const noexcept;


    protected:

        //! Whether the gradient of finite element is required or not.
        AllocateGradientFEltPhi DoAllocateGradientFEltPhi() const noexcept;


        /*!
         * \brief Perform the elementary calculation (more exactly handle the call to the LocalParameterOperator...)
         *
         * \internal This method is called in UpdateImpl() and has no business being called elsewhere.
         *
         *
         * \param[in] local_felt_space Local finite element space considered.
         * \param[in,out] local_operator Local operator in charge of the elementary computation at each quadrature point.
         *
         */
        void PerformElementaryCalculation(const LocalFEltSpace& local_felt_space,
                                          LocalParameterOperatorT& local_operator,
                                          std::tuple<>&& ) const;


        /*!
         * \brief Overload when there are variadic arguments to handle.
         *
         * \internal This method is called in UpdateImpl() and has no business being called elsewhere.
         *
         * \param[in] local_felt_space Local finite element space considered.
         * \param[in,out] local_operator Local operator in charge of the elementary computation.
         * \param[in] args Variadic template arguments, specific to the operator being implemented. These arguments
         * might be global: they are to be given to DerivedT::SetComputeEltArrayArguments() which will produce
         * the local ones.
         *
         */
        template<typename... Args>
        void PerformElementaryCalculation(const LocalFEltSpace& local_felt_space,
                                          LocalParameterOperatorT& local_operator,
                                          std::tuple<Args...>&& args) const;


    protected:

        /*!
         * \brief Fetch the local operator associated to the finite element type.
         *
         * \return Local operator associated to a given reference geometric element. This method assumes the
         * \a ref_geom_elt is valid and gets an associated LocalOperator (an assert is there in debug mode to check that);
         * the check of such an assumption may be performed by a call to DoConsider() in case there is a genuine
         * reason to check that in a release mode context.
         *
         * \param[in] ref_geom_elt_id Identifier of the reference geometric element which local Parameters operator is
         * requested.
         *
         */
        LocalParameterOperator& GetNonCstLocalOperator(Advanced::GeometricEltEnum ref_geom_elt_id) const;

    protected:

        /*!
         * \brief Whether there is a local Parameters operator related to a given \a ref_geom_elt.
         *
         * \param[in] ref_geom_elt_id Identifier of the reference geometric element which pertinence is evaluated.
         *
         * \return True if a local Parameters operator was found.
         *
         * It might not if there is a restriction of the domain of definition (for instance elastic stiffness
         * operator does not act upon geometric objects of dimension 1).
         *
         * This method should be called prior to GetNonCstLocalOperator(): the latter will assert if the
         * \a ref_felt is invalid.
         */
        bool DoConsider(Advanced::GeometricEltEnum ref_geom_elt_id) const;

    private:

        /*!
         * \brief Create a LocalParameterOperator for each RefFEltInFEltSpace and store it into the class.
         *
         * \param[in] mesh_dimension Dimension of the mesh considered.
         * \copydoc doxygen_hide_cplusplus_variadic_args
         * \param[in,out] parameter The \a ParameterAtQuadraturePoint object to be updated by the current operator.
         */
        template<typename... Args>
        void CreateLocalOperatorList(unsigned int mesh_dimension,
                                     ParameterAtQuadraturePoint<TypeT, ParameterNS::TimeDependencyNS::None>& parameter,
                                     Args&&... args);


        //! Return the quadrature rule that match a given reference geometric element.
        const QuadratureRule& GetQuadratureRule(const RefGeomElt& ref_geom_elt) const;

        /*!
         * \brief Returns the quadrature rule to use for each topology.
         *
         * \return List of quadrature rule to use for each topology.
         */
        const QuadratureRulePerTopology& GetQuadratureRulePerTopology() const noexcept;

    private:


        //! List of nodes eligible for boundary conditions.
        NodeBearer::vector_shared_ptr node_for_boundary_condition_;

        //! Map the finite element type to the matching local operator.
        std::unordered_map<Advanced::GeometricEltEnum, typename LocalParameterOperatorT::unique_ptr>
            local_operator_per_ref_geom_elt_;

        //! Finite element space.
        const FEltSpace& felt_space_;

        /*!
         * \brief List of quadrature rules for each type of reference geometric element considered.
         *
         * If left empty (nullptr) default one from \a FEltSpace is used.
         *
         * \internal This list is given in the constructor; it is up to the caller of the operator to make sure
         * all cases are covered. If not an exception is thrown.
         */
        const QuadratureRulePerTopology* const quadrature_rule_per_topology_ = nullptr;

        /*!
         * \brief Constant reference to parameter
         *
         * Reference might be constant here as the global parameter won't attempt any change directly; it is the
         * responsability of \a LocalParameterOperator.
         */
        const ParameterAtQuadraturePoint<TypeT, TimeDependencyT>& parameter_;

        //! \copydoc doxygen_hide_operator_do_allocate_gradient_felt_phi
        const AllocateGradientFEltPhi do_allocate_gradient_felt_phi_;

        //! Unknown/numbering subset list.
        const ExtendedUnknown::const_shared_ptr extended_unknown_;

    };


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


# include "Operators/ParameterOperator/GlobalParameterOperator/GlobalParameterOperator.hxx"


#endif // MOREFEM_x_OPERATORS_x_PARAMETER_OPERATOR_x_GLOBAL_PARAMETER_OPERATOR_x_GLOBAL_PARAMETER_OPERATOR_HPP_
