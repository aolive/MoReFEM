target_sources(${MOREFEM_OP}

	PRIVATE
)

include(${CMAKE_CURRENT_LIST_DIR}/GlobalParameterOperator/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/LocalParameterOperator/SourceList.cmake)
