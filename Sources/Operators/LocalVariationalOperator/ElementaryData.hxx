///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 25 Aug 2014 17:20:25 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ELEMENTARY_DATA_HXX_
# define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ELEMENTARY_DATA_HXX_


namespace MoReFEM
{


    namespace Advanced
    {


        template
        <
            class MatrixTypeT
        >
        ElementaryData<OperatorNS::Nature::bilinear, MatrixTypeT, std::false_type>
        ::ElementaryData(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                         const QuadratureRule& quadrature_rule,
                         const ExtendedUnknown::vector_const_shared_ptr& unknown_storage,
                         const ExtendedUnknown::vector_const_shared_ptr& test_unknown_storage,
                         unsigned int felt_space_dimension,
                         unsigned int mesh_dimension,
                         AllocateGradientFEltPhi do_allocate_gradient_felt_phi)
        : Internal::LocalVariationalOperatorNS::ElementaryDataImpl(ref_felt_space,
                                                                   quadrature_rule,
                                                                   unknown_storage,
                                                                   test_unknown_storage,
                                                                   felt_space_dimension,
                                                                   mesh_dimension,
                                                                   do_allocate_gradient_felt_phi),
        matrix_storage_parent()
        {
            Allocate();
        }


        template
        <
            class VectorTypeT
        >
        ElementaryData<OperatorNS::Nature::linear, std::false_type, VectorTypeT>
        ::ElementaryData(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                         const QuadratureRule& quadrature_rule,
                         const ExtendedUnknown::vector_const_shared_ptr& unknown_storage,
                         const ExtendedUnknown::vector_const_shared_ptr& test_unknown_storage,
                         unsigned int felt_space_dimension,
                         unsigned int mesh_dimension,
                         AllocateGradientFEltPhi do_allocate_gradient_felt_phi)
        : Internal::LocalVariationalOperatorNS::ElementaryDataImpl(ref_felt_space,
                                                                   quadrature_rule,
                                                                   unknown_storage,
                                                                   test_unknown_storage,
                                                                   felt_space_dimension,
                                                                   mesh_dimension,
                                                                   do_allocate_gradient_felt_phi),
        vector_storage_parent()
        {
            Allocate();
        }


        template
        <
            class MatrixTypeT,
            class VectorTypeT
        >
        ElementaryData<OperatorNS::Nature::nonlinear, MatrixTypeT, VectorTypeT>
        ::ElementaryData(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                         const QuadratureRule& quadrature_rule,
                         const ExtendedUnknown::vector_const_shared_ptr& unknown_storage,
                         const ExtendedUnknown::vector_const_shared_ptr& test_unknown_storage,
                         unsigned int felt_space_dimension,
                         unsigned int mesh_dimension,
                         AllocateGradientFEltPhi do_allocate_gradient_felt_phi)
        : Internal::LocalVariationalOperatorNS::ElementaryDataImpl(ref_felt_space,
                                                                   quadrature_rule,
                                                                   unknown_storage,
                                                                   test_unknown_storage,
                                                                   felt_space_dimension,
                                                                   mesh_dimension,
                                                                   do_allocate_gradient_felt_phi),
        vector_storage_parent(),
        matrix_storage_parent()
        {
            Allocate();
        }


        template
        <
            class MatrixTypeT
        >
        void ElementaryData<OperatorNS::Nature::bilinear, MatrixTypeT, std::false_type>::Allocate()
        {
           this->AllocateMatrix(this->NdofRow(), this->NdofCol());
        }


        template
        <
            class VectorTypeT
        >
        void ElementaryData<OperatorNS::Nature::linear, std::false_type, VectorTypeT>::Allocate()
        {
            this->AllocateVector(this->NdofRow());
        }



        template
        <
            class MatrixTypeT,
            class VectorTypeT
        >
        void ElementaryData<OperatorNS::Nature::nonlinear, MatrixTypeT, VectorTypeT>::Allocate()
        {
            this->AllocateMatrix(this->NdofRow(), this->NdofCol());
            this->AllocateVector(this->NdofRow());
        }


        template
        <
            class MatrixTypeT
        >
        void ElementaryData<OperatorNS::Nature::bilinear, MatrixTypeT, std::false_type>::Zero()
        {
            this->GetNonCstMatrixResult().Zero();
        }


        template
        <
            class VectorTypeT
        >
        void ElementaryData<OperatorNS::Nature::linear, std::false_type, VectorTypeT>::Zero()
        {
            this->GetNonCstVectorResult().Zero();
        }


        template
        <
            class MatrixTypeT,
            class VectorTypeT
        >
        void ElementaryData<OperatorNS::Nature::nonlinear, MatrixTypeT, VectorTypeT>::Zero()
        {
            this->GetNonCstMatrixResult().Zero();
            this->GetNonCstVectorResult().Zero();
        }


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ELEMENTARY_DATA_HXX_
