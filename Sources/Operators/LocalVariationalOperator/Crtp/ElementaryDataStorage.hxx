///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 27 Jun 2014 10:23:26 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_ELEMENTARY_DATA_STORAGE_HXX_
# define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_ELEMENTARY_DATA_STORAGE_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace LocalVariationalOperatorNS
        {


            template<class DerivedT, class VectorTypeT>
            const VectorTypeT& ElementaryDataStorage<DerivedT, IsMatrixOrVector::vector, VectorTypeT>
            ::GetVectorResult() const
            {
                return vector_;
            }


            template<class DerivedT, class VectorTypeT>
            VectorTypeT& ElementaryDataStorage<DerivedT, IsMatrixOrVector::vector, VectorTypeT>
            ::GetNonCstVectorResult()
            {
                return vector_;
            }


            template<class DerivedT, class MatrixTypeT>
            const MatrixTypeT& ElementaryDataStorage<DerivedT, IsMatrixOrVector::matrix, MatrixTypeT>
            ::GetMatrixResult() const
            {
                return matrix_;
            }


            template<class DerivedT, class MatrixTypeT>
            MatrixTypeT& ElementaryDataStorage<DerivedT, IsMatrixOrVector::matrix, MatrixTypeT>
            ::GetNonCstMatrixResult()
            {
                return matrix_;
            }


            template<class DerivedT, class VectorTypeT>
            void ElementaryDataStorage<DerivedT, IsMatrixOrVector::vector, VectorTypeT>
            ::AllocateVector(unsigned int Ndof)
            {
                assert(Ndof > 0);
                vector_.Resize(static_cast<int>(Ndof));
                vector_.Zero();
            }


            template<class DerivedT, class MatrixTypeT>
            void ElementaryDataStorage<DerivedT, IsMatrixOrVector::matrix, MatrixTypeT>
            ::AllocateMatrix(unsigned int Ndof_row, unsigned int Ndof_col)
            {
                assert(Ndof_row > 0);
                assert(Ndof_col > 0);
                matrix_.Resize(static_cast<int>(Ndof_row), static_cast<int>(Ndof_col));
                matrix_.Zero();
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_ELEMENTARY_DATA_STORAGE_HXX_
