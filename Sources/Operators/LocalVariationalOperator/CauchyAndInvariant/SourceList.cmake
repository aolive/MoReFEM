target_sources(${MOREFEM_OP}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/InvariantComputation.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/InvariantComputation.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/InvariantComputation.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/InvariantHolder.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/InvariantHolder.hxx"
)

