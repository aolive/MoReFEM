///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 21 Oct 2016 16:22:30 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_INTERNAL_x_COMPUTE_PATTERN_HELPER_HXX_
# define MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_INTERNAL_x_COMPUTE_PATTERN_HELPER_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace ConformInterpolatorNS
        {




        } // namespace ConformInterpolatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_INTERNAL_x_COMPUTE_PATTERN_HELPER_HXX_
