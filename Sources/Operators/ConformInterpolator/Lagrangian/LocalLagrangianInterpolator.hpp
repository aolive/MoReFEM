///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 11 Sep 2015 15:50:44 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_LAGRANGIAN_x_LOCAL_LAGRANGIAN_INTERPOLATOR_HPP_
# define MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_LAGRANGIAN_x_LOCAL_LAGRANGIAN_INTERPOLATOR_HPP_

# include <memory>
# include <vector>

# include "Utilities/MatrixOrVector.hpp"

# include "Geometry/RefGeometricElt/RefGeomElt.hpp"

# include "FiniteElement/Unknown/ExtendedUnknown.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class FEltSpace;
    class RefGeomElt;


    namespace Internal
    {


        namespace RefFEltNS
        {


            class RefLocalFEltSpace;


        } // namespace RefFEltNS


    } // namespace Internal


    namespace Advanced
    {


        namespace ConformInterpolatorNS
        {


            class InterpolationData;


        } // namespace ConformInterpolatorNS


    } // namespace Advanced


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================



    namespace ConformInterpolatorNS
    {


        namespace LagrangianNS
        {


            /*!
             * \brief Performs the elementary part of the computation of a \a LagrangianInterpolator.
             *
             * \a LocalLagrangianInterpolator are given as second template arguments of \a LagrangianInterpolator
             */
            class LocalLagrangianInterpolator
            {

            public:

                //! \copydoc doxygen_hide_alias_self
                using self = LocalLagrangianInterpolator;

            public:

                /// \name Special members.
                ///@{

                /*!
                 * \class doxygen_hide_local_lagrangian_interpolator_constructor
                 *
                 * \brief Constructor.
                 *
                 * \param[in] ref_geom_elt Reference geometric element being considered.
                 * \copydoc doxygen_hide_conform_interpolator_interpolation_data_arg
                 *
                 */

                //! \copydoc doxygen_hide_local_lagrangian_interpolator_constructor
                explicit LocalLagrangianInterpolator(const RefGeomElt& ref_geom_elt,
                                                     const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data);


                //! Destructor.
                virtual ~LocalLagrangianInterpolator();

                //! Copy constructor.
                LocalLagrangianInterpolator(const LocalLagrangianInterpolator&) = delete;

                //! Move constructor.
                LocalLagrangianInterpolator(LocalLagrangianInterpolator&&) = delete;

                //! Copy affectation.
                LocalLagrangianInterpolator& operator=(const LocalLagrangianInterpolator&) = delete;

                //! Move affectation.
                LocalLagrangianInterpolator& operator=(LocalLagrangianInterpolator&&) = delete;

                ///@}

                //! Local projection matrix.
                const LocalMatrix& GetProjectionMatrix() const noexcept;

                //! Accessor to the RefGeomElt handled by current local operator.
                const RefGeomElt& GetRefGeomElt() const noexcept;


            protected:

                //! Accessor to the local projection matrix.
                LocalMatrix& GetNonCstProjectionMatrix() noexcept;

                /*!
                 * \brief Get the \a RefLocalFEltSpace matching the given arguments.
                 *
                 * \param[in] felt_space \a FEltSpace considered.
                 * \param[in] ref_geom_elt \a RefGeomElt considered.
                 *
                 * \return \a RefLocalFEltSpace matching both arguments.
                 */
                const Internal::RefFEltNS::RefLocalFEltSpace& GetRefLocalFEltSpace(const FEltSpace& felt_space,
                                                                        const RefGeomElt& ref_geom_elt) const;

                //! Accessor to the object holding relevant interpolation data.
                const Advanced::ConformInterpolatorNS::InterpolationData& GetInterpolationData() const noexcept;


                /*!
                 * \brief Once the content for the matrix (Nnode_in_row, Nnode_in_col) has been computed, copy it
                 * as a block for all relevant components.
                 *
                 * \param[in] node_block Matrix which content should be reported as a block in larger matrix.
                 */
                void FillMatrixFromNodeBlock(LocalMatrix&& node_block);


            private:

                //! RefGeomElt handled by current local operator.
                const RefGeomElt& ref_geom_elt_;

                //! Convenient object which stores data relevant for interpolation.
                const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data_;

                //! Local matrix.
                LocalMatrix local_projection_matrix_;

            };


        } // namespace LagrangianNS


    } // namespace ConformInterpolatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


# include "Operators/ConformInterpolator/Lagrangian/LocalLagrangianInterpolator.hxx"


#endif // MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_LAGRANGIAN_x_LOCAL_LAGRANGIAN_INTERPOLATOR_HPP_
