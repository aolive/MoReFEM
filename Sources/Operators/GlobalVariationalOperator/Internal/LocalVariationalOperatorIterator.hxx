///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 13 Mar 2017 22:12:49 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_LOCAL_VARIATIONAL_OPERATOR_ITERATOR_HXX_
# define MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_LOCAL_VARIATIONAL_OPERATOR_ITERATOR_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace GlobalVariationalOperatorNS
        {


            template
            <
                class TupleT,
                std::size_t I,
                std::size_t TupleSizeT,
                Advanced::OperatorNS::Nature NatureT
            >
            constexpr bool LocalVariationalOperatorIterator<TupleT, I, TupleSizeT, NatureT>
            ::IsNullptr()
            {
                if constexpr (std::is_same
                    <
                        typename current_item_type::local_operator_pointer_type,
                        std::nullptr_t
                    >())
                {
                    return true;
                }
                else
                {
                    static_assert(NatureT == current_item_type::local_operator_type::GetOperatorNature(),
                                  "Global and local variational operator must act on same operator nature!");
                    return false;
                }
            }


            template
            <
                class TupleT,
                std::size_t I,
                std::size_t TupleSizeT,
                Advanced::OperatorNS::Nature NatureT
            >
            template
            <
                class GlobalOperatorT,
                typename... Args
            >
            void LocalVariationalOperatorIterator<TupleT, I, TupleSizeT, NatureT>
            ::FillLocalVariationalOperatorList(const unsigned int mesh_dimension,
                                               GlobalOperatorT& global_operator,
                                               TupleT& list,
                                               Args&&... args)
            {
                // If the current \a RefGeomElt consider should be ignored, skip everything except the recursive call.
                if constexpr (!IsNullptr())
                {
                    const auto& felt_space = global_operator.GetFEltSpace();
                    const auto& felt_storage = felt_space.GetLocalFEltSpacePerRefLocalFEltSpace();

                    if (felt_storage.empty())
                        std::cout << "[WARNING] Finite element space related to operator " << GlobalOperatorT::ClassName()
                        << " is empty! It might be due for instance to a non existing label for the dimension considered "
                        "here (namely "
                        << felt_space.GetDimension() << ")." << std::endl;

                    decltype(auto) unknown_storage = global_operator.GetExtendedUnknownList();

                    decltype(auto) test_unknown_storage = global_operator.GetExtendedTestUnknownList();

                    using local_operator_type = typename std::tuple_element_t<I, TupleT>::local_operator_type;

                    // Might be nullptr if current RefGeometricElt is not present in the mesh hence the test just above.
                    decltype(auto) ref_local_felt_space_ptr = FEltSpaceNS::FindRefLocalFEltSpace<I>(felt_storage);

                    if (!(!ref_local_felt_space_ptr))
                    {
                        assert(!(!ref_local_felt_space_ptr));
                        decltype(auto) ref_local_felt_space = *ref_local_felt_space_ptr;

                        const auto& quadrature_rule =
                            global_operator.GetQuadratureRule(ref_local_felt_space.GetRefGeomElt());

                        typename local_operator_type::elementary_data_type
                        elementary_data(ref_local_felt_space,
                                        quadrature_rule,
                                        unknown_storage,
                                        test_unknown_storage,
                                        felt_space.GetDimension(),
                                        mesh_dimension,
                                        global_operator.DoAllocateGradientFEltPhi());

                        auto&& local_operator_ptr =
                        std::make_unique<local_operator_type>(unknown_storage,
                                                              test_unknown_storage,
                                                              std::move(elementary_data),
                                                              std::forward<decltype(args)>(args)...);

                        std::get<I>(list).SetLocalOperator(std::move(local_operator_ptr));
                    }
                }

                // Recursivity.
                LocalVariationalOperatorIterator<TupleT, I + 1, TupleSizeT, NatureT>
                ::FillLocalVariationalOperatorList(mesh_dimension,
                                                   global_operator,
                                                   list,
                                                   std::forward<decltype(args)>(args)...);
            }


            template
            <
                class TupleT,
                std::size_t TupleSizeT,
                Advanced::OperatorNS::Nature NatureT
            >
            template
            <
                class GlobalOperatorT,
                typename... Args
            >
            void LocalVariationalOperatorIterator<TupleT, TupleSizeT, TupleSizeT, NatureT>
            ::FillLocalVariationalOperatorList(unsigned int ,
                                               GlobalOperatorT& ,
                                               TupleT& ,
                                               Args&&...)
            {
                // Do nothing!
            }


            template
            <
                class LocalOperatorTupleT,
                std::size_t I,
                std::size_t TupleSizeT,
                Advanced::OperatorNS::Nature NatureT
            >
            template
            <
                elementary_mode ModeT,
                class GlobalOperatorT,
                class LinearAlgebraTupleT,
                class AdditionalArgsTupleT
            >
            void LocalVariationalOperatorIterator<LocalOperatorTupleT, I, TupleSizeT, NatureT>
            ::Assemble(const GlobalOperatorT& global_operator,
                       const LocalOperatorTupleT& local_operator_tuple,
                       const LinearAlgebraTupleT& linear_algebra_tuple,
                       const Domain& domain,
                       assemble_into_matrix do_assemble_into_matrix,
                       assemble_into_vector do_assemble_into_vector,
                       const AdditionalArgsTupleT& additional_args_as_tuple)
            {
                // If the current \a RefGeomElt consider should be ignored, skip everything except the recursive call.
                if constexpr (!IsNullptr())
                {
                    decltype(auto) tuple_item =
                        std::get<I>(local_operator_tuple); // Will select the local operator helper related to the
                                                           // I -th RefGeomElt (see index meaning in GeometricEltEnum).

                    if (tuple_item.IsRelevant())
                    {
                        auto& local_operator = tuple_item.GetNonCstLocalOperator();

                        local_operator.SetAssembleTarget(do_assemble_into_matrix, do_assemble_into_vector);

                        decltype(auto) felt_storage = global_operator.GetFEltSpace().GetLocalFEltSpacePerRefLocalFEltSpace(domain);

                        for (const auto& pair : felt_storage)
                        {
                            auto& ref_local_felt_space_ptr = pair.first;
                            assert(!(!ref_local_felt_space_ptr));
                            const auto& ref_local_felt_space = *ref_local_felt_space_ptr;

                            const auto& ref_geom_elt = ref_local_felt_space.GetRefGeomElt();
                            const auto ref_geom_elt_id = EnumUnderlyingType(ref_geom_elt.GetIdentifier());

                            if (ref_geom_elt_id != I)
                                continue;

                            const auto& local_felt_space_list = pair.second;

                            for (const auto& local_felt_space_pair : local_felt_space_list)
                            {
                                const auto& local_felt_space_ptr = local_felt_space_pair.second;
                                assert(!(!local_felt_space_ptr));

                                auto& local_felt_space = *local_felt_space_ptr;
                                local_operator.SetLocalFEltSpace(local_felt_space);

                                global_operator.template PerformElementaryCalculation<ModeT>(local_felt_space,
                                                                                             local_operator,
                                                                                             additional_args_as_tuple);


                                switch(ModeT)
                                {
                                    case elementary_mode::full:
                                        Internal::GlobalVariationalOperatorNS::InjectInGlobalLinearAlgebra(global_operator,
                                                                                                           linear_algebra_tuple,
                                                                                                           local_felt_space,
                                                                                                           local_operator);
                                        break;
                                    case elementary_mode::just_init:
                                        break;
                                }
                            }
                        }
                    }
                }

                LocalVariationalOperatorIterator<LocalOperatorTupleT, I + 1, TupleSizeT, NatureT>
                ::template Assemble<ModeT>(global_operator,
                                           local_operator_tuple,
                                           linear_algebra_tuple,
                                           domain,
                                           do_assemble_into_matrix,
                                           do_assemble_into_vector,
                                           additional_args_as_tuple);
            }


            template
            <
                class TupleT,
                std::size_t TupleSizeT,
                Advanced::OperatorNS::Nature NatureT
            >
            template
            <
                elementary_mode ModeT,
                class GlobalOperatorT,
                class LinearAlgebraTupleT,
                class AdditionalArgsTupleT
            >
            void LocalVariationalOperatorIterator<TupleT, TupleSizeT, TupleSizeT, NatureT>
            ::Assemble(const GlobalOperatorT& ,
                       const TupleT& ,
                       const LinearAlgebraTupleT& linear_algebra_tuple,
                       const Domain&,
                       assemble_into_matrix,
                       assemble_into_vector,
                       const AdditionalArgsTupleT& )
            {
                Internal::GlobalVariationalOperatorNS::Assembly(linear_algebra_tuple);
            }


        } // namespace GlobalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_LOCAL_VARIATIONAL_OPERATOR_ITERATOR_HXX_
