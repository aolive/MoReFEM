///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 13 Sep 2016 15:09:24 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_DETERMINE_EXTENDED_UNKNOWN_LIST_HXX_
# define MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_DETERMINE_EXTENDED_UNKNOWN_LIST_HXX_


namespace MoReFEM
{


    namespace Advanced
    {


        namespace GlobalVariationalOperatorNS
        {

            template <std::size_t NunknownsT>
            ExtendedUnknown::vector_const_shared_ptr
            DetermineExtendedUnknownList(const FEltSpace& felt_space,
                                         const std::array<Unknown::const_shared_ptr, NunknownsT>& unknown_list)
            {
                ExtendedUnknown::vector_const_shared_ptr ret;

                for (unsigned int i = 0 ; i < NunknownsT ; ++i)
                {
                    const auto& unknown_ptr = unknown_list[i];
                    ret.push_back(felt_space.GetExtendedUnknownPtr(*unknown_ptr));
                }

                return ret;
            }


        } // namespace GlobalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_DETERMINE_EXTENDED_UNKNOWN_LIST_HXX_
