target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/RefGeomElt.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/RefGeomElt.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/RefGeomElt.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Instances/SourceList.cmake)
