///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 5 Nov 2014 12:26:00 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_SHAPE_FUNCTION_x_ACCESS_SHAPE_FUNCTION_HPP_
# define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_SHAPE_FUNCTION_x_ACCESS_SHAPE_FUNCTION_HPP_

# include <cassert>

# include "Geometry/Coords/LocalCoords.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace ShapeFunctionNS
        {


            namespace Crtp
            {


                /*!
                 * \brief Curiously recurrent template pattern (CRTP) that provides two accessors to the value
                 * of a shape function and its derivates.
                 *
                 * \tparam DerivedT Any element of RefGeomEltNS::ShapeFunctionNS namespace.
                 */
                template<class DerivedT>
                struct AccessShapeFunction
                {

                    /*!
                     * \brief Returns the value of the shape function.
                     *
                     * \param[in] local_node_index Index in the local reference element.
                     * \param[in] local_coords Coords in the local reference element.
                     *
                     * \return Value of the shape function.
                     */
                    static double ShapeFunction(unsigned int local_node_index,
                                                const LocalCoords& local_coords);


                    /*!
                     * \brief Returns the value of the derivate of the shape function.
                     *
                     * \param[in] local_node_index Index in the local reference element.
                     * \param[in] component Derivate against this component is considered.
                     * \param[in] local_coords Coords in the local reference element.
                     *
                     * \return Value of the derivate of the shape function.
                     */
                    static double FirstDerivateShapeFunction(unsigned int local_node_index,
                                                             unsigned int component,
                                                             const LocalCoords& local_coords);

                };


            } // namespace Crtp


        } // namespace ShapeFunctionNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/RefGeometricElt/Internal/ShapeFunction/AccessShapeFunction.hxx"


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_SHAPE_FUNCTION_x_ACCESS_SHAPE_FUNCTION_HPP_
