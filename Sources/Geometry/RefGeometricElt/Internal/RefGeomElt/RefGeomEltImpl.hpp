///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 19 Mar 2014 11:43:44 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_REF_GEOM_ELT_x_REF_GEOM_ELT_IMPL_HPP_
# define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_REF_GEOM_ELT_x_REF_GEOM_ELT_IMPL_HPP_

# include "Geometry/Coords/LocalCoords.hpp"
# include "Geometry/GeometricElt/Advanced/FreeFunctions.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace RefGeomEltNS
        {


            /*!
             * \brief Traits class which unifies traits from Interface and from shape functions.
             *
             * \tparam DerivedT RefGeomEltImpl is also to be used as a CRTP.
             * \tparam ShapeFunctionTraitsT Trait class which define the shape functions and their derivatives.
             * These structures are in namespace RefGeomEltNS::ShapeFunctionNS and bear the same names as the final
             * class in MoReFEM namespace.
             * \tparam TopologyT Topology considered (one of the class defined within TopologyNS namespace).
             */
            template
            <
                class DerivedT,
                class ShapeFunctionTraitsT,
                class TopologyT
            >
            class RefGeomEltImpl : public ShapeFunctionTraitsT
            {
            public:


                //! Alias over Topology.
                using topology = TopologyT;

                //! Number of shape functions considered.
                static constexpr unsigned int NshapeFunction();

                //! Number of coordinates considered (derivates will be performed against each of them).
                static constexpr unsigned int Ncoordinates();

                //! Return the value of the (icoor, jcoor)-th second derivative of the i-th shape function on given point.
                static double SecondDerivateShapeFunction(unsigned int i, unsigned int icoor, unsigned int jcoor,
                                                          const LocalCoords& local_coords);

                //! Return the barycenter.
                static const LocalCoords& GetBarycenter();


            private:


                /// \name Special members.
                ///@{

                //! Constructor (protected due to the traits nature of the class).
                RefGeomEltImpl() = default;

                //! Destructor (protected due to the traits nature of the class).
                ~RefGeomEltImpl() = default;

                //! Disable recopy.
                RefGeomEltImpl(const RefGeomEltImpl&) = delete;

                //! Disable move.
                RefGeomEltImpl(RefGeomEltImpl&&) = delete;

                //! Disable affectation.
                RefGeomEltImpl& operator=(const RefGeomEltImpl&) = delete;

                //! Disable affectation.
                RefGeomEltImpl& operator=(RefGeomEltImpl&&) = delete;

                ///@}


            };


            /*!
             * \brief Computes the barycenter of a reference element of \a TopologyT.
             */
            template<class TopologyT>
            LocalCoords ComputeBarycenter();


        } // namespace RefGeomEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/RefGeometricElt/Internal/RefGeomElt/RefGeomEltImpl.hxx"


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_REF_GEOM_ELT_x_REF_GEOM_ELT_IMPL_HPP_
