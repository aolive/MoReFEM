target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Quadrangle4.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Quadrangle8.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Quadrangle9.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Quadrangle4.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Quadrangle8.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Quadrangle9.hpp"
)

include(${CMAKE_CURRENT_LIST_DIR}/Topology/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Format/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ShapeFunction/SourceList.cmake)
