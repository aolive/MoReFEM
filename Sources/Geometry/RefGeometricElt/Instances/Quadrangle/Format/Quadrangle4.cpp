///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 Apr 2016 15:56:09 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#include "Geometry/RefGeometricElt/Instances/Quadrangle/Format/Quadrangle4.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace MeshNS
        {
            
            
            namespace FormatNS
            {
                
                
                const std::string& Support
                <
                    ::MoReFEM::MeshNS::Format::Ensight,
                    Advanced::GeometricEltEnum::Quadrangle4
                >
                ::EnsightName()
                {
                    static std::string ret("quad4");
                    return ret;
                };
                
                              
            } // namespace FormatNS
            
            
        } // namespace MeshNS
        
        
    } // namespace Internal
  

} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
