target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
)

include(${CMAKE_CURRENT_LIST_DIR}/Tetrahedron/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Hexahedron/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Quadrangle/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Point/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Segment/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Triangle/SourceList.cmake)
