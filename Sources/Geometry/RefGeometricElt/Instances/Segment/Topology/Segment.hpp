///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 25 Sep 2014 17:20:22 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_SEGMENT_x_TOPOLOGY_x_SEGMENT_HPP_
# define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_SEGMENT_x_TOPOLOGY_x_SEGMENT_HPP_

# include <memory>
# include <vector>
# include <array>

# include "Geometry/Coords/LocalCoords.hpp"
# include "Geometry/Interfaces/EnumInterface.hpp"
# include "Geometry/RefGeometricElt/Internal/Topology/EnumTopology.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace TopologyNS
        {


            /*!
             * \brief Topology of a segment.
             */
            class Segment final
            {

            public:

                //! Data.
                enum : unsigned int
                {
                    dimension = 1u,
                    Nvertex = 2u,
                    Nedge = 1u,
                    Nface = 0u,
                    Nvolume = 0u,
                };


                //! Topology of an edge.
                using EdgeTopology = Segment;

                //! Topology of a face (irrelevant here...).
                using FaceTopology = std::false_type;

                //! Container used to store all the points of an edge. The index of the points are actually stored.
                using EdgeContent = std::array<unsigned int, EdgeTopology::Nvertex>;

                //! Container used to store all the points of a face. The index of the points are actually stored.
                using FaceContent = std::false_type;

                //! Container used to store the local coordinates of the vertices.
                using VerticeCoordListType = std::vector<LocalCoords>;

                /*!
                 * \brief Interface in which interior dofs (i.e. those without continuity with neighbor elements) are
                 * stored.
                 */
                static InterfaceNS::Nature GetInteriorInterface();

                //! Returns the enum that tags the topology.
                static Type GetType() noexcept;


            public:

                /// \name Special members.
                ///@{

                //! Constructor.
                explicit Segment() = default;

            protected:

                //! Destructor.
                ~Segment() = default;

                //! Copy constructor.
                Segment(const Segment&) = delete;

                //! Move constructor.
                Segment(Segment&&) = delete;

                //! Affectation.
                Segment& operator=(const Segment&) = delete;

                //! Affectation.
                Segment& operator=(Segment&&) = delete;

                ///@}

            public:

                //! Return the name of the topology.
                static const std::string& ClassName();

                /*!
                 * \copydoc doxygen_hide_is_on_local_vertex
                 */
                static bool IsOnVertex(unsigned int local_vertex_index, const LocalCoords& local_coords);

                /*!
                 * \copydoc doxygen_hide_is_on_local_edge
                 */
                static bool IsOnEdge(unsigned int local_edge_index, const LocalCoords& local_coords);

                // ============================
                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                // ============================

                // Just there to make the code compile; should never be called!
                [[noreturn]] static bool IsOnFace(unsigned int, const LocalCoords& );


                // Just there to make the code compile; should never be called!
                [[noreturn]] static LocalCoords TransformFacePoint(const LocalCoords& coords,
                                                                             unsigned int face_index,
                                                                             unsigned int orientation);

                // ============================
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN
                // ============================

                //! Return the local coordinates of the vertices.
                static const VerticeCoordListType& GetVertexLocalCoordsList();


            private:

                //! Whether a \a coords is inside the reference segment or not.
                static bool IsInside(const LocalCoords& coords);

                //! Return the list of edges.
                static const std::array<EdgeContent, Nedge>& GetEdgeList();

                //! Return the list of faces.
                static const std::vector<FaceContent>& GetFaceList();


                /*!
                 * \brief Friendship to LocalData.
                 *
                 * As you can see above, list of local edges, faces and list of vertex coordinates are private
                 * members; to access them you must use the LocalData class.
                 *
                 * For instance:
                 *
                 * \code
                 * const auto& topology_face = RefGeomEltNS::TopologyNS::LocalData<TopologyT>::GetFace(local_face_index);
                 * \endcode
                 *
                 */
                template<class TopologyT>
                friend struct LocalData;



            };


        } // namespace TopologyNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_SEGMENT_x_TOPOLOGY_x_SEGMENT_HPP_
