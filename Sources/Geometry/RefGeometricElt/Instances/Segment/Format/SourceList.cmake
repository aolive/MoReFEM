target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Segment2.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Segment3.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Segment2.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Segment2.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Segment3.hpp"
)

