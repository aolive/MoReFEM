///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 25 Sep 2014 17:20:22 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_HEXAHEDRON_x_TOPOLOGY_x_HEXAHEDRON_HPP_
# define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_HEXAHEDRON_x_TOPOLOGY_x_HEXAHEDRON_HPP_

# include <memory>
# include <vector>
# include <array>

# include "Geometry/Coords/LocalCoords.hpp"
# include "Geometry/RefGeometricElt/Instances/Segment/Topology/Segment.hpp"
# include "Geometry/RefGeometricElt/Instances/Quadrangle/Topology/Quadrangle.hpp"
# include "Geometry/Interfaces/EnumInterface.hpp"
# include "Geometry/RefGeometricElt/Internal/Topology/EnumTopology.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace TopologyNS
        {


            /*!
             * \brief Topology of a hexahedron.
             */
            class Hexahedron final
            {

            public:

                //! Data.
                enum : unsigned int
                {
                    dimension = 3u,
                    Nvertex = 8u,
                    Nedge = 12u,
                    Nface = 6u,
                    Nvolume = 1u,
                };


                //! Topology of an edge.
                using EdgeTopology = Segment;

                //! Topology of a face.
                using FaceTopology = Quadrangle;

                //! Container used to store all the points of an edge. The index of the points are actually stored.
                using EdgeContent = std::array<unsigned int, EdgeTopology::Nvertex>;

                //! Container used to store all the points of a face. The index of the points are actually stored.
                using FaceContent = std::array<unsigned int, FaceTopology::Nvertex>;

                //! Container used to store the local coordinates of the vertices.
                using VerticeCoordListType = std::vector<LocalCoords>;

                /*!
                 * \brief Interface in which interior dofs (i.e. those without continuity with neighbor elements) are
                 * stored.
                 */
                static InterfaceNS::Nature GetInteriorInterface();

                //! Returns the enum that tags the topology.
                static Type GetType() noexcept;


            public:

                /// \name Special members.
                ///@{

                //! Constructor.
                explicit Hexahedron() = default;

            protected:

                //! Destructor.
                ~Hexahedron() = default;

                //! Copy constructor.
                Hexahedron(const Hexahedron&) = delete;

                //! Move constructor.
                Hexahedron(Hexahedron&&) = delete;

                //! Affectation.
                Hexahedron& operator=(const Hexahedron&) = delete;

                //! Affectation.
                Hexahedron& operator=(Hexahedron&&) = delete;

                ///@}

            public:

                //! Return the name of the topology.
                static const std::string& ClassName();


                /*!
                 * \copydoc doxygen_hide_is_on_local_vertex
                 */
                static bool IsOnVertex(unsigned int local_vertex_index, const LocalCoords& local_coords);

                /*!
                 * \copydoc doxygen_hide_is_on_local_edge
                 */
                static bool IsOnEdge(unsigned int local_edge_index, const LocalCoords& local_coords);

                /*!
                 * \copydoc doxygen_hide_is_on_local_face
                 */
                static bool IsOnFace(unsigned int local_face_index, const LocalCoords& local_coords);

                //! \copydoc doxygen_hide_transform_face_point
                static LocalCoords TransformFacePoint(const LocalCoords& coords,
                                                                unsigned int face_index,
                                                                unsigned int orientation);


                //! Return the local coordinates of the vertices.
                static const VerticeCoordListType& GetVertexLocalCoordsList();

            private:

                //! Whether a \a coords is inside the reference hexahedron or not.
                static bool IsInside(const LocalCoords& coords);

                //! Return the list of edges.
                static const std::array<EdgeContent, Nedge>& GetEdgeList();

                //! Return the list of faces.
                static const std::array<FaceContent, Nface>& GetFaceList();

                /*!
                 * \brief Friendship to LocalData.
                 *
                 * As you can see above, list of local edges, faces and list of vertex coordinates are private
                 * members; to access them you must use the LocalData class.
                 *
                 * For instance:
                 *
                 * \code
                 * const auto& topology_face = RefGeomEltNS::TopologyNS::LocalData<TopologyT>::GetFace(local_face_index);
                 * \endcode
                 *
                 */
                template<class TopologyT>
                friend struct LocalData;


            };


        } // namespace TopologyNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_HEXAHEDRON_x_TOPOLOGY_x_HEXAHEDRON_HPP_
