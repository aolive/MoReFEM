target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Triangle3.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Triangle6.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Triangle3.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Triangle6.hpp"
)

