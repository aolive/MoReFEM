///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 14 Dec 2015 11:09:00 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_INTERPOLATOR_x_VERTEX_MATCHING_HXX_
# define MOREFEM_x_GEOMETRY_x_INTERPOLATOR_x_VERTEX_MATCHING_HXX_


namespace MoReFEM
{


    namespace MeshNS
    {


        namespace InterpolationNS
        {


            template<class InputParameterDataT>
            VertexMatching::VertexMatching(const InputParameterDataT& input_parameter_data)
            {
                decltype(auto) interpolation_file =
                Utilities::InputParameterListNS::Extract<InputParameter::InterpolationFile>::Path(input_parameter_data);

                Read(interpolation_file);
            }


            inline const std::vector<unsigned int>& VertexMatching::GetSourceIndexList() const noexcept
            {
                assert(!source_index_list_.empty());
                assert(source_index_list_.size() == target_index_list_.size());
                return source_index_list_;
            }


            inline const std::vector<unsigned int>& VertexMatching::GetTargetIndexList() const noexcept
            {
                assert(!target_index_list_.empty());
                assert(target_index_list_.size() == target_index_list_.size());
                return target_index_list_;
            }


        } // namespace InterpolationNS


    } // namespace MeshNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERPOLATOR_x_VERTEX_MATCHING_HXX_
