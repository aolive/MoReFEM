///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_HEXAHEDRON_x_HEXAHEDRON8_HPP_
# define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_HEXAHEDRON_x_HEXAHEDRON8_HPP_

# include "Geometry/RefGeometricElt/RefGeomElt.hpp"
# include "Geometry/GeometricElt/Advanced/TGeometricElt.hpp"
# include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"

# include "Geometry/RefGeometricElt/Instances/Hexahedron/Hexahedron8.hpp"


namespace MoReFEM
{


    /*!
     * \brief A Hexahedron8 geometric element read in a mesh.
     *
     * We are not considering here a generic Hexahedron8 object (that's the role of MoReFEM::RefGeomEltNS::Hexahedron8), but
     * rather a specific geometric element of the mesh, with for instance the coordinates of its coord,
     * the list of its interfaces and so forth.
     */
    class Hexahedron8 final : public Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Hexahedron8>
    {
    public:

        //! Minimal constructor: only the Mesh is provided.
        explicit Hexahedron8(unsigned int mesh_unique_id);

        //! Constructor from stream: 'Ncoords' are read in the stream and object is built from them.
        explicit Hexahedron8(unsigned int mesh_unique_id,
                             const Coords::vector_unique_ptr& mesh_coords_list,
                             std::istream& stream);

        //! Constructor from vector of coords.
        explicit Hexahedron8(unsigned int mesh_unique_id,
                             const Coords::vector_unique_ptr& mesh_coords_list,
                             std::vector<unsigned int>&& coords);

        //! Destructor.
        ~Hexahedron8() override;

        //! Copy constructor.
        Hexahedron8(const Hexahedron8&) = delete;

        //! Move constructor.
        Hexahedron8(Hexahedron8&&) = delete;

        //! Copy affectation.
        Hexahedron8& operator=(const Hexahedron8&) = delete;

        //! Move affectation.
        Hexahedron8& operator=(Hexahedron8&&) = delete;

        //! Reference geometric element.
        virtual const RefGeomElt& GetRefGeomElt() const override final;

    private:

        //! Reference geometric element.
        static const RefGeomEltNS::Hexahedron8& StaticRefGeomElt();


    };



} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_HEXAHEDRON_x_HEXAHEDRON8_HPP_
