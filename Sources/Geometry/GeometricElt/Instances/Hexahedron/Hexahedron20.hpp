///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_HEXAHEDRON_x_HEXAHEDRON20_HPP_
# define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_HEXAHEDRON_x_HEXAHEDRON20_HPP_

# include "Geometry/RefGeometricElt/RefGeomElt.hpp"
# include "Geometry/GeometricElt/Advanced/TGeometricElt.hpp"
# include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"

# include "Geometry/RefGeometricElt/Instances/Hexahedron/Hexahedron20.hpp"


namespace MoReFEM
{


    /*!
     * \brief A Hexahedron20 geometric element read in a mesh.
     *
     * We are not considering here a generic Hexahedron20 object (that's the role of MoReFEM::RefGeomEltNS::Hexahedron20), but
     * rather a specific geometric element of the mesh, with for instance the coordinates of its coord,
     * the list of its interfaces and so forth.
     */
    class Hexahedron20 final : public Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Hexahedron20>
    {
    public:

        //! Minimal constructor: only the Mesh is provided.
        explicit Hexahedron20(unsigned int mesh_unique_id);

        //! Constructor from stream: 'Ncoords' are read in the stream and object is built from them.
        explicit Hexahedron20(unsigned int mesh_unique_id,
                              const Coords::vector_unique_ptr& mesh_coords_list,
                              std::istream& stream);

        //! Constructor from vector of coords.
        explicit Hexahedron20(unsigned int mesh_unique_id,
                              const Coords::vector_unique_ptr& mesh_coords_list,
                              std::vector<unsigned int>&& coords);

        //! Destructor.
        ~Hexahedron20() override;

        //! Copy constructor.
        Hexahedron20(const Hexahedron20&) = delete;

        //! Move constructor.
        Hexahedron20(Hexahedron20&&) = delete;

        //! Copy affectation.
        Hexahedron20& operator=(const Hexahedron20&) = delete;

        //! Move affectation.
        Hexahedron20& operator=(Hexahedron20&&) = delete;

        //! Reference geometric element.
        virtual const RefGeomElt& GetRefGeomElt() const override final;

    private:

        //! Reference geometric element.
        static const RefGeomEltNS::Hexahedron20& StaticRefGeomElt();


    };



} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_HEXAHEDRON_x_HEXAHEDRON20_HPP_
