///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_QUADRANGLE_x_QUADRANGLE8_HPP_
# define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_QUADRANGLE_x_QUADRANGLE8_HPP_

# include "Geometry/RefGeometricElt/RefGeomElt.hpp"
# include "Geometry/GeometricElt/Advanced/TGeometricElt.hpp"
# include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"

# include "Geometry/RefGeometricElt/Instances/Quadrangle/Quadrangle8.hpp"


namespace MoReFEM
{


    /*!
     * \brief A Quadrangle8 geometric element read in a mesh.
     *
     * We are not considering here a generic Quadrangle8 object (that's the role of MoReFEM::RefGeomEltNS::Quadrangle8), but
     * rather a specific geometric element of the mesh, with for instance the coordinates of its coord,
     * the list of its interfaces and so forth.
     */
    class Quadrangle8 final : public Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Quadrangle8>
    {
    public:


        //! Minimal constructor: only the Mesh is provided.
        explicit Quadrangle8(unsigned int mesh_unique_id);

        //! Constructor from stream: 'Ncoords' are read in the stream and object is built from them.
        explicit Quadrangle8(unsigned int mesh_unique_id,
                             const Coords::vector_unique_ptr& mesh_coords_list,
                             std::istream& stream);

        //! Constructor from vector of coords.
        explicit Quadrangle8(unsigned int mesh_unique_id,
                             const Coords::vector_unique_ptr& mesh_coords_list,
                             std::vector<unsigned int>&& coords);

        //! Destructor.
        ~Quadrangle8() override;

        //! Copy constructor.
        Quadrangle8(const Quadrangle8&) = delete;

        //! Move constructor.
        Quadrangle8(Quadrangle8&&) = delete;

        //! Copy affectation.
        Quadrangle8& operator=(const Quadrangle8&) = delete;

        //! Move affectation.
        Quadrangle8& operator=(Quadrangle8&&) = delete;

        //! Reference geometric element.
        virtual const RefGeomElt& GetRefGeomElt() const override final;

    private:

        //! Reference geometric element.
        static const RefGeomEltNS::Quadrangle8& StaticRefGeomElt();


    };



} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_QUADRANGLE_x_QUADRANGLE8_HPP_
