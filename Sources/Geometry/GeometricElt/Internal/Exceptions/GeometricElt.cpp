///
////// \file
///
///
/// Created by Sebastien Gilles <srpgilles@gmail.com> on the Tue, 5 Feb 2013 11:59:16 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#include <sstream>
#include <cassert>

#include "Geometry/GeometricElt/Internal/Exceptions/GeometricElt.hpp"


namespace // anonymous
{
    
    
    std::string FormatNotSupportedMsg(const std::string& geometric_elt_identifier, const std::string& format);
    
    std::string InterfaceTypeNotBuiltMsg(const std::string& edge_or_face);
    
    std::string InvalidInterfaceBuildOrderMsg(std::string&& attempted_type, std::string&& missing_type);
    
    std::string Global2LocalNoConvergenceMsg();
    
    
} // namespace anonymous


namespace MoReFEM
{
    
    
    namespace ExceptionNS
    {
        
        
        namespace GeometricElt
        {
            
            
            Exception::~Exception() = default;
            
            
            Exception::Exception(const std::string& msg, const char* invoking_file, int invoking_line)
            : MoReFEM::Exception(msg, invoking_file, invoking_line)
            { }
            
            
            FormatNotSupported::~FormatNotSupported() = default;
            
            
            FormatNotSupported::FormatNotSupported(const std::string& geometric_elt_identifier,
                                                   const std::string& format,
                                                   const char* invoking_file, int invoking_line)
            : Exception(FormatNotSupportedMsg(geometric_elt_identifier, format), invoking_file, invoking_line)
            { }
            
            
            InterfaceTypeNotBuilt::~InterfaceTypeNotBuilt() = default;
            
            
            InterfaceTypeNotBuilt::InterfaceTypeNotBuilt(const std::string& edge_or_face,
                                                         const char* invoking_file, int invoking_line)
            : Exception(InterfaceTypeNotBuiltMsg(edge_or_face), invoking_file, invoking_line)
            { }
            
            
            
            InvalidInterfaceBuildOrder::~InvalidInterfaceBuildOrder() = default;
            
            
            InvalidInterfaceBuildOrder::InvalidInterfaceBuildOrder(std::string&& attempted_type,
                                                                   std::string&& missing_type,
                                                                   const char* invoking_file, int invoking_line)
            : Exception(InvalidInterfaceBuildOrderMsg(std::move(attempted_type),
                                                      std::move(missing_type)),
                        invoking_file, invoking_line)
            { }
            
            
            Global2LocalNoConvergence::~Global2LocalNoConvergence() = default;
            
            
            Global2LocalNoConvergence::Global2LocalNoConvergence(const char* invoking_file, int invoking_line)
            : Exception(Global2LocalNoConvergenceMsg(), invoking_file, invoking_line)
            { }
            
            
            
        } // namespace GeometricElt
        
        
    } // namespace ExceptionNS
    
    
} // namespace MoReFEM



namespace // anonymous
{
    
    
    // Definitions of functions defined at the beginning of the file
    
    std::string FormatNotSupportedMsg(const std::string& geometric_elt_identifier, const std::string& format)
    {
        std::ostringstream oconv;
        oconv << "GeometricElt " << geometric_elt_identifier << " is not supported by "
        << format << " format";
        
        return oconv.str();
    }
    
    
    std::string InterfaceTypeNotBuiltMsg(const std::string& edge_or_face)
    {
        
        std::ostringstream oconv;
        
        std::string with_upper_case(edge_or_face);
        with_upper_case[0] = static_cast<char>(toupper(with_upper_case[0]));
        
        oconv << with_upper_case << "s not properly built; a call to Mesh::Build" << with_upper_case << "List() "
        "is probably missing.";
        
        assert(false);
        return ""; // to avoid warning
    }
    
    
    std::string InvalidInterfaceBuildOrderMsg(std::string&& attempted_type, std::string&& missing_type)
    {
        std::ostringstream oconv;
        oconv << "You attempted to build the list of " << std::move(attempted_type) << " while the "
        "list of " << std::move(missing_type) << " was not built.";
        
        return oconv.str();
    }
    
    
    std::string Global2LocalNoConvergenceMsg()
    {
        return "No convergence was reached in the Newton algorithm used to find out local coordinates of a mesh point.";
    }
    
    
} // namespace anonymous


/// @} // addtogroup GeometryGroup
