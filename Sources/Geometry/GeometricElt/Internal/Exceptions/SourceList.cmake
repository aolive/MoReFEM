target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/GeometricElt.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/GeometricEltFactory.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/GeometricElt.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/GeometricEltFactory.hpp"
)

