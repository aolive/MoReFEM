///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 1 Jun 2016 10:13:09 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#include "Geometry/GeometricElt/Advanced/ComputeJacobian.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Coords/LocalCoords.hpp"


namespace MoReFEM
{
    
    
    namespace Advanced
    {
        
        
        namespace GeomEltNS
        {
            
            
            ComputeJacobian::ComputeJacobian(const unsigned int mesh_dimension)
            {
                jacobian_.Resize(static_cast<int>(mesh_dimension), static_cast<int>(mesh_dimension));
                first_derivate_shape_function_.resize(static_cast<std::size_t>(mesh_dimension));
                
            }
            
            const LocalMatrix& ComputeJacobian::Compute(const GeometricElt& geometric_elt,
                                                        const LocalCoords& local_coords)
            {
                const unsigned int Ncoords = geometric_elt.Ncoords();
                const unsigned int Ncomponent = geometric_elt.GetDimension();
                
                auto& jacobian = GetNonCstJacobian();
                auto& first_derivate_shape_function = GetNonCstFirstDerivateShapeFunction();
                
                const unsigned int Nshape_function = static_cast<unsigned int>(jacobian.GetN());
                
                for (auto& item : first_derivate_shape_function)
                    item = 0.;
                
                assert(Ncomponent <= Nshape_function);
                jacobian.Zero();
                
                for (unsigned int local_coord_index = 0u; local_coord_index < Ncoords; ++local_coord_index)
                {
                    for (unsigned int component = 0u; component < Ncomponent; ++component)
                    {
                        first_derivate_shape_function[component] = geometric_elt.FirstDerivateShapeFunction(local_coord_index,
                                                                                                            component,
                                                                                                            local_coords);
                    }
                    
                    assert("All values above the dimension should stay 0!"
                           && std::all_of(first_derivate_shape_function.cbegin() + Ncomponent,
                                          first_derivate_shape_function.cend(),
                                          [](double value)
                                          {
                                              return NumericNS::IsZero(value);
                                          }));
                    
                    const auto& coords_in_geom_elt = geometric_elt.GetCoord(local_coord_index);
                    
                    // If LocalCoords is less than geometric element dimension, columns above its dimension are filled with zeros.
                    for (unsigned int component = 0u; component < Nshape_function; ++component)
                    {
                        const int int_component = static_cast<int>(component);
                        
                        for (unsigned int shape_fct_index = 0u; shape_fct_index < Nshape_function; ++shape_fct_index)
                        {
                            jacobian(int_component, static_cast<int>(shape_fct_index)) +=
                            coords_in_geom_elt[component] * first_derivate_shape_function[shape_fct_index];
                        }
                    }
                    // #446
                    if (geometric_elt.GetIdentifier() == Advanced::GeometricEltEnum::Point1)
                    {
                        jacobian(0, 0) = 1;
                    }
                }
                
                return jacobian;
            }
            
            
        } // namespace GeomEltNS
        
        
    } // namespace Advanced
  

} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
