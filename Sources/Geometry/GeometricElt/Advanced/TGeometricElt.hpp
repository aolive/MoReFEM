///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Jan 2013 14:21:56 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_T_GEOMETRIC_ELT_HPP_
# define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_T_GEOMETRIC_ELT_HPP_


# include <iomanip>
# include "Core/Enum.hpp"
# include "Geometry/GeometricElt/GeometricElt.hpp"
# include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
# include "Geometry/Mesh/Internal/Format/Format.hpp"
# include "Geometry/Mesh/Internal/Format/Exceptions/Format_fwd.hpp"
# include "Geometry/Interfaces/Internal/BuildInterfaceListHelper.hpp"
# include "Geometry/Interfaces/Internal/BuildInterfaceHelper.hpp"
# include "Geometry/Interfaces/Internal/ComputeOrientation.hpp"


namespace MoReFEM
{

    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GeometricElt;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Internal
    {


        namespace GeomEltNS
        {


            /*!
             * \brief Derived from 'GeometricElt', it defines many virtual methods that depends one way or another from template parameters
             *
             * 'TGeometricElt' stands for templatized geometric element.
             *
             * \tparam TraitsRefGeomEltT A trait class that defines GeoRefElt behaviour.
            */
            template<class TraitsRefGeomEltT>
            class TGeometricElt : public GeometricElt
            {
            public:

                //! Convenient alias.
                using self = TGeometricElt<TraitsRefGeomEltT>;

                /*!
                 * \brief Object that store the information about Medit support.
                 *
                 * This object inherits from std::true_type if Medit is supported, std::false_type otherwise.
                 */
                using MeditSupport =
                    Internal::MeshNS::FormatNS::Support<::MoReFEM::MeshNS::Format::Medit,TraitsRefGeomEltT::Identifier()>;

                /*!
                 * \brief Object that store the information about Ensight support.
                 *
                 * This object inherits from std::true_type if Ensight is supported, std::false_type otherwise.
                 */
                using EnsightSupport =
                    Internal::MeshNS::FormatNS::Support<::MoReFEM::MeshNS::Format::Ensight,TraitsRefGeomEltT::Identifier()>;


                /// \name Constructors & destructor
                ///@{
                //! Minimal constructor.
                explicit TGeometricElt(unsigned int mesh_unique_id);

                /*!
                 * \brief Constructor from stream: 'Ncoords' are read in the stream and object is built from them.
                 *
                 * \param[in,out] stream Input stream from which the geometric element is to be constructed.
                 * For instance, "42 84 126" may be used to create a 'Triangle3' object
                 * \copydoc doxygen_hide_geom_elt_mesh_id_constructor_arg
                 * \copydoc doxygen_hide_geom_elt_mesh_coords_list_constructor_arg
                 *
                 * If the reading fails (ie the stream state doesn't remain 'good' for some reason) the object
                 * is constructed with default values; so the state of the stream should be checked in output
                 */
                explicit TGeometricElt(unsigned int mesh_unique_id,
                                       const Coords::vector_unique_ptr& mesh_coords_list,
                                       std::istream& stream);


                /*!
                 * \brief Constructor from vector
                 *
                 * \param[in] index Index of the geometric element
                 * \copydoc doxygen_hide_geom_elt_mesh_coords_list_and_coord_indexes_constructor_arg
                 * \copydoc doxygen_hide_geom_elt_mesh_id_constructor_arg
                 * Vector should be the NcoordsT-long
                 */
                explicit TGeometricElt(unsigned int mesh_unique_id,
                                       const Coords::vector_unique_ptr& mesh_coords_list,
                                       unsigned int index,
                                       std::vector<unsigned int>&& coords_indexes);

                /*!
                 * \brief Constructor from vector; index left undefined
                 *
                 * \copydoc doxygen_hide_geom_elt_mesh_coords_list_and_coord_indexes_constructor_arg
                 * \copydoc doxygen_hide_geom_elt_mesh_id_constructor_arg
                 *
                 * Index should be specified through dedicated method.
                 */
                explicit TGeometricElt(unsigned int mesh_unique_id,
                                       const Coords::vector_unique_ptr& mesh_coords_list,
                                       std::vector<unsigned int>&& coords_indexes);


                //! Destructor
                virtual ~TGeometricElt() override = 0;

                //! Copy constructor.
                TGeometricElt(const TGeometricElt&) = delete;

                //! Move constructor.
                TGeometricElt(TGeometricElt&&) = delete;

                //! Copy affectation.
                TGeometricElt& operator=(const TGeometricElt&) = delete;

                //! Move affectation.
                TGeometricElt& operator=(TGeometricElt&&) = delete;


                ///@}


                //! Returns the identifier of the geometric reference element (for instance 'Tria3')
                static constexpr Advanced::GeometricEltEnum Identifier() { return TraitsRefGeomEltT::Identifier(); }

                //! Returns the name  of the geometric reference element (for instance 'Triangle3')
                static std::string Name() { return TraitsRefGeomEltT::Name(); }

                //! Get the identifier of the geometric element.
                virtual Advanced::GeometricEltEnum GetIdentifier() const override final;

                //! Get a reference to the reference geometric element.
                virtual const RefGeomElt& GetRefGeomElt() const override = 0;

                //! Get the name of the geometric element.
                virtual const std::string GetName() const override final;

                //! \copydoc doxygen_hide_geometric_elt_Ncoords_method
                virtual unsigned int Ncoords() const override final;

                //! Get the number of summits.
                virtual unsigned int Nvertex() const override final;

                //! Get the number of faces.
                virtual unsigned int Nface() const override final;

                //! Get the number of edges.
                virtual unsigned int Nedge() const override final;

                //! Get the dimension of the geometric element.
                virtual unsigned int GetDimension() const override final;


                //! \name Shape function methods.
                //@{
                virtual double ShapeFunction(unsigned int i,
                                             const LocalCoords& local_coords) const override final;

                //! Return the value of the icoor-th derivative of the i-th shape function on given point.
                virtual double FirstDerivateShapeFunction(unsigned int i, unsigned int icoor,
                                                          const LocalCoords& local_coords) const override final;

                //! Return the value of the (icoor, jcoor)-th second derivative of the i-th shape function on given point.
                virtual double SecondDerivateShapeFunction(unsigned int i, unsigned int icoor, unsigned int jcoor,
                                                           const LocalCoords& local_coords) const override final;

                //@}



            private:


                /*!
                 * \brief Build the list of all vertices that belongs to the geometric element.
                 *
                 * \param[in,out] existing_list List of all vertice that have been built so far for the enclosing geometric
                 * mesh. If a vertex already exists, point to the already existing object instead of recreating one.
                 * If not, create a new object from scratch and add it in the existing_list.
                 * \copydoc doxygen_hide_geom_elt_build_interface_ptr
                 */
                virtual void BuildVertexList(const GeometricElt* geom_elt_ptr,
                                             Vertex::InterfaceMap& existing_list) override final;


                /*!
                 * \brief Build the list of all edges that belongs to the geometric element.
                 *
                 * \param[in,out] existing_list List of all edges that have been built so far for the enclosing geometric
                 * mesh. If an edge already exists, point to the already existing object instead of recreating one.
                 * If not, create a new object from scratch and add it in the existing_list.
                 * \copydoc doxygen_hide_geom_elt_build_interface_ptr
                 *
                 */
                virtual void BuildEdgeList(const GeometricElt* geom_elt_ptr,
                                           Edge::InterfaceMap& existing_list) override final;

                /*!
                 * \brief Build the list of all faces that belongs to the geometric element.
                 *
                 * \param[in,out] existing_list List of all faces that have been built so far for the enclosing geometric
                 * mesh. If a face already exists, point to the already existing object instead of recreating one.
                 * If not, create a new object from scratch and add it in the existing_list.
                 * \copydoc doxygen_hide_geom_elt_build_interface_ptr
                 *
                 */
                virtual void BuildFaceList(const GeometricElt* geom_elt_ptr,
                                           Face::InterfaceMap& existing_list) override final;


                /*!
                 * \brief Build the pointer to the volume if there is one to consider as interface.
                 *
                 * \copydoc doxygen_hide_geom_elt_build_interface_ptr
                 * \param[in,out] existing_list This list is mostly there for genericity, so that same C++ interface might
                 * be used for all 4 kind of interfaces. It is not used however as a searching way to see if the interface
                 * already exist or not: by nature a Volume interface can appear only once for its related GeometricElt.
                 */
                virtual void BuildVolumeList(const GeometricElt* geom_elt_ptr,
                                             Volume::InterfaceMap& existing_list) override final;



            public :

                //! \name Ensight-related methods.
                //@{
                //! Get the Ensight name. If Ensight doesn't support the type empty string is returned.
                virtual const std::string& GetEnsightName() const override;

                /*!
                 * \brief Write the geometric element in Ensight format.
                 *
                 * \param[in,out] stream Stream to which the GeometricElt is written.
                 *
                 * \param do_print_index True if the geometric elementis preceded by its internal index.
                 */
                virtual void WriteEnsightFormat(std::ostream& stream, bool do_print_index) const override final;
                //@}

            public :

                //! \name Medit-related methods.
                //@{


                /*!
                 * \brief Get the identifier Medit use to tag the geometric element.
                 *
                 * An exception is thrown if Medit format is not supported.
                 *
                 * \return Identifier.
                 */

                virtual GmfKwdCod GetMeditIdentifier() const override final;


                /*!
                 * \brief Write the geometric element in Medit format.
                 *
                 * \param[in] mesh_index Index that was returned by Medit when the output file was opened.
                 * \param[in] processor_wise_reindexing Medit requires the indexes run from 1 to Ncoord on each processor;
                 * this container maps the Coords index to the processor-wise one to write in the Medit file.
                 *
                 * The method is indeed an underlying call to the Medit API.
                 */
                virtual void WriteMeditFormat(int mesh_index,
                                              const std::unordered_map<unsigned int, int>& processor_wise_reindexing) const override final;


                //! \copydoc doxygen_hide_geometric_elt_read_medit_method
                virtual void ReadMeditFormat(const Coords::vector_unique_ptr& mesh_coords_list,
                                             int libmesh_mesh_index,
                                             unsigned int Ncoord_in_mesh,
                                             int& reference_index) override final;

            private:


                /*!
                 * \brief Assert at compile time that the number of coordinates in shape functions is consistent
                 * with the topological dimension.
                 *
                 */
                void StaticAssertBuiltInConsistency();


            };


        } // namespace GeomEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/GeometricElt/Advanced/TGeometricElt.hxx"


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_T_GEOMETRIC_ELT_HPP_
