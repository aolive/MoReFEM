///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 2 May 2013 14:45:26 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_GEOMETRIC_ELT_ENUM_HPP_
# define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_GEOMETRIC_ELT_ENUM_HPP_


#include <functional>


namespace MoReFEM
{


    namespace Advanced
    {


        /*!
         * This enum class defines an alias for each geometric element.
         *
         * Doing so breaks some of the beauty of the GeometricElt factory: each GeometricElt has now to create a value
         * in this enum here, which will trigger more compilation.
         * However, adding a new geometric element should be quite rare, contrary to element lookup, and using
         * the name of the class as the identifier would hamper the performances.
         *
         * The following enum class should only be used internally.
         *
         */
        enum class GeometricEltEnum : std::size_t
        {
            Begin = 0,
            Point1 = Begin,
            Segment2, Segment3,
            Triangle3, Triangle6,
            Quadrangle4, Quadrangle8, Quadrangle9,
            Tetrahedron4, Tetrahedron10,
            Hexahedron8, Hexahedron20, Hexahedron27,

            //Pyram5, Pyram13, // \todo currently not defined
            //Prism6, Prism15 // \todo currently not defined

            Nref_geom_elt,
            End = Nref_geom_elt, // when iterating, we do not want to consider the placeholder Undefined.
            Undefined
        };


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


namespace std
{


    //! Provide hash function for the enum class
    template<>
    struct hash<MoReFEM::Advanced::GeometricEltEnum>
    {
    public:

        //! Overload.
        std::size_t operator()(MoReFEM::Advanced::GeometricEltEnum value) const
        {
            return std::hash<int>()(static_cast<int>(value));
        }
    };


}


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_GEOMETRIC_ELT_ENUM_HPP_
