///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Jan 2013 16:47:52 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_GEOMETRIC_ELT_FACTORY_HPP_
# define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_GEOMETRIC_ELT_FACTORY_HPP_


# include <map>
# include <unordered_map>
# include <iostream>
# include "Utilities/Singleton/Singleton.hpp"
# include "Geometry/GeometricElt/GeometricElt.hpp"
# include "Geometry/RefGeometricElt/RefGeomElt.hpp"
# include "Geometry/GeometricElt/Internal/Exceptions/GeometricEltFactory.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        /*!
         * \brief The purpose of this class is to create on demand a pointer to a newly created object
         * which type depends on the name given in a specific format.
         *
         * For instance, 'CreateFromEnsightName' with 'geometric_elt_name'='tria3' and a valid input stream will
         * return a pointer to a P1 triangle created from data of the input stream.
         *
         * This class is implemented as a singleton: there is a unique instance of it that should be created
         * at the start of the program. The content of the class should remain unchanged after initialisation:
         * all geometric elements must register themselves at the beginning and the class should remain unchanged after this step.
         * Therefore, there should not be any problem related to multi-threading, as this class acts in a read-only
         * way.
         *
         *
         */
        class GeometricEltFactory final : public Utilities::Singleton<GeometricEltFactory>
        {
        public:

            /*!
             * \brief Alias for a function which will create a 'GeometricElt' out of a constructor which takes only
             * Mesh unique id as argument.
             */
            using CreateGeometricEltCallBack = std::function<GeometricElt::unique_ptr(unsigned int mesh_unique_id)>;


            //! Alias for a function which will create a 'GeometricElt' out of a constructor with istream argument.
            using CreateGeometricEltCallBackIstream = std::function<GeometricElt::unique_ptr(unsigned int mesh_unique_id,
                                                                                             const Coords::vector_unique_ptr& mesh_coords_list,
                                                                                             std::istream&)>;


            /*!
             * \brief Convenient alias.
             *
             * Key is string identifier of a geometric element.
             * Value is a pointer to a function that will create the chosen geometric element (by calling constructor.
             * without argument)
             */
            using CallBack = std::unordered_map<Advanced::GeometricEltEnum, CreateGeometricEltCallBack>;


            /*!
             * \brief Convenient alias.
             *
             * Key is string identifier of a geometric reference element (e.g. 'Triangle3').
             * Value is the value in the enum matching this elements.
             */
            using MatchNameEnum = std::unordered_map<std::string, Advanced::GeometricEltEnum>;



            /*!
             * \brief Convenient alias.
             *
             * Key is Ensight string identifier of a geometric element.
             * Value is a pointer to a function that will create the chosen geometric element (by calling constructor
             * with as argument the input stream being read, typically an Ensight file).
             */
            using CallBackEnsight = std::unordered_map<std::string, CreateGeometricEltCallBackIstream>;


            /*!
             * \brief Convenient alias.
             *
             * Key is Medit identifier of a geometric element.
             * Value is a pointer to a function that will create the chosen geometric element (by calling constructor
             * without argument).
             */
            using CallBackMedit = std::map<GmfKwdCod, CreateGeometricEltCallBack>;

            //! Name of the class (required for some Singleton-related errors).
            static const std::string ClassName();


        public:


            /*!
             * \brief Register a geometric element
             *
             * Each geometric element *NewGeometricEltt* should register itself with the line
             * GeometricEltFactory::CreateOrGetInstance(invoking_file, invoking_line).RegisterGeometricElt<*NewGeometricElt*>(*CreateGeometricEltCallBack used to create an object*)
             *
             * \tparam RefGeomEltT Type of the 'RefGeomElt' to be registered. This is an object that should be
             * defined in GeoRef namespace (for instance RefGeomEltNS::Triangle6); beware however: it is the polymorphic class
             * that is required here, not the mere traits class (not RefGeomEltNS::Traits::Triangle6 for instance).
             * \param[in] default_create The 'CreateGeometricEltCallBack' that is used to create a RefGeomEltT object with
             * a constructor that takes only a const reference to a Mesh.
             * \param[in] ensight_create The 'CreateGeometricEltCallBackIstream' that is used to create a RefGeomEltT object with
             * constructor that takes a istream argument; Ensight geometric elementname is expected here. nullptr is expected if Ensight is not supported
             *
             * \return True if the geometric element was registered (and actually never false as exceptions are thrown in those case)
             * The value is needed because we need to init a value if we call this outside the boundaries of a function
             * (see how a 'GeometricElt', eg 'Triangle3' is registered; the trick is taken from "Modern C++ Design", A. Alexandrescu,
             * chapter 8 P 205 (even if the need for return value is not emphasized in the text)
             */
            template<class RefGeomEltT>
            bool RegisterGeometricElt(CreateGeometricEltCallBack default_create,
                                      CreateGeometricEltCallBackIstream ensight_create);

            /*!
             * \brief Create an object according to its Ensight name.
             *
             * \param[in] mesh_unique_id Unique identifier of the mesh.
             * \param[in] mesh_coords_list List of all Coords present in the mesh.
             * \param[in] geometric_elt_name Name of the geometric element in Ensight. For instance "tria3".
             * for P1 triangle
             * If the 'geometric_elt_name' given as argument is invalid an exception is thrown.
             * \param[in,out] stream Input stream that help to construct the object.
             *
             * For instance "3 12 15" (indexes of the coords used).
             *
             * \internal <b><tt>[internal]</tt></b> Mesh is not given here to derive id and coords
             * because at the time of this call its list of Coords might not be completely set.
             *
             * \return Pointer to a newly created object. If the stream fails to deliver what is expected, nullptr is returned.
             */
            GeometricElt::unique_ptr CreateFromEnsightName(unsigned int mesh_unique_id,
                                                           const Coords::vector_unique_ptr& mesh_coords_list,
                                                           const std::string& geometric_elt_name,
                                                           std::istream& stream) const;

            /*!
             * \brief Returns the shared pointer to the RefGeomElt related to GeometricEltEnum.
             *
             * Remember that RefGeomElt holds no data members, so this type is a strawman for a traits class.
             * So to put in a nutshell all instantiations of this class are purely identical, hence the unique smart
             * pointer held by the factory.
             *
             * \param[in] identifier Identifier of the object (member of GeometricEltEnum)
             * If the id given as argument is invalid an exception is thrown.
             *
             * \return Pointer to the built-in object that is the flagship of its class.
             */
            RefGeomElt::shared_ptr GetRefGeomEltPtr(Advanced::GeometricEltEnum identifier) const;

            //! Same as GetRefGeomEltPtr but returns a const reference.
            const RefGeomElt& GetRefGeomElt(Advanced::GeometricEltEnum identifier) const;

            //! Returns the identifier of a RefGeomElt from its name.
            Advanced::GeometricEltEnum GetIdentifier(const std::string& geometric_reference_name) const;

            /*!
             * \brief Return the list of all recognized geometric element names.
             */
            const std::string EnsightGeometricEltNames() const;

            //! Number of elements registered in the factory.
            CallBack::size_type NgeometricElt() const;

            //! Whether a given name is one of accepted Ensight name or not.
            bool IsEnsightName(const std::string& name) const;

            //! List of all known Medit geometric elements in keys, and a function that creates default object in value.
            const CallBackMedit& MeditRefGeomEltList() const;

            //! Number of coords matching a given Medit geometric element code.
            unsigned int NumberOfCoordsInGeometricElt(GmfKwdCod code) const;

            /*!
             * \brief Get the list of names of geometric elements known to the factory.
             */
            const std::unordered_set<std::string>& GetNameList() const;


        private:


            //! \name Singleton requirements.
            ///@{
            //! Constructor.
            GeometricEltFactory();

            //! Friendship declaration to Singleton template class (to enable call to constructor).
            friend class Utilities::Singleton<GeometricEltFactory>;
            ///@}



        private:



            /*!
             * \brief Associative container to choose the right function given its string identifier.
             *
             * Key is the string identifier, which for easiness should be the name of the class, eg "Triangle3"
             * for 'Triangle3' class.
             * Value is the function used to create an object of the correct type, eg 'Triangle3' in our example.
             *
             */
            CallBack callbacks_;


            /*!
             * \brief Associative container to match a name of RefGeomElt to an enum.
             *
             * Key is string identifier of a geometric reference element (e.g. 'Triangle3').
             * Value is the value in the enum matching this elements.
             */
            MatchNameEnum match_name_enum_;


            /*!
             * \brief Associative container to choose the right function given a Ensight geometric element name.
             *
             * Key is the Ensight name for the geometric element. For instance "tria3" for 'Triangle3'.
             * Value is the function used to create an object of the correct type, eg 'Triangle3' in our example.
             *
             */
            CallBackEnsight callbacks_ensight_;


            /*!
             * \brief List of all recognized Medit types.
             */
            CallBackMedit callbacks_medit_;


            /*!
             * \brief number of coords for each geometric element type that is supported by Medit.
             */
            std::map<GmfKwdCod, unsigned int> Ncoord_medit_;


            /*!
             * \brief Stores for each GeometricEltEnum the related RefGeomElt object.
             *
             * Such objects hold absolutely no data members, so this type is a strawman for a traits class.
             * To put in a nutshell all instantiations of RefGeomElt class for a given GeometricEltEnum
             * are purely identical, hence the smart pointer held directly in the factory.
             */
            std::unordered_map<Advanced::GeometricEltEnum, RefGeomElt::shared_ptr> geom_ref_elt_type_list_;


            /*!
             * \brief List of all geometric element names in the factory.
             *
             * For the factory purposes, such names are rather irrelevant: internal work relies on more efficient
             * enums. However, the name is useful for debugging purpose, and it would be rather annoying to
             * be misled because the creator of a new GeometricElt forgot to replace a name after a copy/paste.
             *
             */
            std::unordered_set<std::string> geometric_elt_name_list_;

        };


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hxx"


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_GEOMETRIC_ELT_FACTORY_HPP_
