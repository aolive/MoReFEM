target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ComputeColoring.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Mesh.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ComputeColoring.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Format.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Mesh.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Mesh.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/Advanced/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
