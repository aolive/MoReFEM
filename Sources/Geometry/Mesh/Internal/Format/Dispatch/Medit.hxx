///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sat, 23 Apr 2016 22:11:19 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_DISPATCH_x_MEDIT_HXX_
# define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_DISPATCH_x_MEDIT_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            namespace FormatNS
            {


                namespace Medit
                {


                    namespace Dispatch
                    {


                        template<class GeoRefElementT>
                        [[noreturn]] GmfKwdCod GetIdentifier(std::false_type)
                        {
                            throw MoReFEM::ExceptionNS::Format::UnsupportedGeometricElt(GeoRefElementT::ClassName(),
                                                                                           "Medit",
                                                                                           __FILE__, __LINE__);
                        }


                        template<class GeoRefElementT>
                        GmfKwdCod GetIdentifier(std::true_type)
                        {
                            return Internal::MeshNS::FormatNS::Support<::MoReFEM::MeshNS::Format::Medit, GeoRefElementT::Identifier()>::MeditId();
                        }




                        template<class GeoRefElementT, unsigned int NcoordT>
                        inline void WriteFormat(std::true_type,
                                                         int mesh_index,
                                                         GmfKwdCod geometric_elt_code,
                                                         const std::vector<int>& coords,
                                                         int label_index)
                        {
                            ::MoReFEM::Wrappers::Libmesh
                            ::MeditSetLin<NcoordT>(mesh_index, geometric_elt_code, coords, label_index);
                        }


                        template<class GeoRefElementT, unsigned int NcoordT>
                        [[noreturn]] void WriteFormat(std::false_type,
                                                               int mesh_index,
                                                               GmfKwdCod geometric_elt_code,
                                                               const std::vector<int>& coords,
                                                               int label_index)
                        {
                            // Prevent warnings about unused variables
                            static_cast<void>(geometric_elt_code);
                            static_cast<void>(mesh_index);
                            static_cast<void>(coords);
                            static_cast<void>(label_index);


                            throw MoReFEM::ExceptionNS::GeometricElt::FormatNotSupported(GeoRefElementT::ClassName(),
                                                                                            "Medit",
                                                                                            __FILE__, __LINE__);

                        }


                        template<class GeoRefElementT, unsigned int NcoordT>
                        inline void ReadFormat(std::true_type,
                                                        int mesh_index,
                                                        GmfKwdCod geometric_elt_code,
                                                        std::vector<unsigned int>& coords,
                                                        int& label_index)
                        {
                            ::MoReFEM::Wrappers::Libmesh
                            ::MeditGetLin<NcoordT>(mesh_index, geometric_elt_code, coords, label_index);
                        }


                        template<class GeoRefElementT, unsigned int NcoordT>
                        [[noreturn]] void ReadFormat(std::false_type,
                                                              int mesh_index,
                                                              GmfKwdCod geometric_elt_code,
                                                              std::vector<unsigned int>& coords,
                                                              int& label_index)
                        {
                            // Prevent warnings about unused variables
                            static_cast<void>(geometric_elt_code);
                            static_cast<void>(mesh_index);
                            static_cast<void>(coords);
                            static_cast<void>(label_index);


                            throw MoReFEM::ExceptionNS::GeometricElt::FormatNotSupported(GeoRefElementT::ClassName(),
                                                                                            "Medit",
                                                                                            __FILE__, __LINE__);
                        }


                    } // namespace Dispatch


                } // namespace Medit


            } // namespace FormatNS


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_DISPATCH_x_MEDIT_HXX_
