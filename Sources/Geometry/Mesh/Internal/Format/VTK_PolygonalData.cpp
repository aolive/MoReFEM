///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 17 Aug 2016 11:17:47 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#include <fstream>
#include <sstream>
#include <numeric>
#include <algorithm>
#include <iostream>


#include "Utilities/Containers/Sort.hpp"

#include "Geometry/Mesh/Internal/Format/Format.hpp"
#include "Geometry/Mesh/Mesh.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"
#include "Geometry/Mesh/Internal/Format/Exceptions/Format_fwd.hpp"
# include "Geometry/Mesh/Internal/Format/VTK_PolygonalData.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace MeshNS
        {
            
            
            namespace FormatNS
            {
                
                
                const std::string& Informations<::MoReFEM::MeshNS::Format::VTK_PolygonalData>::Name()
                {
                    static std::string ret("VTK_PolygonalData");
                    return ret;
                }
                
                
                const std::string& Informations<::MoReFEM::MeshNS::Format::VTK_PolygonalData>::Extension()
                {
                    static std::string ret("vtk");
                    return ret;
                }
                
                             
                
                namespace VTK_PolygonalData
                {

                    
                    
                    
                    [[noreturn]] void ReadFile(const unsigned int mesh_id,
                                                 const std::string& mesh_file,
                                                 double space_unit,
                                                 unsigned int& dimension,
                                                 GeometricElt::vector_shared_ptr& unsort_geom_element_list,
                                                 Coords::vector_unique_ptr& coords_list,
                                                 MeshLabel::vector_const_shared_ptr& mesh_label_list)
                    {
                        static_cast<void>(mesh_id);
                        static_cast<void>(mesh_file);
                        static_cast<void>(space_unit);
                        static_cast<void>(dimension);
                        static_cast<void>(unsort_geom_element_list);
                        static_cast<void>(coords_list);
                        static_cast<void>(mesh_label_list);

                        assert(false && "Read file not implemented yet for VTK_PolygonalData.");
                        exit(EXIT_FAILURE);
                    }
                    
                    
                    void WriteFile(const Mesh& mesh,
                                   const std::string& mesh_file)
                    {
                        std::ofstream file_out(mesh_file);
                        
                        if (!file_out)
                            throw ExceptionNS::Format::UnableToOpenFile(mesh_file, __FILE__, __LINE__);
                        
                        std::cout << "Writing to file "<< mesh_file << std::endl;
                        
                        file_out << "# vtk DataFile Version 1.0\n";
                        file_out << "VTK Mesh from MoReFEM\n";
                        file_out << "ASCII\n";
                        file_out << "DATASET POLYDATA\n";
                        
                        // Write points.
                        file_out << "POINTS ";
                        
                        decltype(auto) coords_list = mesh.GetProcessorWiseCoordsList();
                        auto Ncoords = coords_list.size();
                        file_out << Ncoords << " double\n";
                        
                        // Now write all coords
                        for (unsigned int i = 0u; i < Ncoords; ++i)
                            WriteVTK_PolygonalDataFormat(*coords_list[i], file_out);
                        
                        //VERTICES LINES  POLYGONS TRIANGLE_STRIPS
                        file_out << "POLYGONS ";
                        
                        std::cout << "[WARNING] The VTK format will only write the triangles of the mesh considered for now: " << mesh_file << std::endl;
                        
                        decltype(auto) geometric_type =
                        Advanced::GeometricEltFactory::GetInstance(__FILE__, __LINE__).GetRefGeomElt(Advanced::GeometricEltEnum::Triangle3);
                        
                        auto subset = mesh.GetSubsetGeometricEltList(geometric_type);
                        
                        unsigned int number_of_elements = 0;
                        
                        for (auto it_geom_elemt = subset.first; it_geom_elemt != subset.second; ++it_geom_elemt)
                        {
                            ++number_of_elements;
                        }

                        file_out << number_of_elements << ' ' << 4 * number_of_elements << '\n';
                        
                        for (auto it_geom_elemt = subset.first; it_geom_elemt != subset.second; ++it_geom_elemt)
                        {
                            const auto& geom_elt_ptr = *it_geom_elemt;
                            assert(!(!geom_elt_ptr));
                            
                            const auto& geom_elt_coords_list = geom_elt_ptr->GetCoordsList();
                            
                            assert(coords_list.size() == 3);
                            
                            file_out << "3" << '\t' << geom_elt_coords_list[0]->GetIndex() << '\t' <<  geom_elt_coords_list[1]->GetIndex() << '\t' <<  geom_elt_coords_list[2]->GetIndex() << '\n';
                        }
                    }

                    
                } // namespace VTK_PolygonalData
                
                
            } // namespace FormatNS
            
            
        } // namespace MeshNS
        
        
    } // namespace Internal
  

} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
