///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Mon, 13 Jun 2016 17:23:45 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#include "Geometry/Mesh/Internal/PseudoNormalsManager.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        const std::string& PseudoNormalsManager::ClassName()
        {
            static std::string ret("PseudoNormalsManager");
            return ret;
        }
        
        
        PseudoNormalsManager::PseudoNormalsManager() = default;
        
        
        void PseudoNormalsManager::Create(const std::vector<unsigned int>& domain_index_list,
                                          Mesh& mesh)
        {
            std::cout << "===========================================" << std::endl;
            std::cout << "[WARNING] Pseudo-normals computation." << std::endl;
            std::cout << "For now the computation is limited to Triangle3 and the corresponding Edges and Vertices."
            << std::endl;
            std::cout << "In the case of a volumic mesh the orientation of the triangles with respect to the volume is "
            "tested and in this case all the triangles must be defined with an outside oriented normal." << std::endl;
            
            const auto& domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);
            
            std::vector<unsigned int> label_list_index;
            
            // If domain_index_list is empty no restriction applied on domain.
            if (domain_index_list.empty())
            {
                const auto& mesh_label_list = mesh.GetLabelList();
                
                const unsigned int mesh_label_list_size = static_cast<unsigned int>(mesh_label_list.size());
                
                assert(std::none_of(mesh_label_list.cbegin(),
                                    mesh_label_list.cend(),
                                    Utilities::IsNullptr<MeshLabel::const_shared_ptr>));
                
                for (unsigned int i = 0u; i < mesh_label_list_size; ++i)
                {
                    const auto& mesh_label = *mesh_label_list[i];                    
                    label_list_index.push_back(mesh_label.GetIndex());
                }
            }
            else
            {
                for (const auto domain_index : domain_index_list)
                {
                    const auto& mesh_label_list =
                        domain_manager.GetDomain(domain_index, __FILE__, __LINE__).GetMeshLabelList();
                    
                    assert(std::none_of(mesh_label_list.cbegin(),
                                        mesh_label_list.cend(),
                                        Utilities::IsNullptr<MeshLabel::const_shared_ptr>));
                    
                    const unsigned int mesh_label_list_size = static_cast<unsigned int>(mesh_label_list.size());
                    
                    for (unsigned int j = 0u; j < mesh_label_list_size; ++j)
                    {
                        const auto& mesh_label = *mesh_label_list[j];
                        label_list_index.push_back(mesh_label.GetIndex());
                    }
                }
            }
            
            // mesh.ComputePseudoNormals(label_list_index); //#938 deactivated for the moment.
            
            std::cout << "===========================================" << std::endl;
        }
        
    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
