///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 7 Apr 2014 11:35:24 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_EXCEPTIONS_x_GEOMETRIC_ELT_LIST_HPP_
# define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_EXCEPTIONS_x_GEOMETRIC_ELT_LIST_HPP_


# include "Utilities/Exceptions/Exception.hpp"
# include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"


namespace MoReFEM
{


    namespace ExceptionNS
    {


        namespace GeometricEltList
        {


            //! Generic exception thrown by a GeometricEltList object.
            class Exception : public MoReFEM::Exception
            {
            public:

                /*!
                 * \brief Constructor with simple message
                 *
                 * \param[in] msg Message
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                 */
                explicit Exception(const std::string& msg, const char* invoking_file, int invoking_line);

                //! Destructor
                virtual ~Exception() override;

                //! Copy constructor.
                Exception(const Exception&) = default;

                //! Move constructor.
                Exception(Exception&&) = default;

                //! Copy affectation.
                Exception& operator=(const Exception&) = default;

                //! Move affectation.
                Exception& operator=(Exception&&) = default;

            };


            //! Thrown when GeometricEltList::Init() is called upon an object already initialized.
            class InitNotCleared final : public Exception
            {
            public:

                /*!
                 * \brief Constructor with simple message
                 *
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                 */
                explicit InitNotCleared(const char* invoking_file, int invoking_line);

                //! Destructor
                virtual ~InitNotCleared();

                //! Copy constructor.
                InitNotCleared(const InitNotCleared&) = default;

                //! Move constructor.
                InitNotCleared(InitNotCleared&&) = default;

                //! Copy affectation.
                InitNotCleared& operator=(const InitNotCleared&) = default;

                //! Move affectation.
                InitNotCleared& operator=(InitNotCleared&&) = default;
            };


            /*!
             * \brief Thrown when a request was done for a type not found in the GeometricEltList.
             *
             * For instance if you request a TriangleP1 and none is found, this exception is thrown.
             * The reason for this is that the user should have kept track of the type actually present
             * (GeometricEltList provides this information if needed!)
             */
            class InvalidRequest final : public Exception
            {
            public:

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] geometric_type Geometric element type which was requested.
                 * \param[in] label Index of the label requested.
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                 */
                explicit InvalidRequest(const RefGeomElt& geometric_type,
                                        unsigned int label,
                                        const char* invoking_file, int invoking_line);

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] geometric_type Geometric element type which was requested.
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                 */
                explicit InvalidRequest(const RefGeomElt& geometric_type,
                                        const char* invoking_file, int invoking_line);


                /*!
                 * \brief Constructor.
                 *
                 * \param[in] dimension Dimension of the geometric element requested.
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                 */
                explicit InvalidRequest(unsigned int dimension,
                                        const char* invoking_file, int invoking_line);


                //! Destructor
                virtual ~InvalidRequest();

                //! Copy constructor.
                InvalidRequest(const InvalidRequest&) = default;

                //! Move constructor.
                InvalidRequest(InvalidRequest&&) = default;

                //! Copy affectation.
                InvalidRequest& operator=(const InvalidRequest&) = default;

                //! Move affectation.
                InvalidRequest& operator=(InvalidRequest&&) = default;
            };



            //! Thrown when indexes of the geometric elements read are not unique.
            class DuplicateInGeometricEltIndex final : public Exception
            {
            public:

                /*!
                 * \brief Constructor with simple message.
                 *
                 * \param[in] Ngeometric_element Number of \a GeometricElt.
                 * \param[in] Nunique_indexes Number of unique indexes allotted after building of all \a GeometricElt.
                 * It should have been equal to \a Ngeometric_element.
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                 *
                 */
                explicit DuplicateInGeometricEltIndex(unsigned int Ngeometric_element,
                                                      unsigned int Nunique_indexes,
                                                      const char* invoking_file, int invoking_line);

                //! Destructor
                virtual ~DuplicateInGeometricEltIndex();

                //! Copy constructor.
                DuplicateInGeometricEltIndex(const DuplicateInGeometricEltIndex&) = default;

                //! Move constructor.
                DuplicateInGeometricEltIndex(DuplicateInGeometricEltIndex&&) = default;

                //! Copy affectation.
                DuplicateInGeometricEltIndex& operator=(const DuplicateInGeometricEltIndex&) = default;

                //! Move affectation.
                DuplicateInGeometricEltIndex& operator=(DuplicateInGeometricEltIndex&&) = default;
            };



        } // namespace GeometricEltList


    } // namespace ExceptionNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_EXCEPTIONS_x_GEOMETRIC_ELT_LIST_HPP_
