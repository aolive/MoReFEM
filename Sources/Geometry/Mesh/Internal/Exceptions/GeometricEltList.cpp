///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 30 Apr 2013 14:56:26 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#include <sstream>
#include "Geometry/RefGeometricElt/RefGeomElt.hpp"
#include "Geometry/Mesh/Internal/Exceptions/GeometricEltList.hpp"


namespace // anonymous
{
    
    
    std::string InvalidRequestMsg(const MoReFEM::RefGeomElt& geometric_type, unsigned int label);
    
    std::string InvalidRequestMsg(const MoReFEM::RefGeomElt& geometric_type);
    
    std::string InvalidRequestMsg(unsigned int dimension);
    
    std::string DuplicateInGeometricEltIndexMsg(unsigned int Ngeometric_element,
                                                    unsigned int Nunique_indexes);
    
    
} // namespace anonymous



namespace MoReFEM
{
    

    namespace ExceptionNS
    {
        
        
        namespace GeometricEltList
        {
            
            
            Exception::~Exception() = default;
            
            
            Exception::Exception(const std::string& msg, const char* invoking_file, int invoking_line)
            : MoReFEM::Exception(msg, invoking_file, invoking_line)
            { }
            
            
            InitNotCleared::InitNotCleared(const char* invoking_file, int invoking_line)
            : Exception("GeometricEltList::Init() can only be called upon an empty object",
                        invoking_file, invoking_line)
            { }
            
            
            InitNotCleared::~InitNotCleared() = default;
            
            
            InvalidRequest::InvalidRequest(const RefGeomElt& geometric_type,
                                           unsigned int label,
                                           const char* invoking_file, int invoking_line)
            : Exception(InvalidRequestMsg(geometric_type, label),
                        invoking_file, invoking_line)
            { }
            
            
            InvalidRequest::InvalidRequest(unsigned int dimension,
                                           const char* invoking_file, int invoking_line)
            : Exception(InvalidRequestMsg(dimension),
                        invoking_file, invoking_line)
            { }

            
            
            InvalidRequest::InvalidRequest(const RefGeomElt& geometric_type,
                                           const char* invoking_file, int invoking_line)
            : Exception(InvalidRequestMsg(geometric_type),
                        invoking_file, invoking_line)
            { }
            
            
            InvalidRequest::~InvalidRequest() = default;
            
            
            DuplicateInGeometricEltIndex::~DuplicateInGeometricEltIndex() = default;
            
            
            DuplicateInGeometricEltIndex::DuplicateInGeometricEltIndex(unsigned int Ngeometric_element,
                                                                       unsigned int Nunique_indexes,
                                                                       const char* invoking_file, int invoking_line)
            : Exception(DuplicateInGeometricEltIndexMsg(Ngeometric_element, Nunique_indexes),
                        invoking_file, invoking_line)
            { }
            
            
        } // namespace GeometricEltList
        
        
    } // namespace ExceptionNS
        
    
} // namespace MoReFEM



namespace // anonymous
{
    
    
    std::string InvalidRequestMsg(const MoReFEM::RefGeomElt& geometric_type, unsigned int label)
    {
        std::ostringstream oconv;
        
        oconv << "Position and number of geometric elements with type " << geometric_type.GetName()
        << " and label " << label << " was requested, but no such element exist (the first index is the position "
        << "of the type in enumeration provided in Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp).";
        
        return oconv.str();
    }
    
    
    std::string InvalidRequestMsg(const MoReFEM::RefGeomElt& geometric_type)
    {
        std::ostringstream oconv;
        
        oconv << "Position and number of geometric elements with type " << geometric_type.GetName()
        << " was requested, but no such element exist (the first index is the position "
        << "of the type in enumeration provided in Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp).";
        
        return oconv.str();
    }
    
    
    std::string InvalidRequestMsg(unsigned int dimension)
    {
        std::ostringstream oconv;
        
        oconv << "Position and number of geometric elements with dimension " << dimension
        << " was requested, but no such element exist.";
        
        return oconv.str();
    }
    
    
    std::string DuplicateInGeometricEltIndexMsg(unsigned int Ngeometric_element,
                                                unsigned int Nunique_indexes)
    {
        std::ostringstream oconv;
        
        oconv << "GeometricElt indexes are expected to be unique, and that is not the case here: "
        << Nunique_indexes << " unique indexes were found for " << Ngeometric_element << " geometric elements.";
        
        return oconv.str();
    }
    
    
} // namespace anonymous


/// @} // addtogroup GeometryGroup
