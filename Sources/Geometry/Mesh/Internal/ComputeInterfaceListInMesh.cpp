///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 24 Nov 2014 14:59:10 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#include "Utilities/Containers/Print.hpp"

#include "Geometry/Mesh/Mesh.hpp"
#include "Geometry/Mesh/Internal/ComputeInterfaceListInMesh.hpp"


namespace MoReFEM
{


    namespace Internal
    {
        
        
        namespace MeshNS
        {
        
        
            namespace // anonymous
            {
                
                
                template<class OrientedInterfaceT>
                const auto& GetInterfaceList(const GeometricElt& geom_elt);
                
                
                //! Add to \a mesh_interface_list the interfaces of \a geom_elt which might not yet be taken into account.
                template<class OrientedInterfaceT>
                void ComputeMeshLevelList(const GeometricElt& geom_elt,
                                          typename OrientedInterfaceT::vector_shared_ptr& mesh_interface_list);
                
                
                template<>
                void ComputeMeshLevelList<Volume>(const GeometricElt& geom_elt,
                                                  Volume::vector_shared_ptr& mesh_volume_list);
                

                template<class OrientedInterfaceT>
                void PrintTypeOfInterface(const typename OrientedInterfaceT::vector_shared_ptr& interface_list,
                                          std::ostream& out);
             
                
                
                
            } // namespace anonymous
            
            
            
            ComputeInterfaceListInMesh::ComputeInterfaceListInMesh(const Mesh& mesh)
            {
                const auto& geom_elt_list = mesh.GetGeometricEltList();
                
                // The list of interfaces is not stored as such in mesh...
                vertex_list_.resize(mesh.Nvertex(), nullptr);
                edge_list_.resize(mesh.Nedge(), nullptr);
                face_list_.resize(mesh.Nface(), nullptr);
                volume_list_.resize(mesh.Nvolume(), nullptr);
                
                for (const auto& geom_elt_ptr : geom_elt_list)
                {
                    assert(!(!geom_elt_ptr));
                    
                    const auto& geom_elt = *geom_elt_ptr;
                    
                    ComputeMeshLevelList<Vertex>(geom_elt, vertex_list_);
                    ComputeMeshLevelList<OrientedEdge>(geom_elt, edge_list_);
                    ComputeMeshLevelList<OrientedFace>(geom_elt, face_list_);
                    ComputeMeshLevelList<Volume>(geom_elt, volume_list_);
                }
                
                assert(std::none_of(vertex_list_.cbegin(), vertex_list_.cend(),
                                    Utilities::IsNullptr<Vertex::shared_ptr>));
                assert(std::none_of(edge_list_.cbegin(), edge_list_.cend(),
                                    Utilities::IsNullptr<OrientedEdge::shared_ptr>));
                assert(std::none_of(face_list_.cbegin(), face_list_.cend(),
                                    Utilities::IsNullptr<OrientedFace::shared_ptr>));
                assert(std::none_of(volume_list_.cbegin(), volume_list_.cend(),
                                    Utilities::IsNullptr<Volume::shared_ptr>));
            }
            
            
            void ComputeInterfaceListInMesh::Print(std::ostream& out) const
            {
                out << "#For each geometric interface, give the list of the Coords that delimits it. The index shown "
                "is the one from the original Mesh." << std::endl;
                
                {
                    const auto& vertex_list = GetVertexList();
                    for (auto vertex_ptr : vertex_list)
                    {
                        assert(!(!vertex_ptr));
                        out << vertex_ptr->GetNature() << ' ' << vertex_ptr->GetIndex() << ";";
                        
                        // Put it into a vector solely to ensure same print format as for the other interfaces.
                        const auto& vertex_coords_list = vertex_ptr->GetVertexCoordsList();
                        assert(vertex_coords_list.size() == 1ul);
                        assert(!(!vertex_coords_list.back()));
                        
                        std::vector<unsigned int> index { vertex_coords_list.back()->GetIndex() };
                        Utilities::PrintContainer(index, out);
                    }
                }
                
                {
                    PrintTypeOfInterface<OrientedEdge>(GetEdgeList(), out);
                    PrintTypeOfInterface<OrientedFace>(GetFaceList(), out);
                    PrintTypeOfInterface<Volume>(GetVolumeList(), out);
                }
            }
            
            
            
            namespace // anonymous
            {
                
                
                template<>
                const auto& GetInterfaceList<Vertex>(const GeometricElt& geom_elt)
                {
                    return geom_elt.GetVertexList();
                }
                
                
                template<>
                const auto& GetInterfaceList<OrientedEdge>(const GeometricElt& geom_elt)
                {
                    return geom_elt.GetOrientedEdgeList();
                }
                
                
                
                template<>
                const auto& GetInterfaceList<OrientedFace>(const GeometricElt& geom_elt)
                {
                    return geom_elt.GetOrientedFaceList();
                }
                
                
                
                //! Add to \a mesh_interface_list the interfaces of \a geom_elt which might not yet be taken into account.
                template<class OrientedInterfaceT>
                void ComputeMeshLevelList(const GeometricElt& geom_elt,
                                          typename OrientedInterfaceT::vector_shared_ptr& mesh_interface_list)
                {
                    const auto& interface_list = GetInterfaceList<OrientedInterfaceT>(geom_elt);
                    
                    for (const auto& interface_ptr : interface_list)
                    {
                        assert(!(!interface_ptr));
                        const std::size_t index = interface_ptr->GetIndex();
                        assert(index < mesh_interface_list.size());
                        
                        if (!mesh_interface_list[index])
                            mesh_interface_list[index] = interface_ptr;
                    }
                }
                
                
                
                template<>
                void ComputeMeshLevelList<Volume>(const GeometricElt& geom_elt,
                                                  Volume::vector_shared_ptr& mesh_volume_list)
                {
                    auto volume_ptr = geom_elt.GetVolumePtr();
                    
                    if (!(!volume_ptr))
                    {
                        const std::size_t index = volume_ptr->GetIndex();
                        assert(index < mesh_volume_list.size());
                        assert(mesh_volume_list[index] == nullptr && "One given volume should be filled only once!");
                        
                        mesh_volume_list[index] = volume_ptr;
                    }
                }
                                
                
                template<class OrientedInterfaceT>
                void PrintTypeOfInterface(const typename OrientedInterfaceT::vector_shared_ptr& interface_list,
                                          std::ostream& out)
                {
                    for (auto interface_ptr : interface_list)
                    {
                        assert(!(!interface_ptr));
                        const auto& interface = *interface_ptr;
                        
                        out << interface.StaticNature() << ' ' << interface.GetIndex() << ";";
                        
                        const auto& coords_list = interface.GetVertexCoordsList();
                        std::vector<unsigned int> index_list(coords_list.size());
                        
                        std::transform(coords_list.cbegin(), coords_list.cend(),
                                       index_list.begin(),
                                       [](const auto& coords_ptr)
                                       {
                                           assert(!(!coords_ptr));
                                           return coords_ptr->GetIndex();
                                       });
                        
                        std::sort(index_list.begin(), index_list.end());
                        
                        Utilities::PrintContainer(index_list, out);
                    }
                    
                }

                
            } // namespace anonymous

        
        
        } // namespace MeshNS
        
        
    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
