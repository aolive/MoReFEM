///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Mon, 13 Jun 2016 17:23:45 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_PSEUDO_NORMALS_MANAGER_HPP_
# define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_PSEUDO_NORMALS_MANAGER_HPP_


# include "Utilities/Singleton/Singleton.hpp"

# include "Core/InputParameter/Geometry/PseudoNormals.hpp"

# include "Geometry/Mesh/Mesh.hpp"
# include "Geometry/Mesh/Internal/MeshManager.hpp"
# include "Geometry/Domain/DomainManager.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        /*!
         * \brief Manager of all PseudoNormal objects.
         */
        class PseudoNormalsManager : public Utilities::Singleton<PseudoNormalsManager>
        {

        public:

            //! \copydoc doxygen_hide_alias_self
            using self = PseudoNormalsManager;

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

            //! Alias to vector of unique pointers.
            using vector_unique_ptr = std::vector<unique_ptr>;

        public:


            //! Name of the class (required for some Singleton-related errors).
            static const std::string& ClassName();

            //! Base type of PseudoNormals as input parameter.
            using input_parameter_type = InputParameter::BaseNS::PseudoNormals;

            /*!
             * \brief Create a new PseudoNormals object from the data of the input parameter file.
             *
             *
             * \param[in] section Section from the input parameter file that gives away characteristics of the
             * pseudo-normal to build.
             */
            template<class PseudoNormalsSectionT>
            void Create(const PseudoNormalsSectionT& section);


            /*!
             * \brief Compute PseudoNormals.
             *
             * \param[in] domain_index_list List of domains to consider. If empty, no restriction on domain.
             * \param[in,out] mesh Mesh for which pseudo-normals are built.
             *
             *
             * \internal <b><tt>[internal]</tt></b> This method is public because it is handy for some test executables;
             * however in full-fledged model instances you should not use this constructor at all: the other one above
             * calls it with the correct data from the input parameter file.
             */
            void Create(const std::vector<unsigned int>& domain_index_list,
                        Mesh& mesh);

        private:


            //! \name Singleton requirements.
            ///@{

            //! Constructor.
            PseudoNormalsManager();

            //! Friendship declaration to Singleton template class (to enable call to constructor).
            friend class Utilities::Singleton<PseudoNormalsManager>;

            ///@}


        };


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Mesh/Internal/PseudoNormalsManager.hxx"


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_PSEUDO_NORMALS_MANAGER_HPP_
