///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_COORDS_x_COORDS_HPP_
# define MOREFEM_x_GEOMETRY_x_COORDS_x_COORDS_HPP_


# include <cassert>
# include <string>
# include <vector>
# include <array>
# include <iosfwd>
# include <iomanip>
# include <memory>
# include <set>

# include "ThirdParty/Wrappers/Libmesh/Libmesh.hpp"
# include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

# include "Utilities/Numeric/Numeric.hpp"
# include "Utilities/Exceptions/Exception.hpp"
# include "Utilities/Containers/Vector.hpp"

# include "Core/Enum.hpp"

# include "Geometry/Coords/SpatialPoint.hpp"
# include "Geometry/Domain/MeshLabel.hpp"
# include "Geometry/Interfaces/EnumInterface.hpp"
# include "Geometry/Coords/Internal/CoordIndexes.hpp"


namespace MoReFEM
{

    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class Mesh;


    namespace Internal
    {


        namespace CoordsNS
        {


            struct Factory;


        } // namespace CoordsNS


    } // namespace Internal


    namespace Wrappers
    {

        class Mpi;


    } // namespace Wrappers



    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    /*!
     * \brief Whether the model will compute the list of domains a coord is in or not.
     *
     * This functionality is not required for all models, and is not cheap in memory, hence the possibility not to
     * do it. Of course, methods that expect these domain informations will assert if you call them after setting
     * create_domain_list_for_coords::no.
     */
    enum class create_domain_list_for_coords { yes, no };


    /*!
     * \brief Define a \a SpatialPoint in a mesh.
     *
     * \todo #887 Should probably be advanced, but currently FindCoordsOfGlobalVector requires public and casual access
     * to it. However this class was avoided in Poromechanics; I'll have to check whether the new mechanism would work
     * in CardiacMechanics where FindCoordsOfGlobalVector is applied (the priority of this task is not very high, and
     * I have to retrieve what I did in Poromechanics.
     *
     */
    class Coords final : public SpatialPoint
    {
    public:

        //! Alias to self.
        using self = Coords;

        //! Alias to parent.
        using parent = SpatialPoint;

        /*!
         * \brief Smart pointers used to store objects in \a Mesh.
         *
         * By design all \a Coords object should be stored there.
         */
        using unique_ptr = std::unique_ptr<self>;

        //! Vector of unique_ptr.
        using vector_unique_ptr = std::vector<unique_ptr>;

        /*!
         * \brief Vector of raw pointers.
         *
         * By design all \a Coords object should be stored in \a Mesh; when other objects (e.g.
         * \a Interface) needs them they should point to the underlying pointer (given by std::unique_ptr::get()).
         */
        using vector_raw_ptr = std::vector<self*>;

        //! Friendship to Mesh so that it can update position index.
        friend Mesh;

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        //! Friendship to the only class that should create \a Coords objects.
        friend Internal::CoordsNS::Factory;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

    private:

        /// \name Special member functions.
        ///@{


        /*!
         * \brief Default constructor; all coordinates are set to 0.
         *
         */
        explicit Coords();

        /*!
         * \class class_doxygen_hide_coords_from_components_params
         *
         * \param[in] x First component.
         * \param[in] y Second component.
         * \param[in] z Third component.
         * \copydoc doxygen_hide_space_unit_arg
         */

        /*!
         * \brief Constructor from components.
         *
         * \copydoc class_doxygen_hide_coords_from_components_params
         */
        explicit Coords(double x, double y, double z, const double space_unit);

        /*!
         * \class class_doxygen_hide_coords_from_array_params
         *
         * \tparam T Must be std::array<Floating-point type, 3>.
         * \param[in] value Value of the array to set.
         * \copydoc doxygen_hide_space_unit_arg
         */


        /*!
         * \brief Constructor from an array.
         *
         * \copydoc class_doxygen_hide_coords_from_array_params
         */
        template<typename T>
        explicit Coords(T&& value, const double space_unit);

        /*!
         * \class class_doxygen_hide_coords_from_stream_params
         *
         * \param[in] Ncoor Number of coordinates to be read. Expected to be at most 3.
         * \param[in,out] stream Stream from which the point is read. Coordinates are expected to be separated by tabs
         * or spaces.
         * Stream is read until failbit is met; then it is put back at the position just before that failure.
         * \copydoc doxygen_hide_space_unit_arg
         */


        /*!
         * \brief Constructor from a input stream.
         *
         * \copydoc class_doxygen_hide_coords_from_stream_params
         */
        explicit Coords(unsigned int Ncoor, std::istream& stream, const double space_unit);

    public:

        //! Destructor.
        ~Coords();

    private:

        //! Copy constructor.
        Coords(const Coords& rhs) = delete;

        //! Move constructor.
        Coords(Coords&& rhs) = delete;

        //! Lvalue assignment.
        Coords& operator=(const Coords& rhs) = delete;

        //! Rvalue assignment.
        Coords& operator=(Coords&& rhs) = delete;


        ///@}


    public:

        //! Get the index. This index depends on the format used and might not be contiguous (contrary to position_in_mesh).
        unsigned int GetIndex() const noexcept;

        //! Set the index.
        void SetIndex(unsigned int index) noexcept;

        //! Set the label.
        void SetMeshLabel(const MeshLabel::const_shared_ptr& label);

        //! Get the label.
        MeshLabel::const_shared_ptr GetMeshLabelPtr() const;

        //! Extended Print function: display the coordinates and the index.
        void ExtendedPrint(std::ostream& out) const;

        /*!
         * \brief Set the type of the Coords.
         *
         * \param[in] interface_nature Where is located the Coords on the GeometricElt.
         *
         * If the already set values is not the same as \a type (save Type::undetermined) an exception is thrown:
         * it would mean the same Coords acts as both which is forbidden in finite element problems.
         *
         */
        void SetInterfaceNature(InterfaceNS::Nature interface_nature);

        //! Get the type of the Coords.
        InterfaceNS::Nature GetInterfaceNature() const;

        /*!
         * \brief Get the position in the enclosing mesh list (should be in [0; Ncoords - 1] by design, contrary to index_).
         *
         * \tparam MpiScaleT If processor-wise, the position after mpi reduction is given. If program-wise, the one
         * before is yielded. By convention ghosted only Coords are numbered as if the first one was put immediately
         * after the last processor-wise one.
         *
         * \return Position of the \a Coords in the mesh list.
         */
        template<MpiScale MpiScaleT>
        unsigned int GetPositionInCoordsListInMesh() const noexcept;


        /*!
         * \brief Add a new \a Domain that contains the \a Coords.
         *
         * \param[in] domain_unique_id Unique id of the \a Domain to be added.
         *
         * This method makes sense only if SetCreateDomainListForCoordsToYes() was called.
         */
        void AddDomainContainingCoords(unsigned int domain_unique_id);

        /*!
         * \brief Whether the given Coords belongs to the domain or not.
         *
         * \param[in] domain_unique_id A Domain unique ID.
         *
         * This method makes sense only if SetCreateDomainListForCoordsToYes() was called.
         *
         * \return A boolean.
         */
        bool IsInDomain(unsigned int domain_unique_id) const;

        /*!
         * \brief Indicates we should keep track of the \a Domain to which the \a Coords belongs.
         *
         * \internal This public method should be called only in the base \a Model class; you shouldn't have to call it
         * directly!
         */
        static void SetCreateDomainListForCoords();

        //! Constant accessor on global variable create_domain_list_for_coords.
        static create_domain_list_for_coords GetCreateDomainListForCoords();

        # ifndef NDEBUG
        //! Returns the number of \a Coords objects in existence. For debug purposes only.
        static unsigned int Nobjects();
        # endif // NDEBUG

        /*!
         * \brief Whether the current processor is the lowest-ranked one to feature current \a Coords as a processor-wise
         * one.
         *
         * \internal Despite its public status, this should not be used as it is for low-level usage.
         *
         * \return True if no lowest rank consider current \a Coords as a processor-wise one.
         */
        bool IsLowestProcessorRank() const noexcept;

    private:

        /*!
         * \brief Set the position in the enclosing mesh list.
         *
         * \tparam MpiScaleT If processor-wise, the position after mpi reduction is set. If program-wise, the one
         * before is yielded.
         *
         * \param[in] position Position of the \a Coords in the mesh list.
         */
        template<MpiScale MpiScaleT>
        void SetPositionInCoordsListInMesh(unsigned int position) noexcept;

        //! Accessor to the object that stores the Coords indexes.
        const Internal::CoordsNS::CoordIndexes& GetCoordIndexes() const noexcept;

        //! Non constant accessor to the object that stores the Coords indexes.
        Internal::CoordsNS::CoordIndexes& GetNonCstCoordIndexes() noexcept;

        //! Non constant accessor on global variable create_domain_list_for_coords.
        static create_domain_list_for_coords& GetNonCstCreateDomainListForCoords();

        # ifndef NDEBUG
        //! Returns a reference to the number of \a Coords objects in existence. For debug purposes only.
        static unsigned int& GetNonCstNobjects();
        # endif // NDEBUG

        //! Internal mutator to the lowest processor that features this \a Coords as processor-wise.
        void SetLowestProcessor(bool value);



    private:

        /*!
         * \brief Several indexes might be relevant for a given Coords; a struct has therefore been created to group
         * them together and provide explanations about theirs differences.
         *
         */
        Internal::CoordsNS::CoordIndexes coord_indexes_;

        /*!
         * \brief Label to which the point belongs to.
         *
         * This is optional at the moment: only medit format provides this information.
         * (and it's not entirely clear for me what happens to a point that belongs to two labels).
         */
        MeshLabel::const_shared_ptr mesh_label_ = nullptr;

        //! Location of the Coords (on edge, on face...). May be InterfaceNS::Nature::undefined.
        InterfaceNS::Nature interface_nature_ = InterfaceNS::Nature::undefined;

        //! List of all \a Domain containing the current Coords.
        std::set<unsigned int> domain_list_;

        /*!
         * \brief Whether current processor is the lowest ranked one that features this \a Coords as processor-wise.
         *
         * This is a purely internal quantity, very useful to make sure for instance \a Coords aren't counted twice
         * if they are processor-wise on two different processors (might happen if for example related to both a P1 and
         * a P0 shape function in two different \a NumberingSubset).
         *
         * 'Lowest' here refer to the integer rank; so if a \a Coords is both on processor 3 and 7, both of them
         * should set this value to 3.
         *
         * \attention Due to its role, it is filled only for processor-wise \a Coords, not for ghosted ones.
         */
        bool is_lowest_processor_ = true;

    };


    /*!
     * \copydoc doxygen_hide_operator_less
     *
     * By convention the ordering is performed by increasing index, provided both are of the same nature. If not,
     * an exception is thrown.
     */
    bool operator<(const Coords& lhs, const Coords& rhs);


    /*!
     * \copydoc doxygen_hide_operator_equal
     *
     * By convention the ordering is performed by increasing index, provided both are of the same nature. If not,
     * an exception is thrown.
     */
    bool operator==(const Coords& lhs, const Coords& rhs);


    /*!
     * \brief Write the point in Ensight format
     *
     * This format is to set a width of 12 characters for each coordinate of the point; scientific notation
     * is used
     *
     * \param[in] point Point considered
     * \param[in,out] stream Stream to which the point is written
     *
     * \tparam DoPrintIndexT True if the point is preceded by its internal index
     *
     * \internal <b><tt>[internal]</tt></b> See Item 23 of "Effective C++" from Scott Meyers to see why this is a function and not a method
     */
    template<bool DoPrintIndexT>
    void WriteEnsightFormat(const Coords& point, std::ostream& stream);


    /*!
     * \brief Write the point in Medit format.
     *
     * This is achieved through calls to the libmesh API.
     *
     * \param[in] mesh_dimension Dimension of the mesh considered.
     * \param[in] point Point considered.
     * \param[in] lm_mesh_index Mesh index that has been associated when the output file was opened with Libmesh API.
     *
     *
     * \tparam FloatT float or double (depends on Medit "version" parameter).
     */
    template<class FloatT>
    void WriteMeditFormat(const unsigned int mesh_dimension, const Coords& point, int lm_mesh_index);


    /*!
     * \brief Write the point in VTK_PolygonalData format.
     *
     * This format is classic with each online corresponding to a point.
     *
     * \param[in] point Point considered
     * \param[in,out] stream Stream to which the point is written
     *
     *
     */
    void WriteVTK_PolygonalDataFormat(const Coords& point, std::ostream& stream);


    namespace Internal
    {


        namespace CoordsNS
        {


        } // namespace CoordsNS


    } // namespace Internal


} // namespace MoReFEM




namespace std
{


    //! \copydoc doxygen_hide_std_stream_out_overload
    std::ostream& operator<<(std::ostream& stream, const MoReFEM::Coords& rhs);


} // namespace std


/// @} // addtogroup GeometryGroup



# include "Geometry/Coords/Coords.hxx"


#endif // MOREFEM_x_GEOMETRY_x_COORDS_x_COORDS_HPP_
