///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#include <iostream>
#include <sstream>
#include <iomanip>

#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/Containers/Print.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Coords/Exceptions/Coords.hpp"



namespace MoReFEM
{
    
    
    Coords::~Coords()
    {
        #ifndef NDEBUG
        GetNonCstNobjects()--;
        #endif // NDEBUG
    }
    
    
    Coords::Coords()
    : parent()
    {
        #ifndef NDEBUG
        GetNonCstNobjects()++;
        #endif // NDEBUG
    }


    Coords::Coords(double x, double y, double z, const double space_unit)
    : parent(x,y, z, space_unit)
    {
        #ifndef NDEBUG
        GetNonCstNobjects()++;
        #endif // NDEBUG
    }

    
    Coords::Coords(unsigned int Ncoor, std::istream& stream, const double space_unit)
    : parent(Ncoor, stream, space_unit)
    {
        #ifndef NDEBUG
        GetNonCstNobjects()++;
        #endif // NDEBUG
    }

    
    void Coords::ExtendedPrint(std::ostream& out) const
    {
        parent::Print(out);
        out << " [" << GetIndex() << ']';
    }
    
    
    void Coords::SetInterfaceNature(InterfaceNS::Nature interface_nature)
    {
        if (interface_nature_ == InterfaceNS::Nature::undefined)
            interface_nature_ = interface_nature;
        else if (interface_nature_ != interface_nature)
            throw ExceptionNS::CoordsNS::InconsistentType(GetIndex(), __FILE__, __LINE__);
    }
    
    
    void WriteVTK_PolygonalDataFormat(const Coords& point, std::ostream& stream)
    {
        stream << point.x() << ' ';
        stream << point.y() << ' ';
        stream << point.z() << ' ';
        stream << std::endl;
    }
    
    
    bool Coords::IsInDomain(unsigned int domain_unique_id) const 
    {
        assert(GetNonCstCreateDomainListForCoords() == create_domain_list_for_coords::yes
               && "GetNonCstCreateDomainListForCoords() should be set to yes. You can do it at the creation of your model. See the TestDomainListInCoords model for example.");
        
        auto begin = domain_list_.cbegin();
        auto end = domain_list_.cend();
        
        auto it = std::find(begin, end, domain_unique_id);
        
        return it == end ? false : true;
    }
        
    
    create_domain_list_for_coords& Coords::GetNonCstCreateDomainListForCoords()
    {
        // Static is initialized only during first call.
        static create_domain_list_for_coords create_domain_list_for_coords_ = create_domain_list_for_coords::no;
        return create_domain_list_for_coords_;
    }
    
    
    create_domain_list_for_coords Coords::GetCreateDomainListForCoords()
    {
        return GetNonCstCreateDomainListForCoords();        
    }
    
    
    void Coords::SetCreateDomainListForCoords()
    {
        GetNonCstCreateDomainListForCoords() = create_domain_list_for_coords::yes;
    }
    
    
    #ifndef NDEBUG
    unsigned int& Coords::GetNonCstNobjects()
    {
        static unsigned int ret = 0u;
        return ret;
    }
    
    
    unsigned int Coords::Nobjects()
    {
        return GetNonCstNobjects();
    }
    
    
    #endif // NDEBUG
    
    
    void Coords::SetLowestProcessor(bool value)
    {
        is_lowest_processor_ = value;
    }
    

    bool Coords::IsLowestProcessorRank() const noexcept
    {
        return is_lowest_processor_;
    }
    
    
} // namespace MoReFEM


/// @} // addtogroup GeometryGroup



namespace std
{


    std::ostream& operator<<(std::ostream& stream, const MoReFEM::Coords& point)
    {
        point.Print(stream);
        return stream;
    }


}



