///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Aug 2017 18:24:57 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#include <iostream>
#include <sstream>
#include <iomanip>

#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/Containers/Print.hpp"
#include "Geometry/Coords/SpatialPoint.hpp"
#include "Geometry/Coords/Exceptions/Coords.hpp"



namespace MoReFEM
{
    
    
    SpatialPoint::~SpatialPoint() = default;
    
    
    SpatialPoint::SpatialPoint()
    {
        coordinate_list_.fill(0.);
    }


    SpatialPoint::SpatialPoint(double x, double y, double z, const double space_unit)
    : coordinate_list_({{x * space_unit, y * space_unit, z * space_unit}})
    { }

    
    SpatialPoint::SpatialPoint(unsigned int Ncoor, std::istream& stream, const double space_unit)
    {
        assert(Ncoor <= 3u);
        
        std::array<double, 3ul> buf_coordinates;
        buf_coordinates.fill(0.);

        auto initialPosition = stream.tellg();
        
        for (unsigned int i = 0u; i < Ncoor; ++i)
        {
            stream >> buf_coordinates[i];
            buf_coordinates[i] *= space_unit;
        }        
        
        if (stream)
        {
            // Modify point only if failbit not set.
            coordinate_list_ = buf_coordinates;            
        }
        else
        {
            // In case of failure, put back the cursor at the point it was before trying unsuccessfully to read a point
            auto state = stream.rdstate();
            stream.clear();
            stream.seekg(initialPosition);
            stream.setstate(state);
        }
    }


    void SpatialPoint::Print(std::ostream& out) const
    {
        Utilities::PrintContainer(coordinate_list_, out, ", ", '(',  ')');
    }
    
    
    
    double Distance(const SpatialPoint& point1, const SpatialPoint& point2)
    {
        double sum = 0.;
        
        for (auto i = 0u; i < 3u; ++i)
            sum += NumericNS::Square(point1[i] - point2[i]);
        
        return std::sqrt(sum);
    }
    
    
    void SpatialPoint::Reset()
    {
        coordinate_list_.fill(0.);
    }
    
    
} // namespace MoReFEM


/// @} // addtogroup GeometryGroup



namespace std
{


    std::ostream& operator<<(std::ostream& stream, const MoReFEM::SpatialPoint& point)
    {
        point.Print(stream);
        return stream;
    }


}



