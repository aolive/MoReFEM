///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 26 Mar 2014 12:43:55 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#include <cassert>

#include "Utilities/Numeric/Numeric.hpp"
#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/Containers/EnumClass.hpp"

#include "Geometry/Interfaces/Interface.hpp"


namespace MoReFEM
{
    
    
    namespace // anonymous
    {
    
        
        std::vector<unsigned int> ComputeVertexCoordsIndexList(const Coords::vector_raw_ptr& vertex_coords_list);
        
        
    } // namespace anonymous
    
    
    Interface::Interface(const Coords* vertex_coords)
    : vertex_coords_list_({const_cast<Coords*>(vertex_coords)}),
    id_(NumericNS::UninitializedIndex<unsigned int>())
    {
        assert(!(!vertex_coords));
    }
    
    
    Interface::Interface()
    : id_(NumericNS::UninitializedIndex<unsigned int>())
    { }

    
    Interface::~Interface() = default;
    
    
    void Interface::SetVertexCoordsList(const Coords::vector_raw_ptr& value)
    {
        vertex_coords_list_ = value;
        
        auto begin = vertex_coords_list_.begin();
        auto end = vertex_coords_list_.end();
        
        using difference_type = Coords::vector_raw_ptr::difference_type;
        
        const difference_type Ncoord_on_interface = static_cast<difference_type>(vertex_coords_list_.size());
        assert(Ncoord_on_interface > 0l);
        
        // \todo #205 Not entirely true above: in P2 geometry there could be more than edges...
        
        // If only one element we're already done.
        if (Ncoord_on_interface > 1)
        {
            // First we determine the minimum element.
            auto it_minimum = std::min_element(begin, end,
                                               Utilities::PointerComparison::Less<Coords*>());
            
            // If only two elements, simply put the minimum in first position.
            if (Ncoord_on_interface == 2)
            {
                std::rotate(begin, it_minimum, end);
            }
            
            // If more, we look at its two closest neighbours (with a modulo if at the end of the list) because
            // we also want the second element to be lower than the last.
            // The reason is the following: consider the quadrangle 3 1 2 0.
            // Our rotate would yield: 0 3 1 2.
            // However, if later we meet 0 2 1 3, it is also the same quadrangle, with a different orientation...
            // but orientation is dealt with elsewhere; here we do not want it. Hence the following step
            // to ensure a unique sequence to identify the geometric element.
            else
            {
                auto it_after_minimum(it_minimum);
                ++it_after_minimum;
                
                if (it_after_minimum == end)
                    it_after_minimum = begin;
                
                auto it_before_minimum = it_minimum == begin ? begin + Ncoord_on_interface - 1 : it_minimum - 1;
                
                if (Utilities::PointerComparison::Less<Coords*>()(*it_after_minimum, *it_before_minimum))
                {
                    std::rotate(begin, it_minimum, end);
                }
                else
                {
                    std::reverse(begin, end);
                    
                    // begin and end iterators are still valid, but it_minimum is not: its position
                    // has changed. We could avoid the min_element entirely but as we work on tiny vectors
                    // here the gain would be trifling.
                    it_minimum = std::min_element(begin, end,
                                                  Utilities::PointerComparison::Less<Coords*>());
                    std::rotate(begin, it_minimum, end);
                    
                }
            }
        }
    }
    

    const std::vector<unsigned int>& Interface
    ::GetVertexCoordsIndexList() const
    {
        if (vertex_coords_index_list_.empty()) // means it is not yet initialized.
            vertex_coords_index_list_ = ComputeVertexCoordsIndexList(GetVertexCoordsList());
        
        assert(vertex_coords_index_list_ == ComputeVertexCoordsIndexList(GetVertexCoordsList())
               && "Check the attribute is on par with the underlying list of \a Coords.");
        
        return vertex_coords_index_list_;
    }
    
    
    namespace InterfaceNS
    {
        
        
        std::size_t Hash::operator()(const Interface* const interface_ptr) const
        {
            assert(!(!interface_ptr));
            const auto& interface = *interface_ptr;
            
            std::size_t ret = std::hash<unsigned int>()(EnumUnderlyingType(interface.GetNature()));
            
            Utilities::HashCombine(ret, interface.GetIndex());
            
            return ret;
        }
        
        
        bool LessByCoords::operator()(const Interface::shared_ptr& lhs,
                                      const Interface::shared_ptr& rhs) const
        {
            assert(!(!lhs));
            assert(!(!rhs));
            assert(lhs->GetNature() == rhs->GetNature());
            
            const auto& lhs_coord = lhs->GetVertexCoordsList();
            const auto& rhs_coord = rhs->GetVertexCoordsList();
            const auto lhs_size = lhs_coord.size();
            const auto rhs_size = rhs_coord.size();
            
            if (lhs_size != rhs_size)
                return lhs_size < rhs_size;
            
            for (unsigned int i = 0u; i < lhs_size; ++i)
            {
                auto lhs_item = lhs_coord[i];
                auto rhs_item = rhs_coord[i];
                
                if (lhs_item != rhs_item)
                    return *lhs_item < *rhs_item;
            }
            
            // If there they are strictly identical, natural answer is false.
            return false;

        }
        
        
        
    } // namespace InterfaceNS
    
    
    
    namespace // anonymous
    {
        
        
        std::vector<unsigned int> ComputeVertexCoordsIndexList(const Coords::vector_raw_ptr& vertex_coords_list)
        {
            std::vector<unsigned int> ret;
            
            ret.reserve(vertex_coords_list.size());
            
            for (const auto& coords_ptr : vertex_coords_list)
            {
                assert(!(!coords_ptr));
                ret.push_back(coords_ptr->GetIndex());
            }
            
            return ret;
        }
        
        
    } // namespace anonymous

    
} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
