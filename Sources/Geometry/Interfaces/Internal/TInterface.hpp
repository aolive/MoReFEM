///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 14 Jun 2013 12:09:17 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_T_INTERFACE_HPP_
# define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_T_INTERFACE_HPP_


# include <vector>
# include <algorithm>
# include <sstream>
# include <memory>
# include <cassert>

# include "Utilities/Containers/Print.hpp"
# include "Utilities/Containers/PointerComparison.hpp"
# include "Geometry/Coords/Coords.hpp"
# include "Geometry/Interfaces/Interface.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace InterfaceNS
        {


            /*!
             * \brief CRTP from which the various interfaces (vertex, edge, face or volume) should inherit.
             *
             * \tparam DerivedT CRTP term.
             * \tparam NatureT Whether we are considering a vertex, an edge, a face or a volume.
             */
            template<class DerivedT, ::MoReFEM::InterfaceNS::Nature NatureT>
            class TInterface : public Interface
            {
            public:


                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor for vertex.
                 *
                 * \param[in] vertex_coords Coords object associated to the Vertex interface.
                 */
                explicit TInterface(const Coords* vertex_coords);

                /*!
                 * \brief Constructor for edges and faces.
                 *
                 * \tparam LocalContentT Type of the container or structure that represents the interface in the
                 * local topology. It is given by TopologyT::EdgeContent for the edges and TopologyT::FaceContent
                 * for the faces; where TopologyT is the topology of the GeometricElt for which the Interface is built.
                 *
                 * \param[in] local_content Integer values that represent the interface within the local topology.
                 * Is is one of the element of TopologyT::GetEdgeList() or TopologyT::GetFaceList() (see above for the
                 * meaning of TopologyT).
                 * \param[in] coords_list_in_geom_elt List of Coords objects within the GeometricElt for which the interface
                 * is built.
                 */
                template<class LocalContentT>
                explicit TInterface(const LocalContentT& local_content,
                                    const Coords::vector_raw_ptr& coords_list_in_geom_elt);

                /*!
                 * \brief Empty constructor (for volumes).
                 *
                 * Volume are apart: when they are built the index is already known as we know for sure each Volume is unique
                 * for each GeometricElt. Due to this close relationship, no Coords are stored within a Volume, as it would be
                 * a waste of space given the Coords can be retrieved through the GeometricElt object that shares the same
                 * index. Volume objects include weak pointers to its related GeometricElt so that Coords could
                 * be retrieved without duplication of data.
                 */
                explicit TInterface() = default;

                //! Destructor.
                ~TInterface() override = default;

                //! Do not allow recopy.
                TInterface(const TInterface&) = delete;

                //! Do not allow move.
                TInterface(TInterface&&) = delete;

                //! Do not allow affectation.
                TInterface& operator=(const TInterface&) = delete;

                //! Do not allow affectation.
                TInterface& operator=(TInterface&&) = delete;
                ///@}

                /*!
                 * \brief Print the underlying coords list.
                 *
                 * \param[in,out] stream Stream to which the edge_or_face is written.
                 */
                void Print(std::ostream& stream) const;

                //! Nature of the Interface.
                virtual ::MoReFEM::InterfaceNS::Nature GetNature() const noexcept override final;

                //! Nature of the Interface as a static method.
                constexpr static enum ::MoReFEM::InterfaceNS::Nature StaticNature();
            };


        } // namespace InterfaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup



# include "Geometry/Interfaces/Internal/TInterface.hxx"


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_T_INTERFACE_HPP_
