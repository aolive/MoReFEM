///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 10 Oct 2014 11:45:26 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_BUILD_INTERFACE_LIST_HELPER_HXX_
# define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_BUILD_INTERFACE_LIST_HELPER_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace InterfaceNS
        {



            template<class TopologyT>
            OrientedEdge::vector_shared_ptr BuildEdgeListHelper<TopologyT, false>
            ::Perform(const GeometricElt* geom_elt_ptr,
                      const Coords::vector_raw_ptr& coords_list,
                      Edge::InterfaceMap& existing_list)
            {
                static_cast<void>(coords_list);
                static_cast<void>(existing_list);
                static_cast<void>(geom_elt_ptr);
                OrientedEdge::vector_shared_ptr ret;
                return ret;
            }


            template<class TopologyT>
            OrientedEdge::vector_shared_ptr BuildEdgeListHelper<TopologyT, true>
            ::Perform(const GeometricElt* geom_elt_ptr,
                      const Coords::vector_raw_ptr& coords_list_in_geom_elt,
                      Edge::InterfaceMap& existing_list)
            {
                // Create new edges or retrieve them if they already exist. Do not consider orientation at all there.
                auto&& edge_without_orientation_list =
                    Internal::InterfaceNS::Build<Edge, TopologyT>::Perform(geom_elt_ptr,
                                                                          coords_list_in_geom_elt,
                                                                          existing_list);

                // Now add the orientation information before storing it into the GeometricElt.
                const std::size_t Nedge = edge_without_orientation_list.size();

                OrientedEdge::vector_shared_ptr oriented_edge_list(Nedge);

                for (std::size_t i = 0ul; i < Nedge; ++i)
                {
                    auto& edge_without_orientation_ptr = edge_without_orientation_list[i];

                    oriented_edge_list[i] = std::make_shared<OrientedEdge>(edge_without_orientation_ptr,
                                                                           coords_list_in_geom_elt,
                                                                           i,
                                                                           Utilities::Type2Type<TopologyT>());
                }


                return oriented_edge_list;
            }



            template<class TopologyT>
            OrientedFace::vector_shared_ptr BuildFaceListHelper<TopologyT, false>
            ::Perform(const GeometricElt* geom_elt_ptr,
                      const Coords::vector_raw_ptr& coords_list,
                      Face::InterfaceMap& existing_list)
            {
                static_cast<void>(coords_list);
                static_cast<void>(existing_list);
                static_cast<void>(geom_elt_ptr);
                OrientedFace::vector_shared_ptr ret;
                return ret;
            }


            template<class TopologyT>
            OrientedFace::vector_shared_ptr BuildFaceListHelper<TopologyT, true>
            ::Perform(const GeometricElt* geom_elt_ptr,
                      const Coords::vector_raw_ptr& coords_list_in_geom_elt,
                      Face::InterfaceMap& existing_list)
            {
                // Create new faces or retrieve them if they already exist. Do not consider orientation at all there.
                auto&& face_without_orientation_list =
                    Internal::InterfaceNS::Build<Face, TopologyT>::Perform(geom_elt_ptr,
                                                                          coords_list_in_geom_elt,
                                                                          existing_list);


                // Now add the orientation information before storing it into the GeometricElt.
                const std::size_t Nface = face_without_orientation_list.size();

                OrientedFace::vector_shared_ptr oriented_face_list(Nface);

                for (std::size_t i = 0ul; i < Nface; ++i)
                {
                    auto face_without_orientation_ptr = face_without_orientation_list[i];

                    oriented_face_list[i] = std::make_shared<OrientedFace>(face_without_orientation_ptr,
                                                                           coords_list_in_geom_elt,
                                                                           i,
                                                                           Utilities::Type2Type<TopologyT>());
                }

                return oriented_face_list;
            }




        } // namespace InterfaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_BUILD_INTERFACE_LIST_HELPER_HXX_
