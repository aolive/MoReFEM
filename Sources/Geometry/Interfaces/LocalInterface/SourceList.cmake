target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/LocalInterface.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/IsOnLocalInterface.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/IsOnLocalInterface.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/LocalInterface.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/LocalInterface.hxx"
)

