///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 29 Sep 2014 16:11:54 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_ORIENTED_FACE_HPP_
# define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_ORIENTED_FACE_HPP_

# include <memory>
# include <vector>

# include "Utilities/Miscellaneous.hpp"

# include "Geometry/Interfaces/Instances/Face.hpp"
# include "Geometry/Interfaces/Internal/Orientation.hpp"


namespace MoReFEM
{


    // Forward declaration.
    class GeometricElt;


    /*!
     * \brief Class used to store an face inside a GeometricElement.
     *
     * An face may be shared among several GeometricElement, but their orientation might differ inside each
     * of the GeometricElement. The purpose of the current object is therefore to store two informations:
     * - A shared pointer to an Face.
     * - An integer that gives the orientation.
     */

    class OrientedFace final : public Crtp::Orientation<OrientedFace, Face>
    {

    public:

        //! Alias to shared pointer.
        using shared_ptr = std::shared_ptr<OrientedFace>;

        //! Alias to vector of shared_pointer.
        using vector_shared_ptr = std::vector<shared_ptr>;

    public:

        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] face Face before any orientation is applied.
         * \param[in] local_face_index Index of the face in the local (topology) element; between 0 and Nface - 1.
         * \param[in] coords_list_in_geom_elt List of \a Coords relared to the considered \a GeometricElt.
         * \param[in] topology_token A token there for a C++ trick: OrientedFace constructor requires a template
         * parameter for the topology, but I do not want OrientedFace to be a template class. So the template
         * type must be inferred from the arguments; that's the role of \a topology_token. The user must provide:
         * \code
         * Utilities::TypeToType<TopologyT>()
         * \endcode
         * as argument. To put in a nutshell, Type2Type here allows to use a very shallow object with almost no
         * construction cost.
         */
        template<class Type2TypeTopologyT>
        explicit OrientedFace(const Face::shared_ptr& face,
                              const Coords::vector_raw_ptr& coords_list_in_geom_elt,
                              unsigned int local_face_index,
                              Type2TypeTopologyT topology_token);


        //! Destructor.
        ~OrientedFace() = default;

        //! Copy constructor.
        OrientedFace(const OrientedFace&) = delete;

        //! Move constructor.
        OrientedFace(OrientedFace&&) = delete;

        //! Copy affectation.
        OrientedFace& operator=(const OrientedFace&) = delete;

        //! Move affectation.
        OrientedFace& operator=(OrientedFace&&) = delete;


        ///@}


    };



} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Interfaces/Instances/OrientedFace.hxx"


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_ORIENTED_FACE_HPP_
