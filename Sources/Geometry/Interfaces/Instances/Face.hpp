///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 7 Jun 2013 14:18:11 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_FACE_HPP_
# define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_FACE_HPP_

# include <vector>
# include <memory>

# include "Utilities/Containers/PointerComparison.hpp"

# include "Geometry/Interfaces/Interface.hpp"
# include "Geometry/Interfaces/Internal/TInterface.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GeometricElt;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    /*!
     * \brief Class in charge of the face interface.
     *
     */
    class Face final : public Internal::InterfaceNS::TInterface<Face, InterfaceNS::Nature::face>
    {

    public:

        //! Alias over shared_ptr.
        using shared_ptr = std::shared_ptr<Face>;

        //! Alias over vector of shared_ptr.
        using vector_shared_ptr = std::vector<shared_ptr>;


        /*!
         * \brief Typedef useful when the vertices are built.
         *
         * When a new Vertex is built, index is not yet associated to it; one must determine first whether
         * it has already been built by another GeometricElt. The type below is used to list all those
         * already built.
         */
        using InterfaceMap = Utilities::PointerComparison::Map<Face::shared_ptr, std::vector<const GeometricElt*>, InterfaceNS::LessByCoords>;

        //! Friendship.
        friend Mesh;

    public:


        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \tparam LocalFaceContentT Type of the container or structure that represents the interface in the
         * local topology. It is given by TopologyT::FaceContent where TopologyT is the topology of the GeometricElt
         * for which the Interface is built.
         *
         * \param[in] local_face_content Integer values that represent the interface within the local topology.
         * Is is one of the element of TopologyT::GetFaceList() (see above for the
         * meaning of TopologyT).
         * \param[in] coords_list_in_geom_elt List of Coords objects within the GeometricElt for which the interface
         * is built.
         */
        template<class LocalFaceContentT>
        explicit Face(const LocalFaceContentT& local_face_content,
                      const Coords::vector_raw_ptr& coords_list_in_geom_elt);

        //! Destructor.
        ~Face() override;

        //! Do not allow recopy.
        Face(const Face&) = delete;

        //! Do not allow move.
        Face(Face&&) = delete;

        //! Do not allow copy affectation.
        Face& operator=(const Face&) = delete;

        //! Do not allow move affectation.
        Face& operator=(Face&&) = delete;


        ///@}

    private:

        /*!
         * \brief Compute the pseudo-normal of a face.
         *
         * \param[in] geom_elt_list List of the geometric elements that share this face.
         *
         * \warning Should not be called directly. To compute pseudo-normals use PseudoNormals1 in Lua that will use
         * the dedicated magager to compute them.
         *
         */
        void ComputePseudoNormal(const std::vector<const GeometricElt*>& geom_elt_list);

    };


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Interfaces/Instances/Face.hxx"


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_FACE_HPP_
