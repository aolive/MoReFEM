///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 7 Jun 2013 14:18:11 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_VERTEX_HPP_
# define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_VERTEX_HPP_

# include <vector>
# include <memory>

# include "Utilities/Containers/PointerComparison.hpp"

# include "Geometry/Interfaces/Interface.hpp"
# include "Geometry/Interfaces/Internal/TInterface.hpp"


namespace MoReFEM
{

    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GeometricElt;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    /*!
     * \brief Class in charge of the Vertex interface.
     *
     */
    class Vertex final : public Internal::InterfaceNS::TInterface<Vertex, InterfaceNS::Nature::vertex>
    {

    public:

        //! Alias over shared_ptr.
        using shared_ptr = std::shared_ptr<Vertex>;

        //! Alias over vector of shared_ptr.
        using vector_shared_ptr = std::vector<shared_ptr>;


        /*!
         * \brief Alias useful when the vertices are built.
         *
         * When a new Vertex is built, index is not yet associated to it; one must determine first whether
         * it has already been built by another GeometricElt. The type below is used to list all those
         * already built.
         */
        using InterfaceMap = Utilities::PointerComparison::Map<Vertex::shared_ptr, std::vector<const GeometricElt*>, InterfaceNS::LessByCoords>;

        //! Friendship.
        friend Mesh;

    public:


        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] vertex_coords Coords object associated to the Vertex interface.
         */
        explicit Vertex(const Coords* vertex_coords);

        //! Destructor.
        ~Vertex() override;

        //! Do not allow recopy.
        Vertex(const Vertex&) = delete;

        //! Do not allow move.
        Vertex(Vertex&&) = delete;

        //! Do not allow copy affectation.
        Vertex& operator=(const Vertex&) = delete;

        //! Do not allow move affectation.
        Vertex& operator=(Vertex&&) = delete;


        ///@}

    private:

        /*!
         * \brief Compute the pseudo-normal of a vertex.
         *
         * \param[in] geom_elt_list List of the geometric elements that share this vertex.
         *
         * \warning Should not be called directly. To compute pseudo-normals use PseudoNormals1 in Lua that will use
         * the dedicated magager to compute them.
         *
         */
        void ComputePseudoNormal(const std::vector<const GeometricElt*>& geom_elt_list);

    private:

        /*!
         * \brief Number of vertices.
         *
         * \internal <b><tt>[internal]</tt></b> This is a purely internal attribute used in vertices creation; should not be used outside of
         * Vertex.cpp!
         */
        static unsigned int Nvertice_;

    };


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Interfaces/Instances/Vertex.hxx"


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_VERTEX_HPP_
