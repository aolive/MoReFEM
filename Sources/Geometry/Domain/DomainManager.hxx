///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Mar 2015 16:39:22 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_DOMAIN_x_DOMAIN_MANAGER_HXX_
# define MOREFEM_x_GEOMETRY_x_DOMAIN_x_DOMAIN_MANAGER_HXX_


namespace MoReFEM
{


    template<class DomainSectionT>
    void DomainManager::Create(const DomainSectionT& section)
    {
        namespace ipl = Internal::InputParameterListNS;

        decltype(auto) mesh_index_list = ipl::ExtractParameter<typename DomainSectionT::MeshIndexList>(section);
        decltype(auto) dimension_list = ipl::ExtractParameter<typename DomainSectionT::DimensionList>(section);
        decltype(auto) mesh_label_list = ipl::ExtractParameter<typename DomainSectionT::MeshLabelList>(section);
        decltype(auto) geometric_type_list = ipl::ExtractParameter<typename DomainSectionT::GeomEltTypeList>(section);

        Create(section.GetUniqueId(), mesh_index_list, dimension_list, mesh_label_list, geometric_type_list);
    }


    inline Domain& DomainManager::GetNonCstDomain(unsigned int unique_id, const char* invoking_file, int invoking_line)
    {
        return const_cast<Domain&>(GetDomain(unique_id, invoking_file, invoking_line));
    }


    inline void DomainManager::CreateLightweightDomain(unsigned int unique_id,
                                                       unsigned int mesh_index,
                                                       const std::vector<unsigned int>& mesh_label_list)
    {
        Create(unique_id,
               { mesh_index },
               { },
               mesh_label_list,
               { }
               );
    }


    inline const DomainManager::storage_type& DomainManager::GetDomainList() const noexcept
    {
        return list_;
    }




} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_DOMAIN_x_DOMAIN_MANAGER_HXX_
