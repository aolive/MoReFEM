///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 16 Feb 2016 10:28:36 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FormulationSolverGroup
/// \addtogroup FormulationSolverGroup
/// \{

#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INSTANCES_x_THREE_DIMENSIONAL_INITIAL_CONDITION_HXX_
# define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INSTANCES_x_THREE_DIMENSIONAL_INITIAL_CONDITION_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace FormulationSolverNS
        {



            inline ThreeDimensionalInitialCondition::scalar_initial_condition& ThreeDimensionalInitialCondition
            ::GetScalarInitialConditionX() const noexcept
            {
                assert(!(!scalar_initial_condition_x_));
                return *scalar_initial_condition_x_;
            }


            inline ThreeDimensionalInitialCondition::scalar_initial_condition& ThreeDimensionalInitialCondition
            ::GetScalarInitialConditionY() const noexcept
            {
                assert(!(!scalar_initial_condition_y_));
                return *scalar_initial_condition_y_;
            }


            inline ThreeDimensionalInitialCondition::scalar_initial_condition& ThreeDimensionalInitialCondition
            ::GetScalarInitialConditionZ() const noexcept
            {
                assert(!(!scalar_initial_condition_z_));
                return *scalar_initial_condition_z_;
            }


            inline bool ThreeDimensionalInitialCondition::IsConstant() const
            {
                return GetScalarInitialConditionX().IsConstant()
                && GetScalarInitialConditionY().IsConstant()
                && GetScalarInitialConditionZ().IsConstant();
            }


            inline ThreeDimensionalInitialCondition::return_type ThreeDimensionalInitialCondition::SupplGetConstantValue() const
            {
                return content_;
            }


            inline const LocalVector& ThreeDimensionalInitialCondition::SupplGetValue(const SpatialPoint& coords) const
            {
                content_(0) = GetScalarInitialConditionX().GetValue(coords);
                content_(1) = GetScalarInitialConditionY().GetValue(coords);
                content_(2) = GetScalarInitialConditionZ().GetValue(coords);

                return content_;
            }



        } // namespace FormulationSolverNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INSTANCES_x_THREE_DIMENSIONAL_INITIAL_CONDITION_HXX_
