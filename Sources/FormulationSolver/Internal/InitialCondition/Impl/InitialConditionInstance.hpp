///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 16 Feb 2016 10:28:36 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FormulationSolverGroup
/// \addtogroup FormulationSolverGroup
/// \{

#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_IMPL_x_INITIAL_CONDITION_INSTANCE_HPP_
# define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_IMPL_x_INITIAL_CONDITION_INSTANCE_HPP_

# include <memory>
# include <vector>

# include "Parameters/ParameterType.hpp"

# include "FormulationSolver/Internal/InitialCondition/InitialCondition.hpp"
# include "FormulationSolver/Internal/InitialCondition/Impl/InitialConditionInstance.hpp"
# include "FormulationSolver/Internal/InitialCondition/Policy/Constant.hpp"
# include "FormulationSolver/Internal/InitialCondition/Policy/LuaFunction.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FormulationSolverNS
        {


            namespace Impl
            {


                /*!
                 * \brief Template class that provides actual instantiation of a parameter.
                 *
                 * \tparam TypeT  Type of the parameter (real, vector, matrix).
                 * \tparam NaturePolicyT Policy that determines how to handle the parameter. Policies are enclosed in
                 * ParameterNS::Policy namespace. Policies might be for instance Constant (same value everywhere), LuaFunction
                 * (value is provided by a function defined in the input parameter file; additional arguments are chosen here
                 * with the variadic template argument \a Args.).
                 *
                 */
                template
                <
                    ParameterNS::Type TypeT,
                    template<ParameterNS::Type, typename... Args> class NaturePolicyT,
                    typename... Args
                >
                class InitialConditionInstance final : public InitialCondition<TypeT>,
                public NaturePolicyT<TypeT, Args...>
                {

                public:

                    //! \copydoc doxygen_hide_alias_self
                    using self = InitialConditionInstance<TypeT, NaturePolicyT, Args...>;

                    //! Alias to base class.
                    using parent = InitialCondition<TypeT>;

                    //! Alias to return type.
                    using return_type = typename parent::return_type;

                    //! Alias to traits of parent class.
                    using traits = typename parent::traits;

                    //! Alias to nature policy (constant, per quadrature point, Lua function, etc...).
                    using nature_policy = NaturePolicyT<TypeT, Args...>;

                    //! Alias to unique_ptr.
                    using unique_ptr = std::unique_ptr<self>;


                public:

                    /// \name Special members.
                    ///@{

                    //! Constructor.
                    template<typename... ConstructorArgs>
                    explicit InitialConditionInstance(const Mesh& mesh,
                                                      ConstructorArgs&&... arguments);

                    //! Destructor.
                    ~InitialConditionInstance() override = default;

                    //! Copy constructor.
                    InitialConditionInstance(const InitialConditionInstance&) = delete;

                    //! Move constructor.
                    InitialConditionInstance(InitialConditionInstance&&) = delete;

                    //! Copy affectation.
                    InitialConditionInstance& operator=(const InitialConditionInstance&) = delete;

                    //! Move affectation.
                    InitialConditionInstance& operator=(InitialConditionInstance&&) = delete;

                    ///@}

                    //! Get the value of the initial condition at \a coords.
                    return_type SupplGetValue(const SpatialPoint& coords) const override;

                    //! Get the value of the initial condition at \a coords.
                    return_type SupplGetConstantValue() const override;

                    /*
                     * \brief Whether the parameter varies spatially or not.
                     */
                    bool IsConstant() const override;
                };



            } // namespace Impl


        } // namespace FormulationSolverNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


# include "FormulationSolver/Internal/InitialCondition/Impl/InitialConditionInstance.hxx"


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_IMPL_x_INITIAL_CONDITION_INSTANCE_HPP_
