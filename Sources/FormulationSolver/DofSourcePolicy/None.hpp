///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 23 Sep 2015 13:35:14 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FormulationSolverGroup
/// \addtogroup FormulationSolverGroup
/// \{

#ifndef MOREFEM_x_FORMULATION_SOLVER_x_DOF_SOURCE_POLICY_x_NONE_HPP_
# define MOREFEM_x_FORMULATION_SOLVER_x_DOF_SOURCE_POLICY_x_NONE_HPP_

# include <memory>



namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GlobalVector;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace VariationalFormulationNS
    {


        namespace DofSourcePolicyNS
        {


            /*!
             * \brief Default policy of hyperelastic model, when there are no source given at the dofs.
             *
             * For instance in hyperelastic model sources are given by an linear operator which in fact encapsulates
             * a Parameter, which values are expressed at the quadrature points rather than at the dofs.
             *
             */
            class None
            {

            public:

                //! \copydoc doxygen_hide_alias_self
                using self = None;

            public:

                /// \name Special members.
                ///@{

                //! Constructor.
                explicit None() = default;

            protected:

                //! Destructor.
                ~None() = default;

                //! Copy constructor.
                None(const None&) = delete;

                //! Move constructor.
                None(None&&) = delete;

                //! Copy affectation.
                None& operator=(const None&) = delete;

                //! Move affectation.
                None& operator=(None&&) = delete;

                ///@}

                //! Actually do notthing here!
                static void AddToRhs(GlobalVector& rhs);


            private:



            };


        } // namespace DofSourcePolicyNS


    } // namespace VariationalFormulationNS


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


# include "FormulationSolver/DofSourcePolicy/None.hxx"


#endif // MOREFEM_x_FORMULATION_SOLVER_x_DOF_SOURCE_POLICY_x_NONE_HPP_
