///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 22 Jun 2016 16:37:31 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FormulationSolverGroup
/// \addtogroup FormulationSolverGroup
/// \{

#ifndef MOREFEM_x_FORMULATION_SOLVER_x_CRTP_x_PENALIZATION_HPP_
# define MOREFEM_x_FORMULATION_SOLVER_x_CRTP_x_PENALIZATION_HPP_

# include <memory>
# include <vector>


namespace MoReFEM
{


    namespace FormulationSolverNS
    {


        /*!
         * \brief Crtp to add in a VariationalFormulation an penalization_ data attribute and its accessors.
         *
         * \tparam DerivedT Class upon which the CRTP is applied.
         * \tparam PenalizationLawT Type of the penalization (e.g. PenalizationNS::LogI3Penalization).
         *
         * \attention Create() must be explicitly called before the law is used.
         */
        template
        <
            class DerivedT,
            class PenalizationLawT
        >
        class Penalization
        {

        public:

            //! \copydoc doxygen_hide_alias_self
            using self = Penalization<DerivedT, PenalizationLawT>;

            //! Alias to penalization type.
            using penalization_law_type = PenalizationLawT;


        protected:

            /// \name Special members.
            ///@{

            //! Constructor.

            explicit Penalization() = default;

            //! Destructor.
            ~Penalization() = default;

            //! Copy constructor.
            Penalization(const Penalization&) = delete;

            //! Move constructor.
            Penalization(Penalization&&) = delete;

            //! Copy affectation.
            Penalization& operator=(const Penalization&) = delete;

            //! Move affectation.
            Penalization& operator=(Penalization&&) = delete;

            ///@}

            /*!
             * \brief Instantiate the penalization.
             *
             * \param[in] args penalization_law_type's constructor arguments.
             */
            template<typename... Args>
            void Create(Args&&... args);

            //! Get the underlying penalization.
            const penalization_law_type& GetPenalizationLaw() const noexcept;

            //! Get the underlying penalization as a raw pointer.
            const penalization_law_type* GetPenalizationLawPtr() const noexcept;


        private:

            //! Penalization used in stiffness operator.
            typename penalization_law_type::const_unique_ptr penalization_ = nullptr;


        };


    } // namespace FormulationSolverNS


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


# include "FormulationSolver/Crtp/Penalization.hxx"


#endif // MOREFEM_x_FORMULATION_SOLVER_x_CRTP_x_PENALIZATION_HPP_
