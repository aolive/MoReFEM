target_sources(${MOREFEM_FORMULATION_SOLVER}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/HyperelasticLaw.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/HyperelasticLaw.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Penalization.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Penalization.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/VolumicAndSurfacicSource.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/VolumicAndSurfacicSource.hxx"
)

