///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Oct 2016 14:26:05 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FormulationSolverGroup
/// \addtogroup FormulationSolverGroup
/// \{

#ifndef MOREFEM_x_FORMULATION_SOLVER_x_SNES_x_SNES_INTERFACE_HXX_
# define MOREFEM_x_FORMULATION_SOLVER_x_SNES_x_SNES_INTERFACE_HXX_


namespace MoReFEM
{


    namespace SolverNS
    {


        template<class VariationalFormulationT>
        PetscErrorCode SnesInterface<VariationalFormulationT>
        ::Function(SNES snes, Vec a_evaluation_state, Vec a_residual, void* context_as_void)
        {
            static_cast<void>(snes);
            static_cast<void>(a_residual);

            // \a context_as_void is in fact the VariationalFormulation object.
            assert(context_as_void);
            VariationalFormulationT* formulation_ptr = static_cast<VariationalFormulationT*>(context_as_void);

            auto& formulation = *formulation_ptr;

            #ifndef NDEBUG
            {
                std::string desc;

                namespace Petsc = Wrappers::Petsc;

                const auto& numbering_subset = formulation.GetNumberingSubset();

                const bool is_same = Petsc::AreEqual(Petsc::Vector(a_residual, false),
                                                     formulation.GetSystemRhs(numbering_subset),
                                                     1.e-12,
                                                     desc,
                                                     __FILE__, __LINE__);

                if (!is_same)
                {
                    std::cerr << "Bug in SNES implementation: third argument of snes function implemention is expected "
                    "to be the residual, which is born in VariationalFormulationT by the attribute RHS(). It is not the case "
                    "here.\n" << desc << std::endl;
                    assert(false);
                }
            }
            #endif // NDEBUG

            formulation.SnesUpdate(a_evaluation_state);
            formulation.ComputeResidual();

            return 0;
        }


        template<class VariationalFormulationT>
        PetscErrorCode SnesInterface<VariationalFormulationT>
        ::Jacobian(SNES snes, Vec evaluation_state, Mat jacobian,
                   Mat preconditioner, void* context_as_void)
        {
            static_cast<void>(snes);
            static_cast<void>(jacobian);
            static_cast<void>(preconditioner);
            static_cast<void>(evaluation_state);

            // \a context_as_void is in fact the VariationalFormulation object.
            assert(context_as_void);
            VariationalFormulationT* formulation_ptr = static_cast<VariationalFormulationT*>(context_as_void);

            auto& formulation = *formulation_ptr;
            formulation.ComputeTangent();

            return 0;
        }



        template<class VariationalFormulationT>
        PetscErrorCode SnesInterface<VariationalFormulationT>
        ::Viewer(SNES snes, PetscInt its, PetscReal norm, void* context_as_void)
        {
            static_cast<void>(snes);
            assert(context_as_void);

            const VariationalFormulationT* variational_formulation_ptr =
            static_cast<const VariationalFormulationT*>(context_as_void);

            assert(!(!variational_formulation_ptr));

            const auto& variational_formulation = *variational_formulation_ptr;

            const auto& vector = variational_formulation.GetSnesEvaluationState();

            const PetscReal evaluation_state_min = vector.Min(__FILE__, __LINE__).second;
            const PetscReal evaluation_state_max = vector.Max(__FILE__, __LINE__).second;

            std::ostringstream oconv;
            oconv << VariationalFormulationT::ClassName() << " --- Norm is %14.12e and extrema are %8.6e and %8.6e\n";

            Wrappers::Petsc::PrintMessageOnFirstProcessor(oconv.str().c_str(),
                                                          variational_formulation.GetMpi(),
                                                          __FILE__, __LINE__,
                                                          its,
                                                          norm,
                                                          evaluation_state_min,
                                                          evaluation_state_max
                                                          );
            return 0;
        }


    } // namespace SolverNS


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


#endif // MOREFEM_x_FORMULATION_SOLVER_x_SNES_x_SNES_INTERFACE_HXX_
