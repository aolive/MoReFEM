///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Feb 2016 14:47:05 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParameterInstancesGroup
/// \addtogroup ParameterInstancesGroup
/// \{

#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_SOLID_x_SOLID_HXX_
# define MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_SOLID_x_SOLID_HXX_


namespace MoReFEM
{


    namespace // anonymous
    {


        template<class InputParameterDataT, class T>
        constexpr bool IsDefined()
        {
            if (Utilities::Tuple::IndexOf<T, typename InputParameterDataT::Tuple>::value != NumericNS::UninitializedIndex<unsigned int>())
                return true;

            // If InputParameter::Solid is defined, ALL parameters are defined and true should be returned!
            return (Utilities::Tuple::IndexOf<InputParameter::Solid, typename InputParameterDataT::Tuple>::value != NumericNS::UninitializedIndex<unsigned int>());
        }


    } // namespace anonymous


    template<class InputParameterDataT>
    Solid::Solid(const InputParameterDataT& input_parameter_data,
                 const Domain& domain,
                 const QuadratureRulePerTopology& quadrature_rule_per_topology,
                 const double relative_tolerance)
    : domain_(domain),
    quadrature_rule_per_topology_(quadrature_rule_per_topology)
    {

        using SolidIP = InputParameter::Solid;

        volumic_mass_ =
            InitScalarParameterFromInputData<InputParameter::Solid::VolumicMass>("Volumic mass",
                                                                                 domain,
                                                                                 input_parameter_data);

        if constexpr (IsDefined<InputParameterDataT, SolidIP::LameLambda>())
        {
            static_assert(IsDefined<InputParameterDataT, SolidIP::LameMu>(),
                          "It makes no sense to define one and not the other");

            std::get<0>(lame_coeff_) =
                InitScalarParameterFromInputData<InputParameter::Solid::LameLambda>("Lame lambda",
                                                                                    domain,
                                                                                    input_parameter_data);
            std::get<1>(lame_coeff_) =
                InitScalarParameterFromInputData<InputParameter::Solid::LameMu>("Lame mu",
                                                                                domain,
                                                                                input_parameter_data);
        }

        if constexpr (IsDefined<InputParameterDataT, SolidIP::YoungModulus>())
        {
            static_assert(IsDefined<InputParameterDataT, SolidIP::PoissonRatio>(),
                          "It makes no sense to define one and not the other");

            std::get<0>(young_poisson_) =
                InitScalarParameterFromInputData<InputParameter::Solid::YoungModulus>("Young modulus",
                                                                                      domain,
                                                                                      input_parameter_data);


            std::get<1>(young_poisson_) =
                InitScalarParameterFromInputData<InputParameter::Solid::PoissonRatio>("Poisson ratio",
                                                                                      domain,
                                                                                      input_parameter_data);
        }

        if constexpr (IsDefined<InputParameterDataT, SolidIP::Kappa1>())
        {
            static_assert(IsDefined<InputParameterDataT, SolidIP::Kappa2>(),
                          "It makes no sense to define one and not the other");

            std::get<0>(kappa_list_) =
                InitScalarParameterFromInputData<InputParameter::Solid::Kappa1>("Kappa_1",
                                                                                domain,
                                                                                input_parameter_data);

            std::get<1>(kappa_list_) =
                InitScalarParameterFromInputData<InputParameter::Solid::Kappa2>("Kappa_2",
                                                                                domain,
                                                                                input_parameter_data);
        }

        if constexpr (IsDefined<InputParameterDataT, SolidIP::HyperelasticBulk>())
        {
            hyperelastic_bulk_ =
                InitScalarParameterFromInputData<InputParameter::Solid::HyperelasticBulk>("Hyperelastic bulk",
                                                                                          domain,
                                                                                          input_parameter_data);
        }

        if constexpr (IsDefined<InputParameterDataT, SolidIP::Viscosity>())
        {
            viscosity_ =
                InitScalarParameterFromInputData<InputParameter::Solid::Viscosity>("Viscosity",
                                                                                   domain,
                                                                                   input_parameter_data);
        }

        if constexpr (IsDefined<InputParameterDataT, SolidIP::Mu1>())
        {
            static_assert(IsDefined<InputParameterDataT, SolidIP::Mu2>()
                          && IsDefined<InputParameterDataT, SolidIP::C0>()
                          && IsDefined<InputParameterDataT, SolidIP::C1>()
                          && IsDefined<InputParameterDataT, SolidIP::C2>()
                          && IsDefined<InputParameterDataT, SolidIP::C3>(),
                          "It makes no sense to define one and not the others");

            std::get<0>(mu_i_C_i_) =
                InitScalarParameterFromInputData<InputParameter::Solid::Mu1>("Mu1",
                                                                             domain,
                                                                             input_parameter_data);
            std::get<1>(mu_i_C_i_) =
                InitScalarParameterFromInputData<InputParameter::Solid::Mu2>("Mu2",
                                                                             domain,
                                                                             input_parameter_data);
            std::get<2>(mu_i_C_i_) =
                InitScalarParameterFromInputData<InputParameter::Solid::C0>("C0",
                                                                            domain,
                                                                            input_parameter_data);

            std::get<3>(mu_i_C_i_) =
                InitScalarParameterFromInputData<InputParameter::Solid::C1>("C1",
                                                                            domain,
                                                                            input_parameter_data);

            std::get<4>(mu_i_C_i_) =
                InitScalarParameterFromInputData<InputParameter::Solid::C2>("C2",
                                                                            domain,
                                                                            input_parameter_data);

            std::get<5>(mu_i_C_i_) =
                InitScalarParameterFromInputData<InputParameter::Solid::C3>("C3",
                                                                            domain,
                                                                            input_parameter_data);
        }

        if (relative_tolerance >= 0.)
            CheckConsistency(relative_tolerance);
    }



    inline const Domain& Solid::GetDomain() const noexcept
    {
        return domain_;
    }


    inline const QuadratureRulePerTopology& Solid::GetQuadratureRulePerTopology() const noexcept
    {
        return quadrature_rule_per_topology_;
    }


    inline const Solid::scalar_parameter& Solid::GetVolumicMass() const noexcept
    {
        assert(!(!volumic_mass_));
        return *volumic_mass_;
    }

    inline const Solid::scalar_parameter& Solid::GetHyperelasticBulk() const noexcept
    {
        assert(!(!hyperelastic_bulk_));
        return *hyperelastic_bulk_;
    }

    inline const Solid::scalar_parameter& Solid::GetKappa1() const noexcept
    {
        decltype(auto) ptr = std::get<0>(kappa_list_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const Solid::scalar_parameter& Solid::GetKappa2() const noexcept
    {
        decltype(auto) ptr = std::get<1>(kappa_list_);
        assert(!(!ptr));
        return *ptr;
    }

    inline const Solid::scalar_parameter& Solid::GetYoungModulus() const noexcept
    {
        decltype(auto) ptr = std::get<0>(young_poisson_);
        assert(!(!ptr));
        return *ptr;

    }

    inline const Solid::scalar_parameter& Solid::GetPoissonRatio() const noexcept
    {
        decltype(auto) ptr = std::get<1>(young_poisson_);
        assert(!(!ptr));
        return *ptr;
    }

    inline const Solid::scalar_parameter& Solid::GetLameLambda() const noexcept
    {
        decltype(auto) ptr = std::get<0>(lame_coeff_);
        assert(!(!ptr));
        return *ptr;
    }

    inline const Solid::scalar_parameter& Solid::GetLameMu() const noexcept
    {
        decltype(auto) ptr = std::get<1>(lame_coeff_);
        assert(!(!ptr));
        return *ptr;
    }

    inline const Solid::scalar_parameter& Solid::GetMu1() const noexcept
    {
        decltype(auto) ptr = std::get<0>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }

    inline const Solid::scalar_parameter& Solid::GetMu2() const noexcept
    {
        decltype(auto) ptr = std::get<1>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }

    inline const Solid::scalar_parameter& Solid::GetC0() const noexcept
    {
        decltype(auto) ptr = std::get<2>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }

    inline const Solid::scalar_parameter& Solid::GetC1() const noexcept
    {
        decltype(auto) ptr = std::get<3>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }

    inline const Solid::scalar_parameter& Solid::GetC2() const noexcept
    {
        decltype(auto) ptr = std::get<4>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }

    inline const Solid::scalar_parameter& Solid::GetC3() const noexcept
    {
        decltype(auto) ptr = std::get<5>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }

    inline const Solid::scalar_parameter& Solid::GetViscosity() const noexcept
    {
        assert(!(!viscosity_));
        return *viscosity_;
    }


    inline bool Solid::IsHyperelasticBulk() const noexcept
    {
        return hyperelastic_bulk_ != nullptr;
    }


    inline bool Solid::IsKappa1() const noexcept
    {
        return std::get<0>(kappa_list_) != nullptr;
    }


    inline bool Solid::IsKappa2() const noexcept
    {
        return std::get<1>(kappa_list_) != nullptr;
    }


    inline bool Solid::IsYoungModulus() const noexcept
    {
        return std::get<0>(young_poisson_) != nullptr;
    }


    inline bool Solid::IsPoissonRatio() const noexcept
    {
        return std::get<1>(young_poisson_) != nullptr;
    }


    inline bool Solid::IsLameLambda() const noexcept
    {
        return std::get<0>(lame_coeff_) != nullptr;
    }


    inline bool Solid::IsLameMu() const noexcept
    {
        return std::get<1>(lame_coeff_) != nullptr;
    }


    inline bool Solid::IsMu1() const noexcept
    {
        return std::get<0>(mu_i_C_i_) != nullptr;
    }


    inline bool Solid::IsMu2() const noexcept
    {
        return std::get<1>(mu_i_C_i_) != nullptr;
    }


    inline bool Solid::IsC0() const noexcept
    {
        return std::get<2>(mu_i_C_i_) != nullptr;
    }


    inline bool Solid::IsC1() const noexcept
    {
        return std::get<3>(mu_i_C_i_) != nullptr;
    }


    inline bool Solid::IsC2() const noexcept
    {
        return std::get<4>(mu_i_C_i_) != nullptr;
    }


    inline bool Solid::IsC3() const noexcept
    {
        return std::get<5>(mu_i_C_i_) != nullptr;
    }


    inline bool Solid::IsViscosity() const noexcept
    {
        return viscosity_ != nullptr;
    }


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_SOLID_x_SOLID_HXX_
