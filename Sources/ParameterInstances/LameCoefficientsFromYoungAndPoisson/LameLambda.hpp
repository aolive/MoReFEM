///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 29 May 2015 14:24:14 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParameterInstancesGroup
/// \addtogroup ParameterInstancesGroup
/// \{

#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_LAME_COEFFICIENTS_FROM_YOUNG_AND_POISSON_x_LAME_LAMBDA_HPP_
# define MOREFEM_x_PARAMETER_INSTANCES_x_LAME_COEFFICIENTS_FROM_YOUNG_AND_POISSON_x_LAME_LAMBDA_HPP_

# include "Parameters/Parameter.hpp"


namespace MoReFEM
{


    namespace ParameterNS
    {


        /*!
         * \class doxygen_hide_lame_coefficient_from_young_and_poisson_warning
         *
         * This is present here as an example of a \a Parameter defined from others; avoid using it
         * if possible as using directly a \a Parameter with lame coefficient inside would be more
         * efficient.
         */


        /*!
         * \brief Yields Lame lambda coefficient from Young modulus and Poisson ratio.
         *
         * \copydoc doxygen_hide_parameter_without_time_dependency
         * \copydoc doxygen_hide_lame_coefficient_from_young_and_poisson_warning
         */
        class LameLambda final : public ScalarParameter<>
        {

        public:

            //! \copydoc doxygen_hide_alias_self
            using self = LameLambda;

            //! \copydoc doxygen_hide_parameter_local_coords_type
            using local_coords_type = LocalCoords;

            //! Alias to scalar parameter.
            using scalar_parameter = ScalarParameter<TimeDependencyNS::None>;


        public:

            /// \name Special members.
            ///@{

            //! Constructor.
            explicit LameLambda(const scalar_parameter& young_modulus,
                                const scalar_parameter& poisson_ratio);

            //! Destructor.
            ~LameLambda() override;

            //! Copy constructor.
            LameLambda(const LameLambda&) = delete;

            //! Move constructor.
            LameLambda(LameLambda&&) = delete;

            //! Copy affectation.
            LameLambda& operator=(const LameLambda&) = delete;

            //! Move affectation.
            LameLambda& operator=(LameLambda&&) = delete;

            ///@}

            /*!
             * \brief Enables to modify the constant value of a parameter. Disabled for this Parameter.
             */
            void SetConstantValue(double value) override;

        private:

            //! \copydoc doxygen_hide_parameter_suppl_get_value_local_coords
            return_type SupplGetValue(const local_coords_type& local_coords, const GeometricElt& geom_elt) const override;

            /*
             * \brief Whether the parameter varies spatially or not.
             */
            bool IsConstant() const override;

            //! Young modulus.
            const scalar_parameter& GetYoungModulus() const;

            //! Poisson coefficient.
            const scalar_parameter& GetPoissonRatio() const;

            //! Compute the current value given the values of Young modulus and Poisson ratio.
            return_type ComputeValue(const double young_modulus, const double poisson_ratio) const;


            //! copydoc doxygen_hide_parameter_suppl_get_constant_value
            return_type SupplGetConstantValue() const override;


            /*!
             * \brief Write the content of the Parameter in a stream.
             */
            void SupplWrite(std::ostream& out) const override;


            //! \copydoc doxygen_hide_parameter_suppl_time_update
            void SupplTimeUpdate() override;


            //! \copydoc doxygen_hide_parameter_suppl_time_update_with_time
            void SupplTimeUpdate(double time) override;

            /*!
             *
             * \copydoc doxygen_hide_parameter_suppl_get_any_value
             */
            return_type SupplGetAnyValue() const override;

        private:

            //! Young modulus.
            const scalar_parameter& young_modulus_;

            //! Poisson coefficient.
            const scalar_parameter& poisson_ratio_;

            //! Constant value. Irrelevant if IsConstant = false (the default value is in this case kept).
            double constant_value_ = std::numeric_limits<double>::lowest();

        };


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


# include "ParameterInstances/LameCoefficientsFromYoungAndPoisson/LameLambda.hxx"


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_LAME_COEFFICIENTS_FROM_YOUNG_AND_POISSON_x_LAME_LAMBDA_HPP_
