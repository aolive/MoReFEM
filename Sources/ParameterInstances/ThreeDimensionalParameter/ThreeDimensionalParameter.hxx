///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 28 May 2015 14:52:56 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParameterInstancesGroup
/// \addtogroup ParameterInstancesGroup
/// \{

#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_THREE_DIMENSIONAL_PARAMETER_x_THREE_DIMENSIONAL_PARAMETER_HXX_
# define MOREFEM_x_PARAMETER_INSTANCES_x_THREE_DIMENSIONAL_PARAMETER_x_THREE_DIMENSIONAL_PARAMETER_HXX_


namespace MoReFEM
{


    namespace ParameterNS
    {

        template
        <
            template<ParameterNS::Type> class TimeDependencyT
        >
        void ThreeDimensionalParameter<TimeDependencyT>::SupplWrite(std::ostream& out) const
        {
            out << "# This parameter is defined from three scalar parameters:" << std::endl;
            out << std::endl;
            GetScalarParameterX().Write(out);
            out << std::endl;
            GetScalarParameterY().Write(out);
            out << std::endl;
            GetScalarParameterZ().Write(out);
        }


        template
        <
            template<ParameterNS::Type> class TimeDependencyT
        >
        template<class T>
        ThreeDimensionalParameter<TimeDependencyT>
        ::ThreeDimensionalParameter(T&& name,
                                    scalar_parameter_ptr&& x_component,
                                    scalar_parameter_ptr&& y_component,
                                    scalar_parameter_ptr&& z_component)
        : parent(std::forward<T>(name),
                 x_component->GetDomain()
                 ),
        scalar_parameter_x_(std::move(x_component)),
        scalar_parameter_y_(std::move(y_component)),
        scalar_parameter_z_(std::move(z_component))
        {
            assert(parent::GetDomain() == GetScalarParameterY().GetDomain());
            assert(parent::GetDomain() == GetScalarParameterZ().GetDomain());

            content_.Resize(3);

            if (IsConstant())
            {
                content_(0) = GetScalarParameterX().GetConstantValue();
                content_(1) = GetScalarParameterY().GetConstantValue();
                content_(2) = GetScalarParameterZ().GetConstantValue();
            }

            // \todo #873 Add assert: scalar parameters should not bear any time dependency!

        }


        template
        <
            template<ParameterNS::Type> class TimeDependencyT
        >
        inline typename ThreeDimensionalParameter<TimeDependencyT>::scalar_parameter&
        ThreeDimensionalParameter<TimeDependencyT>
        ::GetScalarParameterX() const
        {
            assert(!(!scalar_parameter_x_));
            return *scalar_parameter_x_;
        }


        template
        <
            template<ParameterNS::Type> class TimeDependencyT
        >
        inline typename ThreeDimensionalParameter<TimeDependencyT>::scalar_parameter&
        ThreeDimensionalParameter<TimeDependencyT>
        ::GetScalarParameterY() const
        {
            assert(!(!scalar_parameter_y_));
            return *scalar_parameter_y_;
        }


        template
        <
            template<ParameterNS::Type> class TimeDependencyT
        >
        inline typename ThreeDimensionalParameter<TimeDependencyT>::scalar_parameter&
        ThreeDimensionalParameter<TimeDependencyT>
        ::GetScalarParameterZ() const
        {
            assert(!(!scalar_parameter_z_));
            return *scalar_parameter_z_;
        }


        template
        <
            template<ParameterNS::Type> class TimeDependencyT
        >
        inline bool ThreeDimensionalParameter<TimeDependencyT>::IsConstant() const
        {
            return GetScalarParameterX().IsConstant()
            && GetScalarParameterY().IsConstant()
            && GetScalarParameterZ().IsConstant();
        }


        template
        <
            template<ParameterNS::Type> class TimeDependencyT
        >
        inline typename ThreeDimensionalParameter<TimeDependencyT>::return_type
        ThreeDimensionalParameter<TimeDependencyT>
        ::SupplGetConstantValue() const
        {
            return content_;
        }


        template
        <
            template<ParameterNS::Type> class TimeDependencyT
        >
        inline typename ThreeDimensionalParameter<TimeDependencyT>::return_type
        ThreeDimensionalParameter<TimeDependencyT>
        ::SupplGetValue(const local_coords_type& local_coords,
                        const GeometricElt& geom_elt) const
        {
            content_(0) = GetScalarParameterX().GetValue(local_coords, geom_elt);
            content_(1) = GetScalarParameterY().GetValue(local_coords, geom_elt);
            content_(2) = GetScalarParameterZ().GetValue(local_coords, geom_elt);

            return content_;
        }


        template
        <
            template<ParameterNS::Type> class TimeDependencyT
        >
        void ThreeDimensionalParameter<TimeDependencyT>::SupplTimeUpdate()
        {
            GetScalarParameterX().TimeUpdate();
            GetScalarParameterY().TimeUpdate();
            GetScalarParameterZ().TimeUpdate();
        }



        template
        <
        template<ParameterNS::Type> class TimeDependencyT
        >
        void ThreeDimensionalParameter<TimeDependencyT>::SupplTimeUpdate(double time)
        {
            GetScalarParameterX().TimeUpdate(time);
            GetScalarParameterY().TimeUpdate(time);
            GetScalarParameterZ().TimeUpdate(time);
        }

        template
        <
            template<ParameterNS::Type> class TimeDependencyT
        >
        typename ThreeDimensionalParameter<TimeDependencyT>::return_type
        ThreeDimensionalParameter<TimeDependencyT>::SupplGetAnyValue() const
        {
            return content_;
        }

        template
        <
        template<ParameterNS::Type> class TimeDependencyT
        >
        inline void ThreeDimensionalParameter<TimeDependencyT>::SetConstantValue(double value)
        {
            static_cast<void>(value);
            assert(false && "SetConstantValue() meaningless for current Parameter.");
            exit(EXIT_FAILURE);
        }


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_THREE_DIMENSIONAL_PARAMETER_x_THREE_DIMENSIONAL_PARAMETER_HXX_
