target_sources(${MOREFEM_PARAM_INSTANCES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/CopyTimeDependency.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/CopyTimeDependency.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/FromAtDofToAtQuadPt.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/FromAtDofToAtQuadPt.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/Local/SourceList.cmake)
