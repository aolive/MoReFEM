///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 14 Oct 2016 11:24:15 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParameterInstancesGroup
/// \addtogroup ParameterInstancesGroup
/// \{

#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_INTERNAL_x_LOCAL_x_FROM_AT_DOF_TO_AT_QUAD_PT_HXX_
# define MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_INTERNAL_x_LOCAL_x_FROM_AT_DOF_TO_AT_QUAD_PT_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace LocalParameterOperatorNS
        {


            template
            <
                ParameterNS::Type TypeT,
                template<ParameterNS::Type> class TimeDependencyT,
                unsigned int NfeltSpace
            >
            const std::string& FromAtDofToAtQuadPt<TypeT, TimeDependencyT, NfeltSpace>::ClassName()
            {
                static std::string ret = std::string("LocalParameterOperatorNS::FromAtDofToAtQuadPt<")
                + ::MoReFEM::ParameterNS::Traits<TypeT>::TypeName() + ">";
                return ret;
            }


            template
            <
                ParameterNS::Type TypeT,
                template<ParameterNS::Type> class TimeDependencyT,
                unsigned int NfeltSpace
            >
            FromAtDofToAtQuadPt<TypeT, TimeDependencyT, NfeltSpace>
            ::FromAtDofToAtQuadPt(const ExtendedUnknown::const_shared_ptr& a_unknown_storage,
                                  elementary_data_type&& a_elementary_data,
                                  ParameterAtQuadraturePoint<TypeT, TimeDependencyT>& parameter_to_set,
                                  const ParameterAtDof<TypeT, TimeDependencyT, NfeltSpace>& param_at_dof)
            : parent(a_unknown_storage, std::move(a_elementary_data), parameter_to_set),
            param_at_dof_(param_at_dof)
            { }


            template
            <
                ParameterNS::Type TypeT,
                template<ParameterNS::Type> class TimeDependencyT,
                unsigned int NfeltSpace
            >
            inline const ParameterAtDof<TypeT, TimeDependencyT, NfeltSpace>&
            FromAtDofToAtQuadPt<TypeT, TimeDependencyT, NfeltSpace>::GetParamAtDof() const noexcept
            {
                return param_at_dof_;
            }



            template
            <
                ParameterNS::Type TypeT,
                template<ParameterNS::Type> class TimeDependencyT,
                unsigned int NfeltSpace
            >
            void FromAtDofToAtQuadPt<TypeT, TimeDependencyT, NfeltSpace>::ComputeEltArray()
            {
                auto& elementary_data = parent::GetNonCstElementaryData();
                const auto& infos_at_quad_pt_list = elementary_data.GetInformationsAtQuadraturePointListForUnknown();
                const auto& geom_elt = elementary_data.GetCurrentGeomElt();

                decltype(auto) param_at_dof = GetParamAtDof();
                decltype(auto) param_to_set = parent::GetNonCstParameter();

                for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
                {
                    const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();
                    decltype(auto) value_from_param_at_dof = param_at_dof.GetValue(quad_pt, geom_elt);

                    param_to_set.UpdateValue(quad_pt, geom_elt,
                                             [&value_from_param_at_dof](non_constant_reference value_to_set)
                                             {
                                                 CopyValue(value_from_param_at_dof, value_to_set);
                                             });
                }
            }


        } // namespace LocalParameterOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_INTERNAL_x_LOCAL_x_FROM_AT_DOF_TO_AT_QUAD_PT_HXX_
