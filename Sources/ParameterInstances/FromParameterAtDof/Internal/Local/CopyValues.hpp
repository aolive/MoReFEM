///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Oct 2016 14:15:02 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParameterInstancesGroup
/// \addtogroup ParameterInstancesGroup
/// \{

#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_INTERNAL_x_LOCAL_x_COPY_VALUES_HPP_
# define MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_INTERNAL_x_LOCAL_x_COPY_VALUES_HPP_

# include "Utilities/MatrixOrVector.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace LocalParameterOperatorNS
        {


            /*!
             * \brief Copy a value for \a FromParameterAtDof in case type is scalar.
             *
             * \param[in] source Source value.
             * \param[out] target Target value.
             */
            void CopyValue(double source, double& target);


            /*!
             * \brief Copy a value for \a FromParameterAtDof in case type is vectorial.
             *
             * \param[in] source Source value.
             * \param[out] target Target value. Must already be allocated.
             */
            void CopyValue(const LocalVector& source, LocalVector& target);


            /*!
             * \brief Copy a value for \a FromParameterAtDof in case type is matricial.
             *
             * \param[in] source Source value.
             * \param[out] target Target value. Must already be allocated.
             */
            void CopyValue(const LocalMatrix& source, LocalMatrix& target);




        } // namespace LocalParameterOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_INTERNAL_x_LOCAL_x_COPY_VALUES_HPP_
