///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 25 Aug 2014 15:52:43 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_SUB_VECTOR_x_BASE_HXX_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_SUB_VECTOR_x_BASE_HXX_


namespace Seldon
{


    ////////////////////
    // SUBVECTOR_BASE //
    ////////////////////


    /***************
     * CONSTRUCTOR *
     ***************/


    //! Main constructor.
    template <class T, class V, class Allocator>
    inline SubVector_Base<T, V, Allocator>
    ::SubVector_Base(V& A, Vector<int> row_list):
    Vector_Base<T, Allocator>(row_list.GetLength()),
    vector_(&A), row_list_(row_list)
    {
    }


    /**************
     * DESTRUCTOR *
     **************/


    //! Destructor.
    template <class T, class V, class Allocator>
    inline SubVector_Base<T, V, Allocator>::~SubVector_Base()
    {
    }


    /************************
     * ACCESS AND AFFECTION *
     ************************/


    //! Access operator.
    /*!
     Returns the value of element (\a i).
     \param[in] i row index.
     \return Element (\a i, \a j) of the sub-vector.
     */
    template <class T, class V, class Allocator>
    inline typename SubVector_Base<T, V, Allocator>::access_type
    SubVector_Base<T, V, Allocator>::operator() (int i)
    {
        return (*this->vector_)(this->row_list_(i));
    }


    //! Access operator.
    /*!
     Returns the value of element (\a i).
     \param[in] i row index.
     \return Element (\a i) of the sub-vector.
     */
    template <class T, class V, class Allocator>
    inline typename SubVector_Base<T, V, Allocator>::const_access_type
    SubVector_Base<T, V, Allocator>::operator() (int i) const
    {
        return (*this->vector_)(this->row_list_(i));
    }


    //! Access operator.
    /*!
     Returns the value of element (\a i).
     \param[in] i row index.
     \return Element (\a i) of the sub-vector.
     */
    template <class T, class V, class Allocator>
    inline typename SubVector_Base<T, V, Allocator>::entry_type&
    SubVector_Base<T, V, Allocator>::Val(int i)
    {
        return this->vector_->Val(this->row_list_(i));
    }


    //! Access operator.
    /*!
     Returns the value of element (\a i).
     \param[in] i row index.
     \return Element (\a i) of the sub-vector.
     */
    template <class T, class V, class Allocator>
    inline const typename SubVector_Base<T, V, Allocator>::entry_type&
    SubVector_Base<T, V, Allocator>::Val(int i) const
    {
        return this->vector_->Val(this->row_list_(i));
    }


    /*****************
     * BASIC METHODS *
     *****************/


    //! Returns the number of rows.
    /*!
     \return The number of rows.
     */
    template <class T, class V, class Allocator>
    inline int SubVector_Base<T, V, Allocator>::GetM() const
    {
        return row_list_.GetLength();
    }


    //! Returns the number of rows of the sub-vector possibly transposed.
    /*!
     \param status assumed status about the transposition of the sub-vector.
     \return The number of rows of the possibly-transposed sub-vector.
     */
    template <class T, class V, class Allocator>
    inline int SubVector_Base<T, V, Allocator>
    ::GetM(const SeldonTranspose& status) const
    {
        if (status.NoTrans())
            return row_list_.GetLength();
        else
            return 1;
    }



    //! Prints a vector on screen.
    /*! Displays all elements on the standard output, in text format.
     */
    template <class T, class V, class Allocator>
    void SubVector_Base<T, V, Allocator>::Print() const
    {
        for (int i = 0; i < this->GetM(); i++)
        {
            std::cout << (*this)(i) << "\t";
            std::cout << std::endl;
        }
    }


} // namespace Seldon


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_SUB_VECTOR_x_BASE_HXX_
