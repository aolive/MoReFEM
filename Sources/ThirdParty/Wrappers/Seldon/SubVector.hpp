///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 25 Aug 2014 15:52:43 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_SUB_VECTOR_HPP_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_SUB_VECTOR_HPP_


# include "Utilities/Pragma/Pragma.hpp"

PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wdeprecated")

# ifdef __clang__
    PRAGMA_DIAGNOSTIC(ignored "-Wreserved-id-macro")
# endif // __clang __

# include "ThirdParty/IncludeWithoutWarning/Seldon/Seldon.hpp"
# include "ThirdParty/Wrappers/Seldon/SubVector_Base.hpp"


namespace Seldon
{


    ////////////////////////
    // VECTOR<SUBSTORAGE> //
    ////////////////////////


    /*!
     * \brief Sub-vector class.
     *
     * \copydoc doxygen_hide_seldon_sub_vector_class
     */
    template <class T, class V, class Allocator>
    class Vector<T, SubStorage<V>, Allocator>:
    public SubVector_Base<T, V, Allocator>
    {
        // Aliases.
    public:

        //! \copydoc doxygen_hide_seldon_sub_vector_base_traits
        using value_type = typename V::value_type;

        //! \copydoc doxygen_hide_seldon_sub_vector_base_traits
        using pointer = typename V::pointer;

        //! \copydoc doxygen_hide_seldon_sub_vector_base_traits
        using const_pointer = typename V::const_pointer;

        //! \copydoc doxygen_hide_seldon_sub_vector_base_traits
        using reference = typename V::reference;

        //! \copydoc doxygen_hide_seldon_sub_vector_base_traits
        using const_reference = typename V::const_reference;

        //! \copydoc doxygen_hide_seldon_sub_vector_base_traits
        using entry_type = typename V::allocator::value_type;

        //! \copydoc doxygen_hide_seldon_sub_vector_base_traits
        using access_type = typename V::allocator::reference;

        //! \copydoc doxygen_hide_seldon_sub_vector_base_traits
        using const_access_type = typename V::allocator::const_reference;

        // Methods.
    public:

        //! \copydoc doxygen_hide_seldon_subvector_constructor
        Vector(V& A, Vector<int> row_list);

        //! Destructor.
        ~Vector();
    };


    ///////////////
    // SUBVECTOR //
    ///////////////


    //! Sub-vector class.
    /*!
     \extends SubVector_Base<T, V, Allocator>
     */
    template <class V>
    class SubVector:
    public Vector<typename V::value_type,
    SubStorage<V>, typename V::allocator>
    {
    public:

        //! \copydoc doxygen_hide_seldon_subvector_constructor
        SubVector(V& A, Vector<int> row_list);
    };


} // namespace Seldon


/// @} // addtogroup ThirdPartyGroup


# include "ThirdParty/Wrappers/Seldon/SubVector.hxx"

PRAGMA_DIAGNOSTIC(pop)


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_SUB_VECTOR_HPP_
