///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 25 Aug 2014 15:52:43 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_SUB_VECTOR_x_BASE_HPP_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_SUB_VECTOR_x_BASE_HPP_



namespace Seldon
{

    /*!
     * \class doxygen_hide_seldon_sub_vector_base_traits
     *
     * \brief Traits required by a Seldon \a Vector, directly lifted from \a V type.
     */


    /*!
     * \class doxygen_hide_seldon_sub_vector_class
     *
     * Seldon defines this kind of utility for matrices but not for vector; I therefore did the latter.
     *
     * \tparam T Type of the quantities stored within the vector.
     * \tparam V Type of the vector from which we want a subpart.
     * \tparam Allocator Allocator.
     */

    /*!
     * \brief Sub-vector base class.
     *
     * \copydoc doxygen_hide_seldon_sub_vector_class
     */
    template <class T, class V, class Allocator>
    class SubVector_Base: public Vector_Base<T, Allocator>
    {
        // Aliases.
    public:

        //! \copydoc doxygen_hide_seldon_sub_vector_base_traits
        using value_type = typename V::value_type;

        //! \copydoc doxygen_hide_seldon_sub_vector_base_traits
        using pointer = typename V::pointer;

        //! \copydoc doxygen_hide_seldon_sub_vector_base_traits
        using const_pointer = typename V::const_pointer;

        //! \copydoc doxygen_hide_seldon_sub_vector_base_traits
        using reference = typename V::reference;

        //! \copydoc doxygen_hide_seldon_sub_vector_base_traits
        using const_reference = typename V::const_reference;

        //! \copydoc doxygen_hide_seldon_sub_vector_base_traits
        using entry_type = typename V::allocator::value_type;

        //! \copydoc doxygen_hide_seldon_sub_vector_base_traits
        using access_type = typename V::allocator::reference;

        //! \copydoc doxygen_hide_seldon_sub_vector_base_traits
        using const_access_type = typename V::allocator::const_reference;

        // Attributes.
    protected:

        //! Pointer to the base vector.
        V* vector_;

        //! List of rows.
        Vector<int> row_list_;

        // Methods.
    public:

        /*!
         * \class doxygen_hide_seldon_subvector_constructor
         *
         * \brief Constructor.
         *
         * \param[in,out] A Vector from which the subvector is extracted. It is modified along the subvector.
         * \param[in] row_list List of the rows of \a A extracted for the sub-vector.
         */

        //! \copydoc doxygen_hide_seldon_subvector_constructor
        SubVector_Base(V& A, Vector<int> row_list);

        //! Destructor.
        ~SubVector_Base();

        //! Element access and affectation.
        access_type operator() (int i);
#ifndef SWIG

        //! Element access and affectation.
        const_access_type operator() (int i) const;
#endif

        //! Element access and affectation.
        entry_type& Val(int i);
#ifndef SWIG

        //! Element access and affectation.
        const entry_type& Val(int i) const;
#endif

        // Basic methods.

        //! Get size of the sub-vector.
        int GetM() const;

        //! Get size of the sub-vector with transpose status (whatever it is: I didn't dig that far into Seldon...)
        int GetM(const SeldonTranspose& status) const;

        //! Print the subvector.
        void Print() const;
    };


} // namespace Seldon


/// @} // addtogroup ThirdPartyGroup


#include "ThirdParty/Wrappers/Seldon/SubVector_Base.hxx"


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_SUB_VECTOR_x_BASE_HPP_
