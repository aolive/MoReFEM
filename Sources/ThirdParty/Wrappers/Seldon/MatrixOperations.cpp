///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 1 Dec 2014 17:14:34 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#include "ThirdParty/Wrappers/Seldon/MatrixOperations.hpp"

#include "Utilities/Numeric/Numeric.hpp"


namespace MoReFEM
{
    
    
    namespace Wrappers
    {
        
        
        namespace Seldon
        {
            
            
            namespace // anonymous
            {
                
                
                void ComputeTransposeComatrix(const LocalMatrix& local_matrix, LocalMatrix& out);
                
                
                
            } // namespace // anonymous
            
            
            
            
            
            double ComputeDeterminant(const LocalMatrix& local_matrix)
            {
                assert(local_matrix.GetM() == local_matrix.GetN() && "Matrix must be square!");
                assert(local_matrix.GetM() < 4 && "At the moment, only determinants for 2x2 or 3x3 matrices are "
                       "computed.");
                
                const int dimension = local_matrix.GetM();
                
                switch(dimension)
                {
                    case 1:
                        return local_matrix(0, 0);
                    case 2:
                        return local_matrix(0, 0) * local_matrix(1, 1) - local_matrix(0, 1) * local_matrix(1, 0);
                    case 3:
                        return local_matrix(0, 0) * (local_matrix(1, 1) * local_matrix(2, 2)
                                                     - local_matrix(1, 2) * local_matrix(2, 1))
                                - local_matrix(0, 1) * (local_matrix(1, 0) * local_matrix(2, 2)
                                                        - local_matrix(1, 2) * local_matrix(2, 0))
                                + local_matrix(0, 2) * (local_matrix(1, 0) * local_matrix(2, 1)
                                                        - local_matrix(1, 1) * local_matrix(2, 0));
                    default:
                        assert(false && "Not handled by this function!");
                        exit(EXIT_FAILURE);
                }
            }
            
            
            LocalMatrix ComputeInverseSquareMatrix(const LocalMatrix& local_matrix, double& determinant)
            {
                LocalMatrix ret(local_matrix);
                ComputeInverseSquareMatrix(local_matrix, ret, determinant);
                return ret;
            }
            
            
            void ComputeInverseSquareMatrix(const LocalMatrix& local_matrix, LocalMatrix& inverse, double& determinant)
            {
                const auto Nrow = local_matrix.GetM();
                assert(Nrow == local_matrix.GetN());
                assert(Nrow == inverse.GetM());
                assert(Nrow == inverse.GetN());
                
                determinant = ComputeDeterminant(local_matrix);
                
                if (NumericNS::IsZero(determinant))
                    throw Exception("Determinant is zero!", __FILE__, __LINE__);
                
                if (Nrow == 1)
                    inverse(0, 0) = 1. / determinant;
                else
                {
                    ComputeTransposeComatrix(local_matrix, inverse);
                    inverse *= 1. / determinant;
                }
            }
            
            
            #ifdef MOREFEM_CHECK_NAN_AND_INF
            void ThrowIfNanInside(const LocalMatrix& matrix, const char* invoking_file, int invoking_line)
            {
                const int Nrow = matrix.GetM();
                const int Ncol = matrix.GetN();
                
                assert(Nrow > 0);
                assert(Ncol > 0);
                
                bool is_nan = false;
                
                for (int i = 0; !is_nan && i < Nrow; ++i)
                {
                    for (int j = 0; !is_nan && j < Ncol; ++j)
                    {
                        if (std::isnan(matrix(i,j)))
                            is_nan = true;
                    }
                }
                                
                if (is_nan)
                    throw Exception("NaN value found in local matrix!", invoking_file, invoking_line);
            }
            #endif // MOREFEM_CHECK_NAN_AND_INF
            
            
            bool IsZeroMatrix(const LocalMatrix& matrix)
            {
                const auto Nrow = matrix.GetM();
                const auto Ncol = matrix.GetN();
                
                bool is_non_zero = false;
                
                for (int i = 0; i < Nrow && !is_non_zero; ++i)
                    for (int j = 0; j < Ncol && !is_non_zero; ++j)
                        if (!NumericNS::IsZero(matrix(i, j)))
                            return false;

                return true;
            }

            
            namespace // anonymous
            {
                
                
                void ComputeTransposeComatrix(const LocalMatrix& local_matrix, LocalMatrix& out)
                {
                    const int Nrow = local_matrix.GetM();
                    assert(Nrow == local_matrix.GetN() && "Matrix must be square!");
                    assert(Nrow < 4 && "At the moment, only determinants for 2x2 or 3x3 matrices are "
                           "computed.");
                    assert(Nrow > 1);
                    assert(out.GetM() == Nrow);
                    assert(out.GetN() == Nrow);
                    
                    switch (Nrow)
                    {
                        case 2:
                        {
                            out(0, 0) = local_matrix(1, 1);
                            out(1, 0) = -local_matrix(1, 0);
                            out(0, 1) = -local_matrix(0, 1);
                            out(1, 1) = local_matrix(0, 0);
                            break;
                        }
                        case 3:
                        {
                            out(0, 0) = local_matrix(1, 1) * local_matrix(2, 2) - local_matrix(2, 1) * local_matrix(1, 2);
                            out(1, 0) = -local_matrix(1, 0) * local_matrix(2, 2) + local_matrix(2, 0) * local_matrix(1, 2);
                            out(2, 0) = local_matrix(1, 0) * local_matrix(2, 1) - local_matrix(2, 0) * local_matrix(1, 1);
                            
                            out(0, 1) = -local_matrix(0, 1) * local_matrix(2, 2) + local_matrix(2, 1) * local_matrix(0, 2);
                            out(1, 1) = local_matrix(0, 0) * local_matrix(2, 2) - local_matrix(2, 0) * local_matrix(0, 2);
                            out(2, 1) = -local_matrix(0, 0) * local_matrix(2, 1) + local_matrix(2, 0) * local_matrix(0, 1);
                            
                            out(0, 2) = local_matrix(0, 1) * local_matrix(1, 2) - local_matrix(1, 1) * local_matrix(0, 2);
                            out(1, 2) = -local_matrix(0, 0) * local_matrix(1, 2) + local_matrix(1, 0) * local_matrix(0, 2);
                            out(2, 2) = local_matrix(0, 0) * local_matrix(1, 1) - local_matrix(1, 0) * local_matrix(0, 1);
                            break;
                        }
                        default:
                            assert(false && "Should not happen!");
                            break;
                    }
                }
                
                
                
            } // namespace // anonymous
            
            
        } // namespace Seldon
        
        
    } // namespace Wrappers
    
    
} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup
