///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 9 Sep 2013 09:55:11 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_SELDON_FUNCTIONS_HPP_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_SELDON_FUNCTIONS_HPP_

# include <cassert>

# include "ThirdParty/IncludeWithoutWarning/Seldon/Seldon.hpp"

# include "Utilities/Exceptions/Exception.hpp"
# include "Utilities/MatrixOrVector.hpp"
# include "Utilities/Numeric/Numeric.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Seldon
        {


            /*!
             * \class doxygen_hide_seldon_outer_prod
             *
             * \brief Computes the outer (tensorial) product of two vectors.
             *
             * \internal <b><tt>[internal]</tt></b> The types of the vectors and matrix below are very constrained on
             * purpose; I need this calculation only for a very specific case (calculation of small matrices for local
             * finite elements).
             *
             * \tparam T1 Type of the content of the vector and matrix. Typically 'double'.
             * \tparam Allocator1 Allocator of the first vector.
             * \tparam Allocator2 Allocator of the second vector.
             *
             * \param[in] X First vector.
             * \param[in] Y Second vector.
             */



            /*!
             * \copydoc doxygen_hide_seldon_outer_prod
             *
             * This method returns a new matrix allocated on the fly; you may want to use its overload which uses up
             * an already allocated matrix.
             *
             * \return Resulting matrix, allocated on the fly.
             */
            template<class T1, class Allocator1, class Allocator2>
            ::Seldon::Matrix<T1, ::Seldon::General, ::Seldon::RowMajor, ::Seldon::SELDON_DEFAULT_ALLOCATOR<T1> >
            OuterProd(const ::Seldon::Vector<T1, ::Seldon::VectFull, Allocator1>& X,
                      const ::Seldon::Vector<T1, ::Seldon::VectFull, Allocator2>& Y);


            /*!
             * \copydoc doxygen_hide_seldon_outer_prod
             *
             * \param[in,out] out Matrix in which the outer product is written. This matrix must be already properly
             * allocated prior to the function call.
             */
            template<class T1, class Allocator1, class Allocator2>
            void
            OuterProd(const ::Seldon::Vector<T1, ::Seldon::VectFull, Allocator1>& X,
                      const ::Seldon::Vector<T1, ::Seldon::VectFull, Allocator2>& Y,
                      ::Seldon::Matrix<T1, ::Seldon::General, ::Seldon::RowMajor, ::Seldon::SELDON_DEFAULT_ALLOCATOR<T1> >& out);


            /*!
             * \brief Computes the cross product of two 3d vectors.
             *
             * \internal <b><tt>[internal]</tt></b> The types of the vectors and matrix below are very constrained on purpose; I need this
             * calculation only for a very specific case (calculation of small matrices for local finite elements).
             *
             * \tparam T1 Type of the content of the vector and matrix. Typically 'double'.
             * \tparam Allocator1 Allocator of the first vector.
             * \tparam Allocator2 Allocator of the second vector.
             *
             * \param[in] X First vector. Must be 3d.
             * \param[in] Y Second vector. Must be 3d.
             * \param[out] out Resulting vector.
             */
            template<class T1, class Allocator1, class Allocator2, class Allocator3>
            void
            CrossProduct(const ::Seldon::Vector<T1, ::Seldon::VectFull, Allocator1>& X,
                         const ::Seldon::Vector<T1, ::Seldon::VectFull, Allocator2>& Y,
                         ::Seldon::Vector<T1, ::Seldon::VectFull, Allocator3>& out);


            /*!
             * \brief Computes the cross product of two 3d vectors.
             *
             * \internal <b><tt>[internal]</tt></b> The types of the vectors and matrix below are very constrained on purpose; I need this
             * calculation only for a very specific case (calculation of small matrices for local finite elements).
             *
             * \tparam T1 Type of the content of the vector and matrix. Typically 'double'.
             * \tparam Allocator1 Allocator of the first vector.
             * \tparam Allocator2 Allocator of the second vector.
             *
             * \param[in] X First vector. Must be 3d.
             * \param[in] Y Second vector. Must be 3d.
             * \return Resulting vector.
             *
             * \warning If you can do it, avoid this form and prefer the namesake function that does not allocate memory for
             * resulting vector.
             */
            template<class T1, class Allocator1, class Allocator2>
            ::Seldon::Vector<T1, ::Seldon::VectFull, ::Seldon::SELDON_DEFAULT_ALLOCATOR<T1> >
            CrossProduct(const ::Seldon::Vector<T1, ::Seldon::VectFull, Allocator1>& X,
                         const ::Seldon::Vector<T1, ::Seldon::VectFull, Allocator2>& Y);




            /*!
             * \brief Performs a LU substitution.
             *
             * \internal <b><tt>[internal]</tt></b> The aim of current function is to provide a similar functionality as UBLAS lu_substitute().
             *
             * For each column COL of \a matrix_to_substitute, the system
             * system_matrix * X = COL is resolved. The solution X is then substituted to COL in \a matrix_to_substitute.
             *
             * \param[in] system_matrix See explanation above. A LU matrix is calculated internally to solve
             * each of the systems. It is required this matrix is square.
             * \param[in,out] matrix_to_substitute In input, the matrix which each column is the RHS of one equation.
             * In output, the solution for each equation.
             * \param[out] determinant Determinant of the LU matrix generated obtained from system_matrix.
             *
             */
            template <class T0, class Prop0, class Storage0, class Allocator0,
            class T1, class Prop1, class Storage1, class Allocator1>
            void SubstituteLU(const ::Seldon::Matrix<T0, Prop0, Storage0, Allocator0>& system_matrix,
                              ::Seldon::Matrix<T1, Prop1, Storage1, Allocator1>& matrix_to_substitute,
                              double& determinant);


            /*!
             * \class doxygen_hide_seldon_transpose_matrix_arg_and_return
             *
             * \param[in] original Original matrix. It is left unchanged: a new matrix is returned.
             *
             * \return Transposed matrix. This new matrix is allocated on the fly.
             */

            /*!
             * \brief Transpose a matrix.
             *
             * \::Seldon provides the function itself, but it is performed inplace:
             * \code
             * \::Seldon\::Transpose(M);
             * \endcode
             *
             * This function copies the original matrix and then returns the transposition of the copy.
             *
             * It is therefore intended to run on small matrices.
             *
             * \internal <b><tt>[internal]</tt></b> This is in fact a direct equivalent to UBLAS trans() function.
             *
             * \copydoc doxygen_hide_seldon_transpose_matrix_arg_and_return
             */
            template <class T, class Prop, class Storage, class Allocator>
            ::Seldon::Matrix<T, Prop, Storage, Allocator>
            Transpose(const ::Seldon::Matrix<T, Prop, Storage, Allocator>& original);


            /*!
             * \brief Transpose a sub-matrix.
             *
             * \::Seldon doesn't handle the transposition of a SubStorage matrix; so it must be copied and then transposed.
             *
             * \copydoc doxygen_hide_seldon_transpose_matrix_arg_and_return
             */
            template <class T, class Prop, class Storage, class Allocator>
            ::Seldon::Matrix<T, Prop, Storage, Allocator>
            Transpose(const ::Seldon::SubMatrix<::Seldon::Matrix<T, Prop, Storage, Allocator>>& original);



            /*!
             * \brief Transpose a matrix in the case the resulting matrix has already been allocated.
             *
             * This does not use Seldon at all; as Seldon would in any case perform a memory allocation at some point.
             *
             * \param[in] original Original matrix.
             * \param[in,out] transposed Transposed matrix. This matrix must be already properly allocated in input.
             *
             */
            template<class SeldonMatrixT>
            void Transpose(const SeldonMatrixT& original, LocalMatrix& transposed);



            /*!
             * \class doxygen_hide_seldon_contiguous_sequence
             *
             * \brief Create a sequence of integer values.
             *
             * \code
             * ContinousSequence(5, 3); // yields {5, 6, 7}
             * ContinousSequence(10, 4); // yields {10, 11, 12, 13}
             * \endcode
             *
             * \param[in] first_index First value to put in the generated vector.
             * \param[in] N Number of values to put in the generated vector (and therefore its size).
             *
             * \return Contiguous sequence of integers value, within a Seldon \a Vector.
             */

            //! \copydoc doxygen_hide_seldon_contiguous_sequence
            ::Seldon::Vector<int> ContiguousSequence(int first_index, int N);

            //! \copydoc doxygen_hide_seldon_contiguous_sequence
            ::Seldon::Vector<int> ContiguousSequence(unsigned int first_index, unsigned int N);


            /*!
             * \brief Check whether a matrix is filled with zero or not.
             */
            template<class SeldonMatrixT>
            bool IsZeroMatrix(const SeldonMatrixT& matrix);


            /*!
             * \brief Check whether a vector is filled with zero or not.
             */
            template<class SeldonVectorT>
            bool IsZeroVector(const SeldonVectorT& vector);


            /*!
             * \brief Copy content of a matrix into another one.
             *
             * \param[in] source Source matrix.
             * \param[in] target Target matrix. This must be already allocated and be the same size as \a source.
             */
            template<class SeldonMatrixT>
            void CopyMatrixContent(const SeldonMatrixT& source, SeldonMatrixT& target);


            /*!
             * \brief Copy content of a vector into another one.
             *
             * \param[in] source Source vector.
             * \param[in] target Target vector. This must be already allocated and be the same size as \a source.
             */
            template<class SeldonVectorT>
            void CopyVectorContent(const SeldonVectorT& source, SeldonVectorT& target);


            /*!
             * \brief Write the result of \a source x \a factor into matrix \a target.
             *
             * \param[in] source Source matrix.
             * \param[in] target Target matrix. This must be already allocated and be the same size as \a source.
             * \param[in] factor The scale factor applied to \a source.
             */
            template<class SeldonMatrixT>
            void ScaleMatrix(double factor, const SeldonMatrixT& source, SeldonMatrixT& target);


            /*!
             * \brief Write the result of \a source x \a factor into vector \a target.
             *
             * \param[in] source Source vector.
             * \param[in] target Target vector. This must be already allocated and be the same size as \a source.
             * \param[in] factor The scale factor applied to \a source.
             */
            template<class SeldonVectorT>
            void ScaleVector(double factor, const SeldonVectorT& source, SeldonVectorT& target);


        } // namespace Seldon


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


# include "ThirdParty/Wrappers/Seldon/SeldonFunctions.hxx"


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_SELDON_FUNCTIONS_HPP_
