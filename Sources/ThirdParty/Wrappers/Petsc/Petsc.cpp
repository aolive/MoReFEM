///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Sep 2013 14:51:15 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#include <cassert>

#include "ThirdParty/Wrappers/Petsc/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"


namespace MoReFEM
{
    
    
    namespace Wrappers
    {
        
        
        namespace Petsc
        {
            
            
            Petsc::Petsc(const char* invoking_file, int invoking_line)
            {
                int error_code = PetscInitialize(PETSC_NULL, PETSC_NULL, PETSC_NULL, "");
                
                if (error_code)
                      throw ExceptionNS::Exception(error_code, "PetscInitialize", invoking_file, invoking_line);
                
            }
                        
            
            Petsc::~Petsc()
            {
                int error_code = PetscFinalize();
                assert(!error_code && "Error in PetscFinalize call!"); // No exception in destructors...
                static_cast<void>(error_code); // avoid warning in release compilation.
            }
            
            
        } // namespace Petsc
        
        
    } // namespace Wrappers
    
    
} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup
