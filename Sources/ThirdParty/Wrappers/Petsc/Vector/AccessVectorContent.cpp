///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 10 Jul 2014 15:37:26 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"


namespace MoReFEM
{
    
    
    namespace Wrappers
    {
        
        
        namespace Petsc
        {
            
            
            
            template<>
            AccessVectorContent<Utilities::Access::read_and_write>
            ::AccessVectorContent(typename VectorForAccess<Utilities::Access::read_and_write>::Type& vector,
                                  const char* invoking_file, int invoking_line)
            : vector_(vector)
            {
                int error_code = VecGetArray(vector.Internal(), &values_);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecGetArray", invoking_file, invoking_line);
            }
            
            
            template<>
            AccessVectorContent<Utilities::Access::read_only>
            ::AccessVectorContent(typename VectorForAccess<Utilities::Access::read_only>::Type& vector,
                                  const char* invoking_file, int invoking_line)
            : vector_(vector)
            {
                int error_code = VecGetArrayRead(vector.Internal(), &values_);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecGetArrayRead", invoking_file, invoking_line);
            }
            
            
            template<>
            AccessVectorContent<Utilities::Access::read_and_write>::~AccessVectorContent()
            {
                assert(vector_.InternalWithoutCheck() != NULL);
                
                int error_code = VecRestoreArray(vector_.Internal(), &values_);
                
                if (error_code)
                {
                    std::cerr << "Error during call to Petsc function VecRestoreArray(). As a result, program will abort."
                    << std::endl;
                    abort();
                }
                
                try
                {
                    vector_.UpdateGhosts(__FILE__, __LINE__);
                } catch (...)
                {
                    std::cerr << "Exception thrown when updating ghosts. As a result, program will abort." << std::endl;
                    abort();
                }
                
            }
            
            
            
            template<>
            AccessVectorContent<Utilities::Access::read_only>::~AccessVectorContent()
            {
                assert(vector_.InternalWithoutCheck() != NULL);
                
                int error_code = VecRestoreArrayRead(vector_.Internal(), &values_);
                assert(error_code == 0); // error code should be 0; exception can't be thrown in a destructor!
                static_cast<void>(error_code); // avoid warning in release mode
            }

            
            
            
            
            
        } // namespace Petsc
        
        
    } // namespace Wrappers
    
    
} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup
