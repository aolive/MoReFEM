///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 24 Nov 2014 11:36:08 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#include <iomanip>

#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Internal/VectorHelper.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace Wrappers
        {
            
            
            namespace Petsc
            {
                
                
                void PrintPerProcessor(const Vector& vector,
                                       const std::string& output_file,
                                       const char* invoking_file, int invoking_line)
                {
                    std::ofstream out;
                    
                    FilesystemNS::File::Create(out, output_file, invoking_file, invoking_line);
                    
                    ::MoReFEM::Wrappers::Petsc
                    ::AccessVectorContent<Utilities::Access::read_only> content(vector,
                                                                                invoking_file, invoking_line);
                    
                    const unsigned int Nvalue = content.GetSize(invoking_file, invoking_line);
                    
                    for (unsigned int i = 0u; i < Nvalue; ++i)
                    {
                        if (std::fabs(content.GetValue(i)) <= NumericNS::DefaultEpsilon<double>())
                        {
                            out << std::setw(12) << std::scientific << 0. << std::endl;
                        }
                        else
                        {
                            out << std::setw(12) << std::scientific << std::setprecision(20) <<  content.GetValue(i) << std::endl;
                        }
                    }
                }
                
                
            } // namespace Petsc
            
            
        } // namespace Wrappers
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup
