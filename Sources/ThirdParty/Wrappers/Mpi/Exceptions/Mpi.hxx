///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Sep 2013 11:37:23 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

//  geometric_elements_exceptions.h
//
//
//  Created by Sebastien Gilles on 12/10/12.
//  Copyright 2012 INRIA. All rights reserved.
//

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_EXCEPTIONS_x_MPI_HXX_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_EXCEPTIONS_x_MPI_HXX_



namespace MoReFEM
{


    namespace Wrappers
    {


        namespace ExceptionNS
        {


            namespace Mpi
            {





            } // namespace Mpi


        } // namespace ExceptionNS


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup




#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_EXCEPTIONS_x_MPI_HXX_
