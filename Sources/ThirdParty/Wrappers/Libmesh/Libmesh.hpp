///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 17 Jan 2013 15:01:33 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_LIBMESH_x_LIBMESH_HPP_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_LIBMESH_x_LIBMESH_HPP_


# include <vector>
# include <cassert>

# include "Utilities/Pragma/Pragma.hpp"


PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wunused-result")

// \attention access to libmesh API should be done only through this current file
// The reason is that library libmesh6.h header is not protected by an include guard.
extern "C"
{


    #include "ThirdParty/Source/Lm6/libmesh6.h"


}


PRAGMA_DIAGNOSTIC(pop)


namespace MoReFEM
{


    namespace Wrappers
    {



        /*!
         * \brief Wrapper to call functions of libmesh 5 library.
         *
         * Libmesh5 uses an interface to read geometrical geometric elements:
         *   GmfGetLin(mesh_index, meditCode, &coor[0], &coor[1], ..., &coor[n-1], &label)
         *
         * where mesh_index is the returned code when the Medit file was opened, meditCode est the
         * enumeration value matching the kind of geometrical hape read (for instance Triangles)
         * and coordinates are the coords belonging to the current geometric element.
         *
         * Such a form is clumsy for generic use: we can't for instance store all coordinates in
         * a single vector easily.
         *
         * The current class provides a static template function to do such tasks; the menial
         * task of providing one by one each element of the vector is hence done once here in
         * this class and we shouldn't have to repeat it later.
         *
         * There is a Python script "generateFunctions.py" in current directory that generates
         * the specializations if someday a new geometric elementis introduced with a number of coords not yet handled.
         */

        class Libmesh final
        {
        public:

            /*!
             * \brief Wrapper around libmesh GmfSetLin.
             *
             * \param[in] mesh_index Index provided by libmesh when the output file was opened
             * \param[in] geometric_elt_code Code of the geometric element considered in Medit (for instance 'GmfTriangles')
             * \param[in] coords Index of coords belonging to the geometric element
             * \param[in] label_index Index of the labels to which the geometric elementbelongs to
             */
            template<unsigned int NumberOfCoordsT>
            static void MeditSetLin(int mesh_index, GmfKwdCod geometric_elt_code,
                                    const std::vector<int>& coords,
                                    int label_index);


            /*!
             * \brief Wrapper around libmesh GmfGetLin.
             *
             * \param[in] mesh_index Index provided by libmesh when the input file was opened
             * \param[in] geometric_elt_code Code of the geometric elementconsidered in Medit (for instance 'GmfTriangles')
             * \param[out] coords Index of coords belonging to the geometric element
             * \param[out] label_index Index of the labels to which the geometric elementbelongs to
             */

            template<unsigned int NumberOfCoordsT>
            static void MeditGetLin(int mesh_index, GmfKwdCod geometric_elt_code,
                                    std::vector<unsigned int>& coords,
                                    int& label_index);


        };


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_LIBMESH_x_LIBMESH_HPP_
