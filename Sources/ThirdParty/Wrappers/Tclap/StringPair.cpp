//! \file 
//
//
//  StringPair.cpp
//  MoReFEM
//
//  Created by sebastien on 06/04/2018.
//Copyright © 2018 Inria. All rights reserved.
//

#include "Utilities/String/String.hpp"
#include "ThirdParty/Wrappers/Tclap/StringPair.hpp"


namespace MoReFEM
{
    
    
    namespace Wrappers
    {
        
        
        namespace Tclap
        {


            StringPair& StringPair::operator=(std::string rhs)
            {
                if (rhs.empty())
                {
                    std::pair<std::string, std::string> empty;
                    value_.swap(empty);
                    return *this;
                }

                Utilities::String::Strip(rhs);

                const auto pos = rhs.find('=');

                const auto last_pos = rhs.rfind('=');

                if (pos == std::string::npos || pos != last_pos)
                    throw TCLAP::ArgParseException(rhs + " is not a StringPair, which expected format is KEY=VALUE.");

                value_.first.assign(rhs, 0, pos);
                value_.second.assign(rhs, pos + 1, rhs.size() - pos - 1);

                return *this;
            }
          
            
        } // namespace Tclap
        
        
    } // namespace Wrappers
  

} // namespace MoReFEM
