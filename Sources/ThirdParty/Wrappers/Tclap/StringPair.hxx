//! \file
//
//
//  StringPair.hxx
//  MoReFEM
//
//  Created by sebastien on 06/04/2018.
//Copyright © 2018 Inria. All rights reserved.
//

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_TCLAP_x_STRING_PAIR_HXX_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_TCLAP_x_STRING_PAIR_HXX_


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Tclap
        {


            inline const std::pair<std::string, std::string>& StringPair::GetValue() const noexcept
            {
                return value_;
            }


        } // namespace Tclap


    } // namespace Wrappers


} // namespace MoReFEM


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_TCLAP_x_STRING_PAIR_HXX_
