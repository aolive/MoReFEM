///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sat, 12 Oct 2013 00:02:08 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#ifndef MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_PARMETIS_x_PARMETIS_HPP_
# define MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_PARMETIS_x_PARMETIS_HPP_

# include "Utilities/Pragma/Pragma.hpp"

PRAGMA_DIAGNOSTIC(push)

PRAGMA_DIAGNOSTIC(ignored "-Wconversion")
PRAGMA_DIAGNOSTIC(ignored "-Wcast-align")

# ifdef __clang__
    PRAGMA_DIAGNOSTIC(ignored "-Wweak-vtables")
    PRAGMA_DIAGNOSTIC(ignored "-Wdeprecated")
    PRAGMA_DIAGNOSTIC(ignored "-Wreserved-id-macro")

    PRAGMA_DIAGNOSTIC(ignored "-Wzero-as-null-pointer-constant")

# endif // __clang__

# ifdef MOREFEM_GCC
#  if __GNUC__ >= 8
PRAGMA_DIAGNOSTIC(ignored "-Wcast-function-type")
#  endif
# endif // MOREFEM_GCC

PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")

#include "parmetis.h"

PRAGMA_DIAGNOSTIC(pop)


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_PARMETIS_x_PARMETIS_HPP_
