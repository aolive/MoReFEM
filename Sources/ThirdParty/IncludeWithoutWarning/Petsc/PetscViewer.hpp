///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 3 Mar 2014 09:12:36 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#ifndef MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_PETSC_x_PETSC_VIEWER_HPP_
# define MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_PETSC_x_PETSC_VIEWER_HPP_

# include "Utilities/Pragma/Pragma.hpp"

PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wsign-conversion")
PRAGMA_DIAGNOSTIC(ignored "-Wfloat-equal")
PRAGMA_DIAGNOSTIC(ignored "-Wcast-qual")
PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")

# ifdef __clang__
    PRAGMA_DIAGNOSTIC(ignored "-Wcomma")
    PRAGMA_DIAGNOSTIC(ignored "-Wreserved-id-macro")
    PRAGMA_DIAGNOSTIC(ignored "-Wzero-as-null-pointer-constant")
# endif // __clang__

#include "petscviewer.h"

PRAGMA_DIAGNOSTIC(pop)


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_PETSC_x_PETSC_VIEWER_HPP_
