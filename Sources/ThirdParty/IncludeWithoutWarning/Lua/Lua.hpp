///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sat, 12 Oct 2013 00:02:08 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#ifndef MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_LUA_x_LUA_HPP_
# define MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_LUA_x_LUA_HPP_

# include "Utilities/Pragma/Pragma.hpp"

PRAGMA_DIAGNOSTIC(push)

PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")

extern "C"
{
    # include "lua.h"
    # include "lualib.h"
    # include "lauxlib.h"
}


PRAGMA_DIAGNOSTIC(pop)


/// @} // addtogroup ThirdPartyGroup

#endif // MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_LUA_x_LUA_HPP_
