///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 13 Oct 2013 22:12:19 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#ifndef MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_SELDON_x_SELDON_HPP_
# define MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_SELDON_x_SELDON_HPP_

# include "Utilities/Pragma/Pragma.hpp"

PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wsign-conversion")
PRAGMA_DIAGNOSTIC(ignored "-Wfloat-equal")

# ifdef __clang__
    PRAGMA_DIAGNOSTIC(ignored "-Wweak-vtables")
    PRAGMA_DIAGNOSTIC(ignored "-Wdeprecated")
    PRAGMA_DIAGNOSTIC(ignored "-Wdocumentation")
    PRAGMA_DIAGNOSTIC(ignored "-Wdangling-else")
    PRAGMA_DIAGNOSTIC(ignored "-Wdocumentation-unknown-command")
    PRAGMA_DIAGNOSTIC(ignored "-Wshorten-64-to-32")
    PRAGMA_DIAGNOSTIC(ignored "-Wreserved-id-macro")
    PRAGMA_DIAGNOSTIC(ignored "-Wzero-as-null-pointer-constant")

# endif // __clang__

# ifdef MOREFEM_GCC
    PRAGMA_DIAGNOSTIC(ignored "-Wparentheses")
# endif // MOREFEM_GCC

PRAGMA_DIAGNOSTIC(ignored "-Wunused-parameter")

# include "Seldon.hxx"

PRAGMA_DIAGNOSTIC(pop)


/// @} // addtogroup ThirdPartyGroup

#endif // MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_SELDON_x_SELDON_HPP_
