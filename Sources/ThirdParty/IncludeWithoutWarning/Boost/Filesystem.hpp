///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 31 Aug 2017 16:47:35 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#ifndef MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_BOOST_x_FILESYSTEM_HPP_
# define MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_BOOST_x_FILESYSTEM_HPP_

# include "Utilities/Pragma/Pragma.hpp"

PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wsign-conversion")
PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
PRAGMA_DIAGNOSTIC(ignored "-Wparentheses")
PRAGMA_DIAGNOSTIC(ignored "-Wcast-qual")

# ifdef __clang__
PRAGMA_DIAGNOSTIC(ignored "-Wdeprecated")
PRAGMA_DIAGNOSTIC(ignored "-Wconversion")
PRAGMA_DIAGNOSTIC(ignored "-Wreserved-id-macro")
PRAGMA_DIAGNOSTIC(ignored "-Wweak-vtables")
PRAGMA_DIAGNOSTIC(ignored "-Wundef")
PRAGMA_DIAGNOSTIC(ignored "-Wzero-as-null-pointer-constant")
# endif // __clang__
    
# ifdef MOREFEM_GCC
PRAGMA_DIAGNOSTIC(ignored "-Wconversion")
# endif // MOREFEM_GCC

#include "boost/exception/diagnostic_information.hpp"
#include "boost/filesystem.hpp"

PRAGMA_DIAGNOSTIC(pop)


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_BOOST_x_FILESYSTEM_HPP_
