/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 14 Nov 2013 13:42:29 +0100
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_INPUT_PARAMETER_LIST_HPP_
# define MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_INPUT_PARAMETER_LIST_HPP_

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/InputParameterData/InputParameterList.hpp"
# include "Core/InputParameter/Geometry/Domain.hpp"
# include "Core/InputParameter/FElt/FEltSpace.hpp"
# include "Core/InputParameter/FElt/Unknown.hpp"
# include "Core/InputParameter/FElt/NumberingSubset.hpp"
# include "Core/InputParameter/Parameter/Source/ScalarTransientSource.hpp"
# include "Core/InputParameter/Parameter/Diffusion/Diffusion.hpp"
# include "Core/InputParameter/InitialCondition/InitialCondition.hpp"
# include "Core/InputParameter/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
# include "Core/InputParameter/TimeManager/TimeManager.hpp"
# include "Core/InputParameter/Geometry/Mesh.hpp"
# include "Core/InputParameter/Solver/Petsc.hpp"


namespace MoReFEM
{


    namespace HeatNS
    {


        //! \copydoc doxygen_hide_source_enum
        enum class ForceIndexList : unsigned int
        {
            volumic_source = 1,
            neumann_boundary_condition = 2,
            robin_boundary_condition = 3
        };


        //! \copydoc doxygen_hide_mesh_enum
            enum class MeshIndex
        {
            mesh = 1 // only one mesh considered in current model!
        };


        //! \copydoc doxygen_hide_domain_enum
            enum class DomainIndex
        {
            highest_dimension = 1,
            neumann = 2,
            robin = 3,
            dirichlet_1 = 4,
            dirichlet_2 = 5,
            full_mesh = 6
        };


        //! \copydoc doxygen_hide_boundary_condition_enum
        enum class BoundaryConditionIndex
        {
            first = 1,
            second = 2
        };


        //! \copydoc doxygen_hide_felt_space_enum
            enum class FEltSpaceIndex
        {
            highest_dimension = 1,
            neumann = 2,
            robin = 3
        };


        //! \copydoc doxygen_hide_unknown_enum
            enum class UnknownIndex
        {
            temperature = 1
        };


        //! \copydoc doxygen_hide_solver_enum
            enum class SolverIndex

        {
            solver = 1
        };


        //! \copydoc doxygen_hide_numbering_subset_enum
            enum class NumberingSubsetIndex
        {
            monolithic = 1
        };


        //! Index to tag the tensors involved.
        enum class TensorIndex
        {
            diffusion_tensor = 1
        };


        //! \copydoc doxygen_hide_initial_condition_enum
        enum class InitialConditionIndex
        {
            temperature_initial_condition = 1
        };


        //! \copydoc doxygen_hide_input_parameter_tuple
        using InputParameterTuple = std::tuple
        <
            InputParameter::TimeManager,

            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic)>,

            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::temperature)>,

            InputParameter::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            InputParameter::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::neumann)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::robin)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::dirichlet_1)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::dirichlet_2)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,

            InputParameter::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::first)>,
            InputParameter::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::second)>,

            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>,
            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>,
            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::robin)>,

            InputParameter::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputParameter::Diffusion::Density,
            InputParameter::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::diffusion_tensor)>,
            InputParameter::Diffusion::TransfertCoefficient,

            InputParameter::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::volumic_source)>,
            InputParameter::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::neumann_boundary_condition)>,
            InputParameter::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::robin_boundary_condition)>,

            InputParameter::InitialCondition<EnumUnderlyingType(InitialConditionIndex::temperature_initial_condition)>,

            InputParameter::Result

        >;


        //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = InputParameterList<InputParameterTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputParameterList>;


    } // namespace HeatNS


} // namespace MoReFEM


#endif // MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_INPUT_PARAMETER_LIST_HPP_
