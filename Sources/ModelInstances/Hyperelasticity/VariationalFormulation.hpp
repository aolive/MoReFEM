/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Jan 2016 15:34:09 +0100
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_MODEL_INSTANCES_x_HYPERELASTICITY_x_VARIATIONAL_FORMULATION_HPP_
# define MOREFEM_x_MODEL_INSTANCES_x_HYPERELASTICITY_x_VARIATIONAL_FORMULATION_HPP_

# include <memory>
# include <vector>

# include "Utilities/InputParameterList/LuaFunction.hpp"

# include "Geometry/Domain/Domain.hpp"

# include "Core/InputParameter/Parameter/Solid/Solid.hpp"

# include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"
# include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"

# include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor.hpp"
# include "OperatorInstances/HyperelasticLaws/CiarletGeymonat.hpp"

# include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/None.hpp"
# include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/ActiveStressPolicy/None.hpp"

# include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/Internal/Helper.hpp"

# include "FormulationSolver/VariationalFormulation.hpp"
# include "FormulationSolver/Crtp/VolumicAndSurfacicSource.hpp"
# include "FormulationSolver/Crtp/HyperelasticLaw.hpp"


# include "ModelInstances/Hyperelasticity/InputParameterList.hpp"


namespace MoReFEM
{


    namespace MidpointHyperelasticityNS
    {


        //! \copydoc doxygen_hide_simple_varf
        class VariationalFormulation
        : public MoReFEM::VariationalFormulation
        <
            VariationalFormulation,
            EnumUnderlyingType(SolverIndex::solver)
        >,
        public FormulationSolverNS::HyperelasticLaw
        <
            VariationalFormulation,
            HyperelasticLawNS::CiarletGeymonat
        >
        {
        private:

            //! \copydoc doxygen_hide_alias_self
            using self = VariationalFormulation;

            //! Alias to the parent class.
            using parent = MoReFEM::VariationalFormulation
            <
                VariationalFormulation,
                EnumUnderlyingType(SolverIndex::solver)
            >;

            //! Friendship to parent class, so this one can access private methods defined below through CRTP.
            friend parent;

            //! Alias to hyperlastic law parent,
            using hyperelastic_law_parent = FormulationSolverNS::HyperelasticLaw
            <
                VariationalFormulation,
                HyperelasticLawNS::CiarletGeymonat
            >;

            //! Alias to the viscoelasticity policy used.
            using ViscoelasticityPolicy =
                GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS::None;

            //! Alias to the active stress policy used.
            using ActiveStressPolicy =
                GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ActiveStressPolicyNS::None;

            //! Alias to the hyperelasticity policy used.
            using hyperelasticity_policy =
                GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS
                ::Hyperelasticity<typename hyperelastic_law_parent::hyperelastic_law_type>;

            //! Alias to the type of the source parameter.
            using force_parameter_type =
                Parameter<ParameterNS::Type::vector, LocalCoords, ParameterNS::TimeDependencyNS::None>;

            //! Alias on a pair of Unknown.
            using UnknownPair = std::pair<const Unknown&, const Unknown&>;

        public:

            //! Alias to the stiffness operator type used.
            using StiffnessOperatorType =
                GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensor
                <
                    hyperelasticity_policy,
                    ViscoelasticityPolicy,
                    ActiveStressPolicy
                >;

        public:

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

        public:

            /// \name Special members.
            ///@{

            //! copydoc doxygen_hide_varf_constructor
            explicit VariationalFormulation(const morefem_data_type& morefem_data,
                                            const NumberingSubset& displacement_numbering_subset,
                                            TimeManager& time_manager,
                                            const GodOfDof& god_of_dof,
                                            DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list);

            //! Destructor.
            ~VariationalFormulation() = default;

            //! Copy constructor.
            VariationalFormulation(const VariationalFormulation&) = delete;

            //! Move constructor.
            VariationalFormulation(VariationalFormulation&&) = delete;

            //! Copy affectation.
            VariationalFormulation& operator=(const VariationalFormulation&) = delete;

            //! Move affectation.
            VariationalFormulation& operator=(VariationalFormulation&&) = delete;

            ///@}

            /*!
             * \brief Get the displacement numbering subset relevant for this VariationalFormulation.
             *
             * There is a more generic accessor in the base class but its use is more unwieldy.
             *
             * \return \a NumberingSubset related to displacement.
             */
            const NumberingSubset& GetDisplacementNumberingSubset() const;

            //! Update for next time step. (not called after each dynamic iteration).
            void UpdateForNextTimeStep();

            //! Prepare dynamic runs.
            void PrepareDynamicRuns();

        private:

            /// \name CRTP-required methods.
            ///@{

            //! \copydoc doxygen_hide_varf_suppl_init
            void SupplInit(const InputParameterList& input_parameter_data);

            /*!
             * \brief Allocate the global matrices and vectors.
             */
            void AllocateMatricesAndVectors();

            //! Define the pointer function required to calculate the function required by the non-linear problem.
            Wrappers::Petsc::Snes::SNESFunction ImplementSnesFunction() const;

            //! Define the pointer function required to calculate the jacobian required by the non-linear problem.
            Wrappers::Petsc::Snes::SNESJacobian ImplementSnesJacobian() const;

            //! Define the pointer function required to view the results required by the non-linear problem.
            Wrappers::Petsc::Snes::SNESViewer ImplementSnesViewer() const;

            //! Define the pointer function required to test the convergence required by the non-linear problem.
            Wrappers::Petsc::Snes::SNESConvergenceTestFunction ImplementSnesConvergenceTestFunction() const;

            ///@}

        private:

            /*!
             * \brief This function is given to SNES to compute the function.
             *
             * However as a matter of fact the jacobian will also be computed here, as it is the way Felisce works
             * (it hence avoids duplication or storage of intermediate matrixes)
             *
             * \param[in,out] snes SNES object. Not explicitly used but required by Petsc prototype.
             * \param[in,out] petsc_vec_displacement_at_newton_iteration State at which to evaluate residual.
             * \param[in,out] residual Vector to put residual. What is used as residual in \a VariationalFormulation is
             * \a GetSystemRHS().
             * \param[in,out] context_as_void Optional user-defined function context. In our case a pointer to the
             * VariationalFormulationT object.
             *
             * \return Petsc error code.
             */
            static PetscErrorCode Function(SNES snes, Vec petsc_vec_displacement_at_newton_iteration,
                                           Vec residual, void* context_as_void);


            /*!
             * \brief This function is used to monitor evolution of the convergence.
             *
             * Is is intended to be passed to SNESMonitorSet()
             *
             * \param[in] snes SNES object. Not explicitly used but required by Petsc prototype.
             * \param[in] its Index of iteration.
             * \param[in] norm Current L^2 norm.
             * \param[in] context_as_void Optional user-defined function context. In our case is is a pointer to a
             * VariationalFormulationT object.
             *
             * \return Petsc error code.
             */
            static PetscErrorCode Viewer(SNES snes, PetscInt its, PetscReal norm, void* context_as_void);


            /*!
             * \brief This function is supposed to be given to SNES to compute the jacobian.
             *
             * \copydoc doxygen_hide_snes_interface_common_arg
             * \param[in,out] evaluation_state Most recent value of the quantity the solver tries to compute.
             * \param[in,out] jacobian Jacobian matrix. Actually unused in our wrapper.
             * \param[in,out] preconditioner Preconditioner matrix. Actually unused in our wrapper.
             */
            static PetscErrorCode Jacobian(SNES snes, Vec evaluation_state, Mat jacobian , Mat preconditioner,
                                           void* context_as_void);

        private:

            /*!
             * \brief Assemble method for the mass operator.
             */
            void AssembleStaticOperators();


            /*!
             * \brief Assemble method for the mass operator.
             */
            void AssembleDynamicOperators();

            /*!
             * \brief Assemble method for all the dynamic operators.
             */
            void AssembleOperators();

            /*!
             * \brief Assemble method for all the static operators.
             */
            void AssembleNewtonStaticOperators();

            /*!
             * \brief Assemble method for all the dynamic operators.
             */
            void AssembleNewtonDynamicOperators();

            /*!
             * \brief Update the content of all the vectors and matrices relevant to the computation of the tangent
             * and the residual.
             */
            void UpdateVectorsAndMatrices(const Vec& petsc_vec_displacement_at_newton_iteration);

            /*!
             * \brief Update the content of all the vectors and matrices relevant to the computation of the tangent
             * and the residual.
             */
            void UpdateStaticVectorsAndMatrices(const Vec& petsc_vec_displacement_at_newton_iteration);

            /*!
             * \brief Update the content of all the vectors and matrices relevant to the computation of the tangent
             * and the residual.
             */
            void UpdateDynamicVectorsAndMatrices(const Vec& petsc_vec_displacement_at_newton_iteration);

            //! Compute the matrix of the system.
            void ComputeTangent();

            //! Compute the matrix of the system for a static case.
            void ComputeStaticTangent();

            //! Compute the rhs of the system for a static case.
            void ComputeStaticResidual();

            //! Compute the matrix of the system for a dynamic case.
            void ComputeDynamicTangent();

            //! Compute the rhs of the system for a dynamic case.
            void ComputeDynamicResidual();

            /*!
             * \brief Compute the RHS of the system.
             *
             * The index of the time iteration is used to know whether the static or dynamic system muste be computed.
             */
            void ComputeResidual();

            //! Update current displacement. Already called in UpdateForNextTimeStep().
            void UpdateDisplacementBetweenTimeStep();

            //! Update current displacement. Already called in UpdateForNextTimeStep().
            void UpdateVelocityBetweenTimeStep();

            //! Compute the guess for next time step with the new velocity.
            void ComputeGuessForNextTimeStep();

        private:

            /*!
             * \brief Define the properties of all the static global variational operators involved.
             *
             * \copydoc doxygen_hide_input_parameter_data_arg
             */
            void DefineStaticOperators(const InputParameterList& input_parameter_data);

            /*!
             * \brief Define the properties of all the dynamic global variational operators involved.
             */
            void DefineDynamicOperators();

            //! Get the mass per square time step operator.
            const GlobalVariationalOperatorNS::Mass& GetMassOperator() const noexcept;

            //! Get the hyperelastic stiffness operator.
            const StiffnessOperatorType& GetStiffnessOperator() const noexcept;

            //! Accessor to the surfacic source operator.
            const GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector>&
                GetSurfacicForceOperator() const noexcept;

        private:

            /// \name Global variational operators.
            ///@{

            //! Mass operator.
            GlobalVariationalOperatorNS::Mass::const_unique_ptr mass_operator_ = nullptr;

            //! Stiffness operator.
            StiffnessOperatorType::const_unique_ptr stiffness_operator_ = nullptr;

            //! Volumic source operator.
            GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector>::const_unique_ptr surfacic_force_operator_ = nullptr;

            ///@}

        private:

            /// \name Accessors to the global vectors and matrices managed by the class.
            ///@{

            const GlobalMatrix& GetMatrixMassPerSquareTimeStep() const noexcept;

            GlobalMatrix& GetNonCstMatrixMassPerSquareTimeStep() noexcept;

            const GlobalVector& GetVectorStiffnessResidual() const noexcept;

            GlobalVector& GetNonCstVectorStiffnessResidual() noexcept;

            const GlobalMatrix& GetMatrixTangentStiffness() const noexcept;

            GlobalMatrix& GetNonCstMatrixTangentStiffness() noexcept;

            const GlobalVector& GetVectorSurfacicForce() const noexcept;

            GlobalVector& GetNonCstVectorSurfacicForce() noexcept;

            const GlobalVector& GetVectorCurrentDisplacement() const noexcept;

            GlobalVector& GetNonCstVectorCurrentDisplacement() noexcept;

            const GlobalVector& GetVectorCurrentVelocity() const noexcept;

            GlobalVector& GetNonCstVectorCurrentVelocity() noexcept;

            GlobalVector& GetNonCstVectorVelocityAtNewtonIteration() noexcept;

            const GlobalVector& GetVectorVelocityAtNewtonIteration() const noexcept;

            const GlobalVector& GetVectorMidpointPosition() const noexcept;

            GlobalVector& GetNonCstVectorMidpointPosition() noexcept;

            const GlobalVector& GetVectorMidpointVelocity() const noexcept;

            GlobalVector& GetNonCstVectorMidpointVelocity() noexcept;

            const GlobalVector& GetVectorDiffDisplacement() const noexcept;

            GlobalVector& GetNonCstVectorDiffDisplacement() noexcept;

            ///@}

            //! Access to the solid.
            const Solid& GetSolid() const noexcept;

        private:

            /*!
             * \brief Update the vector that contains the values seeked at the moment the residual is evaluated.
             *
             * \param[in] petsc_vec_displacement_at_newton_iteration Evaluation state given by the Petsc's snes function.
             */
            void UpdateDisplacementAtNewtonIteration(Vec petsc_vec_displacement_at_newton_iteration);

            /*!
             * \brief Non constant access to the evaluation state, i.e. the values at the time Petsc evaluates
             * the residual.
             *
             * \internal <b><tt>[internal]</tt></b> This accessor should not be used except in the Snes::Function() method: the point is to
             * store the value given by Petsc internal Newton algorithm.
             * Its value should be modified only through the call if \a UpdateDisplacementAtNewtonIteration().
             *
             * \return Reference to the vector ernclosing displacement at newton iteration.
             */
            GlobalVector& GetNonCstVectorDisplacementAtNewtonIteration() noexcept;

            //! Constant access to the current displacement at newton iteration.
            const GlobalVector& GetVectorDisplacementAtNewtonIteration() const noexcept;

            /*!
             * \brief Update the vector that contains the values seeked at the moment the residual is evaluated.
             *
             */
            void UpdateVelocityAtNewtonIteration();

        private:

            /// \name Global vectors and matrices specific to the problem.
            ///@{

            //! Stiffness residual vector.
            GlobalVector::unique_ptr vector_stiffness_residual_ = nullptr;

            //! Evaluation state of the residual of the problem (only useful in SNES method)
            GlobalVector::unique_ptr vector_surfacic_force_ = nullptr;

            //! Mass matrix.
            GlobalMatrix::unique_ptr matrix_mass_per_square_time_step_ = nullptr;

            //! Matrix stiffness tangent.
            GlobalMatrix::unique_ptr matrix_tangent_stiffness_ = nullptr;

            //! Evaluation state of the residual of the problem (only useful in SNES method)
            GlobalVector::unique_ptr vector_displacement_at_newton_iteration_ = nullptr;

            //! Evaluation state of the residual of the problem (only useful in SNES method)
            GlobalVector::unique_ptr vector_velocity_at_newton_iteration_ = nullptr;

            //! Velocity from previous time iteration.
            GlobalVector::unique_ptr vector_current_velocity_ = nullptr;

            //! Displacement from previous time iteration.
            GlobalVector::unique_ptr vector_current_displacement_ = nullptr;

            //! Midpoint position.
            GlobalVector::unique_ptr vector_midpoint_position_ = nullptr;

            //! Difference displacement Yn+1 - Yn. Here just to avoid allocate it every time step.
            GlobalVector::unique_ptr vector_diff_displacement_ = nullptr;

            //! Midpoint velocity.
            GlobalVector::unique_ptr vector_midpoint_velocity_ = nullptr;

            ///@}

        private:

            /// \name Numbering subsets used in the formulation.
            ///@{

            const NumberingSubset& displacement_numbering_subset_;

            ///@}

        private:

            //! Material parameters of the solid.
            Solid::const_unique_ptr solid_ = nullptr;

            //! Force parameter for the static force.
            force_parameter_type::unique_ptr force_parameter_ = nullptr;
        };


    } // namespace MidpointHyperelasticityNS


} // namespace MoReFEM


# include "ModelInstances/Hyperelasticity/VariationalFormulation.hxx"


#endif // MOREFEM_x_MODEL_INSTANCES_x_HYPERELASTICITY_x_VARIATIONAL_FORMULATION_HPP_
