

#include "Utilities/Filesystem/File.hpp"

#include "ModelInstances/Hyperelasticity/InputParameterList.hpp"

using namespace MoReFEM;

#include "Test/Tools/CatchMainTest.hpp"
#include "Test/Tools/CheckIdenticalFiles.hpp"
#include "Test/Tools/CompareEnsightFiles.hpp"


void CommonTestCase(std::string&& seq_or_par);


void CommonTestCase(std::string&& seq_or_par)
{
    decltype(auto) environment = Utilities::Environment::GetInstance(__FILE__, __LINE__);
    std::string root_dir, output_dir;

    REQUIRE_NOTHROW(root_dir = environment.GetEnvironmentVariable("MOREFEM_ROOT", __FILE__, __LINE__));
    REQUIRE_NOTHROW(output_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__));

    REQUIRE(FilesystemNS::Folder::DoExist(root_dir));
    REQUIRE(FilesystemNS::Folder::DoExist(output_dir));

    std::string ref_dir = root_dir + "/Sources/ModelInstances/Hyperelasticity/ExpectedResults/";
    std::string obtained_dir = output_dir + std::string("/") + seq_or_par + std::string("/MidpointHyperelasticity/");

    TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "input_data.lua", __FILE__, __LINE__);
    TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "model_name.hhdata", __FILE__, __LINE__);
    TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "unknowns.hhdata", __FILE__, __LINE__);

    ref_dir += "/Mesh_1/";
    obtained_dir += "/Mesh_1/";

    TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "interfaces.hhdata", __FILE__, __LINE__);

    ref_dir += "Ensight6/";
    obtained_dir += "Ensight6/";

    TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "mesh.geo", __FILE__, __LINE__);
    TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "problem.case", __FILE__, __LINE__);

    std::ostringstream oconv;

    for (auto i = 0; i <= 20; ++i)
    {
        oconv.str("");
        oconv << "displacement." << std::setw(5) << std::setfill('0') << i << ".scl";
        TestNS::CompareEnsightFiles(ref_dir, obtained_dir, oconv.str(), __FILE__, __LINE__);
    }

}
                         


TEST_CASE("Hyperelasticity output is conform to what is expected - 3D case, sequential")
{
    CommonTestCase("Seq");
}


TEST_CASE("Hyperelasticity output is conform to what is expected - 3D case, mpi 4 processors")
{
    CommonTestCase("Mpi4");
}


