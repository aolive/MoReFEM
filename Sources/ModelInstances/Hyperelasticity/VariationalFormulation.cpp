/// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 3 Feb 2017 11:26:22 +0100
/// Copyright (c) Inria. All rights reserved.
///

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Filesystem/Folder.hpp"
#include "Utilities/Filesystem/File.hpp"

# include "Geometry/Mesh/Advanced/DistanceFromMesh.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "ModelInstances/Hyperelasticity/VariationalFormulation.hpp"


namespace MoReFEM
{
    
    
    namespace MidpointHyperelasticityNS
    {
        

        VariationalFormulation::VariationalFormulation(const morefem_data_type& morefem_data,
                                                       const NumberingSubset& displacement_numbering_subset,
                                                       TimeManager& time_manager,
                                                       const GodOfDof& god_of_dof,
                                                       DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list)
        : parent(morefem_data,
                 time_manager,
                 god_of_dof,
                 std::move(boundary_condition_list)),
        displacement_numbering_subset_(displacement_numbering_subset)
        {
            assert(time_manager.IsTimeStepConstant() && "Current instantiation relies on this assumption!");
        }
        
        
        void VariationalFormulation::AllocateMatricesAndVectors()
        {
            const auto& displacement_numbering_subset = GetDisplacementNumberingSubset();
            
            parent::AllocateSystemMatrix(displacement_numbering_subset, displacement_numbering_subset);
            parent::AllocateSystemVector(displacement_numbering_subset);
            
            const auto& system_matrix = GetSystemMatrix(displacement_numbering_subset, displacement_numbering_subset);
            const auto& system_rhs = GetSystemRhs(displacement_numbering_subset);
            
            vector_stiffness_residual_ = std::make_unique<GlobalVector>(system_rhs);
            vector_surfacic_force_ = std::make_unique<GlobalVector>(system_rhs);
            vector_displacement_at_newton_iteration_ = std::make_unique<GlobalVector>(system_rhs);
            vector_velocity_at_newton_iteration_ = std::make_unique<GlobalVector>(system_rhs);
            
            matrix_tangent_stiffness_ = std::make_unique<GlobalMatrix>(system_matrix);
            
            vector_current_displacement_ = std::make_unique<GlobalVector>(system_rhs);
            vector_current_velocity_ = std::make_unique<GlobalVector>(system_rhs);
            vector_midpoint_position_ = std::make_unique<GlobalVector>(system_rhs);
            vector_midpoint_velocity_ = std::make_unique<GlobalVector>(system_rhs);
            vector_diff_displacement_ = std::make_unique<GlobalVector>(system_rhs);
            matrix_mass_per_square_time_step_ = std::make_unique<GlobalMatrix>(system_matrix);
        }
        
        
        void VariationalFormulation::SupplInit(const InputParameterList& input_parameter_data)
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& felt_space_volume = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume));
            
            decltype(auto) domain_full_mesh =
                DomainManager::GetInstance(__FILE__, __LINE__).GetDomain(EnumUnderlyingType(DomainIndex::full_mesh), __FILE__, __LINE__);
            
            solid_ = std::make_unique<Solid>(input_parameter_data,
                                             domain_full_mesh,
                                             felt_space_volume.GetQuadratureRulePerTopology());

            DefineStaticOperators(input_parameter_data);
        }

        
        void VariationalFormulation::DefineStaticOperators(const InputParameterList& input_parameter_data)
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& felt_space_volume = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume));
            const auto& felt_space_force = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::force));
            
            const auto& displacement_ptr = UnknownManager::GetInstance(__FILE__, __LINE__).GetUnknownPtr(EnumUnderlyingType(UnknownIndex::displacement));
            
            namespace GVO = GlobalVariationalOperatorNS;
            
            namespace IPL = Utilities::InputParameterListNS;
            
            hyperelastic_law_parent::Create(GetSolid());
            
            stiffness_operator_ =
                std::make_unique<StiffnessOperatorType>(felt_space_volume,
                                                        displacement_ptr,
                                                        displacement_ptr,
                                                        GetSolid(),
                                                        GetTimeManager(),
                                                        hyperelastic_law_parent::GetHyperelasticLawPtr(),
                                                        nullptr,
                                                        nullptr);
            
            decltype(auto) domain_force =
                DomainManager::GetInstance(__FILE__, __LINE__).GetDomain(EnumUnderlyingType(DomainIndex::force), __FILE__, __LINE__);
            
            using parameter_type = InputParameter::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic)>;
            
            force_parameter_ =
                InitParameterFromInputData<ParameterNS::Type::vector>::template Perform<parameter_type>("Surfacic force",
                                                                                                        domain_force,
                                                                                                        input_parameter_data);
            
            if (force_parameter_ != nullptr)
            {
                surfacic_force_operator_ = std::make_unique<GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector>>(felt_space_force,
                                                                                                                                     displacement_ptr,
                                                                                                                                     *force_parameter_);
            }
        }
        
        PetscErrorCode VariationalFormulation::Function(SNES snes,
                                                        Vec petsc_vec_displacement_at_newton_iteration,
                                                        Vec a_residual,
                                                        void* context_as_void)
        {
            static_cast<void>(snes);
            static_cast<void>(a_residual); // a_residual is not used as its content mirror the one of system_rhs_.
            
            // \a context_as_void is in fact the VariationalFormulation object.
            assert(context_as_void);
            
            VariationalFormulation* variational_formulation_ptr = static_cast<VariationalFormulation*>(context_as_void);
            
            assert(!(!variational_formulation_ptr));
            
            auto& variational_formulation = *variational_formulation_ptr;
            
            #ifndef NDEBUG
            
            const auto& displacement_numbering_subset = variational_formulation.GetDisplacementNumberingSubset();
            
            {
                std::string desc;
                
                namespace Petsc = Wrappers::Petsc;
                
                const bool is_same = Petsc::AreEqual(Petsc::Vector(a_residual, false),
                                                     variational_formulation.GetSystemRhs(displacement_numbering_subset),
                                                     1.e-12,
                                                     desc,
                                                     __FILE__, __LINE__);
                
                if (!is_same)
                {
                    std::cerr << "Bug in SNES implementation: third argument of snes function implemention is expected "
                    "to be the residual, which is born in VariationalFormulationT by the attribute RHS(). It is not the case "
                    "here.\n" << desc << std::endl;
                    assert(false);
                }
            }
            #endif // NDEBUG
            
            variational_formulation.UpdateVectorsAndMatrices(petsc_vec_displacement_at_newton_iteration);
            variational_formulation.ComputeResidual();
            
            return 0;
        }
        
        
        void VariationalFormulation::UpdateVectorsAndMatrices(const Vec& petsc_vec_displacement_at_newton_iteration)
        {
            switch(GetTimeManager().GetStaticOrDynamic())
            {
                case StaticOrDynamic::static_:
                    UpdateStaticVectorsAndMatrices(petsc_vec_displacement_at_newton_iteration);
                    break;
                case StaticOrDynamic::dynamic_:
                    UpdateDynamicVectorsAndMatrices(petsc_vec_displacement_at_newton_iteration);
                    break;
            }
            
            AssembleOperators();
        }
        
        
        void VariationalFormulation::UpdateStaticVectorsAndMatrices(const Vec& petsc_vec_state_at_newton_iteration)
        {
            UpdateDisplacementAtNewtonIteration(petsc_vec_state_at_newton_iteration);
        }
        
        
        void VariationalFormulation::UpdateDisplacementAtNewtonIteration(Vec petsc_vec_displacement_at_newton_iteration)
        {
            auto& displacement_at_newton_iteration = GetNonCstVectorDisplacementAtNewtonIteration();
            displacement_at_newton_iteration.SetFromPetscVec(petsc_vec_displacement_at_newton_iteration, __FILE__, __LINE__);
            displacement_at_newton_iteration.UpdateGhosts(__FILE__, __LINE__);
            
            GetNonCstVectorCurrentDisplacement().UpdateGhosts(__FILE__, __LINE__);
        }
        
        void VariationalFormulation::UpdateDynamicVectorsAndMatrices(const Vec& petsc_vec_displacement_at_newton_iteration)
        {
            UpdateDisplacementAtNewtonIteration(petsc_vec_displacement_at_newton_iteration);
            
            UpdateVelocityAtNewtonIteration();
            
            auto& midpoint_position = GetNonCstVectorMidpointPosition();
            
            midpoint_position.Copy(GetVectorCurrentDisplacement(), __FILE__, __LINE__);
            Wrappers::Petsc::AXPY(1., GetVectorDisplacementAtNewtonIteration(), midpoint_position, __FILE__, __LINE__);
            
            midpoint_position.Scale(0.5, __FILE__, __LINE__);
            
            midpoint_position.UpdateGhosts(__FILE__, __LINE__);
            
            auto& midpoint_velocity = GetNonCstVectorMidpointVelocity();
            
            midpoint_velocity.Copy(GetVectorDisplacementAtNewtonIteration(), __FILE__, __LINE__);
            
            Wrappers::Petsc::AXPY(-1., GetVectorCurrentDisplacement(), midpoint_velocity, __FILE__, __LINE__);
            
            midpoint_velocity.Scale(1. / GetTimeManager().GetTimeStep(), __FILE__, __LINE__);
            
            midpoint_velocity.UpdateGhosts(__FILE__, __LINE__);
            
            GetNonCstVectorCurrentDisplacement().UpdateGhosts(__FILE__, __LINE__);
        }
        
        
        void VariationalFormulation::UpdateVelocityAtNewtonIteration()
        {
            auto& velocity_at_newton_iteration = GetNonCstVectorVelocityAtNewtonIteration();
            const auto& velocity_previous_time_iteration = GetVectorCurrentVelocity();
            
            if (GetSnes().GetSnesIteration(__FILE__, __LINE__) == 0)
            {
                velocity_at_newton_iteration.Copy(GetVectorCurrentVelocity(), __FILE__, __LINE__);
            }
            else
            {
                const auto& displacement_previous_time_iteration = GetVectorCurrentDisplacement();
                const auto& displacement_at_newton_iteration = GetVectorDisplacementAtNewtonIteration();
                
                auto& diff_displacement = GetNonCstVectorDiffDisplacement();
                
                diff_displacement.Copy(displacement_at_newton_iteration, __FILE__, __LINE__);
                Wrappers::Petsc::AXPY(-1., displacement_previous_time_iteration, diff_displacement, __FILE__, __LINE__);
                diff_displacement.Scale(2. / GetTimeManager().GetTimeStep(), __FILE__, __LINE__);
                velocity_at_newton_iteration.Copy(diff_displacement, __FILE__, __LINE__);
                Wrappers::Petsc::AXPY(- 1., velocity_previous_time_iteration, velocity_at_newton_iteration, __FILE__, __LINE__);
            }
            
            velocity_at_newton_iteration.UpdateGhosts(__FILE__, __LINE__);
        }
        
        
        void VariationalFormulation::AssembleOperators()
        {
            switch(GetTimeManager().GetStaticOrDynamic())
            {
                case StaticOrDynamic::static_:
                    AssembleNewtonStaticOperators();
                    break;
                case StaticOrDynamic::dynamic_:
                    AssembleNewtonDynamicOperators();
                    break;
            }
        }

        
        void VariationalFormulation::AssembleNewtonStaticOperators()
        {
            {
                auto& matrix_tangent_stiffness = GetNonCstMatrixTangentStiffness();
                auto& vector_stiffness_residual = GetNonCstVectorStiffnessResidual();
                
                matrix_tangent_stiffness.ZeroEntries(__FILE__, __LINE__);
                vector_stiffness_residual.ZeroEntries(__FILE__, __LINE__);
                
                GlobalMatrixWithCoefficient mat(matrix_tangent_stiffness, 1.);
                GlobalVectorWithCoefficient vec(vector_stiffness_residual, 1.);
                
                const GlobalVector& displacement_vector = GetVectorDisplacementAtNewtonIteration();
                GetStiffnessOperator().Assemble(std::make_tuple(std::ref(mat), std::ref(vec)),
                                                displacement_vector);
                
            }
            
            const unsigned int newton_iteration = GetSnes().GetSnesIteration(__FILE__, __LINE__);
            
            if (newton_iteration == 0)
            {
                auto& vector_surfacic_force = GetNonCstVectorSurfacicForce();
                
                vector_surfacic_force.ZeroEntries(__FILE__, __LINE__);
                
                GlobalVectorWithCoefficient vec(vector_surfacic_force, 1.);
                
                const double time = parent::GetTimeManager().GetTime();
                
                GetSurfacicForceOperator().Assemble(std::make_tuple(std::ref(vec)),
                                                    time);
            }
        }
        
        void VariationalFormulation::AssembleNewtonDynamicOperators()
        {
            {
                auto& matrix_tangent_stiffness = GetNonCstMatrixTangentStiffness();
                auto& vector_stiffness_residual = GetNonCstVectorStiffnessResidual();
                
                matrix_tangent_stiffness.ZeroEntries(__FILE__, __LINE__);
                vector_stiffness_residual.ZeroEntries(__FILE__, __LINE__);
                
                GlobalMatrixWithCoefficient mat(matrix_tangent_stiffness, 1.);
                GlobalVectorWithCoefficient vec(vector_stiffness_residual, 1.);
                
                const GlobalVector& displacement_vector = GetVectorMidpointPosition();
                
                GetStiffnessOperator().Assemble(std::make_tuple(std::ref(mat), std::ref(vec)),
                                                displacement_vector);
            }
        }
        
        
        void VariationalFormulation::ComputeResidual()
        {
            switch(GetTimeManager().GetStaticOrDynamic())
            {
                case StaticOrDynamic::static_:
                    ComputeStaticResidual();
                    break;
                case StaticOrDynamic::dynamic_:
                    ComputeDynamicResidual();
                    break;
            }
        }
        
        
        void VariationalFormulation::ComputeStaticResidual()
        {
            const auto& displacement_numbering_subset = GetDisplacementNumberingSubset();
            auto& rhs = GetNonCstSystemRhs(displacement_numbering_subset);
            rhs.ZeroEntries(__FILE__, __LINE__);
            
            Wrappers::Petsc::AXPY(1.,
                                  GetVectorStiffnessResidual(),
                                  rhs,
                                  __FILE__, __LINE__);
            
            Wrappers::Petsc::AXPY(1.,
                                  GetVectorSurfacicForce(),
                                  rhs,
                                  __FILE__, __LINE__);
            
            ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_rhs>(displacement_numbering_subset,
                                                                                      displacement_numbering_subset);
        }
        
        
        void VariationalFormulation::ComputeDynamicResidual()
        {
            const auto& displacement_numbering_subset = GetDisplacementNumberingSubset();
            auto& rhs = GetNonCstSystemRhs(displacement_numbering_subset);
            rhs.ZeroEntries(__FILE__, __LINE__);
            
            Wrappers::Petsc::AXPY(1.,
                                  GetVectorStiffnessResidual(),
                                  rhs,
                                  __FILE__, __LINE__);
            
            const auto& time_manager = GetTimeManager();
            const double time_step = time_manager.GetTimeStep();
            const auto& displacement_previous_time_iteration = GetVectorCurrentDisplacement();
            const auto& velocity_previous_time_iteration = GetVectorCurrentVelocity();
            auto& diff_displacement = GetNonCstVectorDiffDisplacement();
            diff_displacement.Copy(GetVectorDisplacementAtNewtonIteration(), __FILE__, __LINE__);
            
            Wrappers::Petsc::AXPY(-1.,
                                  displacement_previous_time_iteration,
                                  diff_displacement,
                                  __FILE__, __LINE__);
            
            Wrappers::Petsc::AXPY(- time_step,
                                  velocity_previous_time_iteration,
                                  diff_displacement,
                                  __FILE__, __LINE__);
            
            Wrappers::Petsc::MatMultAdd(GetMatrixMassPerSquareTimeStep(),
                                        diff_displacement,
                                        rhs,
                                        rhs,
                                        __FILE__, __LINE__);
            
            ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_rhs>(displacement_numbering_subset,
                                                                                      displacement_numbering_subset);
        }
        
        
        PetscErrorCode VariationalFormulation::Jacobian(SNES snes,
                                                        Vec evaluation_state,
                                                        Mat jacobian,
                                                        Mat preconditioner,
                                                        void* context_as_void)
        {
            static_cast<void>(snes);
            static_cast<void>(jacobian);
            static_cast<void>(preconditioner);
            static_cast<void>(evaluation_state);
            
            // \a context_as_void is in fact the VariationalFormulation object.
            assert(context_as_void);
            
            VariationalFormulation* variational_formulation_ptr = static_cast<VariationalFormulation*>(context_as_void);
            
            assert(!(!variational_formulation_ptr));
            
            auto& variational_formulation = *variational_formulation_ptr;
            
            variational_formulation.ComputeTangent();
            
            return 0;
        }
        
        void VariationalFormulation::ComputeTangent()
        {
            switch(GetTimeManager().GetStaticOrDynamic())
            {
                case StaticOrDynamic::static_:
                    ComputeStaticTangent();
                    break;
                case StaticOrDynamic::dynamic_:
                    ComputeDynamicTangent();
                    break;
            }
        }
        
        void VariationalFormulation::ComputeStaticTangent()
        {
            const auto& displacement_numbering_subset = GetDisplacementNumberingSubset();
            auto& system_matrix = GetNonCstSystemMatrix(displacement_numbering_subset, displacement_numbering_subset);
            system_matrix.ZeroEntries(__FILE__, __LINE__);
            
            Wrappers::Petsc::AXPY<NonZeroPattern::same>(1.,
                                                        GetMatrixTangentStiffness(),
                                                        system_matrix, __FILE__, __LINE__);
            
            ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_matrix>(displacement_numbering_subset,
                                                                                         displacement_numbering_subset);
        }
        
        
        void VariationalFormulation::ComputeDynamicTangent()
        {
            const auto& displacement_numbering_subset = GetDisplacementNumberingSubset();
            auto& system_matrix = GetNonCstSystemMatrix(displacement_numbering_subset, displacement_numbering_subset);
            system_matrix.ZeroEntries(__FILE__, __LINE__);
            
#ifndef NDEBUG
                AssertSameNumberingSubset(GetMatrixTangentStiffness(), system_matrix);
                AssertSameNumberingSubset(GetMatrixMassPerSquareTimeStep(), system_matrix);
#endif // NDEBUG
                
            Wrappers::Petsc::AXPY<NonZeroPattern::same>(1.,
                                                        GetMatrixMassPerSquareTimeStep(),
                                                        system_matrix, __FILE__, __LINE__);
            
            Wrappers::Petsc::AXPY<NonZeroPattern::same>(0.5,
                                                        GetMatrixTangentStiffness(),
                                                        system_matrix, __FILE__, __LINE__);
            
            ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_matrix>(displacement_numbering_subset,
                                                                                         displacement_numbering_subset);
        }
        
        
        PetscErrorCode VariationalFormulation::Viewer(SNES snes,
                                                      PetscInt its,
                                                      PetscReal norm,
                                                      void* context_as_void)
        {
            static_cast<void>(snes);
            assert(context_as_void);
            
            const VariationalFormulation* variational_formulation_ptr =
            static_cast<const VariationalFormulation*>(context_as_void);
            
            assert(!(!variational_formulation_ptr));
            
            const auto& variational_formulation = *variational_formulation_ptr;
            
            const auto& displacement = variational_formulation.GetVectorDisplacementAtNewtonIteration();
            const auto& velocity = variational_formulation.GetVectorVelocityAtNewtonIteration();
            
            const PetscReal displacement_min = displacement.Min(__FILE__, __LINE__).second;
            const PetscReal displacement_max = displacement.Max(__FILE__, __LINE__).second;
            
            const PetscReal velocity_min = velocity.Min(__FILE__, __LINE__).second;
            const PetscReal velocity_max = velocity.Max(__FILE__, __LINE__).second;
            
            Wrappers::Petsc::PrintMessageOnFirstProcessor("%3D Residual norm: %1.12e Ymin: %8.6e Ymax: %8.6e Ypmin: %8.6e Ypmax: %8.6e\n",
                                                          variational_formulation.GetMpi(),
                                                          __FILE__, __LINE__,
                                                          its + 1,
                                                          norm,
                                                          displacement_min,
                                                          displacement_max,
                                                          velocity_min,
                                                          velocity_max
                                                          );
            
            return 0;
        }
        
        void VariationalFormulation::PrepareDynamicRuns()
        {
            DefineDynamicOperators();
            
            AssembleDynamicOperators();
            
            UpdateForNextTimeStep();
        }
        
        
        void VariationalFormulation::DefineDynamicOperators()
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& felt_space_volume = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume));
            
            const auto& displacement_ptr = UnknownManager::GetInstance(__FILE__, __LINE__).GetUnknownPtr(EnumUnderlyingType(UnknownIndex::displacement));
            
            namespace GVO = GlobalVariationalOperatorNS;
            
            mass_operator_ = std::make_unique<GVO::Mass>(felt_space_volume,
                                                         displacement_ptr,
                                                         displacement_ptr);
        }
        
        
        void VariationalFormulation::AssembleDynamicOperators()
        {
            const double volumic_mass = GetSolid().GetVolumicMass().GetConstantValue();
            
            const double time_step = GetTimeManager().GetTimeStep();
            
            const double mass_coefficient = 2. * volumic_mass / (time_step*time_step);
            
            GlobalMatrixWithCoefficient matrix(GetNonCstMatrixMassPerSquareTimeStep(),
                                               mass_coefficient);
            
            GetMassOperator().Assemble(std::make_tuple(std::ref(matrix)));
        }
        
        
        void VariationalFormulation::UpdateForNextTimeStep()
        {
            UpdateDisplacementBetweenTimeStep();
            
            UpdateVelocityBetweenTimeStep();
            
            //ComputeGuessForNextTimeStep();
        }
        
        
        void VariationalFormulation::UpdateDisplacementBetweenTimeStep()
        {
            GetNonCstVectorCurrentDisplacement().Copy(GetSystemSolution(GetDisplacementNumberingSubset()),
                                                      __FILE__, __LINE__);
            
            GetNonCstVectorCurrentDisplacement().UpdateGhosts(__FILE__, __LINE__);
        }
        
        
        void VariationalFormulation::UpdateVelocityBetweenTimeStep()
        {
            GetNonCstVectorCurrentVelocity().Copy(GetVectorVelocityAtNewtonIteration(),
                                                  __FILE__, __LINE__);
            
            GetNonCstVectorCurrentVelocity().UpdateGhosts(__FILE__, __LINE__);
        }
        
        
        void VariationalFormulation::ComputeGuessForNextTimeStep()
        {
            const auto& displacement_numbering_subset = GetDisplacementNumberingSubset();
            auto& system_solution = GetNonCstSystemSolution(displacement_numbering_subset);
            const auto& velocity_previous_time_iteration = GetVectorCurrentVelocity();
            
            Wrappers::Petsc::AXPY(GetTimeManager().GetTimeStep(), velocity_previous_time_iteration, system_solution, __FILE__, __LINE__);
            
            system_solution.UpdateGhosts(__FILE__, __LINE__);
        }
        
        
    } // namespace MidpointHyperelasticityNS


} // namespace MoReFEM
