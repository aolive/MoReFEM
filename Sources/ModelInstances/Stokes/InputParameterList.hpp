/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 14 Nov 2013 13:42:29 +0100
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_MODEL_INSTANCES_x_STOKES_x_INPUT_PARAMETER_LIST_HPP_
# define MOREFEM_x_MODEL_INSTANCES_x_STOKES_x_INPUT_PARAMETER_LIST_HPP_


# include "Core/InputParameterData/InputParameterList.hpp"
# include "Core/InputParameter/Geometry/Domain.hpp"
# include "Core/InputParameter/FElt/FEltSpace.hpp"
# include "Core/InputParameter/FElt/Unknown.hpp"
# include "Core/InputParameter/FElt/NumberingSubset.hpp"
# include "Core/InputParameter/Parameter/Source/VectorialTransientSource.hpp"
# include "Core/InputParameter/Parameter/Fluid/Fluid.hpp"
# include "Core/InputParameter/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
# include "Core/InputParameter/TimeManager/TimeManager.hpp"
# include "Core/InputParameter/Geometry/Mesh.hpp"
# include "Core/InputParameter/Solver/Petsc.hpp"

# include "FormulationSolver/Crtp/VolumicAndSurfacicSource.hpp"


namespace MoReFEM
{


    namespace StokesNS
    {


        //! \copydoc doxygen_hide_mesh_enum
            enum class MeshIndex
        {
            mesh = 1 // only one mesh considered in current model!
        };


        //! \copydoc doxygen_hide_domain_enum
            enum class DomainIndex
        {
            highest_dimension = 1,
            neumann = 2,
            dirichlet = 3,
            full_mesh
        };


        //! \copydoc doxygen_hide_felt_space_enum
            enum class FEltSpaceIndex
        {
            highest_dimension = 1,
            neumann = 2
        };


        //! \copydoc doxygen_hide_unknown_enum
            enum class UnknownIndex
        {
            velocity = 1,
            pressure = 2
        };


        //! \copydoc doxygen_hide_solver_enum
            enum class SolverIndex

        {
            solver = 1
        };


        //! \copydoc doxygen_hide_numbering_subset_enum
            enum class NumberingSubsetIndex
        {
            velocity_or_monolithic = 1,
            pressure_or_irrelevant = 2
        };


        //! \copydoc doxygen_hide_source_enum
        enum class SourceIndex
        {
            volumic = 1,
            surfacic = 2
        };


        //! \copydoc doxygen_hide_boundary_condition_enum
        enum class BoundaryConditionIndex
        {
            sole = 1
        };



        //! \copydoc doxygen_hide_input_parameter_tuple
        using InputParameterTuple = std::tuple
        <
            InputParameter::TimeManager,

            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::velocity_or_monolithic)>,
            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::pressure_or_irrelevant)>,

            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::velocity)>,
            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::pressure)>,

            InputParameter::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::sole)>,

            InputParameter::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            InputParameter::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::neumann)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::dirichlet)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,

            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>,
            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>,

            InputParameter::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputParameter::Fluid::Viscosity,

            InputParameter::VectorialTransientSource<EnumUnderlyingType(SourceIndex::volumic)>,
            InputParameter::VectorialTransientSource<EnumUnderlyingType(SourceIndex::surfacic)>,

            InputParameter::Result
        >;


        //! \copydoc doxygen_hide_morefem_data_type
        //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = InputParameterList<InputParameterTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputParameterList>;


    }; // namespace StokesNS


} // namespace MoReFEM


#endif // MOREFEM_x_MODEL_INSTANCES_x_STOKES_x_INPUT_PARAMETER_LIST_HPP_
