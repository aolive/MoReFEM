/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 17 Dec 2014 10:45:44 +0100
/// Copyright (c) Inria. All rights reserved.
///

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Core/NumberingSubset/Internal/NumberingSubsetManager.hpp"

#include "Geometry/Mesh/Internal/MeshManager.hpp"

#include "PostProcessing/OutputFormat/Ensight6.hpp"
#include "PostProcessing/PostProcessing.hpp"

#include "ModelInstances/Stokes/Model.hpp"
#include "ModelInstances/Stokes/InputParameterList.hpp"


using namespace MoReFEM;
using namespace MoReFEM::StokesNS;


int main(int argc, char** argv)
{
    
    try
    {
        //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = StokesNS::InputParameterList;
        
        MoReFEMData<InputParameterList, Utilities::InputParameterListNS::DoTrackUnusedFields::no> morefem_data(argc, argv);
        
        const auto& input_parameter_data = morefem_data.GetInputParameterList();
        const auto& mpi = morefem_data.GetMpi();
        
        try
        {
            namespace ipl = Utilities::InputParameterListNS;
            
            using Result = InputParameter::Result;
            decltype(auto) result_directory = ipl::Extract<Result::OutputDirectory>::Folder(input_parameter_data);
            
            if (!FilesystemNS::Folder::DoExist(result_directory))
                throw Exception("The specified directory doesn't exist!", __FILE__, __LINE__);
            
            
            auto& mesh_manager = Internal::MeshNS::MeshManager::CreateOrGetInstance(__FILE__, __LINE__);
            Advanced::SetFromInputParameterData<>(input_parameter_data, mesh_manager);
            
            const Mesh& mesh = mesh_manager.GetMesh(EnumUnderlyingType(MeshIndex::mesh));
            
            {
                auto& manager = Internal::NumberingSubsetNS::NumberingSubsetManager::CreateOrGetInstance(__FILE__, __LINE__);
                Advanced::SetFromInputParameterData<>(input_parameter_data, manager);
            }
            
            {
                std::vector<unsigned int> numbering_subset_id_list
                {
                    EnumUnderlyingType(NumberingSubsetIndex::velocity_or_monolithic),
                    EnumUnderlyingType(NumberingSubsetIndex::velocity_or_monolithic)
                };
                
                std::vector<std::string> unknown_list
                {
                    "velocity", "pressure"
                };
                
                PostProcessingNS::OutputFormat::Ensight6 ensight_output(result_directory,
                                                                        unknown_list,
                                                                        numbering_subset_id_list,
                                                                        mesh);
            }
            
            std::cout << "End of Post-Processing." << std::endl;
            std::cout << TimeKeep::GetInstance(__FILE__, __LINE__).TimeElapsedSinceBeginning() << std::endl;
            
        }
        catch(const std::exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());
        }
        catch(Seldon::Error& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.What());
        }
    }
    catch(const std::exception& e)
    {
        std::ostringstream oconv;
        oconv << "Exception caught from MoReFEMData<InputParameterList>: " << e.what() << std::endl;
        std::cout << oconv.str();
        return EXIT_FAILURE;
    }
    
    
    return EXIT_SUCCESS;
}

