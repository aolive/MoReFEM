/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 16:00:22 +0100
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_MODEL_INSTANCES_x_LAPLACIAN_x_MODEL_HPP_
# define MOREFEM_x_MODEL_INSTANCES_x_LAPLACIAN_x_MODEL_HPP_

# include <memory>
# include <vector>

# include "Model/Model.hpp"

# include "ModelInstances/Laplacian/InputParameterList.hpp"
# include "ModelInstances/Laplacian/VariationalFormulation.hpp"


namespace MoReFEM
{


    namespace LaplacianNS
    {


        //! \copydoc doxygen_hide_simple_model
        class Model final : public MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>
        {


        public:

            //! Return the name of the model.
            static const std::string& ClassName();

            //! Convenient alias to parent.
            using parent = MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>;

            //! Friendship granted to the base class so this one can manipulates private methods.
            friend parent;

        public:

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<Model>;

        public:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \copydoc doxygen_hide_morefem_data_param
             */
            explicit Model(const morefem_data_type& morefem_data);

            //! Destructor.
            ~Model() = default;

            //! Copy constructor.
            Model(const Model&) = delete;

            //! Move constructor.
            Model(Model&&) = delete;

            //! Copy affectation.
            Model& operator=(const Model&) = delete;

            //! Move affectation.
            Model& operator=(Model&&) = delete;

            ///@}


            /// \name Crtp-required methods.
            ///@{


            /*!
             * \brief Initialise the problem.
             *
             * This initialisation includes the resolution of the static problem.
             */
            void SupplInitialize();


            //! Manage time iteration.
            void Forward();

            /*!
             * \brief Additional operations to finalize a dynamic step.
             *
             * Base class already update the time for next time iterations.
             */
            void SupplFinalizeStep();


            /*!
             * \brief Initialise a dynamic step.
             *
             */
            void SupplFinalize() const;


        private:


            //! Non constant access to the underlying VariationalFormulation object.
            VariationalFormulation& GetNonCstVariationalFormulation();

            //! Access to the underlying VariationalFormulation object.
            const VariationalFormulation& GetVariationalFormulation() const;

        private:


            //! \copydoc doxygen_hide_model_SupplHasFinishedConditions_always_true
            bool SupplHasFinishedConditions() const;


            /*!
             * \brief Part of InitializedStep() specific to Elastic model.
             *
             * As there are none, the body of this method is empty.
             */
            void SupplInitializeStep();


            ///@}


        private:

            //! Underlying variational formulation.
            VariationalFormulation::unique_ptr variational_formulation_;

        };


    } // namespace LaplacianNS


} // namespace MoReFEM


# include "ModelInstances/Laplacian/Model.hxx"


#endif // MOREFEM_x_MODEL_INSTANCES_x_LAPLACIAN_x_MODEL_HPP_
