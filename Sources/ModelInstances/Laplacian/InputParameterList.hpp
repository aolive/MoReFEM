/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 14 Nov 2013 13:42:29 +0100
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_MODEL_INSTANCES_x_LAPLACIAN_x_INPUT_PARAMETER_LIST_HPP_
# define MOREFEM_x_MODEL_INSTANCES_x_LAPLACIAN_x_INPUT_PARAMETER_LIST_HPP_

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/InputParameterData/InputParameterList.hpp"
# include "Core/InputParameter/Geometry/Domain.hpp"
# include "Core/InputParameter/FElt/FEltSpace.hpp"
# include "Core/InputParameter/FElt/Unknown.hpp"
# include "Core/InputParameter/FElt/NumberingSubset.hpp"
# include "Core/InputParameter/Parameter/Source/ScalarTransientSource.hpp"
# include "Core/InputParameter/InitialCondition/InitialCondition.hpp"
# include "Core/InputParameter/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
# include "Core/InputParameter/TimeManager/TimeManager.hpp"
# include "Core/InputParameter/Geometry/Mesh.hpp"
# include "Core/InputParameter/Solver/Petsc.hpp"


namespace MoReFEM
{


    namespace LaplacianNS
    {


        //! \copydoc doxygen_hide_mesh_enum
            enum class MeshIndex
        {
            mesh = 1 // only one mesh considered in current model!
        };

        //! \copydoc doxygen_hide_numbering_subset_enum
            enum class NumberingSubsetIndex
        {
            monolithic = 1
        };

        //! \copydoc doxygen_hide_unknown_enum
            enum class UnknownIndex
        {
            pressure = 1
        };


        //! \copydoc doxygen_hide_domain_enum
            enum class DomainIndex
        {
            full_mesh = 1,
            volume = 2,
            dirichlet = 3
        };


        //! \copydoc doxygen_hide_boundary_condition_enum
        enum class BoundaryConditionIndex
        {
            first = 1
        };


        //! \copydoc doxygen_hide_felt_space_enum
            enum class FEltSpaceIndex
        {
            volume = 1
        };

        //! \copydoc doxygen_hide_solver_enum
            enum class SolverIndex

        {
            solver = 1
        };

        //! \copydoc doxygen_hide_source_enum
        enum class SourceIndexList : unsigned int
        {
            volumic_source = 1
        };


        //! \copydoc doxygen_hide_initial_condition_enum
        enum class InitialConditionIndex
        {
            initial_condition = 1
        };

        //! \copydoc doxygen_hide_input_parameter_tuple
        using InputParameterTuple = std::tuple
        <
            InputParameter::TimeManager,

            InputParameter::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic)>,

            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::pressure)>,

            InputParameter::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::volume)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::dirichlet)>,

            InputParameter::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::first)>,

            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume)>,

            InputParameter::ScalarTransientSource<EnumUnderlyingType(SourceIndexList::volumic_source)>,

            InputParameter::InitialCondition<EnumUnderlyingType(InitialConditionIndex::initial_condition)>,

            InputParameter::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputParameter::Result

        >;


        //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = InputParameterList<InputParameterTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputParameterList>;


    } // namespace LaplacianNS


} // namespace MoReFEM


#endif // MOREFEM_x_MODEL_INSTANCES_x_LAPLACIAN_x_INPUT_PARAMETER_LIST_HPP_
