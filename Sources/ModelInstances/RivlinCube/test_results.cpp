

#include "Utilities/Filesystem/File.hpp"

#include "ModelInstances/RivlinCube/InputParameterList.hpp"

using namespace MoReFEM;

#include "Test/Tools/CatchMainTest.hpp"
#include "Test/Tools/CheckIdenticalFiles.hpp"
#include "Test/Tools/CompareEnsightFiles.hpp"


void CommonTestCase(std::string&& seq_or_par,
                    std::string&& geometry);


void CommonTestCase(std::string&& seq_or_par,
                    std::string&& geometry)
{
    decltype(auto) environment = Utilities::Environment::GetInstance(__FILE__, __LINE__);
    std::string root_dir, output_dir;

    REQUIRE_NOTHROW(root_dir = environment.GetEnvironmentVariable("MOREFEM_ROOT", __FILE__, __LINE__));
    REQUIRE_NOTHROW(output_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__));

    REQUIRE(FilesystemNS::Folder::DoExist(root_dir));
    REQUIRE(FilesystemNS::Folder::DoExist(output_dir));

    std::string ref_dir = root_dir + "/Sources/ModelInstances/RivlinCube/ExpectedResults/" + geometry;
    std::string obtained_dir = output_dir + std::string("/") + seq_or_par + std::string("/RivlinCube/") + geometry;

    TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "input_data.lua", __FILE__, __LINE__);
    TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "model_name.hhdata", __FILE__, __LINE__);
    TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "unknowns.hhdata", __FILE__, __LINE__);

    ref_dir += "/Mesh_1/";
    obtained_dir += "/Mesh_1/";

    TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "interfaces.hhdata", __FILE__, __LINE__);

    ref_dir += "Ensight6/";
    obtained_dir += "Ensight6/";

    TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "mesh.geo", __FILE__, __LINE__);
    TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "problem.case", __FILE__, __LINE__);

    TestNS::CompareEnsightFiles(ref_dir, obtained_dir, "solid_displacement.00000.scl", __FILE__, __LINE__);

}
                         


TEST_CASE("RivlinCube output is conform to what is expected - Hexahedra case, sequential")
{
    CommonTestCase("Seq", "Hexahedra");
}


TEST_CASE("RivlinCube output is conform to what is expected - Hexahedra case, mpi 4 processors")
{
    CommonTestCase("Mpi4", "Hexahedra");
}


TEST_CASE("RivlinCube output is conform to what is expected - Tetrahedra case, sequential")
{
    CommonTestCase("Seq", "Tetrahedra");
}


TEST_CASE("RivlinCube output is conform to what is expected - Tetrahedra case, mpi 4 processors")
{
    CommonTestCase("Mpi4", "Tetrahedra");
}
