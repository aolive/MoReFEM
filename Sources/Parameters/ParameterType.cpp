///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Oct 2016 14:15:02 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParametersGroup
/// \addtogroup ParametersGroup
/// \{

# include "Parameters/ParameterType.hpp"


namespace MoReFEM
{


    namespace ParameterNS
    {
        
        
        namespace // anonymous
        {
            
            
            LocalVector InitLocalVector(unsigned int Nrow, unsigned int);
            
            LocalMatrix InitLocalMatrix(unsigned int Nrow, unsigned int Ncol);
            
            
        } // namespace anonymous

        
        Traits<Type::vector>::return_type Traits<Type::vector>
        ::AllocateDefaultValue(unsigned int Nrow, unsigned int ) noexcept
        {
            static auto ret = InitLocalVector(Nrow, 0u);
            return ret;
        }
        
        
        Traits<Type::matrix>::return_type Traits<Type::matrix>
        ::AllocateDefaultValue(unsigned int Nrow, unsigned int Ncol) noexcept
        {
            static auto ret = InitLocalMatrix(Nrow, Ncol);
            return ret;
        }

        
        
        namespace // anonymous
        {
            
            
            LocalVector InitLocalVector(unsigned int Nrow, unsigned int)
            {
                LocalVector ret(static_cast<int>(Nrow));
                ret.Zero();
                return ret;
            }
            
            
            LocalMatrix InitLocalMatrix(unsigned int Nrow, unsigned int Ncol)
            {
                LocalMatrix ret(static_cast<int>(Nrow), static_cast<int>(Ncol));
                ret.Zero();
                return ret;
            }
            
            
            
        } // namespace anonymous

     

    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup

