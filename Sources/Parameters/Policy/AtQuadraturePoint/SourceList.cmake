target_sources(${MOREFEM_PARAM}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/AtQuadraturePoint.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/AtQuadraturePoint.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
