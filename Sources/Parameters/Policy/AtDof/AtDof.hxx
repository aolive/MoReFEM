///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 2 Oct 2015 11:57:13 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParametersGroup
/// \addtogroup ParametersGroup
/// \{

#ifndef MOREFEM_x_PARAMETERS_x_POLICY_x_AT_DOF_x_AT_DOF_HXX_
# define MOREFEM_x_PARAMETERS_x_POLICY_x_AT_DOF_x_AT_DOF_HXX_


namespace MoReFEM
{


    namespace ParameterNS
    {


        namespace Policy
        {


            template
            <
                ParameterNS::Type TypeT,
                unsigned int Ndim
            >
            AtDof<TypeT, Ndim>::AtDof(const Domain& domain,
                                      const FEltSpace& felt_space,
                                      const Unknown& unknown,
                                      const GlobalVector& global_vector)
            : felt_space_storage_(felt_space),
            unknown_(unknown),
            global_vector_(global_vector)
            {
                static_assert(Ndim == 1, "This specific constructor should be called only in this case!");

                Construct(domain, felt_space, unknown, global_vector);
            }


            template
            <
                ParameterNS::Type TypeT,
                unsigned int Ndim
            >
            AtDof<TypeT, Ndim>::AtDof(const Domain& domain,
                                      const FEltSpace& felt_space_dim,
                                      const FEltSpace& felt_space_dim_minus_1,
                                      const Unknown& unknown,
                                      const GlobalVector& global_vector)
            : felt_space_storage_(felt_space_dim, felt_space_dim_minus_1),
            unknown_(unknown),
            global_vector_(global_vector)
            {
                static_assert(Ndim == 2, "This specific constructor should be called only in this case!");

                Construct(domain, felt_space_dim, unknown, global_vector);

                assert(felt_space_dim_minus_1.GetGodOfDofFromWeakPtr()->GetUniqueId() == domain.GetMeshIdentifier());
                assert(felt_space_dim_minus_1.DoCoverNumberingSubset(global_vector.GetNumberingSubset()));
            }


            template
            <
                ParameterNS::Type TypeT,
                unsigned int Ndim
            >
            AtDof<TypeT, Ndim>::AtDof(const Domain& domain,
                                      const FEltSpace& felt_space_dim,
                                      const FEltSpace& felt_space_dim_minus_1,
                                      const FEltSpace& felt_space_dim_minus_2,
                                      const Unknown& unknown,
                                      const GlobalVector& global_vector)
            : felt_space_storage_(felt_space_dim, felt_space_dim_minus_1, felt_space_dim_minus_2),
            unknown_(unknown),
            global_vector_(global_vector)
            {
                static_assert(Ndim == 3, "This specific constructor should be called only in this case!");

                Construct(domain, felt_space_dim, unknown, global_vector);

                assert(felt_space_dim_minus_1.GetGodOfDofFromWeakPtr()->GetUniqueId() == domain.GetMeshIdentifier());
                assert(felt_space_dim_minus_1.DoCoverNumberingSubset(global_vector.GetNumberingSubset()));
                assert(felt_space_dim_minus_2.GetGodOfDofFromWeakPtr()->GetUniqueId() == domain.GetMeshIdentifier());
                assert(felt_space_dim_minus_2.DoCoverNumberingSubset(global_vector.GetNumberingSubset()));
            }


            template
            <
                ParameterNS::Type TypeT,
                unsigned int Ndim
            >
            typename AtDof<TypeT, Ndim>::return_type AtDof<TypeT, Ndim>
            ::GetValueFromPolicy(const local_coords_type& local_coords,
                                 const GeometricElt& geom_elt) const
            {
                const auto& felt_space = GetFEltSpace(geom_elt);

                const auto& ref_geom_elt = geom_elt.GetRefGeomElt();
                const auto& ref_felt_space = felt_space.GetRefLocalFEltSpace(ref_geom_elt);

                const auto& local_felt_space = felt_space.GetLocalFEltSpace(geom_elt);

                const auto& unknown = GetUnknown();

                const auto& felt = local_felt_space.GetFElt(unknown);

                const auto& node_list = felt.GetNodeList();

                const auto& global_vector = GetGlobalVector();
                const auto& numbering_subset = global_vector.GetNumberingSubset();

                Wrappers::Petsc::AccessGhostContent access_ghost_content(global_vector, __FILE__, __LINE__);

                const auto& vector_with_ghost = access_ghost_content.GetVectorWithGhost();

                Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only>
                vector_with_ghost_content(vector_with_ghost, __FILE__, __LINE__);

                const auto& basic_ref_elt = ref_felt_space.GetRefFElt(felt_space, unknown).GetBasicRefFElt();

                Internal::ParameterNS::Policy::AtDofNS::ComputeLocalValue(basic_ref_elt,
                                                                          local_coords,
                                                                          node_list,
                                                                          numbering_subset,
                                                                          vector_with_ghost_content,
                                                                          local_value_);

                return local_value_;
            }


            template
            <
                ParameterNS::Type TypeT,
                unsigned int Ndim
            >
            typename AtDof<TypeT, Ndim>::return_type AtDof<TypeT, Ndim>
            ::GetAnyValueFromPolicy() const
            {
                return local_value_;
            }


            template
            <
                ParameterNS::Type TypeT,
                unsigned int Ndim
            >
            void AtDof<TypeT, Ndim>::WriteFromPolicy(std::ostream& out) const
            {
                out << "AtDof<TypeT, Ndim>::WriteFromPolicy() is not activated at the moment; you can however ask instead "
                << "for GetGlobalVector() and then use one of its method to see its content." << std::endl;
            }


            template
            <
                ParameterNS::Type TypeT,
                unsigned int Ndim
            >
            [[noreturn]] typename AtDof<TypeT, Ndim>::return_type
            AtDof<TypeT, Ndim>::GetConstantValueFromPolicy() const
            {
                assert(false && "Should yield IsConstant() = false!");
                exit(-1);
            }


            template
            <
                ParameterNS::Type TypeT,
                unsigned int Ndim
            >
            inline bool AtDof<TypeT, Ndim>::IsConstant() const noexcept
            {
                return false;
            }


            template
            <
                ParameterNS::Type TypeT,
                unsigned int Ndim
            >
            inline const GlobalVector& AtDof<TypeT, Ndim>::GetGlobalVector() const noexcept
            {
                return global_vector_;
            }


            template
            <
                ParameterNS::Type TypeT,
                unsigned int Ndim
            >
            inline const Unknown& AtDof<TypeT, Ndim>::GetUnknown() const noexcept
            {
                return unknown_;
            }


            template
            <
                ParameterNS::Type TypeT,
                unsigned int Ndim
            >
            inline const FEltSpace& AtDof<TypeT, Ndim>::GetFEltSpace(const GeometricElt& geom_elt) const noexcept
            {
                return felt_space_storage_.GetFEltSpace(geom_elt);
            }


            template
            <
                ParameterNS::Type TypeT,
                unsigned int Ndim
            >
            void AtDof<TypeT, Ndim>::Construct(const Domain& domain,
                                               const FEltSpace& first_felt_space,
                                               const Unknown& unknown,
                                               const GlobalVector& global_vector)
            {
                static_cast<void>(first_felt_space);
                static_cast<void>(unknown);
                static_cast<void>(global_vector);

                assert(first_felt_space.DoCoverNumberingSubset(global_vector.GetNumberingSubset()));
                assert(first_felt_space.GetGodOfDofFromWeakPtr()->GetUniqueId() == domain.GetMeshIdentifier());

                #ifndef NDEBUG
                {
                    if (TypeT == ParameterNS::Type::scalar && unknown.GetNature() == UnknownNS::Nature::vectorial)
                        assert(false && "A scalar parameter given at dof can only be matched with a scalar Unknown.");
                }
                #endif // NDEBUG

                decltype(auto) mesh = domain.GetMesh();

                assert(first_felt_space.GetMeshDimension() == mesh.GetDimension());
                Internal::ParameterNS::Policy::AtDofNS::InitLocalValue(mesh.GetDimension(), local_value_);
            }


            template
            <
                ParameterNS::Type TypeT,
                unsigned int Ndim
            >
            const Internal::ParameterNS::Policy::AtDofNS::FEltSpaceStorage<Ndim>&
            AtDof<TypeT, Ndim>::GetFEltSpaceStorage() const noexcept
            {
                return felt_space_storage_;
            }


            template
            <
                ParameterNS::Type TypeT,
                unsigned int Ndim
            >
            constexpr unsigned int AtDof<TypeT, Ndim>::NfeltSpace() noexcept
            {
                return Ndim;
            }

            template
            <
                ParameterNS::Type TypeT,
                unsigned int Ndim
            >
            inline void AtDof<TypeT, Ndim>::SetConstantValue(double value)
            {
                static_cast<void>(value);
                assert(false && "SetConstantValue() meaningless for current Parameter.");
                exit(EXIT_FAILURE);
            }


        } // namespace Policy


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_POLICY_x_AT_DOF_x_AT_DOF_HXX_
