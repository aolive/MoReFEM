///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 May 2015 14:55:40 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParametersGroup
/// \addtogroup ParametersGroup
/// \{

#ifndef MOREFEM_x_PARAMETERS_x_POLICY_x_LUA_FUNCTION_x_LUA_FUNCTION_HPP_
# define MOREFEM_x_PARAMETERS_x_POLICY_x_LUA_FUNCTION_x_LUA_FUNCTION_HPP_

# include <memory>
# include <vector>

# include "Utilities/InputParameterList/LuaFunction.hpp"
# include "Utilities/InputParameterList/LuaFunction.hpp"

# include "Core/InputParameter/Parameter/SpatialFunction.hpp"

# include "Geometry/Coords/Coords.hpp"
# include "Geometry/GeometricElt/Advanced/FreeFunctions.hpp"

# include "Parameters/ParameterType.hpp"


namespace MoReFEM
{


    namespace ParameterNS
    {


        namespace Policy
        {


            /*!
             * \brief Parameter policy when the parameter gets a value given by an analytic function provided in the
             * input parameter file.
             *
             * \tparam TypeT  Type of the parameter (real, vector, matrix).
             * \tparam SettingsSpatialFunctionT An instantiation of InputParameter::SpatialFunction<> template class.
             *
             * \internal <b><tt>[internal]</tt></b> An enum or a template template parameter would be more elegant for SettingsSpatialFunctionT,
             * but it is unfortunately not possible, as the policy is passed through variadic arguments, and variadic
             * arguments can't mix type and non-type parameters.
             */
            template<ParameterNS::Type TypeT, class SettingsSpatialFunctionT>
            class LuaFunction
            {

            public:

                //! \copydoc doxygen_hide_alias_self
                using self = LuaFunction<TypeT, SettingsSpatialFunctionT>;

                //! \copydoc doxygen_hide_parameter_local_coords_type
                using local_coords_type = LocalCoords;

            private:

                //! Alias to traits class related to TypeT.
                using traits = Traits<TypeT>;

            public:

                //! Alias to return type.
                using return_type = typename traits::return_type;

                //! Alias to the storage of the Lua function.
                using storage_type = const typename SettingsSpatialFunctionT::return_type&;

            public:

                /// \name Special members.
                ///@{

                //! Constructor.
                explicit LuaFunction(const Domain& domain,
                                     storage_type lua_function);

                //! Destructor.
                ~LuaFunction() = default;

                //! Copy constructor.
                LuaFunction(const LuaFunction&) = delete;

                //! Move constructor.
                LuaFunction(LuaFunction&&) = delete;

                //! Copy affectation.
                LuaFunction& operator=(const LuaFunction&) = delete;

                //! Move affectation.
                LuaFunction& operator=(LuaFunction&&) = delete;

                ///@}

            public:

                /*!
                 * \brief Enables to modify the constant value of a parameter. Disabled for this Policy.
                 */
                void SetConstantValue(double value);

            protected:

                /*!
                 * \brief Provided here to make the code compile, but should never be called
                 *
                 * (Constant value should not be given through a function: it is less efficient and the constness
                 * is not appearant in the code...).
                 *
                 * \return Irrelevant here.
                 */
                [[noreturn]] return_type GetConstantValueFromPolicy() const;

                //! Return the value for \a quadrature_point and \a geom_elt.
                return_type GetValueFromPolicy(const local_coords_type& local_coords,
                                               const GeometricElt& geom_elt) const;


                //! \copydoc doxygen_hide_parameter_suppl_get_any_value
                return_type GetAnyValueFromPolicy() const;

            protected:

                //! Whether the parameter varies spatially or not.
                bool IsConstant() const;

                //! Write the content of the parameter for which policy is used in a stream.
                void  WriteFromPolicy(std::ostream& out) const;

            private:

                /*!
                 * \brief Non constant accessor to the helper Coords object, useful when Lua function acts upon global
                 * elements.
                 *
                 * \internal <b><tt>[internal]</tt></b> This is const because the helper coords is mutable to be
                 * used in const methods.
                 *
                 * \return Reference to the helper Coords object.
                 */
                SpatialPoint& GetNonCstWorkCoords() const noexcept;

            private:

                //! Store a reference to the Lua function (which is owned by the InputParameterList class).
                storage_type lua_function_;

                //! Helper Coords object, useful when Lua function acts upon global elements.
                mutable SpatialPoint work_coords_;

            };


        } // namespace Policy


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


# include "Parameters/Policy/LuaFunction/LuaFunction.hxx"


#endif // MOREFEM_x_PARAMETERS_x_POLICY_x_LUA_FUNCTION_x_LUA_FUNCTION_HPP_
