///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 29 Mar 2016 10:53:35 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParametersGroup
/// \addtogroup ParametersGroup
/// \{

#ifndef MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_TIME_DEPENDENCY_HXX_
# define MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_TIME_DEPENDENCY_HXX_


namespace MoReFEM
{


    namespace ParameterNS
    {


        namespace TimeDependencyNS
        {


            template
            <
                ParameterNS::Type TypeT,
                class TimeDependencyPolicyT
            >
            template<typename... Args>
            Base<TypeT, TimeDependencyPolicyT>::Base(const TimeManager& time_manager,
                                                 Args&&... args)
            : TimeDependencyPolicyT(std::forward<Args>(args)...),
            time_manager_(time_manager)
            {
                const auto& dependency_policy_ptr = static_cast<TimeDependencyPolicyT*>(this);
                auto& dependency_policy = *dependency_policy_ptr;
                current_time_factor_ = dependency_policy.GetCurrentTimeFactor(time_manager.GetTime());
            }


            template
            <
                ParameterNS::Type TypeT,
                class TimeDependencyPolicyT
            >
            void Base<TypeT, TimeDependencyPolicyT>::Init(return_type any_value)
            {
                Internal::ParameterNS::TimeDependencyNS::InitStoredQuantity<TypeT>::Perform(any_value, GetNonCstValueWithTimeFactor());
            }


            template
            <
                ParameterNS::Type TypeT,
                class TimeDependencyPolicyT
            >
            typename Base<TypeT, TimeDependencyPolicyT>::return_type Base<TypeT, TimeDependencyPolicyT>
            ::ApplyTimeFactor(return_type value_without_time_factor) const
            {
                auto& ret = GetNonCstValueWithTimeFactor();

                Internal::ParameterNS::TimeDependencyNS::ApplyTimeFactor(value_without_time_factor,
                                                                         GetCurrentTimeFactor(),
                                                                         ret);

                return ret;
            }


            template
            <
                ParameterNS::Type TypeT,
                class TimeDependencyPolicyT
            >
            inline const TimeManager& Base<TypeT, TimeDependencyPolicyT>::GetTimeManager() const noexcept
            {
                return time_manager_;
            }


            template
            <
                ParameterNS::Type TypeT,
                class TimeDependencyPolicyT
            >
            void Base<TypeT, TimeDependencyPolicyT>::Update()
            {
                decltype(auto) time_manager = GetTimeManager();

                UpdateNtimesModifiedAtLastUpdate();

                const auto& dependency_policy_ptr = static_cast<TimeDependencyPolicyT*>(this);
                auto& dependency_policy = *dependency_policy_ptr;

                current_time_factor_ = dependency_policy.GetCurrentTimeFactor(time_manager.GetTime());
            }


            template
            <
            ParameterNS::Type TypeT,
            class TimeDependencyPolicyT
            >
            void Base<TypeT, TimeDependencyPolicyT>::Update(double time)
            {
                UpdateNtimesModifiedAtLastUpdate();

                const auto& dependency_policy_ptr = static_cast<TimeDependencyPolicyT*>(this);
                auto& dependency_policy = *dependency_policy_ptr;

                current_time_factor_ = dependency_policy.GetCurrentTimeFactor(time);
            }


            template
            <
                ParameterNS::Type TypeT,
                class TimeDependencyPolicyT
            >
            void Base<TypeT, TimeDependencyPolicyT>::UpdateNtimesModifiedAtLastUpdate()
            {
                Ntimes_modified_at_last_update_ = GetTimeManager().NtimeModified();
            }


            template
            <
                ParameterNS::Type TypeT,
                class TimeDependencyPolicyT
            >
            unsigned int Base<TypeT, TimeDependencyPolicyT>::GetNtimesModifiedAtLastUpdate() const noexcept
            {
                return Ntimes_modified_at_last_update_;
            }


            template
            <
                ParameterNS::Type TypeT,
                class TimeDependencyPolicyT
            >
            inline double Base<TypeT, TimeDependencyPolicyT>::GetCurrentTimeFactor() const noexcept
            {
                # ifndef NDEBUG
                {
                    const auto lhs = GetNtimesModifiedAtLastUpdate();
                    const auto rhs = GetTimeManager().NtimeModified();

                    assert(lhs == rhs
                           && "Check parameter is correctly up-to-date! (it is likely a call to Parameter::");
                }
                # endif // NDEBUG
                return current_time_factor_;
            }


            template
            <
                ParameterNS::Type TypeT,
                class TimeDependencyPolicyT
            >
            inline typename  Base<TypeT, TimeDependencyPolicyT>::storage_type& Base<TypeT, TimeDependencyPolicyT>
            ::GetNonCstValueWithTimeFactor() const noexcept
            {
                return value_with_time_factor_;
            }



        } // namespace TimeDependencyNS


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_TIME_DEPENDENCY_HXX_
