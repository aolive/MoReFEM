///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 31 Mar 2016 15:36:13 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParametersGroup
/// \addtogroup ParametersGroup
/// \{

#include "Parameters/TimeDependency/Internal/InitStoredQuantity.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace ParameterNS
        {
            
            
            namespace TimeDependencyNS
            {
             
                
                void InitStoredQuantity<Type::scalar>::Perform(double any_value,
                                                               double& stored_value)
                {
                    static_cast<void>(any_value);
                    static_cast<void>(stored_value);
                }
                
                
                void InitStoredQuantity<Type::vector>::Perform(const LocalVector& any_value,
                                                               LocalVector& stored_value)
                {
                    stored_value.Resize(any_value.GetM());
                }
                    

                void InitStoredQuantity<Type::matrix>::Perform(const LocalMatrix& any_value,
                                                               LocalMatrix& stored_value)
                {
                    stored_value.Resize(any_value.GetM(), any_value.GetN());
                }
                
                
            } // namespace TimeDependencyNS
        
        
        } // namespace ParameterNS
        
        
    } // namespace Internal

  
} // namespace MoReFEM


/// @} // addtogroup ParametersGroup
