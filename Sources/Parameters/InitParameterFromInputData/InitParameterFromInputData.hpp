///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 May 2015 11:30:57 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParametersGroup
/// \addtogroup ParametersGroup
/// \{

#ifndef MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INIT_PARAMETER_FROM_INPUT_DATA_HPP_
# define MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INIT_PARAMETER_FROM_INPUT_DATA_HPP_

# include "Core/InputParameterData/InputParameterList.hpp"

# include "Parameters/InitParameterFromInputData/Internal/InitParameterFromInputData.hpp"
# include "Parameters/Policy/Constant/Constant.hpp"
# include "Parameters/Policy/LuaFunction/LuaFunction.hpp"
# include "Parameters/Policy/AtQuadraturePoint/AtQuadraturePoint.hpp"
# include "Parameters/Policy/PiecewiseConstantByDomain/PiecewiseConstantByDomain.hpp"
# include "ParameterInstances/ThreeDimensionalParameter/ThreeDimensionalParameter.hpp"
# include "Parameters/TimeDependency/None.hpp"


namespace MoReFEM
{


    /*!
     * \brief Initialize properly a Parameter from the informations given in the input file.
     *
     * Typically, a parameter is defined like:
     * \code
     * YoungModulus = {
     * nature = "lua_function",
     * scalar_value = 10.,
     * lua_function = function (x, y, z)
     * return x + y * 10. + z * 100.
     *  end
     * }
     * \endcode
     *
     * The current function will build the parameter from the nature chosen in the input file; lines matching other
     * values (such as the scalar value in the example above) are not used.
     *
     * \tparam ParameterT Which parameter is to be built. It is a member of the namespace InputParameter;
     * for instance for the Young Modulus example above it is InputParameter::YoungModulus.
     * \tparam InputParameterDataT Type of the input parameter file for the considered model instance.
     *
     * \param[in] name Name of the parameter, as it will appear in outputs.
     * \copydoc doxygen_hide_input_parameter_data_arg
     * \copydoc doxygen_hide_parameter_domain_arg
     *
     * \attention Please notice all \a Parameter can't be initialized from the input parameter file. For instance a
     * parameter defined directly at quadrature points or at dofs must be built in hard in the code; current function
     * enables to choose among certain pre-definite choices (at the time of this writing, 'constant', 'lua function' and
     * 'piecewise constant by domain').
     *
     * \return Parameter considered.
     */
    template
    <
        class ParameterT,
        template<ParameterNS::Type> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
        class StringT,
        class InputParameterDataT
    >
    typename ScalarParameter<TimeDependencyT>::unique_ptr
        InitScalarParameterFromInputData(StringT&& name,
                                         const Domain& domain,
                                         const InputParameterDataT& input_parameter_data);


    /*!
     * \brief Init a three dimensional parameter, e.g. a source.
     *
     * This is handled actually as three separate \a ScalarParameter, which nature may change: one might be defined
     * by a Lua function and another one by a constant.
     *
     * \param[in] name Name of the parameter, as it will appear in outputs.
     * \copydoc doxygen_hide_input_parameter_data_arg
     * \copydoc doxygen_hide_parameter_domain_arg
     *
     * \return Parameter considered.
     */
    template
    <
        class ParameterT,
        template<ParameterNS::Type> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
        class StringT,
        class InputParameterDataT
    >
    typename Parameter<ParameterNS::Type::vector, LocalCoords, TimeDependencyT>::unique_ptr
    InitThreeDimensionalParameterFromInputData(StringT&& name,
                                               const Domain& domain,
                                               const InputParameterDataT& input_parameter_data);


    /*!
     * \brief Usual dummy struct to mimic partial template specialization for template functions.
     *
     * \tparam TypeT Whether parameter is scalar or vectorial (no specialization given for matricial).
     */
    template<ParameterNS::Type TypeT>
    struct InitParameterFromInputData
    {

        /*!
         * \brief Init a scalar or vectorial dimensional parameter, depending on \a TypeT.
         *
         * This function actually calls one of the two functions above.
         *
         * \param[in] name Name of the parameter, as it will appear in outputs.
         * \copydoc doxygen_hide_input_parameter_data_arg
         * \copydoc doxygen_hide_parameter_domain_arg
         *
         * \return Parameter considered.
         */
        template
        <
            class ParameterT,
            template<ParameterNS::Type> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
            class StringT,
            class InputParameterDataT
        >
        static typename Parameter<TypeT, LocalCoords, TimeDependencyT>::unique_ptr
        Perform(StringT&& name,
                const Domain& domain,
                const InputParameterDataT& input_parameter_data);


    };



} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


# include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hxx"


#endif // MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INIT_PARAMETER_FROM_INPUT_DATA_HPP_
