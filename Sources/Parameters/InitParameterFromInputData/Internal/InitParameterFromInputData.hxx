///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 12 Oct 2016 13:41:06 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParametersGroup
/// \addtogroup ParametersGroup
/// \{

#ifndef MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INTERNAL_x_INIT_PARAMETER_FROM_INPUT_DATA_HXX_
# define MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INTERNAL_x_INIT_PARAMETER_FROM_INPUT_DATA_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {


            template
            <
                class LuaFieldT,
                template<ParameterNS::Type> class TimeDependencyT,
                class T,
                class InputParameterDataT
            >
            typename ScalarParameter<TimeDependencyT>::unique_ptr
            InitScalarParameterFromInputData(T&& name,
                                             const Domain& domain,
                                             const InputParameterDataT& input_parameter_data,
                                             const std::string& nature,
                                             const double scalar_value,
                                             const std::vector<unsigned int>& domain_id,
                                             const std::vector<double>& value_by_domain)
            {
                namespace IPL = Utilities::InputParameterListNS;

                if (nature == "ignore")
                    return nullptr;

                if (nature == "constant")
                {

                    using parameter_type = Internal::ParameterNS::ParameterInstance
                    <
                        ParameterNS::Type::scalar,
                        ::MoReFEM::ParameterNS::Policy::Constant,
                        TimeDependencyT
                    >;

                    return std::make_unique<parameter_type>(std::forward<T>(name),
                                                            domain,
                                                            scalar_value);
                }
                else if (nature == "lua_function")
                {
                    using parameter_type = Internal::ParameterNS::ParameterInstance
                    <
                        ParameterNS::Type::scalar,
                        ::MoReFEM::ParameterNS::Policy::LuaFunction,
                        TimeDependencyT,
                        LuaFieldT
                    >;

                    decltype(auto) value =
                        IPL::Extract<LuaFieldT>::Value(input_parameter_data);

                    return std::make_unique<parameter_type>(std::forward<T>(name),
                                                            domain,
                                                            value);
                }
                else if (nature == "piecewise_constant_by_domain")
                {
                    using parameter_type = Internal::ParameterNS::ParameterInstance
                    <
                        ParameterNS::Type::scalar,
                        ::MoReFEM::ParameterNS::Policy::PiecewiseConstantByDomain,
                        TimeDependencyT
                    >;

                    return std::make_unique<parameter_type>(std::forward<T>(name),
                                                            domain,
                                                            domain_id,
                                                            value_by_domain);
                }
                else
                {
                    assert(false && "Should not happen: all the possible choices are assumed to be checked by "
                           "LuaOptionFile Constraints.");
                    exit(EXIT_FAILURE);
                }

                return nullptr;
            }


        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INTERNAL_x_INIT_PARAMETER_FROM_INPUT_DATA_HXX_
