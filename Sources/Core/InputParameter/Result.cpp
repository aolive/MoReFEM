///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 19 Feb 2015 14:38:32 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#include "Utilities/String/EmptyString.hpp"

#include "Core/InputParameter/Result.hpp"


namespace MoReFEM
{
    
    
    namespace InputParameter
    {
        
        
        const std::string& Result::GetName()
        {
            static std::string ret("Result");
            return ret;
        }
        
            
        const std::string& Result::OutputDirectory::NameInFile()
        {
            static std::string ret("output_directory");
            return ret;
        }
        
        
        const std::string& Result::OutputDirectory::Description()
        {
            static std::string ret("Directory in which all the results will be written. This path may use the "
                                   "environment variable MOREFEM_RESULT_DIR, which is either provided in user's "
                                   "environment or automatically set to '/Volumes/Data/${USER}/MoReFEM/Results' "
                                   "in MoReFEM initialization step. Please do not read this value directly: it "
                                   "might have been extended in MoReFEMData class! Rather call the GetResultDirectory() "
                                   "from this class."
                                   );
            return ret;
        }
        
        
        const std::string& Result::OutputDirectory::Constraint()
        {
            return Utilities::EmptyString();
        }
        
        
        const std::string& Result::OutputDirectory::DefaultValue()
        {
            return Utilities::EmptyString();
        }

        
        const std::string& Result::DisplayValue::NameInFile()
        {
            static std::string ret("display_value");
            return ret;
        }
        
        
        const std::string& Result::DisplayValue::Description()
        {
            static std::string ret("Enables to skip some printing in the console. Can be used to WriteSolution every n time.");
            return ret;
        }
        
        const std::string& Result::DisplayValue::Constraint()
        {
            return Utilities::EmptyString();
        }
        
        
        
        const std::string& Result::DisplayValue::DefaultValue()
        {
            static std::string ret("1");
            return ret;
        }
        
        
    } // namespace InputParameter
    
    
} // namespace MoReFEM


/// @} // addtogroup CoreGroup


