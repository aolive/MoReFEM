///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 10 Apr 2015 16:10:10 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_F_ELT_x_NUMBERING_SUBSET_HPP_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_F_ELT_x_NUMBERING_SUBSET_HPP_

# include "Core/InputParameter/Crtp/Section.hpp"
# include "Core/InputParameter/FElt/Impl/NumberingSubset.hpp"
# include "Core/InputParameterData/InputParameterList.hpp"



namespace MoReFEM
{


    namespace InputParameter
    {


        namespace BaseNS
        {


            /*!
             * \brief Common base class from which all InputParameter::NumberingSubset should inherit.
             *
             * This brief class is used to tag domains within the input parameter data (through std::is_base_of<>).
             */
            struct NumberingSubset
            { };


        } // namespace BaseNS


        //! \copydoc doxygen_hide_core_input_parameter_list_section_with_index
        template<unsigned int IndexT>
        struct NumberingSubset
        : public Crtp::Section<NumberingSubset<IndexT>, NoEnclosingSection>,
        public BaseNS::NumberingSubset
        {


            //! Convenient alias.
            using self = NumberingSubset<IndexT>;

            //! Return the unique id (i.e. 'IndexT').
            static constexpr unsigned int GetUniqueId() noexcept;


            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            /*!
             * \brief Return the name of the section in the input parameter.
             *
             * e.g. 'NumberingSubset1' for IndexT = 1.
             *
             * \return Name of the section in the input parameter.
             */
            static const std::string& GetName();


            /*!
             * \brief Name of the numbering subset.
             */
            struct Name : public Crtp::InputParameter<Name, self, std::string>,
                          public Impl::NumberingSubsetNS::Name
            { };


            /*!
             * \brief Whether a vector defined on this numbering subset might be used to compute a
             * movemesh.
             */
            struct DoMoveMesh : public Crtp::InputParameter<DoMoveMesh, self, bool>,
            public Impl::NumberingSubsetNS::DoMoveMesh
            { };




            //! Alias to the tuple of structs.
            using section_content_type = std::tuple
            <
                Name,
                DoMoveMesh
            >;

        private:

            //! Content of the section.
            section_content_type section_content_;



        }; // struct NumberingSubset


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/InputParameter/FElt/NumberingSubset.hxx"


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_F_ELT_x_NUMBERING_SUBSET_HPP_
