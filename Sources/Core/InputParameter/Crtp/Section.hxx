///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Aug 2015 14:08:45 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_CRTP_x_SECTION_HXX_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_CRTP_x_SECTION_HXX_


namespace MoReFEM
{


    namespace InputParameter
    {


        namespace Impl
        {




        } // namespace Impl


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_CRTP_x_SECTION_HXX_
