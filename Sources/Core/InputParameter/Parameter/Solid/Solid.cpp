///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Aug 2015 15:31:20 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#include "Core/InputParameter/Parameter/Solid/Solid.hpp"


namespace MoReFEM
{
    
    
    namespace InputParameter
    {
        
        
        const std::string& Solid::GetName()
        {
            static std::string ret("Solid");
            return ret;
        };
        
              
        
        const std::string& Solid::PlaneStressStrain::NameInFile()
        {
            static std::string ret("PlaneStressStrain");
            return ret;
        }
        
        
        const std::string& Solid::PlaneStressStrain::Description()
        {
            static std::string ret("For 2D operators, which approximation to use.");
            return ret;
        }
        
        const std::string& Solid::PlaneStressStrain::Constraint()
        {
            static std::string ret( "value_in(v, {'irrelevant', 'plane_strain', 'plane_stress'})");
            return ret;
        }
        
        
        
        const std::string& Solid::PlaneStressStrain::DefaultValue()
        {
            static std::string ret("\"plane_strain\"");
            return ret;
        }
        


        
    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
