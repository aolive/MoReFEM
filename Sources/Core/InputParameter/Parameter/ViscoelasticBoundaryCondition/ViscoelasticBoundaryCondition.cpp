///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Mon, 18 Apr 2016 11:44:26 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#include "Utilities/String/EmptyString.hpp"

#include "Core/InputParameter/Parameter/ViscoelasticBoundaryCondition/ViscoelasticBoundaryCondition.hpp"


namespace MoReFEM
{
    
    
    namespace InputParameter
    {
        
        
        const std::string& ViscoelasticBoundaryCondition::GetName()
        {
            static std::string ret("ViscoelasticBoundaryCondition");
            return ret;
        }
        
        
        const std::string& ViscoelasticBoundaryCondition::Damping::GetName()
        {
            static std::string ret("Damping");
            return ret;
        }
        
        
        const std::string& ViscoelasticBoundaryCondition::Damping::Scalar::Constraint()
        {
            return Utilities::EmptyString();
        }
        
        
        const std::string& ViscoelasticBoundaryCondition::Stiffness::GetName()
        {
            static std::string ret("Stiffness");
            return ret;
        }
        
        
        const std::string& ViscoelasticBoundaryCondition::Stiffness::Scalar::Constraint()
        {
            return Utilities::EmptyString();
        }
        
        
    } // namespace InputParameter
    
    
} // namespace MoReFEM


/// @} // addtogroup CoreGroup
