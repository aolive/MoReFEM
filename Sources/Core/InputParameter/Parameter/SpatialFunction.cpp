///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 2 Jun 2015 15:29:57 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#include "Core/InputParameter/Parameter/SpatialFunction.hpp"


namespace MoReFEM
{
    
    
    namespace InputParameter
    {
        
        
        template<>
        const std::string& SpatialFunction<CoordsType::local>::DescriptionCoordsType()
        {
            static std::string ret("\n Function expects as arguments local coordinates (e.g. coords of a quadrature point).");
            return ret;
        }

        
        
        template<>
        const std::string& SpatialFunction<CoordsType::global>::DescriptionCoordsType()
        {
            static std::string ret("\n Function expects as arguments global coordinates (coordinates in the mesh).");
            return ret;
        }

     
        
    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
