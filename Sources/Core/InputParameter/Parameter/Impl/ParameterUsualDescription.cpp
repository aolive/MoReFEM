///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 9 Jul 2015 20:59:04 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#include "Utilities/String/EmptyString.hpp"

#include "Core/InputParameter/Parameter/Impl/ParameterUsualDescription.hpp"


namespace MoReFEM
{
    
    
    namespace InputParameter
    {
        
        
        namespace Impl
        {
                        
            
            const std::string& Nature::NameInFile()
            {
                static std::string ret("nature");
                return ret;
            }
            
            
            const std::string& Nature::Description()
            {
                static std::string ret("How is given the parameter (as a constant, as a Lua function, per "
                                       "quadrature point, etc...). Choose \"ignore\" if you do not want this "
                                       "parameter (in this case it will stay at nullptr).");
                return ret;
            }
            
            const std::string& Nature::Constraint()
            {
                static std::string ret("value_in(v, {"
                                       "'ignore', "
                                       "'constant', "
                                       "'lua_function',"
                                       "'at_quadrature_point',"
                                       "'piecewise_constant_by_domain'})");
                return ret;
            }
            
            
            const std::string& Nature::DefaultValue()
            {
                return Utilities::EmptyString();
            }
            
            
            
            const std::string& Scalar::NameInFile()
            {
                static std::string ret("scalar_value");
                return ret;
            }
            
            
            const std::string& Scalar::Description()
            {
                static std::string ret("Value of the volumic mass in the case nature is 'constant'"
                                       "(and also initial value if nature is 'at_quadrature_point'; irrelevant otherwise).");
                return ret;
            }
            
            const std::string& Scalar::Constraint()
            {
                return Utilities::EmptyString();
            }
            
            
            const std::string& Scalar::DefaultValue()
            {
                return Utilities::EmptyString();
            }
            
            
            
            const std::string& LuaFunction::NameInFile()
            {
                static std::string ret("lua_function");
                return ret;
            }
            
            
            const std::string& LuaFunction::Description()
            {
                static std::string ret("Value of the volumic mass in the case nature is 'lua_function'"
                                       "(and also initial value if nature is 'at_quadrature_point'; irrelevant otherwise).");
                return ret;
            }
            
            const std::string& LuaFunction::Constraint()
            {
                return Utilities::EmptyString();
            }
            
            
            const std::string& LuaFunction::DefaultValue()
            {
                return Utilities::EmptyString();
            }
            
            
            
            const std::string& PiecewiseConstantByDomainId::NameInFile()
            {
                static std::string ret("piecewise_constant_domain_id");
                return ret;
            }
            
            
            const std::string& PiecewiseConstantByDomainId::Description()
            {
                static std::string ret("Domain indices of the parameter in the case nature is "
                                       "'piecewise_constant_by_domain'. The various domains given here must not "
                                       "intersect.");
                return ret;
            }
            
            const std::string& PiecewiseConstantByDomainId::Constraint()
            {
                return Utilities::EmptyString();
            }
            
            
            const std::string& PiecewiseConstantByDomainId::DefaultValue()
            {
                static std::string ret("{}");
                return ret;
            }
            
            
            const std::string& PiecewiseConstantByDomainValue::NameInFile()
            {
                static std::string ret("piecewise_constant_domain_value");
                return ret;
            }
            
            
            const std::string& PiecewiseConstantByDomainValue::Description()
            {
                static std::string ret("Value of the parameter in the case nature is 'piecewise_constant_by_domain'.");
                return ret;
            }
            
            const std::string& PiecewiseConstantByDomainValue::Constraint()
            {
                return Utilities::EmptyString();
            }
            
            
            const std::string& PiecewiseConstantByDomainValue::DefaultValue()
            {
                static std::string ret("{}");
                return ret;
            }

            
        } // namespace Impl
        
        
    } // namespace InputParameter
  

} // namespace MoReFEM


/// @} // addtogroup CoreGroup
