///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 2 Aug 2013 11:08:01 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_IMPL_x_PARAMETER_USUAL_DESCRIPTION_HPP_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_IMPL_x_PARAMETER_USUAL_DESCRIPTION_HPP_

# include "Utilities/InputParameterList/LuaFunction.hpp"

# include "Core/InputParameter/Parameter/SpatialFunction.hpp"


namespace MoReFEM
{


    namespace InputParameter
    {


        namespace Impl
        {


            /*!
             * \brief Choose how is described the volumic mass (through a scalar, a function, etc...)
             */
            struct Nature
            {


                //! Convenient alias.
                using storage_type = std::string;

                //! Name of the input parameter in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input parameter.
                static const std::string& Description();

                /*!
                 * \return Constraint to fulfill.
                 *
                 * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some text from \a LuaOptionFile example file:
                 *
                 * An age should be greater than 0 and less than, say, 150. It is possible
                 * to check it with a logical expression (written in Lua). The expression
                 * should be written with 'v' being the variable to be checked.
                 * \a constraint = "v >= 0 and v < 150"
                 *
                 * It is possible to check whether a variable is in a set of acceptable
                 * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                 * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                 *
                 * If a vector is retrieved, the constraint must be satisfied on every
                 * element of the vector.
                 */
                static const std::string& Constraint();


                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no value has been given
                 * in the input parameter file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be \a LuaOptionFile-formatted.
                 */
                static const std::string& DefaultValue();
            };



            /*!
             * \brief Scalar value. Irrelevant if nature is not scalar.
             */
            struct Scalar
            {

                //! Convenient alias.
                using storage_type = double;

                //! Name of the input parameter in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input parameter.
                static const std::string& Description();

                /*!
                 * \return Constraint to fulfill.
                 *
                 * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some text from \a LuaOptionFile example file:
                 *
                 * An age should be greater than 0 and less than, say, 150. It is possible
                 * to check it with a logical expression (written in Lua). The expression
                 * should be written with 'v' being the variable to be checked.
                 * \a constraint = "v >= 0 and v < 150"
                 *
                 * It is possible to check whether a variable is in a set of acceptable
                 * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                 * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                 *
                 * If a vector is retrieved, the constraint must be satisfied on every
                 * element of the vector.
                 */
                static const std::string& Constraint();


                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no value has been given
                 * in the input parameter file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be \a LuaOptionFile-formatted.
                 */
                static const std::string& DefaultValue();
            };


            /*!
             * \brief Function that determines volumic mass value. Irrelevant if nature is not lua_function.
             */
            struct LuaFunction : public SpatialFunction<CoordsType::global>
            {

                //! Convenient alias.
                using storage_type = Utilities::InputParameterListNS::LuaFunction<double(double, double, double)>;

                //! Name of the input parameter in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input parameter.
                static const std::string& Description();

                /*!
                 * \return Constraint to fulfill.
                 *
                 * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some text from \a LuaOptionFile example file:
                 *
                 * An age should be greater than 0 and less than, say, 150. It is possible
                 * to check it with a logical expression (written in Lua). The expression
                 * should be written with 'v' being the variable to be checked.
                 * \a constraint = "v >= 0 and v < 150"
                 *
                 * It is possible to check whether a variable is in a set of acceptable
                 * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                 * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                 *
                 * If a vector is retrieved, the constraint must be satisfied on every
                 * element of the vector.
                 */
                static const std::string& Constraint();


                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no value has been given
                 * in the input parameter file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be \a LuaOptionFile-formatted.
                 */
                static const std::string& DefaultValue();
            };


            /*!
             * \brief Piecewise Constant domain index.
             */
            struct PiecewiseConstantByDomainId
            {


                //! Convenient alias.
                using storage_type = std::vector<unsigned int>;

                //! Name of the input parameter in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input parameter.
                static const std::string& Description();

                /*!
                 * \return Constraint to fulfill.
                 *
                 * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some text from \a LuaOptionFile example file:
                 *
                 * An age should be greater than 0 and less than, say, 150. It is possible
                 * to check it with a logical expression (written in Lua). The expression
                 * should be written with 'v' being the variable to be checked.
                 * \a constraint = "v >= 0 and v < 150"
                 *
                 * It is possible to check whether a variable is in a set of acceptable
                 * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                 * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                 *
                 * If a vector is retrieved, the constraint must be satisfied on every
                 * element of the vector.
                 */
                static const std::string& Constraint();


                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no value has been given
                 * in the input parameter file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be \a LuaOptionFile-formatted.
                 */
                static const std::string& DefaultValue();
            };


            /*!
             * \brief Piecewise Constant value by domain.
             */
            struct PiecewiseConstantByDomainValue
            {


                //! Convenient alias.
                using storage_type = std::vector<double>;

                //! Name of the input parameter in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input parameter.
                static const std::string& Description();

                /*!
                 * \return Constraint to fulfill.
                 *
                 * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some text from \a LuaOptionFile example file:
                 *
                 * An age should be greater than 0 and less than, say, 150. It is possible
                 * to check it with a logical expression (written in Lua). The expression
                 * should be written with 'v' being the variable to be checked.
                 * \a constraint = "v >= 0 and v < 150"
                 *
                 * It is possible to check whether a variable is in a set of acceptable
                 * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                 * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                 *
                 * If a vector is retrieved, the constraint must be satisfied on every
                 * element of the vector.
                 */
                static const std::string& Constraint();


                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no value has been given
                 * in the input parameter file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be \a LuaOptionFile-formatted.
                 */
                static const std::string& DefaultValue();
            };



        } // namespace Impl


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/InputParameter/Parameter/Impl/ParameterUsualDescription.hxx"


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_IMPL_x_PARAMETER_USUAL_DESCRIPTION_HPP_
