///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 2 Aug 2013 11:08:01 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_ELECTRICAL_ACTIVATION_x_ELECTRICAL_ACTIVATION_HPP_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_ELECTRICAL_ACTIVATION_x_ELECTRICAL_ACTIVATION_HPP_

# include "Core/InputParameter/Crtp/Section.hpp"
# include "Core/InputParameter/Parameter/Impl/ParameterUsualDescription.hpp"


namespace MoReFEM
{


    namespace InputParameter
    {


        //! \copydoc doxygen_hide_core_input_parameter_list_section
        struct ElectricalActivation : public Crtp::Section<ElectricalActivation, NoEnclosingSection>
        {

            /*!
             * \brief Return the name of the section in the input parameter ('ElectricalActivation' here).
             *
             */
            static const std::string& GetName();

            //! Convenient alias.
            using self = ElectricalActivation;

            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            /*!
             * \brief Choose how is described the  (through a scalar, a function, etc...)
             */
            struct Delay : public Crtp::InputParameter<Delay, self, double>
            {


                //! Name of the input parameter in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input parameter.
                static const std::string& Description();

                /*!
                 * \return Constraint to fulfill.
                 *
                 * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some text from \a LuaOptionFile example file:
                 *
                 * An age should be greater than 0 and less than, say, 150. It is possible
                 * to check it with a logical expression (written in Lua). The expression
                 * should be written with 'v' being the variable to be checked.
                 * \a constraint = "v >= 0 and v < 150"
                 *
                 * It is possible to check whether a variable is in a set of acceptable
                 * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                 * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                 *
                 * If a vector is retrieved, the constraint must be satisfied on every
                 * element of the vector.
                 */
                static const std::string& Constraint();

                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no value has been given
                 * in the input parameter file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be \a LuaOptionFile-formatted.
                 */
                static const std::string& DefaultValue();
            };


            /*!
             * \brief Choose how is described the parameter (through a scalar, a function, etc...)
             */
            struct AmplitudeMax : public Crtp::InputParameter<AmplitudeMax, self, double>
            {


                //! Name of the input parameter in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input parameter.
                static const std::string& Description();

                /*!
                 * \return Constraint to fulfill.
                 *
                 * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some text from \a LuaOptionFile example file:
                 *
                 * An age should be greater than 0 and less than, say, 150. It is possible
                 * to check it with a logical expression (written in Lua). The expression
                 * should be written with 'v' being the variable to be checked.
                 * \a constraint = "v >= 0 and v < 150"
                 *
                 * It is possible to check whether a variable is in a set of acceptable
                 * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                 * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                 *
                 * If a vector is retrieved, the constraint must be satisfied on every
                 * element of the vector.
                 */
                static const std::string& Constraint();

                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no value has been given
                 * in the input parameter file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be \a LuaOptionFile-formatted.
                 */
                static const std::string& DefaultValue();
            };


            /*!
             * \brief Choose how is described the parameter (through a scalar, a function, etc...)
             */
            struct AmplitudeMin : public Crtp::InputParameter<AmplitudeMin, self, double>
            {


                //! Name of the input parameter in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input parameter.
                static const std::string& Description();

                /*!
                 * \return Constraint to fulfill.
                 *
                 * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some text from \a LuaOptionFile example file:
                 *
                 * An age should be greater than 0 and less than, say, 150. It is possible
                 * to check it with a logical expression (written in Lua). The expression
                 * should be written with 'v' being the variable to be checked.
                 * \a constraint = "v >= 0 and v < 150"
                 *
                 * It is possible to check whether a variable is in a set of acceptable
                 * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                 * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                 *
                 * If a vector is retrieved, the constraint must be satisfied on every
                 * element of the vector.
                 */
                static const std::string& Constraint();

                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no value has been given
                 * in the input parameter file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be \a LuaOptionFile-formatted.
                 */
                static const std::string& DefaultValue();
            };


            /*!
             * \brief Choose how is described the parameter (through a scalar, a function, etc...)
             */
            struct DepolarizationDuration : public Crtp::InputParameter<DepolarizationDuration, self, double>
            {


                //! Name of the input parameter in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input parameter.
                static const std::string& Description();

                /*!
                 * \return Constraint to fulfill.
                 *
                 * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some text from \a LuaOptionFile example file:
                 *
                 * An age should be greater than 0 and less than, say, 150. It is possible
                 * to check it with a logical expression (written in Lua). The expression
                 * should be written with 'v' being the variable to be checked.
                 * \a constraint = "v >= 0 and v < 150"
                 *
                 * It is possible to check whether a variable is in a set of acceptable
                 * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                 * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                 *
                 * If a vector is retrieved, the constraint must be satisfied on every
                 * element of the vector.
                 */
                static const std::string& Constraint();

                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no value has been given
                 * in the input parameter file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be \a LuaOptionFile-formatted.
                 */
                static const std::string& DefaultValue();
            };


            /*!
             * \brief Choose how is described the parameter (through a scalar, a function, etc...)
             */
            struct PlateauDuration : public Crtp::InputParameter<PlateauDuration, self, double>
            {


                //! Name of the input parameter in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input parameter.
                static const std::string& Description();

                /*!
                 * \return Constraint to fulfill.
                 *
                 * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some text from \a LuaOptionFile example file:
                 *
                 * An age should be greater than 0 and less than, say, 150. It is possible
                 * to check it with a logical expression (written in Lua). The expression
                 * should be written with 'v' being the variable to be checked.
                 * \a constraint = "v >= 0 and v < 150"
                 *
                 * It is possible to check whether a variable is in a set of acceptable
                 * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                 * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                 *
                 * If a vector is retrieved, the constraint must be satisfied on every
                 * element of the vector.
                 */
                static const std::string& Constraint();

                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no value has been given
                 * in the input parameter file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be \a LuaOptionFile-formatted.
                 */
                static const std::string& DefaultValue();
            };


            /*!
             * \brief Choose how is described the parameter (through a scalar, a function, etc...)
             */
            struct RepolarizationDuration : public Crtp::InputParameter<RepolarizationDuration, self, double>
            {


                //! Name of the input parameter in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input parameter.
                static const std::string& Description();

                /*!
                 * \return Constraint to fulfill.
                 *
                 * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some text from \a LuaOptionFile example file:
                 *
                 * An age should be greater than 0 and less than, say, 150. It is possible
                 * to check it with a logical expression (written in Lua). The expression
                 * should be written with 'v' being the variable to be checked.
                 * \a constraint = "v >= 0 and v < 150"
                 *
                 * It is possible to check whether a variable is in a set of acceptable
                 * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                 * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                 *
                 * If a vector is retrieved, the constraint must be satisfied on every
                 * element of the vector.
                 */
                static const std::string& Constraint();

                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no value has been given
                 * in the input parameter file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be \a LuaOptionFile-formatted.
                 */
                static const std::string& DefaultValue();
            };

            /*!
             * \brief Choose how is described the parameter (through a scalar, a function, etc...)
             */
            struct Type : public Crtp::InputParameter<Type, self, std::string>
            {


                //! Name of the input parameter in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input parameter.
                static const std::string& Description();

                /*!
                 * \return Constraint to fulfill.
                 *
                 * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some text from \a LuaOptionFile example file:
                 *
                 * An age should be greater than 0 and less than, say, 150. It is possible
                 * to check it with a logical expression (written in Lua). The expression
                 * should be written with 'v' being the variable to be checked.
                 * \a constraint = "v >= 0 and v < 150"
                 *
                 * It is possible to check whether a variable is in a set of acceptable
                 * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                 * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                 *
                 * If a vector is retrieved, the constraint must be satisfied on every
                 * element of the vector.
                 */
                static const std::string& Constraint();

                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no value has been given
                 * in the input parameter file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be \a LuaOptionFile-formatted.
                 */
                static const std::string& DefaultValue();
            };


            //! Alias to the tuple of structs.
            using section_content_type = std::tuple
            <
                Delay,
                AmplitudeMax,
                AmplitudeMin,
                DepolarizationDuration,
                PlateauDuration,
                RepolarizationDuration,
                Type
            >;

        private:

            //! Content of the section.
            section_content_type section_content_;


        }; // struct ElectricalActivation


    } // InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/InputParameter/Parameter/ElectricalActivation/ElectricalActivation.hxx"


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_ELECTRICAL_ACTIVATION_x_ELECTRICAL_ACTIVATION_HPP_
