///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 20 May 2015 14:19:50 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#include "Core/InputParameter/Parameter/Diffusion/Diffusion.hpp"
#include "Utilities/String/EmptyString.hpp"


namespace MoReFEM
{
    
    
    namespace InputParameter
    {
        
        
        const std::string& Diffusion::TransfertCoefficient::GetName()
        {
            static std::string ret("TransfertCoefficient");
            return ret;
        }
        
    
    } // namespace InputParameter
    
    
} // namespace MoReFEM


/// @} // addtogroup CoreGroup
