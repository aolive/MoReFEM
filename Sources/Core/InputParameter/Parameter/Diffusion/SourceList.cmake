target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Density.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Diffusion.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/TransfertCoefficient.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Diffusion.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Diffusion.hxx"
)

