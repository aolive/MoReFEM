///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 8 Jun 2015 12:06:22 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_TIME_MANAGER_x_TIME_MANAGER_INSTANCE_HXX_
# define MOREFEM_x_CORE_x_TIME_MANAGER_x_TIME_MANAGER_INSTANCE_HXX_


namespace MoReFEM
{



    template<class EvolutionPolicyT>
    template<class InputParameterDataT>
    TimeManagerInstance<EvolutionPolicyT>::TimeManagerInstance(const InputParameterDataT& input_parameter_data)
    : TimeManager(input_parameter_data),
    EvolutionPolicyT(input_parameter_data)
    { }


    template<class EvolutionPolicyT>
    inline void TimeManagerInstance<EvolutionPolicyT>::IncrementTime()
    {
        IncrementNtimeModified();
        EvolutionPolicyT::IncrementTime(GetNonCstTime());
    }


    template<class EvolutionPolicyT>
    inline void TimeManagerInstance<EvolutionPolicyT>::ResetTimeAtPreviousTimeStep()
    {
        EvolutionPolicyT::DecrementTime(GetNonCstTime());
    }


    template<class EvolutionPolicyT>
    double TimeManagerInstance<EvolutionPolicyT>::GetTimeStep() const
    {
        return EvolutionPolicyT::GetTimeStep();
    }


    template<class EvolutionPolicyT>
    bool TimeManagerInstance<EvolutionPolicyT>::HasFinished() const
    {
        return EvolutionPolicyT::HasFinished(GetTime());
    }


    template<class EvolutionPolicyT>
    bool TimeManagerInstance<EvolutionPolicyT>::IsTimeStepConstant() const
    {
        return EvolutionPolicyT::IsTimeStepConstant();
    }


    template<class EvolutionPolicyT>
    double TimeManagerInstance<EvolutionPolicyT>::GetMaximumTime() const
    {
        return EvolutionPolicyT::GetMaximumTime();
    }


    template<class EvolutionPolicyT>
    void TimeManagerInstance<EvolutionPolicyT>::AdaptTimeStep(const Wrappers::Mpi& mpi,
                                                              policy_to_adapt_time_step a_policy_to_adapt_time_step)
    {
        const auto time = GetTime();
        return EvolutionPolicyT::AdaptTimeStep(mpi, a_policy_to_adapt_time_step, time);
    }


    template<class EvolutionPolicyT>
    void TimeManagerInstance<EvolutionPolicyT>::SetTimeStep(double time_step)
    {
        EvolutionPolicyT::SetTimeStep(time_step);
    }


    template<class EvolutionPolicyT>
    double& TimeManagerInstance<EvolutionPolicyT>::GetNonCstTimeStep()
    {
        return EvolutionPolicyT::GetNonCstTimeStep();
    }

    template<class EvolutionPolicyT>
    void TimeManagerInstance<EvolutionPolicyT>::ResetTimeManagerAtInitialTime()
    {
        EvolutionPolicyT::ResetTimeManagerAtInitialTime(GetNonCstTime());
    }



} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_TIME_MANAGER_x_TIME_MANAGER_INSTANCE_HXX_
