///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 5 Jun 2015 17:24:41 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_CONSTANT_TIME_STEP_HXX_
# define MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_CONSTANT_TIME_STEP_HXX_


namespace MoReFEM
{


    namespace TimeManagerNS
    {


        namespace Policy
        {


            template<class InputParameterDataT>
            ConstantTimeStep::ConstantTimeStep(const InputParameterDataT& input_parameter_data)
            {
                namespace IPL = Utilities::InputParameterListNS;
                using TimeManager = InputParameter::TimeManager;

                time_step_ = IPL::Extract<TimeManager::TimeStep>::Value(input_parameter_data);
                maximum_time_ = IPL::Extract<TimeManager::TimeMax>::Value(input_parameter_data);
                initial_time_ = IPL::Extract<TimeManager::TimeInit>::Value(input_parameter_data);

                 assert(time_step_ > 1.e-9 && "Should have been already checked when reading the input parameter data "
                        "hence the assert rather than an exception.");
            }


            inline void ConstantTimeStep::IncrementTime(double& time)
            {
                auto& Niterations_from_start = NiterationsFromStart();
                ++Niterations_from_start;

                time = GetInitialTime() + Niterations_from_start * GetTimeStep();
            }


            inline void ConstantTimeStep::DecrementTime(double& time)
            {
                auto& Niterations_from_start = NiterationsFromStart();
                --Niterations_from_start;

                time = GetInitialTime() + Niterations_from_start * GetTimeStep();;
            }


            inline double ConstantTimeStep::GetTimeStep() const noexcept
            {
                assert(!NumericNS::AreEqual(time_step_, std::numeric_limits<double>::lowest()));
                return time_step_;
            }


            inline double& ConstantTimeStep::GetNonCstTimeStep()
            {
                assert(!NumericNS::AreEqual(time_step_, std::numeric_limits<double>::lowest()));
                return time_step_;
            }


            inline double ConstantTimeStep::GetMaximumTime() const noexcept
            {
                assert(!NumericNS::AreEqual(maximum_time_, std::numeric_limits<double>::lowest()));
                return maximum_time_;
            }


            inline bool ConstantTimeStep::HasFinished(const double time) const
            {
                return time + NumericNS::DefaultEpsilon<double>() >= GetMaximumTime();
            }


            inline constexpr bool ConstantTimeStep::IsTimeStepConstant() noexcept
            {
                return true;
            }


            inline unsigned int& ConstantTimeStep::NiterationsFromStart() noexcept
            {
                return Niterations_from_start_;
            }


            inline double ConstantTimeStep::GetInitialTime() const noexcept
            {
                return initial_time_;
            }


            [[noreturn]] inline void ConstantTimeStep::AdaptTimeStep(const Wrappers::Mpi& mpi,
                                                                     policy_to_adapt_time_step a_policy_to_adapt_time_step,
                                                                     const double time)
            {
                static_cast<void>(mpi);
                static_cast<void>(a_policy_to_adapt_time_step);
                static_cast<void>(time);
                assert(false && "AdaptTimeStep meaningless for current policy, ConstantTimeStep.");
                exit(EXIT_FAILURE);
            }


            inline void ConstantTimeStep::SetTimeStep(double time_step)
            {
                time_step_ = time_step;
            }

            inline void ConstantTimeStep::ResetTimeManagerAtInitialTime(double& time)
            {
                time = initial_time_;
                Niterations_from_start_ = 0;
            }


        } // namespace Policy


    } // namespace TimeManagerNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_CONSTANT_TIME_STEP_HXX_
