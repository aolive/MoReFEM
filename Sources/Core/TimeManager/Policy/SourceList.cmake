target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ConstantTimeStep.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/ConstantTimeStep.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/VariableTimeStep.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/VariableTimeStep.hxx"
)

