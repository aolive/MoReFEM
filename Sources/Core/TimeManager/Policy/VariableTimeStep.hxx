///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 26 Jul 2017 14:46:48 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_VARIABLE_TIME_STEP_HXX_
# define MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_VARIABLE_TIME_STEP_HXX_


namespace MoReFEM
{


    namespace TimeManagerNS
    {


        namespace Policy
        {


            template<class InputParameterDataT>
            VariableTimeStep::VariableTimeStep(const InputParameterDataT& input_parameter_data)
            {
                namespace IPL = Utilities::InputParameterListNS;
                using TimeManager = InputParameter::TimeManager;

                time_step_ = IPL::Extract<TimeManager::TimeStep>::Value(input_parameter_data);
                maximum_time_ = IPL::Extract<TimeManager::TimeMax>::Value(input_parameter_data);
                minimum_time_step_ = IPL::Extract<TimeManager::MinimumTimeStep>::Value(input_parameter_data);
                maximum_time_step_ = time_step_;
                initial_time_ = IPL::Extract<TimeManager::TimeInit>::Value(input_parameter_data);

                assert(time_step_ > 1.e-9);
            }


            inline void VariableTimeStep::IncrementTime(double& time) const
            {
                time += GetTimeStep();
            }


            inline void VariableTimeStep::DecrementTime(double& time) const
            {
                time -= GetTimeStep();
            }


            inline double VariableTimeStep::GetTimeStep() const
            {
                assert(!NumericNS::AreEqual(time_step_, std::numeric_limits<double>::lowest()));
                return time_step_;
            }


            inline double VariableTimeStep::GetMaximumTime() const
            {
                assert(!NumericNS::AreEqual(maximum_time_, std::numeric_limits<double>::lowest()));
                return maximum_time_;
            }


            inline bool VariableTimeStep::HasFinished(const double time) const
            {
                return ((time > GetMaximumTime()) || NumericNS::AreEqual(time, GetMaximumTime(), GetMinimumTimeStep() / 2.));
            }


            inline constexpr bool VariableTimeStep::IsTimeStepConstant() noexcept
            {
                return false;
            }


            inline void VariableTimeStep::AdaptTimeStep(const Wrappers::Mpi& mpi,
                                                       policy_to_adapt_time_step a_policy_to_adapt_time_step,
                                                       const double time)
            {
                auto& time_step = GetNonCstTimeStep();
                auto minimum_time_step = GetMinimumTimeStep();

                switch (a_policy_to_adapt_time_step)
                {
                    case policy_to_adapt_time_step::decreasing:
                    {
                        if (NumericNS::AreEqual(time_step, minimum_time_step))
                        {
                            throw Exception("Your simulation tries to decrease the time step to less than the minimum time step allowed.",
                                            __FILE__, __LINE__);
                        }

                        if (time_step > minimum_time_step)
                        {
                            time_step = std::max(0.5 * time_step, minimum_time_step);

                            if (mpi.IsRootProcessor())
                            {
                                std::cout << std::endl << "Time step subiteration with time step " << time_step << "." << std::endl;
                            }
                        }
                        break;
                    }
                    case policy_to_adapt_time_step::increasing:
                    {
                        const auto maximum_time_step = GetMaximumTimeStep();

                        const double modulus = time - maximum_time_step * std::floor(time / maximum_time_step);

                        bool condition_max_time_step = ((maximum_time_step - modulus) < minimum_time_step);
                        condition_max_time_step |=
                            NumericNS::AreEqual(maximum_time_step - modulus, minimum_time_step, 0.5 * minimum_time_step);

                        const double new_time_step =
                            condition_max_time_step
                            ? maximum_time_step
                            : std::min(maximum_time_step - modulus, maximum_time_step);

                        if (!NumericNS::AreEqual(new_time_step, time_step))
                        {
                            time_step = new_time_step;

                            if (mpi.IsRootProcessor())
                            {
                                std::cout << std::endl << "Time step re-adaptation with time step " << new_time_step << "." << std::endl;
                            }
                        }
                        break;
                    }
                }
            }


            inline double& VariableTimeStep::GetNonCstTimeStep() noexcept
            {
                return time_step_;
            }


            inline double VariableTimeStep::GetMaximumTimeStep() const noexcept
            {
                return maximum_time_step_;
            }


            inline double VariableTimeStep::GetMinimumTimeStep() const noexcept
            {
                return minimum_time_step_;
            }


            inline void VariableTimeStep::SetTimeStep(double time_step)
            {
                time_step_ = time_step;
            }


            inline void VariableTimeStep::ResetTimeManagerAtInitialTime(double& time)
            {
                time = initial_time_;
            }


        } // namespace Policy


    } // namespace TimeManagerNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_VARIABLE_TIME_STEP_HXX_
