///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 31 Mar 2015 17:11:45 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_NUMBERING_SUBSET_HXX_
# define MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_NUMBERING_SUBSET_HXX_


namespace MoReFEM
{


    inline bool operator==(const NumberingSubset& lhs, const NumberingSubset& rhs)
    {
        return lhs.GetUniqueId() == rhs.GetUniqueId();
    }


    inline bool operator<(const NumberingSubset& lhs, const NumberingSubset& rhs)
    {
        return lhs.GetUniqueId() < rhs.GetUniqueId();
    }


    inline bool operator!=(const NumberingSubset& lhs, const NumberingSubset& rhs)
    {
        return !(operator==(lhs, rhs));
    }


    inline bool NumberingSubset::DoMoveMesh() const noexcept
    {
        return do_move_mesh_;
    }


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_NUMBERING_SUBSET_HXX_
