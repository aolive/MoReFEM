///
////// \file
///
///
/// Created by Sebastien Gilles <srpgilles@gmail.com> on the Fri, 15 Feb 2013 12:45:11 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_DATA_x_INPUT_PARAMETER_LIST_HPP_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_DATA_x_INPUT_PARAMETER_LIST_HPP_

# include <type_traits>
# include <map>
# include <string>

# include "Utilities/InputParameterList/Base.hpp"
# include "Utilities/Exceptions/Exception.hpp"
# include "Utilities/Containers/Print.hpp"

# include "Utilities/Mpi/Mpi.hpp"

# include "Core/MoReFEMData/MoReFEMData.hpp" // not used here directly, but all models which include current header
                                             // nedd this one at the same time.


namespace MoReFEM
{


    /// \addtogroup CoreGroup
    ///@{

    /*!
     * \brief This class read the input parameters and then is in charge of holding the values read.
     *
     * It hold the charge of checking that mandatory parameters are correctly filled in the data file as soon
     * as possible: we want for instance to avoid a program failure at the end because a path for an output
     * file is invalid.
     *
     */
    template<class TupleT>
    class InputParameterList : public Utilities::InputParameterListNS::Base<InputParameterList<TupleT>, TupleT>
    {

    public:

        //! Alias to unique smart pointers.
        using const_unique_ptr = std::unique_ptr<const InputParameterList>;

        //! Special members.
        //@{

        /*!
         * \brief Constructor.
         *
         * \param[in] filename Name of the input parameter list to be read. This file is expected to be formatted
         * in \a LuaOptionFile format.
         * \param[in] do_track_unused_fields Whether a field found in the file but not referenced in the tuple
         * yields an exception or not. Should be 'yes' most of the time; I have introduced the 'no' option for
         * cases in which we need only a handful of parameters shared by all models for post-processing
         * purposes (namely mesh-related ones).
         * \copydoc doxygen_hide_mpi_param
         */
        explicit InputParameterList(const std::string& filename,
                                    const Wrappers::Mpi& mpi,
                                    Utilities::InputParameterListNS::DoTrackUnusedFields do_track_unused_fields =
                                    Utilities::InputParameterListNS::DoTrackUnusedFields::yes);

        //! Destructor
        ~InputParameterList() = default;

        //! Copy constructor.
        InputParameterList(const InputParameterList&) = delete;

        //! Move constructor.
        InputParameterList(InputParameterList&&) = delete;

        //! Copy affectation.
        InputParameterList& operator=(const InputParameterList&) = delete;

        //! Move affectation.
        InputParameterList& operator=(InputParameterList&&) = delete;


        //@}


    private:

        //! Check the vectors defined in Variable section are the same length; throw an exception if not.
//        void CheckVariableConsistency() const;

        //! Check the vectors defined in BoundaryCondition section are the same length; throw an exception if not.
//        void CheckBoundaryConditionConsistency() const;

    };


    ///@} // \addtogroup CoreGroup


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/InputParameterData/InputParameterList.hxx"


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_DATA_x_INPUT_PARAMETER_LIST_HPP_
