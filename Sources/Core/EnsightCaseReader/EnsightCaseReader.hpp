///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 20 Jul 2016 11:01:34 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_ENSIGHT_CASE_READER_x_ENSIGHT_CASE_READER_HPP_
# define MOREFEM_x_CORE_x_ENSIGHT_CASE_READER_x_ENSIGHT_CASE_READER_HPP_

# include <memory>
# include <vector>
# include <string>


namespace MoReFEM
{


    /*!
     * \brief Struct to store the Ensight Geometry of a case.
     *
     * This function stores the Ensight geometry.
     *
     */
    struct EnsightGeometry
    {
        /*!
         * \brief Constructor.
         *
         * \param[in] time_set Time set of the case.
         * \param[in] file Geometry file.
         */
        EnsightGeometry(unsigned int time_set, const std::string& file);

        //! Time set of the geometry.
        unsigned int time_set_;

        //! Geometry file.
        std::string file_;
    };


    /*!
     * \brief Struct to store the Ensight Time of a case.
     *
     * This function stores the Ensight Time.
     *
     */
    struct EnsightTime
    {

        /*!
         * \brief Constructor.
         *
         * \param[in] time_set Time set of the case.
         * \param[in] number_of_steps Number of steps of the case.
         * \param[in] filename_start_number Number of the first file.
         * \param[in] filename_increment Increment of the name file.
         * \param[in] time_values Values of the times.
         */
        EnsightTime(unsigned int time_set,
                    unsigned int number_of_steps,
                    unsigned int filename_start_number,
                    unsigned int filename_increment,
                    const std::vector<std::pair<double, unsigned int>>& time_values);

        //! Time set of the case.
        unsigned int time_set_;

        //! Number of steps of the case.
        unsigned int number_of_steps_;

        //! Number of the first file.
        unsigned int filename_start_number_;

        //! Increment of the name file.
        unsigned int filename_increment_;

        //! Values of the times.
        std::vector<std::pair<double, unsigned int>> time_values_;
    };


    /*!
     * \brief Class to read an Ensight case.
     *
     * This function read an Ensight case.
     *
     */
    class EnsightCaseReader
    {

    public:

        //! Alias to self.
        using self = EnsightCaseReader;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

    public:

        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] filename name of the file.
         *
         */
        explicit EnsightCaseReader(std::string filename);

        //! Destructor.
        ~EnsightCaseReader() = default;

        //! Copy constructor.
        EnsightCaseReader(const EnsightCaseReader&) = delete;

        //! Move constructor.
        EnsightCaseReader(EnsightCaseReader&&) = delete;

        //! Copy affectation.
        EnsightCaseReader& operator=(const EnsightCaseReader&) = delete;

        //! Move affectation.
        EnsightCaseReader& operator=(EnsightCaseReader&&) = delete;

        ///@}

        //! Print the info of the case.
        void Print();

    public:

        //! Accessor to the EnsightGeometry.
        const std::vector<EnsightGeometry>& GetEnsightGeometry() const noexcept;

        //! Accessor to the EnsightTime.
        const std::vector<EnsightTime>& GetEnsightTime() const noexcept;

    private:

        //! Name of the file.
        std::string filename_;

        //! EnsightGeometry of the case.
        std::vector<EnsightGeometry> geometry_;

        //! EnsightTime of the case.
        std::vector<EnsightTime> time_;

    };


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/EnsightCaseReader/EnsightCaseReader.hxx"


#endif // MOREFEM_x_CORE_x_ENSIGHT_CASE_READER_x_ENSIGHT_CASE_READER_HPP_
