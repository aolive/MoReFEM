///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 21 Jan 2015 15:50:06 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_HXX_
# define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_HXX_


namespace MoReFEM
{


    template
    <
        class InputParameterDataT,
        Utilities::InputParameterListNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    MoReFEMData<InputParameterDataT, DoTrackUnusedFieldsT, AdditionalCommandLineArgumentsPolicyT>
    ::MoReFEMData(int argc, char** argv, std::string&& suppl_result_subdir)
    {
        Wrappers::Mpi::InitEnvironment(argc, argv);

        mpi_ = std::make_unique<Wrappers::Mpi>(0, Wrappers::MpiNS::Comm::World);
        petsc_ = std::make_unique<Wrappers::Petsc::Petsc>(__FILE__, __LINE__);

        auto& environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

        std::string input_parameter_file;

        try
        {
            TCLAP::CmdLine cmd("Command description message");
            cmd.setExceptionHandling(false);

            TCLAP::ValueArg<std::string> input_parameter_file_arg("i",
                                                                  "input_parameters",
                                                                  "Input parameter file (Lua)",
                                                                    true,
                                                                  "",
                                                                  "string",
                                                                  cmd);

            TCLAP::MultiArg<Wrappers::Tclap::StringPair> env_arg("e",
                                                                 "env",
                                                                 "environment_variable",
                                                                 false,
                                                                 "string=string",
                                                                 cmd);

            if constexpr (!std::is_same<AdditionalCommandLineArgumentsPolicyT, NoAdditionalCommandLineArgument>())
                AdditionalCommandLineArgumentsPolicyT::Add(cmd);

            cmd.parse(argc, argv);

            decltype(auto) env_var_list = env_arg.getValue();

            for (const auto& pair : env_var_list)
                environment.SetEnvironmentVariable(pair.GetValue(), __FILE__, __LINE__);

            input_parameter_file = input_parameter_file_arg.getValue();
        }
        catch (TCLAP::ArgException &e)  // catch any exceptions
        {
            std::ostringstream oconv;
            oconv << "Caught command line exception " << e.error() << " for argument " << e.argId() << std::endl;
            throw Exception(oconv.str(), __FILE__, __LINE__);
        }

        const auto& mpi = GetMpi();

        // Set the MOREFEM_RESULT_DIR to default value if not yet settled.
        {
            const char* const result_dir = "MOREFEM_RESULT_DIR";

            if (!environment.DoExist(result_dir))
            {
                const char* const default_value("/Volumes/Data/${USER}/MoReFEM/Results");

                environment.SetEnvironmentVariable(std::make_pair("MOREFEM_RESULT_DIR", default_value),
                                                   __FILE__, __LINE__);

                std::cout << "[WARNING] Environment variable '" << result_dir << "' was not defined; default value '"
                << default_value << "' has therefore be provided. This environment variable may appear in mesh "
                "directory defined in input parameter file; if not it is in fact unused." << std::endl;

            }

            const char* const morefem_dir = "MOREFEM_ROOT";

            if (!environment.DoExist(morefem_dir))
            {
                const char* const default_value("${HOME}/Codes/MoReFEM/CoreLibrary");

                environment.SetEnvironmentVariable(std::make_pair("MOREFEM_ROOT", default_value),
                                                   __FILE__, __LINE__);

                std::cout << "[WARNING] Environment variable '" << morefem_dir << "' was not defined; default value '"
                << default_value << "' has therefore be provided. This environment variable may appear in output "
                "directory defined in input parameter file; if not it is in fact unused." << std::endl;

            }

            mpi.Barrier();
        }


        // Check the input parameter file can be found on each processor.
        const bool do_file_exist_for_rank = FilesystemNS::File::DoExist(input_parameter_file) ;
        std::vector<bool> sent_data { do_file_exist_for_rank };
        std::vector<bool> gathered_data;

        mpi.Gather(sent_data, gathered_data);

        // Root processor sent true if all files exist for all ranks, false otherwise.
        // Beware (#461): it doesn't check here the file is consistent!
        std::vector<bool> do_file_exist_for_all_ranks { true };
        std::vector<std::size_t> rank_without_file;

        if (mpi.IsRootProcessor())
        {
            for (std::size_t i = 0ul, Nproc = gathered_data.size(); i < Nproc; ++i)
            {
                if (!gathered_data[i])
                    rank_without_file.push_back(i);
            }

            if (!rank_without_file.empty())
                do_file_exist_for_all_ranks.back() = false;
        }

        mpi.Broadcast(do_file_exist_for_all_ranks);

        if (mpi.IsRootProcessor())
        {
            if (!do_file_exist_for_rank)
            {
                Utilities::InputParameterListNS::CreateDefaultInputFile<typename InputParameterDataT::Tuple>(input_parameter_file);

                std::cout << '\n' << input_parameter_file << " wasn't existing and has just been created on processor 0; "
                "please edit it and then copy it onto all machines intended to run the code in parallel." << std::endl;

                throw Exception("Input parameter file to edit and complete!", __FILE__, __LINE__);
            }

            if (!rank_without_file.empty())
            {
                std::ostringstream oconv;
                oconv << "The following ranks couldn't find the input parameter file: ";
                Utilities::PrintContainer(rank_without_file, oconv, ", ", "", ".\n");
                throw Exception(oconv.str(), __FILE__, __LINE__);
            }
        }
        else
        {
            if (do_file_exist_for_all_ranks.back() == false)
                throw Exception("Input parameter file not found for one or more rank; see root processor "
                                "exception for more details!", __FILE__, __LINE__);
        }

        // We can be here only if the file exists...
        input_parameter_data_ = std::make_unique<InputParameterDataT>(input_parameter_file,
                                                                      mpi,
                                                                      DoTrackUnusedFieldsT);

        namespace ipl = Utilities::InputParameterListNS;
        using Result = InputParameter::Result;

        result_directory_ = ipl::Extract<Result::OutputDirectory>::Folder(*input_parameter_data_);

        if (!suppl_result_subdir.empty())
        {
            result_directory_ += "/" + suppl_result_subdir;

            if (mpi.IsRootProcessor() && !FilesystemNS::Folder::DoExist(result_directory_))
                FilesystemNS::Folder::Create(result_directory_, __FILE__, __LINE__);

            mpi.Barrier();
        }

        InitTimeKeepLog(mpi, result_directory_);
    }


    template
    <
        class InputParameterDataT,
        Utilities::InputParameterListNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    MoReFEMData<InputParameterDataT, DoTrackUnusedFieldsT, AdditionalCommandLineArgumentsPolicyT>
    ::~MoReFEMData()
    {

        // Flush all the standard outputs. Catch all exceptions: exceptions in destructor are naughty!
        const auto& mpi = GetMpi();

        try
        {
            Wrappers::Petsc::SynchronizedFlush(mpi, stdout, __FILE__, __LINE__);
            Wrappers::Petsc::SynchronizedFlush(mpi, stderr, __FILE__, __LINE__);
        }
        catch (const std::exception& e)
        {
            std::cerr << "Untimely exception caught in ~MoReFEMData(): " << e.what() << std::endl;
            assert(false && "No exception in destructors!");
        }
        catch (...)
        {
            assert(false && "No exception in destructors!");
        }

        input_parameter_data_ = nullptr;

        mpi.Barrier(); // to better handle the possible MpiAbort() on one of the branch...
    }


    template
    <
        class InputParameterDataT,
        Utilities::InputParameterListNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    inline const Wrappers::Mpi&
    MoReFEMData<InputParameterDataT, DoTrackUnusedFieldsT, AdditionalCommandLineArgumentsPolicyT>
    ::GetMpi() const
    {
        assert(!(!mpi_));
        return *mpi_;
    }


    template
    <
        class InputParameterDataT,
        Utilities::InputParameterListNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    const InputParameterDataT& MoReFEMData<InputParameterDataT, DoTrackUnusedFieldsT, AdditionalCommandLineArgumentsPolicyT>::GetInputParameterList() const
    {
        assert(!(!input_parameter_data_));
        return *input_parameter_data_;
    }


    template
    <
        class InputParameterDataT,
        Utilities::InputParameterListNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    const std::string& MoReFEMData<InputParameterDataT, DoTrackUnusedFieldsT, AdditionalCommandLineArgumentsPolicyT>
    ::GetResultDirectory() const
    {
        return result_directory_;
    }


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_HXX_
