///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 21 Jan 2015 15:50:06 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_HPP_
# define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_HPP_


# include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
# include "ThirdParty/Wrappers/Petsc/Petsc.hpp"
# include "ThirdParty/Wrappers/Petsc/Print.hpp"
# include "ThirdParty/Wrappers/Tclap/StringPair.hpp"
# include "ThirdParty/IncludeWithoutWarning/Tclap/Tclap.hpp"

# include "Utilities/Filesystem/File.hpp"
# include "Utilities/InputParameterList/Base.hpp"
# include "Utilities/TimeKeep/TimeKeep.hpp"
# include "Utilities/Environment/Environment.hpp"

# include "Core/InputParameter/Result.hpp"
# include "Core/InitTimeKeepLog.hpp"


namespace MoReFEM
{


    /// \addtogroup CoreGroup
    ///@{


    //! Empty shell when no additional command line argument are required in the model for which \a MoReFEMData is built.
    struct NoAdditionalCommandLineArgument
    { };


    /*!
     * \brief Init MoReFEM: initialize mpi and read the input parameter file.
     *
     * \warning As mpi is not assumed to exist until the constructor has done is job, the exceptions there that might
     * happen only on some of the ranks don't lead to a call to MPI_Abort(), which can lead to a dangling program.
     * Make sure each exception is properly communicated to all ranks so that each rank can gracefully throw
     * an exception and hence allow the program to stop properly.
     *
     * \tparam AdditionalCommandLineArgumentsPolicyT Policy if you need additional arguments on the command line.
     * To see a concrete example of this possibility, have a look at Test/Core/MoReFEMData/test_command_line_options.cpp
     * which demonstrate the possibility.
     *
     * http://tclap.sourceforge.net gives a nice manual of how to add additional argument on the command lines.
     * By default, there is one mandatory argument (--input_parameters *lua file*) and one optional that might be
     * repeated to define pseudo environment variables (-e KEY=VALUE).
     */
    template
    <
        class InputParameterDataT,
        Utilities::InputParameterListNS::DoTrackUnusedFields DoTrackUnusedFieldsT = Utilities::InputParameterListNS::DoTrackUnusedFields::yes,
        class AdditionalCommandLineArgumentsPolicyT = NoAdditionalCommandLineArgument
    >
    class MoReFEMData : public AdditionalCommandLineArgumentsPolicyT
    {

    public:

        //! Alias to self.
        using self = MoReFEMData<InputParameterDataT, DoTrackUnusedFieldsT, AdditionalCommandLineArgumentsPolicyT>;

        //! Alias to unique_ptr.
        using const_unique_ptr = std::unique_ptr<self>;

        //! Alias to \a InputParameterDataT.
        using input_parameter_data_type = InputParameterDataT;

        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] argc Number of argument in the command line (including the program name).
         * \param[in] argv List of arguments read.
         * \param[in] suppl_result_subdir If not empty, add to the path read in the input parameter file the subdir
         * specified here. The whole directory is created if it doesn't exist already.
         */
        explicit MoReFEMData(int argc, char** argv, std::string&& suppl_result_subdir = "");

        //! Destructor.
        ~MoReFEMData();

        //! Copy constructor.
        MoReFEMData(const MoReFEMData&) = delete;

        //! Move constructor.
        MoReFEMData(MoReFEMData&&) = delete;

        //! Copy affectation.
        MoReFEMData& operator=(const MoReFEMData&) = delete;

        //! Move affectation.
        MoReFEMData& operator=(MoReFEMData&&) = delete;

        ///@}

        //! Accessor to underlying mpi object.
        const Wrappers::Mpi& GetMpi() const;

        //! Accessor to underlying InputParameterList object.
        const InputParameterDataT& GetInputParameterList() const;

        //! Accessor to the result directory, in which all the outputs of MoReFEM should be written.
        const std::string& GetResultDirectory() const;

    private:

        //! Holds Mpi object.
        Wrappers::Mpi::const_unique_ptr mpi_ = nullptr;

        //! Holds RAII Petsc object, which ensures PetscFinalize() is called in due time.
        Wrappers::Petsc::Petsc::const_unique_ptr petsc_ = nullptr;

        //! Holds InputParameterList.
        typename InputParameterDataT::const_unique_ptr input_parameter_data_ = nullptr;

        //! Result directory, in which all the outputs of MoReFEM should be written.
        std::string result_directory_;

    };


    /*!
     *
     * \class doxygen_hide_init_morefem_param
     *
     * \param[in] morefem_data The object which encapsulates some stuff that acts as global data, such as:
     * - The content of the input parameter data.
     * - Mpi related informations.
     * - The directory into which output is to be written.
     */


    ///@} // \addtogroup


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/MoReFEMData/MoReFEMData.hxx"


#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_HPP_
