///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 27 Jan 2016 14:47:22 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_LINEAR_ALGEBRA_x_GLOBAL_DIAGONAL_MATRIX_HPP_
# define MOREFEM_x_CORE_x_LINEAR_ALGEBRA_x_GLOBAL_DIAGONAL_MATRIX_HPP_

# include "Core/LinearAlgebra/GlobalMatrix.hpp"


namespace MoReFEM
{


    /// \addtogroup CoreGroup
    ///@{


    /*!
     * \brief Specific case of GlobalMatrix which is square and diagonal.
     *
     * \internal <b><tt>[internal]</tt></b> Actually there is not much here that signes the diagonal structure of the matrix, save the fact only one
     * numbering subset is used for both rows and columns. However, AllocateGlobalMatrix() provides an overload for
     * this type that allocates only values on the diagonal.
     */
    class GlobalDiagonalMatrix final : public GlobalMatrix
    {

    public:

        //! \copydoc doxygen_hide_alias_self
        using self = GlobalDiagonalMatrix;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to direct parent.
        using parent = GlobalMatrix;

    public:

        /// \name Special members.
        ///@{

        //! Constructor.
        explicit GlobalDiagonalMatrix(const NumberingSubset& numbering_subset);

        //! Destructor.
        ~GlobalDiagonalMatrix() override;

        //! Copy constructor.
        GlobalDiagonalMatrix(const GlobalDiagonalMatrix&) = delete;

        //! Move constructor.
        GlobalDiagonalMatrix(GlobalDiagonalMatrix&&) = delete;

        //! Copy affectation.
        GlobalDiagonalMatrix& operator=(const GlobalDiagonalMatrix&) = delete;

        //! Move affectation.
        GlobalDiagonalMatrix& operator=(GlobalDiagonalMatrix&&) = delete;

        ///@}


    };


    ///@} // \addtogroup CoreGroup


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/LinearAlgebra/GlobalDiagonalMatrix.hxx"


#endif // MOREFEM_x_CORE_x_LINEAR_ALGEBRA_x_GLOBAL_DIAGONAL_MATRIX_HPP_
