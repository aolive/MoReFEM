target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/GlobalDiagonalMatrix.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/GlobalMatrix.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/GlobalVector.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/GlobalDiagonalMatrix.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/GlobalDiagonalMatrix.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/GlobalMatrix.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/GlobalMatrix.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/GlobalVector.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/GlobalVector.hxx"
)

